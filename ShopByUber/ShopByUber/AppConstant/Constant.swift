//
//  Constant.swift
//
//  Created by Administrator on 4/4/17.
//  Copyright © 2017 Differenz System Pvt. Ltd. All rights reserved.
//

import UIKit
import Foundation

//MARK: Device Constant

class Constant: NSObject {
    
    enum UIUserInterfaceIdiom : Int
    {
        case Unspecified
        case Phone
        case Pad
    }
    
    enum RequestStatus:Int {
        case Requested = 0
        case Confirm = 1
        case PayDone = 2
        case OnRouteToClient = 3
        case StartService = 4
        case Completed = 5
        case Cancelled = 6
    }
    struct appTitle {
        static let strTitle = "Shop By Uber"
    }
    struct NotificationType {
        static let PlaySoundOnNotification                   = "Play sound on notification"
        static let VibrateOnNotification                     = "Vibrate on notification"
        static let PulseOnNotification                       = "Pulse light for notification"
        static let VendorIsOnRouteToYourLocation             = "Vendor is on route to your location"
        static let VendorHasArrivedToYourLocation            = "Vendor has arrived to your location"
        static let ServiceHasBeenCompleted                   = "My service has been completed"
        static let ICanReviewMyRecentPurchases               = "I can review my recent purchase"
        static let NewAppFeaturesAreAvailable                = "New app features are available"
        static let ThereAreItemsLeftInMyCart                 = "There are item left in my cart"
        static let NewUpdatesInMyFavouriteService            = "New update in my favorite services"
        
        static let UserHasMadeRequest                        = "User has made request"
        static let UserHasMadePaymentForConfirmedService     = "User has made payment for confirmed service"
        static let UserHasCanceledRequestForConfirmedService = "User has canceled request for confirmed service"
        
    }
    
    struct ScreenSize
    {
        static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
        static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
        static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    }
    
    
    struct DeviceType {
        static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
        static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
        static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
        static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
        static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
        static let IS_IPAD_PRO          = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1366.0
    }
    
    //MARK:- Stripe Sandbox
//    static let stripe_key = "pk_test_vxPSxLscb2NDyzMSvRM9l3Cy"
//    static let stripe_Secret = "sk_test_txT6g58Wj8cV5lTlqBNsO9xm"

    
    //MARK:- Paypal Client ID
    
    // Paypal Pro account detail
    
    //Sandbox
   // static let paypal_Client_Id = "AcHKz-o2MgbkUJhieww9XZkf5PyDFdaRxDUkABERkXFOGPNW_8gW7DNXFlBtPYtZRy0gBG6NBhLWcZNS"
    
    //Live
    static let paypal_Client_Id = "Ab9mRwkuVn-yMpGKVwVEdKnQMUrUDpeCbSqXyC8Eqhz8GUmo5-MmdcFU8KxJ5_xRkPhkCF3oqJL0U1qj"
    
   //----------------------------------------------------------------------------------------------------//
   // Paypal Regular Account
    
    //Sandbox
   // static let paypal_Client_Id = "AXpbE1YKLusg5IlXvykVNg7vBplzK4rR-BbVEWaRNlVASbUFWbC0sLGxeoX9R2Otx_W_IYbBLxWz-85_"
    
   // Paypal Live
  //  static let paypal_Client_Id = "ARnYF_wuYZQdRep1lJPuFkgm0xhzLm3KVApa1gJO5WT_K315KtdCA9IFdSF9vV6r5dDYVQ0zpkFco1aZ"
    
    //Client Secret
    static let paypal_Client_Secret  = "ENOTwbY0Pwbo5ybAh46EpHKU_7APOw8w9bn2RSr1PT94VYI_CkAqMT-KWMd5W8WTWjk2i1H1kZDyykeY"
  //------------------------------------------------------------------------------------------------------//
    
    //SANDBOX
    //katgranstrom27-facilitator@gmail.com -
    static let  sandbox_paypal_Client_Id     = "AXd8v6C-DHwV9E_V94D9n7wBdxrZDIJIiKQDzQgSPfs6wQGdsZmZURXHLxOXmE8UDRDjtqwF7vSkpC7Y"
    static let  sandbox_paypal_Client_Secret = "ENOTwbY0Pwbo5ybAh46EpHKU_7APOw8w9bn2RSr1PT94VYI_CkAqMT-KWMd5W8WTWjk2i1H1kZDyykeY"
    
    
    // Device Type is 2 for iOS
    static let deviceType = 2
    
    static let googleAPIKey = "AIzaSyDXy_AYpcSUFcyQGTsHK-b9yLkRpyu8TZ8"
    static let googleRootsAPIKey = "AIzaSyADnqKqncYsAZ3z7K2iOMDGJI5YJv73uMw"
    static let kNoDataFoundMsg = "No data found"
    
    struct timeSlot{
        
        static let arr_OneHour = ["12:00 am - 1:00 am","1:00 am - 2:00 am","2:00 am - 3:00 am","3:00 am - 4:00 am","4:00 am - 5:00 am","5:00 am - 6:00 pm","6:00 pm - 7:00 pm","7:00 am - 8:00 am","8:00 am - 9:00 am","9:00 am - 10:00 am","10:00 am - 11:00 am","11:00 am - 12:00 pm","12:00 pm - 1:00 pm","1:00 pm - 2:00 pm"]
        static let arr_TwoHour = ["12:00 am - 2:00 am","2:00 am - 4:00 am","4:00 am - 6:00 am","6:00 am - 8:00 am","8:00 am - 10:00 am","10:00 am - 12:00 pm","12:00 pm - 2:00 pm"]
        static let arr_ThreeHour = ["12:00 am - 3:00 am","3:00 am - 6:00 am","6:00 am - 9:00 am","9:00 am - 12:00 pm","12:00 pm - 2:00 pm"]
        static let arr_FourHour = ["12:00 am - 4:00 am","4:00 am - 8:00 am","8:00 am - 12:00 pm","12:00 pm - 2:00 pm"]
        
    }
    
    struct ImageType {
        static let UserProfile = 1
        static let CompanyProfile = 2
        static let ServiceImage = 3
    }
    
    struct Color {
        static let stutusBarColor               = Utilities.colorWithHexString("057987")
     //   static let themeColor                   = Utilities.colorWithHexString("2BB8C8")
        static let buttonBorderColor            = Utilities.colorWithHexString("D0D0D0")
        static let kGreen                       = Utilities.colorWithHexString("34A442")
        static let kBlue                        = Utilities.colorWithHexString("2C65B9")
        static let kSegmentBackgroundColor      = Utilities.colorWithHexString("EEEFEE")
        static let menuSelectedColor            = Utilities.colorWithHexString("00FE67")
        static let menuColor                    = Utilities.colorWithHexString("2BB8C8")
        static let placeholderColor             = Utilities.colorWithHexString("C7C7CD")
        static let sellerUnavailableColor       = Utilities.colorWithHexString("A3A3A3")
        static let serviceAvailableColor        = Utilities.colorWithHexString("FA8606")
        static let serviceAvailableByAppColor   = Utilities.colorWithHexString("29A036")
        static let serviceInProgressColor       = Utilities.colorWithHexString("29A036")
        static let placeholderTextColor         = Utilities.colorWithHexString("999999")
        static let segmentBackgroundColor       = Utilities.colorWithHexString("EEEFEE")
        static let viewShadowColor              = Utilities.colorWithHexString("D3D3D3")
        static let kButtonColor                 = Utilities.colorWithHexString("3AAFC1")
        static let kCancelStatus                = Utilities.colorWithHexString("D52A2B")
        static let kInProgress                  = Utilities.colorWithHexString("2b9e36")
        static let kMapRootStrokColor           = Utilities.colorWithHexString("AAAAAA")
        static let kMapStrokColor               = Utilities.colorWithHexString("26afc1")
        static let kSegmentSelectColor          = Utilities.colorWithHexString("30AD63")
        static let kBuyerSelectedColor          = Utilities.colorWithHexString("D2E6E8")
        static let kBuyerBackgroundColor        = Utilities.colorWithHexString("ECECEC")
        static let kPinkColor                   = Utilities.colorWithHexString("f97a09")
    }
    
    struct UserType{
        static let Buyer = 1
        static let Seller = 2
    }
    
    // DataModel Keys
    struct DataModelKey {
        static let KIsUserLoggedIn            = "UserLoggedIn"
        static let KUserId                    = "userId"
        static let KSessionId                 = "sessionId"
        static let KDeviceToken               = "device_token"
        static let KUserType                  = "user_type"
        static let kIsProfileCreated          = "IsProfileCreated"
        static let KUserEmail                 = "EmailId"
        static let KUserName                  = "Name"
        static let kFirstName                 = "FirstName"
        static let KLastName                  = "LastName"
        static let KUserMobile                = "Mobile"
        static let KUserPhone                 = "Phone"
        static let KUserCountryCode           = "countrycode"
        static let KUserImage                 = "UserImage"
        static let KSellerDescription         = "description"
        static let kLatitude                  = "Latitude"
        static let kLongtitude                = "Longtitude"
        static let kzipCode                   = "zipCode"
        static let kCity                      = "city"
        static let kState                     = "state"
        static let kaddress                   = "address"
        static let kDuration                  = "duration"
        static let kRequestId                 = "requestId"
        static let kScreenName                = "screenName"
        static let kServiceImage              = "serviceImage"
        static let kStatus                    = "status"
        static let kLines                     = "lines"
        static let kLegalBusinessName         = "legalBusinessName"
        static let kezListScreenName          = "ezListScreenName"
        static let kpromoCode                 = "promoCode"
        static let kisSalesperson             = "isSalesperson"
        static let kQuickbloxId               = "quickBloxId"
        static let kcheckVersionUpdate        = "checkVersionUpdate"
        static let kIsServiceAvailable        = "IsServiceAvailable"
        static let kisCommercialPlayed        = "isCommercialPlayed"
    }
    
    //MARK: - API constants
    struct serverAPI {
        
        //MARK: API Constant
        
        struct URL {
            
        //For Internal Testing:- Local Testing api
     //    static let kBaseURL                          = "http://192.168.1.145:8082/TheEZList/api/"
        
        //Live Url
            static let kBaseURL                         = "http://shopbyuber.com/api/"
            
        //UAT Url
         //  static let kBaseURL                            = "http://34.216.180.213/api/"
            
            //MARK: Login Module
            
            static let kRegisterUser                    = kBaseURL + "register/"
            static let kSendCode                        = kBaseURL + "sendVerificationCode/"
            static let kLogin                           = kBaseURL + "login/"
            static let kVerifyCode                      = kBaseURL + "verifyCode/"
            static let kAddCompanyProfile               = kBaseURL + "addCompanyProfile/"
            static let kConnectWithFacebook             = kBaseURL + "connectWithFacebook/"
            static let kConnectWithTwitter              = kBaseURL + "connectWithTwitter/"
            static let kUploadImage                     = kBaseURL + "uploadImage/"
            static let kRecoverPassword                 = kBaseURL + "recoverPassword/"
            static let kRecoverPasswordVerifyCode       = kBaseURL + "recoverPasswordVerifyCode/"
            static let kSetNewPassword                  = kBaseURL + "setNewPassword/"
            static let kLogout                          = kBaseURL + "signOut/"
            static let kUpdateMyProfile                 = kBaseURL + "updateMyProfile/"
            static let kgetUserProfileData              = kBaseURL + "getEditUserData/"
            static let kcheckLogin                      = kBaseURL + "checkLogin/"
            
            //MARK: Buyer Tabs
            
            static let kGetShopbyUberPicks              = kBaseURL + "getShopbyUberPicks/"
            static let kGetPicksCompanyService          = kBaseURL + "getPicksCompanyService/"
            static let kGetCategories                   = kBaseURL + "getCategories/"
            static let kgetSubSubcategories             = kBaseURL + "getSubSubcategories/"
            static let kGetLocalCategories              = kBaseURL + "getLocalCategories/"
            static let kgetLocalVendors                 = kBaseURL + "getLocalVendors/"
            static let kGetCategoryVendors              = kBaseURL + "getCategoryVendors/"
            static let kGetCompanyService               = kBaseURL + "getCompanyService/"
            static let kAddToFavoriteCompany            = kBaseURL + "addToFavoriteCompany/"
            static let kAddToFavoriteService            = kBaseURL + "addToFavoriteService/"
            static let kSendRequestToBook               = kBaseURL + "sendRequestToBook/"
            static let kGetConfirmRequestData           = kBaseURL + "getConfirmRequestData/"
            static let kCancelServiceAppointments       = kBaseURL + "cancelServiceAppointments/"
            static let kPayForService                   = kBaseURL + "payForService/"
            static let kpayment                         = kBaseURL + "payment/"
            static let kpayForServiceAdaptive           = kBaseURL + "payForServiceAdaptive/"
            static let kGetSearchData                   = kBaseURL + "getSearchData/"
            static let kGetFavouriteServices            = kBaseURL + "getFavouriteServices/"
            static let kGetMyPurchases                  = kBaseURL + "getMyPurchases/"
            static let kGetOrderDetail                  = kBaseURL + "getOrderDetail/"
            static let kGetCancelOrderScreenData        = kBaseURL + "getCancelOrderScreenData/"
            static let kGetFavouritesOfUser             = kBaseURL + "getFavouritesOfUser/"
            static let kCancelSingleAppointments        = kBaseURL + "cancelSingleAppointments/"
            static let kYourActivity                    = kBaseURL + "yourActivity/"
            static let kClearNotification               = kBaseURL + "clearNotification/"
            static let kClearYourActivity               = kBaseURL + "clearYourActivity/"
            static let kAddWantedServices               = kBaseURL + "addwantedservices/"
            static let kGetAllWantedList                = kBaseURL + "getAllwantedList/"
            static let kShowWantedDetails               = kBaseURL + "showWantedDetails/"
            static let kgetDetail                       = kBaseURL + "getDetail/"
            static let kCancelWantedService             = kBaseURL + "cancelWantedService/"
            static let kUpdatewantedService             = kBaseURL + "updateWantedService/"
            static let kGetwantedServiceDetailList      = kBaseURL + "getwantedreponselist/"
            
            //MARK: Seller Tabs
            
            static let kGetMyServices                   = kBaseURL + "myServices/"
            static let kSetServiceAvailability          = kBaseURL + "setServiceAvailability/"
            static let kGetBusinessProfile              = kBaseURL + "getBusinessProfile/"
            static let kUpdateBusinessProfile           = kBaseURL + "updateBusinessProfile/"
            static let kMyRequest                       = kBaseURL + "getMyRequest/"
            static let kGetRecentAppointments           = kBaseURL + "getRecentAppointments/"
            static let kGetCompletedAppointments        = kBaseURL + "getCompletedAppointments/"
            static let kGetUpcomingAppointments         = kBaseURL + "getUpcomingAppointments/"
            static let kViewRequestDetail               = kBaseURL + "viewRequestDetail/"
            static let kChangeRequestStatus             = kBaseURL + "changeRequestStatus/"
            static let kDoCancelRequest                 = kBaseURL + "doCancelRequest/"
            static let kSwitchUser                      = kBaseURL + "switchUserLogin/"
            static let kgetStatesList                   = kBaseURL + "getStatesList/"
            static let kgetCitiesList                   = kBaseURL + "getCitiesList/"
            static let kverifyPromoCode                 = kBaseURL + "verifyPromoCode/"
            
            //MARK: Seller ADD/EDIT Service
            
            static let kgetScreenOccupiedData           = kBaseURL + "getScreenOccupiedData/"
            static let kgetCategoryList                 = kBaseURL + "getCategoryList/"
            static let kaddService                      = kBaseURL + "addService/"
            static let kgetDataEditService              = kBaseURL + "getDataEditService/"
            static let kUpdateService                   = kBaseURL + "updateService/"
            static let kGetRecentServices               = kBaseURL + "getRecentServices/"
            static let kMakeNotificationUnread          = kBaseURL + "makeNotificationUnread/"
            static let kGetMySales                      = kBaseURL + "getMySales/"
            static let kDeleteService                   = kBaseURL + "removeService/"
            static let kGetAppointmentOfSpecificDate    = kBaseURL + "getAppointmentOfSpecificDate/"
            
            static let kGetPubnubData                   = kBaseURL + "getPubnubData/"
            //MARK: Notification
            static let kGetNotificationList             = kBaseURL + "getNotificationList/"
            static let kGetUnreadNotificationCount      = kBaseURL + "getUnreadNotificationCount/"
            
            //MARK: Ratings & Reviews 
            static let kGetRatingScreenData             = kBaseURL + "getRatingScreenData/"
            static let kGiveRatingFeedbackService       = kBaseURL + "giveRatingFeedbackService/"
            
            //MARK:- Notification Setting
            static let kSetNotificationData             = kBaseURL + "setNotificationData/"
            static let kGetNotificationData             = kBaseURL + "getNotificationData/"
            
            static let kUpdateDeviceToken               = kBaseURL + "updateDeviceToken/"
            
            static let kGetDuration                     = "https://maps.googleapis.com/maps/api/distancematrix/json?"
            
            //MARK:- Payment
            static let kgetPaymentUrl                   = kBaseURL + "getPaymentUrl/"
            //Contact Us
            static let kContactUs                       = kBaseURL + "contactUs"
            //Terms And Conditions
            static let kTermsAndCondition               = "http://theezlist.com/termsConditions"
            //About This App
            static let kAboutThisApp                    = "http://theezlist.com/aboutApp"
            //Privacy And Policy
            static let kPrivacyAndPolicy                = "http://theezlist.com/privacyPolicy"
            static let kpromoTermsConditions            = "http://theezlist.com/promoTermsConditions"
            
             static let kgetRequestStatus               = kBaseURL + "getRequestStatus"
            //Chat
            static let ksendQuickBloxNotification       = kBaseURL + "sendQuickBloxNotification"
            static let kupdateQuickBloxId               = kBaseURL + "updateQuickBloxId"
            static let kgetUsersFromQuickbloxIds        = kBaseURL + "getUsersFromQuickbloxIds"
        }
        
        struct errorMessages {
            static let kNoInternetConnectionMessage = "Please check your internet connection."
            static let kCommanErrorMessage          = "We had an unexpected error, please try again!"
            static let kServerErrorMessage          = "There seems to be a problem with the connection. Please try again soon."
        }
        
    }
    
    struct Message {
        
        
        static let kDurationChangeTitle             = "Seems appointment duration change!!"
        static let kDurationChange                  = "Are you sure you want to change appointment duration??\nIt will discard your current schedule!!"
        
        static let kChangeMonthTitle                = "Seems schedule data insufficient!!"
        static let kChangeMonth                     = "Are you sure you want switch to next month??\nIt will discard your selections!!"
        
        static let kVerificationCodeSent            = "Verification code sent successfully"
        static let kEnterVarificationCode           = "enter verification code"
        static let kCodeSent                        = "Verification code sent successfully"
        static let kSearchHistory                   = "Search history cleared successfully!"
        static let kNoTimeSlotAvailabel             = "No day & time available for booking"
        
        static let kSearhDefaultMessage              = "Please enter your search topic!"
        static let kSearchNoDataFound                = "No search result found for provided topic"
        static let KServiceNotEditable               = "You Currently Have Appointments Scheduled. You Can Edit This Field Once You Complete or Cancel Your Open Appointments."
        static let kPaymetnExitMessage               =  "Are you sure you want to cancel payment process ?"
    }
    
    // QuickBlox Credentials
    struct QuickBloxCredentials {
        
        //For Testing
//        static let kQBAccountKey = "EYerWtymzgkyEMzvss1a"
//        static let kQuickBloxPassword = "T8EZ07L27tezl"
//        static let kQBApplicationID:UInt = 69295
//        static let kQBAuthKey =   "kSzCdgkyfXrjgjX"
//        static let kQBAuthSecret = "DaMU4aqCrJEJ8WP"
        
        //Live
        static let kQBAccountKey = "HbhL1XeBEh4Ez_xxbW7c"
        static let kQuickBloxPassword = "T8EZ07L27tezl"
        static let kQBApplicationID:UInt = 68211
        static let kQBAuthKey =   "S4mVe3PZJddjjmw"
        static let kQBAuthSecret = "vWjG5JUNN6Zm4eE"
    }
    
    struct DetailsScreenTitle {
        static let kViewRequest                     = "View Request"
        static let kViewAppointment                 = "View Appointment"
        static let kViewSale                        = "View Sale"
    }
    
    struct PopupTitle {
        static let kSelectCategory                  = "Select category"
        static let kSelectSubCategory               = "Select sub category"
        static let kSelectDuration                  = "Service appointment duration"
        static let kSelectDay                       = "Select days you are available"
        static let kSelectTime                      = "Select times you are available"
        static let kAvailability                    = "Your availability"
        static let kReasonToCancel                  = "Reason to cancel"
    }
    
    struct StatusText {
        static let kCONFIRMAPPOINMENT               = "CONFIRM APPOINTMENT"
        static let kONROUTETOCLIENT                 = "PRESS WHEN ON ROUTE TO BUYER"
        static let kSTARTTRACKING                   = "START TRACKING"
        static let kDirectionstoBuyer               = "DIRECTIONS TO BUYER"
        
        static let kRATEREVIEWUS                    = "RATE & REVIEW US!!"
        static let kNotStarted                      = "Waiting"
        static let kOntheWay                        = "On the Way"
        static let kInProgress                      = "In Progress"
        static let kCompleted                       = "Completed"
        static let kCancelledby                     = "Cancelled by "
        static let kCompleteService                 = "COMPLETE SERVICE"
        static let kConfirm                         = "Confirm"
        static let kRequested                       = "Requested"
        static let kWaitingForPayment               = "Waiting for Payment"
    }
    
    struct ValidationMessage {
        static let kSelectDuration                  = "Please select your service appointment duration"
        static let kSelectDay                       = "Please select your appointment availability"
        static let kFillAllInfo                     = "Please fill all required information"
        static let kEnterName                       = "Please enter name"
        static let kfirstName                       = "Please enter first name"
        static let kLastName                        = "Please enter last name"
        static let kEZScreenName                    = "Please enter " + appTitle.strTitle + " screen name"
        static let kValidEmail                      = "Please enter a valid email address"
        static let kValidPaypalEmail                = "Please enter a valid paypal email address"
        static let kMobileNumber                    = "Please enter your mobile number"
        static let kMobileNumberCountryCode         = "Please enter valid mobile number with country code"
        static let kValidMobileNumber               = "Please enter a valid mobile number"
        static let kSelectedState                   = "Please enter your State"
        static let kSelectedCity                    = "Please enter your City"
        static let kNoAppointment                   = "No appointment available"
        static let kEmail                           = "Please enter your email address"
        static let kPaypalEmail                     = "Please enter your paypal email address"
        static let kVarificationCode                = "Please enter your verification code"
        static let kEnterCompanyName                = "Please enter company name"
        static let kCompanyScreenName               = "Please enter company screen name"
        static let kCompanyfirstName                = "Please enter first name"
        static let kCompanylastName                 = "Please enter last name"
        static let kCompnayAddress                  = "Please enter company address/location"
        static let kCompanyPhone                    = "Please enter company phone number"
        static let kCompanyValidPhone               = "Please enter a valid company phone number"
        static let kCompnayWebsite                  = "Please enter your company website"
        static let kWebsiteValidURL                 = "Please enter valid website url"
        static let kLandmark                        = "Please enter landmark"
        static let kAppointment                     = "No appointment available"
        static let kServiceEditWarring              = "You can not edit this service while appointments are in progress"
        static let kServiceDesc                     = "Please enter brief description of your services"
        static let kUserNotVerify                   = "This user is not verified"
        static let kEnterPassword                   = "Please provide a valid password"
        static let kEnterConfirmPassword            = "Please re-enter your password"
        static let kConfirmPassword                 = "Passwords do not match. Please re-enter your password."
        static let kDesc                            = "Please enter a description"
        static let kCancelReasone                   = "Please enter reason to cancel"
        static let kExperience                      = "Please enter a experience"
        static let kSpeciality                      = "Please enter a speciality"
        static let kAddress                         = "Please enter your address"
        static let kStreet                          = "Please enter street address"
        static let kState                           = "Please select state"
        static let kCity                            = "Please select city"
        static let kZipCode                         = "Please enter zip Code"
        static let kZipValidation                   = "Zip Code should be atleast 5 digits"
        static let kRoutingNumber                   = "Please enter routing number"
        static let kAccountNumber                   = "Please enter account number"
        
    }
    struct PlaceHolder {
        static let kPhoto = "placeHolder"
        static let kDefaultUser = "defaultUser"
    }
    
    struct PayPalInfo {
        static let merchantName = "Shop By Uber"
        static let policy = "https://www.paypal.com/webapps/mpp/ua/privacy-full"
        static let agreement = "https://www.paypal.com/webapps/mpp/ua/useragreement-full"
        static let kCurrency = "USD"
        static let kSortDesc = "Shop By Uber"
    }
    
    struct PubNubInfo {
        static let publishKey = "pub-c-d4c80b59-08af-4284-b8c5-eca7639dad30"
        static let subscribeKey = "sub-c-039de6ee-6978-11e7-a3eb-0619f8945a4f"
    }
    
    struct ZoomLevel {
                
        static let World = 1
        static let Landmass = 5
        static let City = 10
        static let Streets = 15
        static let Buildings = 20
    }
    
    static let kArrDays = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]
    static let kArrayHour = ["1 Hour","2 Hours","3 Hours","4 Hours"]
    
    static let kHistory = "history"
    static let kDisableHistory   = "disableHistory"
    
    //MARK:- Notification
    static let kPushToBuyerRegister = "pushToBuyerRegistration"
    static let kPushToSellerRegister = "pushToSellerRegistration"
    
}

enum trackProgress{
    case isFromHome
    case isFromNotification
}

enum isFromActivity : Int
{
    case ServiceDetails
    case Anyothers
}

let kNavController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
let appDeletgate = UIApplication.shared.delegate as! AppDelegate
let appShareManager = Singleton.sharedManager

