//
//  PromotionCodePopup.swift
//  The EZ List
//
//  Created by DIFFERENZ101 on 1/10/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

import UIKit

class PromotionCodePopup: UIView {

    //MARK:- Outlet
    @IBOutlet weak var txtPromoCode: UITextField!
    @IBOutlet weak var btnCheckBoxOutlet: UIButton!
    
    //MARK:- Variable
    var btnSubmitClick : ((String) -> Void)?
    
    //MARK: - View Life Cycle
    override func awakeFromNib() {
      
      
        
    }
    
    override func layoutSubviews() {
        txtPromoCode.useUnderline()
    }
    
    @IBAction func btnTermsConditionCheckBox(_ sender: Any) {
        if btnCheckBoxOutlet.isSelected {
            //Not Selected
            btnCheckBoxOutlet.isSelected = false
        }else {
            //Selected
            btnCheckBoxOutlet.isSelected = true
            
            self.window?.rootViewController?.view.endEditing(true)

            let Popup = Bundle.main.loadNibNamed("Terms&ConditionPopup", owner: self, options: nil)?[0] as! Terms_ConditionPopup
            Popup.frame = self.frame
            self.addSubview(Popup)
        }
    }
    
    @IBAction func btnSubmitClick(_ sender: Any) {
       
        if txtPromoCode.text != "" {
            
            if btnCheckBoxOutlet.isSelected == true {
                
              
                    KVNProgress.show()
                    var dict = [String : Any]()
                    
                    dict["sessionId"]    = DataModel.sharedInstance.onFetchSessionId()
                    dict["userId"]       = DataModel.sharedInstance.onFetchUserId()
                    dict["promoCode"]    = self.txtPromoCode.text
                
                    APIManager.callAPI(url: Constant.serverAPI.URL.kverifyPromoCode, inputParams: dict, success: { (dict) in
                        
                        KVNProgress.dismiss()
                        if dict?["Result"] as? Bool == true {
                            
                            //Save code in a string
                            let code = self.txtPromoCode.text
                            self.removeFromSuperview()
                            self.btnSubmitClick?(code!)
                            
                        }
                        else {
                          
                            KVNProgress.dismiss()
                            self.txtPromoCode.text = ""
                            Utilities.showAlertView(title: "", message: dict?["Message"] as? String ?? "")
                            
                        }
                    }) { error in
                        KVNProgress.dismiss()
                        
                        Utilities.showAlertView(title: dict["Message"] as? String ?? "", message: error)
                        
                    }
                
                
            }else {
                
                print("Enter Code")
                Utilities.showAlertView(title: "", message: "Please accept terms and conditions for submit your promo code.")
            }
        }else {
            
            print("Please Check T&C")
            Utilities.showAlertView(title: "", message: "Please enter promo code")
            
        }
    }
    
    @IBAction func btnClose_Click(_ sender: Any) {
        self.txtPromoCode.text = ""
        self.removeFromSuperview()
    }
    
    
}

extension UITextField {
    
    func useUnderline() {
        removeExtraLayer()
        let border = CALayer()
        border.name = "TextFieldBottomLayer"
        let borderWidth = CGFloat(1.0)
        border.borderColor = UIColor.lightGray.cgColor
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            border.frame = CGRect(x:0,y: self.frame.size.height - 1, width: self.frame.size.width, height:1)
        }
        else{
            border.frame = CGRect(x:0,y: self.frame.size.height - 1, width: self.frame.size.width, height:1)
        }
        
        border.borderWidth = borderWidth
        self.layer.masksToBounds = true
        self.layer.addSublayer(border)
        self.layoutIfNeeded()
    }
    
    func removeExtraLayer() {
        layer.sublayers?.forEach {
            if $0.name == "TextFieldBottomLayer"{
                $0.removeFromSuperlayer()
            }
        }
    }
}
