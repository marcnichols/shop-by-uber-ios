//
//  Rating&ReviewPopup.swift
//  The EZ List
//
//  Created by DIFFERENZ101 on 2/20/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

import UIKit
import Cosmos

class Rating_ReviewPopup: UIView {

    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var lblUserProfileName: UILabel!

    @IBOutlet weak var cosmos_Rating: CosmosView!
    @IBOutlet weak var txtFeedback: UITextView!
    @IBOutlet weak var vw_feedbackcontainner: UIView!
    @IBOutlet weak var btnSubmit: UIButton!
    
    //MARK: - Variables
    var reviewsData: Reviews!
    var vendorImg : String?
    var vendorName : String?
    var requestID : String?
    var serviceID : String?
    var companyID : String?
    
    var btnSubmitClick : (() -> Void)?
    
    //MARK: - View Life Cycle
    override func awakeFromNib() {
        self.initialConfig()
        
    }
    
    //MARK: Private Method
    func initialConfig(){
        
        Utilities.applyShadowEffect(view: [self.vw_feedbackcontainner])
        self.cosmos_Rating.didFinishTouchingCosmos = didFinishTouchingCosmos
        
    }
    
    func setReviewData()  {
        if self.vendorImg != "" {
            self.imgUserProfile.sd_setImage(with:  URL.init(string: (self.vendorImg)!), placeholderImage: #imageLiteral(resourceName: "defaultUser"), options: SDWebImageOptions.retryFailed, completed:{ (image, err, nil, url) in
                if image != nil
                {
                    self.imgUserProfile.image = image
                    self.imgUserProfile.contentMode = .scaleAspectFill
                    self.imgUserProfile.clipsToBounds = true

                }
            })
        }
        
        Utilities.decorateView(imgUserProfile.layer, cornerRadius: self.imgUserProfile.frame.height/2, borderWidth: 5, borderColor: Utilities.colorWithHexString("C5DCDF"))
      
        self.cosmos_Rating.rating    = 0
        self.txtFeedback.text        = ""
        if (self.cosmos_Rating.rating) > 0
        {
            self.btnSubmit.backgroundColor = UIColor.black
            self.btnSubmit.isEnabled = true
        }
        else
        {
            self.btnSubmit.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            self.btnSubmit.isEnabled = false
        }
    }
    
    //MARK: - IBAction Method
    @IBAction func btnSubmit_Clicked(_ sender: Any) {
        if self.cosmos_Rating.rating > 0
        {
            self.callGiveFeedBackApi()
        }
    }
    
    private func didFinishTouchingCosmos(_ rating: Double) {
        if rating > 0
        {
            self.btnSubmit.backgroundColor = UIColor.black 
            self.btnSubmit.isEnabled = true
        }
        else
        {
            self.btnSubmit.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            self.btnSubmit.isEnabled = false
        }
    }
    
    //MARK: - API Call
    
    func callGiveFeedBackApi(){
        self.removeFromSuperview()
        KVNProgress.show()
        var dict = [String : Any]()

        dict["sessionId"]    = DataModel.sharedInstance.onFetchSessionId()
        dict["userId"]       = DataModel.sharedInstance.onFetchUserId()
        dict["serviceId"]    = self.serviceID
        dict["requestId"]    = self.requestID
        dict["companyId"]    = self.companyID
        dict["rating"]       = self.cosmos_Rating.rating
        dict["feedback"]     = self.txtFeedback.text

        APIManager.callAPI(url: Constant.serverAPI.URL.kGiveRatingFeedbackService, inputParams: dict, success: { (dict) in

            KVNProgress.dismiss()
            if dict?["Result"] as? Bool == true {
                Utilities.showAlertWithAction(title: "", message: (dict?["Message"] as? String)!, buttonTitle: "OK", buttonTitle2: "", onOKClick: {
                    Singleton.sharedManager.isFromactivity = .Anyothers
                    //Dismiss popup
                    self.removeFromSuperview()
                   // self.navigationController?.popToRootViewController(animated: true)
                    self.btnSubmitClick?()

                }, onCancelClick: {
                    self.btnSubmitClick?()
                })

            }
        }) { error in
            KVNProgress.dismiss()

            Utilities.showAlertView(title: "", message: error)

        }
    }
    
    
}
