//
//  CreateServiceAlertPopup.swift
//  The EZ List
//
//  Created by mac on 04/06/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

import UIKit

class CreateServiceAlertPopup: UIView {

    @IBOutlet weak var lblAlertMessage: UILabel!
    
    @IBAction func btnYes_Click(_ sender: Any) {
        self.removeFromSuperview()
        
//        self.inputView?.ShowCreateServiceAlertPopup(alert: "Please populate all fields on this screen to create a Service for this Buyer. Once you create the Service, a notification will be sent out to the Buyer to contact you.")
    }
    
    @IBAction func btnNo_Click(_ sender: Any) {
        self.removeFromSuperview()
//        let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
//        let sellerVC = SellerVC(nibName: "SellerVC", bundle: nil)
//        _ = navigationController.pushViewController(sellerVC, animated: true)
    }
    
}
