//
//  Terms&ConditionPopup.swift
//  The EZ List
//
//  Created by DIFFERENZ101 on 1/10/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

import UIKit

class Terms_ConditionPopup: UIView {

    @IBOutlet weak var webView: UIWebView!

    
    override func awakeFromNib() {
        
        
        webView.loadRequest(URLRequest(url: URL(string: Constant.serverAPI.URL.kpromoTermsConditions)!))
        self.layoutIfNeeded()
    }
    
    @IBAction func btnCancle_Click(_ sender: Any) {
        self.removeFromSuperview()
    }
    
    @IBAction func backgroundTap(_ sender: Any) {
        self.removeFromSuperview()
    }
    
}
