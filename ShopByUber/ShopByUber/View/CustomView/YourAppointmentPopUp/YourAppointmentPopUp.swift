//
//  YourAppointmentPopUp.swift
//  The EZ List
//
//  Created by DIFFERENZ101 on 12/18/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class YourAppointmentPopUp: UIView,UITableViewDelegate,UITableViewDataSource {

    //MARK:- Variable
    @IBOutlet var vwContent: UIView!
    @IBOutlet var tblData: UITableView!
    @IBOutlet weak var lblNoDataFound: UILabel!
    @IBOutlet weak var nslcTblHight: NSLayoutConstraint!
    
    var arr_data = [GetAppointmentOfSpecificDate]()
    var btnCloseForEvent : (() -> Void)?
    
    //MARK: - View Life Cycle
    override func awakeFromNib() {
        
        self.initialConfig()
        
        
    }
    
    
    //MARK:- IBAction
    @IBAction func tapOnBackground(_ sender: Any) {
        self.removeFromSuperview()
    }
    //MARK: - Private Methods
    func initialConfig(){
        self.tblData.register(UINib(nibName: "YourAppointmentCell", bundle: nil), forCellReuseIdentifier: "YourAppointmentCell")
        
        self.tblData.dataSource = self
        self.tblData.delegate = self
        self.tblData.reloadData()
        let tblHeight = CGFloat(self.arr_data.count) * 45.0
        let dist = self.frame.height - self.vwContent.frame.height
        let screenSize = self.frame.height - dist
        
        if(tblHeight > screenSize){
            self.tblData.isScrollEnabled = true
        }else{
            self.tblData.isScrollEnabled = false
        }
        
    }
    override func layoutSubviews() {
        if arr_data.count == 0 {
            if Utilities.isDeviceiPad() {
                nslcTblHight.constant = 140
            } else {
                nslcTblHight.constant = 110
            }
        }else{
            var height = self.frame.height - 150 - tblData.contentSize.height
            if height < 0 {
                self.tblData.isScrollEnabled = true
                height = self.frame.height - 150
                nslcTblHight.constant = height
            }
            else {
                self.tblData.isScrollEnabled = false
                nslcTblHight.constant = tblData.contentSize.height
            }
           // nslcTblHight.constant = height// tblData.contentSize.height
            
        }
    }
    
    
    @IBAction func btnClose_Click(_ sender: Any) {
        self.removeFromSuperview()
        self.btnCloseForEvent?()
    }
    

    @IBAction func btnCancle_Click(_ sender: Any) {
        self.removeFromSuperview()
        self.btnCloseForEvent?()
    }
    
    //MARK:- TableView Delegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if Utilities.isDeviceiPad() {
            return 140
        }
        else {
            return 110
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        if arr_data.count == 0 {
            
            lblNoDataFound.isHidden = false
        }else{
            lblNoDataFound.isHidden = true
        }
        
        return arr_data.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "YourAppointmentCell", for: indexPath) as? YourAppointmentCell

        if arr_data[indexPath.row].userImage != nil{
            cell?.imgService.sd_setImage(with: URL.init(string: arr_data[indexPath.row].userImage!), placeholderImage: UIImage(named:Constant.PlaceHolder.kPhoto), options: SDWebImageOptions.retryFailed, completed: nil)
        }else{
            cell?.imgService.image = UIImage.init(named: Constant.PlaceHolder.kPhoto)
        }
        
        cell?.lblServiceName.text = arr_data[indexPath.row].userName
        
        //Converting lower Case today to upperCase
        let today = arr_data[indexPath.row].duration
        let lowerCase =  today?.components(separatedBy: ",")
        if lowerCase![0] == "today" {
            let capitalToday = today?.capitalized
            cell?.lblServiceDate.text = capitalToday
        }else{
            //If not recived "today" in response, dont do anything
            cell?.lblServiceDate.text = arr_data[indexPath.row].duration
        }
        cell?.lblAddress.text = arr_data[indexPath.row].address
        cell?.lblServiceDesc.text = arr_data[indexPath.row].serviceDetail
        cell?.btnCancl.addTarget(self, action: #selector(btnCancelRequest), for: .touchUpInside)
        
        
        return cell!
    }
    
    @objc func btnCancelRequest(_ sender: UIButton) {
        let point = sender.superview?.convert(sender.center, to: self.tblData) ?? .zero
        if let indexPath = self.tblData.indexPathForRow(at: point) {
            
            let timeslot = arr_data[indexPath.row].duration
            let rootView = UIApplication.shared.delegate?.window??.rootViewController as? UINavigationController
            let topView = UIApplication.topViewController(base: rootView)
            
            let alert = UIAlertController(title: "Are you Sure?", message: "Would you like to cancel this appointment for \(String(describing: timeslot!)) ?", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction (title: "Yes", style: .default, handler: { (alert) in
                
                //Show Cancel Reason Alert
                self.superview?.showCancelReasonPopup(obj:self.arr_data[indexPath.row], onSuccessCancel: {
                   
                    self.arr_data.remove(at: indexPath.row)
                    self.tblData.reloadData()
                    
                    
//                    let tblHeight = CGFloat(self.arr_data.count) * 45.0
//                    let dist = self.frame.height - self.vwContent.frame.height
//                    let screenSize = self.frame.height - dist
                    
//                    if(tblHeight > screenSize){
//                        self.tblData.isScrollEnabled = true
//                    }else{
//                        self.tblData.isScrollEnabled = false
//                    }
                    
                }, onCloseClick: {
                    
                })
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
            topView?.present(alert, animated: true, completion: nil)
        }
    }
}
