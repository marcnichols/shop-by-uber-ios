//
//  YourAppointmentCell.swift
//  The EZ List
//
//  Created by DIFFERENZ101 on 12/18/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class YourAppointmentCell: UITableViewCell {
    
    @IBOutlet weak var vwContainer: UIView!
    @IBOutlet weak var imgService: UIImageView!
    @IBOutlet weak var lblServiceName: UILabel!
    @IBOutlet weak var lblServiceDate: UILabel!
    @IBOutlet weak var lblServiceDesc: UILabel!
    @IBOutlet weak var btnCancl: UIButton!
    @IBOutlet weak var lblAddress: UILabel!
    
    var arr_pass = [GetAppointmentOfSpecificDate]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        Utilities.applyShadowEffect(view: [self.vwContainer])
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
}
