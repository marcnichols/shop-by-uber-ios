//
//  CheckBoxPopup.swift
//  ShopByUber
//
//  Created by Administrator on 5/26/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit


class CountryPickPopup: UIView, UITableViewDelegate,UITableViewDataSource {
    
    //MARK: - IBOutlets
    @IBOutlet var vwContent: UIView!
    @IBOutlet var consTextViewHeight: NSLayoutConstraint!
    @IBOutlet weak var tblCountry: UITableView!
    
    var btnSubmitForCountry : ((String) -> Void)?
    var arrCountryFlag = [#imageLiteral(resourceName: "us"),#imageLiteral(resourceName: "in")]
    var arrCountyName = ["United States(US)","India(IN)"]
    var arrCountryCode = ["+1","+91"]
    
    //MARK: - View Life Cycle
    override func awakeFromNib() {
        
        self.initialConfig()
        Utilities.applyShadowEffect(view: [self.vwContent])
        self.tblCountry.tableFooterView = UIView()
        self.consTextViewHeight.constant = CGFloat(44 + (self.arrCountryCode.count *  (Utilities.isDeviceiPad() ? 55 : 40)))
       
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.vwContent.layoutIfNeeded()
    }
    
   
    
    //MARK: - Private Methods
    func initialConfig(){
        self.tblCountry.register(UINib(nibName: "CountryPopupCell", bundle: nil), forCellReuseIdentifier: "CountryPopupCell")
        self.tblCountry.dataSource = self
        self.tblCountry.delegate   = self
    }
    
    //MARK: - TableView Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arrCountryCode.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "CountryPopupCell", for: indexPath) as! CountryPopupCell
        cell.imgCountryFlag.image = self.arrCountryFlag[indexPath.row]
        cell.lblCountryName.text  = self.arrCountyName[indexPath.row]
        cell.lblCountyCode.text   = self.arrCountryCode[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.btnSubmitForCountry!(arrCountryCode[indexPath.row])
        self.removeFromSuperview()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Utilities.isDeviceiPad() ? 50.0 : 40.0
    }
}
