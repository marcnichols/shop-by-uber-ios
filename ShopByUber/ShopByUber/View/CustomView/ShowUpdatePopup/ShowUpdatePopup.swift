//
//  ShowUpdatePopup.swift
//  The EZ List
//
//  Created by mac on 04/06/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

import UIKit
protocol ShowCategoryPopupDelegate {
    func showCategory(selectedCatId: String, selectedIndex: Int, catName: [String])
    func showSubCategory(selectedSubCatId: String, selectedIndex: Int, subCatName: [String])
}

class ShowUpdatePopup: UIView {

    //MARK:- IBOutlet
    @IBOutlet weak var txtCategory: UITextField!
    @IBOutlet weak var txtSubCategory: UITextField!
    @IBOutlet weak var txtScreenName: UITextField!
    @IBOutlet weak var txtDescription: UITextView!
    
    //MARK:- Variable
    
    var selectedCategoryID = ""
    var selectedSubCatagoryID = ""
    var selectedIndexCategory = -1
    var selectedIndexSubCategory = -1
    var categoryID = String()
    
    var arrCategoryList : NSArray = []
    var arrCategoryName = [String]()
    var arrSubCatagoriesNames = [String]()
    var arrCategoryID = [String]()
    var arr_SubCategories = [SubCategory]()
    var requestID = String()
    var isFromWanted = Bool()
    
    var delegate: ShowCategoryPopupDelegate?
    
    override func awakeFromNib() {
        getCategoryList()
        
        
        
    }
    
    //MARK:- API Call
    func getCategoryList() {
        
        KVNProgress.show()
        
        OccupiedData.callGetCategoriesListAPI( success: { (dict) in
            
            KVNProgress.dismiss()
            
            self.arrCategoryList = dict?["Categories"] as! NSArray
            self.arrCategoryName = self.arrCategoryList.value(forKey: "catName") as! [String]
            self.arrCategoryID = self.arrCategoryList.value(forKey: "_id") as! [String]
            self.setCategory()
        }) {
            
            if self.arrCategoryList.count == 0{
                
            }
        }
    }
    
    //MARK:- Private Method
    func setCategory()
    {
        for (index,element) in self.arrCategoryList.enumerated() {
            
            let dic = element as! NSDictionary
            
            categoryID = dic.value(forKey: "_id") as! String
            if categoryID == self.selectedCategoryID {
                
                self.txtCategory.text = dic.value(forKey: "catName") as? String
                self.selectedIndexCategory = index
                print(index)
                self.setSubCategory()
            }
        }
    }
    
    func setSubCategory() {
        
        SubCategory.getSubSubcategories(categoryId: categoryID, allSubcategoryRequired: false, success: { (subcatData) in
            
            self.arr_SubCategories.removeAll()
            self.arr_SubCategories.append(contentsOf: subcatData)
            
    
            self.arrSubCatagoriesNames = self.arr_SubCategories.map({ $0.subCatName! })
            
            for (index,elements) in subcatData.enumerated() {
                let dic = elements

                let subid = dic.value(forKey: "id") as? String
                
                if subid == self.selectedSubCatagoryID{
                    self.txtSubCategory.text = dic.subCatName
                    self.selectedIndexSubCategory = index}
                }

            
            
        }, failure: {_ in
            print("Api Call Fail")
        })
    }
    
    @IBAction func backgroundTap_Click(_ sender: Any) {
        self.removeFromSuperview()
    }
    
    //MARK: IBAction
    @IBAction func vwCategory_Clicked(_ sender: Any) {
        
        self.delegate?.showCategory(selectedCatId: selectedCategoryID, selectedIndex: selectedIndexCategory, catName: arrCategoryName)
    
    }
    
    @IBAction func vwSubCategory_Clicked(_ sender: Any) {
       
        self.delegate?.showSubCategory(selectedSubCatId: selectedSubCatagoryID, selectedIndex: selectedIndexSubCategory, subCatName: arrSubCatagoriesNames)

    }
    
    
    func isValidServiceData(cat: String?, subCat: String?, serviceName: String?, desc: String?)  -> Bool {
        
        let cat = cat?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let subCat = subCat?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let serviceName = serviceName?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let desc = desc?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        var message = ""
        if cat == "" {
            message = "Please select a category for your service"
        }else if subCat == ""{
            message =  "Please select a Subcategory that best matches the Service you are creating!"
        }else if serviceName == ""{
            message =  "Please enter your service name"
        }else if desc == ""{
            message =  "Please enter description for your service"
        }else {
            return true
        }
        Utilities.showAlertView(title: "Alert", message: message)
        return false
    }
    
    
    @IBAction func on_btnOK_Click(_ sender: Any) {
        
        if self.isValidServiceData(cat: txtCategory.text, subCat: txtSubCategory.text, serviceName: txtScreenName.text, desc: txtDescription.text){
            print("===========Success....")
            if isFromWanted {
                //Request new service
                 self.requestServiceDetails()
          //      appDeletgate.isFromRequestWantedService = false
            }else {
                //Update new Service
               
                self.addServiceDetails()
            }
        }
    }
    
    func addServiceDetails(){
        KVNProgress.show()
        var dict = [String:Any]()
        dict["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
        dict["userId"] = DataModel.sharedInstance.onFetchUserId()
        dict["serviceName"] = txtScreenName.text
        dict["categoryId"] = selectedCategoryID
        dict["subCategoryId"] = selectedSubCatagoryID
        dict["serviceDescription"] = txtDescription.text
        
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kAddWantedServices, inputParams: dict, success: { (dict) in
            
            KVNProgress.dismiss()
            
            if dict?["Result"] as? Bool == true {
                
                print("Success")
                
                Utilities.showAlertWithAction(title: "", message: (dict?["Message"] as? String)!, buttonTitle: "OK", buttonTitle2: "", onOKClick: {
                    
                    appDeletgate.onSetupDashboardPage()
                    appDeletgate.isfromMyWantedService = true
                    
                }, onCancelClick: {
                    
                })
            }
            else {
                let msg = dict?["Message"] as? String
                Utilities.showAlertView(title: "", message: msg)
            }
        }) { error in
            
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
        }
    }
    
    func requestServiceDetails(){
        KVNProgress.show()
        var dict = [String:Any]()
        dict["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
        dict["requestId"] = self.requestID
        dict["userId"] = DataModel.sharedInstance.onFetchUserId()
        dict["serviceName"] = txtScreenName.text
        dict["categoryId"] = selectedCategoryID
        dict["subCategoryId"] = selectedSubCatagoryID
        dict["serviceDescription"] = txtDescription.text
        
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kUpdatewantedService, inputParams: dict, success: { (dict) in
            
            KVNProgress.dismiss()
            
            if dict?["Result"] as? Bool == true {
                
                print("Success")
                
                Utilities.showAlertWithAction(title: "", message: (dict?["Message"] as? String)!, buttonTitle: "OK", buttonTitle2: "", onOKClick: {
                    
                    appDeletgate.onSetupDashboardPage()
                    appDeletgate.isfromMyWantedService = true
                }, onCancelClick: {
                    
                })
            }
            else{
                
                let msg = dict?["Message"] as? String
                Utilities.showAlertView(title: "", message: msg)
            }
            
        }) { error in
            
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
        }
    }
    

}
