//
//  UserInfoPopup.swift
//  ShopByUber
//
//  Created by Administrator on 8/28/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class ServiceDetailsPopup: UIView {

    @IBOutlet weak var viewPopup: UIView!
    @IBOutlet weak var imgService: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var lbl3: UILabel!
    @IBOutlet weak var lbl4: UILabel!
    @IBOutlet weak var lbl5: UILabel!
    @IBOutlet weak var lbl6: UILabel!

    //MARK: - View LifeCycle
    override func awakeFromNib() {
        
        self.initialConfig()
    }
    
    //MARK: Private Methods
    func initialConfig(){
        self.backgroundColor = UIColor.black.withAlphaComponent(0.2)
    }
  
    @IBAction func btnTapOutsideView(_ sender: Any) {
        self.removeFromSuperview()
    }
    
  }
