//
//  WantedServicePopUp.swift
//  The EZ List
//
//  Created by mac on 02/06/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

import UIKit

class WantedServicePopUp: UIView {

    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblMessage: UILabel!
    
    override func awakeFromNib() {
        
    }

    @IBAction func btnOK_Click(_ sender: Any) {
        self.removeFromSuperview()
    }
}
