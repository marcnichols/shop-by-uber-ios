//
//  CancellationResonPopUp.swift
//  The EZ List
//
//  Created by DIFFERENZ101 on 12/18/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class CancellationResonPopUp: UIView {

    
    //MARK:- Variable
    var appoinmentObj: GetAppointmentOfSpecificDate?
    var btnCloseForEvent : (() -> Void)?
    var successCancelBlock: (() -> Void)?
    
    @IBOutlet weak var textVwCancelReason: UITextView!
    
    
    //MARK:- View LifeCycle
    override func awakeFromNib() {
        
     
    }
    
    //MARK:- IBAction
    @IBAction func btnCancle_Click(_ sender: Any) {
        self.removeFromSuperview()
    }
    
    @IBAction func btnCancelRequest_Click(_ sender: Any) {

        guard let obj = appoinmentObj else { return }
        if !self.textVwCancelReason.text.isEmpty {
            KVNProgress.show()
            var dict = [String : Any]()

            dict["sessionId"]    = DataModel.sharedInstance.onFetchSessionId()
            dict["userId"]       = DataModel.sharedInstance.onFetchUserId()
            dict["requestId"]    = obj.requestId
            dict["serviceId"]    = obj.serviceId
            dict["timeSlotId"]   = obj.timeSlotId
            dict["flatFee"]      = obj.flatFee
            dict["cancelReason"] = self.textVwCancelReason.text?.trimmingCharacters(in: .whitespaces)
            print(dict)
            
            APIManager.callAPI(url: Constant.serverAPI.URL.kDoCancelRequest, inputParams: dict, success: { (dict) in

                KVNProgress.dismiss()
                if dict?["Result"] as? Bool == true {
                    self.successCancelBlock?()
                    Utilities.showAlertView(title: "", message: dict?["Message"] as? String)
                    self.removeFromSuperview()
                }
                else
                {
                    Utilities.showAlertView(title: "", message: dict?["Message"] as? String)
                }
            }) { error in
                KVNProgress.dismiss()

                Utilities.showAlertView(title: "", message: error)
            }
        }
        else
        {
            Utilities.showAlertView(title: "", message: Constant.ValidationMessage.kCancelReasone)
        }
        
    }
}
