//
//  UserInfoPopup.swift
//  ShopByUber
//
//  Created by Administrator on 8/28/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit
protocol UserInfoPopupDelegate: class {
    
    func btnProcessed_Click(strCID : String, strScreenName: String)
}
class UserInfoPopup: UIView {

    @IBOutlet weak var viewPopup: UIView!
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var txtCompanyDetails: UITextView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnProcessed: UIButton!
    
    @IBOutlet weak var imgPlaceHolder: UIImageView!
    @IBOutlet weak var constarinttextViewHeight: NSLayoutConstraint!
    //MARK: - Variables
    var delegate: UserInfoPopupDelegate?
    var companyID = String()
    var ScreenName = String()
    //MARK: - View LifeCycle
    override func awakeFromNib() {
        
        self.initialConfig()
    }
    
    //MARK: Private Methods
    func initialConfig(){
        
        self.backgroundColor = UIColor.black.withAlphaComponent(0.2)
    }
    //MARK: - IBAction Methods
    @IBAction func btnCancel(_ sender: Any) {
        
        self.removeFromSuperview()
        
    }
    @IBAction func btnProcessed_Click(_ sender: Any) {
        self.delegate?.btnProcessed_Click(strCID: companyID, strScreenName: ScreenName)
        self.removeFromSuperview()
    }
}
