//
//  BorderExtensionView.swift
//  ShopByUber
//
//  Created by Administrator on 5/23/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class BorderExtensionView: UIView {

    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = true
        }
    }
 
    
}
