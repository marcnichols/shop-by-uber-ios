//
//  AlertViewPopUp.swift
//  The EZ List
//
//  Created by mac on 02/06/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

import UIKit

class AlertViewPopUp: UIView {

    @IBOutlet weak var vwContainer: UIView!
    
    @IBOutlet weak var lblalertMessage: UILabel!
    
    
    override func awakeFromNib() {
        vwContainer.layer.cornerRadius = 3
    }

    @IBAction func btnOk_Click(_ sender: Any) {
        self.removeFromSuperview()
    }
}
