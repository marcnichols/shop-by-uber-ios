//
//  CheckBoxPopup.swift
//  ShopByUber
//
//  Created by Administrator on 5/26/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class InfoPopup: UIView {

    //MARK: IBOutlets
    @IBOutlet var vwContent: UIView!
    @IBOutlet var lblHeading: UILabel!
    @IBOutlet var vwContentBottom: NSLayoutConstraint!
    @IBOutlet weak var txt_content: UITextView!
    @IBOutlet var consTextViewHeight: NSLayoutConstraint!
    
    //MARK: View Life Cycle
    override func awakeFromNib() {
        self.initialConfig()
        Utilities.applyShadowEffect(view: [self.vwContent])
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
       
        self.vwContent.layoutIfNeeded()
    }
    
    @IBAction func btnSubmit_Clicked(_ sender: UIButton) {
        self.removeFromSuperview()
    }
    
    //MARK: - Private Methods
    func initialConfig(){

    }
}
