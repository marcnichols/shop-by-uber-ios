//
//  PopupCell.swift
//  ShopByUber
//
//  Created by Administrator on 5/27/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class PopupCell: UITableViewCell {

    @IBOutlet var imgCheckBox: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
