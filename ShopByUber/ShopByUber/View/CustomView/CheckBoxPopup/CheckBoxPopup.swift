//
//  CheckBoxPopup.swift
//  ShopByUber
//
//  Created by Administrator on 5/26/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

enum PopupType{
    case checkbox
    case radiobox
    case event
}

class CheckBoxPopup: UIView,UITableViewDelegate,UITableViewDataSource {

    //MARK: - IBOutlets
    @IBOutlet var vwContent: UIView!
    @IBOutlet var lblHeading: UILabel!
    @IBOutlet var tblData: UITableView!
    @IBOutlet var vwContentBottom: NSLayoutConstraint!
    @IBOutlet weak var nslcTableHight: NSLayoutConstraint!
    
    @IBOutlet weak var lblNoDataFound: UILabel!
    //MARK: - Variables
    var arr_tblData = [String]()
    var btnSubmitForCheckBox : (([String],[Int]) -> Void)?
    var btnSubmitForRadio : ((String,Int) -> Void)?
    var btnSubmitForEvent : (([String]) -> Void)?
    var type = PopupType.checkbox
    
    //CheckBox Variables
    var arr_SelectedIndex = [Int]()
    var arr_AlreadySelectedIndex = [Int]()
    
    //RadioBox Variables
    var selectedIndex:Int?
    var selectedText:String?
    var isDeletable = false
    
    //MARK: - View Life Cycle
    override func awakeFromNib() {
        
        self.initialConfig()
        Utilities.applyShadowEffect(view: [self.vwContent])
    }
    
    override func layoutSubviews() {
        if arr_tblData.count == 0 {
            if Utilities.isDeviceiPad() {
                nslcTableHight.constant = 140
            } else {
                nslcTableHight.constant = 80//105
            }
        }else{
            var height = self.frame.height - 220 - tblData.contentSize.height
            if height < 0 {
                self.tblData.isScrollEnabled = true
                height = self.frame.height - 220
                nslcTableHight.constant = height
            }
            else {
                self.tblData.isScrollEnabled = false
                nslcTableHight.constant = tblData.contentSize.height
            }
        }
    }
    
    @IBAction func btnSubmit_Clicked(_ sender: UIButton) {
        
        if self.type == PopupType.checkbox {
           
            //CheckBox
            if btnSubmitForCheckBox != nil {
                var arrText = [String]()
                for i in self.arr_SelectedIndex{
                    let text = self.arr_tblData[i]
                    arrText.append(text)
                }
                btnSubmitForCheckBox!(arrText,self.arr_SelectedIndex)
            }
        }else if self.type == PopupType.radiobox{
        
            //Radio Button
            if btnSubmitForRadio != nil{
                if selectedIndex != nil
                {
                if selectedIndex != -1 && selectedIndex! < self.arr_tblData.count {
                    btnSubmitForRadio!(self.selectedText ?? self.arr_tblData[selectedIndex!] ,self.selectedIndex ?? 0) 
                }
                }
            }
        }else{
        
            //Event
            
            if btnSubmitForEvent != nil{
                
                btnSubmitForEvent!(self.arr_tblData)
            }
        }
        self.removeFromSuperview()
    }
    
    @IBAction func tapOnBackground(_ sender: Any) {
        
        self.removeFromSuperview()
    }
    
    //MARK: - Private Methods
    func initialConfig(){
        self.tblData.register(UINib(nibName: "PopupCell", bundle: nil), forCellReuseIdentifier: "PopupCell")
        
        self.tblData.dataSource = self
        self.tblData.delegate = self
        
        tblData.estimatedRowHeight = 45.0
        tblData.rowHeight = UITableViewAutomaticDimension
        
    }
  
    //MARK: - TableView Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if arr_tblData.count == 0 {
            
            lblNoDataFound.isHidden = false
        }else{
            lblNoDataFound.isHidden = true
        }
        return self.arr_tblData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        let tblHeight = CGFloat(self.arr_tblData.count) * 45.0
//        let dist = self.frame.height - self.vwContent.frame.height
//        let screenSize = self.frame.height - dist
        
//        if(tblHeight > screenSize){
//            self.tblData.isScrollEnabled = true
//        }else{
//            self.tblData.isScrollEnabled = false
//        }

        let cell = tableView.dequeueReusableCell(withIdentifier: "PopupCell", for: indexPath) as? PopupCell
        
        cell?.lblTitle.text = self.arr_tblData[indexPath.row]
        
        if self.type == PopupType.checkbox {
            
            //CheckBox Popup
            
            if arr_AlreadySelectedIndex.contains(indexPath.row) {
                
                cell?.imgCheckBox.image = UIImage.init(named: "checkIcon")
                
            }else{
                
                if self.arr_SelectedIndex.contains(indexPath.row){
                    
                    cell?.imgCheckBox.image = UIImage.init(named: "checkIcon")
                    
                }else{
                    
                    cell?.imgCheckBox.image = UIImage.init(named: "unCheck")
                }

            }
            
        }else if self.type == PopupType.radiobox{
        
            if self.selectedIndex == indexPath.row{
            
                cell?.imgCheckBox.image = UIImage.init(named: "radioSelect")
            
            }else{
            
                cell?.imgCheckBox.image = UIImage.init(named: "unCheck")
            }
        
        }else{
        
            cell?.imgCheckBox.image = UIImage.init(named: "trashIcon")
            
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.type == PopupType.checkbox{
        
            //CheckBox Popup
            
            if arr_AlreadySelectedIndex.contains(indexPath.row) {
                
                Utilities.showAlertView(title: "", message: "Service is already created by you for the selected day.")
                
                
            }else{
            
                if self.arr_SelectedIndex.contains(indexPath.row){

                        //Remove
                        self.arr_SelectedIndex.remove(at: self.arr_SelectedIndex.index(of: indexPath.row)!)
                    
                }else{
                    //Add
                    self.arr_SelectedIndex.append(indexPath.row)
                    
                }

            }
            
            
        }else if self.type == PopupType.radiobox{
        
            //RadioBox
            
            self.selectedIndex = indexPath.row
            self.selectedText = self.arr_tblData[indexPath.row]
            
        }else{
        
            //Event Delete
            if isDeletable {
                self.arr_tblData.remove(at: indexPath.row)
            }
            else
            {
                //Dont Show Availibility popup
                Utilities.showAlertView(title: "", message: Constant.Message.KServiceNotEditable)
            }

        }
        
        tableView.reloadData()
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return Utilities.isDeviceiPad() ? 55.0 : 45.0
//    }

    
}
