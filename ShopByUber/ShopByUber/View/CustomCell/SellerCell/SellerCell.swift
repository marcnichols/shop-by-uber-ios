//
//  SellerCell.swift
//  ShopByUber
//
//  Created by Administrator on 6/8/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

protocol SellerDelegate : class{
    
    func availabilitySwitchTap(sender:UISwitch,index: Int)
    
}

class SellerCell: UITableViewCell {

    @IBOutlet var imgService: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblAmount: UILabel!
    @IBOutlet var lblstatus: UILabel!
    @IBOutlet var status_Switch: UISwitch!
    @IBOutlet var vwBG: UIView!
    var index: Int!
    weak var delegate: SellerDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .clear
        status_Switch.transform = CGAffineTransform.init(scaleX: 0.75, y: 0.75)
        status_Switch.layer.cornerRadius = 16.0
       
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func availabilitySwitch_Clicked(_ sender: UISwitch) {
        
        delegate?.availabilitySwitchTap(sender: sender, index: index)
        
    }
    
}
