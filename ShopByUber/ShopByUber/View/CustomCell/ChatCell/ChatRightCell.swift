//
//  ChatCell.swift
//  QSMobile
//
//  Created by Administrator on 6/28/17.
//  Copyright © 2017 Differenz System Pvt. Ltd. All rights reserved.
//

import UIKit

class ChatRightCell: UITableViewCell {

    //MARK: IBOutlets
    @IBOutlet weak var rightBubble: UIView!
    @IBOutlet weak var lblMessageSent: UILabel!
    @IBOutlet weak var lblSentTime: UILabel!
    
    //MARK: View life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
    
        self.rightBubble.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
