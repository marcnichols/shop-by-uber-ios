//
//  ChatCell.swift
//  QSMobile
//
//  Created by Administrator on 6/28/17.
//  Copyright © 2017 Differenz System Pvt. Ltd. All rights reserved.
//

import UIKit

class ChatLeftCell: UITableViewCell {

    //MARK: IBOutlets
    @IBOutlet weak var leftBubble: UIView!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblRcdTime: UILabel!
    
    //MARK: View life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.leftBubble.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
