//
//  BookingDayTimeCell.swift
//  ShopByUber
//
//  Created by Differenz215 on 6/22/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class BookingDayTimeCell: UITableViewCell {

    @IBOutlet weak var lblbookTime: UILabel!
    @IBOutlet weak var img_Dropdown: UIImageView!
    
    @IBOutlet weak var lblBookDayTitle: UILabel!
    @IBOutlet weak var vw_content: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
