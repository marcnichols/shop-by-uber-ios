//
//  BankPaymentCell.swift
//  The EZ List
//
//  Created by DIFFERENZ101 on 12/1/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

protocol PassBankTextfield {
    func userInputBankData(_ dic: [String:String])
}
class BankPaymentCell: UITableViewCell, UITextFieldDelegate {

    //MARK:- IBOutlet
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtStreetName: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtState: UITextField!
    @IBOutlet weak var txtZipCode: UITextField!
    @IBOutlet weak var txtRoutingNumber: UITextField!
    @IBOutlet weak var txtAccNumber: UITextField!
   
    @IBOutlet weak var vwState: UIView!
    @IBOutlet weak var vwCity: UIView!
    @IBOutlet weak var vwAddress: UIView!
    
    //MARK:- Variable

    
    let dropDown = DropDown()
    var arr_StateList = [StateList]()
    var arr_CitiesList = [CitiesList]()
    var stateCode = String()
    var cityCode = String()
    
    var delegate: PassBankTextfield?
    var userBankInput = [String:String]()
    
    var userInput = [String:Any]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        txtState.tag = 1
        txtCity.tag = 2
        
        txtState.delegate = self
        txtCity.delegate = self
       
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.callGetStateAPI()
        }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- Textfield Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == txtState || textField == txtCity {
            if textField == txtState {
                
                dropDown.dataSource = arr_StateList.flatMap({ $0.stateName })
                textField.inputView = UIView()
                
            }else if textField == txtCity {
                dropDown.dataSource = arr_CitiesList.flatMap({ $0.cityName })
                textField.inputView = UIView()
            }
            dropDown.anchorView = textField
            dropDown.direction = .any
            dropDown.width = Utilities.isDeviceiPad() ? 200 : 125
            
            dropDown.selectionAction = {(index: Int, item: String) in
                if textField.tag == 1 {
                    self.txtCity.text = ""
                    self.cityCode = ""
                    
                    let state = self.arr_StateList.flatMap({ $0.stateId })
                    self.stateCode = state[index]
                    
                    self.txtState.text = self.dropDown.dataSource[index]
                    self.callGetCityAPI(index: index)
                }else {
                    let city = self.arr_CitiesList.flatMap({ $0.cityId })
                    self.cityCode = city[index]
                    
                    self.txtCity.text = self.dropDown.dataSource[index]
                }
            }
            dropDown.show()
        }
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtFirstName{
            txtLastName.becomeFirstResponder()
        }
        if textField == txtLastName{
            txtState.resignFirstResponder()
        }
        if textField == txtState {
            txtCity.resignFirstResponder()
            self.contentView.endEditing
        }
        if textField == txtCity {
            txtStreetName.resignFirstResponder()
            self.contentView.endEditing
        }
        if textField == txtStreetName {
            txtZipCode.becomeFirstResponder()
            self.contentView.endEditing
        }
        if textField == txtZipCode {
            txtRoutingNumber.becomeFirstResponder()
        }
        if textField == txtRoutingNumber {
            txtAccNumber.becomeFirstResponder()
        }
        else {
            textField.resignFirstResponder()
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        userBankInput["firstname"] = txtFirstName.text ?? ""
        userBankInput["lastname"] = txtLastName.text ?? ""
        userBankInput["statename"] = txtState.text ?? ""
        userBankInput["cityname"] = txtCity.text ?? ""
        userBankInput["street"] = txtStreetName.text ?? ""
        userBankInput["zip"] = txtZipCode.text ?? ""
        userBankInput["routing"] = txtRoutingNumber.text ?? ""
        userBankInput["acc"] = txtAccNumber.text ?? ""
        
        self.delegate?.userInputBankData(userBankInput)
    }
    //MARK:- API Get State list
    func callGetStateAPI(){
        
        KVNProgress.show()
        
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kgetStatesList, inputParams: nil, success: { (dict) in
            
            
            if dict?["Result"] as? Bool == true {
                let data = dict?["Data"] as? [String:Any]
                
       
                let state = data?["States"] as! [[String : Any]]
                if !state.isEmpty
                {
                    self.arr_StateList = state.map(StateList.init)
                }
                KVNProgress.dismiss()
                   //Call Cities API
            }
        }) { error in
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
        }
    }
    
    func callGetCityAPI(index: Int){
        
        KVNProgress.show()
        let dict = [ "stateId" : arr_StateList[index].stateId ]
       
        APIManager.callAPI(url: Constant.serverAPI.URL.kgetCitiesList, inputParams: dict, success: { (dict) in
            
            if dict?["Result"] as? Bool == true {
                let data = dict?["Data"] as? [String:Any]
                
                let cities = data?["Cities"] as! [[String : Any]]
                if cities.count != 0
                {
                    self.arr_CitiesList = cities.map(CitiesList.init)
                }
                
                KVNProgress.dismiss()
                
                
            }
        }) { error in
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
        }
    }
    
    //MARK:- TextField Validation
    func isValidServiceData(firstName: String?, lastName: String?, streetName:String?,city:String?,state: String?,zipcode: String?, routingNumber: String?, accountNumber: String?)  -> Bool {
        
        let firstName = firstName?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let lastName = lastName?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let streetName = streetName?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let city = city?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let state = state?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let zipcode = zipcode?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let routingNumber = routingNumber?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let accountNumber = accountNumber?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        
        var message = ""
        
        if firstName == ""{
            
            message =  Constant.ValidationMessage.kfirstName
            
        }
        else if lastName == ""{
            
            message =  Constant.ValidationMessage.kLastName
            
        }else if state == ""{
            
            message =  Constant.ValidationMessage.kState
            
        }else if city == ""{
            
            message =  Constant.ValidationMessage.kCity
            
        }
        else if streetName == "" {
            
            message = Constant.ValidationMessage.kStreet
        }
        
        else if zipcode == "" {
            
            message =  Constant.ValidationMessage.kZipCode
            
        }
        else if zipcode?.characters.count != 5 {
            
            message =  Constant.ValidationMessage.kZipValidation
        }
        else if routingNumber == ""{
            
            message = Constant.ValidationMessage.kRoutingNumber
          
            
        }else if accountNumber == "" {

            message = Constant.ValidationMessage.kAccountNumber
            
        }
        else {
            
            return true
        }
        
        Utilities.showAlertView(title: "Alert", message: message)
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtZipCode {
        let maxLength = 5
        let currentString: NSString = txtZipCode.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
        }
        return true
    }
    
}



