//
//  PaypalPaymentCell.swift
//  The EZ List
//
//  Created by DIFFERENZ101 on 12/1/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class PaypalPaymentCell: UITableViewCell {

    @IBOutlet weak var txtPaypalEmail: UITextField!    
    @IBOutlet weak var lblNote: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        
        let titleAttributes = [NSForegroundColorAttributeName: UIColor.red, NSFontAttributeName: UIFont(name: "SourceSansPro-Bold", size: 10.0)!]
        let megAttributes = [NSForegroundColorAttributeName: UIColor.gray, NSFontAttributeName: UIFont(name: "SourceSansPro-Regular", size: 10.0)!]
        
        let partOne = NSMutableAttributedString(string: "NOTE: ", attributes: titleAttributes)
        let partTwo = NSMutableAttributedString(string: "Please provide a valid PayPal address for receiving payments from your Buyers", attributes: megAttributes)
        
        let combination = NSMutableAttributedString()
        
        combination.append(partOne)
        combination.append(partTwo)
        
        self.lblNote.attributedText = combination
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func isValidServiceData(email: String?) -> Bool {
    
        let email = email?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        var message = ""
        
        if email == "" {
            message = Constant.ValidationMessage.kPaypalEmail
        }
        else if !Utilities.isValidEmail(email!){
    
            message =  Constant.ValidationMessage.kValidPaypalEmail
        }else {
            
            return true
        }
        
        Utilities.showAlertView(title: "Alert", message: message)
        return false
        
    }
}
