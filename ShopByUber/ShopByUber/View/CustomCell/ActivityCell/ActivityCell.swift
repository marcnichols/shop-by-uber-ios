//
//  ActivityCell.swift
//  ShopByUber
//
//  Created by Administrator on 6/6/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class ActivityCell: UITableViewCell {

    @IBOutlet var vwBG: UIView!
    @IBOutlet var imgService: UIImageView!
    @IBOutlet var lblService: UILabel!
    @IBOutlet var lblHeading: UILabel!
    @IBOutlet var lblStatus: UILabel!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var consBtnLikeHeight: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.consBtnLikeHeight.constant = 0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
