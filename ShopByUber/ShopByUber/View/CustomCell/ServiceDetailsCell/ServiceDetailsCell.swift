//
//  ServiceDetailsCell.swift
//  ShopByUber
//
//  Created by Differenz215 on 6/23/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit
protocol ServiceDetailsCellDelegate: class {
    
    func ServiceDetailCellDelegate(cell:ServiceDetailsCell,buttonType:UIButton)
    func ServiceDetailCellWithDataDelegate(cell:ServiceDetailsCell,buttonType:UIButton,arrTimeSlot: TimeSlot)
    func ServiceDetailCellBookNowDelegate(cell:ServiceDetailsCell,buttonType:UIButton)
    func segmentForLocationValueChange(cell:ServiceDetailsCell, segment: UISegmentedControl)
}
class ServiceDetailsCell: UITableViewCell,UITableViewDelegate,UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var vw_ServiceHeader: UIView!
    @IBOutlet weak var tbl_TimeSlot: UITableView!
    @IBOutlet weak var btnSendBookRequest: UIButton!
    @IBOutlet weak var lblServiceTitle: UILabel!
    @IBOutlet weak var imgServiceHead: UIImageView!
    @IBOutlet weak var lblServiceDesc: UILabel!
    @IBOutlet weak var lblPriceService: UILabel!
    @IBOutlet weak var btnServiceFavourite: UIButton!
    @IBOutlet weak var btnInfo: UIButton!
    
    @IBOutlet weak var constraintBtnBookNow: NSLayoutConstraint!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblVendorLocation: UILabel!
    
    @IBOutlet weak var segmentLocation: UISegmentedControl!
    @IBOutlet weak var lblAvailibility: UILabel!
    
    @IBOutlet weak var btnBookNow: UIButton!
    @IBOutlet weak var constraintTblSlotHeight: NSLayoutConstraint!
    @IBOutlet weak var lblOfferDetails: UILabel!
    @IBOutlet weak var vwOfferContent: UIView!
    @IBOutlet weak var nslcVendorLocation: NSLayoutConstraint!
    
    @IBOutlet weak var nslcSegmentTop: NSLayoutConstraint!
    @IBOutlet weak var nslcBtnRequestTop: NSLayoutConstraint!
    @IBOutlet weak var vwSeperatorLine: UIView!
    
    //MARK: - NSLayout Propery
    
    var selectedTimeSlot: TimeSlot?
    var arrTimeSlots = [TimeSlot]()
    weak var delegate: ServiceDetailsCellDelegate?
    var slotPickerView: UIPickerView = UIPickerView()
    var vw_picker: UIView?
    var selectedRow: Int?
    var TimeSlotPicked = String()
    var serviceAvailable = false

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        Utilities.decorateView(self.btnSendBookRequest.layer, cornerRadius: 0, borderWidth: 1, borderColor: UIColor.black)
        Utilities.applyShadowEffect(view: [self.vw_ServiceHeader])
        self.tbl_TimeSlot.layer.masksToBounds = false
        
        self.tbl_TimeSlot.register(UINib(nibName: "BookingDayTimeCell", bundle: nil), forCellReuseIdentifier: "BookingDayTimeCell")
        self.tbl_TimeSlot.register(UINib(nibName: "NoDayTimeAvailable", bundle: nil), forCellReuseIdentifier: "NoDayTimeAvailable")
        
        self.tbl_TimeSlot.delegate = self
        self.tbl_TimeSlot.dataSource = self

        let screenSize: CGRect = UIScreen.main.bounds

        // UIPicker Configuration
        self.slotPickerView.dataSource = self
        self.slotPickerView.delegate = self
        
        self.slotPickerView.backgroundColor = .white
        self.slotPickerView.frame = CGRect(x: 0, y: 40, width: screenSize.width, height: Utilities.isDeviceiPad() ? 260 : 160)
        
        //Topbar Configuration
        let toolBar = UIToolbar.init(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 40))
        toolBar.barStyle = .default
        
        let font:UIFont = UIFont.systemFont(ofSize: 15, weight: UIFontWeightSemibold)
        let attributes:[String : Any] = [NSFontAttributeName: font]
        let btnDone = UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(selectedTimeSlotIndex))
        
        btnDone.setTitleTextAttributes(attributes, for: UIControlState.normal)

        btnDone.tintColor = UIColor.black
        
        let btnCancel = UIBarButtonItem.init(title: "Cancel", style: .done, target: self, action: #selector(btnCancelSelectClicke))
        
        btnCancel.setTitleTextAttributes(attributes, for: UIControlState.normal);

        btnCancel.tintColor = UIColor.black
        
        let flex = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        toolBar.setItems([btnCancel, flex, btnDone], animated: false)
        
        //Full Pickerview Configuration
        vw_picker = UIView(frame: CGRect(x: 0, y: screenSize.height - (Utilities.isDeviceiPad() ? 300 : 200),width: screenSize.width, height: Utilities.isDeviceiPad() ? 300 : 200))
        self.vw_picker?.layer.addBorder(edge: .top, color: .lightGray, thickness: 0.5)
        self.vw_picker?.addSubview(toolBar)
        self.vw_picker?.addSubview(self.slotPickerView)
        self.vw_picker?.backgroundColor = UIColor.groupTableViewBackground
       
        self.tbl_TimeSlot.reloadData()
        
        let segAttributes: NSDictionary = [
            NSForegroundColorAttributeName: UIColor.init(red: 0/255, green: 254/255, blue: 103/255, alpha: 1.0),
            NSFontAttributeName: UIFont(name: "SourceSansPro-Regular", size: 14)!
        ]
        segmentLocation.setTitleTextAttributes(segAttributes as [NSObject : AnyObject], for: UIControlState.selected)
        
        self.layoutIfNeeded()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    
    func btnCancelSelectClicke(){
        UIView.animate(withDuration: 0.25, animations: {
            self.vw_picker?.removeFromSuperview()
            self.tbl_TimeSlot.reloadData()
        })
    }
    
    func selectedTimeSlotIndex(){
        selectedTimeSlot = arrTimeSlots[slotPickerView.selectedRow(inComponent: 0)]
        self.tbl_TimeSlot.reloadData()
        UIView.animate(withDuration: 0.25, animations: {
            self.vw_picker?.removeFromSuperview()
            self.tbl_TimeSlot.reloadData()
        })
    }
    //MARK:- TableView Methods
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if Utilities.isDeviceiPad()
        {
            return 100
        }
        else
        {
            return 80
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->
        UITableViewCell {
            
            if arrTimeSlots.isEmpty && self.serviceAvailable == false {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "NoDayTimeAvailable") as! NoDayTimeAvailable
                Utilities.applyShadowEffect(view: [cell.vwContainer])
                return cell
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "BookingDayTimeCell", for: indexPath) as! BookingDayTimeCell
                let data = selectedTimeSlot
                Utilities.applyShadowEffect(view: [cell.vw_content])
                cell.lblbookTime.text = data?.duration
                return cell
            }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.selectedTimeSlot != nil
        {
            UIView.animate(withDuration: 0.25, animations: {
                self.viewController?.view.addSubview(self.vw_picker!)
            })
        }
    }
    
    //MARK:- pickerView Methods
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrTimeSlots.count
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView
    {
        let pickerLabel = UILabel()
        pickerLabel.textColor = UIColor.black
        pickerLabel.text = arrTimeSlots[row].duration
        pickerLabel.font = Utilities.isDeviceiPad() ?UIFont.systemFontReguler(with: 19) :UIFont.systemFontReguler(with: 15) // In this use your custom font
        pickerLabel.textAlignment = NSTextAlignment.center
        TimeSlotPicked = pickerLabel.text!  //Getting Selected TimeSlot
        
        return pickerLabel
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat
    {
        return Utilities.isDeviceiPad() ? 50.0 :36.0
    }
    
    //MARK:- IBAction Methods
    
    @IBAction func btnSendBookRequest_Click(_ sender: Any) {
        if self.selectedTimeSlot != nil
        {
            //Changes made by Semein - Add TimeSlot Conformation Alert on 26/10/17
            if TimeSlotPicked == "" {
            selectedTimeSlot = arrTimeSlots[slotPickerView.selectedRow(inComponent: 0)]
            
            }
            
            let rootView = UIApplication.shared.delegate?.window??.rootViewController as? UINavigationController
            
            let topView = UIApplication.topViewController(base: rootView)
            let alert = UIAlertController(title: "Are you Sure?", message: "Would you like to request an appointment for \(selectedTimeSlot!.duration!)?", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction (title: "Yes", style: .default, handler: { (alert) in
                self.delegate?.ServiceDetailCellWithDataDelegate(cell: self, buttonType: self.btnSendBookRequest, arrTimeSlot: self.selectedTimeSlot!)
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
            
            
            topView?.present(alert, animated: true, completion: nil)
            
        }
        else
        {
            Utilities.showAlertView(title: "", message: Constant.Message.kNoTimeSlotAvailabel)
        }
    }
    
    @IBAction func btnInfo_Click(_ sender: Any) {
        delegate?.ServiceDetailCellDelegate(cell: self, buttonType: self.btnInfo)
    }
    
    @IBAction func btnAddFavourite_Click(_ sender: Any) {
        delegate?.ServiceDetailCellDelegate(cell: self, buttonType: self.btnServiceFavourite)
        
    }
    
    @IBAction func btnBookNow_clicked(_ sender: Any) {
   
        self.delegate?.ServiceDetailCellBookNowDelegate(cell: self, buttonType: self.btnBookNow)
    }
    
    @IBAction func segmentLocationChange(_ sender: Any) {
        delegate?.segmentForLocationValueChange(cell: self, segment: self.segmentLocation)
    }
    
}
