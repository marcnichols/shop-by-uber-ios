//
//  SellerCancelRequestCell.swift
//  ShopByUber
//
//  Created by Differenz215 on 7/14/17.
//  Copyright © 2017 Administrator. All rights reserved.
//


import UIKit

class SellerCancelRequestCell: UITableViewCell,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var vw_CompanyDetails: UIView!
    @IBOutlet weak var vw_Serviceprovider: UIView!
    @IBOutlet weak var tbl_TimeSlot: UITableView!
    @IBOutlet weak var imgHeader: UIImageView!
    
    @IBOutlet weak var imgPlaceHolder: UIImageView!
    @IBOutlet weak var lblHeaderTitle: UILabel!
    
    @IBOutlet weak var lblFee: UILabel!
    @IBOutlet weak var lblAppointmentTime: UILabel!
    @IBOutlet weak var lblProfileName: UILabel!
    @IBOutlet weak var imgProfileImage: UIImageView!
    @IBOutlet weak var lblProfileDetails: UILabel!
    @IBOutlet weak var lblExperience: UILabel!
    
    var arrTimeSlotData = [Requests]()
    weak var delegate: AppointmentDetailsCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.tbl_TimeSlot.register(UINib(nibName: "BookingDayTimeCell", bundle: nil), forCellReuseIdentifier: "BookingDayTimeCell")
        Utilities.applyShadowEffect(view: [self.vw_CompanyDetails])
        Utilities.decorateView(imgProfileImage.layer, cornerRadius: self.imgProfileImage.frame.height/2, borderWidth: 5, borderColor: Utilities.colorWithHexString("D3D3D3"))
        self.tbl_TimeSlot.delegate = self
        self.tbl_TimeSlot.dataSource = self
    
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    //MARK:- TableView Methods
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return Constant.DeviceType.IS_IPAD || Constant.DeviceType.IS_IPAD_PRO ? 100 : 80
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrTimeSlotData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->
        UITableViewCell {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "BookingDayTimeCell", for: indexPath) as! BookingDayTimeCell
            let data = self.arrTimeSlotData[indexPath.row]
            Utilities.applyShadowEffect(view: [cell.vw_content])
            cell.lblbookTime.text       = data.duration
            cell.img_Dropdown.isHidden  = true
            cell.lblBookDayTitle.text   = "YOUR APPOINTMENT"
            return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    
    
    
}
