//
//  AppointmentDetailsCell.swift
//  ShopByUber
//
//  Created by Differenz215 on 6/27/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

protocol PaymentServiceCellDelegate: class {
    
    func PaymentServiceRemovetimeService(cell: PaymentServiceCell, buttonType: UIButton)
    func PaymentServiceAddressTap( cell: PaymentServiceCell)
}
class PaymentServiceCell: UITableViewCell {

    @IBOutlet weak var vw_CompanyDetails: UIView!
    @IBOutlet weak var vw_Serviceprovider: UIView!
    @IBOutlet weak var vw_Desc: UIView!

    @IBOutlet weak var imgHeader: UIImageView!
    @IBOutlet weak var imgProfileImage: UIImageView!

    @IBOutlet weak var lblHeaderTitle: UILabel!
    @IBOutlet weak var lblAppointmentTime: UILabel!
    @IBOutlet weak var lblProfileName: UILabel!
    @IBOutlet weak var lblFee: UILabel!
    @IBOutlet weak var lblProfileDetails: UILabel!
    @IBOutlet weak var lblExperience: UILabel!
    @IBOutlet weak var lblServiceDescription: UILabel!
    
    @IBOutlet weak var btnRemoveService: UIButton!
    @IBOutlet weak var lblAddress: UILabel!
    

    var arrTimeSlotData = [Requests]()
    weak var delegate: PaymentServiceCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        Utilities.applyShadowEffect(view: [self.vw_CompanyDetails,self.vw_Serviceprovider, self.vw_Desc])
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btnRemoveService_Click(_ sender: Any) {
        delegate?.PaymentServiceRemovetimeService(cell: self, buttonType: btnRemoveService)
    }
    
    @IBAction func btnAddressClicked(_ sender: Any) {
        delegate?.PaymentServiceAddressTap(cell: self)
    }

    
}
