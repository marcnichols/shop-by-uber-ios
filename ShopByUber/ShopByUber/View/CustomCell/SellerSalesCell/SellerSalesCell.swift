//
//  RecentSellerRequestCell.swift
//  ShopByUber
//
//  Created by Differenz215 on 7/6/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class SellerSalesCell: UITableViewCell {
    
    @IBOutlet weak var imgService: UIImageView!

    @IBOutlet weak var lblPrice: UILabel!
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblServiceTitle: UILabel!
    @IBOutlet weak var vw_Contentcell: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        Utilities.applyShadowEffect(view: [self.vw_Contentcell])

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
