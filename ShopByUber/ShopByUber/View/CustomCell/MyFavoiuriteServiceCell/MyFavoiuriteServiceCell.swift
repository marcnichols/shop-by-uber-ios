//
//  MyFavoiuriteServiceCell.swift
//  ShopByUber
//
//  Created by Differenz215 on 7/7/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

protocol MyFavoiuriteServiceCellDelegate: class {
    
    func MyFavoiuriteServiceCellDelegate(cell:MyFavoiuriteServiceCell,buttonType:UIButton)
    
}

class MyFavoiuriteServiceCell: UITableViewCell {

    @IBOutlet weak var imgService: UIImageView!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblServiceTitle: UILabel!
    @IBOutlet weak var btnFavourite: UIButton!
    @IBOutlet weak var vw_Contentcell: UIView!
    
    weak var delegate: MyFavoiuriteServiceCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        Utilities.applyShadowEffect(view: [self.vw_Contentcell])
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func btnAddFavourite_Click(_ sender: Any) {
        delegate?.MyFavoiuriteServiceCellDelegate(cell: self, buttonType: self.btnFavourite)
        
    }
}
