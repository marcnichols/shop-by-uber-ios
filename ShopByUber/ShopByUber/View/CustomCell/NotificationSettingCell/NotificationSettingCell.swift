//
//  NotificationSettingCell.swift
//  ShopByUber
//
//  Created by Chirag Kalsariya on 7/12/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class NotificationSettingCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var swOnOff: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
//        if !Utilities.isDeviceiPad()
//        {
            swOnOff.transform = CGAffineTransform.init(scaleX: 0.75, y: 0.75)
            swOnOff.layer.cornerRadius = 16.0
 //       }

        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
