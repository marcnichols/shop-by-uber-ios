//
//  CategoryCollCell.swift
//  The EZ List
//
//  Created by DIFFERENZ101 on 2/2/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

import UIKit

class CategoryCollCell: UICollectionViewCell {

    @IBOutlet weak var vwContainer: UIView!
    @IBOutlet weak var imgCatagory: UIImageView!
    @IBOutlet weak var lblCatName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        vwContainer.layer.borderWidth = 0.5
        vwContainer.layer.borderColor = UIColor.black.cgColor
        
    }

}
