//
//  CountryPopupCell.swift
//  ShopByUber
//
//  Created by Differenz215 on 7/27/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class CountryPopupCell: UITableViewCell {

    @IBOutlet weak var lblCountyCode: UILabel!
    @IBOutlet weak var lblCountryName: UILabel!
    @IBOutlet weak var imgCountryFlag: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
