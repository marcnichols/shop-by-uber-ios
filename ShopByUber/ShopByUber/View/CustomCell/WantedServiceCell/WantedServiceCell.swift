//
//  WantedServiceCell.swift
//  The EZ List
//
//  Created by mac on 4/13/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

import UIKit

//protocol TableViewCellDelegate : class {
//    func cellTapPopup(_ sender: WantedServiceCell)
//}

class WantedServiceCell: UITableViewCell {

    
    @IBOutlet var btnDescriptionPopup: UIButton!
    @IBOutlet var imgServiceProfile: UIImageView!
    @IBOutlet var lblServiceName: UILabel!
    @IBOutlet var lblCategory: UILabel!
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var vwBG: UIView!
    @IBOutlet weak var nslcImageWidth: NSLayoutConstraint!
    
    @IBOutlet weak var lblServiceAvaliable: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func ShowDescriptionPopup_click(_ sender: UIButton) {
//        delegate?.cellTapPopup(self)
    }
    
}
