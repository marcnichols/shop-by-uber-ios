//
//  NotificationCell.swift
//  ShopByUber
//
//  Created by Differenz215 on 7/3/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {

    @IBOutlet weak var vw_Content: UIView!
    @IBOutlet weak var imgService: UIImageView!
    @IBOutlet weak var lblServiceDetails: UILabel!
    @IBOutlet weak var lblVendortrack: UILabel!
    @IBOutlet weak var lblServiceDesc: UILabel!
    @IBOutlet weak var nslclblServiceDiscHight: NSLayoutConstraint!
    @IBOutlet weak var nslclblServiceTitleTop: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        Utilities.applyShadowEffect(view: [self.vw_Content])
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
