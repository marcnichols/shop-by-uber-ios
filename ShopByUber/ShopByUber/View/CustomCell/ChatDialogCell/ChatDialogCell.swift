//
//  MyFriendCell.swift
//  AwakenedMind
//
//  Created by mac on 7/13/17.
//  Copyright © 2017 Differenz System Pvt. Ltd. All rights reserved.
//

import UIKit

class ChatDialogCell: UITableViewCell {
    
    //MARK: - Outlets
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblUnread: UILabel!
    @IBOutlet var imgUserProfile: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        //Image radius
        let notificationRadius =  CGFloat(Constant.DeviceType.IS_IPAD || Constant.DeviceType.IS_IPAD_PRO ? 30 : 15)
        Utilities.decorateView(self.lblUnread.layer, cornerRadius: notificationRadius, borderWidth: 0.0, borderColor: UIColor.clear)
        
        let imageRadius =  CGFloat(Constant.DeviceType.IS_IPAD || Constant.DeviceType.IS_IPAD_PRO ? 25 : 17.5)
        Utilities.decorateView(self.imgUserProfile.layer, cornerRadius: imageRadius, borderWidth: 0.0, borderColor: UIColor.clear)
        self.imgUserProfile.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
