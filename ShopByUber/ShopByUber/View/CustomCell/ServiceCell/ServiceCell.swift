//
//  ServiceCell.swift
//  ShopByUber
//
//  Created by Administrator on 6/5/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class ServiceCell: UITableViewCell {

    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var serviceImage: UIImageView!
    @IBOutlet var vwBG: UIView!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var consBtnLikeHeight: NSLayoutConstraint!
    @IBOutlet weak var lblCatagoryType: UILabel!
    @IBOutlet weak var nslcLblCatagoryTypeHight: NSLayoutConstraint!
    @IBOutlet weak var nslclblDescHight: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.consBtnLikeHeight.constant = 0
      //  Utilities.applyShadowEffect(view: [vwBG])
        self.backgroundColor = .clear
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
