//
//  MassageTherapyCell.swift
//  ShopByUber
//
//  Created by Administrator on 6/6/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

protocol MassageTherapyCellDelegate: class {
    
    func MassageTherapyCellDelegate(cell:MassageTherapyCell,buttonType:UIButton)
    
}
class MassageTherapyCell: UITableViewCell {

    @IBOutlet var vwBG: UIView!
    @IBOutlet var imgMassage: UIImageView!
    @IBOutlet var lblMassage: UILabel!
    @IBOutlet var lblHeading: UILabel!
    @IBOutlet var lblStatus: UILabel!
    @IBOutlet weak var btnFavourite: UIButton!
    weak var delegate: MassageTherapyCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        Utilities.applyShadowEffect(view: [self.vwBG])

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btnAddFavourite_Click(_ sender: Any) {
        delegate?.MassageTherapyCellDelegate(cell: self, buttonType: self.btnFavourite)
        
    }
    
}
