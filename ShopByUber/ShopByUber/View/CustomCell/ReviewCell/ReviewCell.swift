//
//  ReviewCell.swift
//  ShopByUber
//
//  Created by Differenz215 on 6/22/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class ReviewCell: UITableViewCell {

    @IBOutlet weak var lblNameReviewBy: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblReview: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
