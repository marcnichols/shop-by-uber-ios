//
//  SettingsMenuCell.swift
//  ShopByUber
//
//  Created by Differenz215 on 6/27/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class SettingsMenuCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCurrency: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code


    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
