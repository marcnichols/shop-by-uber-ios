//
//  APIManager.swift
//
//  Created by Administrator on 4/4/17.
//  Copyright © 2017 Differenz System Pvt. Ltd. All rights reserved.
//

import UIKit
import Foundation
import SystemConfiguration
import Alamofire

class APIManager: NSObject {
    
    //MARK: Check for internet connection
    class func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
    }
    
    //MARK: - Post method with content type application/json
    class func callURLStringJson(_ URLString: String, withRequest requestDictionary: [String: Any]?, withSuccess success: @escaping (_ responseDictionary: AnyObject) -> Void, failure: @escaping (_ error: String) -> Void, connectionFailed: @escaping (_ error: String) -> Void) {
        
        if(Utilities.checkInternetConnection())
        {
            
            let url = URL(string:URLString)!
            print("url",url)
            
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: requestDictionary ?? [:], options: JSONSerialization.WritingOptions.prettyPrinted)
                let jsonString: String = String(data: jsonData, encoding: String.Encoding.utf8)!
                print(jsonString)
                // here "jsonData" is the dictionary encoded in JSON data
            } catch let error as NSError {
                print(error)
            }
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            manager.request(url, method: .post, parameters: requestDictionary ?? [:], encoding: JSONEncoding.default).responseJSON(completionHandler: { (response) in
                switch response.result {
                    
                case .success:
                    KVNProgress.dismiss()
                    let jsonString: String = String(data: response.data!, encoding: String.Encoding.utf8)!
                    print(jsonString)
                    let dict : [String : Any ] = response.result.value! as! [String : Any]
                    let msg = dict["Message"] as? String
                    if msg == "Session id is not valid." {
                        
                        print("Call logout")
                        
                        Utilities.showAlertWithAction(title: "", message: "Session id is not valid.", buttonTitle: "OK", buttonTitle2: "", onOKClick: {
                           
                            appDeletgate.onLogout()
                            
                        }, onCancelClick: {
                            
                        })
                    
                    }else{
                        success(response.result.value! as AnyObject)
                    }
                    
                    
                case .failure(let error):
                    KVNProgress.dismiss()
                    print("Request failed with error: \(error.localizedDescription)")
                    failure(Constant.serverAPI.errorMessages.kCommanErrorMessage)
                }
            })
            
        }
            
        else
        {
            connectionFailed(Constant.serverAPI.errorMessages.kNoInternetConnectionMessage)
        }
    }
    
    
    class func callAPI(url: String, inputParams: [String : Any]?,success: @escaping ([String:Any]?) -> (), failure: @escaping (_ error:String) -> ()) {
        
        callURLStringJson(url, withRequest: inputParams, withSuccess: { (response) in
            
            success(response as? [String:Any])
            
        }, failure: { (error) in
            
            failure(error)
            
        }) { (connectionFailed) in
            
            failure(connectionFailed)
        }
    }
    
    //MARK: - Upload Images
    static func uploadImage(_ URLString: String,imageType:Int,key:String, withRequest imgData: Data, withSuccess success: @escaping (_ responseDictionary: [String:Any]?) -> Void, failure: @escaping (_ err:String?) -> Void) {
        
        if(Utilities.checkInternetConnection())
        {
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            manager.upload(multipartFormData: { (multipartFormData) in
                
                multipartFormData.append(imgData, withName: key, fileName: "image.jpg", mimeType: "image/jpg")
                multipartFormData.append("\(imageType)".data(using: String.Encoding.utf8)!, withName: "imageType")
                
            }, to:URLString)
            { (result) in
                switch result {
                case .success(let upload,_,_ ):
                    
                    upload.uploadProgress(closure: { (progress) in
                        //Print progress
                        print(progress)
                    })
                    
                    upload.responseJSON { response in
                        
                        
                        let dict : [String : Any ] = response.result.value! as! [String : Any]
                        let msg = dict["Message"] as? String
                        if msg == "Session id is not valid." {
                            
                            print("Call logout")
                            Utilities.showAlertWithAction(title: "", message: "Session id is not valid.", buttonTitle: "OK", buttonTitle2: "", onOKClick: {
                                
                                KVNProgress.dismiss()
                                appDeletgate.onLogout()
                                
                            }, onCancelClick: {
                                
                            })
                            
                        }else{
                           
                            success(response.result.value as? [String:Any])
                        }
                        
                    }
                    
                case .failure(_):
                    
                    KVNProgress.dismiss()
                    failure(Constant.serverAPI.errorMessages.kCommanErrorMessage)
                    
                }
            }
        }
            
        else
        {
            failure(Constant.serverAPI.errorMessages.kNoInternetConnectionMessage)
        }
    }
}
