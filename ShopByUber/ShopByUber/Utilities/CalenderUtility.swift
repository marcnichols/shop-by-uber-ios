//
//  CalenderUtility.swift
//  ShopByUber
//
//  Created by Administrator on 5/26/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class CalenderUtility: NSObject {
    
    static func getHeaderTextFromDate(date: Date) -> String{
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM yyyy"
        let dt = date
        let headerDT = formatter.string(from: dt).uppercased()
        return headerDT
    }
    
    
    static func getHourMinSecond (totalSeconds : TimeInterval) -> String {
        
        let hours:Int = Int(totalSeconds.truncatingRemainder(dividingBy: 86400) / 3600)
        let minutes:Int = Int(totalSeconds.truncatingRemainder(dividingBy: 3600) / 60)
        //let seconds:Int = Int(totalSeconds.truncatingRemainder(dividingBy: 60))
        
        if hours > 0 {
            if hours == 1 {
                return String(format: "%i hr %i Min", hours, minutes)
            }else{
                return String(format: "%i hrs %i Mins", hours, minutes)
            }
            
        } else {
            
            if minutes == 1 {
                
                return String(format: "%i Min", minutes)
                
            }else{
                
                return String(format: "%i Mins", minutes)
            }
            
        }

    }
    
    //MARK:- Date to String Method
    
    static func DateToString(date:Date) -> String
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        let dateString : String = formatter.string(from: date)
        return dateString
    }

    
    
    static func getNumberOfSpecificDaysInMonth (weekDay:Int,month : Int , Year : Int) -> [Date]{
        
        var arrDates = [Date]()
        let dateComponents = NSDateComponents()
        dateComponents.year = Year
        dateComponents.month = month
        
        
        let calendar = NSCalendar.current
        let date = calendar.date(from: dateComponents as DateComponents)
        
        //let range = calendar.rangeOfUnit(.Day, inUnit: .Month, forDate: date)
        
        let range = calendar.range(of: .day, in: Calendar.Component.month, for: date!)
        
        let numDays = range!.count
        
        //let dateFormatter = DateFormatter()
        
        for day in 1...numDays {
            
            dateComponents.day = day
            
            let date = calendar.date(from: dateComponents as DateComponents)

            let day = calendar.component(.weekday, from: date!)
            
            
            if day == weekDay {
                
                arrDates.append(date!)
            }
        }
        
        return  arrDates
    }
    
    static func getWeekDayFromWeekName(weekDayName:String) -> Int{
        
        switch weekDayName {
        case "Sunday":
             return 1
        case "Monday":
            return 2
        case "Tuesday":
            return 3
        case "Wednesday":
            return 4
        case "Thursday":
            return 5
        case "Friday":
            return 6
        case "Saturday":
            return 7
        default:
            return 0
        }
    }
}
