//
//  ChatHandler.swift
//  The EZ List
//
//  Created by mac on 2/2/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

import UIKit
import Quickblox

let QBChatMsgLimit = 50

enum ChatSender {
    case kReceiver
    case kSender
}

protocol ChatServiceDelegate : class {
    func didFinishedWithFetchingUnreadCountForDialog(dialog : QBChatDialog,unreadCount : Int)
    func didAddNewDialogInList()
    func didReceiveSystemMessage(message: QBChatMessage)
}

protocol ChatServiceMessagesDelegate : class {
    func didReciveMessage(message : QBChatMessage)
    func didRoomReceiveMessage(message : QBChatMessage, forDialogId: String)
    func didMessageReadforID(messageID : String, dialogID : String, readId: UInt)
    func didMessageDelivered(messageID : String, dialogID : String, readId: UInt)
}

class ChatService: NSObject, QBChatDelegate {
    
    static let sharedInstance = ChatService()
    
    //MARK: - Variables
    weak var delegate: ChatServiceDelegate?
    weak var delegateMessage: ChatServiceMessagesDelegate?
    
    var arrayDialogs : NSMutableArray?
    var dictDialogs : NSMutableDictionary?
    
    var arrayUsersIDs : NSMutableArray?
    var arrayUserProfile : NSMutableArray?
    var dictUserProfile : NSMutableDictionary?
    
    var isDialogFetchedDone = false
    
    //Handler for chat connect and dialog
    var chatConnectHandler:((Bool)->())?
    var getDialogCompletionBlock:((Bool)->())?
    
    //MARK: - Private Methods
    
    func isChatConnected() -> Bool {
        return QBChat.instance.isConnected
    }
    
    func isDialogFetched() -> Bool {
        return self.isDialogFetchedDone
    }
    
    func onConnectUserToChat(user : QBUUser,completion: @escaping(_ error : Error?) -> ()) {
        
        QBChat.instance.addDelegate(self)
        QBChat.instance.connect(with: user) { (error) in
            completion(error)
            if error == nil {
                if self.chatConnectHandler != nil {
                    self.chatConnectHandler!(true)
                }
            } else {
                if self.chatConnectHandler != nil {
                    self.chatConnectHandler!(false)
                }
            }
        }
    }
    
    func onDisConnectUser (completion:@escaping (Error?) -> ()) {
        
        if self.arrayDialogs != nil {
            self.arrayDialogs?.removeAllObjects()
        }
        if self.dictDialogs != nil {
            self.dictDialogs?.removeAllObjects()
        }
        if self.arrayUsersIDs != nil {
            self.arrayUsersIDs?.removeAllObjects()
        }
        if self.arrayUserProfile != nil {
            self.arrayUserProfile?.removeAllObjects()
        }
        if self.dictUserProfile != nil {
            self.dictUserProfile?.removeAllObjects()
        }
        if self.isDialogFetchedDone {
            self.isDialogFetchedDone = false
        }
        if self.isChatConnected() == true && QuickBloxManager.sharedInstance.isUserLoggedIn {
            QBChat.instance.disconnect(completionBlock: { (_ error : Error?) in
                completion(error)
            })
        }
    }
    
    func onCreatePrivateDialog(userID: NSInteger, completion: @escaping (_ response: QBResponse,_ dialog: QBChatDialog?) -> ()) {
        
        if userID > 0 {
            let numberIDs : NSNumber = NSNumber(value: userID)
            let chatDialog: QBChatDialog = QBChatDialog(dialogID: nil, type: .private)
            chatDialog.occupantIDs = [numberIDs]
            
            QBRequest.createDialog(chatDialog, successBlock: { (response, chatDialog) in
                if response.status == .created || response.status == .OK {
                    
                    //Newly created dialog added to list
                    self.arrayDialogs?.add(chatDialog)
                    self.dictDialogs?.setObject(chatDialog, forKey: chatDialog.id! as NSCopying)
                    self.sendDialogCreationNotification(withDialog: chatDialog, occupantId: userID)
                    
                    completion(response, chatDialog)
                } else {
                    completion(response, nil)
                }
            }, errorBlock: { (response) in
                completion(response, nil)
            })
        }
    }
    
    func fetchOpenDialogDetailFrom(dialogId: String, completion: @escaping(QBChatDialog?) -> ()) {
        
        var objDialog: QBChatDialog?
        let respPage = QBResponsePage(limit: 1, skip: 0)
        QBRequest.dialogs(for: respPage, extendedRequest: ["_id": dialogId], successBlock: { (response, chatDialogs, no, page) in
            if chatDialogs.count > 0 {
                objDialog = chatDialogs.first
            }
            completion(objDialog)
        }) { (response) in
            print("Error while fetching group with id \(dialogId)")
            completion(objDialog)
        }
    }
    
    
    
    func onSetDialogs(dialogs: [QBChatDialog]) {
        
        if dialogs.count <= 0 {
            return
        }
        if self.arrayDialogs == nil {
            
            self.arrayDialogs = NSMutableArray()
            self.dictDialogs = NSMutableDictionary()
        }
        for (_, dialog) in dialogs.enumerated() {
            
            let dialogTemp = dialog
            var isFound = false
            
            for(_, valTemp) in (self.arrayDialogs?.enumerated())! {
                let dialogAdded = valTemp as! QBChatDialog
                if dialogTemp.id == dialogAdded.id {
                    isFound = true
                    break
                }
            }
            if isFound == false {
                self.arrayDialogs?.add(dialogTemp)
                self.dictDialogs?.setObject(dialogTemp, forKey: dialogTemp.id! as NSCopying)
            }
        }
    }
    
    //LOG: - Set all users
    
    func onSetUserIDs(chatIDs : Set<NSNumber>) {
        
        if chatIDs.count <= 0 {
            return
        }
        if arrayUsersIDs == nil {
            self.arrayUsersIDs = NSMutableArray()
        }
        for (_, value) in chatIDs.enumerated() {
            let userTemp = value.stringValue
            var isFound = false
            
            for(_, valTemp) in (self.arrayUsersIDs?.enumerated())! {
                let userAdded = valTemp as! String
                if userTemp == userAdded {
                    isFound = true
                    break
                }
            }
            if isFound == false {
                self.arrayUsersIDs?.add(userTemp)
            }
        }
    }
    
    //LOG: - Get all chat dialogs
    func onRequestDialogs(completion: @escaping(Bool) -> ()) {
        
        if !DataModel.sharedInstance.isUserLoggedIn() {
            completion(false)
            return
        }
        self.isDialogFetchedDone = false
        
        weak var weakSelf = self
        let dictExtendRequest = ["sort_desc" : "last_message_date_sent"]
        var t_rquest :((_ responsePage : QBResponsePage) -> ())? = { arg in }
        
        let request: ((_ responsePage : QBResponsePage) -> ())? = {(responsePage : QBResponsePage) -> Void in
            let strongSelf = weakSelf
            if strongSelf?.isChatConnected() == false {
                self.isDialogFetchedDone = true
                if self.getDialogCompletionBlock != nil {
                    self.getDialogCompletionBlock!(false)
                }
                completion(false)
                return
            }
            
            QBRequest.dialogs(for: responsePage, extendedRequest: dictExtendRequest, successBlock: { (response, dialogObjects, dialogsUsersIDs, page) in
                
                // set dialogs locally
                strongSelf?.onSetDialogs(dialogs: dialogObjects)
                
                var cancel = false
                page.skip += (dialogObjects.count);
                
                if (dialogObjects.count) > 0 {
                    strongSelf?.onSetUserIDs(chatIDs: dialogsUsersIDs)
                } else {
                    self.isDialogFetchedDone = true
                }
                
                if (page.totalEntries) <= UInt((page.skip)) {
                    self.isDialogFetchedDone = true
                    
                    QuickBloxManager.sharedInstance.onFetchUsersFromServer {

                        // handle unread message count
                        strongSelf?.onHandleUnreadMessageCountForAllDialogs()
                        
                        self.isDialogFetchedDone = true
                        if self.getDialogCompletionBlock != nil {
                            self.getDialogCompletionBlock!(true)
                        }
                    }
                    cancel = true
                }
                if cancel == false {
                    t_rquest!(page)
                } else {
                    t_rquest = nil
//                    self.isDialogFetchedDone = true
//                    if self.getDialogCompletionBlock != nil {
//                        self.getDialogCompletionBlock!(true)
//                    }
                    completion(true)
                }
            }) { (error) in
                t_rquest = nil
                self.isDialogFetchedDone = true
                if self.getDialogCompletionBlock != nil {
                    self.getDialogCompletionBlock!(false)
                }
                completion(false)
            }
        }
        
        t_rquest = request
        request!(QBResponsePage.init(limit: 100))
    }
    
    func onHandleUnreadMessageCountForAllDialogs() {
        
        if self.arrayDialogs != nil {
            for (_,value) in (self.arrayDialogs?.enumerated())! {
                let valueDialog = value as! QBChatDialog
                if valueDialog.unreadMessagesCount > 0 {
                    self.delegate?.didFinishedWithFetchingUnreadCountForDialog(dialog: valueDialog, unreadCount: Int(valueDialog.unreadMessagesCount))
                }
            }
        }
    }
    
    func setUnreadMessageCountForDialogID(_ dialogID: String) {
        
        if self.arrayDialogs != nil {
            for (_,value) in (self.arrayDialogs?.enumerated())! {
                let valueTemp = value as! QBChatDialog
                if (valueTemp.id == dialogID) {
                    valueTemp.unreadMessagesCount = valueTemp.unreadMessagesCount + 1
                    break
                }
            }
        }
    }
    
    func onFetchDialogWithID(dialogID : String) {
        
        let responsePage = QBResponsePage.init(limit: 100)
        QBRequest.dialogs(for: responsePage, extendedRequest: ["_id" : dialogID], successBlock: { (response, dialogObjects, dialogsUsersIDs, page) in

            if dialogObjects.count > 0 {

                self.onSetUserIDs(chatIDs: dialogsUsersIDs)
                self.onSetDialogs(dialogs: dialogObjects)

                QuickBloxManager.sharedInstance.onFetchUsersFromServer {
                    self.delegate?.didAddNewDialogInList()
                }
            }
        }) { (error) in
            print(error.description)
        }
    }
    
    //MARK: - Chat Messages Handler
    
    func onCreateMessage(messageText: String, dialogID: String, senderID: UInt) -> QBChatMessage {
        
        let userId = UInt(DataModel.sharedInstance.onFetchQuickBloxId())
        let userFullName = DataModel.sharedInstance.onFetchFirstName() + " " + DataModel.sharedInstance.onFetchLastName()
        /*if let profileImg = appInstance.activeUser?.ProfileImage, profileImg.characters.count > 0 {
            userProfile = profileImg
        }*/
        let numberIDs : NSNumber = NSNumber(value: senderID)

        let message = QBChatMessage.markable()
        message.text = messageText
        message.dialogID = dialogID
        message.dateSent = NSDate() as Date
        message.createdAt = NSDate() as Date
        message.readIDs = [numberIDs]
        message.deliveredIDs = [numberIDs]
        message.senderID = senderID

        let custParams = NSMutableDictionary()
        custParams.setObject(userFullName, forKey: "username" as NSCopying)
        custParams.setObject(userId, forKey: "UserId" as NSCopying)
        //custParams.setObject(userProfile, forKey: "profile" as NSCopying)
        custParams.setObject(true, forKey: "save_to_history" as NSCopying)

        message.customParameters = custParams

        return message
    }
    
    func onSendMessage(message: QBChatMessage, dialog: QBChatDialog, completion:@escaping (_ error : Error?) -> ()) {
        
        dialog.lastMessageUserID = (QBSession.current.currentUser?.id)!
        dialog.lastMessageText = message.text
        dialog.lastMessageDate = message.dateSent

        dialog.send(message) { (error) in
            if let _ = self.arrayDialogs {
                for(index, value) in (self.arrayDialogs?.enumerated())! {
                    let valueTemp = value as! QBChatDialog
                    if valueTemp.id == dialog.id {
                        self.arrayDialogs?.replaceObject(at: index, with: dialog)
                        break
                    }
                }
            }
            completion(error)
        }
    }
    
    //MARK: - Send system message when new private dialog added
    
    func createChatNotificationForGroupChatUpdate(dialog: QBChatDialog) -> QBChatMessage {
        
        let inviteMessage = QBChatMessage()
        inviteMessage.text = "NewDialogAdded"
        
        let customParams = NSMutableDictionary()
        customParams["xmpp_room_jid"] = dialog.roomJID
        customParams["name"] = dialog.name
        customParams["_id"] = dialog.id
        customParams["type"] = dialog.type.rawValue
        
        let stringOccupants = dialog.occupantIDs?.map {
            String(describing: $0)
        }
        customParams["occupants_ids"] = stringOccupants!.joined(separator: ", ")
        customParams["notification_type"] = "2"
        inviteMessage.customParameters = customParams
        return inviteMessage
    }
    
    func sendDialogCreationNotification(withDialog objDialog: QBChatDialog, occupantId: NSInteger) {
        
        let inviteMessage = self.createChatNotificationForGroupChatUpdate(dialog: objDialog)
        let timestamp = Date().timeIntervalSince1970
        inviteMessage.customParameters["date_sent"] = timestamp
        inviteMessage.recipientID = UInt(occupantId)
        
        QBChat.instance.sendSystemMessage(inviteMessage) { (error) in
            if error != nil {
                print("Error while send system msg \(error.debugDescription)")
            }
        }
    }
    
    //MARK: - QBChatRequest Delegate Methods
    
    func chatDidConnect() {
        print("LOG: ChatService- chatDidConnect")
        //self.onRequestDialogs {(isSuccess)in }
    }
    
    func chatDidReconnect() {
        print("LOG: ChatService- chatDidReconnect")
        //self.onRequestDialogs {(isSuccess)in }
    }
    
    func chatDidFail(withStreamError error: Error) {
        print("LOG: ChatService- chatDidFail \(error.localizedDescription)")
        if KVNProgress.isVisible() {
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "Quickblox", message: "The server might be down, please try again later.")
        }
    }
    
    func chatDidNotConnectWithError(_ error: Error) {
        print("LOG: ChatService- chatDidNotConnectWithError \(error.localizedDescription)")
        if KVNProgress.isVisible() {
            KVNProgress.dismiss()
        }
    }
    
    func chatDidAccidentallyDisconnect() {
        print("LOG: ChatService- chatDidAccidentallyDisconnect")
        if KVNProgress.isVisible() {
            KVNProgress.dismiss()
        }
    }
    
    //MARK: - QBChatRequest-QBChatMessage Methods
    func chatDidReceive(_ message: QBChatMessage) {
        
        self.delegateMessage?.didReciveMessage(message: message)

        var isFound = false
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if let strDialogId = appDelegate.strDialogID, strDialogId != message.dialogID {
            isFound = true
        }
        if appDelegate.strDialogID == nil {
            isFound = true
        }
        var isDialogNotInList = false
        if let _ = self.arrayDialogs {
            for (_,value) in (self.arrayDialogs?.enumerated())! {
                let tempDialog = value as! QBChatDialog
                if tempDialog.id == message.dialogID {
                    isDialogNotInList = true
                    tempDialog.lastMessageText = message.text
                    tempDialog.lastMessageDate = message.dateSent
                }
            }
        }
        if isDialogNotInList == false {
            self.onFetchDialogWithID(dialogID: message.dialogID!)
        }
        if isFound == true {
            self.setUnreadMessageCountForDialogID(message.dialogID!)
            self.onHandleUnreadMessageCountForAllDialogs()
        }
    }
    
    func chatRoomDidReceive(_ message: QBChatMessage, fromDialogID dialogID: String) {
        self.delegateMessage?.didRoomReceiveMessage(message: message, forDialogId: dialogID)
    }
    
    func chatDidDeliverMessage(withID messageID: String, dialogID: String, toUserID userID: UInt) {
        self.delegateMessage?.didMessageDelivered(messageID: messageID, dialogID: dialogID, readId: userID)
    }
    
    func chatDidReadMessage(withID messageID: String, dialogID: String, readerID: UInt) {
        self.delegateMessage?.didMessageReadforID(messageID: messageID, dialogID: dialogID, readId: readerID)
    }
    
    func chatDidReceiveSystemMessage(_ message: QBChatMessage) {
        
        // Add new dialog to list
        if let dialogId = message.customParameters["_id"] as? String {
            self.fetchOpenDialogDetailFrom(dialogId: dialogId) { (chatDialog) in
                if chatDialog != nil {
                    self.arrayDialogs?.add(chatDialog!)
                    self.dictDialogs?.setObject(chatDialog!, forKey: chatDialog!.id! as NSCopying)
                }
                self.delegate?.didReceiveSystemMessage(message: message)
            }
        }
    }
}
