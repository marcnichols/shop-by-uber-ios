//
//  Utilities.swift
//
//  Created by Administrator on 4/4/17.
//  Copyright © 2017 Differenz System Pvt. Ltd. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class Utilities: NSObject {
    
    //MARK: Decorate view with border and corner radius
    class func decorateView(_ layer: CALayer, cornerRadius: CGFloat, borderWidth: CGFloat, borderColor: UIColor){
        
        layer.cornerRadius = cornerRadius
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor.cgColor
        
    }
    
    
    class func setCornerRadius(layer:CALayer){
        
        let path = UIBezierPath(roundedRect:layer.bounds,
                                byRoundingCorners:[.topLeft, .bottomLeft],
                                cornerRadii: CGSize(width: 5, height:  5))
        
        let maskLayer = CAShapeLayer()
        
        maskLayer.path = path.cgPath
        layer.mask = maskLayer
    }
    
    //MARK: Shadow Effect View
    
    class func applyShadowEffect(view: [UIView]){
        
        for vw in view{
            vw.layer.shadowColor = UIColor.lightGray.cgColor
            vw.layer.shadowOpacity = 0.6
            vw.layer.shadowOffset = CGSize.zero
            vw.layer.shadowRadius = 1.2
        }
        
    }
    
    
    //MARK: Get UIColor from hex color
    class func colorWithHexString (_ hex:String) -> UIColor
    {
        let whitespace = CharacterSet.whitespacesAndNewlines
        var cString = (hex as AnyObject).trimmingCharacters(in: whitespace).uppercased()
        if (cString.hasPrefix("#")) {
            let index: String.Index = cString.index(cString.startIndex, offsetBy: 1)
            cString = cString.substring(from: index)
        }
        
        if (cString.count != 6) {
            return UIColor.gray
        }
        let index: String.Index = cString.index(cString.startIndex, offsetBy: 2)
        let index1: String.Index = cString.index(cString.startIndex, offsetBy: 4)
        let rString = cString.substring(to: index)
        let gString = cString.substring(from: index).substring(to: index)
        let bString = cString.substring(from: index1).substring(to: index)
        
        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
        Scanner(string: rString).scanHexInt32(&r)
        Scanner(string: gString).scanHexInt32(&g)
        Scanner(string: bString).scanHexInt32(&b)
        
        return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(1))
    }
    
    // MARK: - textViewHeight
    class func estimatedHeight(from string: String, with font: UIFont, width: CGFloat) -> CGFloat {
        let strComment: String = string
        let textSize: CGSize = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        var height: CGFloat = 18
        var rect = CGRect.zero
        rect = strComment.boundingRect(with: textSize, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        
        height += rect.size.height
        return height
    }
    
    //MARK: Check internet connection
    static func checkInternetConnection() -> Bool
    {
        if(APIManager.isConnectedToNetwork())
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    
    
    // MARK: - textViewHeight
    static func heigth(from string: String, with font: UIFont, width: CGFloat) -> CGFloat {
        let strComment: String = string
        let textSize: CGSize = CGSize(width: width - 50.0, height: CGFloat.greatestFiniteMagnitude)
        var height: CGFloat = 5.0
        var rect = CGRect.zero
        rect = strComment.boundingRect(with: textSize, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        
        height += rect.size.height
        return height
    }
    
    
    //MARK:- Validation
    
    //Validate email
    class func isValidEmail(_ strEmail:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: strEmail)
    }
    
    class func isValidPassword(pwd: String) -> Bool{
        
        let password = pwd.trimmingCharacters(in: CharacterSet.whitespaces)
        
        if(password.characters.count == 10){
            return true
        }else{
            return false
        }
        
    }
    
    //MARK:- Check Empty Value
    class func checkEmptyValue(value:AnyObject) -> AnyObject {
        
        if value is NSNull
        {
            return "" as AnyObject
        }
        else if value is NSNumber
        {
            return String(format:"%d", Int(value as! NSNumber)) as AnyObject
        }
        return value
    }
    
    class func validateTextFieldString(object:String?,validationMessage:String?)->Bool
    {
        let str = object?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if (str?.characters.count)! > 0
        {
            return true
        }
        Utilities.showAlertView(title: "Alert", message: validationMessage)
        return false
    }
    
    class func validateEmail(email:String?,validationMessage:String)->Bool
    {
        if validateTextFieldString(object: email, validationMessage: validationMessage)
        {
            if !isValidEmail(email!)
            {
                Utilities.showAlertView(title: "Alert", message: validationMessage)
                return false
            }
            return true
        }
        return false
    }
    
    class func validateOptionalEmail(email:String?,validationMessage:String)->Bool
    {
        let email = email?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if (email?.characters.count)! > 0
        {
            if !isValidEmail(email!)
            {
                Utilities.showAlertView(title: "Alert", message: validationMessage)
                return false
            }
        }
        return true
    }
    
    
    class func validateImage(image:UIImage?,validationMessage:String?)->Bool
    {
        if image == nil
        {
            Utilities.showAlertView(title: "Alert", message: validationMessage)
            return false
        }
        return true
    }
    
    class func validateURL(url:String?,validationMessage:String?)->Bool
    {
        let regEx = "((https|http)://)((\\w|-)+)(([.]|[/])((\\w|-)+))+"
        let predicate = NSPredicate(format:"SELF MATCHES %@", argumentArray:[regEx])
        let valid = predicate.evaluate(with: url)
        if !valid
        {
            Utilities.showAlertView(title: "Alert", message: validationMessage)
            return false
        }
        return true
    }
    
    class func validateURL(url:String?)->Bool
    {
        guard let urlString = url else {return false}
        guard let url = NSURL(string: urlString) else {return false}
        if !UIApplication.shared.canOpenURL(url as URL) {return false}
        
        return true
    }
    
    
    class func applyAttributedString(text:String,fontSize:CGFloat,range:NSRange) -> NSAttributedString{
        
        let myAttribute = [ NSFontAttributeName: UIFont(name: "SOURCESANSPRO-LIGHT", size: fontSize)!,NSForegroundColorAttributeName: UIColor.white ]
        let str = NSMutableAttributedString.init(string: text, attributes: myAttribute)
        str.addAttributes([ NSFontAttributeName: UIFont(name: "SOURCESANSPRO-SEMIBOLD", size: fontSize)!,NSForegroundColorAttributeName: UIColor.white ], range: range)
        
        return str
    }
    
    class func setupAttributedButton(range:NSRange, btn:UIButton){
        
        if Constant.DeviceType.IS_IPAD {
            btn.setAttributedTitle(Utilities.applyAttributedString(text: (btn.titleLabel?.text)!, fontSize: 22.0, range: range), for: .normal)
        }else{
            btn.setAttributedTitle(Utilities.applyAttributedString(text: (btn.titleLabel?.text)!, fontSize: 15.0, range: range), for: .normal)
        }
        
    }
    
    class func showAlertView(title: String?, message: String?) {
        
        let rootView = UIApplication.shared.delegate?.window??.rootViewController as? UINavigationController
        
        let topView = UIApplication.topViewController(base: rootView)
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        topView?.present(alert, animated: true, completion: nil)
    }
    
        
//    class func TimeSlotConformationAlert(title: String?, message: String?) {
//
//        let rootView = UIApplication.shared.delegate?.window??.rootViewController as? UINavigationController
//        
//        let topView = UIApplication.topViewController(base: rootView)
//        
//        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
//        alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (<#UIAlertAction#>) in
//            <#code#>
//        }))
//        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
//        
//        
//        topView?.present(alert, animated: true, completion: nil)
//        
//    }
    
    class func showAlertWithAction(title: String,message:String,buttonTitle:String,buttonTitle2:String,onOKClick: @escaping () -> (),onCancelClick: @escaping () -> () ){
        
        if checkAlertExist() == false {
            
            let rootView = UIApplication.shared.delegate?.window??.rootViewController as? UINavigationController
            
            let topView = UIApplication.topViewController(base: rootView)
            
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: buttonTitle, style: .default, handler: { action in
                onOKClick()
            }))
            
            if buttonTitle2.count != 0 {
                
                alert.addAction(UIAlertAction(title: buttonTitle2, style: .default, handler: { action in
                    onCancelClick()
                }))
                
            }
            topView?.present(alert, animated: true, completion: nil)
            
        }
        
    }
    
    class func checkAlertExist() -> Bool {
        for window: UIWindow in UIApplication.shared.windows {
            if (window.rootViewController?.presentedViewController is UIAlertController) {
                return true
            }
        }
        return false
    }
    
    
    //Unique Array
    class func removeDuplicates(array: [String]) -> [String] {
        var encountered = Set<String>()
        var result: [String] = []
        for value in array {
            if encountered.contains(value) {
                // Do not add a duplicate element.
            }
            else {
                // Add value to the set.
                encountered.insert(value)
                // ... Append the value.
                result.append(value)
            }
        }
        return result
    }
    
    //Decorate text
    class func decorateText(attributedString:NSMutableAttributedString,decorateString:String,decorate:[String:Any]) -> NSMutableAttributedString {
        
        let range = (attributedString.string as NSString).range(of: decorateString)
        attributedString.addAttributes(decorate, range: range)
        return attributedString
    }
    
    class func isDeviceiPad()->Bool
    {
        // Return DeviceType.IS_IPAD || DeviceType.IS_IPAD_PRO
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            return false
        case .pad:
            return true
        case .unspecified:
            return false
        default:
            return false
        }
    }
    
    class func checkMaximumTextLenght(textFiled:UITextField,text:String,textLength:Int)->Bool
    {
        if text == ""
        {
            return true
        }
        else if textFiled.text?.characters.count == textLength
        {
            return false
        }
        else
        {
            return true
        }
    }
    
    //MARK: - store and retrieve value from UserDefault
    
    class func setValueInNSUserDefaults(_ value:AnyObject?, forKey key:String) {
        UserDefaults.standard.set(value, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    class func retriveValueFromDefault(forKey:String)->String
    {
        return UserDefaults.standard.value(forKey: forKey) as? String ?? ""
    }
    
    class func setObjectToDefault(object:Any,forKey:String)
    {
        let data = NSKeyedArchiver.archivedData(withRootObject: object)
        UserDefaults.standard.set(data, forKey: forKey)
        UserDefaults.standard.synchronize()
    }
    
    class func retriveObjectFromDefault(forKey:String)->Any?
    {
        let data = UserDefaults.standard.object(forKey: forKey)
        if data != nil
        {
            let object = NSKeyedUnarchiver.unarchiveObject(with: data as! Data)
            return object
        }
        return nil
    }
    
    class func setBoolValue(value:Bool,forKey:String)
    {
        UserDefaults.standard.set(value, forKey: forKey)
        UserDefaults.standard.synchronize()
    }
    
    class func retriveBoolValue(forKey:String)->Bool {
        return UserDefaults.standard.value(forKey: forKey) as? Bool ?? false
    }
    
    class func resetUserDefault(forKey:String)
    {
        UserDefaults.standard.removeObject(forKey: forKey)
        UserDefaults.standard.synchronize()
    }
    
    //MARK:- Get Notification Setting ID
    class func getUserNotificationIDFrom(NotificationType:String)->Int
    {
        
        switch NotificationType {
        case Constant.NotificationType.PlaySoundOnNotification:
            return 1
        case Constant.NotificationType.VibrateOnNotification:
            return 2
        case Constant.NotificationType.PulseOnNotification:
            return 3
        case Constant.NotificationType.VendorIsOnRouteToYourLocation:
            return 4
        case Constant.NotificationType.VendorHasArrivedToYourLocation:
            return 5
        case Constant.NotificationType.ServiceHasBeenCompleted:
            return 6
        case Constant.NotificationType.ICanReviewMyRecentPurchases:
            return 7
        case Constant.NotificationType.NewAppFeaturesAreAvailable:
            return 8
        case Constant.NotificationType.ThereAreItemsLeftInMyCart:
            return 9
        case Constant.NotificationType.NewUpdatesInMyFavouriteService:
            return 10
        default:
            return 0
        }
        
    }
    
    class func getSellerNotificationIDFrom(NotificationType:String)->Int
    {
        
        switch NotificationType {
        case Constant.NotificationType.PlaySoundOnNotification:
            return 1
        case Constant.NotificationType.VibrateOnNotification:
            return 2
        case Constant.NotificationType.PulseOnNotification:
            return 3
        case Constant.NotificationType.UserHasMadeRequest:
            return 4
        case Constant.NotificationType.UserHasMadePaymentForConfirmedService:
            return 5
        case Constant.NotificationType.UserHasCanceledRequestForConfirmedService:
            return 6
        case Constant.NotificationType.NewAppFeaturesAreAvailable:
            return 7
        default:
            return 0
        }
        
    }
    
    //Country Code Get
    class func getCountryCallingCode(countryRegionCode:String)->String{
        
        let prefixCodes = ["AF": "93", "AE": "971", "AL": "355", "AN": "599", "AS":"1", "AD": "376", "AO": "244", "AI": "1", "AG":"1", "AR": "54","AM": "374", "AW": "297", "AU":"61", "AT": "43","AZ": "994", "BS": "1", "BH":"973", "BF": "226","BI": "257", "BD": "880", "BB": "1", "BY": "375", "BE":"32","BZ": "501", "BJ": "229", "BM": "1", "BT":"975", "BA": "387", "BW": "267", "BR": "55", "BG": "359", "BO": "591", "BL": "590", "BN": "673", "CC": "61", "CD":"243","CI": "225", "KH":"855", "CM": "237", "CA": "1", "CV": "238", "KY":"345", "CF":"236", "CH": "41", "CL": "56", "CN":"86","CX": "61", "CO": "57", "KM": "269", "CG":"242", "CK": "682", "CR": "506", "CU":"53", "CY":"537","CZ": "420", "DE": "49", "DK": "45", "DJ":"253", "DM": "1", "DO": "1", "DZ": "213", "EC": "593", "EG":"20", "ER": "291", "EE":"372","ES": "34", "ET": "251", "FM": "691", "FK": "500", "FO": "298", "FJ": "679", "FI":"358", "FR": "33", "GB":"44", "GF": "594", "GA":"241", "GS": "500", "GM":"220", "GE":"995","GH":"233", "GI": "350", "GQ": "240", "GR": "30", "GG": "44", "GL": "299", "GD":"1", "GP": "590", "GU": "1", "GT": "502", "GN":"224","GW": "245", "GY": "595", "HT": "509", "HR": "385", "HN":"504", "HU": "36", "HK": "852", "IR": "98", "IM": "44", "IL": "972", "IO":"246", "IS": "354", "IN": "91", "ID":"62", "IQ":"964", "IE": "353","IT":"39", "JM":"1", "JP": "81", "JO": "962", "JE":"44", "KP": "850", "KR": "82","KZ":"77", "KE": "254", "KI": "686", "KW": "965", "KG":"996","KN":"1", "LC": "1", "LV": "371", "LB": "961", "LK":"94", "LS": "266", "LR":"231", "LI": "423", "LT": "370", "LU": "352", "LA": "856", "LY":"218", "MO": "853", "MK": "389", "MG":"261", "MW": "265", "MY": "60","MV": "960", "ML":"223", "MT": "356", "MH": "692", "MQ": "596", "MR":"222", "MU": "230", "MX": "52","MC": "377", "MN": "976", "ME": "382", "MP": "1", "MS": "1", "MA":"212", "MM": "95", "MF": "590", "MD":"373", "MZ": "258", "NA":"264", "NR":"674", "NP":"977", "NL": "31","NC": "687", "NZ":"64", "NI": "505", "NE": "227", "NG": "234", "NU":"683", "NF": "672", "NO": "47","OM": "968", "PK": "92", "PM": "508", "PW": "680", "PF": "689", "PA": "507", "PG":"675", "PY": "595", "PE": "51", "PH": "63", "PL":"48", "PN": "872","PT": "351", "PR": "1","PS": "970", "QA": "974", "RO":"40", "RE":"262", "RS": "381", "RU": "7", "RW": "250", "SM": "378", "SA":"966", "SN": "221", "SC": "248", "SL":"232","SG": "65", "SK": "421", "SI": "386", "SB":"677", "SH": "290", "SD": "249", "SR": "597","SZ": "268", "SE":"46", "SV": "503", "ST": "239","SO": "252", "SJ": "47", "SY":"963", "TW": "886", "TZ": "255", "TL": "670", "TD": "235", "TJ": "992", "TH": "66", "TG":"228", "TK": "690", "TO": "676", "TT": "1", "TN":"216","TR": "90", "TM": "993", "TC": "1", "TV":"688", "UG": "256", "UA": "380", "US": "1", "UY": "598","UZ": "998", "VA":"379", "VE":"58", "VN": "84", "VG": "1", "VI": "1","VC":"1", "VU":"678", "WS": "685", "WF": "681", "YE": "967", "YT": "262","ZA": "27" , "ZM": "260", "ZW":"263"]
        
        let countryDialingCode = prefixCodes[countryRegionCode]
        
        return countryDialingCode!
    }
    
    class func onFetchMessageTime(date : Date) -> String {
        
        var strTime = ""
        let calendar = NSCalendar.autoupdatingCurrent
        let formatTime = DateFormatter()
        
        formatTime.dateFormat = "MMMM d, yyyy"
        formatTime.locale = NSLocale.init(localeIdentifier: "en_US_POSIX") as Locale!
        
        let outputTimeZone = NSTimeZone.system
        formatTime.timeZone = outputTimeZone
        strTime = formatTime.string(from:date)
        
//        if calendar.isDateInYesterday(date) {
//
//            let formatTime = DateFormatter()
//            formatTime.dateFormat = "h:mm a"
//            formatTime.locale = NSLocale.init(localeIdentifier: "en_US_POSIX") as Locale!
//            formatTime.timeZone = NSTimeZone.system
//            let time = formatTime.string(from:date)
//
//            strTime = "Yesterday " + time
//        } else if calendar.isDateInToday(date) {
//
//            let formatTime = DateFormatter()
//            formatTime.dateFormat = "h:mm a"
//            formatTime.locale = NSLocale.init(localeIdentifier: "en_US_POSIX") as Locale!
//
//            let outputTimeZone = NSTimeZone.system
//            formatTime.timeZone = outputTimeZone
//
//            strTime = formatTime.string(from:date)
//        } else {
//            let formatTime = DateFormatter()
//            formatTime.dateFormat = "MMMM d, yyyy h:mm a"
//            formatTime.locale = NSLocale.init(localeIdentifier: "en_US_POSIX") as Locale!
//
//            let outputTimeZone = NSTimeZone.system
//            formatTime.timeZone = outputTimeZone
//
//            strTime = formatTime.string(from:date)
//        }
        return strTime
    }
    
    class func getDuration(second:CGFloat) -> String
    {
        let hours = Int(second / 3600)
        let minutes = Int((second.truncatingRemainder(dividingBy: 3600)) / 60)
        let seconds = Int(second.truncatingRemainder(dividingBy: 60))
        
        var time = ""
        if hours > 0
        {
            time = NSString (format: "%02d:%02d:%02d",hours ,minutes,seconds) as String
        }
        else{
            time = NSString (format: "%02d:%02d",minutes,seconds) as String
        }
        return time
    }
}

//MARK: - UIViewController Extension
extension UIViewController
{
    func revealToggle(_ animated: Bool)
    {
        self.revealViewController().revealToggle(animated: animated)
    }
    
    func showAlertWithButtonAction(title: String,message:String,buttonTitle:String,buttonTitle2:String,onOKClick: @escaping () -> (),onCancelClick: @escaping () -> () ){
        
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: buttonTitle, style: .default, handler: { action in
                onOKClick()
            }))
            if buttonTitle2.count != 0 {
                alert.addAction(UIAlertAction(title: buttonTitle2, style: .default, handler: { action in
                    onCancelClick()
                }))
            }
            self.present(alert, animated: true, completion: nil)
    }
    
    func showAlertWithYESNOAction(title: String,message:String,buttonTitle:String,buttonTitle2:String,onNOClick: @escaping () -> (),onYESClick: @escaping () -> () ){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: buttonTitle, style: .default, handler: { action in
            onNOClick()
        }))
        if buttonTitle2.count != 0 {
            alert.addAction(UIAlertAction(title: buttonTitle2, style: .default, handler: { action in
                onYESClick()
            }))
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    func showSimpleAlert(title: String,message:String){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
 
}

//MARK: - UIView Extension

extension CALayer {
    func borderUIColor() -> UIColor? {
        return borderColor != nil ? UIColor(cgColor: borderColor!) : nil
    }
    
    func setBorderUIColor(color: UIColor) {
        borderColor = color.cgColor
    }
    
    func addBorder(edge: UIRectEdge, color: UIColor, thickness: CGFloat) {
        
        let border = CALayer()
        
        switch edge {
        case UIRectEdge.top:
            border.frame = CGRect.init(x: 0, y: 0, width: frame.width, height: thickness)
            break
        case UIRectEdge.bottom:
            border.frame = CGRect.init(x: 0, y: frame.height - thickness, width: frame.width, height: thickness)
            break
        case UIRectEdge.left:
            border.frame = CGRect.init(x: 0, y: 0, width: thickness, height: frame.height)
            break
        case UIRectEdge.right:
            border.frame = CGRect.init(x: frame.width - thickness, y: 0, width: thickness, height: frame.height)
            break
        default:
            break
        }
        
        border.backgroundColor = color.cgColor;
        
        self.addSublayer(border)
    }
}

extension UITextField{
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSForegroundColorAttributeName: newValue!])
        }
    }
}
@IBDesignable class TIFAttributedLabel: UILabel {
    
    @IBInspectable var fontSize: CGFloat = 13.0
    
    @IBInspectable var fontFamily: String = "DIN Light"
    
    override func awakeFromNib() {
        let attrString = NSMutableAttributedString(attributedString: self.attributedText!)
        attrString.addAttribute(NSFontAttributeName, value: UIFont(name: self.fontFamily, size: Utilities.isDeviceiPad() ? self.fontSize : self.fontSize + 5)!, range: NSMakeRange(0, attrString.length))
        self.attributedText = attrString
    }
}
extension UIView {

    func showRateReviewPopUp(serviceId: String?, requestId: String?, companyId:String?,serviceProviderImg:String?,serviceProviderName: String?, onSubmitClick: @escaping () -> ()) {
        
        self.window?.rootViewController?.view.endEditing(true)
        
        let reviewPopup = Bundle.main.loadNibNamed("Rating&ReviewPopup", owner: self, options: nil)?[0] as! Rating_ReviewPopup
        reviewPopup.frame = self.frame
        reviewPopup.serviceID = serviceId
        reviewPopup.requestID = requestId
        reviewPopup.companyID = companyId
        reviewPopup.vendorImg = serviceProviderImg
        reviewPopup.lblUserProfileName.text = serviceProviderName
        reviewPopup.setReviewData()
        reviewPopup.btnSubmitClick = {() in
            onSubmitClick()
        }
        self.addSubview(reviewPopup)
    }
    
    func showPromoCodePopUp(onSubmitClick: @escaping (String) -> ()) {
        
        self.window?.rootViewController?.view.endEditing(true)
        
        let promoPopup = Bundle.main.loadNibNamed("PromotionCodePopup", owner: self, options: nil)?[0] as! PromotionCodePopup
        promoPopup.frame = self.frame
        promoPopup.btnSubmitClick = {(code) in
                onSubmitClick(code)
        }
        self.addSubview(promoPopup)
    }
    
    func showRadioBoxPopup(topHeading: String?,defaultSelectedIndex:Int?, arrData: [String], missingData: String = "No data found", onSubmitClick: @escaping (String,Int) -> ()) {
        
        self.window?.rootViewController?.view.endEditing(true)
        
        let radioPopup = Bundle.main.loadNibNamed("CheckBoxPopup", owner: self, options: nil)?[0] as! CheckBoxPopup
        
        radioPopup.frame = self.frame
        radioPopup.type = PopupType.radiobox
        radioPopup.lblHeading.text = topHeading
        radioPopup.arr_tblData = arrData
        radioPopup.selectedIndex = defaultSelectedIndex
        radioPopup.lblNoDataFound.text = missingData
        
        radioPopup.btnSubmitForRadio = {(selectedText,selectedIndex) in
            onSubmitClick(selectedText,selectedIndex)
        }
     
        self.addSubview(radioPopup)
        
    }
    func showCatSelectionPopup(topHeading: String?,defaultSelectedIndex:Int?, arrData: [String], missingData: String = "No data found", onSubmitClick: @escaping (String,Int) -> ()) {
        
        self.window?.rootViewController?.view.endEditing(true)
        let radioPopup = Bundle.main.loadNibNamed("CheckBoxPopup", owner: self, options: nil)?[0] as! CheckBoxPopup
        radioPopup.frame = self.frame
        radioPopup.type = PopupType.radiobox
        radioPopup.lblHeading.text = topHeading
        radioPopup.arr_tblData = arrData
        radioPopup.selectedIndex = defaultSelectedIndex
        radioPopup.lblNoDataFound.text = missingData
        
        radioPopup.btnSubmitForRadio = {(selectedText,selectedIndex) in
            onSubmitClick(selectedText,selectedIndex)
        }
        
        
        
        self.addSubview(radioPopup)
    }
    
    func showCheckBoxPopup(topHeading: String?,arr_DefaultSelectedIndex:[Int],arr_AlreadySelectedIndex:[Int], arrData: [String] , onSubmitClick: @escaping ([String],[Int]) -> ()) {
        
        self.window?.rootViewController?.view.endEditing(true)
        
        let checkBoxPopup = Bundle.main.loadNibNamed("CheckBoxPopup", owner: self, options: nil)?[0] as! CheckBoxPopup
        
        checkBoxPopup.frame = self.frame
        checkBoxPopup.type = PopupType.checkbox
        checkBoxPopup.lblHeading.text = topHeading
        checkBoxPopup.arr_tblData = arrData
        checkBoxPopup.arr_SelectedIndex = arr_DefaultSelectedIndex
        checkBoxPopup.arr_AlreadySelectedIndex = arr_AlreadySelectedIndex
        checkBoxPopup.btnSubmitForCheckBox = {(arr_SelectedText,arr_SelectedIndex) in
            
            onSubmitClick(arr_SelectedText,arr_SelectedIndex)
            
        }
        
        self.addSubview(checkBoxPopup)
    }
    
    func showEventPopup(topHeading: String?, arrData: [String], allowDelete: Bool = true, onSubmitClick: @escaping ([String]) -> ()) {
        
        self.window?.rootViewController?.view.endEditing(true)
        
        let eventPopup = Bundle.main.loadNibNamed("CheckBoxPopup", owner: self, options: nil)?[0] as! CheckBoxPopup
        
        eventPopup.frame = self.frame
        eventPopup.type = PopupType.event
        eventPopup.lblHeading.text = topHeading
        eventPopup.arr_tblData = arrData
      
        //   eventPopup.reloadStatusView()
        eventPopup.isDeletable = allowDelete
        
        eventPopup.btnSubmitForEvent = {(arr_Data) in
            
            onSubmitClick(arr_Data)
        }
        self.addSubview(eventPopup)
    }
    
    func showAppoinmnetPopup(arrData: [GetAppointmentOfSpecificDate], onCloseClick: @escaping () -> ()) {
        
        self.window?.rootViewController?.view.endEditing(true)
        
        let eventPopup = Bundle.main.loadNibNamed("YourAppointmentPopUp", owner: self, options: nil)?[0] as! YourAppointmentPopUp
        
        eventPopup.frame = self.frame
        eventPopup.arr_data = arrData
        
        
        eventPopup.btnCloseForEvent = {
            onCloseClick()
        }
        
        self.addSubview(eventPopup)
    }
    
    func showCancelReasonPopup(obj: GetAppointmentOfSpecificDate, onSuccessCancel: @escaping () -> (), onCloseClick: @escaping () -> ()) {
        
        self.window?.rootViewController?.view.endEditing(true)
        
        
        let eventPopup = Bundle.main.loadNibNamed("CancellationResonPopUp", owner: self, options: nil)?[0] as! CancellationResonPopUp
        
        eventPopup.frame = self.frame
        eventPopup.appoinmentObj = obj
        eventPopup.successCancelBlock = {
            onSuccessCancel()
        }
        eventPopup.btnCloseForEvent = {
            onCloseClick()
        }
        
        self.superview?.addSubview(eventPopup)
    }
    
    func showSimpleDetailsPopup(topHeading: String?, infoContent: String?)
    {
        
        self.window?.rootViewController?.view.endEditing(true)
        
        let infoPopup = Bundle.main.loadNibNamed("InfoPopup", owner: self, options: nil)?[0] as! InfoPopup
        infoPopup.frame = self.frame
        
        //  infoPopup.lblHeading.text = topHeading
        
        DispatchQueue.main.async {
            
            infoPopup.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: Constant.ScreenSize.SCREEN_HEIGHT)
            if infoContent == "" {
                infoPopup.txt_content.text = "No description available"
            }else {
                infoPopup.txt_content.text = infoContent
            }
            
            var height: CGFloat = 105
            let estimHeight = Utilities.estimatedHeight(from: infoContent!, with: UIFont.systemFontReguler(with:Utilities.isDeviceiPad() ?20 :14), width: Constant.ScreenSize.SCREEN_WIDTH - 36)
            
            height += estimHeight
            
            if height > (Constant.ScreenSize.SCREEN_HEIGHT - 137)
            {
                height = Constant.ScreenSize.SCREEN_HEIGHT - 137
            }
            
            if Utilities.isDeviceiPad()
            {
                if height > (Constant.ScreenSize.SCREEN_HEIGHT - (137 * 2))
                {
                    height = Constant.ScreenSize.SCREEN_HEIGHT - (137 * 2)
                }
            }
            
            infoPopup.consTextViewHeight.constant = height
            infoPopup.txt_content.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
            
            let window = UIApplication.shared.keyWindow
            window?.addSubview(infoPopup)
         
        }
    }
    
    func alertPopup(alert: String) {
       
        self.window?.rootViewController?.view.endEditing(true)
        let AlertViewPopUp = Bundle.main.loadNibNamed("AlertViewPopUp", owner: self, options: nil)?[0] as! AlertViewPopUp
        AlertViewPopUp.frame = self.frame
        AlertViewPopUp.lblalertMessage.text = alert
        self.addSubview(AlertViewPopUp)
        
    }
    
    func ShowWantedServicePopup(title: String, msg:String) {
        
        self.window?.rootViewController?.view.endEditing(true)
        let WantedServicePopUp = Bundle.main.loadNibNamed("WantedServicePopUp", owner: self, options: nil)?[0] as! WantedServicePopUp
    
        WantedServicePopUp.lblTitle.text = title
        WantedServicePopUp.lblMessage.text = msg
        WantedServicePopUp.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: Constant.ScreenSize.SCREEN_HEIGHT)
      
     //   self.addSubview(WantedServicePopUp)
        
        let window = UIApplication.shared.keyWindow
        window?.addSubview(WantedServicePopUp)
    }
    
    func ShowCreateServiceAlertPopup(alert: String) {
        
        self.window?.rootViewController?.view.endEditing(true)
        let CreateServiceAlertPopup = Bundle.main.loadNibNamed("CreateServiceAlertPopup", owner: self, options: nil)?[0] as! CreateServiceAlertPopup
        CreateServiceAlertPopup.frame = self.frame
        CreateServiceAlertPopup.lblAlertMessage.text = alert
        self.addSubview(CreateServiceAlertPopup)
    }
    
    func showCountryPickPopup(onSubmitClick: @escaping (String) -> ()) {
        
        self.window?.rootViewController?.view.endEditing(true)
        
        let countryPickPopup = Bundle.main.loadNibNamed("CountryPickPopup", owner: self, options: nil)?[0] as! CountryPickPopup
        
        countryPickPopup.frame = self.frame
        countryPickPopup.btnSubmitForCountry = {(selectedCountryCode) in
            
            onSubmitClick(selectedCountryCode)
            
        }
        
        self.addSubview(countryPickPopup)
    }
    
    func ShowUpdateConformationPopup(requestID: String,CatID: String, SubCatID:String, name: String, Desc: String, delegate: ShowCategoryPopupDelegate? = nil, isFromWanted: Bool) -> ShowUpdatePopup {
              
        self.window?.rootViewController?.view.endEditing(true)
        let showUpdatePopup = Bundle.main.loadNibNamed("ShowUpdatePopup", owner: self, options: nil)?[0] as! ShowUpdatePopup
        showUpdatePopup.frame = self.frame
        showUpdatePopup.txtScreenName.text = name
        showUpdatePopup.txtDescription.text = Desc
        showUpdatePopup.selectedCategoryID = CatID
        showUpdatePopup.selectedSubCatagoryID = SubCatID
        showUpdatePopup.delegate = delegate
        showUpdatePopup.requestID = requestID
        showUpdatePopup.isFromWanted = isFromWanted
        self.addSubview(showUpdatePopup)
        return showUpdatePopup
    }
    
}

extension UIApplication {
    
    //MARK: - Find top most view controller
    class func topViewController(base: UIViewController? = (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController) -> UIViewController? {
        
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        
        if let tab = base as? UITabBarController {
            
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        
        return base
    }
    
}

//Get fonts with size
extension UIFont {
    class func systemFontReguler(with size:CFloat)-> UIFont{
        return UIFont(name: "SourceSansPro-Regular", size: CGFloat(size))!
    }
    class func systemFontBold(with size:CFloat)-> UIFont{
        return UIFont(name: "SourceSansPro-Bold", size: CGFloat(size))!
    }
    class func systemFontExtraBold(with size:CFloat)-> UIFont{
        return UIFont(name: "SourceSansPro-Black", size: CGFloat(size))!
    }
    class func systemFontSemiBold(with size:CFloat)-> UIFont{
        return UIFont(name: "SourceSansPro-SemiBold", size: CGFloat(size))!
    }
    class func systemFontLight(with size:CFloat)-> UIFont{
        return UIFont(name: "SourceSansPro-Light", size: CGFloat(size))!
    }
    class func systemFontItalic(with size:CFloat)-> UIFont{
        return UIFont(name: "SourceSansPro-Italic", size: CGFloat(size))!
    }
   
}

extension NSDate{
    
    class func getDateFrom(formate:String,dateString:String)->Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formate //Your date format
        let date = dateFormatter.date(from: dateString)
        return date!
    }
}

extension Array {
    // Code taken from https://stackoverflow.com/a/26174259/1975001
    /// Removes objects at indexes that are in the specified `NSIndexSet`.
    /// - parameter indexes: the index set containing the indexes of objects that will be removed
    public mutating func removeAtIndexes(indexes: NSIndexSet) {
        for i in indexes.reversed() {
            self.remove(at: i)
        }
    }
}

extension String {
    
    subscript(pos: Int) -> String {
        precondition(pos >= 0, "character position can't be negative")
        return self[pos...pos]
    }
    subscript(range: Range<Int>) -> String {
        precondition(range.lowerBound >= 0, "range lowerBound can't be negative")
        let lowerIndex = index(startIndex, offsetBy: range.lowerBound, limitedBy: endIndex) ?? endIndex
        return self[lowerIndex..<(index(lowerIndex, offsetBy: range.count, limitedBy: endIndex) ?? endIndex)]
    }
    subscript(range: ClosedRange<Int>) -> String {
        precondition(range.lowerBound >= 0, "range lowerBound can't be negative")
        let lowerIndex = index(startIndex, offsetBy: range.lowerBound, limitedBy: endIndex) ?? endIndex
        return self[lowerIndex..<(index(lowerIndex, offsetBy: range.count, limitedBy: endIndex) ?? endIndex)]
    }
    
    public func toPhoneNumber() -> String {
        return replacingOccurrences(of: "(\\d{3})(\\d{3})(\\d+)", with: "$1-$2-$3", options: .regularExpression, range: nil)
        
    }
}
