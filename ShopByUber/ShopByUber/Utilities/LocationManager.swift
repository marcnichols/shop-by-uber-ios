//
//  LocationManager.swift
//  ShopByUber
//
//  Created by Administrator on 6/30/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class LocationManager: NSObject,CLLocationManagerDelegate {
    
    let locManager = CLLocationManager()
    static let sharedManger = LocationManager()
    var latitude:Double = 0
    var longitude:Double = 0
    var myLocation:CLLocation!
    var timer:Timer?
    
    var handlerLocation:((CLLocation)->())?
    var handlerDirection:((CLHeading)->())?
    
    override init() {
        super.init()
        self.startUpdating()
    }
    
    init(locationHander:@escaping(CLLocation)->(),directionHandler:@escaping(CLHeading)->())
    {
        super.init()
        locManager.allowsBackgroundLocationUpdates = true
        handlerLocation = locationHander
        handlerDirection = directionHandler
        self.startUpdating()
        
    }
    
    func startUpdating()
    {
        locManager.delegate = self
        locManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locManager.requestAlwaysAuthorization()
        locManager.startUpdatingLocation()
        locManager.startUpdatingHeading()
        timer = Timer.scheduledTimer(timeInterval: 60*10, target: self, selector: #selector(startUpdating), userInfo: nil, repeats: true)
    }
    
    //MARK: - Location Delegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if locations.count == 0
        {
            return;
        }
        let newLocation: CLLocation? = locations.last
        // We don't want a cached location that is too out of date.
//        let locationAge: TimeInterval? = fabs((newLocation?.timestamp.timeIntervalSinceNow)!)
//        if (locationAge!) > 15.0 {
//            return
//        }
        if (Double((newLocation?.horizontalAccuracy)!) < 25) { return; }

        myLocation = newLocation;
        self.processOn(location: newLocation!)
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        if handlerDirection != nil{
            handlerDirection?(newHeading)
        }
    }
    
    func processOn(location:CLLocation)
    {
        if handlerLocation != nil{
            
            handlerLocation!(location)
            return
        }
        let aGMSGeocoder: GMSGeocoder = GMSGeocoder()
        aGMSGeocoder.reverseGeocodeCoordinate(CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude), completionHandler: { (response, error) in
            
            if let gmsAddress: GMSAddress = response?.firstResult() {
                
                print("\ncoordinate.latitude=\(gmsAddress.coordinate.latitude)")
                print("coordinate.longitude=\(gmsAddress.coordinate.longitude)")
                print("thoroughfare=\(gmsAddress.thoroughfare ?? "")")
                print("locality=\(gmsAddress.locality ?? "")")
                print("subLocality=\(gmsAddress.subLocality ?? "")")
                print("administrativeArea=\(gmsAddress.administrativeArea ?? "")")
                print("postalCode=\(gmsAddress.postalCode ?? "")")
                print("country=\(gmsAddress.country ?? "")")
                print("lines=\(gmsAddress.lines ?? [""])")
                
                self.latitude = gmsAddress.coordinate.latitude
                self.longitude = gmsAddress.coordinate.longitude
                var addressString = ""
                if gmsAddress.lines?.count != 0
                {
                    for i in gmsAddress.lines!
                    {
                        addressString = addressString + " " + i
                    }
                }
                UserDefaults.standard.set(gmsAddress.postalCode ?? "", forKey: Constant.DataModelKey.kzipCode)
                UserDefaults.standard.set(gmsAddress.coordinate.latitude, forKey: Constant.DataModelKey.kLatitude)
                UserDefaults.standard.set(gmsAddress.coordinate.longitude, forKey: Constant.DataModelKey.kLongtitude)
                UserDefaults.standard.set(addressString, forKey: Constant.DataModelKey.kLines)
                UserDefaults.standard.synchronize()
                
            }
            
        })
        locManager.stopUpdatingLocation()
        locManager.stopUpdatingHeading()
    }
    
    
    
}
