//
//  QuickBloxManager.swift
//  The EZ List
//
//  Created by mac on 2/2/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

import UIKit
import Quickblox

class QuickBloxManager: NSObject {
    
    static let sharedInstance = QuickBloxManager()
    
    //MARK: - Variables
    var currentUser: QBUUser?
    var isUserLoggedIn = false
    
    func isUserLoggedInQuickblox() -> Bool {
        return self.isUserLoggedIn
    }

    //MARK: - check and login/signup in quickblox
    func checkAndLoginInQuickBlox() {
        
        guard DataModel.sharedInstance.onFetchUserId() != "" else {
            return
        }
        let userID = DataModel.sharedInstance.onFetchUserId()
        let quickbloxID = DataModel.sharedInstance.onFetchQuickBloxId()
        let fullName = DataModel.sharedInstance.onFetchFirstName() + " " + DataModel.sharedInstance.onFetchLastName()
        
        if quickbloxID != 0 {
            // Login to quickblox as user register with quickblox
            self.onLoginWithUserLoginID(userLogindID: userID, completionHandler: { (isSuccess, quickBloxUser) in

            })
        } else {
            // if user not signup before then sign up user in quickblox
            self.onSignUpUserOnQuickBloxUsingUserID(userID: userID, name: fullName, completionHandler: { (isSuccess, quickBloxUser) in

                if isSuccess {
                    // update user id on server
                    let userChatID = quickBloxUser?.id ?? 0
                    User.callUpdateQuickbloxID(quickBloxId: userChatID)

                    // set user quickblox id
                    DataModel.sharedInstance.setValueInUserDefaults(value: quickBloxUser?.id, key: Constant.DataModelKey.kQuickbloxId)
                }
            })
        }
    }
    
    //MARK: - login with quickblox
    func onLoginWithUserLoginID(userLogindID : String, completionHandler : @escaping (Bool, QBUUser?) -> ()) {
        
        self.logInQuickBloxForUserID(userID: userLogindID) { (response, quickBloxUser) in
            if quickBloxUser != nil && DataModel.sharedInstance.isUserLoggedIn() {
                
                print("Quickblox User Id : \(quickBloxUser?.id ?? 0)")
                
                self.isUserLoggedIn = true
                self.currentUser = quickBloxUser
                
                // set user quickblox id
                DataModel.sharedInstance.setValueInUserDefaults(value: quickBloxUser?.id, key: Constant.DataModelKey.kQuickbloxId)
                
                // Connect user to chat
                quickBloxUser?.password = Constant.QuickBloxCredentials.kQuickBloxPassword
                ChatService.sharedInstance.onConnectUserToChat(user: quickBloxUser!, completion: { (error) in
                    
                    if !DataModel.sharedInstance.isUserLoggedIn() {
                        self.onLogoutFromQuickblox()
                    }
                })
                completionHandler(true, quickBloxUser)
            } else {
                completionHandler(false,nil)
            }
        }
    }
    
    func logInQuickBloxForUserID(userID : String,completionHandler : @escaping (QBResponse,QBUUser?) -> ()) {
        
        QBRequest.logIn(withUserLogin: userID, password: Constant.QuickBloxCredentials.kQuickBloxPassword, successBlock: { (response, userQuickBlox) in
            completionHandler(response, userQuickBlox)
        }) { (response) in
            completionHandler(response,nil)
        }
    }
    
    //MARK: - signup with quickblox
    func onSignUpUserOnQuickBloxUsingUserID(userID: String, name: String, completionHandler: @escaping (Bool, QBUUser?) -> ()) {
        
        let user : QBUUser = QBUUser.init()
        user.login = userID
        user.fullName = name
        user.password = Constant.QuickBloxCredentials.kQuickBloxPassword
        
        QBRequest.signUp(user, successBlock: { (response, quickBloxUser) in
            
            // login with quickblox
            self.onLoginWithUserLoginID(userLogindID: userID, completionHandler: { (isSuccess, quickBloxUser) in
                
            })
            completionHandler(true, quickBloxUser)
        }) { (response) in
            
            if let errorDescription = response.error?.reasons {
                if let errorTemp = errorDescription["errors"] as? NSDictionary {
                    if let loginArray = errorTemp.object(forKey: "login") as? NSArray {
                        if let strTemp = loginArray.object(at: 0) as? String {
                            if strTemp == "has already been taken." {
                                // User already exist so login
                                self.onLoginWithUserLoginID(userLogindID: userID, completionHandler: { (isSuccess, quickBloxUser) in
                                    if isSuccess {
                                        
                                        // update user id on server
                                        let userChatID = quickBloxUser?.id ?? 0
                                        User.callUpdateQuickbloxID(quickBloxId: userChatID)
                                    }
                                })
                            }
                        }
                    }
                }
            }
            completionHandler(false, nil)
        }
    }
    
    //MARK: - Logout from quickblox
    func onLogoutFromQuickblox(){
        
        ChatService.sharedInstance.onDisConnectUser { (error) in
            if error == nil {
                QBRequest.logOut(successBlock: { (response) in
                    print("onLogoutFromQuickblox: logOut from Quickblox")
                }, errorBlock: { (errResponse) in
                    print("onLogoutFromQuickblox: \(errResponse.description)")
                })
            }
        }
    }
    
    func onFetchUsersFromServer(completion:@escaping () -> ())  {
        
        if !self.isUserLoggedIn && !ChatService.sharedInstance.isChatConnected() && !DataModel.sharedInstance.isUserLoggedIn() {
            return
        }
        let arrayIDs = NSMutableArray()
        let quickBloxId = DataModel.sharedInstance.onFetchQuickBloxId()
        
        if let arrUsersIds = ChatService.sharedInstance.arrayUsersIDs {
            for (_,value) in arrUsersIds.enumerated() {
                let userChatID = value as! String
                if UInt(userChatID) != UInt(quickBloxId) {
                    arrayIDs.add(UInt(userChatID))
                }
            }
        }
        
        var dictParam = [String : Any?]()
        dictParam["userId"] = DataModel.sharedInstance.onFetchUserId()
        dictParam["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
        dictParam["quickBloxIds"] = arrayIDs
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kgetUsersFromQuickbloxIds, inputParams: dictParam, success: { (response) in
            
            KVNProgress.dismiss()
            if response?["Result"] as? Bool == true {
                let arrayDialogTemp = NSMutableArray()
                let arrayProfile =  response?["Data"] as? NSArray
                
                for (_,value) in (arrayProfile?.enumerated())! {
                    
                    let valueDict = value as! NSDictionary
                    let userChatID = valueDict.object(forKey: "quickBloxId") as? UInt
                    
                    for (_,value) in (ChatService.sharedInstance.arrayDialogs?.enumerated())! {
                        let chatDialog = value as! QBChatDialog
                        
                        for (_,value) in (chatDialog.occupantIDs?.enumerated())! {
                            let valueNumber = value
                            if valueNumber.uintValue == userChatID ?? 0 {
                                chatDialog.name = valueDict.object(forKey: "name") as? String
                                chatDialog.photo = valueDict.object(forKey: "profileImage") as? String
                                chatDialog.data = ["receiverId": valueDict.object(forKey: "_id") as? String ?? ""]
                                arrayDialogTemp.add(chatDialog)
                                break
                            }
                        }
                    }
                }
                if arrayDialogTemp.count > 0 {
                    if ChatService.sharedInstance.arrayDialogs != nil {
                        ChatService.sharedInstance.arrayDialogs?.removeAllObjects()
                        ChatService.sharedInstance.arrayDialogs = arrayDialogTemp
                    } else {
                        ChatService.sharedInstance.arrayDialogs = NSMutableArray()
                        ChatService.sharedInstance.arrayDialogs = arrayDialogTemp
                    }
                }
                completion()
            } else {
                completion()
            }
        }) { (error) in
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
            completion()
        }
    }
}
