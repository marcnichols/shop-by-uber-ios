//
//  RequestServiceVC.swift
//  The EZ List
//
//  Created by mac on 4/12/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

import UIKit

class RequestServiceVC: BaseView, ShowCategoryPopupDelegate {

    
    @IBOutlet var txtServiceName: UITextField!
    @IBOutlet var txtServiceCategory: UITextField!
    @IBOutlet var txtDescription: UITextView!
    @IBOutlet weak var txtServiceSubCategory: UITextField!
    @IBOutlet weak var lblNote: UILabel!
    
    var selectedCategoryID = ""
    var selectedSubCatagoryID = ""
    var selectedIndexCategory = -1
    var selectedIndexSubCategory = -1
    var categoryID = String()
    
    var arrCategoryList : NSArray = []
    var arrCategoryName = [String]()
    var arrSubCatagoriesNames = [String]()
    var arrCategoryID = [String]()
    var arr_SubCategories = [SubCategory]()
    var isfromAllWantedService = false
    var passDescription = String()
    var passName = String()
    var passRequestId = String()
    var showUpdatePopup: ShowUpdatePopup?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getCategoryList()
        
        if isfromAllWantedService == false {
            self.showSimpleAlert(title: "", message: "Please fill in the details of the service you requesting the service provider.")
        }else {
            //Prefill Data - Is from Update Service
            self.txtDescription.text = passDescription
            self.txtServiceName.text = passName
            print(selectedCategoryID)
            print(selectedSubCatagoryID)
        }
        self.setupNote()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupNote() {
        let titleAttributes = [NSForegroundColorAttributeName: UIColor.red, NSFontAttributeName: UIFont(name: "SourceSansPro-Bold", size: 13.0)!]
        let megAttributes = [NSForegroundColorAttributeName: UIColor.gray, NSFontAttributeName: UIFont(name: "SourceSansPro-Regular", size: 13.0)!]
        
        let partOne = NSMutableAttributedString(string: "Note: ", attributes: titleAttributes)
        let partTwo = NSMutableAttributedString(string: "Please fill in the details of the service you requesting the service provider.", attributes: megAttributes)
        
        let combination = NSMutableAttributedString()
        combination.append(partOne)
        combination.append(partTwo)
        self.lblNote.attributedText = combination
    }
    
    func getCategoryList() {
        
        KVNProgress.show()
        
        OccupiedData.callGetCategoriesListAPI( success: { (dict) in
            
            KVNProgress.dismiss()
          
            self.arrCategoryList = dict?["Categories"] as! NSArray
            self.arrCategoryName = self.arrCategoryList.value(forKey: "catName") as! [String]
            self.arrCategoryID = self.arrCategoryList.value(forKey: "_id") as! [String]
            self.setCategory()
        }) {
            
            if self.arrCategoryList.count == 0{
                
            }
        }
    }
    
    
    func setCategory()
    {
        for (index,element) in self.arrCategoryList.enumerated() {
            
            let dic = element as! NSDictionary
            
            categoryID = dic.value(forKey: "_id") as! String
            if categoryID == self.selectedCategoryID {
                
                self.txtServiceCategory.text = dic.value(forKey: "catName") as? String
                self.selectedIndexCategory = index
                print(index)
                self.setSubCategory()
            }
        }
    }
    
    func setSubCategory() {
        
        SubCategory.getSubSubcategories(categoryId: categoryID, allSubcategoryRequired: false, success: { (subcatData) in
            
            self.arr_SubCategories.removeAll()
            self.arr_SubCategories.append(contentsOf: subcatData)
            self.arrSubCatagoriesNames = self.arr_SubCategories.map({ $0.subCatName! })
            
            for (index,elements) in subcatData.enumerated() {
                
                let dic = elements
                
                let subid = dic.value(forKey: "id") as? String
              
                    self.txtServiceSubCategory.text = dic.subCatName
                    self.selectedIndexSubCategory = index
            }
            
            
        }, failure: {_ in
            print("Api Call Fail")
        })
    }
    
    @IBAction func vwCategory_Clicked(_ sender: Any) {
        
        print("Selected Index",self.selectedIndexCategory)
        self.view.showRadioBoxPopup(topHeading: Constant.PopupTitle.kSelectCategory, defaultSelectedIndex: selectedIndexCategory, arrData: arrCategoryName) { (selectedText, selectedIndex) in

            if selectedIndex == self.selectedIndexCategory {
                
            }else {
                self.txtServiceSubCategory.text = ""
                self.arrSubCatagoriesNames.removeAll()
                self.selectedIndexSubCategory = 0
            }
            self.txtServiceCategory.text = selectedText
            self.selectedIndexCategory = selectedIndex
            let dic = self.arrCategoryList.object(at: selectedIndex) as! NSDictionary
            self.selectedCategoryID = dic.value(forKey: "_id") as! String
            
            SubCategory.getSubSubcategories(categoryId: self.selectedCategoryID,isfromService: true, allSubcategoryRequired: false, success: { (sub_Cat) in
                
                self.arr_SubCategories.removeAll()
                self.arr_SubCategories.append(contentsOf: sub_Cat)
                self.arrSubCatagoriesNames = self.arr_SubCategories.map({ $0.subCatName! })
                
            }, failure: {(mess) in
                print("API call Error")
            //    self.noSubCatMsg = mess ?? "No subcategories in the selected category!"
                print(mess!)
            })
        }
    }
    
    @IBAction func vwSubCategory_Clicked(_ sender: Any) {
        
        //  Show Sub Category pop up
        self.view.showRadioBoxPopup(topHeading: Constant.PopupTitle.kSelectSubCategory, defaultSelectedIndex: self.selectedIndexSubCategory, arrData: self.arrSubCatagoriesNames, missingData: "No subcategories in the selected category!") { (selectedText, selectedIndex) in
            
            self.txtServiceSubCategory.text = selectedText
            self.selectedIndexSubCategory = selectedIndex
            self.selectedSubCatagoryID = self.arr_SubCategories[selectedIndex].id!
        }
    }
    
    func isValidServiceData(cat: String?, subCat: String?, serviceName: String?, desc: String?)  -> Bool {
        
        let cat = cat?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let subCat = subCat?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let serviceName = serviceName?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let desc = desc?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        var message = ""
        if cat == "" {
            message = "Please select a category for your service"
        }else if subCat == ""{
            message =  "Please select a Subcategory that best matches the Service you are creating!"
        }else if serviceName == ""{
            message =  "Please enter your service name"
        }else if desc == ""{
            message =  "Please enter description for your service"
        }else {
            return true
        }
        Utilities.showAlertView(title: "Alert", message: message)
        return false
    }

   
    @IBAction func on_btnSubmit_Click(_ sender: Any) {
        
        if self.isValidServiceData(cat: txtServiceCategory.text, subCat: txtServiceSubCategory.text, serviceName: txtServiceName.text, desc: txtDescription.text){
           
            let name = self.txtServiceName.text!
            let Desc = self.txtDescription.text!
            self.showUpdatePopup = self.view.ShowUpdateConformationPopup(requestID: passRequestId, CatID: selectedCategoryID, SubCatID: selectedSubCatagoryID, name: name , Desc: Desc, delegate: self, isFromWanted: isfromAllWantedService)
        }
    }
    
    func requestServiceDetails(){
        KVNProgress.show()
        var dict = [String:Any]()
        dict["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
        dict["userId"] = DataModel.sharedInstance.onFetchUserId()
        dict["serviceName"] = txtServiceName.text
        dict["categoryId"] = selectedCategoryID
        dict["subCategoryId"] = selectedSubCatagoryID
        dict["serviceDescription"] = txtDescription.text
   
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kAddWantedServices, inputParams: dict, success: { (dict) in
            
            KVNProgress.dismiss()
            
            if dict?["Result"] as? Bool == true {
                
                print("Success")
          
                Utilities.showAlertWithAction(title: "", message: (dict?["Message"] as? String)!, buttonTitle: "OK", buttonTitle2: "", onOKClick: {
                    
                self.navigationController?.popToRootViewController(animated: true)
                
                }, onCancelClick: {
                
                })
            }
            else{
                
                let msg = dict?["Message"] as? String
                Utilities.showAlertView(title: "", message: msg)
            }
            
            
        }) { error in
            
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
        }
    }
    
    //MARK:- Custome Delegate
    func showCategory(selectedCatId: String, selectedIndex: Int, catName: [String]) {

        print("Selected Index",selectedIndex)
     
        let navController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
        
        guard let popup = self.showUpdatePopup else {
            return
        }
        
        navController.view.showRadioBoxPopup(topHeading: Constant.PopupTitle.kSelectCategory, defaultSelectedIndex: selectedIndex, arrData: catName) { (selectedText, selectedIndex) in

            if selectedIndex == popup.selectedIndexCategory {

            }else {
                popup.txtSubCategory.text! = ""
                popup.arrSubCatagoriesNames.removeAll()
                popup.selectedIndexSubCategory = 0
            }
            popup.txtCategory.text! = selectedText
            popup.selectedIndexCategory = selectedIndex
            let dic = popup.arrCategoryList.object(at: selectedIndex) as! NSDictionary
            let selectedCategoryId = dic.value(forKey: "_id") as! String
            popup.selectedCategoryID = selectedCategoryId
            

            SubCategory.getSubSubcategories(categoryId: selectedCategoryId,isfromService: true, allSubcategoryRequired: false, success: { (sub_Cat) in

                popup.arr_SubCategories.removeAll()
                popup.arr_SubCategories.append(contentsOf: sub_Cat)
                popup.arrSubCatagoriesNames = popup.arr_SubCategories.map({ $0.subCatName! })

            }, failure: {(mess) in
                print("API call Error")
                //    self.noSubCatMsg = mess ?? "No subcategories in the selected category!"
                print(mess!)
            })
        }
    }
    
    func showSubCategory(selectedSubCatId: String, selectedIndex: Int, subCatName: [String]) {
        
        let navController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
        
        guard let popup = self.showUpdatePopup else {
            return
        }
        
        navController.view.showRadioBoxPopup(topHeading: Constant.PopupTitle.kSelectSubCategory, defaultSelectedIndex: selectedIndex, arrData: subCatName, missingData: "No subcategories in the selected category!") { (selectedText, selectedIndex) in
           
            popup.txtSubCategory.text = selectedText
          
            popup.selectedIndexSubCategory = selectedIndex
            
            popup.selectedSubCatagoryID = popup.arr_SubCategories[selectedIndex].id!
           
        }
    }
}

extension RequestServiceVC : UITextFieldDelegate
{
    //MARK: UITextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtServiceCategory {
            txtServiceSubCategory.becomeFirstResponder()
        }
        else if textField == txtServiceSubCategory {
            txtServiceName.becomeFirstResponder()
        }
        else if textField == txtServiceName{
            txtDescription.becomeFirstResponder()
        }else{
            textField.resignFirstResponder()
        }
        return true
    }
}
