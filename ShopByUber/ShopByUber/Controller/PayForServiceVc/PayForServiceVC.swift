//
//  PayForServiceVC.swift
//  ShopByUber
//
//  Created by Differenz215 on 6/27/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit
import CoreLocation

class PayForServiceVC: BaseView,UITableViewDelegate,UITableViewDataSource,PaymentServiceCellDelegate,UITextViewDelegate, PayPalPaymentDelegate,PaymentSuccessDelegate, STPAddCardViewControllerDelegate, STPPaymentCardTextFieldDelegate {
    
    //set pay pal environment
    var environment:String = PayPalEnvironmentProduction /*PayPalEnvironmentSandbox */{
        
        willSet(newEnvironment) {
            if (newEnvironment != environment) {
                PayPalMobile.preconnect(withEnvironment: newEnvironment)
            }
        }
    }
    
    var payPalConfig = PayPalConfiguration() // default
    
    
    //MARK:- IBOutlet
    @IBOutlet weak var tblService: UITableView!
    
    @IBOutlet weak var lblCartEmpty: UILabel!
    @IBOutlet weak var btnContinueShophing: UIButton!
    var btnpayService: UIButton!
    var btnStripPayment: UIButton!
    let paymentCardTextField = STPPaymentCardTextField()
    
    
    //MARK: - Variables
    var arrAppointmentData = [AppointmentConfirm]()
    var userDataDetails: userData?
    var isFrom: trackProgress?
    var indexpathIndex: Int?
    
    
    var PayKey : String = ""
    var redirectUrl : String = ""

    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        //for paypal init methos
        self.payPalInit()
        self.initialConfig()
        let notificationName = Notification.Name("ReloadView")
        NotificationCenter.default.addObserver(self, selector: #selector(initialConfig), name: notificationName, object: nil)
        
        // Setup Stripe payment card text field
        paymentCardTextField.delegate = self
        view.addSubview(paymentCardTextField)  // Add payment card text field to view
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      
        PayPalMobile.preconnect(withEnvironment: environment)
        Singleton.sharedManager.currentView = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        Singleton.sharedManager.currentView = nil
    }
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Private Methods
    
    func initialConfig(){
        
        self.tblService.register(UINib(nibName: "PaymentServiceCell", bundle: nil), forCellReuseIdentifier: "PaymentServiceCell")
        
        tblService.rowHeight = UITableViewAutomaticDimension
        tblService.estimatedRowHeight = 100000
        
        callGetAppointmentDetailsAPI()
    }
    
    //MARK:- Stripe Textfield Delegate
    func paymentCardTextFieldDidChange(_ textField: STPPaymentCardTextField) {
        // Toggle buy button state
        self.btnStripPayment.isEnabled = textField.isValid
        
    }
    
    //MARK: - Pay For Service
    func payForService()  {
        var totalAmount: Double = 0
        for i in arrAppointmentData
        {
            totalAmount += Double(i.flatFee!)!
        }
     
        self.payNow(total: totalAmount.description)
    }
    
    func performPayment(total: Double)  {
        

        KVNProgress.show()
        let service = self.arrAppointmentData[0]
        var dict = [String:Any]()
        dict["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
        dict["requestId"] = service.requests[0].requestId
        dict["Total"] = total
        dict["userId"] = DataModel.sharedInstance.onFetchUserId()

        APIManager.callAPI(url: Constant.serverAPI.URL.kgetPaymentUrl, inputParams: dict, success: { (dict) in
            
            KVNProgress.dismiss()
            
            if dict?["Result"] as? Bool == true {
                
                print("Success")
                let dict = dict?["Data"] as? [String:Any]
                self.redirectUrl = dict?["redirectUrl"] as! String
                self.PayKey = dict?["PayKey"] as! String
                
                let payPalWebVC = PayPalWebVC(nibName: "PayPalWebVC", bundle: nil)
                payPalWebVC.redirectURL = self.redirectUrl
                payPalWebVC.delegate = self
                self.navigationController?.pushViewController(payPalWebVC, animated: true)
                
                
            }
            else{
                
                let msg = dict?["Message"] as? String
                Utilities.showAlertView(title: "", message: msg)
            }
            
            
        }) { error in
            
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
        }

    }
    
    func payPalInit()
    {
        // Set up payPalConfig
        payPalConfig.acceptCreditCards = true
        payPalConfig.merchantName = Constant.PayPalInfo.merchantName
        payPalConfig.merchantPrivacyPolicyURL = URL(string: Constant.PayPalInfo.policy)
        payPalConfig.merchantUserAgreementURL = URL(string: Constant.PayPalInfo.agreement)
        
        
        payPalConfig.languageOrLocale = Locale.preferredLanguages[0]
        
        // Setting the payPalShippingAddressOption property is optional.
        //
        // See PayPalConfiguration.h for details.
        
        payPalConfig.payPalShippingAddressOption = .payPal;
        
        print("PayPal iOS SDK Version: \(PayPalMobile.libraryVersion())")
    }
    
    func payNow(total: String)
    {
        
        // Note: For purposes of illustration, this example shows a payment that includes
        //       both payment details (subtotal, shipping, tax) and multiple items.
        //       You would only specify these if appropriate to your situation.
        //       Otherwise, you can leave payment.items and/or payment.paymentDetails nil,
        //       and simply set payment.amount to your total charge.
        
        // Optional: include multiple items
        let item1 = PayPalItem(name: Constant.PayPalInfo.merchantName, withQuantity: 1, withPrice: NSDecimalNumber(string: total), withCurrency: Constant.PayPalInfo.kCurrency, withSku: "")
        
        
        let items = [item1]
        let subtotal = PayPalItem.totalPrice(forItems: items)
        
        //        // Optional: include payment details
        //        let shipping = NSDecimalNumber(string: "5.99")
        //        let tax = NSDecimalNumber(string: "2.50")
        //        let paymentDetails = PayPalPaymentDetails(subtotal: subtotal, withShipping: shipping, withTax: tax)
        //
        //        let total = subtotal.adding(shipping).adding(tax)
        
        let payment = PayPalPayment(amount: subtotal, currencyCode: Constant.PayPalInfo.kCurrency, shortDescription: Constant.PayPalInfo.kSortDesc, intent: .sale)
        
        payment.items = items
        //        payment.paymentDetails = paymentDetails
        
        if (payment.processable) {
            let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
            present(paymentViewController!, animated: true, completion: nil)
        }
        else {
            // This particular payment will always be processable. If, for
            // example, the amount was negative or the shortDescription was
            // empty, this payment wouldn't be processable, and you'd want
            // to handle that here.
            print("Payment not processalbe: \(payment)")
        }
        
    }
    
    
    //MARK:- IBAction
    
    @IBAction func btnPayForService_Click(_ sender: Any) {
        if self.arrAppointmentData.count == 0
        {
            Utilities.showAlertView(title: "", message: "No appointment available")
        }
        else
        {
            self.payForService()
        }
        
    }
    
    @IBAction func btnPayByStripe_Click(_ sender: Any) {
        
        // Setup add card view controller
        let addCardViewController = STPAddCardViewController()
        addCardViewController.delegate = self
        
        // Present add card view controller
        let navigationController = UINavigationController(rootViewController: addCardViewController)
        present(navigationController, animated: true)
    }
    
    @IBAction func btnContinueShopping_Click(_ sender: Any) {
            appDeletgate.dismissBubble()
            appShareManager.selectedMenuItemIndex = 0
            let revealController = self.revealViewController
            let vc = BuyerVC(nibName: "BuyerVC", bundle: nil) as BuyerVC
            if revealController() != nil
            {
                revealController().pushFrontViewController(vc, animated: true)
            }
            else
            {
                appDeletgate.onSetupDashboardPage()
            }
            
    }
    func PaymentSuccess(){
        
        
        var dict                = [String : Any]()
        let service = self.arrAppointmentData[0]
        dict["requestId"] = service.requests[0].requestId
        dict["sessionId"]       = DataModel.sharedInstance.onFetchSessionId()
        dict["userId"]          = DataModel.sharedInstance.onFetchUserId()
        dict["PayKey"]          = PayKey
        dict["serviceId"]       = self.arrAppointmentData[0].serviceId

        var userDict = [String:Any]()
        userDict["latitude"] = service.requests[0].userInfo?.latitude
        userDict["longitude"] = service.requests[0].userInfo?.longitude
        userDict["address"] = service.requests[0].userInfo?.address
        dict["userInfo"]    = userDict
        
        KVNProgress.show()
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kpayForServiceAdaptive, inputParams: dict, success: { (dict) in
            KVNProgress.dismiss()
            
            if dict?["Result"] as? Bool == true {
                
                let track = TrackProgressVC.init(nibName: "TrackProgressVC", bundle: nil)
                track.requestID = self.arrAppointmentData[0].requests[0].requestId!
                
                track.isFrom = self.isFrom
                self.navigationController?.pushViewController(track, animated: true)
                
            }
            else
            {
                Utilities.showAlertView(title: "", message: dict?["Message"] as? String)
            }
        }) { error in
            KVNProgress.dismiss()
            
            Utilities.showAlertView(title: "", message: error)
            
        }
    
    }
    
    
    //MARK: - Appointment Detail Delegate
    func PaymentServiceRemovetimeService(cell: PaymentServiceCell, buttonType: UIButton)
    {
        if(buttonType == cell.btnRemoveService )
        {
            KVNProgress.show()
            let service = self.arrAppointmentData[cell.btnRemoveService.tag]
            var dict = [String : Any]()
            dict["sessionId"]    = DataModel.sharedInstance.onFetchSessionId()
            dict["userId"]       = DataModel.sharedInstance.onFetchUserId()
            dict["requestId"]    = service.requests[0].requestId
            dict["timeSlotId"]   = service.requests[0].timeslotId
            
            APIManager.callAPI(url: Constant.serverAPI.URL.kCancelSingleAppointments, inputParams: dict, success: { (dict) in
                
                if dict?["Result"] as? Bool == true {
                    self.arrAppointmentData.remove(at: cell.btnRemoveService.tag)
                    self.tblService.deleteRows(at: [IndexPath.init(row: cell.btnRemoveService.tag, section: 0)], with: .fade)
                    UIView.animate(withDuration: 0.25, animations: {
                        self.tblService.reloadData()
                        
                    })
                }
                if self.arrAppointmentData.count == 0
                {
                    self.btnContinueShophing.isHidden = true
                //    self.btnpayService.isHidden = true
                    self.tblService.tableFooterView = nil

                    self.lblCartEmpty.isHidden = false
                }
                KVNProgress.dismiss()
            }) { error in
                KVNProgress.dismiss()
                Utilities.showAlertView(title: "", message: error)
            }
        }
    }
    
    func PaymentServiceAddressTap(cell: PaymentServiceCell)  {
        self.indexpathIndex = cell.tag
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)

    }
    
    //MARK:  - TableView Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrAppointmentData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->
        UITableViewCell {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentServiceCell", for: indexPath) as! PaymentServiceCell
            let appointmentCellData = self.arrAppointmentData[indexPath.row]
            
            if appointmentCellData.serviceImage != nil{
                cell.imgHeader.sd_setImage(with: URL.init(string: (appointmentCellData.serviceImage)!), placeholderImage: #imageLiteral(resourceName: "placeHolder"), options: SDWebImageOptions.retryFailed, completed: nil)
            }
            
            
            cell.lblHeaderTitle.text = appointmentCellData.screenName
            cell.lblFee.text = "$ " + appointmentCellData.flatFee!
            cell.arrTimeSlotData = appointmentCellData.requests
            cell.lblServiceDescription.text = appointmentCellData.serviceDetail ?? "-"
            cell.lblProfileName.text = appointmentCellData.vendor?.name
            if appointmentCellData.vendor?.profileImage != nil{
                cell.imgProfileImage.sd_setImage(with: URL.init(string: (appointmentCellData.vendor?.profileImage)!), placeholderImage: UIImage(named:Constant.PlaceHolder.kPhoto), options: SDWebImageOptions.retryFailed, completed:{ (image, err, nil, url) in
                    if image == nil
                    {
                        cell.imgProfileImage.image = #imageLiteral(resourceName: "placeHolder")
                        cell.imgProfileImage.contentMode = .scaleAspectFit
                    }
                    else
                    {
                        cell.imgProfileImage.image = image
                        cell.imgProfileImage.contentMode = .scaleAspectFill
                    }
                })
                
            }
            else
            {
                cell.imgProfileImage.image = #imageLiteral(resourceName: "placeHolder")
                cell.imgProfileImage.contentMode = .scaleAspectFit
            }
         
            cell.delegate = self
            let data = appointmentCellData.requests[0]
            cell.lblAppointmentTime.text    = data.duration
            cell.lblProfileDetails.text     = appointmentCellData.vendor?.speciality
            cell.lblExperience.text         = appointmentCellData.vendor?.vendordescription
            cell.btnRemoveService.tag       = indexPath.row
            cell.tag                        = indexPath.row
            cell.lblAddress.text            = data.userInfo?.address
         
            return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    // MARK: -  PayPalPaymentDelegate
    func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController) {
        print("PayPal Payment Cancelled")
        
        paymentViewController.dismiss(animated: true, completion: nil)
    }
    
    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment) {
        print("PayPal Payment Success !")
        paymentViewController.dismiss(animated: true, completion: { () -> Void in
            // send completed confirmaion to your server
            print("Here is your proof of payment:\n\n\(completedPayment.confirmation)\n\nSend this to your server for confirmation and fulfillment.")
            self.submitConfirmationData(Payment: completedPayment)
            //self.showSuccess()
        })
    }
    
    //MARK: - API Call
    func callGetAppointmentDetailsAPI(){
        
        KVNProgress.show()
        var dict = [String : Any]()
        
        dict["sessionId"]    = DataModel.sharedInstance.onFetchSessionId()
        dict["userId"]       = DataModel.sharedInstance.onFetchUserId()
        
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kGetConfirmRequestData, inputParams: dict, success: { (dict) in
            
            if dict?["Result"] as? Bool == true {
                let data = dict?["Data"] as? [String:Any]
                if data?.count != 0
                {
                    
                    let payment = data?["payment_configuration"] as? [String:Any]
                    
                    let key = payment?["key"] as! String
                    
                    if (payment!["key"] != nil)
                    {
                        print(payment!["key"]!)
                        STPPaymentConfiguration.shared().publishableKey = key
                    }
                    
                    let confirmServices = data?["confirmServices"] as? NSArray
                    if confirmServices != nil
                    {
                        for i in confirmServices!
                        {
                            let appointment = AppointmentConfirm.init(dict: i as! [String : Any])
                            self.arrAppointmentData.append(appointment)
                        }
                        self.tblService.reloadData()
                        self.lblCartEmpty.isHidden = true
                        self.btnContinueShophing.isHidden = true//false
                        let footerView = UIView()
                        footerView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width - 20, height: Utilities.isDeviceiPad() ? 105 : 90 )
//                        self.btnpayService = UIButton(frame: CGRect(x: 10, y: 0, width: UIScreen.main.bounds.size.width - 20, height: Utilities.isDeviceiPad() ? 50 : 45 ))
//
                        //Paypal Button
//                        self.btnpayService.backgroundColor = Constant.Color.kPinkColor
//                        self.btnpayService.titleLabel?.font = UIFont.systemFontSemiBold(with: 22)
//                        self.btnpayService.setTitle("PAY FOR SERVICE", for: .normal)
//                        self.btnpayService.addTarget(self, action: #selector(self.btnPayForService_Click(_:)), for: .touchUpInside)
//                        self.btnpayService.tag = 1
//                        footerView.addSubview(self.btnpayService)
                        
                        //Stripe Button
                        self.btnStripPayment = UIButton(frame: CGRect(x: 10, y: 0, width: UIScreen.main.bounds.size.width - 20, height: Utilities.isDeviceiPad() ? 105 : 90 ))
                        self.btnStripPayment.backgroundColor = Constant.Color.kPinkColor
                        self.btnStripPayment.titleLabel?.font = UIFont.systemFontSemiBold(with: 22)
                        self.btnStripPayment.setTitle("PAY FOR SERVICE", for: .normal)
                        self.btnStripPayment.addTarget(self, action: #selector(self.btnPayByStripe_Click(_:)), for: .touchUpInside)
                        footerView.addSubview(self.btnStripPayment)
                        
                        
                        self.tblService.tableFooterView = footerView
                        self.view.layoutIfNeeded()
                    }
                    
                }
            }
            KVNProgress.dismiss()
        }) { error in
            KVNProgress.dismiss()
            
            Utilities.showAlertView(title: "", message: error)
            
        }
    }
    
    func submitConfirmationData(Payment: PayPalPayment)
    {
        var subServiceDict      = [[String : Any]]()
        for i in self.arrAppointmentData
        {
            var ServiceDict     = [String : Any]()
            ServiceDict["serviceId"] = i.serviceId
            var request  = [[String:Any]]()
            for j in i.requests
            {
                var dict = [String:Any]()
                dict["id"] = j.requestId!
                dict["flatFee"] = i.flatFee
                var userDict = [String:Any]()
                userDict["latitude"] = j.userInfo?.latitude
                userDict["longitude"] = j.userInfo?.longitude
                userDict["address"] = j.userInfo?.address
                dict["userInfo"]    = userDict
                request.append(dict)
            }
            ServiceDict["requestId"]   = request
            subServiceDict.append(ServiceDict)
        }
        
        var dict                = [String : Any]()
        dict["sessionId"]       = DataModel.sharedInstance.onFetchSessionId()
        dict["userId"]          = DataModel.sharedInstance.onFetchUserId()
        dict["TransactionId"]   = (Payment.confirmation["response"] as? NSDictionary)?.value(forKey: "id") as! String
        dict["Total"]           = Payment.amount.description
        dict["paymentDateTime"] = (Payment.confirmation["response"] as? NSDictionary)?.value(forKey: "create_time") as! String
        dict["services"]        = subServiceDict
        
        KVNProgress.show()
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kPayForService, inputParams: dict, success: { (dict) in
            KVNProgress.dismiss()
            
            if dict?["Result"] as? Bool == true {
                
                let track = TrackProgressVC.init(nibName: "TrackProgressVC", bundle: nil)
                track.requestID = self.arrAppointmentData[0].requests[0].requestId!
                
                track.isFrom = self.isFrom
                self.navigationController?.pushViewController(track, animated: true)

            }
            else
            {
                Utilities.showAlertView(title: "", message: dict?["Message"] as? String)
            }
        }) { error in
            KVNProgress.dismiss()
            
            Utilities.showAlertView(title: "", message: error)
            
        }
    }
    
    func submitPaymentData(token: String)
    {
        var subServiceDict      = [[String : Any]]()
        for i in self.arrAppointmentData
        {
            var ServiceDict     = [String : Any]()
            ServiceDict["serviceId"] = i.serviceId
            var request  = [[String:Any]]()
            for j in i.requests
            {
                var dict = [String:Any]()
                dict["id"] = j.requestId!
                dict["flatFee"] = i.flatFee
                var userDict = [String:Any]()
                userDict["latitude"] = j.userInfo?.latitude
                userDict["longitude"] = j.userInfo?.longitude
                userDict["address"] = j.userInfo?.address
                dict["userInfo"]    = userDict
                request.append(dict)
            }
            ServiceDict["requestId"]   = request
            subServiceDict.append(ServiceDict)
        }
        
        var totalAmount: Double = 0
        for i in arrAppointmentData
        {
            totalAmount += Double(i.flatFee!)!
        }
        var dict                = [String : Any]()
        dict["sessionId"]       = DataModel.sharedInstance.onFetchSessionId()
        dict["userId"]          = DataModel.sharedInstance.onFetchUserId()
        dict["Total"]           = totalAmount
        dict["token"]           = token
        dict["services"]        = subServiceDict
        
        KVNProgress.show()
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kpayment, inputParams: dict, success: { (dict) in
            KVNProgress.dismiss()
            
            if dict?["Result"] as? Bool == true {
                
                let track = TrackProgressVC.init(nibName: "TrackProgressVC", bundle: nil)
                track.requestID = self.arrAppointmentData[0].requests[0].requestId!
                
                track.isFrom = self.isFrom
                self.navigationController?.pushViewController(track, animated: true)
                
            }
            else
            {
                Utilities.showAlertView(title: "", message: dict?["Message"] as? String)
            }
        }) { error in
            KVNProgress.dismiss()
            
            Utilities.showAlertView(title: "", message: error)
            
        }
    }
    
    //MARK:- Stripe Delegate
    
    func addCardViewControllerDidCancel(_ addCardViewController: STPAddCardViewController) {
        // Dismiss add card view controller
        dismiss(animated: true)
    }
    
    func addCardViewController(_ addCardViewController: STPAddCardViewController, didCreateToken token: STPToken, completion: @escaping STPErrorBlock) {
        
        submitTokenToBackend(token: token, completion: { (error: Error?) in
        if let error = error {
            // Show error in add card view controller
            completion(error)
        }
        else {
            // Notify add card view controller that token creation was handled successfully
            // Dismiss add card view controller
            completion(nil)
            dismiss(animated: true)
        }
        })
    }
    
    func submitTokenToBackend(token: STPToken, completion: (_ error:Error)->()){
        
        print("token",token)
        submitPaymentData(token: token.tokenId)  //Call Backend Api
        self.dismiss(animated: true)
    }
    
}
extension PayForServiceVC : GMSAutocompleteViewControllerDelegate
{
    //MARK: - Auto Complete Google Delegates
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        if let addressLines = place.addressComponents {
            // Populate all of the address fields we can find.
            
            for field in addressLines {
                
                switch field.type {
                case kGMSPlaceTypePostalCode:
                    _ = field.name
                default:
                    //print("Type: \(field.type), Name: \(field.name)")
                    print("")
                }
            }
        }
        let cell = tblService.cellForRow(at: IndexPath.init(row: self.indexpathIndex!, section: 0)) as! PaymentServiceCell
        let dict = ["address":place.formattedAddress!,"latitude":place.coordinate.latitude,"longitude":place.coordinate.longitude] as [String : Any]
        self.arrAppointmentData[cell.tag].requests[0].userInfo = userData.init(dict: dict)
        cell.lblAddress.text = place.formattedAddress!
        dismiss(animated: true, completion: nil)
    }
    
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    func wasCancelled(_ viewController: GMSAutocompleteViewController)
    {
        viewController.dismiss(animated: true, completion: nil)
    }
    
}
