//
//  TermsAndConditionVC.swift
//  ShopByUber
//
//  Created by Administrator on 7/6/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class TermsAndConditionVC: BaseView {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var webView: UIWebView!
    
    var strTitle : String = ""
    var url: URL?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        lblTitle.text = strTitle
        
        // Adding webView content
        let requestObj = URLRequest(url: url!)
        webView.loadRequest(requestObj)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnMenu_Tapped(_ sender: Any) {
        if self.revealViewController() != nil
        {
            self.revealToggle(true)
        }
    }
}
