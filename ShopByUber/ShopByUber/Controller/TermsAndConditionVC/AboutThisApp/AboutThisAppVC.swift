//
//  AboutThisAppVC.swift
//  The EZ List
//
//  Created by DIFFERENZ101 on 10/28/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class AboutThisAppVC: UIViewController ,UIWebViewDelegate{

    //MARK:- Variable
    var strTitle : String = ""
    var url: URL?
    
    //MARK:- Outlet
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var lblTitle: UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        lblTitle.text = strTitle
        
        KVNProgress.show()
        // Adding webView content
        let requestObj = URLRequest(url: url!)
        webView.delegate = self
        webView.loadRequest(requestObj)
        KVNProgress.dismiss()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func btnMenu_Click(_ sender: Any) {
        if self.revealViewController() != nil
        {
            self.revealToggle(true)
        }

    }

    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        print(error)
    }

}
