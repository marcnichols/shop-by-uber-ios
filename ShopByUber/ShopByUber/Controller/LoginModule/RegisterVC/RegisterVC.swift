//
//  RegisterVC.swift
//  ShopByUber
//
//  Created by Administrator on 5/24/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit
import GoogleMaps


class RegisterVC: UIViewController, CLLocationManagerDelegate {

    //MARK: IBOutlet
    @IBOutlet var txtFirstName: UITextField!
    @IBOutlet var txtLastName: UITextField!
    @IBOutlet weak var txtEzlistScreenName: UITextField!
    @IBOutlet var txtMobileNumber: UITextField!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtPassword: UITextField!
    @IBOutlet var txtConfirmPassword: UITextField!
    @IBOutlet var btnLogin: UIButton!
    @IBOutlet var userTypeSegment: UISegmentedControl!
    @IBOutlet weak var vwNavBar: UIView!
    @IBOutlet weak var lblCounrtyCode: UILabel!
    
    //MARK:- Variable
    var isPush: Bool = false
    var isForBuyer:Bool = false
    var isFromSwitchProfile:Bool = false
    var strAddress = String()
    var locationManager = CLLocationManager()
    var lat = Double()
    var long = Double()
    var city = String()
    var state = String()
    var country = String()
    
    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialConfig()
        
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                
                //Show Alert to Enable Location Service
                let alertController = UIAlertController(title: NSLocalizedString(Constant.appTitle.strTitle + " Need to access your Location", comment: ""), message: NSLocalizedString("Please turn on Location from Settings", comment: ""), preferredStyle: .alert)
                let cancelAction = UIAlertAction (title: "Cancel", style: .cancel, handler: { (UIAlertAction) in
                    self.navigationController?.popViewController(animated: true)
                })
              
                let settingsAction = UIAlertAction(title: "Settings", style: .default) { (UIAlertAction) in
                    UIApplication.shared.openURL(NSURL(string: UIApplicationOpenSettingsURLString)! as URL)
                    self.navigationController?.popViewController(animated: true)
                }
                
                alertController.addAction(cancelAction)
                alertController.addAction(settingsAction)
                self.present(alertController, animated: true, completion: nil)
                
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {

        vwNavBar.isHidden = !isPush
       
        if isFromSwitchProfile {
            
            vwNavBar.isHidden = true
        }
        
        //MARK:- Getting User Current Location
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
    }
    
    //MARK:- CLLocation Manager Delegate
    @nonobjc func locationManager(_ manager: CLLocationManager, didFailWithError error: NSError)
    {
        print("Error" + error.description)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let userLocation = locations.last
    
        lat = userLocation!.coordinate.latitude
        long = userLocation!.coordinate.longitude
        print("Latitude :- \(userLocation!.coordinate.latitude)")
        print("Longitude :-\(userLocation!.coordinate.longitude)")
       
        locationManager.stopUpdatingLocation()
        
        //Address Maker
        let geocoder = CLGeocoder()
        let location = CLLocation(latitude: userLocation!.coordinate.latitude, longitude: userLocation!.coordinate.longitude)
        geocoder.reverseGeocodeLocation(location) { (placemarksArray, error) in
            
            if (placemarksArray?.count)! > 0 {
                
                let placemark = placemarksArray?.first
                let number = placemark?.subThoroughfare
                let street = placemark?.thoroughfare
                let bairro = placemark?.subLocality
                self.city = (placemark?.locality)!
                let postcode = placemark?.postalCode
                self.state = (placemark?.administrativeArea)!
                self.country = (placemark?.country)!
                
                
                self.strAddress  = "\(number ?? ""), \(street ?? ""), \(bairro ?? ""), \(self.city), \(postcode ?? ""), \(self.state), \(self.country)"
                print(self.strAddress)
                
            }
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK: - IBAction
    
    @IBAction func vwBG_Clicked(_ sender: UITapGestureRecognizer) {
        
        //hide keyboard
        self.view.endEditing(true)
    }
    
    
    @IBAction func btnLogin_Clicked(_ sender: UIButton) {
        
        if isPush
        {
            let login = LoginVC(nibName: "LoginVC", bundle: nil)
            login.isPush = true
            self.navigationController?.pushViewController(login, animated: true)
        }
        else
        {
            _ = navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func btnRegister_Clicked(_ sender: UIButton) {
        
       self.view.endEditing(true)
        
       let mobileNo = txtMobileNumber.text?.replacingOccurrences(of: "-", with: "")
        if User.isValidUserData(userType: 0, firstName: txtFirstName.text, lastName: txtLastName.text, email: txtEmail.text, mobileNumber: mobileNo, password: txtPassword.text, confirmPassword: txtConfirmPassword.text) {
            
            User.callRegisterBuyerAPI(firstName: txtFirstName.text, lastName: txtLastName.text,ezListScreenName : txtEzlistScreenName.text, email: txtEmail.text, mobileNumber: mobileNo, countryCode: self.lblCounrtyCode.text, password: txtPassword.text, userType: Constant.UserType.Buyer, address: strAddress,city: self.city, state: self.state, country: self.country, latitude: self.lat, longitude: self.long, registerFrom: "ios", quickBloxId: 0, success: { responseDict in
                
                    print(responseDict!)
                let userId = responseDict?["userId"] as? String
                DataModel.sharedInstance.setValueInUserDefaults(value: userId, key: Constant.DataModelKey.KUserId)
                DataModel.sharedInstance.setValueInUserDefaults(value: self.strAddress, key: Constant.DataModelKey.kaddress)
                DataModel.sharedInstance.setValueInUserDefaults(value: self.city, key: Constant.DataModelKey.kCity)
                DataModel.sharedInstance.setValueInUserDefaults(value: self.state, key: Constant.DataModelKey.kState)
                
                let verficationCode = responseDict?["verficationCode"] as? Int
                
                let numberVerification = NumberVerificationVC(nibName: "NumberVerificationVC", bundle: nil) as NumberVerificationVC
                
                numberVerification.userType = Constant.UserType.Buyer
                numberVerification.countryCode = self.lblCounrtyCode.text
                numberVerification.code = verficationCode ?? 0
                numberVerification.mobileNumber = self.txtMobileNumber.text!
                numberVerification.countryCode = self.lblCounrtyCode.text
                _ = self.navigationController?.pushViewController(numberVerification, animated: true)
                
            }, failure: {
                
            })
            
        }

    }

    
    @IBAction func btnBack_Clicked(_ sender: UIButton) {
        
        _ = navigationController?.popViewController(animated: true)
        
    }

    @IBAction func btnCountryPic(_ sender: Any) {
        self.view.showCountryPickPopup(onSubmitClick: {(selectedCountryCode) in
            self.lblCounrtyCode.text = selectedCountryCode
        })
    }
    
    //MARK: - Private Methods
    func initialConfig(){
       
        Utilities.setupAttributedButton(range: NSRange(location:17,length:9), btn: btnLogin)
        self.lblCounrtyCode.text = "+1"
    }
    
    func resetTextField(){
    
        txtFirstName.text = ""
        txtLastName.text = ""
        txtMobileNumber.text = ""
        txtEmail.text = ""
        txtPassword.text = ""
        txtConfirmPassword.text = ""
        
    }
    
    func emailFirstResponder() {
        txtEmail.becomeFirstResponder()
    }
    
    func viewEndEditing()  {
        self.view.endEditing(true)
    }
}

extension RegisterVC : UITextFieldDelegate
{
    //MARK: UITextField Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == txtMobileNumber{
        let keyboardToolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: 44))
        keyboardToolBar.barStyle = .default

        let done = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(viewEndEditing))
        done.tintColor = UIColor.black
        done.setTitleTextAttributes([NSFontAttributeName:UIFont.systemFont(ofSize: 17, weight: UIFontWeightSemibold)], for: .normal)
        let next = UIBarButtonItem(title: "Next", style: .plain, target: self, action: #selector(emailFirstResponder))
        next.tintColor = UIColor.black
        next.setTitleTextAttributes([NSFontAttributeName:UIFont.systemFont(ofSize: 17, weight: UIFontWeightSemibold)], for: .normal)
        keyboardToolBar.items = [next,UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),done]
    
        textField.inputAccessoryView = keyboardToolBar
        }
        else
        {
            textField.inputAccessoryView?.removeFromSuperview()
            //textField.inputAccessoryView = nil
            
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtFirstName{
            txtLastName.becomeFirstResponder()
        }else if textField == txtLastName{
            txtEzlistScreenName.becomeFirstResponder()
        }else if textField == txtEzlistScreenName{
            txtMobileNumber.becomeFirstResponder()
        }else if textField == txtMobileNumber{
            txtEmail.becomeFirstResponder()
        }else if textField == txtEmail{
            txtPassword.becomeFirstResponder()
        }else if textField == txtPassword{
            txtConfirmPassword.becomeFirstResponder()
        }else{
            textField.resignFirstResponder()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let str = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        if textField == txtMobileNumber{
            
            return checkEnglishPhoneNumberFormat(string: string, str: str)
            
        }else{
            
            return true
        }
    }
    
    func checkEnglishPhoneNumberFormat(string: String?, str: String?) -> Bool{
        
        if string == ""{ //BackSpace
            
            return true
            
        }else if str!.count == 4{
            
            txtMobileNumber.text = txtMobileNumber.text! + "-"
            
        }else if str!.count == 8{
            
            txtMobileNumber.text = txtMobileNumber.text! + "-"
            
        }else if str!.count > 12{
            
            return false
        }
        
        return true
    }
}


