//
//  AddServiceVC.swift
//  ShopByUber
//
//  Created by Administrator on 5/25/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

protocol reloadServiceTableDelegate: class {
    func reloadServiceTable()
}

class AddServiceVC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,PECropViewControllerDelegate, PassBankTextfield, PassChequeTextfield {
    
    
    //MARK: - IBOutlet
    //UIView
    @IBOutlet var navigationBar: UIView!
    @IBOutlet var vwCategory: UIView!
    @IBOutlet weak var vwSubCatagory: UIView!
    @IBOutlet var vwScreenName: UIView!
    @IBOutlet var vwDuration: UIView!
    @IBOutlet var vwDays: UIView!
    @IBOutlet var vwTimes: UIView!
    @IBOutlet var vwFee: UIView!
    @IBOutlet var vwOfferDetails: UIView!
    @IBOutlet var vwCalender: FSCalendar!
    @IBOutlet var vwCalenderBG: UIView!
    @IBOutlet var vwOfferDetailsHeight: NSLayoutConstraint!
    @IBOutlet var vwInputImage: UIView!
    @IBOutlet var imgProviderHeight: NSLayoutConstraint!
    
    @IBOutlet weak var btnMenu: UIButton!
    
    @IBOutlet var lblMonthName: UILabel!
    @IBOutlet var txtCategory: UITextField!
    @IBOutlet weak var txtSubCategory: UITextField!
    @IBOutlet var txtTimes: UITextField!
    @IBOutlet var txtDays: UITextField!
    @IBOutlet var txtDuration: UITextField!
    @IBOutlet var imgServiceProvider: UIImageView!
    @IBOutlet var txtScreenName: UITextField!
    @IBOutlet var txtOfferDetails: UITextView!
    @IBOutlet var txtFlatFee: UITextField!
    @IBOutlet weak var txtPaymentOptions: UITextField!
    @IBOutlet weak var txtKeywords: UITextField!
    
    @IBOutlet weak var vwChangePicture: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var btnDelete_Outlet: UIButton!
    @IBOutlet weak var tblDropDown: UITableView!
    @IBOutlet weak var nslcTblDropDownHeight: NSLayoutConstraint!
    
    //MARK: - Variables
    var handlerNewAdded:((Void)->())?
    var imagePicker = UIImagePickerController()
    
    var arrEvents = [String]()
    
    var arrCategoryName = [String]()
    var arrCategoryID = [String]()
    
    var arrHoursList : [String] = []
    var arrOneHour : [String] = []
    var arrTwoHour : [String] = []
    var arrThreeHour : [String] = []
    var arrFourHour : [String] = []
    
    var arrAvailableDays : [String] = []
    var arrAllocatedDates = [AllocatedDates]()
    var arrCategoryList : NSArray = []
    var arr_SubCategories = [SubCategory]()  //
    
    var arrAvailableSchedule = [MonthOccupiedData]()
    var month : Int = 0
    var year : Int = 0
    var timeDuration : Int = 0
    var arrDays = [String]()
    var arrTimesSlots = [String]()
    var arrSpecificDateApp = [GetAppointmentOfSpecificDate]()
    
    let dropDown = DropDown()
    var streetAddress = String()
    var paymentData = [String:Any]()
    var userPaymentData : PaymentDetails?
    var objEditData : OccupiedData?
    var latitude = 0.0
    var longitude = 0.0
    var zipcode = ""
    
    //Selected Popup Data Variables
    var selectedIndexDuration = -1
    var selectedIndexDays = -1
    var selectedIndexCategory = -1
    var selectedIndexSubCategory = -1
    var arrSelectedIndexTimes = [Int]()
    var arrSelectedIndexDays = [Int]()
    
    var arrAlreadySelectedIndexDays = [Int]()
    var arrSavedAlredySelectedIndexDays = [[String:Any]]()
    
    var arrSelectedTimeSlots : [String] = []
    var selectedCategoryID = ""
    var selectedSubCatagoryID = ""
    var selectedDate = ""
    var shouldRelaodData = false
    
    var isFromEditService = false
    var editServiceID : String = ""
    var isFromRegistration = false
    var isImagePicked:Bool = false
    var isEditable:Bool = false
    var selectedIndex = 0
    var selectedAppointmentDate = String()
    var arrSubCatagoriesNames = [String]()
    var categoryID = String()
    var noSubCatMsg = String()
    var requestID = String()
    var isFromWantedService:Bool = false
    var subCatID = String()
    
    var delegate: reloadServiceTableDelegate?
    
    //MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("RequestId-------------->", requestID)
        self.tblDropDown.register(UINib(nibName: "PaypalPaymentCell", bundle: nil), forCellReuseIdentifier: "PaypalPaymentCell")
        self.tblDropDown.register(UINib(nibName: "BankPaymentCell", bundle: nil), forCellReuseIdentifier: "BankPaymentCell")
        self.tblDropDown.register(UINib(nibName: "CheckPaymentCell", bundle: nil), forCellReuseIdentifier: "CheckPaymentCell")
        
        tblDropDown.dataSource = self
        tblDropDown.delegate = self
        tblDropDown.reloadData()
        tblDropDown.layoutIfNeeded()
        
        
        btnDelete_Outlet.isHidden = true
        self.initialConfig()
        
        // Get screen occupie data
        if isFromEditService {
            //Pushed from Edit Service
            self.getEditServiceData(success:{(success) in })
        }
        else if isFromWantedService {
            //Get Wanted Service Data
            
            self.showAlertWithYESNOAction(title: "", message: "Are you sure want to create a Service to accommodate this Buyer request?", buttonTitle: "NO", buttonTitle2: "YES", onNOClick: {
                
                self.navigationController?.popViewController(animated: true)
            }) {
                
                self.showAlertWithYESNOAction(title: "", message: "Please populate all fields on this screen to create a Service for this Buyer. Once you create the Service, a notification will be sent out to the Buyer to contact you.", buttonTitle: "NO", buttonTitle2: "YES", onNOClick: {
                    self.navigationController?.popViewController(animated: true)
                }, onYESClick: {
                    self.dismiss(animated: true, completion: nil)
                })
            }
            
            self.getWantedList(showLoader: true)
        }
        else{
            //Pushed from Add Service
            getScreenOccupiedData(success:{(success) in })
        }
        
        if isFromRegistration
        {
            btnMenu.isHidden = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        imagePicker.delegate = self
    }
    
    override func viewDidLayoutSubviews() {
        makeTopCornerRedius()
    }
    //MARK: - Get Category List API call
    
    func getCategoryList() {
        
        KVNProgress.show()
        
        OccupiedData.callGetCategoriesListAPI( success: { (dict) in
            
            KVNProgress.dismiss()
            if !self.isFromWantedService {
                if  dict?["isServiceCreated"] as? Bool == false {
                    Utilities.showAlertView(title: "", message: "Please create a Service to offer to your Buyers!  We cannot add you as Vendor if you have not created a Service. Thank you!")
                }
            }
            self.arrCategoryList = dict?["Categories"] as! NSArray
            self.arrCategoryName = self.arrCategoryList.value(forKey: "catName") as! [String]
            self.arrCategoryID = self.arrCategoryList.value(forKey: "_id") as! [String]
            self.setCategory()
        }) {
            
            if self.arrCategoryList.count == 0{
                
            }
        }
    }
    
    //MARK: -  Get Screen Occupied Data API call
    
    func getScreenOccupiedData(success:@escaping(Bool)->()) {
        
        KVNProgress.show()
        var dict = [String:Any]()
        dict["userId"] = DataModel.sharedInstance.onFetchUserId()
        dict["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
        dict["month"] = month - 1
        dict["year"] = year
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kgetScreenOccupiedData, inputParams: dict, success: { (dict) in
            
            KVNProgress.dismiss()
            
            //Organize all occupied data based on model
            
            if dict?["Result"] as? Bool == true {
                
                let data = dict?["Data"] as? [String:Any]
                self.objEditData = OccupiedData.init(dict: data!) as OccupiedData
                
                print(self.objEditData)
                
                self.objEditData?.objServiceDetail?.paymentMethod = data?["paymentMethod"] as! Int
                self.objEditData?.objServiceDetail?.paypalEmail = (data?["paypalEmail"] as! String)
                
                // Set Payment data
                let paymentData = data?["paymentDetail"] as? [String:Any] ?? [:]
                self.userPaymentData = PaymentDetails.init(dict: paymentData)
                self.setPaymentData()
                
                // Set available timeslot
                self.setHourList(timeslots: self.objEditData?.timeslot as! [TimeSlotList])
                
                // Set available days
                self.setAvailableDay(days: self.objEditData?.availableDays as! [AvailableDays])
                
                self.arrDays.removeAll()
                for dayIndex in self.arrSelectedIndexDays
                {
                    self.arrDays.append(Constant.kArrDays[dayIndex])
                }
                
                let arrTimeSlots = self.objEditData?.objTimeSlotList
                //Reset timeslot
                self.arrTimesSlots.removeAll()
                self.arrSelectedIndexTimes.removeAll()
                self.arrSelectedTimeSlots.removeAll()
                //Set Timeslot
                for (index,elment) in (arrTimeSlots?.enumerated())! {
                    
                    let objTimeSlotDetail = elment
                    
                    self.arrTimesSlots.append(objTimeSlotDetail.time!)
                    
                    if objTimeSlotDetail.allocated! {
                        
                        self.arrSelectedIndexTimes.append(index)
                        self.arrSelectedTimeSlots.append(objTimeSlotDetail.time!)
                    }
                }
                
                success(true)
                self.getCategoryList()
            }
                
            else {
                
                let msg = dict?["Message"] as? String
                Utilities.showAlertView(title: "", message: msg)
                
            }
            
        }) { error in
            
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
        }
    }
    
    //MARK: - Get Screen Occupied Data API Call For Edit Service
    func getEditServiceData(success:@escaping(Bool)->()) {
        KVNProgress.show()
        var dict = [String:Any]()
        dict["userId"] = DataModel.sharedInstance.onFetchUserId()
        dict["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
        dict["serviceId"] = editServiceID
        dict["month"] = month - 1
        dict["year"] = year
        
        //  OccupiedData.getDataEditService(dictParam: dict, success: { (dict) in
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kgetDataEditService, inputParams: dict, success: { (response) in
            
            KVNProgress.dismiss()
            //Organize all occupied data based on model
            
            if response?["Result"] as? Bool == true {
                
                let dict = response?["Data"] as? [String:Any]
                self.objEditData = OccupiedData.init(dict: dict!) as OccupiedData
                
                print(self.objEditData)

                // Get whole edit object
                self.selectedSubCatagoryID = self.objEditData?.objServiceDetail?.subCatagoryId ?? ""
                // Set available timeslot
                self.setHourList(timeslots: self.objEditData?.timeslot as! [TimeSlotList])
                
                self.objEditData?.objServiceDetail?.paymentMethod = dict?["paymentMethod"] as! Int
                self.objEditData?.objServiceDetail?.paypalEmail = (dict?["paypalEmail"] as! String)
                
                // Set Payment data
                let paymentData = dict?["paymentDetail"] as? [String:Any] ?? [:]
                self.userPaymentData = PaymentDetails.init(dict: paymentData)
                self.setPaymentData()
                
                // Set available days
                self.setAvailableDay(days: self.objEditData?.availableDays as! [AvailableDays])
                self.arrSelectedIndexDays = (self.objEditData?.objServiceDetail?.availableDay)!
                
                self.arrDays.removeAll()
                for dayIndex in self.arrSelectedIndexDays
                {
                    self.arrDays.append(Constant.kArrDays[dayIndex])
                }
                
                let duration : Int = (self.objEditData!.objServiceDetail?.appointmentDuration)!
                self.timeDuration = duration
                self.selectedIndexDuration = duration - 1
                
                let arrTimeSlots = self.objEditData?.objTimeSlotList
                //Reset timeslot
                self.arrTimesSlots.removeAll()
                self.arrSelectedIndexTimes.removeAll()
                self.arrSelectedTimeSlots.removeAll()
                //Set Timeslot
                for (index,elment) in (arrTimeSlots?.enumerated())! {
                    
                    let objTimeSlotDetail = elment
                    
                    self.arrTimesSlots.append(objTimeSlotDetail.time!)
                    
                    if objTimeSlotDetail.allocated! {
                        
                        self.arrSelectedIndexTimes.append(index)
                        self.arrSelectedTimeSlots.append(objTimeSlotDetail.time!)
                    }
                }
                
                //Fill Selected information
                
                if self.selectedIndexDuration != -1
                {
                    self.txtDuration.text = self.arrHoursList[self.selectedIndexDuration]
                }
                else
                {
                    self.txtDuration.text = ""
                }
                
                self.txtDays.text = self.getDayNameInSortFormate(dayNames: self.arrDays)
                if self.arrSelectedTimeSlots.count > 0
                {
                    self.txtTimes.text = self.arrSelectedTimeSlots[0]
                }
                else
                {
                    self.txtTimes.text = ""
                }
                
                self.arrEvents = self.arrSelectedTimeSlots
                
                self.setupServiceData(objDetail: (self.objEditData?.objServiceDetail)!)
                self.arrAllocatedDates = (self.objEditData?.objServiceDetail?.dayScheduling)!
                
                //self.saveMonthOccupiedData()
                //Reload calender
                self.shouldRelaodData = false
                self.vwCalender.reloadData()
                print(self.objEditData)
                KVNProgress.dismiss()
                success(true)
                self.getCategoryList()
                
            }else {
                KVNProgress.dismiss()
                let msg = response?["Message"] as? String
                Utilities.showAlertView(title: "", message: msg)
                
            }
        }) { error in
            
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
        }
        
    }
    
    func setPaymentData(){
        
        dropDown.dataSource = ["Select your Payment Option","Paypal","Bank Account (ACH/EFT)", "Pay by Check"]
        if objEditData?.objServiceDetail?.paymentMethod == 1 {
            self.txtPaymentOptions.text      = "Paypal"
            self.nslcTblDropDownHeight.constant = Utilities.isDeviceiPad() ? 150 : 130
            self.selectedIndex = 1
           
            
        } else if objEditData?.objServiceDetail?.paymentMethod == 2 {
            self.txtPaymentOptions.text      = "Bank Account (ACH/EFT)"
            self.nslcTblDropDownHeight.constant = Utilities.isDeviceiPad() ? 400 : 325
            self.selectedIndex = 2
        }else if objEditData?.objServiceDetail?.paymentMethod == 3 {
            self.txtPaymentOptions.text      = "Pay by Check"
            self.nslcTblDropDownHeight.constant = Utilities.isDeviceiPad() ? 300 : 240
            self.selectedIndex = 3
        }else {
            self.txtPaymentOptions.text      = "Select your Payment Option"
            self.nslcTblDropDownHeight.constant = 0
            self.selectedIndex = 0
        }

    }
    
    //Set Hour List Base On Timeslots
    func setHourList(timeslots:[TimeSlotList])
    {
        // Reset timeslot
        self.arrHoursList.removeAll()
        self.arrOneHour.removeAll()
        self.arrTwoHour.removeAll()
        self.arrThreeHour.removeAll()
        self.arrFourHour.removeAll()
        
        for element in timeslots {
            
            let objTimeSlotList : TimeSlotList = element
            
            if objTimeSlotList.duration == 1 {
                
                self.arrHoursList.append(Constant.kArrayHour[0])
                for item in objTimeSlotList.timeslot {
                    
                    let objTimeSlotDetail = item as! TimeSlotDetail
                    self.arrOneHour.append(objTimeSlotDetail.time!)
                }
                
            }else if objTimeSlotList.duration == 2 {
                
                self.arrHoursList.append(Constant.kArrayHour[1])
                for item in objTimeSlotList.timeslot {
                    
                    let objTimeSlotDetail = item as! TimeSlotDetail
                    self.arrTwoHour.append(objTimeSlotDetail.time!)
                }
            }else if objTimeSlotList.duration == 3 {
                
                self.arrHoursList.append(Constant.kArrayHour[2])
                for item in objTimeSlotList.timeslot {
                    
                    let objTimeSlotDetail = item as! TimeSlotDetail
                    self.arrThreeHour.append(objTimeSlotDetail.time!)
                }
            }else if objTimeSlotList.duration == 4 {
                
                self.arrHoursList.append(Constant.kArrayHour[3])
                for item in objTimeSlotList.timeslot {
                    
                    let objTimeSlotDetail = item as! TimeSlotDetail
                    self.arrFourHour.append(objTimeSlotDetail.time!)
                }
            }
            
            
        }
        
    }
    
    //Set Available Day Base On Avaiable Days
    func setAvailableDay(days:[AvailableDays])
    {
        // Reset available days
        self.arrAvailableDays.removeAll()
        self.arrAlreadySelectedIndexDays.removeAll()
        
        for element in days {
            
            let objAvailableDays : AvailableDays = element
            
            if objAvailableDays.day == 0 {
                
                if objAvailableDays.isAllocate == true {
                    self.arrAlreadySelectedIndexDays.append(0)
                }
                self.arrAvailableDays.append(Constant.kArrDays[0])
                
                
            }else if objAvailableDays.day == 1 {
                
                if objAvailableDays.isAllocate == true {
                    
                    self.arrAlreadySelectedIndexDays.append(1)
                }
                self.arrAvailableDays.append(Constant.kArrDays[1])
                
            }else if objAvailableDays.day == 2 {
                
                if objAvailableDays.isAllocate == true {
                    
                    self.arrAlreadySelectedIndexDays.append(2)
                }
                self.arrAvailableDays.append(Constant.kArrDays[2])
                
            }else if objAvailableDays.day == 3 {
                
                if objAvailableDays.isAllocate == true {
                    
                    self.arrAlreadySelectedIndexDays.append(3)
                }
                self.arrAvailableDays.append(Constant.kArrDays[3])
                
            }else if objAvailableDays.day == 4 {
                
                if objAvailableDays.isAllocate == true {
                    
                    self.arrAlreadySelectedIndexDays.append(4)
                    
                }
                self.arrAvailableDays.append(Constant.kArrDays[4])
                
            }else if objAvailableDays.day == 5 {
                
                if objAvailableDays.isAllocate == true {
                    
                    self.arrAlreadySelectedIndexDays.append(5)
                    
                }
                self.arrAvailableDays.append(Constant.kArrDays[5])
                
            }else if objAvailableDays.day == 6 {
                
                if objAvailableDays.isAllocate == true {
                    
                    self.arrAlreadySelectedIndexDays.append(6)
                    
                }
                self.arrAvailableDays.append(Constant.kArrDays[6])
            }
            
        }
        
    }
    
    //Set Service Information
    func setupServiceData(objDetail:ServiceDetail)
    {
        self.imgServiceProvider.sd_setImage(with:URL(string: objDetail.serviceImage ?? ""), completed: { (image, err, nil, url) in
            self.imgServiceProvider.image = image
            self.vwInputImage.isHidden = true
            self.imgProviderHeight.constant = Utilities.isDeviceiPad() ? 480 : 250
            self.vwChangePicture.isHidden = false
            self.imgServiceProvider.isUserInteractionEnabled = true
        })
        
        self.txtFlatFee.text = objDetail.flatFee
        self.txtOfferDetails.text = objDetail.serviceDetail
        self.txtKeywords.text = objDetail.keywords
        self.txtOfferDetails.text = objDetail.serviceDetail
        let contentSize = self.txtOfferDetails.sizeThatFits(self.txtOfferDetails.bounds.size)
        let defaultHeight:CGFloat = Utilities.isDeviceiPad() ? 120 : 70
        
        if contentSize.height > defaultHeight
        {
            self.vwOfferDetailsHeight.constant = contentSize.height + 30
        }
        else
        {
            self.vwOfferDetailsHeight.constant = defaultHeight
        }
        
        
        // Saparate service detail
        self.selectedCategoryID = (objDetail.categoryId)!
        self.setCategory()
        self.txtScreenName.text = objDetail.screenName
        self.lblTitle.text = objDetail.screenName
        
    }
    
    //Set Selected Category
    func setCategory()
    {
        for (index,element) in self.arrCategoryList.enumerated() {
            
            let dic = element as! NSDictionary
            
            categoryID = dic.value(forKey: "_id") as! String
            if categoryID == self.selectedCategoryID {
                
                self.txtCategory.text = dic.value(forKey: "catName") as? String
                self.selectedIndexCategory = index
                print(index)
                self.setSubCategory()
            }
        }
    }
    
    func setSubCategory() {
        var catID = String()
        if isFromWantedService {
            catID = categoryID
        }else {
            catID = (self.objEditData?.objServiceDetail?.categoryId)!
        }

        
        SubCategory.getSubSubcategories(categoryId: catID, allSubcategoryRequired: false, success: { (subcatData) in
            
            self.arr_SubCategories.removeAll()
            self.arr_SubCategories.append(contentsOf: subcatData)
            self.arrSubCatagoriesNames = self.arr_SubCategories.map({ $0.subCatName! })
            
            var subcatagoryID = String()
            if self.isFromWantedService {
                subcatagoryID = self.subCatID
            }else {
                subcatagoryID = (self.objEditData?.objServiceDetail?.subCatagoryId)!
            }
            for (index,elements) in subcatData.enumerated() {
                
                let dic = elements
                
                let subid = dic.value(forKey: "id") as? String
                if subcatagoryID == subid {
                    self.txtSubCategory.text = dic.subCatName
                    self.selectedIndexSubCategory = index
                }
            }
            

        }, failure: {_ in
            print("Api Call Fail")
        })
    }
    //MARK: -  IBAction
    
    @IBAction func btnBack_Clicked(_ sender: UIButton) {
        
        if isFromWantedService{
            appDeletgate.onSetupDashboardPage()
        }
        else{
            _ = navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func vwBG_Tapped(_ sender: UITapGestureRecognizer) {
        
        self.view.endEditing(true)
        
    }
    
    //Click On Category For Change And Set
    @IBAction func vwCategory_Clicked(_ sender: UITapGestureRecognizer) {
        
        print("Selected Index",self.selectedIndexCategory)
        self.view.showRadioBoxPopup(topHeading: Constant.PopupTitle.kSelectCategory, defaultSelectedIndex: selectedIndexCategory, arrData: arrCategoryName) { (selectedText, selectedIndex) in
            
            if selectedIndex == self.selectedIndexCategory {
                
            }else {
                self.txtSubCategory.text = ""
                self.arrSubCatagoriesNames.removeAll()
                self.selectedIndexSubCategory = 0
            }
            
            self.txtCategory.text = selectedText
            self.selectedIndexCategory = selectedIndex
            let dic = self.arrCategoryList.object(at: selectedIndex) as! NSDictionary
            self.selectedCategoryID = dic.value(forKey: "_id") as! String
            
            
            SubCategory.getSubSubcategories(categoryId: self.selectedCategoryID,isfromService: true, allSubcategoryRequired: false, success: { (sub_Cat) in
               
                self.arr_SubCategories.removeAll()
                self.arr_SubCategories.append(contentsOf: sub_Cat)
                self.arrSubCatagoriesNames = self.arr_SubCategories.map({ $0.subCatName! })
             
                
            }, failure: {(mess) in
                print("API call Error")
                self.noSubCatMsg = mess ?? "No subcategories in the selected category!"
                print(mess!)
            })
        }
    }
    
    @IBAction func vwSubCatagory_Clicked(_ sender: Any) {
        
        if OccupiedData.isValidCatagory(category: txtCategory.text) {
        
            //  Show Sub Category pop up
            self.view.showRadioBoxPopup(topHeading: Constant.PopupTitle.kSelectSubCategory, defaultSelectedIndex: self.selectedIndexSubCategory, arrData: self.arrSubCatagoriesNames, missingData: self.noSubCatMsg /*"No subcategories in the selected category!"*/) { (selectedText, selectedIndex) in
                
                    self.txtSubCategory.text = selectedText
                    self.selectedIndexSubCategory = selectedIndex
                    self.selectedSubCatagoryID = self.arr_SubCategories[selectedIndex].id!
                }
        }
    }           
    
    //Click On Duration For Change And Set
    @IBAction func vwDuration_Clicked(_ sender: UITapGestureRecognizer) {
        
        if !self.isFromEditService || (self.isEditable && self.isFromEditService) || self.isFromWantedService {
            
            //Show choose duration popup
            self.view.showRadioBoxPopup(topHeading: Constant.PopupTitle.kSelectDuration, defaultSelectedIndex: selectedIndexDuration, arrData: arrHoursList) { (selectedText, selectedIndex) in
                
                if self.isFromEditService || self.isFromWantedService{
                    
                    if self.selectedIndexDuration != selectedIndex
                    {
                        if (self.selectedIndexDays != -1 || self.arrSelectedIndexTimes.count > 0)
                        {
                            self.showAlertWithButtonAction(title: Constant.Message.kDurationChangeTitle, message: Constant.Message.kDurationChange, buttonTitle: "Yes", buttonTitle2: "Cancel", onOKClick: {
                                self.setDuration(duration: selectedText, durationIndex: selectedIndex)
                            }, onCancelClick: {
                                
                            })
                        }
                        else
                        {
                            self.setDuration(duration: selectedText, durationIndex: selectedIndex)
                        }
                    }
                    else{
                        
                    }
                    
                }
                else
                {
                    self.setDuration(duration: selectedText, durationIndex: selectedIndex)
                }
            }
            
        }else {
            //Dont Show choose duration popup
            Utilities.showAlertView(title: "", message: Constant.Message.KServiceNotEditable)
        }
    }
    
    
    //Set Duration
    func setDuration(duration:String,durationIndex:Int)
    {
        self.txtDuration.text = duration
        self.selectedIndexDuration = durationIndex
        self.arrSelectedIndexTimes = [Int]()
        self.arrTimesSlots.removeAll()
        self.arrDays.removeAll()
        self.txtDays.text = nil
        self.arrSelectedIndexDays.removeAll()
        
        switch durationIndex {
        case 0:
            self.txtTimes.text = ""
            self.timeDuration = 1
            self.arrTimesSlots = self.arrOneHour
        case 1:
            self.timeDuration = 2
            self.txtTimes.text = ""
            self.arrTimesSlots = self.arrTwoHour
        case 2:
            self.timeDuration = 3
            self.txtTimes.text = ""
            self.arrTimesSlots = self.arrThreeHour
        case 3:
            self.timeDuration = 4
            self.txtTimes.text = ""
            self.arrTimesSlots = self.arrFourHour
        default:
            self.txtTimes.text = ""
            self.timeDuration = 0
            self.arrTimesSlots = self.arrOneHour
        }
        
        self.arrEvents.removeAll()
        self.arrSelectedTimeSlots.removeAll()
        self.removeTimeslot()
        self.shouldRelaodData = true
        self.vwCalender.reloadData()
    }
    
    //Click On Day For Change And Set
    @IBAction func vwDays_Clicked(_ sender: UITapGestureRecognizer) {
        
        if !self.isFromEditService || (self.isEditable && self.isFromEditService) || self.isFromWantedService {
           
            if selectedIndexDuration == -1 {
                
                Utilities.showAlertView(title: "", message: Constant.ValidationMessage.kSelectDuration)
            }else{
                
                // Show day selection popup
                self.view.showCheckBoxPopup(topHeading: Constant.PopupTitle.kSelectDay, arr_DefaultSelectedIndex: arrSelectedIndexDays,arr_AlreadySelectedIndex: arrAlreadySelectedIndexDays, arrData: arrAvailableDays) { (arr_SelectedText, arr_SelectedIndex) in
                    self.arrDays.removeAll()
                    
                    self.arrSelectedIndexDays = arr_SelectedIndex
                    self.arrDays = self.sortDayname(weekdays: arr_SelectedText)
                    
                    if arr_SelectedText.count == 0 {
                        self.txtDays.text = ""
                    }else{
                        self.txtDays.text = self.getDayNameInSortFormate(dayNames: self.arrDays)
                    }
                    
                    self.shouldRelaodData = true
                    self.arrAllocatedDates.removeAll()
                    self.vwCalender.reloadData()
                }
            }
        }
        else
        {
            //Dont Show Pop Up
            Utilities.showAlertView(title: "", message: Constant.Message.KServiceNotEditable)
        }
    }
    
    // Dayname Sort And Display In 2 Character
    func sortDayname(weekdays:[String])->[String]
    {
        var days = [Int]()
        
        for day in weekdays {
            
            let weekdayString = Constant.kArrDays.index(of: day)
            
            let dayInt = Int(weekdayString!)
            
            days.append(dayInt)
            
        }
        days.sort()
        
        var arrSorted=[String]()
        for dayIndex in days
        {
            arrSorted.append(Constant.kArrDays[dayIndex])
        }
        return arrSorted
    }
    
    func getDayNameInSortFormate(dayNames:[String])->String
    {
        if dayNames.count == 1
        {
            return dayNames[0]
        }
        var sort=[String]()
        
        for day in dayNames {
            sort.append(String(day.substring(to:day.index(day.startIndex, offsetBy: 2))))
        }
        return sort.joined(separator: ", ")
    }
    
    //Click On Timeslot For Change And Set
    @IBAction func vwTimes_Clicked(_ sender: UITapGestureRecognizer) {

        if !self.isFromEditService || (self.isEditable && self.isFromEditService) || self.isFromWantedService {
            
            if selectedIndexDuration == -1 {
                Utilities.showAlertView(title: "", message: Constant.ValidationMessage.kSelectDuration)
            }
            else if arrSelectedIndexDays.count == 0 {
                
                Utilities.showAlertView(title: "", message: Constant.ValidationMessage.kSelectDay)
            }
            else
            {
                //Show TimeSlot Pop Up
                self.view.showCheckBoxPopup(topHeading: Constant.PopupTitle.kSelectTime, arr_DefaultSelectedIndex: arrSelectedIndexTimes,arr_AlreadySelectedIndex: [], arrData: self.arrTimesSlots) { (arr_SelectedText, arr_SelectedIndex) in
                    
                    
                    if arr_SelectedText.count == 0 {
                        
                        self.txtTimes.text = ""
                        self.arrSelectedTimeSlots.removeAll()
                        self.arrSelectedIndexTimes.removeAll()
                        self.arrEvents.removeAll()
                    }else{
                        
                        self.txtTimes.text = arr_SelectedText[0]
                        self.arrEvents = arr_SelectedText
                        self.arrSelectedTimeSlots = arr_SelectedText
                        self.arrSelectedIndexTimes = arr_SelectedIndex
                    }
                    
                    self.shouldRelaodData = true
                    self.removeTimeslot()
                    self.vwCalender.reloadData()
                }
                
            }
        }
        else
        {
            //Dont Show Pop Up
            Utilities.showAlertView(title: "", message: Constant.Message.KServiceNotEditable)
        }
    }
    
    
    @IBAction func imgServiceProvider_Clicked(_ sender: UITapGestureRecognizer) {
        if self.imgServiceProvider.image == nil
        {
            openImagePicker()
        }
        
    }
    
    @IBAction func btnChangePicture_Tapped(_ sender: Any) {
        
        openImagePicker()
    }
    
    //MARK : - Open Imag
    @IBAction func btnSubmit_Clicked(_ sender: UIButton) {
        
        if OccupiedData.isValidServiceData(imageServiceProvide: imgServiceProvider.image,category: txtCategory.text,subcategory:txtSubCategory.text, screenName: txtScreenName.text, duration: txtDuration.text, days: txtDays.text, time: txtTimes.text, flatFee: txtFlatFee.text, offerDetail: txtOfferDetails.text,keyword: txtKeywords.text, objCount: arrAvailableSchedule.count) {
            
            if self.selectedIndex  == 0 {
                callUploadImage()
            }
            else if self.selectedIndex  == 1 {
                let cell = tblDropDown.cellForRow(at: IndexPath(row: 0, section: 0)) as? PaypalPaymentCell
                if  (cell?.isValidServiceData(email: cell?.txtPaypalEmail.text))!{
                    
                    callUploadImage()
                }
            }
            else if self.selectedIndex  == 2 {
                
                let cell = tblDropDown.cellForRow(at: IndexPath(row: 0, section: 0)) as? BankPaymentCell
                if (cell?.isValidServiceData(firstName: cell?.txtFirstName.text, lastName: cell?.txtLastName.text, streetName: cell?.txtStreetName.text, city: cell?.txtCity.text, state: cell?.txtState.text, zipcode: cell?.txtZipCode.text, routingNumber: cell?.txtRoutingNumber.text, accountNumber: cell?.txtAccNumber.text))! {
                    
                    callUploadImage()
                }
            }
            else {
                let cell = tblDropDown.cellForRow(at: IndexPath(row: 0, section: 0)) as? CheckPaymentCell
                if (cell?.isValidServiceData(firstName: cell?.txtFirstName.text, lastName: cell?.txtLastName.text, streetName: cell?.txtStreet.text, city: cell?.txtCity.text, state: cell?.txtState.text, zipcode: cell?.txtZipCode.text))! {
                    
                    self.callUploadImage()
                }
            }
      
        }
    }
    
    //Changes made by Semein - Delete Call Service - 27/10/17
    @IBAction func btnDelete_Click(_ sender: Any) {
        
        let rootView = UIApplication.shared.delegate?.window??.rootViewController as? UINavigationController
        
        let topView = UIApplication.topViewController(base: rootView)
        let alert = UIAlertController(title: "Are you Sure?", message: "Do you want to delete this service?", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction (title: "Yes", style: .default, handler: { (alert) in
            self.callDeleteService()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        topView?.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnPaymentDropDown_Click(_ sender: Any) {
        
        dropDown.anchorView = txtPaymentOptions
        dropDown.direction = .top
        dropDown.dataSource = ["Select your Payment Option","Paypal","Bank Account (ACH/EFT)", "Pay by Check"]
        dropDown.width = txtPaymentOptions.frame.size.width
        
        
        dropDown.selectionAction = {(index: Int, item: String) in
            
            let message: String
            if index == 0 {
                message = "Are you Sure? If you remove your payment option here, it will update on all services you offer to your buyers."
            }
            else {
                message = "Are you Sure? If you change your payment option here, it will update on all services you offer to your buyers."
            }
            
            if self.selectedIndex != index {
                
                if self.objEditData?.objServiceDetail?.paymentMethod == 0 || self.selectedIndex == 0 {
                    if index == 0
                    {
                        //select payment option
                        self.txtPaymentOptions.text = "Select your Payment Option"
                        self.nslcTblDropDownHeight.constant = 0
                        self.selectedIndex = 0
                    }else if index == 1
                    {
                        //Paypal
                        self.txtPaymentOptions.text = "Paypal"
                        self.nslcTblDropDownHeight.constant = Utilities.isDeviceiPad() ? 150 : 130
                        self.tblDropDown.reloadData()
                        self.selectedIndex = 1
                    }else if index == 2
                    {
                        //Bank
                        self.txtPaymentOptions.text = "Bank Account (ACH/EFT)"
                        self.nslcTblDropDownHeight.constant = Utilities.isDeviceiPad() ? 400 : 325
                        self.tblDropDown.reloadData()
                        self.selectedIndex = 2
                    }else {
                        //Cheque
                        self.txtPaymentOptions.text = "Pay by Check"
                        self.nslcTblDropDownHeight.constant = Utilities.isDeviceiPad() ? 300 : 240
                        self.tblDropDown.reloadData()
                        self.selectedIndex = 3
                    }
                }
                else {
                    
                    let alert = UIAlertController(title: "Are you sure?", message: message, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action) in
                        
                        if index == 0
                        {
                            //select payment option
                            self.txtPaymentOptions.text = "Select your Payment Option"
                            self.nslcTblDropDownHeight.constant = 0
                            self.selectedIndex = 0
                        }else if index == 1
                        {
                            //Paypal
                            self.txtPaymentOptions.text = "Paypal"
                            self.nslcTblDropDownHeight.constant = Utilities.isDeviceiPad() ? 150 : 130
                            self.tblDropDown.reloadData()
                            self.selectedIndex = 1
                        }else if index == 2
                        {
                            //Bank
                            self.txtPaymentOptions.text = "Bank Account (ACH/EFT)"
                            self.nslcTblDropDownHeight.constant = Utilities.isDeviceiPad() ? 400 : 325
                            self.tblDropDown.reloadData()
                            self.selectedIndex = 2
                        }else {
                            //Cheque
                            self.txtPaymentOptions.text = "Pay by Check"
                            self.nslcTblDropDownHeight.constant = Utilities.isDeviceiPad() ? 300 : 240
                            self.tblDropDown.reloadData()
                            self.selectedIndex = 3
                        }
                        
                    }))
                    
                    alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (cancle) in
                        
                    }))
                    self.present(alert, animated: true)
                }
            }
            
            
        }
        dropDown.show()
        
    }
    //MARK:- Custome Delegate
    func userInputData(_ dic: [String:String]) {
        
        userPaymentData?.firstName = dic["firstname"] ?? ""
        userPaymentData?.lastName =  dic["lastname"]  ?? ""
        userPaymentData?.stateName =  dic["statename"] ?? ""
        userPaymentData?.cityName = dic["cityname"] ?? ""
        userPaymentData?.street = dic["street"] ?? ""
        userPaymentData?.zipCode = dic["zip"] ?? ""
        tblDropDown.reloadData()
    }
    
    func userInputBankData(_ dic: [String:String]) {
        
        userPaymentData?.firstName = dic["firstname"] ?? ""
        userPaymentData?.lastName =  dic["lastname"]  ?? ""
        userPaymentData?.stateName =  dic["statename"] ?? ""
        userPaymentData?.cityName = dic["cityname"] ?? ""
        userPaymentData?.street = dic["street"] ?? ""
        userPaymentData?.zipCode = dic["zip"] ?? ""
        userPaymentData?.routingNumber = dic["routing"] ?? ""
        userPaymentData?.accountNumber = dic["acc"] ?? ""
        tblDropDown.reloadData()
    }
    
    //MARK: - Upload image API Call
    
    func callUploadImage(){
        
        //Check current tast is for add new service or edit.
        //If edit service than check image is change or not, if image was chnaged than upload new image.
        if isFromEditService
        {
            if self.isImagePicked
            {
                uplodaImageStart()
            }
            else
            {
                self.callAddNewService(imageName: "")
            }
        }
        else
        {
            uplodaImageStart()
        }
        
    }
    
    func uplodaImageStart()
    {
        ServiceProfile.callUploadImageAPI(imageType: Constant.ImageType.ServiceImage, image: self.imgServiceProvider.image, success: { (name) in
            
            self.callAddNewService(imageName: name!)
            
        }) {
            
        }
    }
    
    //MARK: - Add new service API Call
    
    func getWantedList(showLoader:Bool)
    {
        MyWantedServices.getWantedDetailForAddNewService(requestID: requestID, isShowloader: true) { (dict) in
            
            KVNProgress.dismiss()
        
            self.txtCategory.text = dict["catName"] as? String
            self.txtScreenName.text = dict["serviceName"] as? String
            self.txtSubCategory.text = dict["subCatName"] as? String
            self.categoryID = (dict["categoryId"] as? String)!
            self.selectedCategoryID = (dict["categoryId"] as? String)!
            self.subCatID = (dict["subCategoryId"] as? String)!
            self.selectedSubCatagoryID = (dict["subCategoryId"] as? String)!
            self.txtOfferDetails.text = dict["serviceDescription"] as? String
            
            self.vwCategory.isUserInteractionEnabled = false
            self.vwScreenName.isUserInteractionEnabled = false
            self.vwSubCatagory.isUserInteractionEnabled = false
            self.vwOfferDetails.isUserInteractionEnabled = false
            self.vwChangePicture.isUserInteractionEnabled = false
            
            if dict["cateImage"] as? String != nil{

                self.imgServiceProvider.sd_setImage(with: URL.init(string: (dict["cateImage"] as? String)!), placeholderImage: UIImage(named:Constant.PlaceHolder.kPhoto), options: SDWebImageOptions.retryFailed, completed: nil)
                    self.vwInputImage.isHidden = true
                    self.imgProviderHeight.constant = Utilities.isDeviceiPad() ? 480 : 250
                    self.vwChangePicture.isHidden = false
                    self.imgServiceProvider.isUserInteractionEnabled = true

            }else{
                self.imgServiceProvider.image = UIImage.init(named: Constant.PlaceHolder.kPhoto)
            }
            //Get screen Occupied Data API Call
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.getScreenOccupiedData(success:{(success) in })
            }
            
        }
    }
    
    
    func callAddNewService(imageName : String)  {
        
        KVNProgress.show()
        saveMonthOccupiedData()
        
        arrAllocatedDates.removeAll()
        for (_,obj) in arrAvailableSchedule.enumerated()
        {
            arrAllocatedDates.append(contentsOf: obj.arrDayScheduling)
        }
        
        
        let arrDaySchedule = NSMutableArray()
        // Prepare timeslots for selected day
        for (_,element) in arrAllocatedDates.enumerated() {
            
            let objAllocateDate = element as AllocatedDates
            let dict = NSMutableDictionary()
            dict.setValue(objAllocateDate.date, forKey: "date")
            
            let arrTimeSlotDetail = NSMutableArray()
            for (_,element) in element.timeslot.enumerated() {
                
                let dic2 = NSMutableDictionary()
                let objTimeDetail = element as! TimeSlotDetail
                dic2.setValue(objTimeDetail.allocated, forKey: "allocated")
                dic2.setValue(objTimeDetail.time, forKey: "time")
                
                arrTimeSlotDetail.add(dic2)
            }
            
            dict.setValue(arrTimeSlotDetail, forKey: "timeslot")
            arrDaySchedule.add(dict)
        }
        
        
        
        var dict = [String:Any]()
        dict["userId"] = DataModel.sharedInstance.onFetchUserId()
        dict["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
        dict["screenName"] = txtScreenName.text?.trimmingCharacters(in: .whitespaces)
        dict["serviceDetail"] = txtOfferDetails.text?.trimmingCharacters(in: .whitespaces)
        dict["profileImage"] = imageName
        dict["flatFee"] = txtFlatFee.text?.trimmingCharacters(in: .whitespaces)
        dict["categoryId"] = selectedCategoryID
        dict["subCategoryId"] = selectedSubCatagoryID
        dict["serviceEditAble"] = self.isEditable
        dict["paymentMethod"] = self.selectedIndex
        dict["keywords"] = txtKeywords.text?.trimmingCharacters(in: .whitespaces)
        dict["requestId"] = requestID
       
        var payment_detail = [String : Any]()

        if selectedIndex == 0 {
                dict["paypalEmail"]         = ""
                dict["paymentDetail"]       = [:]
        }
        else if selectedIndex  == 1 {
            if let cell = tblDropDown.cellForRow(at: IndexPath(row: 0, section: 0)) as? PaypalPaymentCell {
                dict["paypalEmail"]         = cell.txtPaypalEmail.text ?? ""
                dict["paymentDetail"]       = [:]
            }
        }
        else if selectedIndex  == 2 {
            if let cell = tblDropDown.cellForRow(at: IndexPath(row: 0, section: 0)) as? BankPaymentCell {
                
                if cell.stateCode.isEmpty || cell.cityCode.isEmpty {
                    cell.stateCode = (userPaymentData?.state)!
                    cell.cityCode = (userPaymentData?.city)!
                }
                
                payment_detail["firstName"]         = cell.txtFirstName.text ?? ""
                payment_detail["lastName"]          = cell.txtLastName.text ?? ""
                payment_detail["streetAddress"]     = cell.txtStreetName.text ?? ""
                payment_detail["city"]              = cell.cityCode
                payment_detail["state"]             = cell.stateCode
                payment_detail["zipCode"]           = cell.txtZipCode.text ?? ""
                payment_detail["routingNumber"]     = cell.txtRoutingNumber.text ?? ""
                payment_detail["accountNumber"]     = cell.txtAccNumber.text ?? ""
                dict["paymentDetail"]               = payment_detail
                dict["paypalEmail"]                 = ""
                
            }
        }
        else if self.selectedIndex == 3 {
            if let cell = tblDropDown.cellForRow(at: IndexPath(row: 0, section: 0)) as? CheckPaymentCell {
                
                if cell.stateCode.isEmpty || cell.cityCode.isEmpty {
                    cell.stateCode = (userPaymentData?.state)!
                    cell.cityCode = (userPaymentData?.city)!
                }
                
                payment_detail["firstName"]         = cell.txtFirstName.text ?? ""
                payment_detail["lastName"]          = cell.txtLastName.text ?? ""
                payment_detail["streetAddress"]     = cell.txtStreet.text ?? ""
                payment_detail["city"]              = cell.cityCode
                payment_detail["state"]             = cell.stateCode
                payment_detail["zipCode"]           = cell.txtZipCode.text ?? ""
                payment_detail["routingNumber"]     = ""
                payment_detail["accountNumber"]     = ""
                dict["paymentDetail"]               = payment_detail
                dict["paypalEmail"]                 = ""
            }
        }
        
        
        var arrMonthData = [[String:Any]]()
        for (_,element) in arrAvailableSchedule.enumerated() {
            
            let objMonthData = element
            let dic = objMonthData.toDictionary()
            arrMonthData.append(dic)
        }
        dict["availableSchedule"] = arrMonthData
        
        dict["dayScheduling"] = arrDaySchedule
        
        var API:String
        //If current task is for edit service tahn API will be chnage and pass serviceid.
        if isFromEditService
        {
            API = Constant.serverAPI.URL.kUpdateService
            dict["serviceId"]=editServiceID
        }
        else
        {
            API = Constant.serverAPI.URL.kaddService
        }
        
        APIManager.callAPI(url: API, inputParams: dict, success: { (dict) in
            
            KVNProgress.dismiss()
            
            if dict?["Result"] as? Bool == true {
                
                print("Success")
                Utilities.showAlertWithAction(title: "", message: (dict?["Message"] as? String)!, buttonTitle: "OK", buttonTitle2: "", onOKClick: {
                    
                    if self.isFromWantedService{
                         appDeletgate.onSetupDashboardPage()
                    }
                    
                    if self.isFromRegistration
                    {
                        let sellerVC = SellerVC(nibName: "SellerVC", bundle: nil) as SellerVC
                        _ = self.navigationController?.pushViewController(sellerVC, animated: true)
                        
                    }
                    else
                    {
                        self.navigationController?.popViewController(animated: true)
                        if self.handlerNewAdded != nil
                        {
                            self.handlerNewAdded!()
                        }
                    }
                    
                    
                }, onCancelClick: {
                    
                })
                
            }
            else{
                
                let msg = dict?["Message"] as? String
                Utilities.showAlertView(title: "", message: msg)
            }
            
            
        }) { error in
            
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
        }
        
    }
    
    //MARK : Delete Service API
    
    func callDeleteService() {
        
        var dict = [String:Any]()
        dict["userId"] = DataModel.sharedInstance.onFetchUserId()
        dict["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
        dict["serviceId"] = editServiceID
        print("--->>>>>>",dict)
        var API:String
        API = Constant.serverAPI.URL.kDeleteService
        
        KVNProgress.show()
        APIManager.callAPI(url: API, inputParams: dict, success: { (dict) in
            
            KVNProgress.dismiss()
        
            //Reload Service Table Data in SellerVC
            self.delegate?.reloadServiceTable()
            
            if dict?["Result"] as? Bool == true {
                
                print("Success")
                //Reload service table in SellerVC
                func reloadServiceTable() {
                   let seller = SellerVC(nibName: "SellerVC", bundle: nil) as SellerVC
                    seller.tblServices.reloadData()
                }
                
                Utilities.showAlertWithAction(title: "", message: (dict?["Message"] as? String)!, buttonTitle: "OK", buttonTitle2: "", onOKClick: {
                    
                    self.navigationController?.popViewController(animated: true)

                }, onCancelClick: {
                    
                })
                
            }
            else{
                
                let msg = dict?["Message"] as? String
                Utilities.showAlertView(title: "", message: msg)
            }
            
            
        }) { error in
            
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
        }

    }
    
    func callgetAppointmentOfSpecificDate(completion: @escaping (() -> Void)) {
        self.arrSpecificDateApp = []
        var dict = [String:Any]()
        dict["userId"] = DataModel.sharedInstance.onFetchUserId()
        dict["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
        dict["serviceId"] = editServiceID
        dict["checkDate"] = selectedAppointmentDate
        print("--->>>>>>",dict)
        var API:String
        API = Constant.serverAPI.URL.kGetAppointmentOfSpecificDate
        
        KVNProgress.show()
        APIManager.callAPI(url: API, inputParams: dict, success: { (dict) in
            
            KVNProgress.dismiss()
            
            if dict?["Result"] as? Bool == true {
                
                let data = dict?["Data"] as? [[String:Any]] ?? []
                print(data)
                
                self.arrSpecificDateApp = data.map(GetAppointmentOfSpecificDate.init)
                print(self.arrSpecificDateApp)
                completion()
            }
            else{
                completion()
//                let msg = dict?["Message"] as? String
//                Utilities.showAlertView(title: "", message: msg)
            }
            
            
        }) { error in
            
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
        }
    }
    
    
    //MARK : Open image picker
    func openImagePicker() {
        
        let alert:UIAlertController = UIAlertController(title: "Choose Image", message: nil, preferredStyle: Utilities.isDeviceiPad() ? .alert : .actionSheet )
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default){
            UIAlertAction in
            self.openProfileUserCamera()
        }
        
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.openProfileUserGallary()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
        }
        
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    //MARK: - Image Delegate
    
    func openProfileUserGallary() {
        
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        //imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        imagePicker.mediaTypes = [kUTTypeImage as String]
        present(imagePicker, animated: true, completion: nil)
    }
    
    func openProfileUserCamera() {
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.allowsEditing = false
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.cameraCaptureMode = .photo
            imagePicker.modalPresentationStyle = .fullScreen
            present(imagePicker,animated: true,completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
//        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
//            self.imgServiceProvider.image = pickedImage
//            self.vwInputImage.isHidden = true
//            self.imgProviderHeight.constant = 250
//            self.vwChangePicture.isHidden = false
//            self.imgServiceProvider.isUserInteractionEnabled = true
//            self.isImagePicked = true
//        }
//        
        dismiss(animated:true, completion: nil)
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.openEditor(image: pickedImage)
        }

    }
    
    //MARK: - PECropViewController Delegate
    func openEditor(image: UIImage) {
        let controller = PECropViewController()
        controller.delegate = self
        controller.image = image
        let width: CGFloat? = image.size.width
        let height: CGFloat? = image.size.height
        let length: CGFloat = min(width!, height!)
        let ratioValue = width!/4
        
        controller.imageCropRect = CGRect(x: (width! - length) / 2, y: (height! - length) / 2, width: ratioValue*4, height: ratioValue*3)
        controller.keepingCropAspectRatio = true
        let navigationController = UINavigationController(rootViewController: controller as UIViewController )
        if UI_USER_INTERFACE_IDIOM() == .pad {
            navigationController.modalPresentationStyle = .formSheet
        }
        present(navigationController, animated: true) { _ in }
    }
    
    func cropViewController(_ controller: PECropViewController!, didFinishCroppingImage croppedImage: UIImage!) {
        controller.dismiss(animated: true) { _ in }
        self.imgServiceProvider.image = croppedImage
        self.vwInputImage.isHidden = true
        self.imgProviderHeight.constant = Utilities.isDeviceiPad() ? 480 : 250
        self.vwChangePicture.isHidden = false
        self.imgServiceProvider.isUserInteractionEnabled = true
        self.isImagePicked = true
        if UI_USER_INTERFACE_IDIOM() == .pad {
        }
        
    }
    
    func cropViewController(_ controller: PECropViewController, didFinishCroppingImage croppedImage: UIImage, transform: CGAffineTransform, cropRect: CGRect) {
        controller.dismiss(animated: true) { _ in }
        self.imgServiceProvider.image = croppedImage
        self.vwInputImage.isHidden = true
        self.imgProviderHeight.constant = Utilities.isDeviceiPad() ? 480 : 250
        self.vwChangePicture.isHidden = false
        self.imgServiceProvider.isUserInteractionEnabled = true
        self.isImagePicked = true
        if UI_USER_INTERFACE_IDIOM() == .pad {
        }
    }
    
    func cropViewControllerDidCancel(_ controller: PECropViewController) {
        self.isImagePicked = false
        if UI_USER_INTERFACE_IDIOM() == .pad {
            
        }
        controller.dismiss(animated: true) { _ in }
    }
    
    //MARK: - Private Methods
    func makeTopCornerRedius() {
        
        let size: CGFloat = Utilities.isDeviceiPad() ? 12 : 5
        let maskLayer = CAShapeLayer()
        maskLayer.path = UIBezierPath(roundedRect: vwChangePicture.bounds, byRoundingCorners: [.topLeft], cornerRadii: CGSize(width: size, height: size)).cgPath
        vwChangePicture.layer.mask = maskLayer
    }
    
    func initialConfig() {
        
        let currentCalender = Calendar.current
        month = currentCalender.component(.month, from: Date())
        year = currentCalender.component(.year, from: Date())
        
        Utilities.applyShadowEffect(view: [self.navigationBar,self.vwCategory,self.vwSubCatagory,self.vwScreenName,self.vwDays,self.vwTimes,self.vwFee,self.vwOfferDetails,self.vwDuration,self.vwCalenderBG])
        vwCalender.appearance.caseOptions = .weekdayUsesUpperCase
        vwCalender.firstWeekday = 2
        vwCalender.allowsSelection = true
        
        vwChangePicture.isHidden = true
        
        self.lblMonthName.text = CalenderUtility.getHeaderTextFromDate(date: vwCalender.currentPage)
        
        
        if isFromEditService
        {
            btnSubmit.setTitle("UPDATE THIS SERVICE", for: .normal)
            
            if self.isEditable {
                btnDelete_Outlet.isHidden = false
            }
        }
        
    }
    
    //MARK: Calender Delegates & Action
    
    @IBAction func btnPrevious_Clicked(_ sender: UIButton) {
            
            let currentCalender = Calendar.current
            let date = Calendar.current.date(byAdding: .month, value: -1, to: self.vwCalender.currentPage)
            let preMonth = currentCalender.component(.month, from: date!)
            let curMonth = currentCalender.component(.month, from: Date())
            //Prevent to set month lessthan current month
            if preMonth < curMonth
            {
                return
            }
            
            if (self.selectedIndexDays == -1 && self.selectedIndexDuration == -1 && self.arrSelectedIndexTimes.count == 0)
            {
                changeMonth(addingMonth: -1)
            }
            else{
                
                if self.selectedIndexDuration != -1 && arrSelectedIndexDays.count == 0
                {
                    self.showAlertWithButtonAction(title: Constant.Message.kChangeMonthTitle, message: Constant.Message.kChangeMonth, buttonTitle: "Yes", buttonTitle2: "Cancel", onOKClick: {
                        
                        self.changeMonth(addingMonth: -1)
                        
                    }, onCancelClick: {
                        
                    })
                }
                else if arrSelectedIndexDays.count != 0 && self.arrSelectedTimeSlots.count == 0
                {
                    self.showAlertWithButtonAction(title: Constant.Message.kChangeMonthTitle, message: Constant.Message.kChangeMonth, buttonTitle: "Yes", buttonTitle2: "Cancel", onOKClick: {
                        
                        self.changeMonth(addingMonth: -1)
                        
                    }, onCancelClick: {
                        
                    })
                }
                else
                {
                    changeMonth(addingMonth: -1)
                }
            }
    }
    
    @IBAction func btnNext_Clicked(_ sender: UIButton) {
            
            if (self.selectedIndexDays == -1 && self.selectedIndexDuration == -1 && self.arrSelectedIndexTimes.count == 0)
            {
                changeMonth(addingMonth: 1)
            }
            else{
                
                if self.selectedIndexDuration != -1 && arrSelectedIndexDays.count == 0
                {
                    self.showAlertWithButtonAction(title: Constant.Message.kChangeMonthTitle, message: Constant.Message.kChangeMonth, buttonTitle: "Yes", buttonTitle2: "Cancel", onOKClick: {
                        
                        self.changeMonth(addingMonth: 1)
                        
                    }, onCancelClick: {
                        
                    })
                }
                else if arrSelectedIndexDays.count != 0 && self.arrSelectedTimeSlots.count == 0
                {
                    self.showAlertWithButtonAction(title: Constant.Message.kChangeMonthTitle, message: Constant.Message.kChangeMonth, buttonTitle: "Yes", buttonTitle2: "Cancel", onOKClick: {
                        
                        self.changeMonth(addingMonth: 1)
                        
                    }, onCancelClick: {
                        
                    })
                }
                else
                {
                    changeMonth(addingMonth: 1)
                }
            }
    }

    
    func changeMonth(addingMonth : Int) {
        
        //If selected month all data are fill than it's store for later use.
        if self.arrSelectedIndexTimes.count > 0
        {
            self.saveMonthOccupiedData()
        }
        
        let currentCalender = Calendar.current
        let date = Calendar.current.date(byAdding: .month, value: addingMonth, to: self.vwCalender.currentPage)
        
        month = currentCalender.component(.month, from: date!)
        year = currentCalender.component(.year, from: date!)
        //Remove allocated dates timeslot for selected month(month base on addingMonth).
        self.removeTimeslot()
        
        //Check selected month(month base on addingMonth) data are available or not.
        //If data was available than set to prefill for selected month otherwise reset all field
        
        //Get screen occupie data base on service activity (edit or add new)
        
        _ = (arrAvailableSchedule as NSArray).indexOfObject(passingTest:) {obj, idx, stop in
            return (obj as! MonthOccupiedData).year == year && (obj as! MonthOccupiedData).month == (month - 1)
        }
        
       
        if isFromEditService {
           
            getEditServiceData(success: {(success) in
                    self.setPrefieldData(date: date!)
            })
        }else{
            getScreenOccupiedData(success: {(success) in
                self.setPrefieldData(date: date!)
            })
        }
        
       // }
        
    }
    
    func setPrefieldData(date:Date)
    {
        let arrAvailable = self.arrAvailableSchedule as NSArray
        let index = arrAvailable.indexOfObject(passingTest:) {constraint, idx, stop in
            let objOcc = constraint as! MonthOccupiedData
            return objOcc.year == year && objOcc.month == (month - 1)
        }
        
        if index != NSNotFound
        {
            let objSaved = arrAvailableSchedule[index]
            timeDuration = objSaved.duration
            arrSelectedIndexDays = objSaved.day
            arrSelectedTimeSlots = objSaved.timings
            arrAllocatedDates.removeAll()    //Changes as per
            arrAllocatedDates.append(contentsOf: objSaved.arrDayScheduling)
            
            // Set Duration
            self.selectedIndexDuration = timeDuration - 1
            if timeDuration == 0
            {
                self.txtDuration.text = nil
            }else{
                self.txtDuration.text = arrHoursList[self.selectedIndexDuration]
            }
            //Set Days
            var arrTDay = [String]()
            (arrSelectedIndexDays as NSArray).enumerateObjects({ (obj, idx, nil) in
                arrTDay.append(arrAvailableDays[obj as! Int])
            })
            self.arrDays = arrTDay
            self.txtDays.text = self.getDayNameInSortFormate(dayNames: self.arrDays)
            
            if timeDuration == 1{
                self.arrTimesSlots = self.arrOneHour
            }else if timeDuration == 2{
                self.arrTimesSlots = self.arrTwoHour
            }else if timeDuration == 3{
                self.arrTimesSlots = self.arrThreeHour
            }else if timeDuration == 4{
                self.arrTimesSlots = self.arrFourHour
            }
            
            //Set Timeslot
            var arrTSlotIndex = [Int]()
            (arrSelectedTimeSlots as NSArray).enumerateObjects({ (obj, idx, nil) in
                let index = (arrTimesSlots as NSArray).indexOfObject(passingTest:) {constraint, idx, stop in
                    return constraint as! String == obj as! String
                }
                
                if index != NSNotFound
                {
                    arrTSlotIndex.append(index)
                }
            })
            
            self.arrSelectedIndexTimes = arrTSlotIndex
            
            if self.arrSelectedTimeSlots.count > 0
            {
                self.txtTimes.text = self.arrSelectedTimeSlots[0]
            }
            else
            {
                self.txtTimes.text = ""
            }
            
            self.arrEvents = arrSelectedTimeSlots
            
            
            self.shouldRelaodData = false
            
            self.saveMonthOccupiedData()
            self.vwCalender.reloadData()
            
        }
        else
        {
            if !isFromEditService
            {
                self.timeDuration = 0
                self.selectedIndexDuration = -1
                self.arrSelectedIndexDays.removeAll()
                self.arrSelectedTimeSlots.removeAll()
                self.txtDuration.text = nil
                self.arrDays.removeAll()
                self.txtDays.text = nil
                self.arrSelectedIndexTimes.removeAll()
                self.txtTimes.text = nil
                self.arrEvents.removeAll()

            }
            
        }
        
        self.vwCalender.setCurrentPage(date, animated: true)
    }
    
    //MARK:- Remove timeslot of selected month
    func removeTimeslot(){
        
        let currentCalender = Calendar.current
        let indexs = (self.arrAllocatedDates as NSArray).indexesOfObjects(passingTest:) { (obj, idx, nil) -> Bool in
            let dateS = (obj as! AllocatedDates).date
            let cDate = NSDate.getDateFrom(formate:"dd-MM-yyyy" , dateString: dateS!)
            let cMonth = currentCalender.component(.month, from: cDate )
            let cYear = currentCalender.component(.year, from: cDate )
            
            if cYear == year && cMonth == month
            {
                //self.arrAllocatedDates.remove(at: idx)
                return true
            }
            return false
        }
        
        self.arrAllocatedDates.removeAtIndexes(indexes: indexs as NSIndexSet)
    }
    
    // MARK :- Save Single/All Month Data
    func saveMonthOccupiedData() {
        
        var monthDic = [String:Any]()
        
        monthDic["month"] = month - 1
        monthDic["year"] = year
        monthDic["duration"] = timeDuration
        monthDic["day"] = arrSelectedIndexDays
        monthDic["timings"] = arrSelectedTimeSlots
        monthDic["dayScheduling"] = arrAllocatedDates
        
        let objMonthData = MonthOccupiedData.init(fromDictionary: monthDic)
        
        //Check selected month data available or not.
        //If data are available than remove old data and set new.
        let index = (arrAvailableSchedule as NSArray).indexOfObject(passingTest:) {obj, idx, stop in
            return (obj as! MonthOccupiedData).year == objMonthData.year && (obj as! MonthOccupiedData).month == objMonthData.month
        }
        
        if index != NSNotFound
        {
            arrAvailableSchedule.remove(at: index)
        }
        arrAvailableSchedule.append(objMonthData)
    }
    
}

//MARK:- Calender Delegates
extension AddServiceVC : FSCalendarDelegate,FSCalendarDataSource
{
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        self.lblMonthName.text = CalenderUtility.getHeaderTextFromDate(date: calendar.currentPage)
    }
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        
        var event = 0
        let currentCalender = Calendar.current
        let today = calendar.today
        let month = currentCalender.component(.month, from: date)
        let year = currentCalender.component(.year, from: date)
        
       
        //Get Count based on Selected Week Day & Add dot on that day
        
        for (_,element) in arrDays.enumerated() {
            
            
            for dt in CalenderUtility.getNumberOfSpecificDaysInMonth(weekDay: CalenderUtility.getWeekDayFromWeekName(weekDayName: element), month: month, Year: year){
                
                if year != self.year
                {
                    continue
                }
                else if month != self.month
                {
                    continue
                }
                
                
                if dt == date {
                    
                    if shouldRelaodData {
                        
                        print(CalenderUtility.DateToString(date: dt))
                        event = self.arrEvents.count  > 4 ? 4 : self.arrEvents.count
                        print(event)
                        
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "dd-MM-yyyy"
                        let dateString = dateFormatter.string(from: date)
                        
                        let dict = NSMutableDictionary()
                        let currentDay = calendar.today!
                        
                        //Condition to check current date
                        if (date < currentDay) {
                            event = 0
                            continue
                        }
                        dict.setValue(dateString, forKey: "date")
                        var arrTimeSlots = [NSDictionary]()
                        
                        for (_, element) in arrEvents.enumerated() {
                            
                            let timeSlotDict = NSMutableDictionary()
                            timeSlotDict.setValue(element, forKey: "time")
                            timeSlotDict.setValue(true, forKey: "allocated")
                            
                            arrTimeSlots.append(timeSlotDict)
                            print(arrTimeSlots)
                        }
                        
                        dict.setValue(arrTimeSlots, forKey: "timeslot")
                        
                        let objAllocatedDates = AllocatedDates.init(dict: dict)
                        self.arrAllocatedDates.append(objAllocatedDates)
                   
                    }else{
                        
                        
                        let date2 = CalenderUtility.DateToString(date: date)
                        let currentDay = calendar.today!
                        
                        //Condition to check current date
                        if (date < currentDay) {
                            event = 0
                            continue
                        }

                        if date2 == selectedDate {
                            
                            print("Date found")
                            
                            
                            for (_,element) in self.arrAllocatedDates.enumerated() {
                                
                                let objAllocatedDates = element as AllocatedDates
                                
                                if (objAllocatedDates.date?.contains(selectedDate))! {
                                    print("date found")
                                    arrEvents.removeAll()
                                    for (_, element) in objAllocatedDates.timeslot.enumerated() {
                                        
                                        let timeSlotDetail = element as! TimeSlotDetail
                                        
                                        arrEvents.append(timeSlotDetail.time!)
                                    }
                                    event = arrEvents.count
                                    print(arrEvents)
                                }
                                
                            }
                            
                        }else{
                            
                            print("Date not found")
                            
                            
                            for (_,element) in self.arrAllocatedDates.enumerated() {
                                
                                let objAllocatedDates = element as AllocatedDates
                                
                                if (objAllocatedDates.date?.contains(date2))! {
                                    print("date found")
                                    arrEvents.removeAll()
                                    for (_, element) in objAllocatedDates.timeslot.enumerated() {
                                        
                                        let timeSlotDetail = element as! TimeSlotDetail
                                        
                                        arrEvents.append(timeSlotDetail.time!)
                                    }
                                    event = arrEvents.count
                                    print(arrEvents)
                                }
                                
                            }
                            
                        }
                        //event = self.arrEvents.count  > 4 ? 4 : self.arrEvents.count
                        print(event)
                        
                    }
                    
                    break
                }
                else{
                    
                    //event = 0
                    
                }
            }
        }
        print(event)
        return event
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition){
        
        //Convert date to
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let myString = formatter.string(from: date)
        let yourDate = formatter.date(from: myString)
        formatter.dateFormat = "yyyy-MM-dd"
        let myStringdt = formatter.string(from: yourDate!)
        selectedAppointmentDate = myStringdt
        print(selectedAppointmentDate)
        
        let cell = calendar.cell(for: date, at: .current)
        if cell.numberOfEvents == 0 {
            return
        }
        
        
        var isFound = false
       
        
        for (_,element) in arrDays.enumerated() {
            
            for dt in CalenderUtility.getNumberOfSpecificDaysInMonth(weekDay: CalenderUtility.getWeekDayFromWeekName(weekDayName: element), month: month, Year: year) {
                
                if dt == date{
                    isFound = true
                    selectedDate =  CalenderUtility.DateToString(date: date)
                    break
                }
            }
            
        }
        
        //If Events Dot Available
        
        if isFound {
            
            for (_,element) in self.arrAllocatedDates.enumerated() {
                
                let objAllocatedDates = element as AllocatedDates
                if objAllocatedDates.date == selectedDate {
                    
                    arrEvents.removeAll()
                    for (_, element) in objAllocatedDates.timeslot.enumerated() {
                        
                        let timeSlotDetail = element as! TimeSlotDetail
                        arrEvents.append(timeSlotDetail.time!)
                    }
                }
                
            }
            if arrEvents.count == 0
            {
                return
            }
            
            if isFromEditService == true {
                
                //Show Your Appointment PopUp
                self.callgetAppointmentOfSpecificDate()  {
                    //CallSelected date Appointment Data API
                    self.view.showAppoinmnetPopup(arrData:self.arrSpecificDateApp, onCloseClick: {
                        
                    })
                }
            }else {
                //Show Time Availability Popup
                AvailibilityPopUp()
            }
        }

        
    }
    
    //Time Availibility Pop up
    func AvailibilityPopUp() {
        
        if isFromEditService == false{
            isEditable = true
        }
        
        self.view.showEventPopup(topHeading: Constant.PopupTitle.kAvailability, arrData: self.arrEvents, allowDelete: self.isEditable, onSubmitClick: { (arrData) in
            print(self.arrEvents)
            print(arrData)
            
            for (_,element) in self.arrAllocatedDates.enumerated() {
                
                let objAllocatedDates = element as AllocatedDates
                
                if objAllocatedDates.date == self.selectedDate {
                    
                    objAllocatedDates.timeslot.removeAllObjects()
                    
                    let arrTimeSlots : NSMutableArray = []
                    
                    for (_, element) in arrData.enumerated() {
                        
                        let timeSlotDict = NSMutableDictionary()
                        timeSlotDict.setValue(element, forKey: "time")
                        timeSlotDict.setValue(true, forKey: "allocated")
                        
                        let objTimeSlotDetail = TimeSlotDetail.init(dict: timeSlotDict)
                        arrTimeSlots.add(objTimeSlotDetail)
                    }
                    objAllocatedDates.timeslot = arrTimeSlots
                }
            }
 
            var notAvailableCount = 0
            var slotIndex = 0
            var totalCount = 0
            
            for (index, _) in self.arrSelectedTimeSlots.enumerated() {
                notAvailableCount = 0
                totalCount = 0
                
                //print(self.arrAllocatedDates.count)
                for (_,element) in self.arrAllocatedDates.enumerated() {
                    
                    let objAllocatedDates = element as AllocatedDates
                    
                    //print(objAllocatedDates.timeslot.count)
                    for (_, element) in objAllocatedDates.timeslot.enumerated() {
                        
                        let objtimeSlotDetail =  element as! TimeSlotDetail
                        
                        //print(objtimeSlotDetail.time)
                        totalCount += 1
                        
                        if objtimeSlotDetail.time == self.arrSelectedTimeSlots[index] {
                            
                            print("found")
                        }else{
                            print("Not found")
                            slotIndex = index
                            notAvailableCount += 1
                        }
                    }
                }
                
                print(slotIndex)
                print(notAvailableCount)
                
                if totalCount == notAvailableCount {
                    
                    self.arrSelectedTimeSlots.remove(at: slotIndex)
                    self.arrSelectedIndexTimes.remove(at: slotIndex)
                    break
                }
                
            }
            if self.arrSelectedTimeSlots.count != 0 {
                self.txtTimes.text = self.arrSelectedTimeSlots[0]
            }
            else
            {
                self.txtTimes.text = ""
            }
            
            self.shouldRelaodData = false
            self.vwCalender.reloadData()
            print(self.arrAllocatedDates)
        })
    }
    
    func minimumDate(for calendar: FSCalendar) -> Date {
        return Date()
    }
    
}

//MARK:- TableView Delegate
extension AddServiceVC : UITableViewDelegate ,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if self.selectedIndex == 1
        {
            return Utilities.isDeviceiPad() ? 150 : 130      //paypal cell Height
        }
        else if self.selectedIndex == 2
        {
            return Utilities.isDeviceiPad() ? 400 : 325      //Bank Payment Cell Height
        }else
        {
            return Utilities.isDeviceiPad() ? 300 : 240      //Check Payment Cell Height
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if selectedIndex == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PaypalPaymentCell") as! PaypalPaymentCell
            
            //Get Paypal emailID
     
            cell.txtPaypalEmail.text = objEditData?.objServiceDetail?.paypalEmail
            
            return cell
        }
        else if selectedIndex == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BankPaymentCell") as! BankPaymentCell
            
            //Adding Tap Gesture to Present Google AutoComplet Address
//            let tapGesture = UITapGestureRecognizer(target: self, action:  #selector(AddressTapHandler))
//            cell.vwAddress.addGestureRecognizer(tapGesture)
            
            //Get Bank Payment Cell Data
            cell.txtFirstName.text = userPaymentData?.firstName
            cell.txtLastName.text = userPaymentData?.lastName
            cell.txtState.text = userPaymentData?.stateName
            cell.txtCity.text = userPaymentData?.cityName
            cell.txtStreetName.text = userPaymentData?.street
            cell.txtZipCode.text = userPaymentData?.zipCode
            cell.txtRoutingNumber.text = userPaymentData?.routingNumber
            cell.txtAccNumber.text = userPaymentData?.accountNumber
            cell.delegate = self
          
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CheckPaymentCell") as! CheckPaymentCell
            
            //Adding Tap Gesture to Present Google AutoComplet Address
//            let tapGesture = UITapGestureRecognizer(target: self, action:  #selector(AddressTapHandler))
//            cell.vwStreetAddress.addGestureRecognizer(tapGesture)
            
            //Getting Cheque payment Cell Data
            cell.txtFirstName.text = userPaymentData?.firstName
            cell.txtLastName.text = userPaymentData?.lastName
            cell.txtState.text = userPaymentData?.stateName
            cell.txtCity.text = userPaymentData?.cityName
            cell.txtStreet.text = userPaymentData?.street
            cell.txtZipCode.text = userPaymentData?.zipCode
            cell.delegate = self
            
            return cell
        }
    }
    
    //MARK:- Private Method
//    func AddressTapHandler() {
//
//        let autocompleteController = GMSAutocompleteViewController()
//        autocompleteController.delegate = self
//        present(autocompleteController, animated: true, completion: nil)
//    }
    
}


extension AddServiceVC : UITextFieldDelegate
{
    //MARK: UITextField Delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtFlatFee {
            
            txtOfferDetails.becomeFirstResponder()
            
        }else{
            
            textField.resignFirstResponder()
            
        }
        
        return true
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtFlatFee
        {
            let inverseSet = NSCharacterSet(charactersIn:"0123456789").inverted
            
            let components = string.components(separatedBy: inverseSet)
            
            let filtered = components.joined(separator: "")
            
            if filtered == string {
                return true
            } else {
                
                if string == "." {
                    if textField.text == "" && (string == ".") {
                        return false
                    }
                    else
                    {
                        let countdots = textField.text!.components(separatedBy:".").count - 1
                        if countdots == 0 {
                            return true
                        }else{
                            
                            if countdots > 0 && string == "." {
                                return false
                            } else {
                                return true
                            }
                        }
                    }
                }else{
                    return false
                }
            }
            
        }else{
            return true
        }
    }
    
}
//extension AddServiceVC : GMSAutocompleteViewControllerDelegate
//{
//    //MARK: Auto Complete Google Delegates
//
//    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
//
//        if let addressLines = place.addressComponents {
//            // Populate all of the address fields we can find.
//
//            for field in addressLines {
//
//                switch field.type {
//                case kGMSPlaceTypePostalCode:
//                    self.zipcode = field.name
//                default:
//                    //print("Type: \(field.type), Name: \(field.name)")
//                    print("")
//                }
//            }
//        }
//        self.latitude = place.coordinate.latitude
//        self.longitude = place.coordinate.longitude
//
//        streetAddress = place.formattedAddress!
//        self.userPaymentData?.street = streetAddress
//        self.tblDropDown.reloadData()
//
//        dismiss(animated: true, completion: nil)
//    }
//
//
//    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
//        // TODO: handle the error.
//        print("Error: ", error.localizedDescription)
//    }
//
//    // User canceled the operation.
//    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
//        dismiss(animated: true, completion: nil)
//    }
//
//    // Turn the network activity indicator on and off again.
//    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
//        UIApplication.shared.isNetworkActivityIndicatorVisible = true
//    }
//
//    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
//        UIApplication.shared.isNetworkActivityIndicatorVisible = false
//    }
//
//}

