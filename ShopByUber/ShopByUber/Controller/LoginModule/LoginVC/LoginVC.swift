//
//  LoginVC.swift
//  ShopByUber
//
//  Created by Administrator on 5/23/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit
import GoogleMaps

class LoginVC: UIViewController, CLLocationManagerDelegate {
    
    //MARK: IBOutlet
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtPassword: UITextField!
    @IBOutlet var btnRegister: UIButton!
    
    //MARK:- Variable
    var isPush:Bool = false
    var strAddress = String()
    var locationManager = CLLocationManager()
    var lat = Double()
    var long = Double()
    var city = String()
    var state = String()
    var country = String()
    
    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialConfig()
      
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        //MARK:- Getting User Current Location
        if APIManager.isConnectedToNetwork() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }else {
            Utilities.showAlertView(title: "", message: Constant.serverAPI.errorMessages.kNoInternetConnectionMessage)
        }
    }
    
    //MARK:- CLLocation Manager Delegate
    @nonobjc func locationManager(_ manager: CLLocationManager, didFailWithError error: NSError)
    {
        print("Error" + error.description)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
            let userLocation = locations.last
        
            lat = userLocation!.coordinate.latitude
            long = userLocation!.coordinate.longitude
        
            print("Latitude :- \(userLocation!.coordinate.latitude)")
            print("Longitude :-\(userLocation!.coordinate.longitude)")
            
            locationManager.stopUpdatingLocation()
            
            //Address Maker
            let geocoder = CLGeocoder()
            let location = CLLocation(latitude: userLocation!.coordinate.latitude, longitude: userLocation!.coordinate.longitude)
            geocoder.reverseGeocodeLocation(location) { (placemarksArray, error) in
                
                if (placemarksArray?.count)! > 0 {
                    
                    let placemark = placemarksArray?.first
                    let number = placemark?.subThoroughfare
                    let street = placemark?.thoroughfare
                    let bairro = placemark?.subLocality
                    self.city = (placemark?.locality)!
                    let postcode = placemark?.postalCode
                    self.state = (placemark?.administrativeArea)!
                    self.country = (placemark?.country)!
                    
                    self.strAddress  = "\(number ?? "") \(street ?? ""), \(bairro ?? ""), \(self.city), \(postcode ?? ""), \(self.state), \(self.country)"
                    print(self.strAddress)
                
                }
            }
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - IBAction
    
    @IBAction func btnLogin_Clicked(_ sender: UIButton) {
        
        if User.isValidLoginData(email: txtEmail.text, password: txtPassword.text){
            
            User.callLoginAPI(email: txtEmail.text, password: txtPassword.text, success: { (dict) in
                
             //   appShareManager.selectedMenuItemIndex = 0
                let userId = dict?["userId"] as? String
                DataModel.sharedInstance.setValueInUserDefaults(value: userId, key: Constant.DataModelKey.KUserId)
                let isProfileCreated = dict?["isCompanyProfileCreated"] as? Bool
                
                DataModel.sharedInstance.setValueInUserDefaults(value: isProfileCreated ?? true, key: Constant.DataModelKey.kIsProfileCreated)
                
                if dict?["number_verified"] as! Bool {
                    
                    //Verified User
                    let sessionId = dict?["sessionId"] as? String
                    let userType = dict?["userRole"] as? Int
                    let email = dict?["email"] as? String
                    let mobilePhone = dict?["mobilePhone"] as? String
                    let name = dict?["name"] as? String
                    let firstName = dict?["firstName"] as? String
                    let lastName = dict?["lastName"] as? String
                    let userImage = dict?["profileImage"] as? String
                    let description = dict?["description"] as? String
                    let countryCode = dict?["countryCode"] as? String
                    let phoneNumber = dict?["phoneNumber"] as? String
                    let legalBusinessName = dict?["legalBusinessName"] as? String
                    let ezListScreenName = dict?["ezListScreenName"] as? String
                    let promoCode = dict?["promoCode"] as? String ?? ""
                    let quickBloxId = dict?["quickBloxId"] as? Int ?? 0
                    
                    DataModel.sharedInstance.setUserDataWhenLogin(userID: userId, sessionId: sessionId, userType: userType, email: email, name: name, firstName: firstName, lastName: lastName, mobile: mobilePhone,phone:phoneNumber,countryCode: countryCode,userProfile: userImage, ezListScreenName: ezListScreenName, description: description,promoCode: promoCode)
                    DataModel.sharedInstance.setUserDefaultQuickBloxId(quickBloxId: quickBloxId)
                    
                    DataModel.sharedInstance.setValueInUserDefaults(value: dict?["isServicesCreated"] as? Bool, key: Constant.DataModelKey.kIsServiceAvailable)
                    DataModel.sharedInstance.setValueInUserDefaults(value: self.strAddress, key: Constant.DataModelKey.kaddress)
                    DataModel.sharedInstance.setValueInUserDefaults(value: self.city, key: Constant.DataModelKey.kCity)
                    DataModel.sharedInstance.setValueInUserDefaults(value: self.state, key: Constant.DataModelKey.kState)
                    
                    //Quickblox
                    QuickBloxManager.sharedInstance.checkAndLoginInQuickBlox()
                    
                    NotificationCenter.default.post(name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil, userInfo: nil)
                    if DataModel.sharedInstance.onFetchUserLoginType() == Constant.UserType.Buyer {
                        
                        //Buyer DashBoard
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.onSetupDashboardPage()
                        DataModel.sharedInstance.CheckVersionUpdate(checkVersionUpdate: true)
                        
                    }else{
                        
                        //Seller
                        UserDefaults.standard.set(legalBusinessName, forKey: Constant.DataModelKey.kLegalBusinessName)
                        UserDefaults.standard.synchronize()
                        DataModel.sharedInstance.CheckVersionUpdate(checkVersionUpdate: true)
                        
                        if dict?["isCompanyProfileCreated"] as! Bool {
                            
                            //Profile Created
                            
                            //Dashboard
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.onSetupDashboardPage()
                        }else{
                            
                            //Profile Not Created
                            
                            let serviceProfile = ServiceProfileVC(nibName: "ServiceProfileVC", bundle: nil) as ServiceProfileVC
                            
                            _ = self.navigationController?.pushViewController(serviceProfile, animated: true)
                            
                        }
                    }
                    
                }else{
                    
                    //Not Verified User
                    let mobileNumber = dict?["phoneNumber"] as? String
                    let counrtyCode = dict?["countryCode"] as? String
                    let numberVerification = NumberVerificationVC(nibName: "NumberVerificationVC", bundle: nil) as NumberVerificationVC
                    
                    let verficationCode = dict?["verficationCode"] as? Int
                    numberVerification.isFromLogin = true
                    numberVerification.countryCode = counrtyCode ?? "+1"
                    numberVerification.mobileNumber = mobileNumber ?? ""
                    numberVerification.code = verficationCode ?? 0
                    _ = self.navigationController?.pushViewController(numberVerification, animated: true)
                }
            }, failure: {
                
            })
            
        }
        
    }
    
    @IBAction func btnRegister_Click(_ sender: UIButton) {
        
        if isPush
        {
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            let register = RegisterVC(nibName: "RegisterVC", bundle: nil) as RegisterVC
            _ = navigationController?.pushViewController(register, animated: true)
        }
    }
    
    @IBAction func btnFacebook_Clicked(_ sender: UIButton) {
       
        if strAddress != "" {
        
        let fbManager: FBSDKLoginManager = FBSDKLoginManager()
        fbManager.logOut()
        let permisions = ["public_profile", "email"]
        
        fbManager.logIn(withReadPermissions: permisions, from: self) { (result, error) in
            
            if (error == nil) {
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if(fbloginresult.grantedPermissions !=  nil) {
                    self.getFbUserData()
                    fbManager.logOut()
                }
            }
        }
    }
             //Show Alert to turn on Location
        else {
        
            
            let alertController = UIAlertController(title: Constant.appTitle.strTitle + " Need to access your Location", message: "Please turn on Location from Settings", preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            let settingsAction = UIAlertAction(title: "Settings", style: .default) { (UIAlertAction) in
                UIApplication.shared.openURL(NSURL(string: UIApplicationOpenSettingsURLString)! as URL)
            }
            
            alertController.addAction(cancelAction)
            alertController.addAction(settingsAction)
            self.present(alertController, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func btnTwitter_Clicked(_ sender: UIButton) {
        
        //KVNProgress.show()
        if strAddress != "" {
        if Twitter.sharedInstance().sessionStore.session() == nil
        {
            Twitter.sharedInstance().logIn(completion: { (session, error) in
                if (session != nil) {
                    
                    let client = TWTRAPIClient(userID: session?.userID)
                    client.loadUser(withID: (session?.userID)!, completion: { (user, error) -> Void in
                        
                        let twitterIUserID = session?.userID
                        var firstName = ""
                        var lastName = ""
                        
                        if user?.name != nil
                        {
                            var fullNameArr = user!.name.components(separatedBy: " ")
                            if fullNameArr.count > 0
                            {
                                firstName = fullNameArr[0]
                            }
                            
                            if fullNameArr.count > 1
                            {
                                lastName = fullNameArr[1]
                            }
                        }
                        
                        self.callAPIForSocialLogin(isFacebookSignUp: false, firstName: firstName, lastName: lastName, ezListScreenName: "", mobilePhone: "", facebookId: "", twitterId: twitterIUserID, email: "", password: "", address: self.strAddress, latitude: self.lat, longitude: self.long, promoCode: "")
                        
                        
                    })
                } else {
                    KVNProgress.dismiss()
                    self.showSimpleAlert(title: "Error", message: Constant.serverAPI.errorMessages.kCommanErrorMessage)
                    print("error: \(String(describing: error?.localizedDescription))");
                }
            })
        }else{
            
            KVNProgress.dismiss()
            
            let store = Twitter.sharedInstance().sessionStore
            if let userID = store.session()?.userID {
                store.logOutUserID(userID)
            }
        }
        
    }
        //Show Alert to turn on Location
        else {
            let alertController = UIAlertController(title: Constant.appTitle.strTitle + " Need to access your Location", message: "Please turn on Location from Settings", preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            let settingsAction = UIAlertAction(title: "Settings", style: .default) { (UIAlertAction) in
                UIApplication.shared.openURL(NSURL(string: UIApplicationOpenSettingsURLString)! as URL)
            }
            
            alertController.addAction(cancelAction)
            alertController.addAction(settingsAction)
            self.present(alertController, animated: true, completion: nil)

        }
    }
    
    @IBAction func btnForgotPwd_Clicked(_ sender: UIButton) {
        
        let forgotPassword = ForgotPasswordVC(nibName: "ForgotPasswordVC", bundle: nil) as ForgotPasswordVC
        _ = self.navigationController?.pushViewController(forgotPassword, animated: true)
        
    }
    
    @IBAction func bgVW_Clicked(_ sender: UITapGestureRecognizer) {
        
        //hide keyboard when view tap
        self.view.endEditing(true)
        
    }
    
    //MARK: - Private Methods
    
    func initialConfig(){
        
        Utilities.setupAttributedButton(range: NSRange(location:23,length:14), btn: btnRegister)
    }
    
    func getFbUserData(){
        KVNProgress.show()
        if((FBSDKAccessToken.current()) != nil){
            
            FBSDKGraphRequest.init(graphPath: "me", parameters: ["fields":"id, first_name, last_name, picture.type(large), email"]).start { (connection, result, error) -> Void in
                
                if(error == nil) {
                    let response = result as AnyObject?
                    let FbID = response?["id"] as? String
                    let email = response?["email"] as? String ?? ""
                    let firstName = response?["first_name"] as? String ?? ""
                    let lastName = response?["last_name"] as? String ?? ""
                    
                    self.callAPIForSocialLogin(isFacebookSignUp: true, firstName: firstName, lastName: lastName, ezListScreenName: "", mobilePhone: "", facebookId: FbID, twitterId: "", email: email, password: "", address: self.strAddress, latitude: self.lat, longitude: self.long, promoCode: "")
                    
                }
                else{
                    KVNProgress.dismiss()
                    return
                }
            }
        }
    }
    
    
    //MARK: Call API For Social Login
    
    func callAPIForSocialLogin(isFacebookSignUp:Bool,firstName: String?, lastName: String?, ezListScreenName: String?, mobilePhone: String?, facebookId: String?, twitterId: String?, email: String?, password: String, address: String?, latitude:  Double?, longitude: Double?,promoCode: String?) {
        
        User.callFacebookTwitterAPI(firstName: firstName, lastName: lastName, ezListScreenName: ezListScreenName, mobilePhone: mobilePhone, facebookId:facebookId , twitterId: twitterId, email: email, password: "", address: strAddress, latitude: self.lat, longitude: self.long, city: self.city, state: self.state, country: self.country, userRole: Constant.UserType.Buyer, isFacebookSignUp: isFacebookSignUp, success: { (dict) in
            
            let userId = dict?["userId"] as? String
            let sessionId = dict?["sessionId"] as? String
            let userType = dict?["userRole"] as? Int
            let email = dict?["email"] as? String
            let mobilePhone = dict?["mobilePhone"] as? String
            let name = dict?["name"] as? String
            let firstName = dict?["firstName"] as? String
            let lastName = dict?["lastName"] as? String
            let userImage = dict?["profileImage"] as? String
            let description = dict?["description"] as? String
            let countryCode = dict?["countryCode"] as? String
            let phoneNumber = dict?["phoneNumber"] as? String
            let ezListScreenName = dict?["ezListScreenName"] as? String
            let promoCode = dict?["promoCode"] as? String
            
            DataModel.sharedInstance.setUserDataWhenLogin(userID: userId, sessionId: sessionId, userType: userType, email: email, name: name, firstName: firstName, lastName: lastName, mobile: mobilePhone,phone:phoneNumber,countryCode: countryCode,userProfile: userImage, ezListScreenName: ezListScreenName, description: description, promoCode: promoCode)
            
            //Quickblox
            QuickBloxManager.sharedInstance.checkAndLoginInQuickBlox()
            
            NotificationCenter.default.post(name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil, userInfo: nil)
            
            let objBuyerVC = BuyerVC(nibName: "BuyerVC", bundle: nil) as BuyerVC
            _ = self.navigationController?.pushViewController(objBuyerVC, animated: true)
            
        }, failure: {
            
        })
        
    }
    
}

extension LoginVC : UITextFieldDelegate
{
    //MARK: UITextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtEmail{
            txtPassword.becomeFirstResponder()
        }else{
            textField.resignFirstResponder()
        }
        return true
    }
    
}


