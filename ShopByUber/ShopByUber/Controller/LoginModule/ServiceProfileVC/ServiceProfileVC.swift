//
//  ServiceProfileVC.swift
//  ShopByUber
//
//  Created by Administrator on 5/25/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit
import SearchTextField

class ServiceProfileVC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,PECropViewControllerDelegate{

    //MARK: IBOutlet
    
    @IBOutlet var vwImagePlaceholder: UIView!
    @IBOutlet var navigationBar: UIView!
    @IBOutlet var vwNumber: UIView!
    @IBOutlet var vwScreenName: UIView!
    @IBOutlet var vwProvider: UIView!
    @IBOutlet weak var vwState: UIView!
    @IBOutlet weak var vwCity: UIView!
    @IBOutlet weak var vwPaymentOptions: UIView!
    @IBOutlet weak var lblCountryCode: UILabel!
    @IBOutlet weak var VwFirstName: UIView!
    @IBOutlet weak var VwLastName: UIView!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet var txtScreenName: UITextField!
    @IBOutlet var txtNumber: UITextField!
    @IBOutlet weak var txtPaymentOption: UITextField!
    @IBOutlet var lblProviderName: UILabel!
    @IBOutlet var imgProvider: UIImageView!
    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet weak var nslcImgProfileHeight: NSLayoutConstraint!
    @IBOutlet weak var nslcTblDropDownHeight: NSLayoutConstraint!
    @IBOutlet weak var vwChangePicture: UIView!
    @IBOutlet weak var tblDropDown: UITableView!
   
    @IBOutlet weak var txt_State: SearchTextField!
    @IBOutlet weak var txt_City: SearchTextField!
    @IBOutlet weak var nslcServiceProviderTop: NSLayoutConstraint!
    
    
    //MARK: Variables
    var imagePicker = UIImagePickerController()
    var isProfileSelected = false
    var isFromRegistration = false
    var requestID = String()
    
    //Request Params
    var latitude = 0.0
    var longitude = 0.0
    var zipcode = ""
    var userPaymentData : PaymentData?
    let dropDown = DropDown()
    var streetAddress = String()
    var paymentData = [String:Any]()
    var paypalEmail = String()
    var selectedIndex = 0
    var promoCode = String()
    
    var arr_StateList = [StateList]()
    var arr_CitiesList = [CitiesList]()
    var state_Code = String()
    var city_Code = String()
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad() 
        self.initialConfig()
        
        self.tblDropDown.register(UINib(nibName: "PaypalPaymentCell", bundle: nil), forCellReuseIdentifier: "PaypalPaymentCell")
        self.tblDropDown.register(UINib(nibName: "BankPaymentCell", bundle: nil), forCellReuseIdentifier: "BankPaymentCell")
        self.tblDropDown.register(UINib(nibName: "CheckPaymentCell", bundle: nil), forCellReuseIdentifier: "CheckPaymentCell")
        
        tblDropDown.dataSource = self
        tblDropDown.delegate = self
        tblDropDown.reloadData()
        
        self.txtPaymentOption.text = "Select your Payment Option"
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - IBAction
    @IBAction func vwBG_Clicked(_ sender: UITapGestureRecognizer) {
    
        self.view.endEditing(true)
        
    }
    
    
    @IBAction func btnSubmit_Clicked(_ sender: UIButton) {
        
        if ServiceProfile.isValidServiceData(isProfileSelected: self.isProfileSelected, firstName: txtFirstName.text, lastName: txtLastName.text, screenName: txtScreenName.text, number: txtNumber.text, state: txt_State.text, city: txt_City.text){
            
            
            if selectedIndex  == 0 {
                callUploadImage()
            }
            else if selectedIndex  == 1 {
                let cell = tblDropDown.cellForRow(at: IndexPath(row: 0, section: 0)) as? PaypalPaymentCell
                if  (cell?.isValidServiceData(email: cell?.txtPaypalEmail.text))!{
                    
                    callUploadImage()
                }
            }
            else if selectedIndex  == 2 {
                
                let cell = tblDropDown.cellForRow(at: IndexPath(row: 0, section: 0)) as? BankPaymentCell
                if (cell?.isValidServiceData(firstName: cell?.txtFirstName.text, lastName: cell?.txtLastName.text, streetName: cell?.txtStreetName.text, city: cell?.txtCity.text, state: cell?.txtState.text, zipcode: cell?.txtZipCode.text, routingNumber: cell?.txtRoutingNumber.text, accountNumber: cell?.txtAccNumber.text))! {
                    
                    callUploadImage()
                }
            }
            else {
                let cell = tblDropDown.cellForRow(at: IndexPath(row: 0, section: 0)) as? CheckPaymentCell
                if (cell?.isValidServiceData(firstName: cell?.txtFirstName.text, lastName: cell?.txtLastName.text, streetName: cell?.txtStreet.text, city: cell?.txtCity.text, state: cell?.txtState.text, zipcode: cell?.txtZipCode.text))! {
                    
                    self.callUploadImage()
                }
            }
        }
    }
    
    @IBAction func imgProfile_Clicked(_ sender: UITapGestureRecognizer) {
        
        openImagePicker()
    }
    
    @IBAction func btnChangePicture_Tapped(_ sender: Any) {
        
        openImagePicker()
    }
    

    @IBAction func btnPaymentOption_Click(_ sender: Any) {
        
        dropDown.anchorView = txtPaymentOption
        dropDown.direction = .top
        dropDown.dataSource = ["Select your Payment Option","Paypal","Bank Account (ACH/EFT)", "Pay by Check"]
        dropDown.width = txtPaymentOption.frame.size.width
        
        
        dropDown.selectionAction = {(index: Int, item: String) in
           
            
            if self.selectedIndex != index {
                
                    if index == 0
                    {
                        //select payment option
                        self.txtPaymentOption.text = "Select your Payment Option"
                        self.nslcTblDropDownHeight.constant = 0
                        self.selectedIndex = 0
                    }else if index == 1
                    {
                        //Paypal
                        self.txtPaymentOption.text = "Paypal"
                        self.nslcTblDropDownHeight.constant = Utilities.isDeviceiPad() ? 150 : 130
                        self.tblDropDown.reloadData()
                        self.selectedIndex = 1
                    }else if index == 2
                    {
                        //Bank
                        self.txtPaymentOption.text = "Bank Account (ACH/EFT)"
                        self.nslcTblDropDownHeight.constant = Utilities.isDeviceiPad() ? 400 : 325
                        self.tblDropDown.reloadData()
                        self.selectedIndex = 2
                    }else {
                        //Cheque
                        self.txtPaymentOption.text = "Pay by Check"
                        self.nslcTblDropDownHeight.constant = Utilities.isDeviceiPad() ? 300 : 240
                        self.tblDropDown.reloadData()
                        self.selectedIndex = 3
                    }
            }
        }
        dropDown.show()
        
    }
    
    //MARK: - Open Image Picker
    
    func openImagePicker() {
        
        imagePicker.delegate = self
        
        let alert:UIAlertController = UIAlertController(title: "Choose Image", message: nil, preferredStyle: Utilities.isDeviceiPad() ? .alert : .actionSheet )
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default){
            UIAlertAction in
            self.openProfileUserCamera()
        }
        
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.openProfileUserGallary()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
        }
        
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)

    }
    
    //MARK: - Image Delegate
    
    func openProfileUserGallary() {
        
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        imagePicker.mediaTypes = [kUTTypeImage as String]
        present(imagePicker, animated: true, completion: nil)
    }
    
    func openProfileUserCamera() {
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.allowsEditing = false
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.cameraCaptureMode = .photo
            imagePicker.modalPresentationStyle = .fullScreen
            present(imagePicker,animated: true,completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {

        dismiss(animated:true, completion: nil)
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            self.openEditor(image: pickedImage)
        }

    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        if self.isProfileSelected == false
        {
            self.isProfileSelected = false
        }
        dismiss(animated:true, completion: nil)
    }
    
    //MARK: - PECropViewController Delegate
    func openEditor(image: UIImage) {
        let controller = PECropViewController()
        controller.delegate = self
        controller.image = image
        let width: CGFloat? = image.size.width
        let height: CGFloat? = image.size.height
        let length: CGFloat = min(width!, height!)
        let ratioValue = width!/4
        
        controller.imageCropRect = CGRect(x: (width! - length) / 2, y: (height! - length) / 2, width: ratioValue*4, height: ratioValue*3)
        controller.keepingCropAspectRatio = true
        let navigationController = UINavigationController(rootViewController: controller as UIViewController )
        if UI_USER_INTERFACE_IDIOM() == .pad {
            navigationController.modalPresentationStyle = .formSheet
        }
        present(navigationController, animated: true) { _ in }
    }
    func cropViewController(_ controller: PECropViewController!, didFinishCroppingImage croppedImage: UIImage!) {
        controller.dismiss(animated: true) { _ in }
        self.imgProfile.image = croppedImage
        self.imgProvider.image = croppedImage
        self.nslcImgProfileHeight.constant = Utilities.isDeviceiPad() ? 480 : 250
        self.isProfileSelected = true
        self.vwImagePlaceholder.isHidden = true
        self.imgProfile.isUserInteractionEnabled = false
        self.vwChangePicture.isHidden = false

        
        if UI_USER_INTERFACE_IDIOM() == .pad {
        }
        
    }
    
    func cropViewController(_ controller: PECropViewController, didFinishCroppingImage croppedImage: UIImage, transform: CGAffineTransform, cropRect: CGRect) {
        controller.dismiss(animated: true) { _ in }
        self.imgProfile.image = croppedImage
        self.imgProvider.image = croppedImage
        self.nslcImgProfileHeight.constant = Utilities.isDeviceiPad() ? 480 : 250
        self.isProfileSelected = true
        self.vwImagePlaceholder.isHidden = true
        self.imgProfile.isUserInteractionEnabled = false
        self.vwChangePicture.isHidden = false

        if UI_USER_INTERFACE_IDIOM() == .pad {
        }
    }
    
    func cropViewControllerDidCancel(_ controller: PECropViewController) {
        self.isProfileSelected = true
        if UI_USER_INTERFACE_IDIOM() == .pad {
            
        }
        controller.dismiss(animated: true) { _ in }
    }

    //MARK: - Private Methods
    func makeTopLeftRedius() {
        let size: CGFloat = Utilities.isDeviceiPad() ? 12 : 5
        let maskLayer = CAShapeLayer()
        maskLayer.path = UIBezierPath(roundedRect: vwChangePicture.bounds, byRoundingCorners: [.topLeft], cornerRadii: CGSize(width: size, height: size)).cgPath
        vwChangePicture.layer.mask = maskLayer
    }
    
    func initialConfig(){
        
        Utilities.applyShadowEffect(view: [self.navigationBar,self.VwFirstName,self.VwLastName,self.vwNumber,self.vwScreenName,self.vwState, self.vwCity,self.vwProvider,self.vwPaymentOptions])
        
        vwChangePicture.isHidden = true
  
        self.txtFirstName.text = DataModel.sharedInstance.onFetchFirstName()
        self.txtLastName.text  = DataModel.sharedInstance.onFetchLastName()
        self.txtScreenName.text = DataModel.sharedInstance.onFetchEzListScreenName()
        self.txtNumber.text = DataModel.sharedInstance.onFetchMobileNumber().toPhoneNumber()
        self.lblCountryCode.text = DataModel.sharedInstance.onFetchCountryCode()
        self.lblProviderName.text = DataModel.sharedInstance.onFetchEzListScreenName()
        
//        DataModel.sharedInstance.setValueInUserDefaults(value: "", key: Constant.DataModelKey.kCity)
//        DataModel.sharedInstance.setValueInUserDefaults(value: "", key: Constant.DataModelKey.kState)
        
        if DataModel.sharedInstance.onFetchCity() == "" || DataModel.sharedInstance.onFetchState() == "" {
      
            txt_State.delegate = self
            txt_City.delegate = self
            self.nslcServiceProviderTop.constant = Utilities.isDeviceiPad() ? 225 : 185
            self.callGetStateAPI()
    
        }else {
            //Hide City & state
            self.nslcServiceProviderTop.constant = 15
        }
    
//      if sellerData?.isSalesperson == true {
//           self.vwPromoCode.isUserInteractionEnabled = false
//      }
    
        print(DataModel.sharedInstance.onFetchIsSalesPerson())
        // if seller ask for promocode
        if DataModel.sharedInstance.onFetchIsSalesPerson() == false {
            //Show Promo Code Popup
            
            Utilities.showAlertWithAction(title: "", message: "Do you have a Promo Code?", buttonTitle: "Yes", buttonTitle2: "No", onOKClick: {
                //Show Promo Code Popup
                
                self.view.showPromoCodePopUp() { (promo) in
                    print(promo)
                    self.promoCode = promo
                    
                }
            }) {
                //Dismiss
            }
        }else {
            // if sales person get promocode
            self.promoCode = DataModel.sharedInstance.onFetchPromoCode()
        }
    }

    
    //MARK: - API Call
    
    func callUploadImage(){

        ServiceProfile.callUploadImageAPI(imageType: Constant.ImageType.CompanyProfile, image: self.imgProfile.image, success: { (name) in

            self.callCreateServiceAPI(imageName: name)

        }) {

        }

    }
    
    func callCreateServiceAPI(imageName:String?){
        let mobileNo = txtNumber.text?.replacingOccurrences(of: "-", with: "")

        if self.selectedIndex  == 0 {
                paypalEmail                      = ""
                paymentData                      = [:]
        }
        else if self.selectedIndex  == 1 {
            if let cell = tblDropDown.cellForRow(at: IndexPath(row: 0, section: 0)) as? PaypalPaymentCell {
                paypalEmail                      = cell.txtPaypalEmail.text ?? ""
                paymentData                      = [:]
            }
        }
        else if self.selectedIndex  == 2 {
            if let cell = tblDropDown.cellForRow(at: IndexPath(row: 0, section: 0)) as? BankPaymentCell {
                
                paymentData["firstName"]         = cell.txtFirstName.text ?? ""
                paymentData["lastName"]          = cell.txtLastName.text ?? ""
                paymentData["streetAddress"]     = cell.txtStreetName.text ?? ""
                paymentData["city"]              = cell.cityCode
                paymentData["state"]             = cell.stateCode
                paymentData["zipCode"]           = cell.txtZipCode.text ?? ""
                paymentData["routingNumber"]     = cell.txtRoutingNumber.text ?? ""
                paymentData["accountNumber"]     = cell.txtAccNumber.text ?? ""
               
            }
        }
        else if self.selectedIndex  == 3 {
            if let cell = tblDropDown.cellForRow(at: IndexPath(row: 0, section: 0)) as? CheckPaymentCell {
                
                paymentData["firstName"]         = cell.txtFirstName.text ?? ""
                paymentData["lastName"]          = cell.txtLastName.text ?? ""
                paymentData["streetAddress"]     = cell.txtStreet.text ?? ""
                paymentData["city"]              = cell.cityCode
                paymentData["state"]             = cell.stateCode
                paymentData["zipCode"]           = cell.txtZipCode.text ?? ""
                paymentData["routingNumber"]     = ""
                paymentData["accountNumber"]     = ""
               
            }
        }
        
        print(paymentData)
        print(DataModel.sharedInstance.onFetchLatitude())
        print(DataModel.sharedInstance.onFetchLongtitude())
        
        ServiceProfile.callCreateServiceAPI(imageName: imageName, companyName: "", ownerName: txtFirstName.text! + " " + txtLastName.text!,firstName: txtFirstName.text,lastName: txtLastName.text, screenName: txtScreenName.text, addess: DataModel.sharedInstance.onFetchAddress(), latitude: DataModel.sharedInstance.onFetchLatitude(), longitude: DataModel.sharedInstance.onFetchLongtitude(), zipcode: self.zipcode, phoneNumber: mobileNo, website: "",paypalEmail : self.paypalEmail, description: "", countryCode: self.lblCountryCode.text,paymentMethod: self.selectedIndex, paymentDetail: paymentData, promoCode: self.promoCode,city: (txt_City.text ?? "").isEmpty ? DataModel.sharedInstance.onFetchCity() : (txt_City.text!), state: (txt_State.text ?? "").isEmpty ? DataModel.sharedInstance.onFetchState() : (txt_State.text!), success: { (data) in
        
            DataModel.sharedInstance.setValueInUserDefaults(value: true, key: Constant.DataModelKey.kIsProfileCreated)
            
        if DataModel.sharedInstance.onFetchCity() == "" && DataModel.sharedInstance.onFetchState() == "" {
                
            DataModel.sharedInstance.setValueInUserDefaults(value: self.txt_State.text, key: Constant.DataModelKey.kState)
            DataModel.sharedInstance.setValueInUserDefaults(value: self.txt_City.text, key: Constant.DataModelKey.kCity)
        }
            
        if self.isFromRegistration
        {
            //Add PromoCode in userdefault
            DataModel.sharedInstance.setValueInUserDefaults(value: self.promoCode, key: Constant.DataModelKey.kpromoCode)
            let addServiceVC = AddServiceVC(nibName: "AddServiceVC", bundle: nil)
            addServiceVC.isFromRegistration = true
          //  if self.requestID != "" {
          //      addServiceVC.isFromWantedService = true
          //      addServiceVC.requestID = self.requestID
          //  }
            _ = self.navigationController?.pushViewController(addServiceVC, animated: true)
            
        }else{
            //Add PromoCode in userdefault
            DataModel.sharedInstance.setValueInUserDefaults(value: self.promoCode, key: Constant.DataModelKey.kpromoCode)
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.onSetupDashboardPage(switchedToSeller: true)

        }

       }) {
        
        }
        
    }
    
    //MARK:- API Get State list
    func callGetStateAPI(){
        
        KVNProgress.show()
        
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kgetStatesList, inputParams: nil, success: { (dict) in
            
            
            if dict?["Result"] as? Bool == true {
                let data = dict?["Data"] as? [String:Any]
                
                
                let state = data?["States"] as! [[String : Any]]
                if !state.isEmpty
                {
                    self.arr_StateList = state.map(StateList.init)
                    
                    self.txt_State.filterStrings(self.arr_StateList.flatMap({ $0.stateName }))
                    
                    self.txt_State.maxNumberOfResults = 5
                    self.txt_State.theme.cellHeight = 44
                    self.txt_State.theme.font = UIFont.systemFontReguler(with: Utilities.isDeviceiPad() ? 20 : 16)
                    self.txt_State.theme.bgColor = UIColor.white
                    self.txt_State.theme.separatorColor = UIColor.lightGray
                    self.txt_State.comparisonOptions = .caseInsensitive
                    self.txt_State.highlightAttributes = [NSBackgroundColorAttributeName: UIColor.white, NSFontAttributeName:UIFont.systemFontBold(with: Utilities.isDeviceiPad() ? 20 : 16)]
                    
                    self.txt_State.itemSelectionHandler = { filteredResults, itemPosition in
                        
                        self.txt_City.text = ""
                        let item = filteredResults[itemPosition]
                        self.txt_State.text = item.title
                    
                        let sName = self.arr_StateList.flatMap({ $0.stateName })
                        let stateCode = sName.index(of: item.title)
                        //Call city api
                        self.callGetCityAPI(index: stateCode!)
                        
                    }
                }
                KVNProgress.dismiss()
                //Call Cities API
            }
        }) { error in
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
        }
    }
    
    func callGetCityAPI(index: Int){
        
        KVNProgress.show()
        let dict = [ "stateId" : arr_StateList[index].stateId ]
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kgetCitiesList, inputParams: dict, success: { (dict) in
            
            if dict?["Result"] as? Bool == true {
                let data = dict?["Data"] as? [String:Any]
                
                let cities = data?["Cities"] as! [[String : Any]]
                if cities.count != 0
                {
                    self.arr_CitiesList = cities.map(CitiesList.init)
                    self.txt_City.filterStrings(self.arr_CitiesList.flatMap({ $0.cityName}))
                    
                    self.txt_City.theme.cellHeight = 44
                    self.txt_City.maxNumberOfResults = 5
                    self.txt_City.theme.font = UIFont.systemFontReguler(with: Utilities.isDeviceiPad() ? 20 : 16)
                    self.txt_City.theme.bgColor = UIColor.white
                    self.txt_City.theme.separatorColor = UIColor.lightGray
                    self.txt_City.comparisonOptions = .caseInsensitive
                    self.txt_City.highlightAttributes = [NSBackgroundColorAttributeName: UIColor.white, NSFontAttributeName:UIFont.systemFontBold(with: Utilities.isDeviceiPad() ? 20 : 16)]
                    self.txt_City.layoutSubviews()
                  
                    
                    self.txt_City.itemSelectionHandler = { filteredResults, itemPosition in
                   
                        let item = filteredResults[itemPosition]
                        self.txt_City.text = item.title
                        
//                        let cName = self.arr_CitiesList.flatMap({ $0.cityName })
//                        let cityCode = cName.index(of: item.title)
                    }
                    
                }
                
                KVNProgress.dismiss()
                
            }
        }) { error in
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
        }
    }
    
}

extension ServiceProfileVC : UITextFieldDelegate
{
    //MARK: UITextField Delegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let str = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if textField == txtNumber{
            
            return checkEnglishPhoneNumberFormat(string: string, str: str)
            
        }else if textField == txtScreenName {
            let searchStr: String = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            self.lblProviderName.text = searchStr
        }
        
        return true
    }
    
    //MARK:- Mobile Validation
    func checkEnglishPhoneNumberFormat(string: String?, str: String?) -> Bool{
        
        if string == ""{ //BackSpace
            
            return true
            
        }else if str!.count == 4{
            
            txtNumber.text = txtNumber.text! + "-"
            
        }else if str!.count == 8{
            
            txtNumber.text = txtNumber.text! + "-"
            
        }else if str!.count > 12{
            
            return false
        }
        
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtFirstName{
            txtLastName.becomeFirstResponder()
        }else
        if textField == txtLastName {
            txtScreenName.becomeFirstResponder()
        }else if textField == txtScreenName {
            textField.resignFirstResponder()
        }
        else if textField == txt_State {
            txt_City.becomeFirstResponder()
        }
        else if textField == txt_City {
            txtPaymentOption.becomeFirstResponder()
        }
//        else if textField == txtPaymentOption{
//            txtDescription.becomeFirstResponder()
//        }
        else{
            textField.resignFirstResponder()
        }
        
        return true
    }

}

//MARK:- TableView Delegate
extension ServiceProfileVC : UITableViewDelegate ,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if self.selectedIndex == 1
        {
            return Utilities.isDeviceiPad() ? 150 : 130      //paypal cell Height
        }
        else if self.selectedIndex == 2
        {
            return Utilities.isDeviceiPad() ? 400 : 325      //Bank Payment Cell Height
        }else
        {
            return Utilities.isDeviceiPad() ? 300 : 240      //Check Payment Cell Height
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if selectedIndex == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PaypalPaymentCell") as! PaypalPaymentCell
            
            //Get Paypal emailID
            
         //   paypalEmail = cell.txtPaypalEmail.text ?? ""
            
            return cell
        }
        else if selectedIndex == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BankPaymentCell") as! BankPaymentCell
            
            //Adding Tap Gesture to Present Google AutoComplet Address
//            let tapGesture = UITapGestureRecognizer(target: self, action:  #selector(AddressTapHandler))
//            cell.vwAddress.addGestureRecognizer(tapGesture)
            
            
            paymentData["firstName"]         = cell.txtFirstName.text ?? ""
            paymentData["lastName"]          = cell.txtLastName.text ?? ""
            paymentData["streetAddress"]     = cell.txtStreetName.text ?? ""
            paymentData["city"]              = cell.cityCode
            paymentData["state"]             = cell.stateCode
            paymentData["zipCode"]           = cell.txtZipCode.text ?? ""
            paymentData["routingNumber"]     = cell.txtRoutingNumber.text ?? ""
            paymentData["accountNumber"]     = cell.txtAccNumber.text ?? ""
            
            
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CheckPaymentCell") as! CheckPaymentCell
            
            //Adding Tap Gesture to Present Google AutoComplet Address
//            let tapGesture = UITapGestureRecognizer(target: self, action:  #selector(AddressTapHandler))
//            cell.vwStreetAddress.addGestureRecognizer(tapGesture)
            
            
            paymentData["firstName"]         = cell.txtFirstName.text ?? ""
            paymentData["lastName"]          = cell.txtLastName.text ?? ""
            paymentData["streetAddress"]     = cell.txtStreet.text ?? ""
            paymentData["city"]              = cell.cityCode
            paymentData["state"]             = cell.stateCode
            paymentData["zipCode"]           = cell.txtZipCode.text ?? ""
            
            
            return cell
        }
    }
    
    //MARK:- Private Method
//    func AddressTapHandler() {
//
//        let autocompleteController = GMSAutocompleteViewController()
//        autocompleteController.delegate = self
//        present(autocompleteController, animated: true, completion: nil)
//    }
    
}

//extension ServiceProfileVC : GMSAutocompleteViewControllerDelegate
//{
//    //MARK: Auto Complete Google Delegates
//
//    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
//
//        if let addressLines = place.addressComponents {
//            // Populate all of the address fields we can find.
//
//            for field in addressLines {
//
//                switch field.type {
//                case kGMSPlaceTypePostalCode:
//                    self.zipcode = field.name
//                default:
//                    //print("Type: \(field.type), Name: \(field.name)")
//                    print("")
//                }
//            }
//        }
//        self.latitude = place.coordinate.latitude
//        self.longitude = place.coordinate.longitude
//
//        streetAddress = place.formattedAddress!
//
//        self.tblDropDown.reloadData()
//
//        dismiss(animated: true, completion: nil)
//    }
//
//
//    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
//        // TODO: handle the error.
//        print("Error: ", error.localizedDescription)
//    }
//
//    // User canceled the operation.
//    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
//        dismiss(animated: true, completion: nil)
//    }
//
//    // Turn the network activity indicator on and off again.
//    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
//        UIApplication.shared.isNetworkActivityIndicatorVisible = true
//    }
//
//    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
//        UIApplication.shared.isNetworkActivityIndicatorVisible = false
//    }
//
//}

