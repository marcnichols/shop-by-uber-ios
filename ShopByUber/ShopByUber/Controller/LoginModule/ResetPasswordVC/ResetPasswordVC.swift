//
//  ResetPasswordVC.swift
//  ShopByUber
//
//  Created by Administrator on 6/2/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class ResetPasswordVC: UIViewController {

    //MARK: IBOutlets
    
    @IBOutlet var txtNewPassword: UITextField!
    @IBOutlet var txtConfirmPassword: UITextField!
    
    //MARK: View LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: IBAction
    
    @IBAction func btnReset_Click(_ sender: UIButton) {
     
        if txtNewPassword.text == "" {
        
            self.showSimpleAlert(title: "Alert", message: Constant.ValidationMessage.kEnterPassword)
            
        }else if txtConfirmPassword.text == "" {
        
            self.showSimpleAlert(title: "Alert", message: Constant.ValidationMessage.kEnterConfirmPassword)
            
        }else if txtConfirmPassword.text != txtNewPassword.text {
            
            self.showSimpleAlert(title: "Alert", message: Constant.ValidationMessage.kConfirmPassword)
            
        }else{
        
            self.callResetPasswordAPI(password: txtNewPassword.text)
        }
        
    }
    
    @IBAction func vwBG_Clicked(_ sender: UITapGestureRecognizer) {
        
        self.view.endEditing(true)
        
    }
    
    
    //MARK: API Call
    
    func callResetPasswordAPI(password:String?){
        
        User.callResetPasswordAPI(password: password, success: { (dict) in
            
            
                
                let userId = dict?["userId"] as? String
                DataModel.sharedInstance.setValueInUserDefaults(value: userId, key: Constant.DataModelKey.KUserId)
                
                if dict?["number_verified"] as! Bool {
                    
                    //Verified User
                    let sessionId = dict?["sessionId"] as? String
                    let userType = dict?["userRole"] as? Int
                    let email = dict?["email"] as? String
                    let isProfileCreated = dict?["isCompanyProfileCreated"] as? Bool
                    
                    DataModel.sharedInstance.setValueInUserDefaults(value: isProfileCreated ?? true, key: Constant.DataModelKey.kIsProfileCreated)
                    
                    let mobilePhone = dict?["mobilePhone"] as? String
                    let name = dict?["name"] as? String
                    let firstName = dict?["firstName"] as? String
                    let lastName = dict?["lastName"] as? String
                    let userImage = dict?["profileImage"] as? String
                    let description = dict?["description"] as? String
                    
                    let countryCode = dict?["countryCode"] as? String
                    let phoneNumber = dict?["phoneNumber"] as? String
                    let ezListScreenName = dict?["ezListScreenName"] as? String
                    let promoCode = dict?["promoCode"] as? String
                    
                    DataModel.sharedInstance.setUserDataWhenLogin(userID: userId, sessionId: sessionId, userType: userType, email: email, name: name, firstName: firstName, lastName: lastName, mobile: mobilePhone,phone:phoneNumber,countryCode: countryCode,userProfile: userImage, ezListScreenName: ezListScreenName, description: description, promoCode: promoCode)
                    
                    if DataModel.sharedInstance.onFetchUserLoginType() == Constant.UserType.Buyer {
                        
                        //Buyer DashBoard
                        
                        let objBuyerVC = BuyerVC(nibName: "BuyerVC", bundle: nil) as BuyerVC
                        _ = self.navigationController?.pushViewController(objBuyerVC, animated: true)
                        
                    }else{
                        
                        //Seller
                        
                        if dict?["isCompanyProfileCreated"] as! Bool {
                            
                            //Profile Created
                            
                            //Dashboard
                            
                            let sellerVC = SellerVC(nibName: "SellerVC", bundle: nil) as SellerVC
                            _ = self.navigationController?.pushViewController(sellerVC, animated: true)
                            
                        }else{
                            
                            //Profile Not Created
                            
                            let serviceProfile = ServiceProfileVC(nibName: "ServiceProfileVC", bundle: nil) as ServiceProfileVC
                            _ = self.navigationController?.pushViewController(serviceProfile, animated: true)
                            
                        }
                        
                    }
                    
                }else{
                    
                    //Not Verified User
                    let mobileNumber = dict?["mobilePhone"] as? String
                    let counrtyCode = dict?["countryCode"] as? String

                    let numberVerification = NumberVerificationVC(nibName: "NumberVerificationVC", bundle: nil) as NumberVerificationVC
                    numberVerification.countryCode = counrtyCode ?? ""
                    numberVerification.mobileNumber = mobileNumber ?? ""
                    _ = self.navigationController?.pushViewController(numberVerification, animated: true)
                }
                
            
            
        }) {
            
        }
        
    }
    
}

extension ResetPasswordVC : UITextFieldDelegate
{
    //MARK: UITextField Delegates
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtNewPassword {
            
            txtConfirmPassword.becomeFirstResponder()
            
        }else{
            
            textField.resignFirstResponder()
            
        }
        
        return true
        
    }
}
