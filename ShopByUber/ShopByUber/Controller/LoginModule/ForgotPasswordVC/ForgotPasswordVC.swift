//
//  ForgotPasswordVC.swift
//  ShopByUber
//
//  Created by Administrator on 6/2/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController {

    //MARK: IBOutlets
    
    @IBOutlet var lblHeading: UILabel!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var btnVerify: UIButton!
    @IBOutlet var btnResendCode: UIButton!
    @IBOutlet weak var vwSeperatorLine: UIView!
    
    //MARK: Variable
    var email = ""
    
    //MARK: View LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialConfig()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: IBAction
    
    @IBAction func btnBack_Clicked(_ sender: UIButton) {
    
        _ =  _ = navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func btnVerify_Clicked(_ sender: UIButton) {
        
        if btnVerify.tag == 0 {
        
            //Send Code
            
            if txtEmail.text == "" {
                
                self.showSimpleAlert(title: "Alert", message: Constant.ValidationMessage.kEmail)
                
            } else if !Utilities.isValidEmail(txtEmail.text!) {
                self.showSimpleAlert(title: "Alert", message: Constant.ValidationMessage.kValidEmail)
            }else{
                
                self.email = txtEmail.text ?? ""
                self.callSendCodeAPI(email: txtEmail.text) //Send Email API Call
                
               
            }
            
        }else{
        
            //Verify Code
            
            if txtEmail.text == "" {
            
                self.showSimpleAlert(title: "Alert", message: Constant.ValidationMessage.kVarificationCode)
                
            }else{
            
                self.callVerifyCodeAPI(code: txtEmail.text) //Varification code API Call
                
            }
            
        }
        
    }

    @IBAction func btnResendCode_Click(_ sender: UIButton) {
    
        if btnVerify.tag == 1 {
        
           self.callSendCodeAPI(email: email)
            
        }else{
        
            if txtEmail.text == "" {
                
                self.showSimpleAlert(title: "Alert", message: Constant.ValidationMessage.kEmail)
                
            }else if !Utilities.isValidEmail(txtEmail.text!) {
                self.showSimpleAlert(title: "Alert", message: Constant.ValidationMessage.kValidEmail)
            }else{
                
                self.email = txtEmail.text ?? ""
                self.callSendCodeAPI(email: txtEmail.text)
                
            }
        
        }
        
        
    }
    
    @IBAction func vwBG_Clicked(_ sender: UITapGestureRecognizer) {
        
        self.view.endEditing(true)
        
    }
    
    //MARK: Private Methods
    
    func initialConfig(){
        
        Utilities.setupAttributedButton(range: NSRange(location:30,length:10), btn: btnResendCode)
        //btnResendCode.isUserInteractionEnabled = false
        btnResendCode.isHidden = true
        self.vwSeperatorLine.isHidden = true
    }
    
    //MARK: API CAll
    
    func callSendCodeAPI(email:String?){
    
        User.callSendCodeForResetCodeAPI(email: email, success: { (data) in
            
            self.txtEmail.text = ""
            self.txtEmail.placeholder = Constant.Message.kEnterVarificationCode
            self.txtEmail.placeHolderColor = UIColor(red: 153/255.0, green: 153/255.0, blue: 153/255.0, alpha: 1.0)
            self.btnVerify.setTitle("VERIFY", for: .normal)
            self.btnVerify.tag = 1
            self.txtEmail.keyboardType = .numberPad
            let userId = data?["userId"] as? String
            DataModel.sharedInstance.setValueInUserDefaults(value: userId, key: Constant.DataModelKey.KUserId)
            //let verficationCode = data?["verficationCode"] as? Int
            //self.txtEmail.text = "\(verficationCode!)"
            self.showSimpleAlert(title: "Success", message: Constant.Message.kVerificationCodeSent)
            
            //self.btnResendCode.isUserInteractionEnabled = false
            self.btnResendCode.isHidden = false
            self.vwSeperatorLine.isHidden = false
            
            
        }, failure: {
            
        })
    
    }
    
    func callVerifyCodeAPI(code:String?){
        
        User.callVerifyCodeForgotAPI(code: code, success: { (data) in
            
            let resetPassword = ResetPasswordVC(nibName: "ResetPasswordVC", bundle: nil) as ResetPasswordVC
            _ = self.navigationController?.pushViewController(resetPassword, animated: true)
            
        }) { 
            
        }
        
    }
}

extension ForgotPasswordVC : UITextFieldDelegate
{
    //MARK: TextField Delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
