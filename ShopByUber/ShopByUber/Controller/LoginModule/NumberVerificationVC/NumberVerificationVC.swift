//
//  NumberVerificationVC.swift
//  ShopByUber
//
//  Created by Administrator on 5/25/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class NumberVerificationVC: UIViewController {

    //MARK: IBOutlet
    
    @IBOutlet var txtMobileNumber: UITextField!
    @IBOutlet var txtCode: UITextField!
    @IBOutlet var btnResendCode: UIButton!
    
    var isFromLogin = false
    //Variables
    
    var userType = 0
    var mobileNumber = ""
    var countryCode: String?
    var code = 0
    
    //MARK: View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isFromLogin {
            self.showSimpleAlert(title: "Alert", message: Constant.ValidationMessage.kUserNotVerify)
        }
        
        self.initialConfig()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: IBAction
    
    @IBAction func vwBG_Tapped(_ sender: UITapGestureRecognizer) {
        
        self.view.endEditing(true)
        
    }

    @IBAction func btnVerify_Clicked(_ sender: UIButton) {
        
        if txtCode.text == "" {
        
            self.showSimpleAlert(title: "Alert", message: Constant.ValidationMessage.kVarificationCode)
            
        }else{
            
            User.callVerifyCodeAPI(userId: DataModel.sharedInstance.onFetchUserId(), mobileNumber: txtMobileNumber.text, code: txtCode.text, success: { (dict) in
                
                let userId = dict?["userId"] as? String
                let sessionId = dict?["sessionId"] as? String
                let userType = dict?["userRole"] as? Int
                let email = dict?["email"] as? String
                let isProfileCreated = dict?["isCompanyProfileCreated"] as? Bool
                
                DataModel.sharedInstance.setValueInUserDefaults(value: isProfileCreated ?? true, key: Constant.DataModelKey.kIsProfileCreated)
                
                let mobilePhone = dict?["mobilePhone"] as? String
                let name = dict?["name"] as? String
                let firstName = dict?["firstName"] as? String
                let lastName = dict?["lastName"] as? String
                let userImage = dict?["profileImage"] as? String
                let description = dict?["description"] as? String
                let countryCode = dict?["countryCode"] as? String
                let phoneNumber = dict?["phoneNumber"] as? String
                let legalBusinessName = dict?["legalBusinessName"] as? String
                let ezListScreenName = dict?["ezListScreenName"] as? String
                let promoCode = dict?["promoCode"] as? String ?? ""
                let city = dict?["city"] as? String ?? ""
                let state = dict?["state"] as? String ?? ""
                let address = dict?["address"] as? String ?? ""
                
                DataModel.sharedInstance.setUserDataWhenLogin(userID: userId, sessionId: sessionId, userType: userType, email: email, name: name, firstName: firstName, lastName: lastName, mobile: mobilePhone,phone:phoneNumber,countryCode: countryCode,userProfile: userImage,ezListScreenName: ezListScreenName, description: description, promoCode: promoCode)
                DataModel.sharedInstance.setValueInUserDefaults(value: city, key: Constant.DataModelKey.kCity)
                DataModel.sharedInstance.setValueInUserDefaults(value: state, key: Constant.DataModelKey.kState)
                DataModel.sharedInstance.setValueInUserDefaults(value: address, key: Constant.DataModelKey.kaddress)
                
                //Quickblox
                QuickBloxManager.sharedInstance.checkAndLoginInQuickBlox()
                
                NotificationCenter.default.post(name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil, userInfo: nil)
                if DataModel.sharedInstance.onFetchUserLoginType() == Constant.UserType.Buyer {
                    
                    //Buyer DashBoard
                    let buyer = BuyerVC(nibName: "BuyerVC", bundle: nil) as BuyerVC
                    _ = self.navigationController?.pushViewController(buyer, animated: true)
                    
                }else{
                    
                    //Seller
                    UserDefaults.standard.set(legalBusinessName, forKey: Constant.DataModelKey.kLegalBusinessName)
                    UserDefaults.standard.synchronize()
                    let serviceProfile = ServiceProfileVC(nibName: "ServiceProfileVC", bundle: nil) as ServiceProfileVC
                    serviceProfile.isFromRegistration = true
                    _ = self.navigationController?.pushViewController(serviceProfile, animated: true)
                    
                }
                
            }, failure: { 
                
            })
            
        }
    }
   
    @IBAction func btnSendAgain_Clicked(_ sender: UIButton) {
        self.txtCode.text = ""
        let mobileNo = txtMobileNumber.text?.replacingOccurrences(of: "-", with: "")

        User.callSendVerificationCodeAPI(userId: DataModel.sharedInstance.onFetchUserId(), mobileNumber: mobileNo, success: { (dict) in
            
            _ = dict?["Data"] as? [String:Any]
            self.showSimpleAlert(title: "", message:dict?["Message"] as? String ?? "")
            
        }) { 
            
        }
    }
    
    //MARK: Private Methods
    
    func initialConfig(){
        
        Utilities.setupAttributedButton(range: NSRange(location:30,length:10), btn: btnResendCode)
        //self.txtCode.text = "\(self.code)"
        self.txtMobileNumber.text = self.countryCode! + self.mobileNumber.toPhoneNumber()
    }
    
}

extension NumberVerificationVC : UITextFieldDelegate
{
    //MARK: UITextField Delegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtCode
        {
            return Utilities.checkMaximumTextLenght(textFiled: txtCode, text: string, textLength: 4)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

