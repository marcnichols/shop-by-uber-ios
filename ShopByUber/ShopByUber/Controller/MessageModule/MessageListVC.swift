//
//  MessageListVC.swift
//  The EZ List
//
//  Created by mac on 2/5/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

import UIKit
import Quickblox

class MessageListVC: BaseView {
    
    //MARK:- IBOutlet
    @IBOutlet var tableView: UITableView!
    
    //MARK:- Variables
    var refreshControl = UIRefreshControl()
    var arrDialogList = NSMutableArray()

    //MARK:- Page life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.register(UINib(nibName: "ChatDialogCell", bundle: nil), forCellReuseIdentifier: "ChatDialogCell")
        self.refreshControl.addTarget(self, action: #selector(refreshList), for: .valueChanged)
        if #available(iOS 10.0, *) {
            self.tableView.refreshControl = self.refreshControl
        } else {
            self.tableView.addSubview(self.refreshControl)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        ChatService.sharedInstance.delegate = self
        ChatService.sharedInstance.chatConnectHandler = {(isSuccess) in
            if isSuccess {
                KVNProgress.show()
                ChatService.sharedInstance.onRequestDialogs(completion: { (isSuccess) in })
            }
        }
        ChatService.sharedInstance.getDialogCompletionBlock = {(isSuccess) in
            if !isSuccess {
                print("Failed to fetch chat dialogs")
            } else {
                print("MessageListVC-getDialogCompletionBlock")
                KVNProgress.dismiss()
                self.sortAndSetupChatListing()
                self.stopRefreshing()
            }
        }
        
        // Fetch dialogs
        self.fetchChatDialogs()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        ChatService.sharedInstance.delegate = nil
        ChatService.sharedInstance.chatConnectHandler = nil
        ChatService.sharedInstance.getDialogCompletionBlock = nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- Private methods
    func fetchChatDialogs() {
        
        KVNProgress.show()
        if !QuickBloxManager.sharedInstance.isUserLoggedIn {
            
            if DataModel.sharedInstance.onFetchUserId() != "" {
                let userID = NSString(format: "%d",DataModel.sharedInstance.onFetchUserId()) as String
                QuickBloxManager.sharedInstance.onLoginWithUserLoginID(userLogindID: userID) { (success, user) in
                    if !success {
                        KVNProgress.dismiss()
                        Utilities.showAlertView(title: nil, message: Constant.serverAPI.errorMessages.kServerErrorMessage)
                    }
                }
            } else {
                KVNProgress.dismiss()
                Utilities.showAlertView(title: nil, message: "User id not found for quickblox login.")
            }
        } else if !ChatService.sharedInstance.isChatConnected() {
            
            if let qbUser = QuickBloxManager.sharedInstance.currentUser {
                ChatService.sharedInstance.onConnectUserToChat(user: qbUser, completion: { (error) in
                    if let errorIs = error {
                        KVNProgress.dismiss()
                        //Utilities.showAlertView(title: nil, message: errorIs.localizedDescription)
                    }
                })
            } else {
                KVNProgress.dismiss()
                Utilities.showAlertView(title: nil, message: "Unable to find quickblox user")
            }
        } else if !ChatService.sharedInstance.isDialogFetched() {
            
            ChatService.sharedInstance.onRequestDialogs(completion: { (isSuccess) in })
            
        } else if ChatService.sharedInstance.isDialogFetched() {
            
            KVNProgress.dismiss()
            self.sortAndSetupChatListing()
            self.stopRefreshing()
            
        } else {
            KVNProgress.dismiss()
            Utilities.showAlertView(title: nil, message: "Try again.")
        }
    }
    
    func sortAndSetupChatListing() {
        
        self.arrDialogList = ChatService.sharedInstance.arrayDialogs ?? NSMutableArray()
        
        let arrSorted = self.arrDialogList.sorted(by: { (first, second) -> Bool in
            return (first as? QBChatDialog)?.lastMessageDate ?? Date(timeIntervalSince1970: 0) > (second as? QBChatDialog)?.lastMessageDate ?? Date(timeIntervalSince1970: 0)
        })
        
        self.arrDialogList.removeAllObjects()
        self.arrDialogList.addObjects(from: arrSorted)
        
        self.tableView.reloadData()
    }
    
    func refreshList() {
        
        if self.refreshControl.isRefreshing {
            ChatService.sharedInstance.onRequestDialogs(completion: { (isSuccess) in
                self.stopRefreshing()
            })
        }
    }
    
    func stopRefreshing() {
        if self.refreshControl.isRefreshing {
            self.refreshControl.endRefreshing()
        }
    }
}

//MARK: - UITableViewDelegate, UITableViewDataSource
extension MessageListVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.arrDialogList.count > 0 {
            self.tableView.backgroundView = nil
            return 1
        } else {
            let emptyView = UILabel(frame: tableView.frame)
            emptyView.text = "No dialog found"
            emptyView.font = UIFont(name: "SourceSansPro-Bold", size: Constant.DeviceType.IS_IPAD ? 18 : 12)
            emptyView.textAlignment = .center
            emptyView.textColor = UIColor.gray
            self.tableView.backgroundView = emptyView
            return 0
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constant.DeviceType.IS_IPAD || Constant.DeviceType.IS_IPAD_PRO ? 75 : 50
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrDialogList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatDialogCell") as! ChatDialogCell
        cell.backgroundColor = UIColor.clear
            
        let objDialog = self.arrDialogList.object(at: indexPath.row) as? QBChatDialog
        cell.lblName.text = objDialog?.name ?? ""
        cell.imgUserProfile.sd_setImage(with: URL(string: objDialog?.photo ?? ""), placeholderImage: #imageLiteral(resourceName: "userPlaceholder"))
        
//        if let unread = objDialog?.unreadMessagesCount, unread > 0 {
//            cell.lblUnread.isHidden = false
//            cell.lblUnread.text = String(format: "%ld", unread)
//        } else {
//            cell.lblUnread.isHidden = true
//        }
        
        cell.selectionStyle = .default
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let objDialog = self.arrDialogList.object(at: indexPath.row) as? QBChatDialog
        let privateChatVC = PrivateChatVC(nibName: "PrivateChatVC", bundle: nil)
        privateChatVC.strDialogId = objDialog?.id
        privateChatVC.strReceiverId = objDialog?.data?["receiverId"] as? String
        privateChatVC.strDialogName = objDialog?.name
        self.navigationController?.pushViewController(privateChatVC, animated: true)
    }
}

extension MessageListVC: ChatServiceDelegate {
    
    func didFinishedWithFetchingUnreadCountForDialog(dialog: QBChatDialog, unreadCount: Int) {
        print("didFinishedWithFetchingUnreadCountForDialog")
    }
    
    func didAddNewDialogInList() {
        print("didAddNewDialogInList")
    }
    
    func didReceiveSystemMessage(message: QBChatMessage) {
        print("didReceiveSystemMessage")
    }
}
