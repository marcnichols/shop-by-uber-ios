//
//  PrivateChatVC.swift
//  The EZ List
//
//  Created by mac on 2/2/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

import UIKit
import Quickblox

class PrivateChatVC: BaseView {
    
    //MARK:- Outlets
    @IBOutlet var lblDialogName: UILabel!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var btnSend: UIButton!
    @IBOutlet var txtViewContainer: UIView!
    @IBOutlet weak var txtViewMessage: AutoGrowingTextView!
    @IBOutlet var someViewBottomConstraint: NSLayoutConstraint!
    
    // Data Pass
    var occupantId: Int? //quickBloxId
    var strDialogId: String?
    var strReceiverId: String?
    var strDialogName: String?
    var strRequestID: String?
    
    //MARK:- Variables
    var chatDialog: QBChatDialog?
    var arrayMessages = NSMutableArray()
    var isLoadMore = false
    var totalMessage: UInt = 0
    var activityIndicator = UIActivityIndicatorView()
    var isfromlocalNotification = false
    
    // to check chat available or not
       var ischatAvailabel = true
    //MARK:- Page Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
       // self.chatEnable(requestId : self.strRequestID!)
        self.initialConfig()
        //set notification
        NotificationCenter.default.addObserver(self, selector:#selector(ReciveSilentNotification), name: NSNotification.Name(rawValue: "SilentNotification"), object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      
       txtViewMessage.textContainerInset = UIEdgeInsets(top: 8, left: 0, bottom: 5, right: 0)
      
    }
    override func viewDidAppear(_ animated: Bool) {
        if ischatAvailabel == true
        {
            self.textViewDidChange(self.txtViewMessage)
            IQKeyboardManager.shared().isEnabled = false
            Singleton.sharedManager.currentView = self
            ChatService.sharedInstance.delegateMessage = self
            self.addKeyboardObserver()
            
            ChatService.sharedInstance.chatConnectHandler = {(isSccess) in
                
                if isSccess {
                    ChatService.sharedInstance.onRequestDialogs(completion: { (isSuccess) in
                    })
                }
            }
            
            ChatService.sharedInstance.getDialogCompletionBlock = { (isSuccess) in
                
                if !isSuccess {
                    if KVNProgress.isVisible() {
                        KVNProgress.dismiss()
                    }
                    NSLog("Failed to fetch dialog.")
                }
                self.checkForDialogsWithSender(strDialogId: self.strDialogId ?? "")
            }
            self.checkAndLoadChat()
            appDeletgate.isOnPrivateChatVC = true
            
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        Singleton.sharedManager.currentView = nil
        ChatService.sharedInstance.delegateMessage = nil
        ChatService.sharedInstance.chatConnectHandler = nil
        ChatService.sharedInstance.getDialogCompletionBlock = nil
        IQKeyboardManager.shared().isEnabled = true
        self.removeKeyboardObserver()
        appDeletgate.isOnPrivateChatVC = false
        
        if KVNProgress.isVisible() {
            KVNProgress.dismiss()
        }
        
        //remove notification
          NotificationCenter.default.removeObserver("SilentNotification")
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Silent notification method
    func ReciveSilentNotification(notification: NSNotification){
        
        if notification.userInfo!["requestId"] as? String != nil
        {
            if notification.userInfo!["requestId"] as? String == self.strRequestID
            {
                self.chatEnable(requestId : (notification.userInfo!["requestId"] as? String)!)
            }
        }
    }
    
    //MARK: - Notification observer for keyboard
    func addKeyboardObserver () {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notifcation:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: .UIKeyboardWillHide, object: nil)
    }
    
    func removeKeyboardObserver () {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    //MARK: - Keyboard Handler Methods
    func keyboardWillShow(notifcation : NSNotification)  {
        
        let keyboardBounds: CGRect? = (notifcation.userInfo?[UIKeyboardFrameEndUserInfoKey] as? CGRect)
        self.someViewBottomConstraint.constant = (keyboardBounds?.size.height)!
        UIView.animate(withDuration: 0.2, animations: {
            self.view.layoutIfNeeded()
        })
        self.onScrollTableViewLastIndex()
    }
    
    func keyboardWillHide(notification : NSNotification) {
        
        self.someViewBottomConstraint.constant = 0
        UIView.animate(withDuration: 0.2, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    //MARK:- Private methods
    func initialConfig() {
        
        //Table View
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "ChatRightCell", bundle: nil), forCellReuseIdentifier: "ChatRightCell")
        self.tableView.register(UINib(nibName: "ChatLeftCell", bundle: nil), forCellReuseIdentifier: "ChatLeftCell")
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 60.0
        self.tableView.transform = CGAffineTransform(rotationAngle: -(CGFloat)(Double.pi))
        
        //Activity Indicator
        self.activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        self.activityIndicator.frame = CGRect(x: 0, y: 0, width: self.tableView.frame.size.width, height: 44)
        self.activityIndicator.backgroundColor = UIColor.clear
        self.tableView.tableFooterView = self.activityIndicator
        
        // Tap gesture for dismiss keyboard
        let gestureBackground = UITapGestureRecognizer.init(target: self, action: #selector(onHandleBackgroundTap(gesture:)))
        self.view.addGestureRecognizer(gestureBackground)
        
        // Data Setup
        self.lblDialogName.text = self.strDialogName
    }
    
    //MARK : - Status checking for chat
    func chatEnable(requestId : String)
    {
        var dict                = [String : Any]()
        dict["userId"]          = DataModel.sharedInstance.onFetchUserId()
        dict["sessionId"]       = DataModel.sharedInstance.onFetchSessionId()
        dict["requestId"]       = requestId
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kgetRequestStatus, inputParams: dict, success: { (dict) in
            
            KVNProgress.dismiss()
            
            if dict?["Result"] as? Bool == true {
                
                print("Success")
                if let data = dict?["Data"] as? [String:Any]{
                    print("Data::::::=========>>>",data)
                    
                    let status = data["status"] as! Int
                    // let status:Int = 5
                    print("Status====>",status)
                    
                    if  status == Constant.RequestStatus.Completed.rawValue {
                        self.ischatAvailabel = false
                        let alert = UIAlertController(title: nil, message: "This service is already been completed. You are no longer allow to make a conversation.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {(action:UIAlertAction!) in
                            print("you have pressed the ok button")
                            _ = self.navigationController?.popViewController(animated: true)
                            
                        }))
                        // alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                        
                        let window = UIApplication.shared.keyWindow
                        window?.rootViewController?.present(alert, animated: true, completion: nil)
                    }
                }
            }
            else{
                
                let msg = dict?["Message"] as? String
                Utilities.showAlertView(title: "", message: msg)
                _ = self.navigationController?.popViewController(animated: true)
            }
            
        }) { error in
            
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
            _ = self.navigationController?.popViewController(animated: true)
        }
        
        
    }
    func checkAndLoadChat() {
        
        if !QuickBloxManager.sharedInstance.isUserLoggedIn {
            
            KVNProgress.show()
            let userId = DataModel.sharedInstance.onFetchUserId()
            QuickBloxManager.sharedInstance.onLoginWithUserLoginID(userLogindID: userId, completionHandler: { (isSuccess, qbUser) in
                if !isSuccess {
                    KVNProgress.dismiss()
                    Utilities.showAlertView(title: nil, message: "Unable to login with quickblox. Please try again later.")
                } else {
                    KVNProgress.show()
                }
            })
        } else if !ChatService.sharedInstance.isChatConnected() {
            
            KVNProgress.show()
            if let qbUser = QuickBloxManager.sharedInstance.currentUser {
                ChatService.sharedInstance.onConnectUserToChat(user: qbUser, completion: { (error) in
                    if let errorIs = error {
                        KVNProgress.dismiss()
                        Utilities.showAlertView(title: nil, message: errorIs.localizedDescription)
                    }
                })
            } else {
                KVNProgress.dismiss()
                Utilities.showAlertView(title: nil, message: "Unable to find quickblox user for chat login")
            }
            
        } else if let dialogId = self.strDialogId {
            
            KVNProgress.show()
            if let _ = ChatService.sharedInstance.arrayDialogs {
                self.checkForDialogWith(dialogId: dialogId)
            } else {
                ChatService.sharedInstance.onRequestDialogs(completion: { (isSuccess) in })
            }
        } else if let _ = self.occupantId {

            KVNProgress.show()
            if let _ = ChatService.sharedInstance.arrayDialogs {
                self.checkForDialogsWithSender(strDialogId: "")
            } else {
                ChatService.sharedInstance.onRequestDialogs(completion: { (isSuccess) in })
            }
        } else {
            Utilities.showAlertView(title: "Error", message: "Try Again.")
        }
    }
    
    func onScrollTableViewLastIndex() {
        
        if self.arrayMessages.count > 0 {
            let indexPath = NSIndexPath(item: 0, section: 0)
            self.tableView.scrollToRow(at: indexPath as IndexPath, at: .top, animated: false)
        }
    }
    
    func onHandleBackgroundTap(gesture : UITapGestureRecognizer) {
        self.txtViewMessage.resignFirstResponder()
    }
    
    // Check in chat service for dialog id
    func checkForDialogsWithSender(strDialogId: String) {
        
        let arrDialogs = ChatService.sharedInstance.arrayDialogs ?? NSMutableArray()
        var isDialogFound = false
        if let occupantIs = self.occupantId {
            for (_, value) in arrDialogs.enumerated() {
                let valueDialog = value as! QBChatDialog
                let senderId = NSNumber(value: UInt(occupantIs))
                if let arrOccupants = valueDialog.occupantIDs, arrOccupants.contains(senderId) {
                    isDialogFound = true
                   
                    //Fetch Chat Records
                    self.chatDialog = valueDialog
                    self.getMessagesFromDialog(strDialogId: (self.chatDialog?.id)!)
                    self.onFetchMessagesCount()
                    appDeletgate.strDialogID = self.chatDialog?.id
                    break
                }
            }
        } else if strDialogId != "" {
            for (_, value) in arrDialogs.enumerated() {
                let valueDialog = value as! QBChatDialog
                if let dialogIdIs = valueDialog.id, dialogIdIs == strDialogId {
                    isDialogFound = true
                    
                    //Fetch Chat Records
                    if let qbUserIds = valueDialog.occupantIDs {
                        for qbId in qbUserIds {
                            if Int(qbId) != DataModel.sharedInstance.onFetchQuickBloxId() {
                                self.occupantId = Int(qbId)
                            }
                        }
                    }
                    self.chatDialog = valueDialog
                    self.getMessagesFromDialog(strDialogId: (self.chatDialog?.id)!)
                    self.onFetchMessagesCount()
                    appDeletgate.strDialogID = self.chatDialog?.id
                    break
                }
            }
        }
        if !isDialogFound {
            if let occupantId = self.occupantId {
                self.createNewChatDialogWithUser(opponantIdIs: occupantId)
            } else {
                self.getDialogDetailsFor(dialogId: strDialogId)
            }
        }
    }
    
    func checkForDialogWith(dialogId: String)  {
        
        let chatTempDialog: QBChatDialog? = ChatService.sharedInstance.dictDialogs?.object(forKey: dialogId) as? QBChatDialog
        if chatTempDialog != nil {
            self.chatDialog = chatTempDialog
            self.getMessagesFromDialog(strDialogId: (self.chatDialog?.id)!)
            self.onFetchMessagesCount()
            appDeletgate.strDialogID = self.chatDialog?.id
        } else {
            self.getDialogDetailsFor(dialogId: dialogId)
        }
    }
    
    func getDialogDetailsFor(dialogId: String) {
        
        ChatService.sharedInstance.fetchOpenDialogDetailFrom(dialogId: dialogId) { (chatDialog) in
            
            if let dialogIs = chatDialog {
                //Fetch Chat Records
                if let qbUserIds = dialogIs.occupantIDs {
                    for qbId in qbUserIds {
                        if Int(qbId) != DataModel.sharedInstance.onFetchQuickBloxId() {
                            self.occupantId = Int(qbId)
                        }
                    }
                }
                self.chatDialog = dialogIs
                self.getMessagesFromDialog(strDialogId: dialogIs.id!)
                self.onFetchMessagesCount()
                appDeletgate.strDialogID = self.chatDialog?.id
            } else {
                KVNProgress.dismiss()
                Utilities.showAlertView(title: "Error", message: "No dialog found with id \(dialogId)")
            }
        }
    }
    
    func createNewChatDialogWithUser(opponantIdIs: Int) {
        
        ChatService.sharedInstance.onCreatePrivateDialog(userID: NSInteger(opponantIdIs)) { (response, dialog) in
            
            if dialog != nil {
                //Fetch Chat Records
                if let qbUserIds = dialog?.occupantIDs {
                    for qbId in qbUserIds {
                        if Int(qbId) != DataModel.sharedInstance.onFetchQuickBloxId() {
                            self.occupantId = Int(qbId)
                        }
                    }
                }
                //Fetch Chat Records
                self.chatDialog = dialog
                self.getMessagesFromDialog(strDialogId: (self.chatDialog?.id)!)
                self.onFetchMessagesCount()
                appDeletgate.strDialogID = self.chatDialog?.id
            } else {
                KVNProgress.dismiss()
                Utilities.showAlertView(title: "Error", message: "Error while creating dialog: \(response.error.debugDescription)")
            }
        }
    }
    
    func getMessagesFromDialog(strDialogId: String) {
        
        let params = ["sort_desc" : "date_sent"]
        let responsePage = QBResponsePage.init(limit: QBChatMsgLimit)
        QBRequest.messages(withDialogID: strDialogId, extendedRequest: params, for: responsePage, successBlock: { (response, messages, page) in
            
            KVNProgress.dismiss()
            self.arrayMessages.removeAllObjects()
            if messages.count > 0 {
                
                self.arrayMessages.addObjects(from: messages)
                self.onReadAllMessageFor(strDialogId: strDialogId)
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.onScrollTableViewLastIndex()
                }
            }
        }) { (error) in
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "Error", message: "Error while fetching chat from quickblox server")
        }
    }
    
    func onFetchMessagesCount() {
        
        if let objDialog = self.chatDialog {
            
            QBRequest.countOfMessages(forDialogID: objDialog.id!, extendedRequest: nil, successBlock: { (response, count) in
                if response.status == .OK {
                    if count > 0 {
                        self.totalMessage = count
                    }
                }
            }, errorBlock: { (error) in
                Utilities.showAlertView(title: "Error", message: "Error while fetching message count from quickblox server")
            })
        }
    }
    
    func onFetchMoreChatRecords() {
        
        self.activityIndicator.startAnimating()
        if self.totalMessage > UInt(self.arrayMessages.count) {
            
            let lastIndex = self.arrayMessages.count - 1
            let chatMessage = self.arrayMessages.object(at: lastIndex) as! QBChatMessage
            
            let responsePage = QBResponsePage(limit: QBChatMsgLimit)
            let timeInterval = (chatMessage.dateSent?.timeIntervalSince1970)! as TimeInterval
            let stringDate = NSString(format: "%f", timeInterval)
            let params = ["sort_desc" : "date_sent","date_sent[lt]" : stringDate] as [String : Any]
            
            QBRequest.messages(withDialogID: (self.chatDialog?.id)!, extendedRequest: params as? [String : String], for: responsePage, successBlock: { (response, messages, page) in
                
                self.isLoadMore = false
                self.activityIndicator.stopAnimating()
                
                if messages.count > 0 {
                    
                    self.arrayMessages.addObjects(from: messages)
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
            }, errorBlock: { (error) in
                self.isLoadMore = false
                self.activityIndicator.stopAnimating()
                Utilities.showAlertView(title: "Error", message: "Error while fetching more messages from quickblox server")
            })
        } else {
            self.isLoadMore = false
            self.activityIndicator.stopAnimating()
        }
    }
    
    func onReadAllMessageFor(strDialogId: String) {
        
        QBRequest.markMessages(asRead: nil, dialogID: strDialogId, successBlock: { (response) in
            if response.status == .OK {
                if let _ = ChatService.sharedInstance.arrayDialogs {
                    for (_,value) in (ChatService.sharedInstance.arrayDialogs?.enumerated())! {
                        let chatDialogTemp = value as! QBChatDialog
                        if chatDialogTemp.id == self.chatDialog?.id {
                            chatDialogTemp.unreadMessagesCount = 0
                            break
                        }
                    }
                }
            }
        }) { (error) in
            Utilities.showAlertView(title: "Error", message: "Error while mark message from quickblox server")
        }
    }
    
    func onReadMessage(for message : QBChatMessage) {
        
        QBChat.instance.read(message, completion: { (error) in
            print("Mark as Read")
        })
    }
    
    func onCheckSenderTypeID(userID : UInt) -> ChatSender {
        
        var chatSender : ChatSender = .kReceiver
        let chatID = UInt(DataModel.sharedInstance.onFetchQuickBloxId())
        if chatID == userID {
            chatSender = .kSender
        }
        return chatSender
    }
    
    func onInsertMessageAnimation(for indexpath: IndexPath?) {
        
        if let index = indexpath {
            UIView.setAnimationsEnabled(false)
            self.tableView.beginUpdates()
            self.tableView.insertRows(at: [index], with: .bottom)
            self.tableView.endUpdates()
            UIView.setAnimationsEnabled(true)
            
            UIView.animate(withDuration: 0.05, animations: {() -> Void in
                self.onScrollTableViewLastIndex()
                if self.arrayMessages.count <= 0 {
                    self.tableView.reloadData()
                }
            })
        }
    }
    
    func onSendPushNotfication(message : String) {
        
        if let _ = self.strReceiverId {
            
            var dictParam=[String:Any]()
            dictParam["userId"] = DataModel.sharedInstance.onFetchUserId()
            dictParam["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
            dictParam["message"] = message
            dictParam["dialogId"] = self.chatDialog?.id!
            dictParam["dialogName"] = self.chatDialog?.name
            dictParam["receiverId"] = self.strReceiverId
            dictParam["quickBloxId"] = DataModel.sharedInstance.onFetchQuickBloxId()
            dictParam["requestId"] =  self.strRequestID ?? ""
            
            APIManager.callAPI(url: Constant.serverAPI.URL.ksendQuickBloxNotification, inputParams: dictParam, success: { (response) in
                if response?["Result"] as? Bool == true {
                    print("true")
                } else {
                    print("false")
                }
            }, failure: { (errorIs) in
//                 Utilities.showAlertView(title: "Error", message: Constant.serverAPI.errorMessages.kNoInternetConnectionMessage)
            })
        }
    }
    
    //MARK:- IBActions
    @IBAction func btnSend_click(_ sender: Any) {
        
        if !self.txtViewMessage.text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            
            let messageIs = self.txtViewMessage.text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let senderID = UInt(DataModel.sharedInstance.onFetchQuickBloxId())
            let messsage = ChatService.sharedInstance.onCreateMessage(messageText: messageIs, dialogID: (self.chatDialog?.id)!, senderID: senderID)
            
            self.txtViewMessage.text = ""
            // For again disable button
            self.textViewDidChange(self.txtViewMessage)
            self.arrayMessages.insert(messsage, at: 0)
            
            let indexPath = IndexPath(row: 0, section: 0)
            self.onInsertMessageAnimation(for: indexPath)
            
            ChatService.sharedInstance.onSendMessage(message: messsage, dialog: self.chatDialog!, completion: { (error) in
                
                if error == nil {
                    self.onSendPushNotfication(message: messsage.text!)
                    print("Message send!!!")
                }
            })
        } else {
            return
        }
    }
    
    @IBAction override func btnBack_Click(_ sender: Any) {
        
        if isfromlocalNotification == true {
            _ = self.navigationController?.popToRootViewController(animated: true)
        }else {
             _ = self.navigationController?.popViewController(animated: true)
        }
    }
}

//MARK: - UITableViewDelegate, UITableViewDataSource
extension PrivateChatVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayMessages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let messageQB = arrayMessages.object(at: indexPath.row) as! QBChatMessage
        let sender = self.onCheckSenderTypeID(userID: messageQB.senderID)
        
        if sender == .kSender {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChatRightCell") as! ChatRightCell
            cell.selectionStyle = .none
            cell.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
            
            cell.rightBubble.backgroundColor = Utilities.colorWithHexString("4FA0EB")
            cell.lblMessageSent.textColor = UIColor.white
            cell.lblMessageSent.text = messageQB.text
            cell.lblSentTime.text = Utilities.onFetchMessageTime(date: messageQB.dateSent!)
            return cell
            
        } else {
           
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChatLeftCell") as! ChatLeftCell
            cell.selectionStyle = .none
            cell.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
            
            cell.leftBubble.backgroundColor = Utilities.colorWithHexString("EEEEEE")
            cell.lblMessage.textColor = UIColor.lightGray
            cell.lblMessage.text = messageQB.text
            cell.lblRcdTime.text = Utilities.onFetchMessageTime(date: messageQB.dateSent!)
            return cell
        }
    }
}

extension PrivateChatVC: ChatServiceMessagesDelegate, UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if let visibleCell = self.tableView.visibleCells.last {
            let indexPathForCell = self.tableView.indexPath(for: visibleCell)!
            let lastFifth = self.arrayMessages.count - 5
            if indexPathForCell.row == lastFifth && !self.isLoadMore {
                print("Load more data")
                self.isLoadMore = true
                self.onFetchMoreChatRecords()
            }
        }
    }
    
    func didReciveMessage(message: QBChatMessage) {
        
        if message.dialogID == self.chatDialog?.id {
            
            self.arrayMessages.insert(message, at: 0)
            let indexPath = NSIndexPath.init(row: 0, section: 0)
            self.onInsertMessageAnimation(for: indexPath as IndexPath)
            
            // read specific message
            self.onReadMessage(for: message)
            
            // update last message to local array
            self.chatDialog?.lastMessageUserID = (QBSession.current.currentUser?.id)!
            self.chatDialog?.lastMessageText = message.text
            self.chatDialog?.lastMessageDate = message.dateSent
        }
    }
    
    func didMessageReadforID(messageID: String, dialogID: String, readId: UInt) {
        
        if self.arrayMessages.count > 0 {
            
            for (_,value) in self.arrayMessages.enumerated() {
                let msgTemp = value as! QBChatMessage
                if messageID == msgTemp.id {
                    let numberIDs : NSNumber = NSNumber(value: readId)
                    msgTemp.readIDs?.append(numberIDs)
                    break;
                }
            }
            self.tableView.reloadData()
        }
    }
    
    func didMessageDelivered(messageID: String, dialogID: String, readId: UInt) {
        
        if self.arrayMessages.count > 0 {
            for (_,value) in self.arrayMessages.enumerated() {
                let msgTemp = value as! QBChatMessage
                
                if messageID == msgTemp.id {
                    let numberIDs : NSNumber = NSNumber(value: readId)
                    msgTemp.deliveredIDs?.append(numberIDs)
                    break;
                }
            }
            self.tableView.reloadData()
        }
    }
    
    func didRoomReceiveMessage(message: QBChatMessage, forDialogId: String) {
        
    }
}

extension PrivateChatVC: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        
        let sendImage = UIImage(named: "send")?.withRenderingMode(.alwaysTemplate)
        self.btnSend.setImage(sendImage, for: .normal)
        if !self.txtViewMessage.text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            self.btnSend.tintColor = Constant.Color.kGreen
        } else {
            self.btnSend.tintColor = UIColor.gray
        }
    }
}
