//
//  BaseView.swift
//  ShopByUber
//
//  Created by Chirag Kalsariya on 7/15/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit
import UserNotifications

class BaseView: UIViewController, UpdateCountDelegate, GMSMapViewDelegate,UserInfoPopupDelegate {
    
    //MARK:- IBOutlet
    @IBOutlet weak var btnNotify: UIButton!
    @IBOutlet var mapView:GMSMapView!
    
    @IBOutlet weak var vwBuyerTop: UIView!
    //MARK:- Variable
    var locations = [CLLocation]()
    var path = GMSMutablePath()
    var isFirstMessage = true
    var polyline = GMSPolyline()
    var segmentControl: WBSegmentControl!
    var userPopup = UserInfoPopup()
    
    var marker = [GMSMarker]()
    
    var routes:[[String:Any]]!
    
    @IBOutlet var vwPinDuration: UIView!
    @IBOutlet weak var lblPinDuration: UILabel!
    
    //MARK:- Variable
    //Stores reference on PubNub client to make sure what it won't be released.
    var client: PubNub!
    //let locationManager = CLLocationManager()
    var isRecordingActive = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (self.revealViewController() != nil)
        {
            self.revealViewController().tapGestureRecognizer()
            self.revealViewController().delegate = self
        }
        appDeletgate.delegate = self
        
        self.path = GMSMutablePath(path: path)
        
        Singleton.sharedManager.unreadHandler={(success) in
            if self.btnNotify != nil
            {
                if Singleton.sharedManager.unreadNotificationCount > 0
                {
                    self.btnNotify.badgeValue = Singleton.sharedManager.unreadNotificationCount.description
                }
                else
                {
                    self.btnNotify.badgeValue = ""
                }
                
            }
        }
        
//        if #available(iOS 10.0, *) {
//            UNUserNotificationCenter.current().requestAuthorization(options: [.alert,.badge,.sound])
//            { (success, error) in
//                if success {
//                    print("Permission Granted")
//                   
//                } else {
//                    print("There was a problem!")
//                    
//                }
//            }
//        } else {
//            // Fallback on earlier versions
//            let settings = UIUserNotificationSettings(types: [.alert,.badge,.sound], categories: nil)
//            UIApplication.shared.registerUserNotificationSettings(settings)
//            
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector:#selector(pushToBuyerRegister), name: NSNotification.Name(rawValue: Constant.kPushToBuyerRegister), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(pushToSellerRegister), name: NSNotification.Name(rawValue: Constant.kPushToSellerRegister), object: nil)
        
        if self.btnNotify != nil
        {
            if Singleton.sharedManager.unreadNotificationCount > 0
            {
                self.btnNotify.badgeValue = Singleton.sharedManager.unreadNotificationCount.description
            }
            else
            {
                self.btnNotify.badgeValue = ""
            }
        }
        
        if Singleton.sharedManager.isNotification
        {
            // Navigate
            self.HandlePushNotification(userInfo: Singleton.sharedManager.userInfo!)
        }
        
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Navgation bar button action
    //MARK:- IBActions
    @IBAction func btnMenu_Clicked(_ sender: Any) {
        
        if self.revealViewController() != nil
        {
            self.revealToggle(true)
        }
        
    }
    
    @IBAction func btnSearch_Clicked(_ sender: Any) {
        
        let objSearchVC = SearchCategoryVC(nibName: "SearchCategoryVC", bundle: nil) as SearchCategoryVC
        self.navigationController?.pushViewController(objSearchVC, animated: true)
    }
    
    @IBAction func btnNotification_Clicked(_ sender: Any) {
        let objNotificationVC = NotificationVC(nibName: "NotificationVC", bundle: nil) as NotificationVC
        self.navigationController?.pushViewController(objNotificationVC, animated: true)
    }
    
    @IBAction func btnCart_Clicked(_ sender: Any) {
        let objPayForServiceVC = PayForServiceVC(nibName: "PayForServiceVC", bundle: nil) as PayForServiceVC
        objPayForServiceVC.isFrom = trackProgress.isFromHome
        self.navigationController?.pushViewController(objPayForServiceVC, animated: true)
        
    }
    
    @IBAction func btnBack_Click(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func pushToBuyerRegister()
    {
        let register = RegisterVC(nibName: "RegisterVC", bundle: nil)
        register.isPush = true
        register.isForBuyer = true
        self.navigationController?.pushViewController(register, animated: false)
        self.perform(#selector(closeMenu), with: nil, afterDelay: 0)
    }
    
    func pushToSellerRegister()
    {
        let register = RegisterVC(nibName: "RegisterVC", bundle: nil)
        register.isPush = true
        register.isForBuyer = false
        self.navigationController?.pushViewController(register, animated: false)
        self.perform(#selector(closeMenu), with: nil, afterDelay: 0)
    }
    
    func closeMenu()
    {
        if self.revealViewController() != nil
        {
            self.revealToggle(true)
        }
        
    }
    
    //MARK: - UPDATENOTIFICATION Count Delegate Method
    func UpdateNotificationCount()
    {
        if self.btnNotify != nil
        {
            if Singleton.sharedManager.unreadNotificationCount != 0
            {
                self.btnNotify.badgeValue = Singleton.sharedManager.unreadNotificationCount.description
            }
        }else{
            // Post notification
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateNotificationCount"), object: nil)
        }
        
    }
    
    //MARK: Location Delegate
    func setCurretnLocationMarker(){
        
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        
        let location:CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: DataModel.sharedInstance.onFetchLatitude(), longitude: DataModel.sharedInstance.onFetchLongtitude())
     //   let location:CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 33.937029, longitude: -118.330058)
        mapView.animate(toLocation: location)
        mapView.camera = GMSCameraPosition.camera(withTarget: location, zoom:8)
    }
    
    //MARK:- Set Seller Marker
    func setSellerMarker(data:CompanyData){
        
        // print("latitude : \(String(describing: lat)) Longitude : \(String(describing: lon))")
        let locationMarker = GMSMarker()
        locationMarker.position = CLLocationCoordinate2D(latitude: data.latitude ?? 0.0, longitude: data.longitude ?? 0.0)
        locationMarker.icon = UIImage(named: "ic_marker")
        locationMarker.title = ""
        locationMarker.appearAnimation = .pop
        locationMarker.map = self.mapView
        locationMarker.userData = data
        marker.append(locationMarker)
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        print("try")
        if(marker.userData != nil)
        {
            self.userPopup = Bundle.main.loadNibNamed("UserInfoPupup", owner: self, options: nil)?[0] as! UserInfoPopup
            self.userPopup.frame = self.view.frame
            self.userPopup.delegate = self
            let data  = marker.userData as! CompanyData
            self.userPopup.lblUserName.text = data.comapnyScreenName
            self.userPopup.companyID = data.companyID!
            self.userPopup.ScreenName = data.companyName!
            self.userPopup.txtCompanyDetails.text = data.serviceDetail
            if data.companyProfile != nil{
                self.userPopup.imageProfile.sd_setImage(with: URL.init(string: (data.companyProfile)!), placeholderImage: nil, options: SDWebImageOptions.retryFailed, completed:{ (image, err, nil, url) in
                    if image == nil
                    {
                        self.userPopup.imgPlaceHolder.isHidden = false
                    }
                })
            }
            else
            {
                self.userPopup.imgPlaceHolder.isHidden = false
            }

            self.userPopup.txtCompanyDetails.textContainerInset = UIEdgeInsets.zero
            
            
            //for chage textview height on graterthan 100 height in compny detail
            let lblTemp = UILabel.init(frame: CGRect(x: 0, y: 0, width: self.userPopup.viewPopup.frame.size.width - 20, height: 21))
            lblTemp.numberOfLines = 0
            lblTemp.font = self.userPopup.txtCompanyDetails.font
            lblTemp.text = data.serviceDetail
            lblTemp.sizeToFit()
            
            if(lblTemp.frame.size.height > (Utilities.isDeviceiPad() ? 150 : 100))
            {
                self.userPopup.constarinttextViewHeight.constant = (Utilities.isDeviceiPad() ? 150 : 100);
            }
            else
            {
                self.userPopup.constarinttextViewHeight.constant = lblTemp.frame.size.height;
            }
            self.view.addSubview(self.userPopup)
        }
        return true
    }
    
    //MARK:- Set User Marker With Duration
    func setUserMarkerWithDuration(position:CLLocationCoordinate2D){
        self.lblPinDuration.layer.cornerRadius = self.lblPinDuration.frame.size.height / 2
        
        let locationMarker = GMSMarker()
        locationMarker.position = position
        locationMarker.iconView = vwPinDuration
        locationMarker.appearAnimation = .pop
        locationMarker.map = self.mapView
        marker.append(locationMarker)
        
        
    }
    
    func getDistanceDuration(startLocation:CLLocationCoordinate2D,endLocation:CLLocationCoordinate2D,success:@escaping(NSString)->(),responseFailed:@escaping(Bool)->())
    {
        let url = Constant.serverAPI.URL.kGetDuration +  "origins=\(startLocation.latitude),\(startLocation.longitude)&destinations=\(endLocation.latitude),\(endLocation.longitude)&key=\(Constant.googleRootsAPIKey)"
        
        APIManager.callAPI(url: url, inputParams: nil, success: { (response) in
            if response?["status"] as! String == "OK"
            {
                
                if let rows = response?["rows"] as? [[String:Any]]
                {
                    if let elements = rows[0]["elements"] as? [[String:Any]]
                    {
                        if elements.count > 0
                        {
                            let element = elements[0]
                            if element["status"] as! String == "OK"
                            {
                                let duration = element["duration"] as? [String:Any] ?? [:]
                                let value = duration["text"] as? String ?? ""
                                success(value as NSString)
                                let temp = (value as NSString).uppercased
                                let durations = temp.components(separatedBy: " ")
                                
                                let paragraphStyle = NSMutableParagraphStyle()
                                paragraphStyle.lineSpacing = 5
                                
                                var formated = Utilities.decorateText(attributedString: NSMutableAttributedString(attributedString: NSAttributedString(string: temp)), decorateString: durations[0], decorate: [NSFontAttributeName:UIFont.systemFontSemiBold(with: 12)])
                                
                                formated = Utilities.decorateText(attributedString: NSMutableAttributedString(attributedString: NSAttributedString(string: temp)), decorateString: durations[1], decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: 10)])
                                
                                //formated = Utilities.decorateText(attributedString: formated, decorateString: formated.string, decorate: [NSParagraphStyleAttributeName:paragraphStyle])
                                self.lblPinDuration.attributedText = formated
                                self.lblPinDuration.textAlignment = .center
                                self.setUserMarkerWithDuration(position: endLocation)
                            }
                            
                        }
                    }
                }
            }
            else
            {
                responseFailed(true)
            }
        }) { (error) in
            
        }
    }
    
    
    //MARK:- Initialize and configure PubNub client instance
    func pubnubConfiguration(withCannel:String)
    {
        let configuration = PNConfiguration(publishKey: Constant.PubNubInfo.publishKey, subscribeKey: Constant.PubNubInfo.subscribeKey)
        self.client = PubNub.clientWithConfiguration(configuration)
        self.client.addListener(self)
        // Subscribe to demo channel with presence observation
        self.client.subscribeToChannels([withCannel], withPresence: true)
    }
    
    func publishLocation(location:CLLocation,bearing:CLLocationDirection,channel:String)
    {
        // Building the message and pushing it
        let message = "{\"latitude\":\(location.coordinate.latitude),\"longitude\":\(location.coordinate.longitude),\"bearing\":\(bearing)}"
        print(message)
        
        self.client.publish(message, toChannel: channel, compressed: true)  { (status) in
            if !status.isError {
                
                // Message successfully published to specified channel.
            }
            else{
                
            }
        }
    }
    
    //MARK:- UserInfo Popup Delegate
    func btnProcessed_Click(strCID : String, strScreenName: String) {
        
        let objServiceDetailForSBUVC = ServiceDetailForSBUVC(nibName: "ServiceDetailForSBUVC", bundle: nil) as ServiceDetailForSBUVC
        objServiceDetailForSBUVC.companyID    = strCID
        objServiceDetailForSBUVC.screenName   = strScreenName
        
        let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
        navigationController.pushViewController(objServiceDetailForSBUVC, animated: true)
        
    }
    
    // MARK: - Polyline and Screen Position Updaters
    
    func updateOverlay(_ currentPosition: CLLocation) {
        self.path.add(currentPosition.coordinate)
        self.polyline.path = self.path
    }
    
    func updateMapFrame(_ newLocation: CLLocation, zoom: Float) {
        
        let camera = GMSCameraPosition.camera(withTarget: newLocation.coordinate, zoom: zoom)
        
        self.mapView.animate(to: camera)
    }
    
    func initializePolylineAnnotation() {
        self.polyline.strokeColor = Constant.Color.kMapStrokColor
        self.polyline.strokeWidth = 5.0
        self.polyline.map = self.mapView
        
    }
    
    func drawPath(startLocation:CLLocationCoordinate2D,endLocation:CLLocationCoordinate2D)
    {
        polyline.map=nil
        self.getRoots(startLocation: startLocation, endLocation: endLocation) { (routes) in
            for route in routes
            {
                let routeOverviewPolyline = route["overview_polyline"] as? [String:Any]
                let points = routeOverviewPolyline?["points"] as? String
                let path = GMSPath.init(fromEncodedPath: points!)
                let polyline = GMSPolyline()
                polyline.map=nil
                polyline.path = path
                polyline.strokeColor = Constant.Color.kMapRootStrokColor
                polyline.strokeWidth = 5.0
                polyline.map = self.mapView
            }
            
        }
    }
    
    
    func getRoots(startLocation:CLLocationCoordinate2D,endLocation:CLLocationCoordinate2D,success:@escaping([[String:Any]])->())
    {
        let origin = "\(startLocation.latitude),\(startLocation.longitude)"
        let destination = "\(endLocation.latitude),\(endLocation.longitude)"
        
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=\(Constant.googleRootsAPIKey)"
        APIManager.callAPI(url: url, inputParams: nil, success: { (response) in
            if response?["status"] as! String == "OK"
            {
                if let routes = response?["routes"] as? [[String:Any]]
                {
                    success(routes)
                }
            }
            else
            {
                
                Utilities.showAlertWithAction(title: "", message: Constant.serverAPI.errorMessages.kCommanErrorMessage, buttonTitle: "OK", buttonTitle2: "", onOKClick: {
                    
                    self.navigationController?.popViewController(animated: true)
                    
                    
                }, onCancelClick: {
                    
                })
            }
            
        }) { (error) in
            
        }
    }
    
    //MARK: - Handle Notification
    func HandlePushNotification(userInfo: notificationData)
    {
        if DataModel.sharedInstance.isUserLoggedIn() {
            if let notificationId = userInfo.notificationId {
                self.callReadNotification(notificationId: notificationId)
            }
            if DataModel.sharedInstance.onFetchUserLoginType() == Constant.UserType.Buyer{
                
                //Buyer
                switch userInfo.notifyType! {
                case 4,11:
                    let vc = PayForServiceVC(nibName: "PayForServiceVC", bundle: nil)
                    let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
                    if Singleton.sharedManager.currentView != nil
                    {
                        if (Singleton.sharedManager.currentView?.isKind(of: PayForServiceVC.self))!   {
                            
                            let notificationName = Notification.Name("ReloadView")
                            NotificationCenter.default.post(name: notificationName, object: nil)
                        }
                        else
                        {
                            vc.isFrom = trackProgress.isFromHome
                            navigationController.pushViewController(vc, animated: true)
                        }
                    }
                    else
                    {
                        vc.isFrom = trackProgress.isFromHome
                        navigationController.pushViewController(vc, animated: true)
                    }
                case 5,6:
                    let vc = TrackProgressVC(nibName: "TrackProgressVC", bundle: nil)
                    vc.requestID = userInfo.requestId!
                    if userInfo.notifyType == 6 {
                      //  vc.didReciveNotification = true
                        NotificationCenter.default.post(name: Notification.Name("ShowRatingPopUp"), object: nil)
                    }
                    let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
                    if Singleton.sharedManager.currentView != nil
                    {
                        
                        if (Singleton.sharedManager.currentView?.isKind(of: TrackProgressVC.self))!   {
                            
                            let notificationName = Notification.Name("TrackProgressVC")
                            NotificationCenter.default.post(name: notificationName, object: nil)
                        }
                        else
                        {
                            vc.isFrom = trackProgress.isFromHome
                            navigationController.pushViewController(vc, animated: true)
                        }
                    }
                    else
                    {
                        vc.isFrom = trackProgress.isFromHome
                        navigationController.pushViewController(vc, animated: true)
                    }
                    
                case 6,7,8:
                    let vc = BuyerPurchaseDetailsVC(nibName: "BuyerPurchaseDetailsVC", bundle: nil)
                    vc.requestID = userInfo.requestId
                    let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
                    if Singleton.sharedManager.currentView != nil
                    {
                        
                        if (Singleton.sharedManager.currentView?.isKind(of: BuyerPurchaseDetailsVC.self))!   {
                            
                            let notificationName = Notification.Name("BuyerPurchaseDetailsVC")
                            NotificationCenter.default.post(name: notificationName, object: nil)
                        }
                        else
                        {
                            navigationController.pushViewController(vc, animated: true)
                        }
                    }
                    else
                    {
                        navigationController.pushViewController(vc, animated: true)
                    }
                    
                case 12:
                    let objMassageTherapyDetailsVC = MassageTherapyDetailsVC(nibName: "MassageTherapyDetailsVC", bundle: nil) as MassageTherapyDetailsVC
                    
                    objMassageTherapyDetailsVC.companyID     = userInfo.companyId
                    objMassageTherapyDetailsVC.categoryID    = userInfo.categoryId
                  
                    let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
                    if Singleton.sharedManager.currentView != nil
                    {
                        
                        if (Singleton.sharedManager.currentView?.isKind(of: MassageTherapyDetailsVC.self))!   {
                            
                            let notificationName = Notification.Name("MassageTherapyDetailsVC")
                            NotificationCenter.default.post(name: notificationName, object: nil)
                        }
                        else
                        {
                            navigationController.pushViewController(objMassageTherapyDetailsVC, animated: true)
                        }
                    }
                    else
                    {
                        navigationController.pushViewController(objMassageTherapyDetailsVC, animated: true)
                    }
                case 15:
                    let objMassageTherapyDetailsVC = MassageTherapyDetailsVC(nibName: "MassageTherapyDetailsVC", bundle: nil) as MassageTherapyDetailsVC

                    objMassageTherapyDetailsVC.companyID          = userInfo.companyId
                    objMassageTherapyDetailsVC.categoryID         = userInfo.categoryId
                    objMassageTherapyDetailsVC.subCatagoryID      = userInfo.subCategoryID
                    objMassageTherapyDetailsVC.serviceId          = userInfo.serviceId
                    objMassageTherapyDetailsVC.requestId          = userInfo.requestId
                    objMassageTherapyDetailsVC.isFromNotification = true

                    let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
                    if Singleton.sharedManager.currentView != nil
                    {

                        if (Singleton.sharedManager.currentView?.isKind(of: MassageTherapyDetailsVC.self))!   {

                            let notificationName = Notification.Name("MassageTherapyDetailsVC")
                            NotificationCenter.default.post(name: notificationName, object: nil)
                        }
                        else
                        {
                            navigationController.pushViewController(objMassageTherapyDetailsVC, animated: true)
                        }
                    }
                    else
                    {
                        navigationController.pushViewController(objMassageTherapyDetailsVC, animated: true)
                    }
                case 0:
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.strDialogID = userInfo.dialogId
                    NSLog("Data for push notification: Dialog Id: \(userInfo.dialogId).... receiver id:\(userInfo.receiverId).... dialog name: \(userInfo.dialogName)....occupant id: \(userInfo.occupantId)")
                    let vc = PrivateChatVC(nibName: "PrivateChatVC", bundle: nil)
                    vc.strDialogId = userInfo.dialogId
                    vc.strReceiverId = userInfo.receiverId
                    vc.strDialogName = userInfo.userName
                    vc.occupantId = userInfo.occupantId
                    vc.strRequestID = userInfo.requestId
                    
                    let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
                    if Singleton.sharedManager.currentView != nil {
                        if !(Singleton.sharedManager.currentView?.isKind(of: PrivateChatVC.self))! {
                            navigationController.pushViewController(vc, animated: true)
                        } else {
                            navigationController.popViewController(animated: true)
                            navigationController.pushViewController(vc, animated: true)
                        }
                    } else {
                        navigationController.pushViewController(vc, animated: true)
                    }
                default:
                    break
                }
                
            }else{
                
                //Seller
                switch userInfo.notifyType! {
                case 0:
                    print("Data for push notification: Dialog Id: \(userInfo.dialogId).... receiver id:\(userInfo.receiverId).... dialog name: \(userInfo.dialogName)....occupant id: \(userInfo.occupantId)")
                    
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.strDialogID = userInfo.dialogId
                    
                    let vc = PrivateChatVC(nibName: "PrivateChatVC", bundle: nil)
                    vc.strDialogId = userInfo.dialogId
                    vc.strReceiverId = userInfo.receiverId
                    vc.strDialogName = userInfo.userName
                    vc.occupantId = userInfo.occupantId
                    vc.strRequestID = userInfo.requestId
                    
                    let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
                    if Singleton.sharedManager.currentView != nil {
                        if !(Singleton.sharedManager.currentView?.isKind(of: PrivateChatVC.self))! {
                            navigationController.pushViewController(vc, animated: true)
                        } else {
                            navigationController.popViewController(animated: true)
                            navigationController.pushViewController(vc, animated: true)
                        }
                    } else {
                        navigationController.pushViewController(vc, animated: true)
                    }
                case 4:
                    let vc = RequestDetailsVC(nibName: "RequestDetailsVC", bundle: nil)
                    vc.requestID = userInfo.requestId
                    vc.strTitle = Constant.DetailsScreenTitle.kViewRequest
                    let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
                    if Singleton.sharedManager.currentView != nil
                    {
                        if (Singleton.sharedManager.currentView?.isKind(of: RequestDetailsVC.self))!   {
                            
                            let notificationName = Notification.Name("RequestDetailsVC")
                            NotificationCenter.default.post(name: notificationName, object: nil)
                            
                        }
                        else
                        {
                            navigationController.pushViewController(vc, animated: true)
                        }
                    }
                    else
                    {
                        navigationController.pushViewController(vc, animated: true)
                    }
                case 5:
                    let vc = RequestDetailsVC(nibName: "RequestDetailsVC", bundle: nil)
                    vc.requestID = userInfo.requestId
                    vc.strTitle = Constant.DetailsScreenTitle.kViewAppointment
                    let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
                    if Singleton.sharedManager.currentView != nil
                    {
                        
                        if (Singleton.sharedManager.currentView!.isKind(of: RequestDetailsVC.self))   {
                            
                            let notificationName = Notification.Name("RequestDetailsVC")
                            NotificationCenter.default.post(name: notificationName, object: nil)
                        }
                        else
                        {
                            navigationController.pushViewController(vc, animated: true)
                        }
                    }
                    else
                    {
                        navigationController.pushViewController(vc, animated: true)
                    }
                case 6:
                    let vc = RequestDetailsVC(nibName: "RequestDetailsVC", bundle: nil)
                    vc.requestID = userInfo.requestId
                    vc.strTitle = Constant.DetailsScreenTitle.kViewSale
                    let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
                    if Singleton.sharedManager.currentView != nil
                    {
                        
                        if (Singleton.sharedManager.currentView!.isKind(of: RequestDetailsVC.self))   {
                            
                            let notificationName = Notification.Name("RequestDetailsVC")
                            NotificationCenter.default.post(name: notificationName, object: nil)
                        }
                        else
                        {
                            navigationController.pushViewController(vc, animated: true)
                        }
                    }
                    else
                    {
                        navigationController.pushViewController(vc, animated: true)
                    }
                case 8:
                    let vc = NotificationVC(nibName: "NotificationVC", bundle: nil)
                    let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
                    if Singleton.sharedManager.currentView != nil
                    {
                        
                        if (Singleton.sharedManager.currentView!.isKind(of: NotificationVC.self))   {
                            
                            let notificationName = Notification.Name("NotificationVC")
                            NotificationCenter.default.post(name: notificationName, object: nil)
                        }
                        else
                        {
                            navigationController.pushViewController(vc, animated: true)
                        }
                    }
                    else
                    {
                        navigationController.pushViewController(vc, animated: true)
                    }
                case 10:
                    let vc = RequestDetailVC(nibName: "RequestDetailVC", bundle: nil)
                    vc.requestId = userInfo.requestId!
                    let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
                    if Singleton.sharedManager.currentView != nil
                    {
                        if (Singleton.sharedManager.currentView!.isKind(of: RequestDetailsVC.self))   {
                            
                            let notificationName = Notification.Name("RequestDetailVC")
                            NotificationCenter.default.post(name: notificationName, object: nil)
                        }
                        else
                        {
                            navigationController.pushViewController(vc, animated: true)
                        }
                    }
                    else
                    {
                        navigationController.pushViewController(vc, animated: true)
                    }
                default:
                    break
                }
                
            }
            
        }
        Singleton.sharedManager.userInfo = nil
        Singleton.sharedManager.isNotification = false
        
    }
    
    func callReadNotification(notificationId: String)
    {
        var dict = [String : Any]()
        
        dict["sessionId"]       = DataModel.sharedInstance.onFetchSessionId()
        dict["userId"]          = DataModel.sharedInstance.onFetchUserId()
        dict["notificationId"]  = notificationId
        
        if notificationId != "" {
            APIManager.callAPI(url: Constant.serverAPI.URL.kMakeNotificationUnread, inputParams: dict, success: { (dict) in
                if dict?["Result"] as? Bool == true {
                    let data = dict?["Data"] as? [String:Any]
                    if data?.count != 0
                    {
                        
                        appShareManager.unreadNotificationCount = (data?["userUnreadNotificationCount"] as? Int)!
                    }
                }
            }) { error in
            }
        }
    }
    
    deinit {
        
        print("Base View Deinit.")
    }
}

extension BaseView : SWRevealViewControllerDelegate
{
    //MARK: - SWRevealController delegate
    private func revealController(revealController: SWRevealViewController!, willMoveToPosition position: FrontViewPosition)
    {
        if revealController.frontViewPosition == FrontViewPosition.left
        {
            self.view.isUserInteractionEnabled = true
        }
        else
        {
            self.view.isUserInteractionEnabled = false
        }
    }
    
    func revealController(_ revealController: SWRevealViewController!, didMoveTo position: FrontViewPosition) {
        if revealController.frontViewPosition == FrontViewPosition.left
        {
            self.view.isUserInteractionEnabled = true
        }
        else
        {
            self.view.isUserInteractionEnabled = false
        }
    }
}

extension BaseView:PNObjectEventListener
{
    // Handle new message from one of channels on which client has been subscribed.
    func client(_ client: PubNub, didReceiveMessage message: PNMessageResult) {
        
    }
}

extension GMSCircle {
    func bounds () -> GMSCoordinateBounds {
        func locationMinMax(positive : Bool) -> CLLocationCoordinate2D {
            let sign:Double = positive ? 1 : -1
            let dx  = sign * self.radius  / 6378000 * (180/Double.pi)
            let lat = position.latitude + dx
            let lon = position.longitude + dx / cos(position.latitude * .pi/180)
            return CLLocationCoordinate2D(latitude: lat, longitude: lon)
        }
        return GMSCoordinateBounds(coordinate: locationMinMax(positive: true),
                                   coordinate: locationMinMax(positive: false))
    }
}
