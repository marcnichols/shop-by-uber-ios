//
//  BusinessProfileVC.swift
//  ShopByUber
//
//  Created by Differenz215 on 6/27/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit
import Cosmos
import QuartzCore
import SearchTextField

struct PaymentOptions {
    
    var paymentType: String!
    var collapsed: Bool!

    init(paymentType: String, collapsed: Bool) {
        self.paymentType = paymentType
        self.collapsed = collapsed
    }
}

class BusinessProfileVC: BaseView,UINavigationControllerDelegate, PECropViewControllerDelegate, PassChequeTextfield ,PassBankTextfield {
    
    //MARK: - IBOutlet
    @IBOutlet var navigationBar: UIView!
    
    @IBOutlet var vwNumber: UIView!
    @IBOutlet var vwWebsite: UIView!
    @IBOutlet var vwOwner: UIView!
    @IBOutlet var vwEmail: UIView!
    @IBOutlet var vwNewPassword: UIView!
    @IBOutlet var vwConfirmPassword: UIView!
    
    @IBOutlet weak var vwPromoCode: UIView!
    
    @IBOutlet weak var txtCountryCode: UILabel!
    @IBOutlet var imgProfile: UIImageView!

    @IBOutlet weak var txtScreenName: UITextField!
    
    @IBOutlet weak var txtfirstName: UITextField!
    
    @IBOutlet weak var txtlastName: UITextField!
    @IBOutlet var txtNumber: UITextField!

    @IBOutlet weak var txtPromoCode: UITextField!
    @IBOutlet weak var txtPaymentOptions: UITextField!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtNewPassword: UITextField!
    @IBOutlet var txtConfirmPassword: UITextField!
    
    @IBOutlet weak var nslcImgProfileHeight: NSLayoutConstraint!
    @IBOutlet weak var nslcDescriptionHeight: NSLayoutConstraint!
    @IBOutlet weak var vw_Reviews: UIView!
    
    @IBOutlet weak var tblReviews: UITableView!
    @IBOutlet weak var cosmos_Ratings: CosmosView!
    @IBOutlet weak var nslayouttblReviewHeight: NSLayoutConstraint!
    @IBOutlet weak var nslayoutReviewViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var tblDropDown: UITableView!
    
    @IBOutlet weak var nslcTblDropdownHeight: NSLayoutConstraint!
    
    
    
    //MARK: - Variables
    var imagePicker = UIImagePickerController()
    var isProfileSelected = false
    var arr_Reviews = [ReviewsModel]()
    var ProfileData: BusinessProfile?
    var sellerData: sellerUserData?
    var userPaymentData : PaymentData?
    var latitude = 0.0
    var longitude = 0.0
    var zipcode = ""
    var showDropdown = false
    var isDropDown = false
    var payment = [PaymentOptions]()
    let dropDown = DropDown()
    var streetAddress = String()
    var companyAddress = String()
    var paymentData = [String:Any]()
    var selectedIndex = 0
    
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialConfig()
  
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillLayoutSubviews() {
        
        self.nslayouttblReviewHeight.constant = self.tblReviews.contentSize.height
        self.nslayoutReviewViewHeight.constant = 99 + self.tblReviews.contentSize.height
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Private Method
    
    func initialConfig(){
        
       
        self.tblDropDown.register(UINib(nibName: "PaypalPaymentCell", bundle: nil), forCellReuseIdentifier: "PaypalPaymentCell")
        self.tblDropDown.register(UINib(nibName: "BankPaymentCell", bundle: nil), forCellReuseIdentifier: "BankPaymentCell")
        self.tblDropDown.register(UINib(nibName: "CheckPaymentCell", bundle: nil), forCellReuseIdentifier: "CheckPaymentCell")
        
        tblDropDown.dataSource = self
        tblDropDown.delegate = self
        tblDropDown.reloadData()
        tblDropDown.layoutIfNeeded()
        
        self.tblReviews.register(UINib(nibName: "ReviewCell", bundle: nil), forCellReuseIdentifier: "ReviewCell")
        tblReviews.rowHeight = UITableViewAutomaticDimension
        
        self.callGetBusinessProfileAPI()
        
    }
    
    func setProfileData()
    {
        if self.ProfileData?.companyProfileImage != nil{
            self.imgProfile.contentMode = .scaleAspectFit
            self.imgProfile.sd_setImage(with: URL.init(string: (self.ProfileData?.companyProfileImage)!), placeholderImage: UIImage(named:Constant.PlaceHolder.kPhoto), options: SDWebImageOptions.retryFailed, completed:{ (image, err, nil, url) in
                if image != nil {
                    self.imgProfile.image = image
                    self.imgProfile.contentMode = .scaleAspectFill
                }
            })
            
        }
        
        self.txtfirstName.text        = self.ProfileData?.firstName
        self.txtlastName.text         = self.ProfileData?.lastName
        self.txtScreenName.text       = self.ProfileData?.comapnyScreenName
        self.companyAddress           = self.ProfileData?.companyAddress ?? ""
        self.latitude                 = self.ProfileData?.latitude ?? 0.0
        self.longitude                = self.ProfileData?.longitude ?? 0.0
        self.zipcode                  = self.ProfileData?.zipcode ?? ""
        
        self.txtCountryCode.text      = self.sellerData?.countryCode ?? ""
        self.txtNumber.text           = self.ProfileData?.companyPhone?.toPhoneNumber()
        self.txtEmail.text            = self.sellerData?.email
        self.txtPromoCode.text        = self.ProfileData?.promoCode
        
        dropDown.dataSource = ["Select your Payment Option","Paypal","Bank Account (ACH/EFT)", "Pay by Check"]
        if ProfileData?.paymentMethod == 1 {
            self.txtPaymentOptions.text      = "Paypal"
            self.nslcTblDropdownHeight.constant = Utilities.isDeviceiPad() ? 150 : 130
            self.selectedIndex = 1
          
            
        } else if ProfileData?.paymentMethod == 2 {
            self.txtPaymentOptions.text      = "Bank Account (ACH/EFT)"
            self.nslcTblDropdownHeight.constant = Utilities.isDeviceiPad() ? 400 : 325
            self.selectedIndex = 2
       
        }else if ProfileData?.paymentMethod == 3 {
            self.txtPaymentOptions.text      = "Pay by Check"
            self.nslcTblDropdownHeight.constant = Utilities.isDeviceiPad() ? 300 : 240
            self.selectedIndex = 3
        
        }else {
            self.txtPaymentOptions.text      = "Select your Payment Option"
            self.nslcTblDropdownHeight.constant = 0
            self.selectedIndex = 0
        }

        if self.sellerData?.email == ""
        {
            self.vwEmail.isUserInteractionEnabled = true
        }
        else
        {
            self.vwEmail.isUserInteractionEnabled = false
        }
        
    
        
         self.view.layoutIfNeeded()
        
    }
    
    
    
    @IBAction func imgProfile_Clicked(_ sender: UITapGestureRecognizer) {
        
        openImagePicker()
    }
    
    
    @IBAction func btnEditProfile_Clicked(_ sender: Any) {
        if self.isValidServiceData(firstName: txtfirstName.text, lastName: txtlastName.text, screenName: txtScreenName.text,  number: txtNumber.text,email: txtEmail.text, password: txtNewPassword.text, confirmPassword: txtConfirmPassword.text!){
            
            if selectedIndex == 0 {
                if self.isProfileSelected
                {
                    self.callUploadImage()
                }
                else
                {
                    callUpdateProfileAPI(imageName: "")
                }
            }
            else if selectedIndex == 1 {
                let cell = tblDropDown.cellForRow(at: IndexPath(row: 0, section: 0)) as? PaypalPaymentCell
                if  (cell?.isValidServiceData(email: cell?.txtPaypalEmail.text))!{
                    if self.isProfileSelected
                    {
                        self.callUploadImage()
                    }
                    else
                    {
                        callUpdateProfileAPI(imageName: "")
                    }
                }
            }
            else if selectedIndex == 2 {
                
                let cell = tblDropDown.cellForRow(at: IndexPath(row: 0, section: 0)) as? BankPaymentCell
                if (cell?.isValidServiceData(firstName: cell?.txtFirstName.text, lastName: cell?.txtLastName.text, streetName: cell?.txtStreetName.text, city: cell?.txtCity.text, state: cell?.txtState.text, zipcode: cell?.txtZipCode.text, routingNumber: cell?.txtRoutingNumber.text, accountNumber: cell?.txtAccNumber.text))! {
                    
                    if self.isProfileSelected
                    {
                        self.callUploadImage()
                    }
                    else
                    {
                        callUpdateProfileAPI(imageName: "")
                    }
                }
            }
            else {
                let cell = tblDropDown.cellForRow(at: IndexPath(row: 0, section: 0)) as? CheckPaymentCell
                if (cell?.isValidServiceData(firstName: cell?.txtFirstName.text, lastName: cell?.txtLastName.text, streetName: cell?.txtStreet.text, city: cell?.txtCity.text, state: cell?.txtState.text, zipcode: cell?.txtZipCode.text))! {
                    
                    if self.isProfileSelected
                    {
                        self.callUploadImage()
                    }
                    else
                    {
                        callUpdateProfileAPI(imageName: "")
                    }
                }
            }
        }
        
    }
    
    @IBAction func btnPaymentDropDownClick(_ sender: UIButton) {
        
        dropDown.anchorView = txtPaymentOptions
        dropDown.direction = .bottom
        dropDown.dataSource = ["Select your Payment Option","Paypal","Bank Account (ACH/EFT)", "Pay by Check"]
        dropDown.width = txtPaymentOptions.frame.size.width
       
        dropDown.selectionAction = {(index: Int, item: String) in
        let message: String
        if index == 0 {
            message = "Are you Sure? If you remove your payment option here, it will update on all services you offer to your buyers."
        }
        else {
            message = "Are you Sure? If you change your payment option here, it will update on all services you offer to your buyers."
        }
            
            if self.selectedIndex != index {
                
                if self.ProfileData?.paymentMethod == 0 || self.selectedIndex == 0{
                    if index == 0
                    {
                        //select payment option
                        self.txtPaymentOptions.text = "Select your Payment Option"
                        self.nslcTblDropdownHeight.constant = 0
                        self.selectedIndex = 0
                    }else if index == 1
                    {
                        //Paypal
                        self.txtPaymentOptions.text = "Paypal"
                        self.nslcTblDropdownHeight.constant = Utilities.isDeviceiPad() ? 150 : 130
                        self.tblDropDown.reloadData()
                        self.selectedIndex = 1
                    }else if index == 2
                    {
                        //Bank
                        self.txtPaymentOptions.text = "Bank Account (ACH/EFT)"
                        self.nslcTblDropdownHeight.constant = Utilities.isDeviceiPad() ? 400 : 325
                        self.tblDropDown.reloadData()
                        self.selectedIndex = 2
                    }else {
                        //Cheque
                        self.txtPaymentOptions.text = "Pay by Check"
                        self.nslcTblDropdownHeight.constant = Utilities.isDeviceiPad() ? 300 : 240
                        self.tblDropDown.reloadData()
                        self.selectedIndex = 3
                    }
                }
                else {
                    let alert = UIAlertController(title: "Are you sure?", message: message, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action) in
                        
                        if index == 0
                        {
                            //select payment option
                            self.txtPaymentOptions.text = "Select your Payment Option"
                            self.nslcTblDropdownHeight.constant = 0
                            self.selectedIndex = 0
                        }else if index == 1
                        {
                            //Paypal
                            self.txtPaymentOptions.text = "Paypal"
                            self.nslcTblDropdownHeight.constant = Utilities.isDeviceiPad() ? 150 : 130
                            self.tblDropDown.reloadData()
                            self.selectedIndex = 1
                        }else if index == 2
                        {
                            //Bank
                            self.txtPaymentOptions.text = "Bank Account (ACH/EFT)"
                            self.nslcTblDropdownHeight.constant = Utilities.isDeviceiPad() ? 400 : 325
                            self.tblDropDown.reloadData()
                            self.selectedIndex = 2
                        }else {
                            //Cheque
                            self.txtPaymentOptions.text = "Pay by Check"
                            self.nslcTblDropdownHeight.constant = Utilities.isDeviceiPad() ? 300 : 240
                            self.tblDropDown.reloadData()
                            self.selectedIndex = 3
                        }
                        
                    }))
                    
                    alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (cancle) in
                        
                    }))
                    self.present(alert, animated: true)
                }
            }
        }
        dropDown.show()
        
        
        
    }
    
    
    //MARK: - Open Image Picker
    
    func openImagePicker() {
        
        imagePicker.delegate = self
        
        let alert:UIAlertController = UIAlertController(title: "Choose Image", message: nil, preferredStyle: Utilities.isDeviceiPad() ? .alert : .actionSheet )
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default){
            UIAlertAction in
            self.openProfileUserCamera()
        }
        
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.openProfileUserGallary()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
        }
        
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    //MARK: - Image Delegate
    
    func openProfileUserGallary() {
        
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        imagePicker.mediaTypes = [kUTTypeImage as String]
        present(imagePicker, animated: true, completion: nil)
    }
    
    func openProfileUserCamera() {
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.allowsEditing = false
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.cameraCaptureMode = .photo
            imagePicker.modalPresentationStyle = .fullScreen
            present(imagePicker,animated: true,completion: nil)
        }
    }
    
    
    func cropViewController(_ controller: PECropViewController!, didFinishCroppingImage croppedImage: UIImage!) {
        controller.dismiss(animated: true) { _ in }
        imgProfile?.image = croppedImage
        isProfileSelected = true
        
        if UI_USER_INTERFACE_IDIOM() == .pad {
        }
        
    }
    
    func cropViewController(_ controller: PECropViewController, didFinishCroppingImage croppedImage: UIImage, transform: CGAffineTransform, cropRect: CGRect) {
        controller.dismiss(animated: true) { _ in }
        imgProfile?.image = croppedImage
        isProfileSelected = true
        if UI_USER_INTERFACE_IDIOM() == .pad {
        }
    }
    
    func cropViewControllerDidCancel(_ controller: PECropViewController) {
        self.isProfileSelected = false
        if UI_USER_INTERFACE_IDIOM() == .pad {
            
        }
        controller.dismiss(animated: true) { _ in }
    }
    
    //MARK: - API Call
    func callGetBusinessProfileAPI(){
        
        KVNProgress.show()
        var dict = [String : Any]()
        
        dict["sessionId"]    = DataModel.sharedInstance.onFetchSessionId()
        dict["userId"]       = DataModel.sharedInstance.onFetchUserId()
        
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kGetBusinessProfile, inputParams: dict, success: { (dict) in
            
            
            if dict?["Result"] as? Bool == true {
                let data = dict?["Data"] as? [String:Any]
                
                self.ProfileData = BusinessProfile.init(dict: data!)
                let sellerdata = data?["vendorData"] as? [String:Any]
                self.sellerData = sellerUserData.init(fromDictionary: sellerdata!)
                
                let paymentData = data?["paymentDetail"] as? [String:Any] ?? [:]
                self.userPaymentData = PaymentData.init(dict: paymentData)
                
                self.setProfileData()
                
                let reviews = data?["serviceReviews"] as! NSArray
                if reviews.count != 0
                {
                    for i in reviews
                    {
                        let review = ReviewsModel.init(dict: i as! [String : Any])
                        self.arr_Reviews.append(review)
                    }
                    self.tblReviews.reloadData()
                }
                self.cosmos_Ratings.rating = (data?["overAllRating"] as? Double ?? 0)!
                self.vw_Reviews.isHidden = false
                KVNProgress.dismiss()
            }
            
        }) { error in
            KVNProgress.dismiss()
            
            Utilities.showAlertView(title: "", message: error)
            
        }
        
    }
    func callUploadImage(){
        
        ServiceProfile.callUploadImageAPI(imageType: Constant.ImageType.CompanyProfile, image: self.imgProfile.image, success: { (name) in
            
            self.callUpdateProfileAPI(imageName: name)
            
        }) {
        }
    }
    
    func callUpdateProfileAPI(imageName:String?){
        
        let mobileNo = txtNumber.text?.replacingOccurrences(of: "-", with: "")

        KVNProgress.show()
        var dict = [String : Any]()
       
        dict["sessionId"]           = DataModel.sharedInstance.onFetchSessionId()
        dict["companyId"]           = self.ProfileData?.companyId
        dict["vendorId"]            = DataModel.sharedInstance.onFetchUserId()
        dict["companyName"]         = ""
        dict["comapnyScreenName"]   = self.txtScreenName.text?.trimmingCharacters(in: .whitespaces)
        dict["companyProfileImage"] = imageName
        dict["firstName"]           = self.txtScreenName.text?.trimmingCharacters(in: .whitespaces)
        dict["lastName"]            = self.txtlastName.text?.trimmingCharacters(in: .whitespaces)
        dict["ownerName"]           = self.txtfirstName.text! + " " + self.txtlastName.text!
        dict["websiteUrl"]          = ""
        dict["serviceDescription"]  = ""
        dict["companyAddress"]      = self.companyAddress
        dict["longitude"]           = self.longitude
        dict["latitude"]            = self.latitude
        dict["zipCode"]             = self.zipcode
        
        dict["companyPhoneNumber"]  = mobileNo
        dict["countryCode"]         = self.txtCountryCode.text
        dict["paymentMethod"]       = self.selectedIndex
        dict["promoCode"]           = self.txtPromoCode.text
        
        
        var vendorDict = [String : Any]()
        vendorDict["email"]               = self.txtEmail.text?.trimmingCharacters(in: .whitespaces)
        vendorDict["userRole"]            = Constant.UserType.Seller
        vendorDict["timeZone"]            = DataModel.sharedInstance.getCurrentTimeZone()
        vendorDict["description"]         = ""
        vendorDict["speciality"]          = ""
        vendorDict["password"]            = self.txtNewPassword.text?.trimmingCharacters(in: .whitespaces)
        dict["vendorData"]                = vendorDict
        
        var payment_detail = [String : Any]()
        
        if selectedIndex == 0 {
            dict["paypalEmail"]         = ""
            dict["paymentDetail"]       = [:]
        }
        else if selectedIndex  == 1 {
            if let cell = tblDropDown.cellForRow(at: IndexPath(row: 0, section: 0)) as? PaypalPaymentCell {
                dict["paypalEmail"]         = cell.txtPaypalEmail.text
                dict["paymentDetail"]       = [:]
            }
        }
        else if selectedIndex  == 2 {
            if let cell = tblDropDown.cellForRow(at: IndexPath(row: 0, section: 0)) as? BankPaymentCell {
                
                if cell.stateCode.isEmpty || cell.cityCode.isEmpty {
                    cell.stateCode = (userPaymentData?.state)!
                    cell.cityCode = (userPaymentData?.city)!
                }
                
                payment_detail["firstName"]         = cell.txtFirstName.text ?? ""
                payment_detail["lastName"]          = cell.txtLastName.text ?? ""
                payment_detail["streetAddress"]     = cell.txtStreetName.text ?? ""
                payment_detail["city"]              = cell.cityCode
                payment_detail["state"]             = cell.stateCode
                payment_detail["zipCode"]           = cell.txtZipCode.text ?? ""
                payment_detail["routingNumber"]     = cell.txtRoutingNumber.text ?? ""
                payment_detail["accountNumber"]     = cell.txtAccNumber.text ?? ""
                dict["paymentDetail"]               = payment_detail
                dict["paypalEmail"]                 = ""
            }
        }
        else if selectedIndex  == 3 {
            if let cell = tblDropDown.cellForRow(at: IndexPath(row: 0, section: 0)) as? CheckPaymentCell {
                
                if cell.stateCode.isEmpty || cell.cityCode.isEmpty {
                    cell.stateCode = (userPaymentData?.state)!
                    cell.cityCode = (userPaymentData?.city)!
                }
                
                payment_detail["firstName"]         = cell.txtFirstName.text ?? ""
                payment_detail["lastName"]          = cell.txtLastName.text ?? ""
                payment_detail["streetAddress"]     = cell.txtStreet.text ?? ""
                payment_detail["city"]              = cell.cityCode
                payment_detail["state"]             = cell.stateCode
                payment_detail["zipCode"]           = cell.txtZipCode.text ?? ""
                payment_detail["routingNumber"]     = ""
                payment_detail["accountNumber"]     = ""
                dict["paymentDetail"]               = payment_detail
                dict["paypalEmail"]                 = ""
            }
        }
        
 
        APIManager.callAPI(url: Constant.serverAPI.URL.kUpdateBusinessProfile, inputParams: dict, success: { (dict) in
            
            if dict?["Result"] as? Bool == true {
                Utilities.showAlertView(title: "", message: dict?["Message"] as? String)
                appDeletgate.dismissBubble()
                DataModel.sharedInstance.setValueInUserDefaults(value: self.txtPromoCode.text, key: Constant.DataModelKey.kpromoCode)
                if DataModel.sharedInstance.onFetchPromoCode() != "" {
                    
                    appDeletgate.ShowBubble(promoCode: DataModel.sharedInstance.onFetchPromoCode())
                }
            }
            else
            {
                Utilities.showAlertView(title: "", message: dict?["Message"] as? String)
                
            }
            self.callGetBusinessProfileAPI()
            KVNProgress.dismiss()
        }) { error in
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
        }
    }
   
    
    //MARK: - Validation
    
    func isValidServiceData(firstName: String?, lastName: String?, screenName:String?,number:String?,email: String?,password: String?, confirmPassword: String)  -> Bool {
    
        let firstName = firstName?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let lastName = lastName?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let screenName = screenName?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let number = number?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let email = email?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let password = password?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let confirmPassword = confirmPassword.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        var message = ""
        
        if firstName == ""{
            
            message =  Constant.ValidationMessage.kCompanyfirstName
            
        }
        else if lastName == ""{
            
            message =  Constant.ValidationMessage.kCompanylastName
            
        }
        else if screenName == "" {
            
            message = Constant.ValidationMessage.kCompanyScreenName
        }
        else if email == ""{
            
            message =  Constant.ValidationMessage.kEmail
            
        }
        else if !Utilities.isValidEmail(email!){
            
            message =  Constant.ValidationMessage.kValidEmail
            
        }
//        else if !Utilities.isValidEmail(email!){
//            message =  Constant.ValidationMessage.kValidPaypalEmail
//        }
        else if number == ""{
            
            message =  Constant.ValidationMessage.kCompanyPhone
            
        } else if number?.characters.count != 12{
            
            message =  Constant.ValidationMessage.kCompanyValidPhone
            
        }
        else if password != ""{
            
            if password != confirmPassword{
                
                message = Constant.ValidationMessage.kConfirmPassword
            }
            else
            {
                return true
            }
            
        }else if confirmPassword != "" {
            
            if password != confirmPassword{
                
                message = Constant.ValidationMessage.kConfirmPassword
                
            }
            else
            {
                return true
            }
            
        }

        else {
            
            return true
        }
        
        Utilities.showAlertView(title: "Alert", message: message)
        return false
    }
    
    //MARK:- Mobile Validation
    func checkEnglishPhoneNumberFormat(string: String?, str: String?) -> Bool{
        
        if string == ""{ //BackSpace
            
            return true
            
        }else if str!.count == 4{
            
            txtNumber.text = txtNumber.text! + "-"
            
        }else if str!.count == 8{
            
            txtNumber.text = txtNumber.text! + "-"
            
        }else if str!.count > 12{
            
            return false
        }
        
        return true
    }
    
    func userInputData(_ dic: [String:String]) {
        
        userPaymentData?.firstName = dic["firstname"] ?? ""
        userPaymentData?.lastName =  dic["lastname"]  ?? ""
        userPaymentData?.stateName =  dic["statename"] ?? ""
        userPaymentData?.cityName = dic["cityname"] ?? ""
        userPaymentData?.street = dic["street"] ?? ""
        userPaymentData?.zipCode = dic["zip"] ?? ""
        tblDropDown.reloadData()
    }
    
    func userInputBankData(_ dic: [String:String]) {
        
        userPaymentData?.firstName = dic["firstname"] ?? ""
        userPaymentData?.lastName =  dic["lastname"]  ?? ""
        userPaymentData?.stateName =  dic["statename"] ?? ""
        userPaymentData?.cityName = dic["cityname"] ?? ""
        userPaymentData?.street = dic["street"] ?? ""
        userPaymentData?.zipCode = dic["zip"] ?? ""
        userPaymentData?.routingNumber = dic["routing"] ?? ""
        userPaymentData?.accountNumber = dic["acc"] ?? ""
        tblDropDown.reloadData()
    }
    
}

extension BusinessProfileVC : UITextFieldDelegate
{
    //MARK: - UITextField Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let str = (textField.text! as NSString).replacingCharacters(in: range, with: string)

        if textField == txtNumber{
            
            return checkEnglishPhoneNumberFormat(string: string, str: str)
            
        }else{
            
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtfirstName{
            txtlastName.becomeFirstResponder()
        }
        if textField == txtlastName{
            txtScreenName.becomeFirstResponder()
        }
        if textField == txtScreenName {
            txtScreenName.becomeFirstResponder()
        }
        else{
            textField.resignFirstResponder()
        }
        
        return true
    }
    
}


extension BusinessProfileVC : UIImagePickerControllerDelegate
{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        dismiss(animated:true, completion: nil)
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            openEditor(image: pickedImage)
            
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.isProfileSelected = false
        dismiss(animated:true, completion: nil)
    }
    
    func openEditor(image: UIImage) {
        let controller = PECropViewController()
        controller.delegate = self
        controller.image = image
        let width: CGFloat? = image.size.width
        let height: CGFloat? = image.size.height
        let length: CGFloat = min(width!, height!)
        let ratioValue = width!/4
        
        controller.imageCropRect = CGRect(x: (width! - length) / 2, y: (height! - length) / 2, width: ratioValue*4, height: ratioValue*3)
        controller.keepingCropAspectRatio = true
        let navigationController = UINavigationController(rootViewController: controller as UIViewController )
        if UI_USER_INTERFACE_IDIOM() == .pad {
            navigationController.modalPresentationStyle = .formSheet
        }
        present(navigationController, animated: true) { _ in }
    }
    
}

extension BusinessProfileVC : UITableViewDelegate,UITableViewDataSource
{
    //MARK:  - TableView Methods
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblReviews {
            
            return self.arr_Reviews.count
        }else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tblReviews {
            let font = UIFont.systemFontLight(with: 15)
            let review = self.arr_Reviews[indexPath.row]
            let serviceName = Utilities.heigth(from: review.Feedback!, with: font, width:self.view.frame.size.width - 40)
            return 60 + serviceName
        }else {
            
            if selectedIndex == 1
            {
                return 130      //paypal cell Height
            }
            else if selectedIndex == 2
            {
                return 325      //Bank Payment Cell Height
            }else if selectedIndex == 3
            {
                return 240      //Check Payment Cell Height
            }else {
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == tblDropDown {
          
            if selectedIndex == 1
            {
                return Utilities.isDeviceiPad() ? 150 : 130      //paypal cell Height
            }
            else if selectedIndex == 2
            {
                return Utilities.isDeviceiPad() ? 400 : 325      //Bank Payment Cell Height
            }else if selectedIndex == 3
            {
                return Utilities.isDeviceiPad() ? 300 : 240      //Check Payment Cell Height
            }else {
                return 0
            }
        }else {
            //Review Cell
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->
        UITableViewCell {
            
            if tableView == tblReviews {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewCell", for: indexPath) as! ReviewCell
                let review = self.arr_Reviews[indexPath.row]
                cell.lblDate.text = review.feedbackDateTime
                cell.lblNameReviewBy.text = review.givenByUser
                cell.lblReview.text = review.Feedback
                return cell
            }
            else {
                if self.selectedIndex == 1 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "PaypalPaymentCell") as! PaypalPaymentCell
                    
                    //Get Paypal emailID
                    cell.txtPaypalEmail.text! = (self.ProfileData?.paypalEmail)!

                    return cell
                }
                else if self.selectedIndex == 2 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "BankPaymentCell") as! BankPaymentCell
                    
                    //Adding Tap Gesture to Present Google AutoComplet Address
//                    let tapGesture = UITapGestureRecognizer(target: self, action:  #selector(AddressTapHandler))
//                    cell.vwAddress.addGestureRecognizer(tapGesture)
                    
                    
                    //Get Bank Payment Cell Data
                    cell.txtFirstName.text = userPaymentData?.firstName
                    cell.txtLastName.text = userPaymentData?.lastName
                    cell.txtState.text = userPaymentData?.stateName
                    cell.txtCity.text = userPaymentData?.cityName
                    cell.txtStreetName.text = userPaymentData?.street
                    cell.txtZipCode.text = userPaymentData?.zipCode
                    cell.txtRoutingNumber.text = userPaymentData?.routingNumber
                    cell.txtAccNumber.text = userPaymentData?.accountNumber
                    cell.delegate = self
                    
                    return cell
                }
                else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "CheckPaymentCell") as! CheckPaymentCell
                   
                    //Adding Tap Gesture to Present Google AutoComplet Address
//                    let tapGesture = UITapGestureRecognizer(target: self, action:  #selector(AddressTapHandler))
//                    cell.vwStreetAddress.addGestureRecognizer(tapGesture)
                    
                    
                    //Getting Cheque payment Cell Data
                    cell.txtFirstName.text = userPaymentData?.firstName
                    cell.txtLastName.text = userPaymentData?.lastName
                    cell.txtState.text = userPaymentData?.stateName
                    cell.txtCity.text = userPaymentData?.cityName
                    cell.txtStreet.text = userPaymentData?.street
                    cell.txtZipCode.text = userPaymentData?.zipCode
                    cell.delegate = self
                   
                    return cell
                }
            }
    }
    
    //MARK:- Private Method
    
    
//    func AddressTapHandler() {
//
//        let autocompleteController = GMSAutocompleteViewController()
//        autocompleteController.delegate = self
//        present(autocompleteController, animated: true, completion: nil)
//    }
}

//extension BusinessProfileVC : GMSAutocompleteViewControllerDelegate
//{
//    //MARK: - Auto Complete Google Delegates
//
//    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
//
//        if let addressLines = place.addressComponents {
//            // Populate all of the address fields we can find.
//
//            for field in addressLines {
//
//                switch field.type {
//                case kGMSPlaceTypePostalCode:
//                    self.zipcode = field.name
//                default:
//                    //print("Type: \(field.type), Name: \(field.name)")
//                    print("")
//                }
//            }
//        }
//
//        self.latitude = place.coordinate.latitude
//        self.longitude = place.coordinate.longitude
//
//        streetAddress = place.formattedAddress!
//        self.userPaymentData?.street = streetAddress
//        tblDropDown.reloadData()
//
//        //   self.txtAddress.text = place.formattedAddress
//        //    let contentSize = self.txtAddress.sizeThatFits(self.txtAddress.bounds.size)
//
//        dismiss(animated: true, completion: nil)
//    }
//
//
//    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
//        // TODO: handle the error.
//        print("Error: ", error.localizedDescription)
//    }
//
//    func wasCancelled(_ viewController: GMSAutocompleteViewController)
//    {
//        viewController.dismiss(animated: true, completion: nil)
//    }
//}




