//
//  ContactUsVC.swift
//  ShopByUber
//
//  Created by Differenz215 on 7/24/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class ContactUsVC: BaseView {
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    
    @IBOutlet weak var txtDescription: UITextView!
    //MARK: - Variables

    //MARK: - View LifeCycles
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - IBAction Methods
    
    @IBAction func backIconClick(_ sender: Any) {
        Singleton.sharedManager.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func btnContactUsClicked(_ sender: Any) {
        
        if self.isValidData(name: txtName.text, email:txtEmail.text,description: txtDescription.text){
            KVNProgress.show()
            var dict = [String : Any]()
            
            dict["name"]         = txtName.text
            dict["email"]        = txtEmail.text
            dict["description"]  = txtDescription.text
            
            
            APIManager.callAPI(url: Constant.serverAPI.URL.kContactUs, inputParams: dict, success: { (dict) in
                
                KVNProgress.dismiss()
                if dict?["Result"] as? Bool == true {
                    
                    Utilities.showAlertWithAction(title: "", message: (dict?["Message"] as? String)!, buttonTitle: "OK", buttonTitle2: "", onOKClick: {
                        Singleton.sharedManager.navigationController?.popViewController(animated: true)
                    }, onCancelClick: {
                        
                    })
                    
                }
                else
                {
                    Utilities.showAlertView(title: "", message: dict?["Message"] as? String)
                }
                
                
            }) { error in
                KVNProgress.dismiss()
                
                Utilities.showAlertView(title: "", message: error)
                
            }
        }
    }
    
    //MARK: - Validation
    
    func isValidData(name: String?, email: String?, description: String?)  -> Bool {
        
        let name = name?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let email = email?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let description = description?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        
        var message = ""
        
        if name == "" {
            
            message = Constant.ValidationMessage.kEnterName
            
        }else if email == ""{
            
            message =  Constant.ValidationMessage.kEmail
            
        }else if !Utilities.isValidEmail(email!){
            
            message =  Constant.ValidationMessage.kValidEmail
            
        }else if description == ""{
            
            message =  "Please enter description"
            
        }else {
            
            return true
        }
        
        Utilities.showAlertView(title: "Alert", message: message)
        return false
    }

}
extension ContactUsVC: UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtName{
            txtEmail.becomeFirstResponder()
        }else if textField == txtEmail {
            txtDescription.becomeFirstResponder()
        }
        else
        {
            txtDescription.resignFirstResponder()
        }
        
        return true
    }

}
