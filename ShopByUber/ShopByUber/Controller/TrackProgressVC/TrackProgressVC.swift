//
//  TrackProgressVC.swift
//  ShopByUber
//
//  Created by Differenz215 on 6/30/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit
import MapboxCoreNavigation
import MapboxNavigation
import MapboxDirections
import Mapbox

// MGLPointAnnotation subclass
class MyCustomPointAnnotation: MGLPointAnnotation {
    var willUseImage: Bool = false
    var isShowTitle : Bool = false
}

class TrackProgressVC: BaseView,MGLMapViewDelegate {
    
    @IBOutlet weak var lblProfileName: UILabel!
    @IBOutlet weak var lblProfileCategory: UILabel!
    @IBOutlet weak var imgProfilePic: UIImageView!
    @IBOutlet weak var lblWaitingTime: UILabel!
    
    
    @IBOutlet weak var bMbMapView: NavigationMapView!
    
    //MARK: -  Variables
    var arr_pubnubChannel = [String]()
    var isFrom: trackProgress?
    var arrMapData = [MapData]()
    var requestID:String!
    
    var serviceDetailsPopup = ServiceDetailsPopup()
    
    var waypoints: [Waypoint] = []
    var currentRoute: Route?
    var exampleMode: ExampleMode?
    var showCustomUserPuck = false
    
    var latestCordinates = CLLocationCoordinate2D()
    var callTimer : Timer?
    
    let vendorLocation = MyCustomPointAnnotation()
    let byuerLocation = MyCustomPointAnnotation()
    
    var expectedTravelTime : TimeInterval = 0
    var didReciveNotification = false
    
    var isAdded = false
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let notificationName = Notification.Name("TrackProgressVC")
        NotificationCenter.default.addObserver(self, selector: #selector(initialConfig), name: notificationName, object: nil)
        
        // Do any additional setup after loading the view.
        
        automaticallyAdjustsScrollViewInsets = false
        bMbMapView.delegate = self
        bMbMapView.userTrackingMode = .follow
        bMbMapView.logoView.isHidden = true
        bMbMapView.attributionButton.isHidden = true
        
        if appDeletgate.isOnTrackProgressVC == true {
            self.showNotificationPopup()
        }
         NotificationCenter.default.addObserver(self, selector:#selector(ShowRatingPopUp), name: NSNotification.Name(rawValue: "ShowRatingPopUp"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        Singleton.sharedManager.currentView = self
        appDeletgate.isOnTrackProgressVC = true
        appDeletgate.trackProgressVC = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
      
        //remove notification
        NotificationCenter.default.removeObserver("ShowRatingPopUp")
        
        callTimer?.invalidate()
        Singleton.sharedManager.currentView = nil
        if self.client != nil
        {
            self.client.unsubscribeFromAll()
        }
        appDeletgate.trackProgressVC = nil
        appDeletgate.isOnTrackProgressVC = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - MapBox Delegate Method
    // Called when Map load finish
    
    func mapViewDidFinishLoadingMap(_ mapView: MGLMapView) {
        
        self.getLocationData()
        
        // Wait for the map to load before initiating the first camera movement.
    }
    
    
    // This delegate method is where you tell the map to load a view for a specific annotation based on the willUseImage property of the custom subclass.
    func mapView(_ mapView: MGLMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {
        
        if let castAnnotation = annotation as? MyCustomPointAnnotation {
            if (castAnnotation.willUseImage) {
                return nil;
            }
        }
        let annotationView = MGLAnnotationView.init(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        /*
        //This one is for reuse annotaiton
        // Assign a reuse identifier to be used by both of the annotation views, taking advantage of their similarities.
        let reuseIdentifier = "reusableDotView"
        
        // For better performance, always try to reuse existing annotations.
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier)
        
        // If there’s no reusable annotation view available, initialize a new one.
        if annotationView == nil {
            
            annotationView = MGLAnnotationView(reuseIdentifier: reuseIdentifier)
            annotationView?.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        }
        */
        if let castAnnotation = annotation as? MyCustomPointAnnotation {
            
            if (castAnnotation.isShowTitle) {
                
                annotationView.layer.cornerRadius = 0
                annotationView.layer.borderWidth = 0.0
                annotationView.layer.borderColor = UIColor.clear.cgColor
                annotationView.backgroundColor = UIColor.clear
            
                self.lblPinDuration.layer.cornerRadius = self.lblPinDuration.frame.size.height / 2
                
                let value = CalenderUtility.getHourMinSecond(totalSeconds: expectedTravelTime)
                let temp = (value as NSString).uppercased
                let durations = temp.components(separatedBy: " ")
                
         
                var formated = Utilities.decorateText(attributedString: NSMutableAttributedString(attributedString: NSAttributedString(string: temp)), decorateString: durations[0], decorate: [NSFontAttributeName:UIFont.systemFontSemiBold(with: 12)])
                
                formated = Utilities.decorateText(attributedString: NSMutableAttributedString(attributedString: NSAttributedString(string: temp)), decorateString: durations[1], decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: 10)])
                
                
                self.lblPinDuration.attributedText = formated
                self.lblPinDuration.textAlignment = .center
                annotationView.addSubview(self.vwPinDuration)
            }else{
                
                /*annotationView?.layer.cornerRadius = (annotationView?.frame.size.width)! / 2
                annotationView?.layer.borderWidth = 5.0
                let opacity:CGFloat = 0.6
                let borderColor = UIColor.lightGray
                annotationView?.layer.borderColor = borderColor.withAlphaComponent(opacity).cgColor
                annotationView!.backgroundColor = UIColor.white
                
                let round = UIView(frame: CGRect(x: 15, y: 15, width: 20, height: 20))
                round.layer.cornerRadius = (round.frame.size.width) / 2
                round.backgroundColor = UIColor.init(colorLiteralRed: 0/255, green: 169/255, blue: 252/255, alpha: 1.0)
                */
                annotationView.layer.cornerRadius = 0
                annotationView.layer.borderWidth = 0.0
                annotationView.layer.borderColor = UIColor.clear.cgColor
                annotationView.backgroundColor = UIColor.clear
                
                let car = UIImageView(frame: CGRect(x: 12.5, y: 0, width: 25, height: 50))
                car.image = UIImage(named: "car")
                
                annotationView.addSubview(car)
            }
        }
        
        return annotationView
    }
    
    // This delegate method is where you tell the map to load an image for a specific annotation based on the willUseImage property of the custom subclass.
 /*   func mapView(_ mapView: MGLMapView, imageFor annotation: MGLAnnotation) -> MGLAnnotationImage? {
        
        if let castAnnotation = annotation as? MyCustomPointAnnotation {
            if (!castAnnotation.willUseImage) {
                return nil;
            }
        }
        
        // For better performance, always try to reuse existing annotations.
        var annotationImage = mapView.dequeueReusableAnnotationImage(withIdentifier: "car")
        
        // If there is no reusable annotation image available, initialize a new one.
        if(annotationImage == nil) {
            annotationImage = MGLAnnotationImage(image: UIImage(named: "car")!, reuseIdentifier: "car")
        }
        
        return annotationImage
    }
  */
    func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
        // Always allow callouts to popup when annotations are tapped.
        return true
    }

    // Helper forDocumentation Issue Group requesting a route
    func requestRoute() {
        
        guard waypoints.count > 0 else { return }
        
        
        // Set here seller current point
        
        let myLocation = CLLocation(latitude: self.arrMapData[0].companyLatitude, longitude: self.arrMapData[0].companyLongitude)
        
        let userWaypoint = Waypoint(location: myLocation/*(bMbMapView.userLocation?.location)!*/, heading: bMbMapView.userLocation?.heading, name: "user")
        waypoints.insert(userWaypoint, at: 0)
        
        let options = NavigationRouteOptions(waypoints: waypoints)
        
        _ = Directions.shared.calculate(options) { [weak self] (waypoints, routes, error) in
            guard error == nil else {
                print(error!.localizedDescription)
                return
            }
            guard let route = routes?.first else { return }
            
            self?.currentRoute = route
            
            // Open method for adding and updating the route line
            self?.bMbMapView.showRoute(route)
            self?.bMbMapView.showWaypoints(route, legIndex: 0)
            
            
            self?.bMbMapView.addArrow(route: route, legIndex: 5, stepIndex: 5)
            
            print(route.expectedTravelTime)
            
            self?.expectedTravelTime = route.expectedTravelTime
            
            print(CalenderUtility.getHourMinSecond(totalSeconds:route.expectedTravelTime))
            if self?.lblWaitingTime.text != "Service provider has arrived" {
                self?.lblWaitingTime.text = "Service provider will be there in \(CalenderUtility.getHourMinSecond(totalSeconds:route.expectedTravelTime))."
            }
            
            self?.bMbMapView.removeAnnotations((self?.bMbMapView.annotations!)!)
    
            self?.byuerLocation.coordinate = CLLocationCoordinate2D(latitude: (self?.arrMapData[0].userLatitude)!, longitude: (self?.arrMapData[0].userLongitude)!)
            self?.byuerLocation.willUseImage = false
            self?.byuerLocation.isShowTitle = true
            //self?.byuerLocation.title = CalenderUtility.getHourMinSecond(totalSeconds: (self?.expectedTravelTime)!)
            
            
            self?.vendorLocation.coordinate = CLLocationCoordinate2D(latitude: (self?.arrMapData[0].companyLatitude)!, longitude: (self?.arrMapData[0].companyLongitude)!)
            self?.vendorLocation.willUseImage = false
            
            // Fill an array with four point annotations.
            let myPlaces = [self?.byuerLocation, self?.vendorLocation]

            self?.bMbMapView.removeAnnotations(myPlaces as! [MGLAnnotation])
            
            // Add all annotations to the map all at once, instead of individually.
            self?.bMbMapView.addAnnotations(myPlaces as! [MGLAnnotation])
            
            self?.bMbMapView.showAnnotations((self?.bMbMapView.annotations!)!, animated: false)
            self?.bMbMapView.selectAnnotation((self?.byuerLocation)!, animated: true)
        }
        
    }
    
    func liveRequestRoute(location: CLLocation, bearing: CLLocationDirection) {
        guard waypoints.count > 0 else { return }

        
        let userWaypoint = Waypoint(location: location/*(bMbMapView.userLocation?.location)!*/, heading: bMbMapView.userLocation?.heading, name: "user")
        waypoints.insert(userWaypoint, at: 0)
        
        let options = NavigationRouteOptions(waypoints: waypoints)
        
        _ = Directions.shared.calculate(options) { [weak self] (waypoints, routes, error) in
            guard error == nil else {
                print(error!.localizedDescription)
                return
            }
            guard let route = routes?.first else { return }
            
            self?.currentRoute = route
            
            
            // Open method for adding and updating the route line
            self?.bMbMapView.showRoute(route)
            self?.bMbMapView.showWaypoints(route, legIndex: 0)
            
            print(route.expectedTravelTime)
            
            self?.expectedTravelTime = route.expectedTravelTime
            
            print(CalenderUtility.getHourMinSecond(totalSeconds:route.expectedTravelTime))
            if self?.lblWaitingTime.text != "Service provider has arrived"  {
                self?.lblWaitingTime.text = "Service provider will be there in \(CalenderUtility.getHourMinSecond(totalSeconds:route.expectedTravelTime))."
            }
            
            self?.bMbMapView.removeAnnotations((self?.bMbMapView.annotations!)!)
            
            self?.byuerLocation.coordinate = CLLocationCoordinate2D(latitude: (self?.arrMapData[0].userLatitude)!, longitude: (self?.arrMapData[0].userLongitude)!)
            self?.byuerLocation.willUseImage = false
            self?.byuerLocation.isShowTitle = true
            //self?.byuerLocation.title = CalenderUtility.getHourMinSecond(totalSeconds: (self?.expectedTravelTime)!)
            
            self?.vendorLocation.coordinate = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            self?.vendorLocation.willUseImage = false
            
            // Fill an array with four point annotations.
            let myPlaces = [self?.byuerLocation, self?.vendorLocation]
            
            self?.bMbMapView.removeAnnotations(myPlaces as! [MGLAnnotation])
            
            // Add all annotations to the map all at once, instead of individually.
            self?.bMbMapView.addAnnotations(myPlaces as! [MGLAnnotation])
            
            //self?.bMbMapView.showAnnotations((self?.bMbMapView.annotations!)!, animated: false)
            //self?.bMbMapView.selectAnnotation((self?.byuerLocation)!, animated: true)
        }
    }
    
    
    //MARK: - Private Mathods
    func initialConfig()
    {
        
        self.pubnubConfiguration(withCannel: self.arrMapData[0].pubChannelName)
        
        if let annotation = bMbMapView.annotations?.last, waypoints.count > 2 {
            bMbMapView.removeAnnotation(annotation)
        }
        
        if waypoints.count > 1 {
            waypoints = Array(waypoints.suffix(1))
        }
        
        // Set here user destination point
        
        let coordinates : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: self.arrMapData[0].userLatitude, longitude: self.arrMapData[0].userLongitude)
        
        let waypoint = Waypoint(coordinate: coordinates)
        waypoint.coordinateAccuracy = -1
        waypoints.append(waypoint)
        self.setupData()
        requestRoute()
        
        if didReciveNotification {
            self.showNotificationPopup()
        }
        
    }
    //MARK: - show rating popup using local notification
    func ShowRatingPopUp(notification: NSNotification){
       self.showNotificationPopup()
    }
    //MARK: - Private Methods
    func showNotificationPopup() {
        self.callTimer?.invalidate()
        let serviceID = self.arrMapData[0].serviceId
        let requestID = self.requestID
        let companyID = self.arrMapData[0].companyId
        let img = self.arrMapData[0].serviceProviderImage
        let name = self.arrMapData[0].serviceProvider
        lblWaitingTime.text = "Service provider has arrived"
        
        self.view.showRateReviewPopUp(serviceId: serviceID, requestId: requestID, companyId: companyID, serviceProviderImg: img, serviceProviderName: name) {
            
            let vc = BuyerPurchaseDetailsVC(nibName: "BuyerPurchaseDetailsVC", bundle: nil)
            vc.requestID = self.requestID
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    func getLocationData()
    {
        var dict                = [String : Any]()
        dict["sessionId"]       = DataModel.sharedInstance.onFetchSessionId()
        dict["userId"]          = DataModel.sharedInstance.onFetchUserId()
        dict["requestIds"]      = [self.requestID]
        APIManager.callAPI(url: Constant.serverAPI.URL.kGetPubnubData, inputParams: dict, success: { (dict) in
            KVNProgress.dismiss()
            
            if dict?["Result"] as? Bool == true {
                let data = dict?["Data"] as? [String:Any]
                if data?.count != 0
                {
                    let datas = data?["mapData"] as? NSArray
                    if datas != nil
                    {
                        for i in datas!
                        {
                            let data = MapData.init(fromDictionary: i as! [String : Any])
                            if data.status == 5 || data.status == 6
                            {
                                Utilities.showAlertWithAction(title: "", message: data.displayMessage, buttonTitle: "OK", buttonTitle2: "", onOKClick: {
                                    self.navigationController?.popViewController(animated: true)
                                    
                                    
                                }, onCancelClick: {
                                    
                                })
                                break
                            }
                            else
                            {
                                self.arrMapData.append(data)
                                self.initialConfig()
                                
                            }
                        }
                        
                    }
                }
            }
            else
            {
                Utilities.showAlertView(title: "", message: dict?["Message"] as? String)
            }
        }) { error in
            KVNProgress.dismiss()
            
            Utilities.showAlertView(title: "", message: error)
            
        }
        
    }
    
    func setupData()
    {
        self.lblProfileName.text = self.arrMapData[0].serviceProvider
        self.lblProfileCategory.text = self.arrMapData[0].catName
        
        self.imgProfilePic.sd_setImage(with: URL.init(string: self.arrMapData[0].serviceProviderImage), placeholderImage: nil, options: SDWebImageOptions.retryFailed, completed:{ (image, err, nil, url) in
            self.imgProfilePic.image = image
            self.imgProfilePic.contentMode = .scaleAspectFill
        })
        
    }
    
    func client(_ client: PubNub, didReceive status: PNStatus) {
        
        print(status)
    }
    
    override func client(_ client: PubNub, didReceiveMessage message: PNMessageResult) {
        
        // Handle new message stored in message.data.message
        if message.data.channel != message.data.subscription {
            
            print("message.data.subscription")
            // Message has been received on channel group stored in message.data.subscription.
        }
        else {
            
            print("message.data.channel")
            // Message has been received on channel stored in message.data.channel.
        }
        
        print("Received message: \(message.data.message ?? "") on channel \(message.data.channel) " +
            "at \(message.data.timetoken)")
        
        
        if let receivedMessage = message.data.message as? [String:Double]
        {
            
            
            let lng : CLLocationDegrees! = receivedMessage["longitude"]
            let lat : CLLocationDegrees! = receivedMessage["latitude"]
            let bearing : CLLocationDirection! = receivedMessage["bearing"]
            
            if lng != 0 && lat != 0  {
                
                
                let cord = CLLocationCoordinate2D(latitude: lat, longitude: lng)
                latestCordinates = cord
                
                UIView.animate(withDuration: 1.5, animations: {
                    // Here Core Animation calls MapBox using an implicit animation.
                    self.vendorLocation.coordinate = cord
                })
                
                
                /*
                 // Optionally set a starting point.
                 bMbMapView.setCenter(cord, zoomLevel: 7, direction: bearing, animated: true)
                 bMbMapView.setDirection(bearing, animated: true)
                 
                 */
                
                // Create a camera that rotates around the same center point, rotating 180°.
                // `fromDistance:` is meters above mean sea level that an eye would have to be in order to see what the map view is showing.
                let camera = MGLMapCamera(lookingAtCenter: cord, fromDistance: 5000, pitch: 7, heading: bearing)
                
                // Animate the camera movement over 5 seconds.
                bMbMapView.setCamera(camera, withDuration: 1, animationTimingFunction: CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut))
                
                
                
                if (self.isFirstMessage) {
                    
                    self.isFirstMessage = false
                    
                    bMbMapView.removeRoute()
                    bMbMapView.removeWaypoints()
                    waypoints.removeAll()
                    let waypoint = Waypoint(coordinate: CLLocationCoordinate2D(latitude: self.arrMapData[0].userLatitude, longitude: self.arrMapData[0].userLongitude))
                    waypoint.coordinateAccuracy = -1
                    waypoints.append(waypoint)
                    
                    self.liveRequestRoute(location: CLLocation(latitude: lat, longitude: lng), bearing: bearing)
                    
                    isAdded = true
                    
                    callTimer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector:#selector(calculateDistanceTime), userInfo: nil, repeats: true)
                    
                }
            }
        }
    }
    

    func calculateDistanceTime() {
        
        let waypoints = [
            Waypoint(coordinate: CLLocationCoordinate2D(latitude: latestCordinates.latitude, longitude: latestCordinates.longitude)),
            Waypoint(coordinate: CLLocationCoordinate2D(latitude: self.arrMapData[0].userLatitude, longitude: self.arrMapData[0].userLongitude))
            ]
        let options = RouteOptions(waypoints: waypoints, profileIdentifier: .automobileAvoidingTraffic)
        options.includesSteps = true
        
        
        _ = Directions.shared.calculate(options) { (waypoints, routes, error) in
            guard error == nil else {
                print("Error calculating directions: \(error!)")
                return
            }
            
            /*if let route = routes?.first, let leg = route.legs.first {
                print("Route via \(leg):")
                
                let distanceFormatter = LengthFormatter()
                let formattedDistance = distanceFormatter.string(fromMeters: route.distance)
                
                let travelTimeFormatter = DateComponentsFormatter()
                travelTimeFormatter.unitsStyle = .short
                let formattedTravelTime = travelTimeFormatter.string(from: route.expectedTravelTime)
                
                print("Distance: \(formattedDistance); ETA: \(formattedTravelTime!)")
                
                for step in leg.steps {
                    print("\(step.instructions)")
                    let formattedDistance = distanceFormatter.string(fromMeters: step.distance)
                    print("— \(formattedDistance) —")
                }
            }*/
            
            guard let route = routes?.first else { return }
            
            print(route.expectedTravelTime)
            
            self.expectedTravelTime = route.expectedTravelTime
            
            print(CalenderUtility.getHourMinSecond(totalSeconds:route.expectedTravelTime))
            if self.lblWaitingTime.text != "Service provider has arrived" {
                self.lblWaitingTime.text = "Service provider will be there in \(CalenderUtility.getHourMinSecond(totalSeconds:route.expectedTravelTime))."
            }
            
            let value = CalenderUtility.getHourMinSecond(totalSeconds: route.expectedTravelTime)
            let temp = (value as NSString).uppercased
            let durations = temp.components(separatedBy: " ")
            
            
            var formated = Utilities.decorateText(attributedString: NSMutableAttributedString(attributedString: NSAttributedString(string: temp)), decorateString: durations[0], decorate: [NSFontAttributeName:UIFont.systemFontSemiBold(with: 12)])
            
            formated = Utilities.decorateText(attributedString: NSMutableAttributedString(attributedString: NSAttributedString(string: temp)), decorateString: durations[1], decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: 10)])
            
            
            self.lblPinDuration.attributedText = formated
            
        }
    }
    
    
    //MARK: - IBAction Methods
    
    @IBAction func btnVendorReached_Action(_ sender: Any) {
        
        Utilities.showAlertWithAction(title: "", message: "Complete the Task", buttonTitle: "OK", buttonTitle2: "Cancel", onOKClick: {
            //Cancel pubnumb secri
            self.client.unsubscribeFromAll()
            
            var dictParam=[String:Any]()
            dictParam["userId"] = DataModel.sharedInstance.onFetchUserId()
            dictParam["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
            dictParam["requestId"] = self.requestID
            dictParam["pubChannelName"] = ""
            dictParam["status"] = 5
            
            APIManager.callAPI(url: Constant.serverAPI.URL.kChangeRequestStatus, inputParams: dictParam, success: { (response) in
                
                KVNProgress.dismiss()
                if response?["Result"] as? Bool == true {
                    //Show Review Popup
                    self.showNotificationPopup()
                }
            }) { (error) in
                KVNProgress.dismiss()
                Utilities.showAlertView(title: "", message: error)
            }
            
        }) {
            //Dismiss
        }
        
    }
    
    
    @IBAction func btnBackClicked(_ sender: Any) {
        
        callTimer?.invalidate()
        
        switch self.isFrom! {
        case .isFromNotification:
            
            let arrViews = self.navigationController!.viewControllers
            
            //            for (_,val) in arrViews.enumerated()
            //            {
            //                if(val.isKind(of: NotificationVC.self))
            //                {
            //                      self.navigationController!.popToViewController(NotificationVC, animated: true)
            //                }
            //                else
            //                {
            //                    self.navigationController?.popToRootViewController(animated: true)
            //                }
            //
            //            }
            for aViewController in arrViews {
                
                if aViewController.isKind(of: NotificationVC.self) {
                    
                    self.navigationController!.popToViewController(aViewController, animated: true)
                }
                else
                {
                    appDeletgate.onSetupDashboardPage()
                }
            }
            
        default:
            
            appDeletgate.onSetupDashboardPage()
            
            
        }
        
    }
    
    @IBAction func serviceDetails_Tap(_ sender: Any) {
        
        /* self.serviceDetailsPopup = Bundle.main.loadNibNamed("ServiceDetailsPopup", owner: self, options: nil)?[0] as! ServiceDetailsPopup
         self.serviceDetailsPopup.frame = self.view.frame
         
         self.serviceDetailsPopup.imgService.sd_setImage(with: URL.init(string: self.arrMapData[0].serviceProviderImage), placeholderImage: UIImage(named:Constant.PlaceHolder.kPhoto), options: SDWebImageOptions.retryFailed, completed: nil)
         var fontSize:CFloat = 15
         if Utilities.isDeviceiPad()
         {
         fontSize = 15 * 1.3
         }
         //==================== ServiceName =====================
         var attName = NSMutableAttributedString(string: "Service Name : " + self.arrMapData[0].screenName)
         attName = Utilities.decorateText(attributedString: attName, decorateString: self.arrMapData[0].screenName, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
         self.serviceDetailsPopup.lblTitle.attributedText = attName
         //===================================================
         
         //==================== Amount Paid =====================
         var attPaid = NSMutableAttributedString(string: "Amount Paid : $ " + self.arrMapData[0].requestFee)
         attPaid = Utilities.decorateText(attributedString: attPaid, decorateString: "$ " + self.arrMapData[0].requestFee, decorate: [NSFontAttributeName:UIFont.systemFontSemiBold(with: fontSize),NSForegroundColorAttributeName:Constant.Color.kGreen])
         self.serviceDetailsPopup.lbl1.attributedText = attPaid
         //=======================================================
         
         //==================== Payment Method =====================
         var attPaymentMethod = NSMutableAttributedString(string: "Payment Method : " + self.arrMapData[0].paymentMethod)
         attPaymentMethod = Utilities.decorateText(attributedString: attPaymentMethod, decorateString: self.arrMapData[0].paymentMethod, decorate: [NSFontAttributeName:UIFont.systemFontSemiBold(with: fontSize),NSForegroundColorAttributeName:Constant.Color.kBlue])
         self.serviceDetailsPopup.lbl2.attributedText = attPaymentMethod
         //=======================================================
         
         
         //==================== Service Provider =====================
         var attProvider = NSMutableAttributedString(string: "Service Provider : " + self.arrMapData[0].serviceProvider)
         attProvider = Utilities.decorateText(attributedString: attProvider, decorateString: self.arrMapData[0].serviceProvider, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName: UIColor.black])
         self.serviceDetailsPopup.lbl3.attributedText = attProvider
         //=======================================================
         
         //==================== Category =====================
         var attCate = NSMutableAttributedString(string: "Category : " + self.arrMapData[0].catName)
         attCate = Utilities.decorateText(attributedString: attCate, decorateString: self.arrMapData[0].catName, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
         self.serviceDetailsPopup.lbl4.attributedText = attCate
         //===================================================
         
         //==================== Status =====================
         var attStatus = NSMutableAttributedString(string: "Status : " + Constant.StatusText.kOntheWay)
         attStatus = Utilities.decorateText(attributedString: attStatus, decorateString: Constant.StatusText.kOntheWay, decorate: [NSFontAttributeName:UIFont.systemFontLight(with: fontSize),NSForegroundColorAttributeName: UIColor.black])
         self.serviceDetailsPopup.lbl5.attributedText = attStatus
         //===================================================
         
         //==================== Schedule =====================
         var attSchedule = NSMutableAttributedString(string: "Schedule : " + self.arrMapData[0].duration)
         attSchedule = Utilities.decorateText(attributedString: attSchedule, decorateString: self.arrMapData[0].duration, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
         self.serviceDetailsPopup.lbl6.attributedText = attSchedule
         //===================================================
         
         
         
         self.view.addSubview(self.serviceDetailsPopup)*/
        
    }
}
