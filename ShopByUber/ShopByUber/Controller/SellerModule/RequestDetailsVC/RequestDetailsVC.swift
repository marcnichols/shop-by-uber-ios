//
//  RequestDetailsVC.swift
//  ShopByUber
//
//  Created by Nirav Shah on 6/29/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class RequestDetailsVC: UIViewController {
    
    //MARK:- IBOutlet
    @IBOutlet weak var imgService: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var lbl3: UILabel!
    @IBOutlet weak var lbl4: UILabel!
    @IBOutlet weak var lbl5: UILabel!
    @IBOutlet weak var lbl6: UILabel!
    @IBOutlet weak var lbl7: UILabel!
    @IBOutlet weak var lbl8: UILabel! //Cancel Reason
    @IBOutlet weak var lbl9: UILabel!
    @IBOutlet weak var lbl10: UILabel! //SubCatagory
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var vwImage: UIView!
    @IBOutlet weak var vwStatus: UIView!
    @IBOutlet weak var btnChangeStatus: UIButton!
    @IBOutlet weak var btnCancleRequest: UIButton!
    @IBOutlet weak var lblScreenTitle: UILabel!
    @IBOutlet weak var constraintBtnChangeStatusHeight: NSLayoutConstraint!
    @IBOutlet weak var ConstraintBtnViewHeight: NSLayoutConstraint!

    //MARK:- Variable
    var requestDetails:RequestDetail!
    var requestStatus:Constant.RequestStatus!
    var requestID:String!
    var requestIndexPath:IndexPath!
    var strTitle: String?
    var handlerDeleteEvent:((IndexPath)->())?
    var tapAddress: UITapGestureRecognizer?
    //MARK:- View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        let notificationName = Notification.Name("RequestDetailsVC")
        NotificationCenter.default.addObserver(self, selector: #selector(initialConfig), name: notificationName, object: nil)
        tapAddress = UITapGestureRecognizer(target: self, action: #selector(addressTapped(address:)))
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.initialConfig()
        Singleton.sharedManager.currentView = self
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        Singleton.sharedManager.currentView = nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initialConfig()
    {
        Utilities.applyShadowEffect(view: [vwImage])
        Utilities.applyShadowEffect(view: [vwStatus])
        self.lblScreenTitle.text = self.strTitle
        
        self.scrollView.isHidden = true

        
        RequestDetail.getRequestDetail(requestID: requestID, isShowloader: true) { (response) in
            self.requestDetails = response
            
            self.scrollView.isHidden = false
            self.btnCancleRequest.isHidden = false
            self.btnChangeStatus.isHidden = false
            
            if self.requestDetails.status == 0
            {
                self.btnChangeStatus.setTitle(Constant.StatusText.kCONFIRMAPPOINMENT, for: .normal)
                
            }
            else if self.requestDetails.status == 1
            {
                //Hide Both Buttons
                self.constraintBtnChangeStatusHeight.constant = 0
            }
            else if self.requestDetails.status == 2
            {
                self.btnChangeStatus.setTitle(Constant.StatusText.kONROUTETOCLIENT, for: .normal)
                self.btnChangeStatus.backgroundColor = Constant.Color.kPinkColor
                //Hide Cancel Button
                self.btnCancleRequest.isHidden = true
                self.ConstraintBtnViewHeight.constant = 60
            }
            else if self.requestDetails.status == 4
            {
               
                self.btnChangeStatus.setTitle(Constant.StatusText.kCompleteService, for: .normal)
                self.btnChangeStatus.backgroundColor = Constant.Color.kPinkColor
                 //Hide Cancel Button
                self.btnCancleRequest.isHidden = true
                self.ConstraintBtnViewHeight.constant = 60
            }
            else if self.requestDetails.status == 3
            {
                self.btnChangeStatus.setTitle(Constant.StatusText.kDirectionstoBuyer, for: .normal)
                self.btnChangeStatus.backgroundColor = Constant.Color.kPinkColor
                //Hide Cancel Button
                self.btnCancleRequest.isHidden = true
                self.ConstraintBtnViewHeight.constant = 60
            }
            else
            {
                self.btnCancleRequest.isHidden = true
                self.btnChangeStatus.isHidden = true
            }
            
            switch self.requestDetails.status {
            case 0,1:
                self.lblScreenTitle.text =  Constant.DetailsScreenTitle.kViewRequest
            case 5,6:
                
                self.lblScreenTitle.text =  Constant.DetailsScreenTitle.kViewSale
            case 2,3,4:
                
                self.lblScreenTitle.text =  Constant.DetailsScreenTitle.kViewAppointment
            default:
                break
            }
            
            self.setupData()
        }
        
    }
    
    func addressTapped(address: UITapGestureRecognizer)
    {
        let vc = UserMapLocationVC(nibName: "UserMapLocationVC", bundle: nil)
        vc.latitude = self.requestDetails.userLatitude
        vc.longitude = self.requestDetails.userLongitude
        vc.requestDetails = self.requestDetails
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func setupData()
    {
        
        imgService.sd_setImage(with: URL(string:requestDetails.userProfileImage), placeholderImage: UIImage(named:Constant.PlaceHolder.kPhoto))
        var fontSize:CFloat = 15
        if Utilities.isDeviceiPad()
        {
            fontSize = 15 * 1.3
        }
        //==================== ServiceName =====================
        var attName = NSMutableAttributedString(string: "Service Name : " + requestDetails.screenName)
        attName = Utilities.decorateText(attributedString: attName, decorateString: requestDetails.screenName, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
        lblTitle.attributedText = attName
        //===================================================
        
       
        
        var statusText = ""
        var  statusColor = UIColor.black
        var methodtext = "Payment Method : "
        switch self.requestDetails.status! {
        case 0:
            statusText = Constant.StatusText.kRequested
        case 1:
            statusText = Constant.StatusText.kWaitingForPayment
            ConstraintBtnViewHeight.constant = 80
        case 2:
            statusText = Constant.StatusText.kNotStarted
            
        case 3:
            statusText = Constant.StatusText.kOntheWay
            
        case 4:
            statusText = Constant.StatusText.kInProgress
            statusColor = Constant.Color.kInProgress
            ConstraintBtnViewHeight.constant = 80
            
        case 5:
            statusText = Constant.StatusText.kCompleted
            self.ConstraintBtnViewHeight.constant = 0

        case 6:
            if requestDetails.isCancelled
            {
                statusText = requestDetails.cancelByText   //Constant.StatusText.kCancelledby + "Buyer"
                
            }
            else
            {
                statusText = requestDetails.cancelByText  //Constant.StatusText.kCancelledby + "User"
            }
            statusColor = Constant.Color.kCancelStatus
            methodtext = "Refund Method : "
            self.ConstraintBtnViewHeight.constant = 0

        default:
            break
        }
        
        if self.requestDetails.status == 0
        {
            
            //==================== Service Detail  =====================
            
            var attServiceDetails = NSMutableAttributedString(string: "Service Detail : ")
            var attServiceDesc =  NSMutableAttributedString(string: requestDetails.serviceDetail)
            
            attServiceDetails = Utilities.decorateText(attributedString: attServiceDetails, decorateString: requestDetails.serviceDetail, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
            attServiceDesc = Utilities.decorateText(attributedString: attServiceDesc, decorateString: requestDetails.serviceDetail, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
            
            lbl1.attributedText = attServiceDetails
            lbl9.attributedText = attServiceDesc
            //=======================================================
            
            //==================== Amount Paid =====================
            var attPaid = NSMutableAttributedString(string: "Amount : $ " + requestDetails.amountPaid)
            attPaid = Utilities.decorateText(attributedString: attPaid, decorateString: "$ " + requestDetails.amountPaid, decorate: [NSFontAttributeName:UIFont.systemFontSemiBold(with: fontSize),NSForegroundColorAttributeName:Constant.Color.kGreen])
            lbl3.attributedText = attPaid
            //=======================================================

            
            //==================== Request From =====================
            var attRequest = NSMutableAttributedString(string: "Request From : " + requestDetails.requestFrom)
            attRequest = Utilities.decorateText(attributedString: attRequest, decorateString: requestDetails.requestFrom, decorate: [NSFontAttributeName:UIFont.systemFontSemiBold(with: fontSize),NSForegroundColorAttributeName:Constant.Color.kGreen])
            lbl2.attributedText = attRequest
            //=======================================================
            
            //==================== Category =====================
            var attCate = NSMutableAttributedString(string: "Category : " + requestDetails.catName)
            attCate = Utilities.decorateText(attributedString: attCate, decorateString: requestDetails.catName, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
            lbl4.attributedText = attCate
            //==================== Sub Category =====================
            var attSubCate = NSMutableAttributedString(string: "Sub Category : " + requestDetails.subcatName)
            attSubCate = Utilities.decorateText(attributedString: attSubCate, decorateString: requestDetails.subcatName, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
            lbl10.attributedText = attSubCate
        
            //==================== Schedule =====================
            var attSchedule = NSMutableAttributedString(string: "Schedule : " + requestDetails.schedule)
            attSchedule = Utilities.decorateText(attributedString: attSchedule, decorateString: requestDetails.schedule, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
            lbl5.attributedText = attSchedule
            //===================================================
            
            //==================== Address =====================
            var attAddress = NSMutableAttributedString(string: "Address : " + requestDetails.userAddress)
            attAddress = Utilities.decorateText(attributedString: attAddress, decorateString: requestDetails.userAddress, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
            lbl6.attributedText = attAddress
            lbl6.addGestureRecognizer(self.tapAddress!)
            //===================================================
            lbl7.text = ""
            lbl8.text = ""
        }
        else if self.requestDetails.status == 1
        {
            
            //==================== Service Detail  =====================
            var attServiceDetails = NSMutableAttributedString(string: "Service Detail : ")
            var attServiceDesc =  NSMutableAttributedString(string: requestDetails.serviceDetail)
            
            attServiceDetails = Utilities.decorateText(attributedString: attServiceDetails, decorateString: requestDetails.serviceDetail, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
            attServiceDesc = Utilities.decorateText(attributedString: attServiceDesc, decorateString: requestDetails.serviceDetail, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
            
            lbl1.attributedText = attServiceDetails
            lbl9.attributedText = attServiceDesc
            //=======================================================
            
            //==================== Amount Paid =====================
            var attPaid = NSMutableAttributedString(string: "Amount : $ " + requestDetails.amountPaid)
            attPaid = Utilities.decorateText(attributedString: attPaid, decorateString: "$ " + requestDetails.amountPaid, decorate: [NSFontAttributeName:UIFont.systemFontSemiBold(with: fontSize),NSForegroundColorAttributeName:Constant.Color.kGreen])
            lbl3.attributedText = attPaid
            //=======================================================
            
            
            //==================== Request From =====================
            var attRequest = NSMutableAttributedString(string: "Request From : " + requestDetails.requestFrom)
            attRequest = Utilities.decorateText(attributedString: attRequest, decorateString: requestDetails.requestFrom, decorate: [NSFontAttributeName:UIFont.systemFontSemiBold(with: fontSize),NSForegroundColorAttributeName:Constant.Color.kGreen])
            lbl2.attributedText = attRequest
            //=======================================================
            
            //==================== Category =====================
            var attCate = NSMutableAttributedString(string: "Category : " + requestDetails.catName)
            attCate = Utilities.decorateText(attributedString: attCate, decorateString: requestDetails.catName, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
            lbl4.attributedText = attCate
            //==================== Sub Category =====================
            var attSubCate = NSMutableAttributedString(string: "Sub Category : " + requestDetails.subcatName)
            attSubCate = Utilities.decorateText(attributedString: attSubCate, decorateString: requestDetails.subcatName, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
            lbl10.attributedText = attSubCate
            
            //==================== Status =====================
            var attStatus = NSMutableAttributedString(string: "Status : " + statusText)
            attStatus = Utilities.decorateText(attributedString: attStatus, decorateString: statusText, decorate: [NSFontAttributeName:UIFont.systemFontLight(with: fontSize), NSForegroundColorAttributeName: statusColor])
            lbl5.attributedText = attStatus
            //===================================================
            
            //==================== Schedule =====================
            var attSchedule = NSMutableAttributedString(string: "Schedule : " + requestDetails.schedule)
            attSchedule = Utilities.decorateText(attributedString: attSchedule, decorateString: requestDetails.schedule, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
            lbl6.attributedText = attSchedule
            //===================================================
            
            //==================== Address =====================
            var attAddress = NSMutableAttributedString(string: "Address : " + requestDetails.userAddress)
            attAddress = Utilities.decorateText(attributedString: attAddress, decorateString: requestDetails.userAddress, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
            lbl7.attributedText = attAddress
            lbl7.addGestureRecognizer(self.tapAddress!)
            //===================================================
            
            lbl8.text = ""
        }
        else if self.requestDetails.status == 6
        {
            if self.requestDetails.isRefund
            {
                //==================== Service Detail  =====================
                var attServiceDetails = NSMutableAttributedString(string: "Service Detail : ")
                var attServiceDesc =  NSMutableAttributedString(string: requestDetails.serviceDetail)
                
                attServiceDetails = Utilities.decorateText(attributedString: attServiceDetails, decorateString: requestDetails.serviceDetail, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
                attServiceDesc = Utilities.decorateText(attributedString: attServiceDesc, decorateString: requestDetails.serviceDetail, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
                
                lbl1.attributedText = attServiceDetails
                lbl9.attributedText = attServiceDesc
                //=======================================================
                
                //==================== Amount Paid =====================
                var attPaid = NSMutableAttributedString(string: "Amount Refund : $ " + requestDetails.amountPaid)
                attPaid = Utilities.decorateText(attributedString: attPaid, decorateString: "$ " + requestDetails.amountPaid, decorate: [NSFontAttributeName:UIFont.systemFontSemiBold(with: fontSize),NSForegroundColorAttributeName:Constant.Color.kGreen])
                lbl2.attributedText = attPaid
                //=======================================================
                
                //==================== Refund Method =====================
                var attRefundMethod = NSMutableAttributedString(string: methodtext + requestDetails.refundMethod)
                attRefundMethod = Utilities.decorateText(attributedString: attRefundMethod, decorateString: requestDetails.refundMethod, decorate: [NSFontAttributeName:UIFont.systemFontSemiBold(with: fontSize),NSForegroundColorAttributeName:Constant.Color.kBlue])
                lbl3.attributedText = attRefundMethod
                
                //=======================================================
                
                //==================== Category =====================
                var attCate = NSMutableAttributedString(string: "Category : " + requestDetails.catName)
                attCate = Utilities.decorateText(attributedString: attCate, decorateString: requestDetails.catName, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
                lbl4.attributedText = attCate
                //==================== Sub Category =====================
                var attSubCate = NSMutableAttributedString(string: "Sub Category : " + requestDetails.subcatName)
                attSubCate = Utilities.decorateText(attributedString: attSubCate, decorateString: requestDetails.subcatName, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
                lbl10.attributedText = attSubCate
                
                //==================== Status =====================
                var attStatus = NSMutableAttributedString(string: "Status : " + statusText)
                attStatus = Utilities.decorateText(attributedString: attStatus, decorateString: statusText, decorate: [NSFontAttributeName:UIFont.systemFontLight(with: fontSize), NSForegroundColorAttributeName: statusColor])
                lbl5.attributedText = attStatus
                //===================================================
                
                //==================== Cancel Reason =====================
                var attCancelReason = NSMutableAttributedString(string: "Cancellation Reason : " + requestDetails.cancelReason)
                attCancelReason = Utilities.decorateText(attributedString: attCancelReason, decorateString: requestDetails.cancelReason, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
                lbl8.attributedText = attCancelReason
                //===================================================
                
                //==================== Schedule =====================
                var attSchedule = NSMutableAttributedString(string: "Schedule : " + requestDetails.schedule)
                attSchedule = Utilities.decorateText(attributedString: attSchedule, decorateString: requestDetails.schedule, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
                lbl6.attributedText = attSchedule
                //===================================================
                
                //==================== Address =====================
                var attAddress = NSMutableAttributedString(string: "Address : " + requestDetails.userAddress)
                attAddress = Utilities.decorateText(attributedString: attAddress, decorateString: requestDetails.userAddress, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
                lbl7.attributedText = attAddress
                lbl7.addGestureRecognizer(self.tapAddress!)
               
                //===================================================

            }
            else
            {
                //==================== Service Detail  =====================
                var attServiceDetails = NSMutableAttributedString(string: "Service Detail : ")
                var attServiceDesc =  NSMutableAttributedString(string: requestDetails.serviceDetail)
                
                attServiceDetails = Utilities.decorateText(attributedString: attServiceDetails, decorateString: requestDetails.serviceDetail, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
                attServiceDesc = Utilities.decorateText(attributedString: attServiceDesc, decorateString: requestDetails.serviceDetail, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
                
                lbl1.attributedText = attServiceDetails
                lbl9.attributedText = attServiceDesc
                //=======================================================
                
                //==================== Request From =====================
                var attRequest = NSMutableAttributedString(string: "Request From : " + requestDetails.requestFrom)
                attRequest = Utilities.decorateText(attributedString: attRequest, decorateString: requestDetails.requestFrom, decorate: [NSFontAttributeName:UIFont.systemFontSemiBold(with: fontSize),NSForegroundColorAttributeName:Constant.Color.kGreen])
                lbl2.attributedText = attRequest
                //=======================================================
                
                //==================== Category =====================
                var attCate = NSMutableAttributedString(string: "Category : " + requestDetails.catName)
                attCate = Utilities.decorateText(attributedString: attCate, decorateString: requestDetails.catName, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
                lbl3.attributedText = attCate
                //==================== Sub Category =====================
                var attSubCate = NSMutableAttributedString(string: "Sub Category : " + requestDetails.subcatName)
                attSubCate = Utilities.decorateText(attributedString: attSubCate, decorateString: requestDetails.subcatName, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
                lbl4.attributedText = attSubCate
                
                //==================== Status =====================
                var attStatus = NSMutableAttributedString(string: "Status : " + statusText)
                attStatus = Utilities.decorateText(attributedString: attStatus, decorateString: statusText, decorate: [NSFontAttributeName:UIFont.systemFontLight(with: fontSize), NSForegroundColorAttributeName: statusColor])
                lbl10.attributedText = attStatus
                //===================================================
                
                //==================== Cancel Reason =====================
                var attCancelReason = NSMutableAttributedString(string: "Cancellation Reason : " + requestDetails.cancelReason)
                attCancelReason = Utilities.decorateText(attributedString: attCancelReason, decorateString: requestDetails.cancelReason, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
                lbl8.attributedText = attCancelReason
                //===================================================
                
                //==================== Schedule =====================
                var attSchedule = NSMutableAttributedString(string: "Schedule : " + requestDetails.schedule)
                attSchedule = Utilities.decorateText(attributedString: attSchedule, decorateString: requestDetails.schedule, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
                lbl5.attributedText = attSchedule
                //===================================================
                
                //==================== Address =====================
                var attAddress = NSMutableAttributedString(string: "Address : " + requestDetails.userAddress)
                attAddress = Utilities.decorateText(attributedString: attAddress, decorateString: requestDetails.userAddress, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
                lbl6.attributedText = attAddress
                lbl6.addGestureRecognizer(self.tapAddress!)

                //===================================================
                lbl7.text = ""

            }
        }
        else
        {
            //==================== Service Detail  =====================
            var attServiceDetails = NSMutableAttributedString(string: "Service Detail : ")
            var attServiceDesc =  NSMutableAttributedString(string: requestDetails.serviceDetail)
            
            attServiceDetails = Utilities.decorateText(attributedString: attServiceDetails, decorateString: requestDetails.serviceDetail, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
            attServiceDesc = Utilities.decorateText(attributedString: attServiceDesc, decorateString: requestDetails.serviceDetail, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
            
            lbl1.attributedText = attServiceDetails
            lbl9.attributedText = attServiceDesc
            //=======================================================
            
            //==================== Amount Paid =====================
            var attPaid = NSMutableAttributedString(string: "Amount Paid : $ " + requestDetails.amountPaid)
            attPaid = Utilities.decorateText(attributedString: attPaid, decorateString: "$ " + requestDetails.amountPaid, decorate: [NSFontAttributeName:UIFont.systemFontSemiBold(with: fontSize),NSForegroundColorAttributeName:Constant.Color.kGreen])
            lbl2.attributedText = attPaid
            //=======================================================
            
            //==================== Refund Method =====================
            var attRefundMethod = NSMutableAttributedString(string: methodtext + requestDetails.refundMethod)
            attRefundMethod = Utilities.decorateText(attributedString: attRefundMethod, decorateString: requestDetails.refundMethod, decorate: [NSFontAttributeName:UIFont.systemFontSemiBold(with: fontSize),NSForegroundColorAttributeName:Constant.Color.kBlue])
            lbl3.attributedText = attRefundMethod
            //=======================================================
            
            //==================== Category =====================
            var attCate = NSMutableAttributedString(string: "Category : " + requestDetails.catName)
            attCate = Utilities.decorateText(attributedString: attCate, decorateString: requestDetails.catName, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
            lbl4.attributedText = attCate
            //==================== Sub Category =====================
            var attSubCate = NSMutableAttributedString(string: "Sub Category : " + requestDetails.subcatName)
            attSubCate = Utilities.decorateText(attributedString: attSubCate, decorateString: requestDetails.subcatName, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
            lbl10.attributedText = attSubCate
            
            //==================== Status =====================
            var attStatus = NSMutableAttributedString(string: "Status : " + statusText)
            attStatus = Utilities.decorateText(attributedString: attStatus, decorateString: statusText, decorate: [NSFontAttributeName:UIFont.systemFontLight(with: fontSize), NSForegroundColorAttributeName: statusColor])
            lbl5.attributedText = attStatus
            //===================================================
            
            //==================== Schedule =====================
            var attSchedule = NSMutableAttributedString(string: "Schedule : " + requestDetails.schedule)
            attSchedule = Utilities.decorateText(attributedString: attSchedule, decorateString: requestDetails.schedule, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
            lbl6.attributedText = attSchedule
            //===================================================
            //==================== Address =====================
            var attAddress = NSMutableAttributedString(string: "Address : " + requestDetails.userAddress)
            attAddress = Utilities.decorateText(attributedString: attAddress, decorateString: requestDetails.userAddress, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
            lbl7.attributedText = attAddress
            lbl7.addGestureRecognizer(self.tapAddress!)

            //===================================================

            lbl8.text = ""
        }
        
    }
    
    //MARK:- IBActions
    @IBAction func btnBack_Clicked(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnChangeStatus_Clicked(_ sender: Any) {
        
        if self.requestDetails.status! != 3
        {
            KVNProgress.show()
            
            var dictParam=[String:Any]()
            dictParam["userId"] = DataModel.sharedInstance.onFetchUserId()
            dictParam["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
            dictParam["requestId"] = requestID
            dictParam["pubChannelName"] = ""
            
            
            
            switch self.requestDetails.status! {
            case 0:
                //Request
                dictParam["status"] = 1
                
            case 2:
                //paydone
                dictParam["status"] = 3
                dictParam["pubChannelName"] = self.requestID
                
            case 4:
                //start service
                dictParam["status"] = 5
                
                
            default:
                break
            }
            
            APIManager.callAPI(url: Constant.serverAPI.URL.kChangeRequestStatus, inputParams: dictParam, success: { (response) in
                
                KVNProgress.dismiss()
                if response?["Result"] as? Bool == true {
                    if self.requestDetails.status! == 2 || self.requestDetails.status! == 3
                    {
                        if let dict = response?["Data"] as? [String:Any]
                        {
                            let track = TrackVC.init(nibName: "TrackVC", bundle: nil)
                            track.latitude = dict["latitude"] as? Double ?? 0.0
                            track.longitude = dict["longitude"] as? Double ?? 0.0
                            track.channleName = dict["pubChannelName"] as? String ?? ""
                            track.requestID = self.requestID
                            self.navigationController?.pushViewController(track, animated: true)
                        }
                    }
                    else if self.requestDetails.status! == 0
                    {
                        Utilities.showAlertWithAction(title: "", message: (response?["Message"] as? String)!, buttonTitle: "OK", buttonTitle2: "", onOKClick: {
                            self.navigationController?.popViewController(animated: true)
                            if self.handlerDeleteEvent != nil
                            {
                                self.handlerDeleteEvent!(self.requestIndexPath)
                            }
                        }, onCancelClick: {
                            
                        })
                    }
                    else
                    {
                        
                        self.navigationController?.popViewController(animated: true)
                        if self.handlerDeleteEvent != nil
                        {
                            self.handlerDeleteEvent!(self.requestIndexPath)
                        }
                    }
                }
                else if response?["Result"] as? Bool == false
                {
                 let msg = response?["Message"] as? String
                  Utilities.showAlertView(title: "", message: msg)
                }
                
            }) { (error) in
                KVNProgress.dismiss()
                Utilities.showAlertView(title: "", message: error)
            }
        }
        else
        {
            self.gotoTracking()
        }
    }
    
    func gotoTracking()
    {
        let track = TrackVC.init(nibName: "TrackVC", bundle: nil)
        track.latitude = requestDetails.userLatitude
        track.longitude = requestDetails.userLongitude
        track.channleName = requestDetails.pubChannelName
        track.requestID = requestID
        self.navigationController?.pushViewController(track, animated: true)
    }
    
    @IBAction func btnCancleRequestClick(_ sender: Any) {
        let vc = CancelOrderVC(nibName: "CancelOrderVC", bundle: nil)
        vc.requestID = self.requestID
        vc.requestIndexPath  = self.requestIndexPath
        vc.handlerDeleteEvent = {(deleteIndexPath) in
            self.handlerDeleteEvent!(self.requestIndexPath)
        };
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
