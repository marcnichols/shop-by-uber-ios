//
//  UserMapLocationVC.swift
//  ShopByUber
//
//  Created by Differenz215 on 8/28/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class UserMapLocationVC: BaseView {
    
    //MARK: Variables
    var currentPositionMarker = GMSMarker()
    var mapBearing:CLLocationDirection!
    var lastDriverBearing:CLLocationDirection!
    var arr_pubnubChannel = [String]()
    var currentMarker:GMSMarker = GMSMarker()
    var latitude:Double = 0
    var longitude:Double = 0
    var requestDetails:RequestDetail!
    
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setCurrentLocationPin()
        if self.latitude != 0 && self.longitude != 0
        {
            setUsersMarker(lat: self.latitude, lon: self.longitude)
        }
    }
    
    func setCurrentLocationPin()
    {
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        
        let location:CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: self.requestDetails.companyLatitude, longitude: self.requestDetails.companyLatitude)
        
        
        let locationMarker = GMSMarker(position: location)
        locationMarker.icon = #imageLiteral(resourceName: "car")
        locationMarker.title = ""
        locationMarker.appearAnimation = .pop
        locationMarker.map = self.mapView
        marker.append(locationMarker)
        
    }
    
    func setUsersMarker(lat: Double, lon: Double)
    {
        let locationMarker = GMSMarker()
        mapView.animate(toZoom: Float(Constant.ZoomLevel.Landmass))
        let location:CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: lat , longitude: lon )
        mapView.animate(toLocation: location)
        mapView.camera = GMSCameraPosition.camera(withTarget: location, zoom: 9)
        locationMarker.position = location
        locationMarker.icon = #imageLiteral(resourceName: "ic_marker")
        locationMarker.title = ""
        locationMarker.appearAnimation = .pop
        locationMarker.map = self.mapView
        marker.append(locationMarker)
    }
}
