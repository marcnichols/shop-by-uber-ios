//
//  MyRequestsVC.swift
//  ShopByUber
//
//  Created by Nirav Shah on 6/28/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class MyRequestsVC: BaseView {
    
    @IBOutlet weak var tblRequest: UITableView!
    @IBOutlet var lblNoDataFound: UILabel!

    var arrRequest = [MyRequest]()
    var pageNo:Int = 0
    var numOfRecords = 10
    var isRefreshing:Bool = false
    
    //MARK:- View Lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.initialConfig()
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        arrRequest.removeAll()
        pageNo = 0
        numOfRecords = 10
        if arrRequest.count == 0
        {
            self.setPagination()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Private Methods
    
    func initialConfig(){
        
        tblRequest.register(UINib.init(nibName: "ServiceCell", bundle: nil), forCellReuseIdentifier: "ServiceCell")
    
    }
    
    func setPagination() {
        self.getMyRequest(showLoader: true)
        
        tblRequest.es.addInfiniteScrolling {
            self.refreshRecord()
        }
        
        tblRequest.es.addPullToRefresh {

        if !(!self.isRefreshing) {
            self.arrRequest.removeAll()
            self.tblRequest.reloadData()
            self.pageNo = 0
            self.refreshRecord()
        }
        else {
            self.tblRequest.es.stopPullToRefresh()
        }
        }
    }
    
    func refreshRecord()
    {
        if self.isRefreshing == false
        {
            if self.pageNo == 0
            {
                self.getMyRequest(showLoader: self.arrRequest.count==0 ?true :false)
            }
            else
            {
                self.getMyRequest(showLoader: false)
            }
        }
    }
    
    func getMyRequest(showLoader:Bool)
    {
        pageNo += 1
        self.isRefreshing = true
        MyRequest.getMyRequest(pageNo: pageNo, numOfRecords: numOfRecords, isShowloader: showLoader, success: { (requests) in
            if self.pageNo == 1
            {
                self.arrRequest.removeAll()
            }
            if requests.count == 0
            {
                self.pageNo -= 1
            }
            else
            {
                if requests.count < self.numOfRecords
                {
                    self.pageNo -= 1
                }
                
                if self.arrRequest.count == 0
                {
                    self.arrRequest.append(contentsOf: requests)
                }
                else
                {
                    let responsRecord = requests as NSArray
                    //let tempLocal = self.arrRequest as NSArray
                    responsRecord.enumerateObjects({ (objF, idxF, nil) in
                        let objRequests = objF as! MyRequest
                
                        if self.arrRequest.contains(where: {$0.requestId == objRequests.requestId}) {
                            //
                            print("it exists, do something")
                        } else {
                            //
                            print("item could not be found")
                            self.arrRequest.append(objRequests)
                        }
    
                    })
                }
            }
            
            self.stopLoading()
        }) {
            self.pageNo -= 1
            self.stopLoading()
        }

    }
    
    func stopLoading()
    {
        self.isRefreshing = false
        self.tblRequest.es.stopPullToRefresh()
        self.tblRequest.es.stopLoadingMore()
        self.tblRequest.reloadData()
        self.lblNoDataFound.isHidden = arrRequest.count==0 ?false :true
    }
    
}

extension MyRequestsVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Utilities.isDeviceiPad() ? 92.0 : 72.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrRequest.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceCell", for: indexPath) as! ServiceCell
        
        Utilities.applyShadowEffect(view: [cell.vwBG])
        cell.lblTitle.text = arrRequest[indexPath.row].screenName
        
        //================= Duration ====================
        let attDuration = NSMutableAttributedString(string: arrRequest[indexPath.row].duration)
        attDuration.addAttributes([NSForegroundColorAttributeName:Constant.Color.kGreen,NSFontAttributeName:UIFont.systemFontSemiBold(with: CFloat(cell.lblDescription.font.pointSize))], range: NSMakeRange(0, attDuration.string.count))
        cell.lblDescription.attributedText = attDuration
        //===============================================
       
        cell.serviceImage.sd_setImage(with: URL(string: arrRequest[indexPath.row].serviceImage), placeholderImage: UIImage(named:Constant.PlaceHolder.kPhoto))
        cell.backgroundColor = UIColor.clear
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let data = self.arrRequest[indexPath.row]
        
        
        switch data.status! {
        case 0:
            self.pushViewControllerToDetails(requestStatus: Constant.RequestStatus.Requested, requestId: data.requestId, strTitle: Constant.DetailsScreenTitle.kViewRequest, indexpath: indexPath)
        case 1:
            self.pushViewControllerToDetails(requestStatus: Constant.RequestStatus.Confirm, requestId: data.requestId, strTitle: Constant.DetailsScreenTitle.kViewRequest, indexpath: indexPath)
        case 2:
            self.pushViewControllerToDetails(requestStatus: Constant.RequestStatus.PayDone, requestId: data.requestId, strTitle: Constant.DetailsScreenTitle.kViewAppointment, indexpath: indexPath)
        case 3:
             self.pushViewControllerToDetails(requestStatus: Constant.RequestStatus.OnRouteToClient, requestId: data.requestId, strTitle: Constant.DetailsScreenTitle.kViewAppointment, indexpath: indexPath)
            
        case 4:
            self.pushViewControllerToDetails(requestStatus: Constant.RequestStatus.StartService, requestId: data.requestId, strTitle: Constant.DetailsScreenTitle.kViewAppointment, indexpath: indexPath)
           
        case 5:
            self.pushViewControllerToDetails(requestStatus: Constant.RequestStatus.Completed, requestId: data.requestId, strTitle: Constant.DetailsScreenTitle.kViewSale, indexpath: indexPath)
        case 6:
            self.pushViewControllerToDetails(requestStatus: Constant.RequestStatus.Cancelled, requestId: data.requestId, strTitle: Constant.DetailsScreenTitle.kViewSale, indexpath: indexPath)
            
        default:
            break
        }
    }
    
    func pushViewControllerToDetails(requestStatus: Constant.RequestStatus, requestId: String, strTitle: String, indexpath: IndexPath)
    {
        let request = RequestDetailsVC(nibName:"RequestDetailsVC",bundle:nil) as RequestDetailsVC
        request.strTitle = strTitle
        request.requestStatus = requestStatus
        request.requestID     = requestId
        request.requestIndexPath = indexpath
        request.handlerDeleteEvent = {(deleteIndexPath) in
            self.pageNo = 0
            self.arrRequest.remove(at: deleteIndexPath.row)
            self.tblRequest.deleteRows(at: [indexpath], with: .fade)
            self.getMyRequest(showLoader: false)
        };
        let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
        navigationController.pushViewController(request, animated: true)
    }
    
}


