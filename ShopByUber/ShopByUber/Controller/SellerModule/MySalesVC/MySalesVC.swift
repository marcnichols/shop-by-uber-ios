//
//  MySalesVC.swift
//  ShopByUber
//
//  Created by Chirag Kalsariya on 7/14/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class MySalesVC: BaseView {
    
    //MARK:- IBOutlet
    @IBOutlet weak var tblSales: UITableView!
    @IBOutlet var lblNoDataFound: UILabel!
    @IBOutlet var vw_Progress: UIActivityIndicatorView!
    
    //MARK:- Variable
    var arrMySales = [MySales]()
    var recordCount = 0
    var pageNo = 0
    var numOfRecords = 10
    var isRefreshing : Bool = false
    
    //MARK:- view Lifcycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialConfig()
        
        // Do any additional setup after loading the view.
         self.automaticallyAdjustsScrollViewInsets = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.arrMySales.removeAll()
        recordCount = 0
        self.isRefreshing = false
        self.pageNo = 0
        if self.arrMySales.count == 0{
            
            self.setPagination()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    
    //MARK: Private Methods
    func initialConfig()
    {
        tblSales.register(UINib.init(nibName: "SellerSalesCell", bundle: nil), forCellReuseIdentifier: "SellerSalesCell")
    }
    
    func setPagination() {
        self.getMyServices(showLoader: true)

        tblSales.es.addInfiniteScrolling {
            self.refreshRecord()
        }
        
        tblSales.es.addPullToRefresh {
            
            if !(!self.isRefreshing) {
                self.arrMySales.removeAll()
                self.tblSales.reloadData()
                self.pageNo = 0
                self.refreshRecord()
            }
            else {
                self.tblSales.es.stopPullToRefresh()
            }            
        }
    }
    
    func refreshRecord()
    {
        if self.isRefreshing == false
        {
            if self.pageNo == 0
            {
                self.getMyServices(showLoader: self.arrMySales.count==0 ?true :false)
            }
            else
            {
                self.getMyServices(showLoader: false)
            }
        }
    }
    
    //MARK: Call API
    func getMyServices(showLoader:Bool)
    {

        pageNo += 1
        self.isRefreshing = true
        MySales.getMySales(pageNo: pageNo, numOfRecords: numOfRecords, isShowloader: showLoader, success: { (services,recordcnt) in
            
            if self.pageNo == 1
            {
                self.arrMySales.removeAll()
            }
            if services.count == 0
            {
                self.pageNo -= 1
            }
            else
            {
                if services.count < self.numOfRecords
                {
                    self.pageNo -= 1
                }
                
                if self.arrMySales.count == 0
                {
                    self.arrMySales.append(contentsOf: services)
                }
                else
                {
                    let responsRecord = services as NSArray
                    //let tempLocal = self.arrRequest as NSArray
                    responsRecord.enumerateObjects({ (objF, idxF, nil) in
                        let objSale = objF as! MySales
                        
                        if self.arrMySales.contains(where: {$0.requestId == objSale.requestId}) {
                            //
                            print("it exists, do something")
                        } else {
                            //
                            print("item could not be found")
                            self.arrMySales.append(objSale)
                        }
                        
                    })
                }
            }
            
            self.stopLoading()
            
        }) {
            self.pageNo -= 1
            self.stopLoading()
            
        }
        
    }
    
    func stopLoading()
    {
        self.isRefreshing = false
        self.tblSales.es.stopPullToRefresh()
        self.tblSales.es.stopLoadingMore()
        self.tblSales.reloadData()
        self.lblNoDataFound.isHidden = arrMySales.count==0 ?false :true
    }
    //MARK: IBAction

    
    func showProgressView(){
        self.vw_Progress.startAnimating()
    }
    
    func hideProgressView(){
        self.vw_Progress.stopAnimating()
    }

}

extension MySalesVC : UITableViewDelegate,UITableViewDataSource
{
    //MARK: - Tableview Method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrMySales.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Utilities.isDeviceiPad() ? 92.0 : 72.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SellerSalesCell", for: indexPath) as? SellerSalesCell
        let serviceData = self.arrMySales[indexPath.row]
        
        if serviceData.serviceImage != nil{
            cell?.imgService.sd_setImage(with: URL.init(string: serviceData.serviceImage!), placeholderImage: UIImage(named:Constant.PlaceHolder.kPhoto), options: SDWebImageOptions.retryFailed, completed: nil)
        }else{
            cell?.imgService.image = UIImage.init(named: Constant.PlaceHolder.kPhoto)
        }
        
        cell?.lblServiceTitle.text = serviceData.screenName
        cell?.lblPrice.text = "$" + serviceData.flatFee
        cell?.lblUserName.text = serviceData.bookedBy
        if serviceData.status == 5
        {
            cell?.lblStatus.text = "Completed"
            cell?.lblStatus.textColor = UIColor.darkGray
        }
        else  if serviceData.status == 6
        {
            cell?.lblStatus.text = "Cancelled"
            cell?.lblStatus.textColor = Constant.Color.kCancelStatus
        }
        else
        {
            cell?.lblStatus.text = Constant.StatusText.kWaitingForPayment
            cell?.lblStatus.textColor = UIColor.darkGray
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let data = self.arrMySales[indexPath.row]
        
        
        switch data.status! {
        case 0,1:
            self.pushViewControllerToDetails(requestId: data.requestId, strTitle: Constant.DetailsScreenTitle.kViewRequest, indexpath: indexPath)
        case 5,6:
            self.pushViewControllerToDetails(requestId: data.requestId, strTitle: Constant.DetailsScreenTitle.kViewSale, indexpath: indexPath)
        case 2,3,4:
            self.pushViewControllerToDetails(requestId: data.requestId, strTitle: Constant.DetailsScreenTitle.kViewAppointment, indexpath: indexPath)
        default:
            break
        }
    }
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        if !isRefreshing
//        {
//            if indexPath.row > self.arrMySales.count - 2 && self.recordCount > self.arrMySales.count{
//                
//                let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
//                spinner.startAnimating()
//                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
//                
//                self.tblSales.tableFooterView = spinner
//                self.tblSales.tableFooterView?.isHidden = false
//                refreshRecord()
//            }
//        }
//        
//    }
    
    func pushViewControllerToDetails(requestId: String, strTitle: String, indexpath: IndexPath)
    {
        let request = RequestDetailsVC(nibName:"RequestDetailsVC",bundle:nil) as RequestDetailsVC
        request.strTitle = strTitle
        request.requestID     = requestId
        request.requestIndexPath = indexpath
        request.handlerDeleteEvent = {(deleteIndexPath) in
            self.pageNo = 0
            self.getMyServices(showLoader: true)
        };
        let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
        navigationController.pushViewController(request, animated: true)
    }


}

