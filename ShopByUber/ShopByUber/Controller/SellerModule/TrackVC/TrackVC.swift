//
//  TrackVC.swift
//  ShopByUber
//
//  Created by Chirag Kalsariya on 7/15/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit
import MapboxCoreNavigation
import MapboxNavigation
import MapboxDirections
import Mapbox

enum ExampleMode {
    case `default`
    case custom
    case styled
    case multipleWaypoints
}

class TrackVC: BaseView,MGLMapViewDelegate {

    @IBOutlet weak var mbMapView: NavigationMapView!
    @IBOutlet weak var btnStartNavigation: UIButton!
   
    //MARK:- Variable
    var latitude:Double = 0
    var longitude:Double = 0
    let locManager = CLLocationManager()
    var currentLatlong:CLLocation!
    var locationManager:LocationManager!
    var currentMarker:GMSMarker = GMSMarker()
    var currentZoom:Double = 9
    var arrMapData = [MapData]()
    var channleName:String!
    var requestID:String!
    var lastHeading: CLLocationDirection!
    var timer: Timer!
    var isPublish = true
    
    var waypoints: [Waypoint] = []
    var currentRoute: Route?
    var exampleMode: ExampleMode?
    var showCustomUserPuck = false
    
    let byuerLocation = MyCustomPointAnnotation()
    let vendorLocation = MyCustomPointAnnotation()
    var expectedTravelTime : TimeInterval = 0
    
    //MARK:- View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(progressDidChange(_ :)), name: RouteControllerProgressDidChange, object: nil)
        
        automaticallyAdjustsScrollViewInsets = false
        mbMapView.delegate = self
        mbMapView.userTrackingMode = .follow
        mbMapView.logoView.isHidden = true
        mbMapView.attributionButton.isHidden = true

    }
    
    override func viewDidAppear(_ animated: Bool) {
        
       // checkForLocationService()

    }
    override func viewWillAppear(_ animated: Bool) {
        
        DayStyle().apply()
    
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if self.client != nil
        {
            self.client.unsubscribeFromAll()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func checkForLocationService() {
        
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                print("No access")
                DispatchQueue.main.async(execute: {
                    let alert = UIAlertController(title: "", message: "Location Services access is restricted. In order to use tracking, please enable Location Services in the Settigs under Privacy, Location Service.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Setting", style: UIAlertActionStyle.default, handler: { (alert: UIAlertAction!) in
                        print("")
                        //UIApplication.shared.openURL(NSURL(string:UIApplicationOpenSettingsURLString)! as URL)
                        if let url = URL(string: "App-Prefs:root=Privacy&path=LOCATION/com.app.ShopByUber") {
                            UIApplication.shared.openURL(url)
                        }
                    }))
                    alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: { (alert: UIAlertAction!) in
                        
                    }))
                    self.present(alert, animated: true, completion: nil)
                    
                })
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
            }
        } else {
            print("Location services are not enabled")
            DispatchQueue.main.async(execute: {
                
                
                let alert = UIAlertController(title: "", message: "Location Services access is restricted. In order to use tracking, please enable Location Services in the Settigs under Privacy, Location Services.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Setting", style: UIAlertActionStyle.default, handler: { (alert: UIAlertAction!) in
                    print("")
                    
                    if let url = URL(string: "App-Prefs:root=Privacy&path=LOCATION/com.app.ShopByUber") {
                        UIApplication.shared.openURL(url)
                    }
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: { (alert: UIAlertAction!) in
                    
                }))
                self.present(alert, animated: true, completion: nil)
                
            })
        }
    }
    
    //MARK: - MapBox Delegate Method
    // Called when Map load finish
    
    func mapViewDidFinishLoadingMap(_ mapView: MGLMapView) {
        
        self.getLocationData()
        
    }
    
    
    // Helper for requesting a route
//    func requestRoute() {
//
//        guard waypoints.count > 0 else { return }
//
//        if mbMapView.userLocation!.location != nil {
//
//            let userWaypoint = Waypoint(location: mbMapView.userLocation!.location!, heading: mbMapView.userLocation?.heading, name: "user")
//            waypoints.insert(userWaypoint, at: 0)
//
//            let options = NavigationRouteOptions(waypoints: waypoints)
//
//            _ = Directions.shared.calculate(options) { [weak self] (waypoints, routes, error) in
//                guard error == nil else {
//                    print(error!.localizedDescription)
//                    return
//                }
//                guard let route = routes?.first else { return }
//
//                self?.currentRoute = route
//
//                // Open method for adding and updating the route line
//                self?.mbMapView.showRoute(route)
//                self?.mbMapView.showWaypoints(route, legIndex: 0)
//
//                print(route.expectedTravelTime)
//                self?.expectedTravelTime = route.expectedTravelTime
//                print(CalenderUtility.getHourMinSecond(totalSeconds:route.expectedTravelTime))
//
//                self?.mbMapView.removeAnnotations((self?.mbMapView.annotations!)!)
//
//                self?.byuerLocation.coordinate = CLLocationCoordinate2D(latitude: (self?.arrMapData[0].userLatitude)!, longitude: (self?.arrMapData[0].userLongitude)!)
//                self?.byuerLocation.willUseImage = false
//                self?.byuerLocation.isShowTitle = true
//
//                //------------------------
//                self?.vendorLocation.coordinate = CLLocationCoordinate2D(latitude: (self?.arrMapData[0].companyLatitude)!, longitude: (self?.arrMapData[0].companyLongitude)!)
//                self?.vendorLocation.willUseImage = false
//
//                // Fill an array with four point annotations.
//                let myPlaces = [self?.byuerLocation, self?.vendorLocation]
//                //-------------------------
//
//                // Fill an array with four point annotations.
//             //   let myPlaces = [self?.byuerLocation]
//
//                self?.mbMapView.removeAnnotations(myPlaces as! [MGLAnnotation])
//
//                // Add all annotations to the map all at once, instead of individually.
//                self?.mbMapView.addAnnotations(myPlaces as! [MGLAnnotation])
//
//                //self?.mbMapView.showAnnotations((self?.mbMapView.annotations!)!, animated: false)
//            }
//
//        }
//    }
    
    func requestRoute() {
        
        guard waypoints.count > 0 else { return }
        
        
        // Set here seller current point
        
        let myLocation = CLLocation(latitude: self.arrMapData[0].companyLatitude, longitude: self.arrMapData[0].companyLongitude)
        
        let userWaypoint = Waypoint(location: myLocation/*(bMbMapView.userLocation?.location)!*/, heading: mbMapView.userLocation?.heading, name: "user")
        waypoints.insert(userWaypoint, at: 0)
        
        let options = NavigationRouteOptions(waypoints: waypoints)
        
        _ = Directions.shared.calculate(options) { [weak self] (waypoints, routes, error) in
            guard error == nil else {
                print(error!.localizedDescription)
                return
            }
            guard let route = routes?.first else { return }
            
            self?.currentRoute = route
            
            // Open method for adding and updating the route line
            self?.mbMapView.showRoute(route)
            self?.mbMapView.showWaypoints(route, legIndex: 0)
            
            self?.mbMapView.addArrow(route: route, legIndex: 5, stepIndex: 5)
            print(route.expectedTravelTime)
            
            self?.expectedTravelTime = route.expectedTravelTime
            print(CalenderUtility.getHourMinSecond(totalSeconds:route.expectedTravelTime))
            
            
            self?.mbMapView.removeAnnotations((self?.mbMapView.annotations!)!)
            
            self?.byuerLocation.coordinate = CLLocationCoordinate2D(latitude: (self?.arrMapData[0].userLatitude)!, longitude: (self?.arrMapData[0].userLongitude)!)
            self?.byuerLocation.willUseImage = false
            self?.byuerLocation.isShowTitle = true
            //self?.byuerLocation.title = CalenderUtility.getHourMinSecond(totalSeconds: (self?.expectedTravelTime)!)
            
            
            self?.vendorLocation.coordinate = CLLocationCoordinate2D(latitude: (self?.arrMapData[0].companyLatitude)!, longitude: (self?.arrMapData[0].companyLongitude)!)
            self?.vendorLocation.willUseImage = false
            
            // Fill an array with four point annotations.
            let myPlaces = [self?.byuerLocation, self?.vendorLocation]
            
            self?.mbMapView.removeAnnotations(myPlaces as! [MGLAnnotation])
            
            // Add all annotations to the map all at once, instead of individually.
            self?.mbMapView.addAnnotations(myPlaces as! [MGLAnnotation])
            
            self?.mbMapView.showAnnotations((self?.mbMapView.annotations!)!, animated: false)
            self?.mbMapView.selectAnnotation((self?.byuerLocation)!, animated: true)
        }
        
    }
    
    // Notifications sent on all location updates
    func progressDidChange(_ notification: NSNotification) {
        
        let routeProgress = notification.userInfo![RouteControllerProgressDidChangeNotificationProgressKey] as! RouteProgress
        let location = notification.userInfo![RouteControllerProgressDidChangeNotificationLocationKey] as! CLLocation
        print(location)
        self.publishLocation(location: location, bearing: CLLocationDirection(location.course), channel: self.channleName)

    }

    //MARK: - Private Mathods
    func initialConfig()
    {
        self.pubnubConfiguration(withCannel: self.arrMapData[0].pubChannelName)
 
        if let annotation = mbMapView.annotations?.last, waypoints.count > 2 {
            mbMapView.removeAnnotation(annotation)
        }
        
        if waypoints.count > 1 {
            waypoints = Array(waypoints.suffix(1))
        }

        
        let coordinates : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: self.arrMapData[0].userLatitude, longitude: self.arrMapData[0].userLongitude)
        
         let waypoint = Waypoint(coordinate: coordinates)
         waypoint.coordinateAccuracy = -1
         waypoints.append(waypoint)
         
         requestRoute()
        
    }
    
    //MARK: - API Call
    func getLocationData()
    {
        var dict                = [String : Any]()
        dict["sessionId"]       = DataModel.sharedInstance.onFetchSessionId()
        dict["userId"]          = DataModel.sharedInstance.onFetchUserId()
        dict["requestIds"]      = [self.requestID]
        APIManager.callAPI(url: Constant.serverAPI.URL.kGetPubnubData, inputParams: dict, success: { (dict) in
            KVNProgress.dismiss()
            
            if dict?["Result"] as? Bool == true {
                let data = dict?["Data"] as? [String:Any]
                if data?.count != 0
                {
                    let datas = data?["mapData"] as? NSArray
                    if datas != nil
                    {
                        for i in datas!
                        {
                            let data = MapData.init(fromDictionary: i as! [String : Any])
                            self.arrMapData.append(data)
                        }
                        self.initialConfig()
                    }
                }
            }
            else
            {
                Utilities.showAlertView(title: "", message: dict?["Message"] as? String)
            }
        }) { error in
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
        }
    }
    
    func reachedBuyersLocation() {
        
        var dictParam=[String:Any]()
        dictParam["userId"] = DataModel.sharedInstance.onFetchUserId()
        dictParam["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
        dictParam["requestId"] = requestID
        dictParam["pubChannelName"] = self.arrMapData[0].pubChannelName
        dictParam["status"] = 4
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kChangeRequestStatus, inputParams: dictParam, success: { (response) in
            
            KVNProgress.dismiss()
            if response?["Result"] as? Bool == true {
                
            }
        }) { (error) in
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
        }
    }
    
    //MARK:  - IBAction Methods
    @IBAction func btnReachedClick(_ sender: Any) {
        
        if btnStartNavigation.titleLabel?.text == "START NAVIGATION" {
            
            checkForLocationService()
            guard let route = currentRoute else { return }
            exampleMode = .default
            let navigationViewController = NavigationViewController(for: route, locationManager: mbLocationManager() /*NavigationLocationManager()*/)   //mbLocationManager:- Auto route update
            navigationViewController.showsReportFeedback = false
            navigationViewController.navigationDelegate = self
            navigationViewController.automaticallyAdjustsStyleForTimeOfDay = true
            navigationViewController.mapView?.attributionButton.isHidden = true
            navigationViewController.mapView?.logoView.isHidden = true
            navigationViewController.mapView?.logoView.alpha = 0.0
            present(navigationViewController, animated: true, completion: nil)
        }else{
            
            var dictParam=[String:Any]()
            dictParam["userId"] = DataModel.sharedInstance.onFetchUserId()
            dictParam["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
            dictParam["requestId"] = requestID
            dictParam["pubChannelName"] = ""
            dictParam["status"] = 4
            
            APIManager.callAPI(url: Constant.serverAPI.URL.kChangeRequestStatus, inputParams: dictParam, success: { (response) in
                
                KVNProgress.dismiss()
                if response?["Result"] as? Bool == true {
                    self.navigationController?.popViewController(animated: true)
                }
            }) { (error) in
                KVNProgress.dismiss()
                Utilities.showAlertView(title: "", message: error)
            }
        }
    }
    
    func mbLocationManager() -> NavigationLocationManager {
        guard let route = currentRoute else { return NavigationLocationManager() }
        return SimulatedLocationManager(route: route)
    }
    
    // This delegate method is where you tell the map to load a view for a specific annotation based on the willUseImage property of the custom subclass.
    func mapView(_ mapView: MGLMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {
        
        if let castAnnotation = annotation as? MyCustomPointAnnotation {
            if (castAnnotation.willUseImage) {
                return nil;
            }
        }
        
        let annotationView = MGLAnnotationView.init(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        /*
        //This one is for reuse annotaiton
        // Assign a reuse identifier to be used by both of the annotation views, taking advantage of their similarities.
        let reuseIdentifier = "reusableDotView"
        
        // For better performance, always try to reuse existing annotations.
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier)
        
        // If there’s no reusable annotation view available, initialize a new one.
        if annotationView == nil {
            
            annotationView = MGLAnnotationView(reuseIdentifier: reuseIdentifier)
            annotationView?.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        }
        */
        if let castAnnotation = annotation as? MyCustomPointAnnotation {
            
            if (castAnnotation.isShowTitle) {
                
                annotationView.layer.cornerRadius = 0
                annotationView.layer.borderWidth = 0.0
                annotationView.layer.borderColor = UIColor.clear.cgColor
                annotationView.backgroundColor = UIColor.clear
                
                self.lblPinDuration.layer.cornerRadius = self.lblPinDuration.frame.size.height / 2
                
                let value = CalenderUtility.getHourMinSecond(totalSeconds: expectedTravelTime)
                let temp = (value as NSString).uppercased
                let durations = temp.components(separatedBy: " ")
                
                
                var formated = Utilities.decorateText(attributedString: NSMutableAttributedString(attributedString: NSAttributedString(string: temp)), decorateString: durations[0], decorate: [NSFontAttributeName:UIFont.systemFontSemiBold(with: 12)])
                
                formated = Utilities.decorateText(attributedString: NSMutableAttributedString(attributedString: NSAttributedString(string: temp)), decorateString: durations[1], decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: 10)])
                
                
                self.lblPinDuration.attributedText = formated
                self.lblPinDuration.textAlignment = .center
                annotationView.addSubview(self.vwPinDuration)
            }else {
                
                annotationView.layer.cornerRadius = 0
                annotationView.layer.borderWidth = 0.0
                annotationView.layer.borderColor = UIColor.clear.cgColor
                annotationView.backgroundColor = UIColor.clear
                
                let car = UIImageView(frame: CGRect(x: 12.5, y: 0, width: 25, height: 50))
                car.image = UIImage(named: "car")
                
                annotationView.addSubview(car)
            }
        }
        return annotationView
    }
}

extension TrackVC
{
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        
    }
}

extension TrackVC: NavigationViewControllerDelegate {
    
    func navigationViewController(_ navigationViewController: NavigationViewController, didArriveAt waypoint: Waypoint) {
        
        // When the user arrives, present a view controller that prompts the user to continue to their next destination
        // This typ of screen could show information about a destination, pickup/dropoff confirmation, instructions upon arrival, etc.
       
//        let coordinateUser = CLLocation(latitude: arrMapData[0].userLatitude, longitude: arrMapData[0].userLongitude)
//        let coordinateComp = CLLocation(latitude: waypoint.coordinate.latitude, longitude: waypoint.coordinate.longitude)
//        let distanceInMeters = coordinateUser.distance(from: coordinateComp)
//
//        if(distanceInMeters <= 0.0310686)
//        {
//            // under 50 meter
//        }
        
        self.reachedBuyersLocation()  //Send Push Notification when vendor reaches location
        btnStartNavigation.setTitle("REACHED & START SERVICE",for: .normal)
        self.dismiss(animated: true, completion: nil)
    }
}

class CustomAnnotationView: MGLUserLocationAnnotationView {
    override func layoutSubviews() {
        super.layoutSubviews()
        
        // Force the annotation view to maintain a constant size when the map is tilted.
        scalesWithViewingDistance = false
        
        // Use CALayer’s corner radius to turn this view into a circle.
        layer.cornerRadius = frame.width / 2
        layer.borderWidth = 2
        layer.borderColor = UIColor.white.cgColor
    }
}



