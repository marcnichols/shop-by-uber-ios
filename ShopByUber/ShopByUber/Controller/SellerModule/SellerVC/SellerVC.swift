//
//  SellerVC.swift
//  ShopByUber
//
//  Created by Administrator on 6/3/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class SellerVC: BaseView, reloadServiceTableDelegate {
    func reloadServiceTable() {
        self.arr_MyServices.removeAll()
        self.tblServices.reloadData()
    }
    

    //MARK: IBOutlets
    
    @IBOutlet var lblNoDataFound: UILabel!
    @IBOutlet var vwNavigationBar: UIView!
    @IBOutlet var tblServices: UITableView!
    @IBOutlet var vw_Progress: UIActivityIndicatorView!
    @IBOutlet var btnAddService: UIButton!

    //MARK: Variables
    var arr_MyServices = [MyServices]()
    var recordCount = 0
    var pageNo = 0
    var numOfRecords = 10
    var isRefreshing : Bool = false
    var showAddService = false
    var passedRequestID = String()
    
    var passedCatID = String()
    var passedSubCatID = String()
    var passedSubCatName = String()
    var passedServiceName = String()
    var passedServiceImage = String()
    var passedCatName = String()
    var isFromMenu = false
    
    //MARK: View LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if DataModel.sharedInstance.onFetchVersionUpdate() {
            appDeletgate.VersionChecker()
            DataModel.sharedInstance.CheckVersionUpdate(checkVersionUpdate: false)
        }
        self.initialConfig()
        self.automaticallyAdjustsScrollViewInsets = false
        
        if showAddService {
            let addServiceVC = AddServiceVC(nibName: "AddServiceVC", bundle: nil) as AddServiceVC
            if isFromMenu == false {
            addServiceVC.requestID = self.passedRequestID
            addServiceVC.isFromWantedService = true
            }
            _ = self.navigationController?.pushViewController(addServiceVC, animated: false)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateCount), name: NSNotification.Name(rawValue: "updateNotificationCount"), object: nil)
     
        self.recordCount = 0
        self.pageNo = 0
        self.isRefreshing = false
        self.refreshRecord()
    }
    
    //MARK: Private Methods
    
    func initialConfig(){
        
        Singleton.sharedManager.unreadHandler={(success) in
            if self.btnNotify != nil
            {
                if Singleton.sharedManager.unreadNotificationCount > 0
                {
                    self.btnNotify.badgeValue = Singleton.sharedManager.unreadNotificationCount.description
                }
                else
                {
                    self.btnNotify.badgeValue = ""
                }
                
            }
        }
        Utilities.applyShadowEffect(view: [self.vwNavigationBar])
        
        self.tblServices.register(UINib(nibName: "SellerCell", bundle: nil), forCellReuseIdentifier: "SellerCell")
        
        //Show Bubble
        if DataModel.sharedInstance.onFetchPromoCode() != "" {
            
            appDeletgate.ShowBubble(promoCode: DataModel.sharedInstance.onFetchPromoCode())
        }
    }
    
    func updateCount() {
        
        if self.btnNotify != nil
        {
            if Singleton.sharedManager.unreadNotificationCount != 0
            {
                self.btnNotify.badgeValue = Singleton.sharedManager.unreadNotificationCount.description
            }
        }
    }
    
    func setPagination() {
       
        
        tblServices.es.addPullToRefresh {
            
            if !(!self.isRefreshing) {
                self.pageNo = 0
                self.arr_MyServices.removeAll()
                self.tblServices.reloadData()
                self.refreshRecord()
            }
            else {
                self.tblServices.es.stopPullToRefresh()
            }
        }
    }
    
    func refreshRecord()
    {
        if self.isRefreshing == false
        {
            if self.pageNo == 0
            {
                self.getMyServices(showLoader: self.arr_MyServices.count==0 ?true :false)
            }
            else
            {
                self.getMyServices(showLoader: false)
            }
        }
    }
    
    //MARK: Call API
    func getMyServices(showLoader:Bool)
    {
        if showLoader
        {
            self.showProgressView()
        }
        self.pageNo += 1
        MyServices.callGetMyServicesAPI(pageNo: pageNo, numOfRecords: numOfRecords, success: { (services,recordcnt) in
            
            self.hideProgressView()
            
            self.recordCount = recordcnt
            self.tblServices.tableFooterView?.isHidden = true
            self.tblServices.tableFooterView?.frame.size = .zero
        
                
                self.isRefreshing = false
                if self.pageNo == 1
                {
                    self.arr_MyServices.removeAll()
                }
                if services.count == 0
                {
                    self.pageNo -= 1
                }
                else
                {
                    if services.count < self.numOfRecords
                    {
                        self.pageNo -= 1
                    }
                    
                    if self.arr_MyServices.count == 0
                    {
                        self.arr_MyServices.append(contentsOf: services)
                    }
                    else
                    {
                        let responsRecord = services as NSArray
                        //let tempLocal = self.arrRequest as NSArray
                        responsRecord.enumerateObjects({ (objF, idxF, nil) in
                            let objService = objF as! MyServices
                            
                            if self.arr_MyServices.contains(where: {$0.id == objService.id}) {
                                //
                                print("it exists, do something")
                            } else {
                                //
                                print("item could not be found")
                                self.arr_MyServices.append(objService)
                            }
                            
                        })
                    }
                }

            self.stopLoading()
            
        }) {
            
           self.stopLoading()
        }
    }
    
 
    
    func stopLoading()
    {
        self.hideProgressView()
        self.tblServices.es.stopPullToRefresh()
        self.tblServices.es.stopLoadingMore()
        self.tblServices.reloadData()
        self.lblNoDataFound.isHidden = arr_MyServices.count==0 ?false :true
    }

    
    
    @IBAction func btnAddService_Clicked(_ sender: UIButton) {
        
        let addServiceVC = AddServiceVC(nibName: "AddServiceVC", bundle: nil) as AddServiceVC
        _ = self.navigationController?.pushViewController(addServiceVC, animated: true)
        
    }
    

    //MARK: Custom Methods
    
    func showProgressView(){
        self.vw_Progress.isHidden = false
        self.vw_Progress.startAnimating()
    }
    
    func hideProgressView(){
        self.vw_Progress.isHidden = true
        self.vw_Progress.stopAnimating()
    }
    
    
}

extension SellerVC : UITableViewDelegate,UITableViewDataSource
{
    
    
    //MARK: TableView Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arr_MyServices.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SellerCell", for: indexPath) as? SellerCell
        
        Utilities.applyShadowEffect(view: [cell!.vwBG])
        
        let data = self.arr_MyServices[indexPath.row]
        
        cell?.lblTitle.text = data.screenName
        cell?.lblAmount.text = "$" + data.flatFee!
        
        if data.serviceImage != nil{
            cell?.imgService.sd_setImage(with: URL.init(string: data.serviceImage!), placeholderImage: UIImage(named:Constant.PlaceHolder.kPhoto), options: SDWebImageOptions.retryFailed, completed: nil)
        }else{
            cell?.imgService.image = UIImage.init(named: Constant.PlaceHolder.kPhoto)
        }
        
        if data.isAvailable != nil{
            
            if data.isAvailable! {
                
                cell?.status_Switch.isOn = true
                cell?.lblstatus.text = "Available now"
                cell?.lblstatus.textColor = UIColor.black
                
            }else{
                
                cell?.status_Switch.isOn = false
                cell?.lblstatus.text = "Available by appointment"
                cell?.lblstatus.textColor = Constant.Color.sellerUnavailableColor
                
            }
            
        }else{
            
            cell?.status_Switch.isOn = false
            cell?.lblstatus.text = "Available by appointment"
            cell?.lblstatus.textColor = Constant.Color.sellerUnavailableColor
        }
        
        cell?.index = indexPath.row
        cell?.delegate = self
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if !isRefreshing
        {
            if indexPath.row > self.arr_MyServices.count - 2 && self.recordCount > self.arr_MyServices.count{
                
                let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                
                self.tblServices.tableFooterView = spinner
                self.tblServices.tableFooterView?.isHidden = false
                refreshRecord()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.reloadData()
//        if !arr_MyServices[indexPath.row].editable!
//        {
//            Utilities.showAlertView(title: "", message: Constant.ValidationMessage.kServiceEditWarring)
//            return
//        }
        
        let editService = AddServiceVC.init(nibName: "AddServiceVC", bundle: nil)
        editService.editServiceID = arr_MyServices[indexPath.row].id!
        editService.isFromEditService = true
        editService.isEditable = arr_MyServices[indexPath.row].editable!
        editService.delegate = self
        self.navigationController?.pushViewController(editService, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Utilities.isDeviceiPad() ? 100.0 : 77.0
    }

}

extension SellerVC : SellerDelegate
{
    //MARK: Seller Delegates
    
    func availabilitySwitchTap(sender: UISwitch, index: Int) {
        
        let isAvailable = sender.isOn ? true : false
        let data = self.arr_MyServices[index]
        
        MyServices.callSetServiceAvailabilityAPI(serviceId: data.id, isAvailable: isAvailable, success: {
            
            //Success
            
            data.isAvailable = isAvailable
            
            self.tblServices.reloadData()
            
            
        }) {
            
            data.isAvailable = !isAvailable
            self.tblServices.reloadData()
            
        }
        
        
    }

}
