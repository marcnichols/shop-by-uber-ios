//
//  MyAppointmentsVC.swift
//  ShopByUber
//
//  Created by Nirav Shah on 6/28/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class MyAppointmentsVC: BaseView {

    //MARK: IBOutlet
    @IBOutlet var vwContent: UIView!
    @IBOutlet var vwTop: UIView!

    
    
    //MARK:- Variables
    var segment: WBSegmentControl!
    var viewPages = UIView()
    var viewLabel = UILabel()
    var pagesController: UIPageViewController!
    var pages: [UIViewController] = []

    
    //MARK:- View Lifecycles
    
    override func viewDidLoad() {
        super.viewDidLoad()
        segment.selectedIndex = 0
        self.initialConfig()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func loadView() {
        super.loadView()
        
        self.setupSegmentView()
         segment.selectedIndex = 0
    }
    
    //MARK:- Private Methods
    func initialConfig(){
        
       
    }
    
    func setupSegmentView(){
        
        initSegmentControl()
        initPagesController()
        
        view.addSubview(segment)
        view.addSubview(viewPages)
        viewPages.addSubview(pagesController.view)
        view.addSubview(viewLabel)
        
        segment.translatesAutoresizingMaskIntoConstraints = false
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[segment]|", options: .alignAllLeading, metrics: nil, views: ["segment": segment]))
        view.addConstraint(NSLayoutConstraint(item: segment, attribute: .top, relatedBy: .equal, toItem: self.vwTop, attribute: .bottom, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: segment, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 0, constant: Utilities.isDeviceiPad() ? 60.0 : 50.0))
        
        viewPages.gestureRecognizers = pagesController.gestureRecognizers
        viewPages.translatesAutoresizingMaskIntoConstraints = false
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[viewPages]|", options: .alignAllLeading, metrics: nil, views: ["viewPages": viewPages]))
        view.addConstraint(NSLayoutConstraint(item: viewPages, attribute: .top, relatedBy: .equal, toItem: segment, attribute: .bottom, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: viewPages, attribute: .bottom, relatedBy: .equal, toItem: viewLabel, attribute: .top, multiplier: 1, constant: 0))
        
        pagesController.view.translatesAutoresizingMaskIntoConstraints = false
        viewPages.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[pagesView]|", options: .alignAllLeading, metrics: nil, views: ["pagesView": pagesController.view]))
        viewPages.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[pagesView]|", options: .alignAllFirstBaseline, metrics: nil, views: ["pagesView": pagesController.view]))
        
        viewLabel.translatesAutoresizingMaskIntoConstraints = false
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[viewLabel]|", options: .alignAllLeading, metrics: nil, views: ["viewLabel": viewLabel]))
        view.addConstraint(NSLayoutConstraint(item: viewLabel, attribute: .top, relatedBy: .equal, toItem: self.bottomLayoutGuide, attribute: .top, multiplier: 1, constant: 37))
        view.addConstraint(NSLayoutConstraint(item: viewLabel, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 0, constant: 50))
        
        viewLabel.textAlignment = .center
        viewLabel.textColor = UIColor.black
        // viewLabel.backgroundColor = UIColor.blue
    }
    
    
    
    
    
    
    func initSegmentControl() {
        segment = WBSegmentControl()
        segment.segments = [
            TextSegment(text: "Recent"),
            TextSegment(text: "Upcoming"),
            /*TextSegment(text: "Completed"),*/
        ]
        
        segment.backgroundColor = Constant.Color.kSegmentBackgroundColor
        segment.style = .cover
        segment.segmentTextBold = false
        segment.segmentTextFontSize = Utilities.isDeviceiPad() ? 18.0 : 15.0
        segment.cover_color = UIColor(red: 202/255.0, green: 235/255.0, blue: 215/255.0, alpha: 1)
        segment.delegate = self
        
        
    
    }
    
    func initPagesController() {
        pagesController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pagesController.dataSource = self
        pagesController.delegate = self
        
        let vc1 = RecentAppointmentVC()
        let vc2 = UpcomingAppointmentVC()
       // let vc2 = RecentAppointmentVC()
        self.pages = [vc1,vc2]
        
    }
    
}

extension MyAppointmentsVC: WBSegmentControlDelegate {
    func segmentControl(_ segmentControl: WBSegmentControl, selectIndex newIndex: Int, oldIndex: Int) {
        let targetPages = [pages[newIndex]]
        let direction = ((newIndex > oldIndex) ? UIPageViewControllerNavigationDirection.forward : UIPageViewControllerNavigationDirection.reverse)
        pagesController.setViewControllers(targetPages, direction: direction, animated: true, completion: nil)
        
        if let selectedSegment = segmentControl.selectedSegment as? TextSegment {
            viewLabel.text = selectedSegment.otherAttr
        }
    }
}

extension MyAppointmentsVC: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let index = pages.index(of: viewController)!
        return index > 0 ? pages[index - 1] : nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let index = pages.index(of: viewController)!
        return index < pages.count - 1 ? pages[index + 1] : nil
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return pages.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
}

extension MyAppointmentsVC: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed == false {
            guard let targetPage = previousViewControllers.first else {
                return
            }
            guard let targetIndex = pages.index(of: targetPage) else {
                return
            }
            segment.selectedIndex = targetIndex
            pageViewController.setViewControllers(previousViewControllers, direction: .reverse, animated: true, completion: nil)
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        guard let targetPage = pendingViewControllers.first else {
            return
        }
        guard let targetIndex = pages.index(of: targetPage) else {
            return
        }
        segment.selectedIndex = targetIndex
    }
}
