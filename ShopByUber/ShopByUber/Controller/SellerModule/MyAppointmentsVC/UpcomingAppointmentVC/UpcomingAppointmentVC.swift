//
//  UpcomingAppointmentVC.swift
//  ShopByUber
//
//  Created by Nirav Shah on 6/29/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class UpcomingAppointmentVC: UIViewController {

    @IBOutlet weak var tblAppointment: UITableView!
    @IBOutlet var lblNoDataFound: UILabel!
    
    var arrAppointment = [Appointment]()
    var pageNo:Int = 0
    var isRefreshing:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialConfig()
        
        
        // Do any additional setup after loading the view.
    }
    
    //MARK:- View Lifecycles
    override func viewWillAppear(_ animated: Bool) {
        if arrAppointment.count == 0
        {
            self.setPagination()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Private Methods
    
    func initialConfig(){
        
        tblAppointment.register(UINib.init(nibName: "ServiceCell", bundle: nil), forCellReuseIdentifier: "ServiceCell")
    }
    
    func setPagination() {
        self.getAppointment(showLoader: true)
        
        tblAppointment.es.addInfiniteScrolling {
            if self.isRefreshing == false
            {
                if self.pageNo == 0
                {
                    self.getAppointment(showLoader: self.arrAppointment.count==0 ?true :false)
                }
                else
                {
                    self.getAppointment(showLoader: false)
                }
            }
        }
    }
    
    func getAppointment(showLoader:Bool)
    {
        self.isRefreshing = false
        pageNo += 1
        Appointment.getAppointment(appointTypeURL: Constant.serverAPI.URL.kGetUpcomingAppointments, pageNo: pageNo, numOfRecords: 10, isShowloader: showLoader, success: { (appoints) in
            
            self.isRefreshing = false
            if self.pageNo == 1
            {
                self.arrAppointment.removeAll()
            }
            if appoints.count == 0
            {
                self.pageNo -= 1
            }
            else
            {
                if appoints.count < 10
                {
                    self.pageNo -= 1
                }
                
                if self.arrAppointment.count == 0
                {
                    self.arrAppointment.append(contentsOf: appoints)
                }
                else
                {
                    let responsRecord = appoints as NSArray
                    let tempLocal = self.arrAppointment as NSArray
                    responsRecord.enumerateObjects({ (objF, idxF, nil) in
                        let objRequests = objF as! Appointment
                        
                        tempLocal.enumerateObjects({ (objLoc, idxLoc, nil) in
                            let tempObjLoc = objLoc as! Appointment
                            
                            if objRequests.requestId != tempObjLoc.requestId
                            {
                                self.arrAppointment.append(objRequests)
                            }
                        })
                        
                    })
                }
            }
            
            self.stopLoading()
        }) {
            self.isRefreshing = false
            self.pageNo -= 1
            self.stopLoading()
        }
    }
    
    func stopLoading()
    {
        self.tblAppointment.es.stopPullToRefresh()
        self.tblAppointment.es.stopLoadingMore()
        self.tblAppointment.reloadData()
        self.lblNoDataFound.isHidden = arrAppointment.count==0 ?false :true
    }
    
    
}

extension UpcomingAppointmentVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Utilities.isDeviceiPad() ? 92.0 : 72.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAppointment.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceCell", for: indexPath) as! ServiceCell
        
        Utilities.applyShadowEffect(view: [cell.vwBG])
        cell.lblTitle.text = arrAppointment[indexPath.row].screenName
        cell.lblDescription.text = arrAppointment[indexPath.row].duration
        cell.serviceImage.sd_setImage(with: URL(string: arrAppointment[indexPath.row].serviceImage), placeholderImage: UIImage(named: ""))
        cell.backgroundColor = UIColor.clear
        cell.lblDescription.textColor = Constant.Color.kGreen

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let data = self.arrAppointment[indexPath.row]
        
        
        switch data.status! {
        case 0:
            self.pushViewControllerToDetails(requestStatus: Constant.RequestStatus.Requested, requestId: data.requestId, strTitle: Constant.DetailsScreenTitle.kViewRequest, indexpath: indexPath)
        case 1:
            self.pushViewControllerToDetails(requestStatus: Constant.RequestStatus.Confirm, requestId: data.requestId, strTitle: Constant.DetailsScreenTitle.kViewSale, indexpath: indexPath)
        case 2:
            self.pushViewControllerToDetails(requestStatus: Constant.RequestStatus.PayDone, requestId: data.requestId, strTitle: Constant.DetailsScreenTitle.kViewAppointment, indexpath: indexPath)
        case 3:
            self.pushViewControllerToDetails(requestStatus: Constant.RequestStatus.OnRouteToClient, requestId: data.requestId, strTitle: Constant.DetailsScreenTitle.kViewAppointment, indexpath: indexPath)
            
        case 4:
            self.pushViewControllerToDetails(requestStatus: Constant.RequestStatus.StartService, requestId: data.requestId, strTitle: Constant.DetailsScreenTitle.kViewAppointment, indexpath: indexPath)
            
        case 5:
            self.pushViewControllerToDetails(requestStatus: Constant.RequestStatus.Completed, requestId: data.requestId, strTitle: Constant.DetailsScreenTitle.kViewSale, indexpath: indexPath)
        case 6:
            self.pushViewControllerToDetails(requestStatus: Constant.RequestStatus.Cancelled, requestId: data.requestId, strTitle: Constant.DetailsScreenTitle.kViewSale, indexpath: indexPath)
            
        default:
            break
        }
    }
    
    func pushViewControllerToDetails(requestStatus: Constant.RequestStatus, requestId: String, strTitle: String, indexpath: IndexPath)
    {
        let request = RequestDetailsVC(nibName:"RequestDetailsVC",bundle:nil) as RequestDetailsVC
        request.strTitle = strTitle
        request.requestStatus = requestStatus
        request.requestID     = requestId
        request.requestIndexPath = indexpath
        request.handlerDeleteEvent = {(deleteIndexPath) in
            self.pageNo = 0
            self.arrAppointment.remove(at: deleteIndexPath.row)
            self.tblAppointment.deleteRows(at: [indexpath], with: .fade)
            self.getAppointment(showLoader: true)
        };
        let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
        navigationController.pushViewController(request, animated: true)
    }

}

