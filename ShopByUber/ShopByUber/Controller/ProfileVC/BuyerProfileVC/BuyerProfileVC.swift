//
//  BuyerProfileVC.swift
//  ShopByUber
//
//  Created by Administrator on 7/6/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class BuyerProfileVC: BaseView {
    
    //MARK: - IBOutlet
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var imgUserProfile: UIImageView!
    @IBOutlet var vwNavigationBar: UIView!
    @IBOutlet weak var tblRecentFavorites: UITableView!
    @IBOutlet weak var lblNoDataFound: UILabel!

    //MARK: - Variables
    var arr_RecentServices = [RecentServiceModel]()
    var arr_CompanyServiceList = [CompanyServiceList]()
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.initialConfig()
        self.callResentListData()
                
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        Utilities.decorateView(imgUserProfile.layer, cornerRadius: self.imgUserProfile.frame.height/2, borderWidth: 5, borderColor: Constant.Color.viewShadowColor)
    }
    

    //MARK: Private Methods
        func initialConfig(){
        
        Utilities.applyShadowEffect(view: [vwNavigationBar])
        self.imgUserProfile.clipsToBounds = true
       
        self.tblRecentFavorites.register(UINib(nibName: "MyFavoiuriteServiceCell", bundle: nil), forCellReuseIdentifier: "MyFavoiuriteServiceCell")
        self.tblRecentFavorites.register(UINib(nibName: "MassageTherapyCell", bundle: nil), forCellReuseIdentifier: "MassageTherapyCell")
        self.callGetUserProfileAPI()
        
    }
    
    func bindUserData(){
        
        self.lblUserName.text = DataModel.sharedInstance.onFetchUserName()
        self.imgUserProfile.sd_setImage(with: URL.init(string: DataModel.sharedInstance.onFetchUserImage()), placeholderImage: UIImage(named:Constant.PlaceHolder.kDefaultUser), options: SDWebImageOptions.retryFailed, completed: nil)
        
    }
    
    //MARK: - IBActions method
    @IBAction func btnMenu_Tapped(_ sender: Any) {
        
        if self.revealViewController() != nil
        {
            self.revealToggle(true)
        }
        
        
    }
    
    @IBAction func btnEditProfile_Tapped(_ sender: Any) {
        
        let editVC = EditProfileVC(nibName: "EditProfileVC", bundle: nil) as EditProfileVC
        _ = self.navigationController?.pushViewController(editVC, animated: true)
        
    }
    
    
    //MARK: - Call Api
    func callResentListData()
    {
         self.arr_RecentServices.removeAll()
         self.arr_CompanyServiceList.removeAll()
        var dict = [String : Any]()
        
        dict["sessionId"]    = DataModel.sharedInstance.onFetchSessionId()
        dict["userId"]       = DataModel.sharedInstance.onFetchUserId()
        
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kGetFavouritesOfUser, inputParams: dict, success: { (dict) in
            
            KVNProgress.dismiss()
            if dict?["Result"] as? Bool == true {
                let data = dict?["Data"] as? [String:Any]
                if data?.count != 0
                {
                    let recent = data?["favourites"] as? NSArray
                    for i in recent!
                    {
                        let idata = i as? [String:Any]
                        let type = idata?["type"] as! Int
                        // Service
                        if type == 1
                        {
                            let recentServices = idata?["data"] as? [String:Any]
                            if recentServices != nil
                            {
                                let Service = RecentServiceModel.init(fromDictionary: recentServices!)
                                self.arr_RecentServices.append(Service)
                            
                            }
                        }
                            // Buyer
                        else if type == 2
                        {
                            let recentServices = idata?["data"] as? [String:Any]
                            if recentServices != nil
                            {
                                let Service = CompanyServiceList.init(dict: recentServices!)
                                self.arr_CompanyServiceList.append(Service)
                            }
                        }
                    }

                }
                
            }
            self.stopLoading()
        }) { error in
            self.stopLoading()
            Utilities.showAlertView(title: "", message: error)
            
        }
        
    }
    
    func stopLoading()
    {
        KVNProgress.dismiss()
        self.tblRecentFavorites.reloadData()
        if self.arr_CompanyServiceList.count == 0 && self.arr_RecentServices.count == 0
        {
            self.lblNoDataFound.isHidden = false
        }
        else
        {
            self.lblNoDataFound.isHidden = true
        }
    }
    
    func callGetUserProfileAPI(){
        
        User.callGetUSerProfileDataAPI(success: { (dict) in
            
            let userId = dict?["userId"] as? String
            let sessionId = dict?["sessionId"] as? String
            let userType = dict?["userRole"] as? Int
            let email = dict?["email"] as? String
            let mobilePhone = dict?["mobilePhone"] as? String
            let name = dict?["name"] as? String
            let firstName = dict?["firstName"] as? String
            let lastName = dict?["lastName"] as? String
            let userImage = dict?["profileImage"] as? String
            let description = dict?["description"] as? String
            
            let countryCode = dict?["countryCode"] as? String
            let phoneNumber = dict?["phoneNumber"] as? String
            let ezListScreenName = dict?["ezListScreenName"] as? String
            let promoCode = dict?["promoCode"] as? String
            
            DataModel.sharedInstance.setUserDataWhenLogin(userID: userId, sessionId: sessionId, userType: userType, email: email, name: name, firstName: firstName, lastName: lastName, mobile: mobilePhone,phone:phoneNumber,countryCode: countryCode,userProfile: userImage, ezListScreenName: ezListScreenName, description: description, promoCode: promoCode)
            
             self.bindUserData()
            
        }) {
            
        }
        
    }

    
}

extension BuyerProfileVC : UITableViewDelegate,UITableViewDataSource
{
    //MARK: - Tableview Method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arr_RecentServices.count + self.arr_CompanyServiceList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Utilities.isDeviceiPad() ? 92.0 : 72.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row < self.arr_RecentServices.count
        {
            self.tblRecentFavorites.register(UINib(nibName: "MyFavoiuriteServiceCell", bundle: nil), forCellReuseIdentifier: "MyFavoiuriteServiceCell")
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyFavoiuriteServiceCell", for: indexPath) as! MyFavoiuriteServiceCell
            let data = self.arr_RecentServices[indexPath.row]
            if data.serviceImage != nil{
                cell.imgService.sd_setImage(with: URL.init(string: data.serviceImage!), placeholderImage: UIImage(named:Constant.PlaceHolder.kPhoto), options: SDWebImageOptions.retryFailed, completed: nil)
            }else{
                cell.imgService.image = UIImage.init(named: Constant.PlaceHolder.kPhoto)
            }
            cell.lblPrice.text        = "$" + data.flatFee!
            cell.btnFavourite.isHidden     = false
            
            cell.lblServiceTitle.text = data.screenName
            
            return cell
        }
        else
        {
            self.tblRecentFavorites.register(UINib(nibName: "MassageTherapyCell", bundle: nil), forCellReuseIdentifier: "MassageTherapyCell")
            let cell = tableView.dequeueReusableCell(withIdentifier: "MassageTherapyCell", for: indexPath) as! MassageTherapyCell
            
            let serviceListData = self.arr_CompanyServiceList[indexPath.row - self.arr_RecentServices.count]
            
            if serviceListData.profileImage != nil{
                cell.imgMassage.sd_setImage(with: URL.init(string: serviceListData.profileImage!), placeholderImage: UIImage(named:"placeHolder"), options: SDWebImageOptions.retryFailed, completed: nil)
            }else{
                cell.imgMassage.image = UIImage.init(named: "placeHolder")
            }
            cell.btnFavourite.isHidden = false
            cell.lblHeading.text = serviceListData.screenName
            cell.lblMassage.text = serviceListData.serviceDescription
            if serviceListData.isAvailable!
            {
                cell.lblStatus.text = "Available Now"
                cell.lblStatus.textColor = Constant.Color.serviceAvailableColor
            }
            else
            {
                cell.lblStatus.text = "Available by Appointment"
                cell.lblStatus.textColor = Constant.Color.serviceAvailableByAppColor
                
            }
            
            return cell
        }
    }
    
}
