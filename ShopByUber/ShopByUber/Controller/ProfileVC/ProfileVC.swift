//
//  ProfileVC.swift
//  ShopByUber
//
//  Created by Administrator on 6/13/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class ProfileVC: BaseView {
    
    //MARK: - IBAction
    
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var imgUserProfile: UIImageView!
    @IBOutlet var vwNavigationBar: UIView!
    @IBOutlet weak var lblRecentText: UILabel!
    @IBOutlet weak var tblRecent: UITableView!
    
    @IBOutlet weak var lblNoDataFound: UILabel!
    
    //MARK: - View Life Cycle
    var arr_RecentServices = [RecentServiceModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialConfig()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        //self.bindUserData()
        self.callResentListData()
    }
    
    
    
    @IBAction func btnEditProfile_Clicked(_ sender: UIButton) {
        
        let editVC = SellerEditProfileVC(nibName: "SellerEditProfileVC", bundle: nil) as SellerEditProfileVC
            _ = self.navigationController?.pushViewController(editVC, animated: true)
        
    }
    
    //MARK: - Private Methods
    override func viewDidLayoutSubviews() {
        print("Frame - ",imgUserProfile.frame)
        Utilities.decorateView(imgUserProfile.layer, cornerRadius: self.imgUserProfile.frame.height/2, borderWidth: 5, borderColor: Constant.Color.viewShadowColor)
    }
    func initialConfig(){
        
        Utilities.applyShadowEffect(view: [vwNavigationBar])
        
        self.imgUserProfile.clipsToBounds = true
        
        Utilities.decorateView(imgUserProfile.layer, cornerRadius: self.imgUserProfile.frame.height/2, borderWidth: 5, borderColor: Constant.Color.viewShadowColor)
        
        self.callGetUserProfileAPI()
        
        self.tblRecent.register(UINib(nibName: "RecentSellerRequestCell", bundle: nil), forCellReuseIdentifier: "RecentSellerRequestCell")
        self.view.layoutIfNeeded()
        
        
    }
    
    func bindUserData(){
        
        
        self.lblUserName.text = DataModel.sharedInstance.onFetchUserName()
        self.imgUserProfile.sd_setImage(with: URL.init(string: DataModel.sharedInstance.onFetchUserImage()), placeholderImage: UIImage(named:Constant.PlaceHolder.kDefaultUser), options: SDWebImageOptions.retryFailed, completed: nil)
        
    }
    
    
    //MARK: - Call Api
    func callResentListData()
    {
        self.arr_RecentServices.removeAll()
        KVNProgress.show()
        var dict = [String : Any]()
        
        dict["sessionId"]    = DataModel.sharedInstance.onFetchSessionId()
        dict["userId"]       = DataModel.sharedInstance.onFetchUserId()
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kGetRecentServices, inputParams: dict, success: { (dict) in
            
            KVNProgress.dismiss()
            if dict?["Result"] as? Bool == true {
                let data = dict?["Data"] as? [String:Any]
                if data?.count != 0
                {
                    let recentServices = data?["recentServices"] as? NSArray
                    if recentServices != nil
                    {
                        for i in recentServices!
                        {
                            let Service = RecentServiceModel.init(fromDictionary: i as! [String : Any])
                            self.arr_RecentServices.append(Service)
                        }
                        
                    }
                    
                }
            }
            self.stopLoading()
        }) { error in
            self.stopLoading()
            Utilities.showAlertView(title: "", message: error)
        }
    }
    
    func stopLoading()
    {
        KVNProgress.dismiss()
        self.tblRecent.reloadData()
        self.lblNoDataFound.isHidden = arr_RecentServices.count==0 ?false :true
        
    }
    
    func callGetUserProfileAPI(){
        
        User.callGetUSerProfileDataAPI(success: { (dict) in
            
            let userId = dict?["userId"] as? String
            let sessionId = dict?["sessionId"] as? String
            let userType = dict?["userRole"] as? Int
            let email = dict?["email"] as? String
            let mobilePhone = dict?["mobilePhone"] as? String
            let name = dict?["name"] as? String
            let firstName = dict?["firstName"] as? String
            let lastName = dict?["lastName"] as? String
            let userImage = dict?["profileImage"] as? String
            let description = dict?["description"] as? String
            
            let countryCode = dict?["countryCode"] as? String
            let phoneNumber = dict?["phoneNumber"] as? String
            let ezListScreenName = dict?["ezListScreenName"] as? String
            
            DataModel.sharedInstance.setUserDataWhenLogin(userID: userId, sessionId: sessionId, userType: userType, email: email, name: name, firstName: firstName, lastName: lastName, mobile: mobilePhone,phone:phoneNumber,countryCode: countryCode,userProfile: userImage, ezListScreenName: ezListScreenName, description: description, promoCode: "")
            
            self.bindUserData()
            
        }) {
            
        }
        
    }
    
}

extension ProfileVC : UITableViewDelegate,UITableViewDataSource
{
    //MARK: - Tableview Method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arr_RecentServices.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Utilities.isDeviceiPad() ? 92.0 : 72.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecentSellerRequestCell", for: indexPath) as? RecentSellerRequestCell
        let serviceData = self.arr_RecentServices[indexPath.row]
        
        if serviceData.serviceImage != nil{
            cell?.imgService.sd_setImage(with: URL.init(string: serviceData.serviceImage!), placeholderImage: UIImage(named:Constant.PlaceHolder.kPhoto), options: SDWebImageOptions.retryFailed, completed: nil)
        }else{
            cell?.imgService.image = UIImage.init(named: Constant.PlaceHolder.kPhoto)
        }
        
        cell?.lblServiceTitle.text = serviceData.screenName
        cell?.lblPrice.text = "$ " + serviceData.flatFee
        
        if serviceData.status == 5
        {
            cell?.lblStatus.text = "Completed"
            cell?.lblStatus.textColor = UIColor.darkGray
        }
        else
        {
            cell?.lblStatus.text = "Confirm"
            cell?.lblStatus.textColor = UIColor.black
        }
        return cell!
    }
}
