//
//  EditProfileVC.swift
//  ShopByUber
//
//  Created by Administrator on 6/13/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class SellerEditProfileVC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,PECropViewControllerDelegate {
    
    //MARK:Outlates
    
    @IBOutlet var vwNavigationBar: UIView!
    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet var vwName: UIView!
    @IBOutlet var vwMobile: UIView!
    @IBOutlet var vwEmail: UIView!
    @IBOutlet var vwNewPassword: UIView!
    @IBOutlet var vwConfirmPassword: UIView!
    
    @IBOutlet weak var btnProfilePicCapture: UIButton!
    @IBOutlet var gesturesTapProfilePic: UITapGestureRecognizer!
    
    @IBOutlet var txtName: UITextField!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtNewPassword: UITextField!
    @IBOutlet var txtConfirmPassword: UITextField!
    @IBOutlet weak var txtDesc: UITextView!
    
    @IBOutlet weak var consTxtDescHeight: NSLayoutConstraint!
    //ProfilePic related
    
    @IBOutlet weak var imgAddPicCam: UIImageView!
    @IBOutlet weak var lblAddProfilePic: UILabel!
    
    @IBOutlet weak var imgBlueCam: UIImageView!
    //MARK: - Variables
    
    var imagePicker = UIImagePickerController()
    var isProfileImageChanged = false
    
    //MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialConfig()
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - IBAction
    
    @IBAction func vwBG_Clicked(_ sender: UITapGestureRecognizer) {
        
        self.view.endEditing(true)
        
    }
    
    @IBAction func btnBack_Clicked(_ sender: UIButton) {
        
        _ = self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func btnSaveChanges_Clicked(_ sender: UIButton) {
        
        if Utilities.validateTextFieldString(object: txtName.text, validationMessage: Constant.ValidationMessage.kEnterName),
            Utilities.validateTextFieldString(object: txtEmail.text, validationMessage: Constant.ValidationMessage.kEmail),
            Utilities.validateEmail(email: txtEmail.text, validationMessage: Constant.ValidationMessage.kValidEmail)
        {
            if (txtNewPassword.text?.characters.count)! > 0
            {
                if Utilities.validateTextFieldString(object: txtConfirmPassword.text, validationMessage: Constant.ValidationMessage.kEnterConfirmPassword)
                {
                    if (self.txtNewPassword.text != self.txtConfirmPassword.text){
                        Utilities.showAlertView(title: "Alert", message: Constant.ValidationMessage.kConfirmPassword)
                        return
                    }
                }
            }
            if isProfileImageChanged {
                
                self.callUploadImage()
                
            }else{
                
                self.callUpdateProfileAPI(imageName: "")
                
            }
            
        }
        
    }
    
    @IBAction func imageProfile_Clicked(_ sender: UITapGestureRecognizer) {
        
        imagePicker.delegate = self
        
        let alert:UIAlertController = UIAlertController(title: "Choose Image", message: nil, preferredStyle: Utilities.isDeviceiPad() ? .alert : .actionSheet )
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default){
            UIAlertAction in
            self.openProfileUserCamera()
        }
        
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.openProfileUserGallary()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
        }
        
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    @IBAction func btnProfileCapture_Click(_ sender: Any) {
        
        self.imageProfile_Clicked(self.gesturesTapProfilePic)
    }
    
    //MARK: - Private Methods
    
    func initialConfig(){
        
        Utilities.applyShadowEffect(view: [vwNavigationBar,vwName,vwMobile,vwEmail,vwNewPassword,vwConfirmPassword])
        
        self.imgProfile.clipsToBounds = true
        
        Utilities.decorateView(imgProfile.layer, cornerRadius: self.imgProfile.frame.height/2, borderWidth: 5, borderColor: Constant.Color.viewShadowColor)
        
        self.callGetUserProfileAPI()
        // self.fillData()
    }
    
    func fillData(){
        
        self.txtEmail.text = DataModel.sharedInstance.onFetchEmail()
        if DataModel.sharedInstance.onFetchEmail() == ""
        {
            self.vwEmail.isUserInteractionEnabled = true
        }
        else
        {
            self.vwEmail.isUserInteractionEnabled = false
        }
        self.txtDesc.text = DataModel.sharedInstance.onFetchSellerDescription()
        self.txtName.text = DataModel.sharedInstance.onFetchUserName()
        if DataModel.sharedInstance.onFetchUserImage() != ""
        {
            self.imgProfile.sd_setImage(with: URL.init(string: DataModel.sharedInstance.onFetchUserImage()), placeholderImage: UIImage(named:Constant.PlaceHolder.kDefaultUser), options: SDWebImageOptions.retryFailed, completed:{ (image, err, nil, url) in
                self.imgProfile.image = image
                self.imgProfile.contentMode = .scaleAspectFill
            })
            self.imgAddPicCam.isHidden = true
            self.lblAddProfilePic.isHidden = true
            self.imgBlueCam.isHidden = false
            self.btnProfilePicCapture.isHidden = false
            imgProfile.removeGestureRecognizer(self.gesturesTapProfilePic)
        }
        else
        {
            self.imgAddPicCam.isHidden = false
            self.lblAddProfilePic.isHidden = false
            self.imgBlueCam.isHidden = true
               self.btnProfilePicCapture.isHidden = true
        }
        
    }
    
    //MARK: Image Delegate
    
    func openProfileUserGallary() {
        
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        imagePicker.mediaTypes = [kUTTypeImage as String]
        present(imagePicker, animated: true, completion: nil)
    }
    
    func openProfileUserCamera() {
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.allowsEditing = false
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.cameraCaptureMode = .photo
            imagePicker.modalPresentationStyle = .fullScreen
            present(imagePicker,animated: true,completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        //        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
        //            self.imgProfile.image = pickedImage
        //            isProfileImageChanged = true
        //            self.imgAddPicCam.isHidden = true
        //            self.lblAddProfilePic.isHidden = true
        //            self.imgBlueCam.isHidden = false
        //            imgProfile.removeGestureRecognizer(self.gesturesTapProfilePic)
        //        }
        //
        //        dismiss(animated:true, completion: nil)
        dismiss(animated:true, completion: nil)
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            openEditor(image: pickedImage)
        }
        
    }
    //MARK: - PECropViewController Delegate
    func openEditor(image: UIImage) {
        let controller = PECropViewController()
        controller.delegate = self
        controller.image = image
        let width: CGFloat? = image.size.width
        let height: CGFloat? = image.size.height
        let length: CGFloat = min(width!, height!)
        let ratioValue = width!/4
        
        controller.imageCropRect = CGRect(x: (width! - length) / 2, y: (height! - length) / 2, width: ratioValue*4, height: ratioValue*3)
        controller.keepingCropAspectRatio = true
        let navigationController = UINavigationController(rootViewController: controller as UIViewController )
        if UI_USER_INTERFACE_IDIOM() == .pad {
            navigationController.modalPresentationStyle = .formSheet
        }
        present(navigationController, animated: true) { _ in }
    }
    func cropViewController(_ controller: PECropViewController!, didFinishCroppingImage croppedImage: UIImage!) {
        controller.dismiss(animated: true) { _ in }
        self.imgProfile.image = croppedImage
        isProfileImageChanged = true
        self.imgAddPicCam.isHidden = true
        self.lblAddProfilePic.isHidden = true
        self.imgBlueCam.isHidden = false
        imgProfile.removeGestureRecognizer(self.gesturesTapProfilePic)
        if UI_USER_INTERFACE_IDIOM() == .pad {
        }
        
    }
    
    func cropViewController(_ controller: PECropViewController, didFinishCroppingImage croppedImage: UIImage, transform: CGAffineTransform, cropRect: CGRect) {
        controller.dismiss(animated: true) { _ in }
        self.imgProfile.image = croppedImage
        isProfileImageChanged = true
        self.imgAddPicCam.isHidden = true
        self.lblAddProfilePic.isHidden = true
        self.imgBlueCam.isHidden = false
        imgProfile.removeGestureRecognizer(self.gesturesTapProfilePic)
        if UI_USER_INTERFACE_IDIOM() == .pad {
        }
    }
    
    func cropViewControllerDidCancel(_ controller: PECropViewController) {
        self.isProfileImageChanged = false
        
        
        
        if UI_USER_INTERFACE_IDIOM() == .pad {
            
        }
        controller.dismiss(animated: true) { _ in }
    }
    
    //MARK: API Call
    
    func callUploadImage(){
        
        ServiceProfile.callUploadImageAPI(imageType: Constant.ImageType.CompanyProfile, image: self.imgProfile.image, success: { (name) in
            
            self.callUpdateProfileAPI(imageName: name)
            
        }) {
            
        }
        
    }
    
    func callUpdateProfileAPI(imageName:String?){
        
        User.callUpdateUserDataAPI(imageName: imageName, name: txtName.text, firstName: "", lastName: "", ezListScreenName: "", mobile: DataModel.sharedInstance.onFetchMobileNumber(), email: txtEmail.text, password: txtNewPassword.text, sellerDescription: txtDesc.text, success: { (dict) in
            
            let userId = dict?["userId"] as? String
            let sessionId = dict?["sessionId"] as? String
            let userType = dict?["userRole"] as? Int
            let email = dict?["email"] as? String
            let mobilePhone = dict?["mobilePhone"] as? String
            let name = dict?["name"] as? String
            let userImage = dict?["profileImage"] as? String
            let description = dict?["description"] as? String
            
            let countryCode = dict?["countryCode"] as? String
            let phoneNumber = dict?["phoneNumber"] as? String
            let ezListScreenName = dict?["ezListScreenName"] as? String
            
            DataModel.sharedInstance.setUserDataWhenLogin(userID: userId, sessionId: sessionId, userType: userType, email: email, name: name, firstName: "",lastName: "", mobile: mobilePhone,phone:phoneNumber,countryCode: countryCode,userProfile: userImage, ezListScreenName: ezListScreenName, description: description, promoCode: "")
            
            self.navigationController?.popViewController(animated: true)
            
        }) {
            
        }
        
    }
    func callGetUserProfileAPI(){
        
        User.callGetUSerProfileDataAPI(success: { (dict) in
            
            let userId = dict?["userId"] as? String
            let sessionId = dict?["sessionId"] as? String
            let userType = dict?["userRole"] as? Int
            let email = dict?["email"] as? String
            let mobilePhone = dict?["mobilePhone"] as? String
            let name = dict?["name"] as? String
            let userImage = dict?["profileImage"] as? String
            let description = dict?["description"] as? String
            
            let countryCode = dict?["countryCode"] as? String
            let phoneNumber = dict?["phoneNumber"] as? String
            let ezListScreenName = dict?["ezListScreenName"] as? String
            
            DataModel.sharedInstance.setUserDataWhenLogin(userID: userId, sessionId: sessionId, userType: userType, email: email, name: name, firstName: "", lastName: "", mobile: mobilePhone,phone:phoneNumber,countryCode: countryCode,userProfile: userImage, ezListScreenName: ezListScreenName, description: description, promoCode: "")
            
            self.fillData()
            
        }) {
            
        }
        
    }
}

extension SellerEditProfileVC : UITextFieldDelegate,UITextViewDelegate
{
    //MARK: UITextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtName{
            txtDesc.becomeFirstResponder()
            return false
        }else if textField == txtDesc{
            txtNewPassword.becomeFirstResponder()
        }else if textField == txtNewPassword{
            txtConfirmPassword.becomeFirstResponder()
        }else{
            textField.resignFirstResponder()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return true
        
    }
    
}
