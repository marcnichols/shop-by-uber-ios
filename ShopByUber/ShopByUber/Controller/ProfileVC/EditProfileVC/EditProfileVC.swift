//
//  EditProfileVC.swift
//  ShopByUber
//
//  Created by Administrator on 6/13/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class EditProfileVC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,PECropViewControllerDelegate {
    
    //MARK: IBAction
    
    @IBOutlet var vwNavigationBar: UIView!
    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet weak var vwFirstName: UIView!
    @IBOutlet weak var vwLastName: UIView!
    @IBOutlet weak var vwScreenName: UIView!
    @IBOutlet var vwMobile: UIView!
    @IBOutlet var vwEmail: UIView!
    @IBOutlet var vwNewPassword: UIView!
    @IBOutlet var vwConfirmPassword: UIView!
    @IBOutlet weak var btnCapturePic: UIButton!
    @IBOutlet var gesturesTapProfilePic: UITapGestureRecognizer!
    
    @IBOutlet var txtName: UITextField!
    
    @IBOutlet weak var txtFirstName: UITextField!
    
    @IBOutlet weak var txtLastName: UITextField!
    
    @IBOutlet weak var txtScreenName: UITextField!
    @IBOutlet var txtMobile: UITextField!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtNewPassword: UITextField!
    @IBOutlet var txtConfirmPassword: UITextField!
    @IBOutlet weak var lblCounrtyCode: UILabel!
    //ProfilePic related
    
    @IBOutlet weak var imgAddPicCam: UIImageView!
    @IBOutlet weak var lblAddProfilePic: UILabel!
    
    @IBOutlet weak var imgBlueCam: UIImageView!
    //MARK: -  Variables
    
    var imagePicker = UIImagePickerController()
    var isProfileImageChanged = false
    
    //MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialConfig()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        Utilities.decorateView(imgProfile.layer, cornerRadius: self.imgProfile.frame.height/2, borderWidth: 5, borderColor: Utilities.colorWithHexString("D3D3D3"))
        
    }
    
    
    //MARK: - IBAction
    
    @IBAction func vwBG_Clicked(_ sender: UITapGestureRecognizer) {
        
        self.view.endEditing(true)
        
    }
    
    @IBAction func btnBack_Clicked(_ sender: UIButton) {
        
        _ = self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func btnSaveChanges_Clicked(_ sender: UIButton) {
        if self.isValidProfileData(firstName: txtFirstName.text,lastName: txtLastName.text,screenName: txtScreenName.text, number: txtMobile.text, email: txtEmail.text, password: txtNewPassword.text, confirmPassword: txtConfirmPassword.text){
            if isProfileImageChanged {
                
                self.callUploadImage()
                
            }else{
                
                self.callUpdateProfileAPI(imageName: "")
                
            }

            
        }
        
        
    }
    
    @IBAction func imageProfile_Clicked(_ sender: UITapGestureRecognizer) {
        
        imagePicker.delegate = self
        
        let alert:UIAlertController = UIAlertController(title: "Choose Image", message: nil, preferredStyle: Utilities.isDeviceiPad() ? .alert : .actionSheet )
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default){
            UIAlertAction in
            self.openProfileUserCamera()
        }
        
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.openProfileUserGallary()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
        }
        
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    @IBAction func btnCountryPic(_ sender: Any) {
        self.view.showCountryPickPopup(onSubmitClick: {(selectedCountryCode) in
            self.lblCounrtyCode.text = selectedCountryCode
        })
    }
    
    @IBAction func btnPicImages_Click(_ sender: Any) {
        
        self.imageProfile_Clicked(self.gesturesTapProfilePic)
    }
    
    //MARK: - Validation
    
    func isValidProfileData(firstName: String?,lastName: String?,screenName: String?,number:String?, email: String?, password: String?, confirmPassword: String?)  -> Bool {
        
        
        let firstName = firstName?.trimmingCharacters(in: .whitespacesAndNewlines)
        let lastName = lastName?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let screenName = screenName?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let number = number?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let email = email?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let password = password?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let confirmPassword = confirmPassword?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        var message = ""
        
        if firstName == "" {
            
            message = Constant.ValidationMessage.kfirstName
            
        }else if lastName == "" {
            
            message = Constant.ValidationMessage.kLastName
            
        }else if screenName == "" {
            
            message = Constant.ValidationMessage.kEZScreenName
            
        }else if email == ""{
            
            message =  Constant.ValidationMessage.kEmail
            
        }else if !Utilities.isValidEmail(email!){
            
            message =  Constant.ValidationMessage.kValidEmail
            
        }else if number == ""{
            
            message =  Constant.ValidationMessage.kMobileNumber
            
        } else if number?.characters.count != 12{
            
            message =  Constant.ValidationMessage.kCompanyValidPhone
            
        }else if password != ""{
            
            if password != confirmPassword{
                
                message = Constant.ValidationMessage.kConfirmPassword
                
            }
            else
            {
                return true
            }
            
        }else if confirmPassword != "" {
            
            if password != confirmPassword{
                
                message = Constant.ValidationMessage.kConfirmPassword
                
            }
            else
            {
                return true
            }

        }
        else {
            
            return true
        }
        
        Utilities.showAlertView(title: "Alert", message: message)
        return false
    }
    
    //MARK: - Private Methods
    
    func initialConfig(){
        
        let image: UIImage? =  #imageLiteral(resourceName: "cameraBlue").withRenderingMode(.alwaysTemplate)
        imgBlueCam.image = image
        imgBlueCam.tintColor = UIColor.init(red: 48/255, green: 173/255, blue: 99/255, alpha: 1.0)
        
        Utilities.applyShadowEffect(view: [vwNavigationBar,vwFirstName,vwLastName,vwScreenName,vwMobile,vwEmail,vwNewPassword,vwConfirmPassword])
        
        self.imgProfile.clipsToBounds = true
        
        Utilities.decorateView(imgProfile.layer, cornerRadius: self.imgProfile.frame.height/2, borderWidth: 5, borderColor: Utilities.colorWithHexString("D3D3D3"))
        
        self.lblCounrtyCode.text = "+1"
        
        self.callGetUserProfileAPI()
    }
    
    func fillData(){
        
        self.txtEmail.text = DataModel.sharedInstance.onFetchEmail()
        if DataModel.sharedInstance.onFetchEmail() == ""
        {
            self.vwEmail.isUserInteractionEnabled = true
        }
        else
        {
            self.vwEmail.isUserInteractionEnabled = false
        }
        self.txtMobile.text = DataModel.sharedInstance.onFetchMobileNumber().toPhoneNumber()
        
     //   self.txtName.text = DataModel.sharedInstance.onFetchUserName()
        self.txtFirstName.text = DataModel.sharedInstance.onFetchFirstName()
        self.txtLastName.text = DataModel.sharedInstance.onFetchLastName()
        self.txtScreenName.text = DataModel.sharedInstance.onFetchEzListScreenName()
        
        if DataModel.sharedInstance.onFetchUserImage() != ""
        {
            self.imgProfile.sd_setImage(with: URL.init(string: DataModel.sharedInstance.onFetchUserImage()), placeholderImage: UIImage(named:Constant.PlaceHolder.kDefaultUser), options: SDWebImageOptions.retryFailed, completed:{ (image, err, nil, url) in
                self.imgProfile.image = image
                self.imgProfile.contentMode = .scaleAspectFill
            })
            
            self.imgAddPicCam.isHidden = true
            self.lblAddProfilePic.isHidden = true
            self.imgBlueCam.isHidden = false
            self.btnCapturePic.isHidden = false
            imgProfile.removeGestureRecognizer(self.gesturesTapProfilePic)
        }
        else
        {
            self.imgAddPicCam.isHidden = false
            self.lblAddProfilePic.isHidden = false
            self.imgBlueCam.isHidden = true
            self.btnCapturePic.isHidden = true
        }
        
        

        
    }
    
    //MARK: - Image Delegate
    
    func openProfileUserGallary() {
        
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        imagePicker.mediaTypes = [kUTTypeImage as String]
        present(imagePicker, animated: true, completion: nil)
    }
    
    func openProfileUserCamera() {
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.allowsEditing = false
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.cameraCaptureMode = .photo
            imagePicker.modalPresentationStyle = .fullScreen
            present(imagePicker,animated: true,completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {

        dismiss(animated:true, completion: nil)
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            openEditor(image: pickedImage)
        }
    }
    
    //MARK: - PECropViewController Delegate
    func openEditor(image: UIImage) {
        let controller = PECropViewController()
        controller.delegate = self
        controller.image = image
        let width: CGFloat? = image.size.width
        let height: CGFloat? = image.size.height
        let length: CGFloat = min(width!, height!)
        let ratioValue = width!/4
        
        controller.imageCropRect = CGRect(x: (width! - length) / 2, y: (height! - length) / 2, width: ratioValue*4, height: ratioValue*3)
        controller.keepingCropAspectRatio = true
        let navigationController = UINavigationController(rootViewController: controller as UIViewController )
        if UI_USER_INTERFACE_IDIOM() == .pad {
            navigationController.modalPresentationStyle = .formSheet
        }
        present(navigationController, animated: true) { _ in }
    }
    func cropViewController(_ controller: PECropViewController!, didFinishCroppingImage croppedImage: UIImage!) {
        controller.dismiss(animated: true) { _ in }
        self.imgProfile.image = croppedImage
        isProfileImageChanged = true
        self.imgAddPicCam.isHidden = true
        self.lblAddProfilePic.isHidden = true
        self.imgBlueCam.isHidden = false
        imgProfile.removeGestureRecognizer(self.gesturesTapProfilePic)
        if UI_USER_INTERFACE_IDIOM() == .pad {
        }
        
    }
    
    func cropViewController(_ controller: PECropViewController, didFinishCroppingImage croppedImage: UIImage, transform: CGAffineTransform, cropRect: CGRect) {
        controller.dismiss(animated: true) { _ in }
        self.imgProfile.image = croppedImage
        isProfileImageChanged = true
        self.imgAddPicCam.isHidden = true
        self.lblAddProfilePic.isHidden = true
        self.imgBlueCam.isHidden = false
        imgProfile.removeGestureRecognizer(self.gesturesTapProfilePic)
        if UI_USER_INTERFACE_IDIOM() == .pad {
        }
    }
    
    func cropViewControllerDidCancel(_ controller: PECropViewController) {
        self.isProfileImageChanged = false
        
        
        
        if UI_USER_INTERFACE_IDIOM() == .pad {
            
        }
        controller.dismiss(animated: true) { _ in }
    }
    
    //MARK:- Mobile Validation
    func checkEnglishPhoneNumberFormat(string: String?, str: String?) -> Bool{
        
        if string == ""{ //BackSpace
            
            return true
            
        }else if str!.characters.count == 4{
            
            txtMobile.text = txtMobile.text! + "-"
            
        }else if str!.characters.count == 8{
            
            txtMobile.text = txtMobile.text! + "-"
            
        }else if str!.characters.count > 12{
            
            return false
        }
        
        return true
    }
    
    //MARK: - API Call
    
    func callUploadImage(){
        
        ServiceProfile.callUploadImageAPI(imageType: Constant.ImageType.UserProfile, image: self.imgProfile.image, success: { (name) in
            
            self.callUpdateProfileAPI(imageName: name)
            
        }) {
            
        }
        
    }
    
    func callUpdateProfileAPI(imageName:String?){
        
        let mobileNo = txtMobile.text?.replacingOccurrences(of: "-", with: "")
        User.callUpdateUserDataAPI(imageName: imageName, name: txtFirstName.text! + " " + txtLastName.text!,firstName: txtFirstName.text, lastName: txtLastName.text,ezListScreenName: txtScreenName.text, mobile: mobileNo,countryCode:self.lblCounrtyCode.text, email: txtEmail.text, password: txtNewPassword.text, sellerDescription: "", success: { (dict) in
                
                let userId = dict?["userId"] as? String
                let sessionId = dict?["sessionId"] as? String
                let userType = dict?["userRole"] as? Int
                let email = dict?["email"] as? String
                let mobilePhone = dict?["mobilePhone"] as? String
                let name = dict?["name"] as? String
                let firstName = dict?["firstName"] as? String
                let lastName = dict?["lastName"] as? String
                let userImage = dict?["profileImage"] as? String
                let description = dict?["description"] as? String
                let countryCode = dict?["countryCode"] as? String
                let ezListScreenName = dict?["ezListScreenName"] as? String
            
           
            DataModel.sharedInstance.setUserDataWhenLogin(userID: userId, sessionId: sessionId, userType: userType, email: email, name: name, firstName: firstName, lastName: lastName, mobile: mobilePhone,phone:mobilePhone,countryCode: countryCode,userProfile: userImage, ezListScreenName: ezListScreenName, description: description, promoCode: "")
            
                self.navigationController?.popViewController(animated: true)
                
            }) {
                
            }
    }
    func callGetUserProfileAPI(){
        
        User.callGetUSerProfileDataAPI(success: { (dict) in
            
            let userId = dict?["userId"] as? String
            let sessionId = dict?["sessionId"] as? String
            let userType = dict?["userRole"] as? Int
            let email = dict?["email"] as? String
            let mobilePhone = dict?["phoneNumber"] as? String
            let name = dict?["name"] as? String
            let firstName = dict?["firstName"] as? String
            let lastName = dict?["lastName"] as? String
            let userImage = dict?["profileImage"] as? String
            let description = dict?["description"] as? String
            let countryCode = dict?["countryCode"] as? String
            let ezListScreenName = dict?["ezListScreenName"] as? String
            
            DataModel.sharedInstance.setUserDataWhenLogin(userID: userId, sessionId: sessionId, userType: userType, email: email, name: name,firstName: firstName, lastName: lastName, mobile: mobilePhone,phone:mobilePhone,countryCode: countryCode,userProfile: userImage, ezListScreenName: ezListScreenName, description: description, promoCode: "")
            
            self.fillData()
            
        }) {
            
        }
        
    }
    
}

extension EditProfileVC : UITextFieldDelegate
{
    //MARK: UITextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtFirstName{
            txtLastName.becomeFirstResponder()
        }else if textField == txtLastName{
            txtScreenName.becomeFirstResponder()
        }else if textField == txtScreenName{
            txtMobile.becomeFirstResponder()
        }else if textField == txtMobile{
            txtNewPassword.becomeFirstResponder()
        }else if textField == txtNewPassword{
            txtConfirmPassword.becomeFirstResponder()
        }else{
            textField.resignFirstResponder()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let str = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        if textField == txtMobile{
            
            return checkEnglishPhoneNumberFormat(string: string, str: str)
            
        }else{
            
            return true
        }
        
    }
    
    
}
