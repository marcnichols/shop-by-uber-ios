//
//  MyWantedServiceVC.swift
//  The EZ List
//
//  Created by mac on 4/12/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

import UIKit

class MyWantedServiceVC: BaseView {
    
    @IBOutlet var tblWantedServices: UITableView!
    @IBOutlet var lblNoDataFound: UILabel!
    @IBOutlet var vwProgress: UIActivityIndicatorView!
    
    
    var arrMyWantedServices = [MyWantedServices]()
    var recordCount = 0
    var pageNo = 0
    var numOfRecords = 10
    var isRefreshing : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialConfig()
        
        // Do any additional setup after loading the view.
        //self.automaticallyAdjustsScrollViewInsets = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //        self.arrMyWantedServices.removeAll()
        //        recordCount = 0
        //        self.isRefreshing = false
        //        self.pageNo = 0
        if self.arrMyWantedServices.count == 0{
            
            self.setPagination()
        }
    }
    
    //MARK: Private Methods
    func initialConfig()
    {
        tblWantedServices.register(UINib.init(nibName: "WantedServiceCell", bundle: nil), forCellReuseIdentifier: "WantedServiceCell")
        self.tblWantedServices.separatorStyle = .none
    }
    
    func setPagination() {
        self.getWantedList(showLoader: true)
        
        tblWantedServices.es.addInfiniteScrolling {
            self.refreshRecord()
        }
        
        tblWantedServices.es.resetNoMoreData()
        
        
        if let animator = self.tblWantedServices.footer?.animator as? ESRefreshFooterAnimator {
            animator.noMoreDataDescription = ""
        }
        
        tblWantedServices.es.addPullToRefresh {
            
            //            if !(!self.isRefreshing) {
            //                self.arrMyWantedServices.removeAll()
            //                self.tblWantedServices.reloadData()
            //                self.pageNo = 0
            //                self.refreshRecord()
            //            }
            //            else {
            //                self.tblWantedServices.es.stopPullToRefresh()
            //            }
            self.pageNo = 0
            self.arrMyWantedServices.removeAll()
            self.tblWantedServices.reloadData()
            self.refreshRecord()
        }
    }
    
    func refreshRecord()
    {
        if self.isRefreshing == false
        {
            if self.pageNo == 0
            {
                self.getWantedList(showLoader: self.arrMyWantedServices.count==0 ?true :false)
            }
            else
            {
                self.getWantedList(showLoader: false)
            }
        }
    }
    
    //MARK: Call API
    
    func getWantedList(showLoader:Bool)
    {
        if showLoader
        {
            self.showProgressView()
        }
        
        pageNo += 1
        self.isRefreshing = true
        MyWantedServices.getMyWantedService(pageNo: pageNo, getAll: false ,numOfRecords: numOfRecords, isShowloader: showLoader, success: { (services,recordcnt) in
            
            self.recordCount = recordcnt
            self.tblWantedServices.tableFooterView?.isHidden = true
            self.tblWantedServices.tableFooterView?.frame.size = .zero
            self.isRefreshing = false
            
            if self.pageNo == 1
            {
                self.arrMyWantedServices.removeAll()
            }
            if services.count == 0
            {
                self.pageNo -= 1
            }
            else
            {
                if services.count < self.numOfRecords
                {
                    self.pageNo -= 1
                }
                
                if self.arrMyWantedServices.count == 0
                {
                    self.arrMyWantedServices.append(contentsOf: services)
                }
                else
                {
                    let responsRecord = services as NSArray
                    //let tempLocal = self.arrRequest as NSArray
                    responsRecord.enumerateObjects({ (objF, idxF, nil) in
                        let objService = objF as! MyWantedServices
                        if self.arrMyWantedServices.contains(where: {$0.requestId == objService.requestId}) {
                            //
                            print("it exists, do something")
                        } else {
                            //
                            print("item could not be found")
                            self.arrMyWantedServices.append(objService)
                        }
                        
                    })
                }
            }
            
            self.stopLoading()
            
        }) {
            self.stopLoading()
            self.tblWantedServices.tableFooterView?.isHidden = true
            self.tblWantedServices.tableFooterView?.frame.size = .zero
            
        }
    }
    
    
    
    
    func stopLoading()
    {
        self.hideProgressView()
        self.tblWantedServices.es.stopPullToRefresh()
        self.tblWantedServices.es.stopLoadingMore()
        self.tblWantedServices.reloadData()
        if self.recordCount == self.arrMyWantedServices.count {
            self.tblWantedServices.es.noticeNoMoreData()
        }
        self.lblNoDataFound.isHidden = arrMyWantedServices.count==0 ?false :true
    }
    //MARK: IBAction
    
    @IBAction func onRequestServiceClick(_ sender: Any) {
        
        let requestServiceVC = RequestServiceVC(nibName: "RequestServiceVC", bundle: nil) as RequestServiceVC
        _ = self.navigationController?.pushViewController(requestServiceVC, animated: false)
    }
    
    
    func showProgressView(){
        self.vwProgress.isHidden = false
        self.vwProgress.startAnimating()
    }
    
    func hideProgressView(){
        self.vwProgress.isHidden = true
        self.vwProgress.stopAnimating()
    }
    
    
}



extension MyWantedServiceVC : UITableViewDelegate,UITableViewDataSource
{
    //MARK: - Tableview Method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrMyWantedServices.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Utilities.isDeviceiPad() ? 102.0 : 92.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WantedServiceCell", for: indexPath) as? WantedServiceCell
        Utilities.applyShadowEffect(view: [cell!.vwBG])
        let WantedServiceData = self.arrMyWantedServices[indexPath.row]
        
        if WantedServiceData.serviceImage != nil{
            cell?.imgServiceProfile.sd_setImage(with: URL.init(string: WantedServiceData.serviceImage!), placeholderImage: UIImage(named:Constant.PlaceHolder.kPhoto), options: SDWebImageOptions.retryFailed, completed: nil)
        }else{
            cell?.imgServiceProfile.image = UIImage.init(named: Constant.PlaceHolder.kPhoto)
        }
        
        cell?.lblServiceName.text = WantedServiceData.serviceName
        cell?.lblCategory.text =  WantedServiceData.catName
        cell?.lblDescription.text = WantedServiceData.serviceDescription
        
        cell?.btnDescriptionPopup.addTarget(self, action: #selector(self.handleClick(_ :)), for: .touchUpInside)
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //        if !isRefreshing
        //        {
        //            if indexPath.row > self.arrMyWantedServices.count - 2 && self.recordCount > self.arrMyWantedServices.count{
        //
        //                let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        //                spinner.startAnimating()
        //                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
        //
        //                self.tblWantedServices.tableFooterView = spinner
        //                self.tblWantedServices.tableFooterView?.isHidden = false
        //                //refreshRecord()
        //            }
        //        }
    }
    
    @IBAction func handleClick(_ sender:UIButton){
        
        let point = sender.superview?.convert(sender.center, to: self.tblWantedServices) ?? .zero
        let indexPath = self.tblWantedServices.indexPathForRow(at: point)
        let data = self.arrMyWantedServices[(indexPath?.row)!]
        let desc = data.serviceDescription
        let heading = data.serviceName
        self.view.showSimpleDetailsPopup(topHeading: heading, infoContent: desc)
    }
    
}
