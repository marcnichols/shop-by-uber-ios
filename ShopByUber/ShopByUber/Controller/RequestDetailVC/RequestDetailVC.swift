//
//  RequestDetailVC.swift
//  The EZ List
//
//  Created by DIFFERENZ101 on 4/14/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

import UIKit

class RequestDetailVC: UIViewController {

    //MARK:- IBOutlet
    @IBOutlet weak var lblServiceName: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var vwRequestDetails: UIView!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var btnCreateService_Outlet: UIButton!
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var nslcDetailViewTop: NSLayoutConstraint!
    @IBOutlet weak var btnCancel_Outlet: UIButton!
    @IBOutlet weak var nslcBtnCancleHight: NSLayoutConstraint!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblstate: UILabel!
    @IBOutlet weak var lblWantedServiceHeader: UILabel!
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var nslcUpdateHight: NSLayoutConstraint!
    @IBOutlet weak var nslcUpdatebtnTop: NSLayoutConstraint!
    
    @IBOutlet weak var tblWantedserviceList: UITableView!
    @IBOutlet weak var nslcUpdateTableTop: NSLayoutConstraint!
    @IBOutlet weak var vwProgress: UIActivityIndicatorView!
    @IBOutlet weak var nslcTableHeight: NSLayoutConstraint!
    
    
    //MARK: Variable
    var requestId = String()
    var isFromMyWantedService = false
    
    //Passed Data
    var categotyID = String()
    var SubCatID = String()
    var image = String()
    var serviceName = String()
    var catName = String()
    var subCatName = String()
    var isServiceCreated = Bool()
    var arr_PicksListing = [UberPicks]()
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        print(requestId)
        
        if isFromMyWantedService == true {
            btnCreateService_Outlet.isHidden = true
        }
        if DataModel.sharedInstance.onFetchUserLoginType() == Constant.UserType.Buyer && isFromMyWantedService  {
            nslcDetailViewTop.constant = Utilities.isDeviceiPad() ? 30 : 20
            imgUserProfile.isHidden = true
            lblName.isHidden = true
            lblWantedServiceHeader.isHidden = true
        }else{
            imgUserProfile.isHidden = false
            lblName.isHidden = false
            lblWantedServiceHeader.isHidden = false
            nslcDetailViewTop.constant = Utilities.isDeviceiPad() ? 350 : 225
        }
        self.initialConfig()
    }
    
   // MARK: - Private Methods
    override func viewDidLayoutSubviews() {
        print("Frame - ",imgUserProfile.frame)
        Utilities.decorateView(imgUserProfile.layer, cornerRadius: self.imgUserProfile.frame.height/2, borderWidth: 5, borderColor: Constant.Color.viewShadowColor)
        
    }
    
    func initialConfig()
    {
        tblWantedserviceList.dataSource = self
        tblWantedserviceList.delegate = self
        tblWantedserviceList.register(UINib.init(nibName: "ServiceCell", bundle: nil), forCellReuseIdentifier: "ServiceCell")
        self.tblWantedserviceList.separatorStyle = .none
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        Utilities.applyShadowEffect(view: [vwRequestDetails])
        self.getWantedList(showLoader: true)
        
        
    }

    //MARK:- Action
    @IBAction func btnBack_Action(_ sender: Any) {
       self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCreateService_Click(_ sender: Any) {
        //Push to Create new service
        
        if DataModel.sharedInstance.onFetchUserLoginType() == Constant.UserType.Buyer {
        
            SwitchUser.switchUser(userId: DataModel.sharedInstance.onFetchUserId(), sessionId: DataModel.sharedInstance.onFetchSessionId(), switchTo: 2, success: { (dict) in
              
                print(dict)
                let viewStatus = UIView.init(frame: CGRect(x: 00, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: 20))
                viewStatus.backgroundColor = .black
                var frontViewController = UIViewController()
                
                if dict?["isCompanyProfileCreated"] as! Bool {
                    let sellerVC = SellerVC(nibName: "SellerVC", bundle: nil)
                    sellerVC.showAddService = true
                    sellerVC.passedRequestID = self.requestId
                    frontViewController = sellerVC
                }else {
                    let serviceProfileVC = ServiceProfileVC(nibName: "ServiceProfileVC", bundle: nil)
                    // serviceProfileVC.requestID = self.requestId
                    frontViewController = serviceProfileVC
                }
                
                QuickBloxManager.sharedInstance.checkAndLoginInQuickBlox()
                
                let rearViewController = MenuVC(nibName: "MenuVC", bundle: nil)
                
                let frontNavigationController = UINavigationController(rootViewController: frontViewController)
                frontNavigationController.isNavigationBarHidden = true
                
                let revealController = SWRevealViewController(rearViewController: rearViewController, frontViewController: frontNavigationController)
                revealController?.panGestureRecognizer().isEnabled = true
                
                let navigationController = UINavigationController.init(rootViewController: revealController!)
                navigationController.isNavigationBarHidden = true
                
                appDeletgate.window?.rootViewController = navigationController
                appDeletgate.window?.makeKeyAndVisible()
                
                let window = UIApplication.shared.keyWindow
                window?.addSubview(viewStatus)
                
        }) {
          //Cancel Click
        }
            
        }else {
            
            //My change
            
          //  appDeletgate.onSetupDashboardPage()
            
            //User type == seller, Push to AddServiceVC
            let addServiceVC = AddServiceVC(nibName: "AddServiceVC", bundle: nil) as AddServiceVC
            addServiceVC.requestID = self.requestId
            addServiceVC.isFromWantedService = true
            _ = self.navigationController?.pushViewController(addServiceVC, animated: true)
        }
    }
    
    @IBAction func btnCancle_Click(_ sender: Any) {
        
        Utilities.showAlertWithAction(title: "Are you sure?", message: "Do you want to cancel this request?", buttonTitle: "OK", buttonTitle2: "Cancel", onOKClick: {
            
            KVNProgress.show()
            MyWantedServices.cancelWantedService(requestID: self.requestId, isShowloader: true) { (dict) in
                
                KVNProgress.dismiss()
                
                Utilities.showAlertWithAction(title: "", message: (dict["Message"] as? String)!, buttonTitle: "OK", buttonTitle2: "", onOKClick: {
                    
                   // self.navigationController?.popViewController(animated: true)
                    appDeletgate.onSetupDashboardPage()
                    appDeletgate.isfromMyWantedService = true
                    
                }, onCancelClick: {
                    
                })
                
            }
            
        }) {
            //Dismiss Alert
        }
    }
    
    @IBAction func btnUpdate_click(_ sender: Any) {
        
        let vc  = RequestServiceVC(nibName: "RequestServiceVC", bundle: nil) as RequestServiceVC
        vc.isfromAllWantedService = true
        vc.selectedCategoryID = categotyID
        vc.selectedSubCatagoryID = SubCatID
        vc.passDescription = self.lblDesc.text!
        vc.passName = self.lblServiceName.text!
        vc.passRequestId = self.requestId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK:- API Call
    func getWantedList(showLoader:Bool)
    {
        MyWantedServices.getWantedDetail(requestID: requestId, isShowloader: true) { (dict) in
            
            self.lblServiceName.text = dict["serviceName"] as? String
            self.lblDesc.text = dict["serviceDescription"] as? String
            self.lblCategory.text = dict["catName"] as? String
            self.lblName.text = dict["name"] as? String
            self.lblCity.text = dict["city"] as? String
            self.lblstate.text = dict["state"] as? String
            self.categotyID = (dict["categoryId"] as? String)!
            self.SubCatID = (dict["subCategoryId"] as? String)!
            self.requestId = (dict["requestId"] as? String)!
            self.isServiceCreated = (dict["isServiceCreated"] as? Bool)!
            
            if dict["userId"] as? String == DataModel.sharedInstance.onFetchUserId() {
                self.btnCreateService_Outlet.isHidden = true
                self.nslcUpdatebtnTop.constant = 20
                self.nslcUpdateHight.constant = Utilities.isDeviceiPad() ? 60 : 45
                self.nslcBtnCancleHight.constant = Utilities.isDeviceiPad() ? 60 : 45
                self.nslcUpdateTableTop.constant = 160

            }else {
                self.btnCreateService_Outlet.isHidden = false
                self.nslcUpdateHight.constant = 0
                self.nslcBtnCancleHight.constant = 0
                self.nslcUpdateTableTop.constant = 80
            }
    
            if dict["image"] as? String != nil{
                
                self.imgUserProfile.sd_setImage(with: URL.init(string: (dict["image"] as? String)!), placeholderImage: UIImage(named:Constant.PlaceHolder.kDefaultUser), options: SDWebImageOptions.retryFailed, completed: nil)
            }else{
                self.imgUserProfile.image = UIImage.init(named: Constant.PlaceHolder.kDefaultUser)
            }
            if (DataModel.sharedInstance.onFetchUserLoginType() == Constant.UserType.Buyer) && (self.isServiceCreated == true) {
                self.getMyServices(showLoader: false)
            }
            
            KVNProgress.dismiss()
            }
        
       
    }
    
    //MARK: Call API
    func getMyServices(showLoader:Bool)
    {
        showProgressView()
        UberPicks.callGetWantedServiceListAPI(requestID : self.requestId, success: { (companyListing,recordcnt) in
            
            let responsRecord = companyListing as NSArray
            print(responsRecord)
            responsRecord.enumerateObjects({ (objF, idxF, nil) in
                let objPiks = objF as! UberPicks
                
                if self.arr_PicksListing.contains(where: {$0.id == objPiks.id}) {
                    //
                    print("it exists, do something")
                } else {
                    print("item could not be found")
                    self.arr_PicksListing.append(objPiks)
                }
                
            })
            self.tblWantedserviceList.reloadData()
            self.tblWantedserviceList.layoutIfNeeded()
            //     self.nslcTableHeight.constant = self.tblWantedServiceList.contentSize.height
            print(self.tblWantedserviceList.contentSize.height)
        }) {
            
            print("done")
        }
        hideProgressView()
    }
    
    func showProgressView(){
        self.vwProgress.isHidden = false
        self.vwProgress.startAnimating()
    }
    
    func hideProgressView(){
        self.vwProgress.isHidden = true
        self.vwProgress.stopAnimating()
    }
    
    
}

extension RequestDetailVC : UITableViewDelegate,UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr_PicksListing.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceCell", for: indexPath) as? ServiceCell
        Utilities.applyShadowEffect(view: [cell!.vwBG])
        
        let data = self.arr_PicksListing[indexPath.row]
        
        if data.imageURL != nil{
            cell?.serviceImage.sd_setImage(with: URL.init(string: data.imageURL!), placeholderImage: UIImage(named:Constant.PlaceHolder.kPhoto), options: SDWebImageOptions.retryFailed, completed: nil)
        }else{
            cell?.serviceImage.image = UIImage.init(named: Constant.PlaceHolder.kPhoto)
        }
        
        //cell?.lblTitle.text = data.companyName  // Removed Company Name
        cell?.lblTitle.text = data.comapnyScreenName
        cell?.nslcLblCatagoryTypeHight.constant = Utilities.isDeviceiPad() ? 22:18
        cell?.nslclblDescHight.constant = 0
        cell?.lblCatagoryType.text = data.categories
        // cell?.lblDescription.text = data.compDescription //Removed Description
        self.nslcTableHeight.constant = self.tblWantedserviceList.contentSize.height
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let Data = self.arr_PicksListing[indexPath.row]
        let objServiceDetailForSBUVC = ServiceDetailForSBUVC(nibName: "ServiceDetailForSBUVC", bundle: nil) as ServiceDetailForSBUVC
        objServiceDetailForSBUVC.companyID    = Data.id
        objServiceDetailForSBUVC.screenName   = Data.companyName!
        let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
        navigationController.pushViewController(objServiceDetailForSBUVC, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Utilities.isDeviceiPad() ? 92.0 : 72.0
    }
    
}
