//
//  NotificationSettingVC.swift
//  ShopByUber
//
//  Created by Chirag Kalsariya on 7/12/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

let kNotificationCell = "NotificationSettingCell"

class NotificationSettingVC: UIViewController {

    //MARK:- IBOutlet
    @IBOutlet weak var tblNotiSetting: UITableView!
    
    //MARK:- Variable
    var arrSettings = [NotificationSetting]()
    
    //MARK:- View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getApiCallData()
        tblNotiSetting.register(UINib.init(nibName: kNotificationCell, bundle: nil), forCellReuseIdentifier: kNotificationCell)
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: IBAction
    
    @IBAction func btnBack_Clicked(_ sender: UIButton) {
        
       _ = navigationController?.popViewController(animated: true)
        
    }
    
    //MARK: - ApiCall
    func getApiCallData()
    {
        arrSettings.removeAll()
        KVNProgress.show()
        var dict = [String : Any]()
        
        dict["sessionId"]    = DataModel.sharedInstance.onFetchSessionId()
        dict["userId"]       = DataModel.sharedInstance.onFetchUserId()
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kGetNotificationData, inputParams: dict, success: { (dict) in
            
            KVNProgress.dismiss()
            if dict?["Result"] as? Bool == true {
                let data = dict?["Data"] as? [String:Any]
                if data?.count != 0
                {
                    let notificatios = data?["otherNotification"] as? NSArray
                    if notificatios != nil
                    {
                        for i in notificatios!
                        {
                            let notification = NotificationSetting.init(fromDictionary: i as! [String : Any])
                            self.arrSettings.append(notification)
                        }
                        self.tblNotiSetting.reloadData()
                        self.tblNotiSetting.isHidden = false
                    }
                    
                }
            }
        }) { error in
            Utilities.showAlertView(title: "", message: error)
        }

    }
}

extension NotificationSettingVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Utilities.isDeviceiPad() ?80 :60
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {

        return 30
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: 30))
        view.backgroundColor = .clear
        let lblHeader = UILabel(frame: CGRect(x: 16, y: 5, width: 0, height: 0))
        lblHeader.font = Utilities.isDeviceiPad() ?UIFont.systemFontReguler(with: 14) :UIFont.systemFontReguler(with: 11)
        lblHeader.textColor = UIColor.gray

        lblHeader.text = "NOTIFY ME WHEN..."
        
        
        lblHeader.sizeToFit()
        view.addSubview(lblHeader)
        
        return view
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return arrSettings.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: kNotificationCell, for: indexPath) as! NotificationSettingCell
        
        let data = arrSettings[indexPath.row]
        cell.lblTitle.text = data.notifyLabel
 
        cell.swOnOff.isHidden = false
   
        if data.isEnabled == true
        {
            cell.swOnOff.isOn = true
        }
        else
        {
            cell.swOnOff.isOn = false
        }
        cell.swOnOff.addTarget(self, action: #selector(swValueChange), for: .valueChanged)
        
        return cell
    }
    
    func swValueChange(sender:UISwitch)  {
        
        let cell : NotificationSettingCell = sender.superview?.superview as! NotificationSettingCell
        
        let indexPath = tblNotiSetting.indexPath(for: cell)
        let data = arrSettings[(indexPath?.row)!]

        NotificationSetting.setSetting(notificationID: data.notifyId, isEnable: sender.isOn, completion: { (success) -> Void in
            
            if success { // this will be equal to whatever value is set in this method call
                print("true")
            } else {
                //print("false")
                self.getApiCallData()
            }
        })
        
    }
    
    
}
