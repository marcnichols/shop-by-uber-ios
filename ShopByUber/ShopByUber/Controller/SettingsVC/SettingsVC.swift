//
//  SettingsVC.swift
//  ShopByUber
//
//  Created by Administrator on 6/7/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class SettingsVC: BaseView {

    //MARK: - IBOutlets
    
    @IBOutlet var btnSignOut: UIButton!
    @IBOutlet weak var tblSettings: UITableView!
    @IBOutlet weak var lblUserName: UILabel!
    
    //MARK: - Variables
    var arrSettings = [[String]]()
    
    //MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialConfig()
        if DataModel.sharedInstance.onFetchUserLoginType() == Constant.UserType.Buyer{
            arrSettings = [["Clear Notifications","Clear Your Activity"],["About This App"],["Notifications"]]
        }else
        {
            arrSettings = [["Clear Notifications"],["About This App"],["Notifications"]]
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: IBAction


    @IBAction func btnSignOut_Clicked(_ sender: UIButton) {
        
        self.callLogoutAPI()
        
    }
    
    //MARK: Private Methods
    
    func initialConfig(){
        
        self.tblSettings.register(UINib(nibName: "SettingsMenuCell", bundle: nil), forCellReuseIdentifier: "SettingsMenuCell")
        self.tblSettings.tableFooterView = UIView()
        lblUserName.text = "Connected as " + DataModel.sharedInstance.onFetchUserName()
    }
    
    //MARK: API CAll
    
    func callLogoutAPI() {
        
        User.callLogoutAPI {
            
            self.onLogout()
        }
        
    }
    
    func onLogout() {
        
        DataModel.sharedInstance.resetUserDataWhenLogout()
        appShareManager.selectedMenuItemIndex = 0
        
        let frontViewController = LoginVC(nibName: "LoginVC", bundle: nil)
        
        let rearViewController = MenuVC(nibName: "MenuVC", bundle: nil)
        
        let frontNavigationController = UINavigationController(rootViewController: frontViewController)
        frontNavigationController.isNavigationBarHidden = true
        
        let revealController = SWRevealViewController(rearViewController: rearViewController, frontViewController: frontNavigationController)
        
        let navigationController = UINavigationController.init(rootViewController: revealController!)
        navigationController.isNavigationBarHidden = true
        
        UIApplication.shared.delegate?.window??.rootViewController = navigationController
        UIApplication.shared.delegate?.window??.makeKeyAndVisible()
        
        let viewStatus = UIView.init(frame: CGRect(x: 00, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: 20))
        viewStatus.backgroundColor = .black
        let window = UIApplication.shared.keyWindow
        window?.addSubview(viewStatus)
    }
}

extension SettingsVC : UITableViewDataSource,UITableViewDelegate
{
    //MARK: - TableView Delegate & Data Source method
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrSettings.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Utilities.isDeviceiPad() ? 75 : 44
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrSettings[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsMenuCell", for: indexPath) as! SettingsMenuCell
        cell.lblTitle.text = self.arrSettings[indexPath.section][indexPath.row]
           

        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if DataModel.sharedInstance.onFetchUserLoginType() == Constant.UserType.Buyer{
            //Buyer Settings
            if indexPath.section == 0
            {
                if indexPath.row == 0
                {
                    self.callClearNotificationDataApi()
                }
                else if indexPath.row == 1
                {
                    self.callClearYourActivityDataApi()
                }
            }
            else if indexPath.section == 1
            {
                //About this app
                self.jumpToAboutThisApp()
            }
            else if indexPath.section == 2
            {
                //Open notification setting
               self.jumpToNotificationSetting()
            }
        }
        else{   //Seller Settings
            if indexPath.section == 0
            {
                //Clear Notification
                self.callClearNotificationDataApi()
            }else if indexPath.section == 1 {
                //About This App
                self.jumpToAboutThisApp()
            }
            else if indexPath.section == 2
            {
                //Open notification setting
                self.jumpToNotificationSetting()
            }
        }
    }
    func jumpToAboutThisApp()  {
        let aboutApp = AboutThisAppVC(nibName: "AboutThisAppVC", bundle: nil)
        aboutApp.strTitle = "About This App"
        aboutApp.url = URL (string: Constant.serverAPI.URL.kAboutThisApp)
        self.revealViewController().pushFrontViewController(aboutApp, animated: true)
    }
    
    func jumpToNotificationSetting() {
        let notiSetting = NotificationSettingVC.init(nibName: "NotificationSettingVC", bundle: nil)
        self.navigationController?.pushViewController(notiSetting, animated: true)
    }
    
    func callClearNotificationDataApi()
    {
        KVNProgress.show()
        var dict = [String : Any]()
        dict["sessionId"]    = DataModel.sharedInstance.onFetchSessionId()
        dict["userId"]       = DataModel.sharedInstance.onFetchUserId()
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kClearNotification, inputParams: dict, success: { (dict) in
            
            
            if dict?["Result"] as? Bool == true {
                Singleton.sharedManager.unreadNotificationCount = 0

                KVNProgress.showSuccess(withStatus: dict?["Message"] as! String)
            }
            else
            {
                Utilities.showAlertView(title: "", message: dict?["Message"] as? String)
            }
            
        }) { error in
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
            
        }

    }
    
    func callClearYourActivityDataApi()
    {
        KVNProgress.show()
        var dict = [String : Any]()
        dict["sessionId"]    = DataModel.sharedInstance.onFetchSessionId()
        dict["userId"]       = DataModel.sharedInstance.onFetchUserId()
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kClearYourActivity, inputParams: dict, success: { (dict) in
            if dict?["Result"] as? Bool == true {
                KVNProgress.showSuccess(withStatus: dict?["Message"] as! String)
            }
            else
            {
                Utilities.showAlertView(title: "", message: dict?["Message"] as? String)
            }
            
        }) { error in
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
        }
    }
}
