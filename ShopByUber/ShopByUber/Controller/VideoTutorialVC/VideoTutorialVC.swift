//
//  VideoTutorialVC.swift
//  AwakenedMind
//
//  Created by mac on 11/22/17.
//  Copyright © 2017 Differenz System Pvt. Ltd. All rights reserved.
//

import UIKit

class VideoTutorialVC: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var videoPlayer: VIMVideoPlayerView!
    @IBOutlet weak var sliderPlayer: UISlider!
    @IBOutlet weak var lblProgressTime: UILabel!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var vwBottom: UIView!
    
    //MARK:- Variables
    var isScrubbing = false
    var isfromMenu = false
    let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
    
    //MARK:- Page Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupSlider()
        self.setupVideoPlayerView()
        self.btnSkip.layer.cornerRadius = 5
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.appDelegate.isRotate = true
        appDelegate.dismissBubble()
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        UIApplication.shared.isIdleTimerDisabled = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.appDelegate.isRotate = false
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue , forKey: "orientation")
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        UIApplication.shared.isIdleTimerDisabled = false
    }
    
    //MARK: - Device Rotation
    
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        
        self.btnSkip.isHidden = true
        self.vwBottom.isHidden = true
    }
    
    //MARK:- Private Methods
    private func setupVideoPlayerView() {
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(sender:)))
        self.videoPlayer.addGestureRecognizer(tapGesture)
        
        self.videoPlayer.delegate = self
        self.videoPlayer.player.isLooping = false
        self.videoPlayer.player.disableAirplay()
        self.videoPlayer.setVideoFillMode(AVLayerVideoGravityResizeAspect)
        
//        if let fileURL = URL(string: Constant.serverAPI.URL.kcommercial) {
//            self.videoPlayer.player.setURL(fileURL)
//        }
        if let path = Bundle.main.path(forResource: "tv_commercial", ofType: "mov")
        {
            self.videoPlayer.player.setURL(NSURL(fileURLWithPath: path) as URL!)
        }
        KVNProgress.show()
    }
    
    private func setupSlider() {
        
        self.sliderPlayer.addTarget(self, action: #selector(VideoTutorialVC.scrubbingDidStart), for: UIControlEvents.touchDown)
        self.sliderPlayer.addTarget(self, action: #selector(VideoTutorialVC.scrubbingDidChange), for: UIControlEvents.valueChanged)
        self.sliderPlayer.addTarget(self, action: #selector(VideoTutorialVC.scrubbingDidEnd), for: UIControlEvents.touchUpInside)
        self.sliderPlayer.addTarget(self, action: #selector(VideoTutorialVC.scrubbingDidEnd), for: UIControlEvents.touchUpOutside)
    }
    
    func handleTap(sender: UITapGestureRecognizer? = nil) {
        
        self.vwBottom.isHidden = !self.vwBottom.isHidden
        self.btnSkip.isHidden = !self.btnSkip.isHidden
    }
    
    //MARK:- IBAction
    @IBAction func onBtnPlayPauseTap(sender: UIButton) {
        
        if self.videoPlayer.player.isPlaying {
            sender.setImage(UIImage(named: "play"), for: .normal)
            self.videoPlayer.player.pause()
        } else {
            sender.setImage(UIImage(named: "pause"), for: .normal)
            self.videoPlayer.player.play()
        }
    }
    
    @IBAction func onBtnSkipTap(sender: UIButton) {
        
        DataModel.sharedInstance.setUserDefaultIsCommercialPlayed(isCommercialPlayed: true)
        if isfromMenu == true {
            _ = self.navigationController?.popViewController(animated: true)
        }else {
             appDeletgate.onSetupDashboardPage()
        }
    }
    
    //MARK:- Scrubbing Actions
    func scrubbingDidStart() {
        
        self.isScrubbing = true
        self.videoPlayer.player.startScrubbing()
    }
    
    func scrubbingDidChange() {
        
        guard let duration = self.videoPlayer.player.player.currentItem?.duration, self.isScrubbing == true else {
            return
        }
        let time = Float(CMTimeGetSeconds(duration)) * self.sliderPlayer.value
        self.videoPlayer.player.scrub(time)
    }
    
    func scrubbingDidEnd() {
        
        self.videoPlayer.player.stopScrubbing()
        self.isScrubbing = false
    }
}
extension VideoTutorialVC: VIMVideoPlayerViewDelegate {
    
    func videoPlayerViewIsReady(toPlayVideo videoPlayerView: VIMVideoPlayerView!) {
        
        self.btnPlay.setImage(UIImage(named: "pause"), for: .normal)
        self.videoPlayer.player.play()
        self.videoPlayer.player.seek(toTime: Float(0))
        KVNProgress.dismiss()
    }
    
    func videoPlayerView(_ videoPlayerView: VIMVideoPlayerView!, timeDidChange cmTime: CMTime) {
        
        guard let duration = self.videoPlayer.player.player.currentItem?.duration, self.isScrubbing == false else {
            return
        }
        let durationInSeconds = CGFloat(CMTimeGetSeconds(duration))
        let timeInSeconds = CGFloat(CMTimeGetSeconds(cmTime))
        
        
        if timeInSeconds >= 0 {
            lblProgressTime.text = Utilities.getDuration(second: timeInSeconds)
        }
        self.sliderPlayer.value = Float(timeInSeconds / durationInSeconds)
    }
    
    func videoPlayerView(_ videoPlayerView: VIMVideoPlayerView!, didFailWithError error: Error!) {
        
        KVNProgress.dismiss()
        Utilities.showAlertView(title: "ALERT", message: error.localizedDescription)
    }
    
    func videoPlayerViewDidReachEnd(_ videoPlayerView: VIMVideoPlayerView!) {
        self.btnPlay.setImage(UIImage(named: "play"), for: .normal)
        self.onBtnSkipTap(sender: self.btnSkip)
    }
}
