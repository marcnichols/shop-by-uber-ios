//
//  PayPalWebVC.swift
//  ShopByUber
//
//  Created by Administrator on 10/5/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class PayPalWebVC: UIViewController,UIWebViewDelegate {

    @IBOutlet weak var webView: UIWebView!
    
    var redirectURL : String = ""
    var delegate: PaymentSuccessDelegate?
    var urlPath : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
        // Do any additional setup after loading the view.
        webView.isHidden = true
        webView.loadRequest(URLRequest(url: URL(string: redirectURL)!))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        
        KVNProgress.show()
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        webView.isHidden = false
        // dismiss progress bar
        KVNProgress.dismiss()
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
    
        urlPath = request.url?.absoluteString
        
        print(urlPath)
            
        if ( urlPath == "http://192.168.1.140:81/Shopbyuber/home/ExecutePayment/true" ) {
            
            delegate?.PaymentSuccess()
            self.navigationController?.popViewController(animated: true)
            
        }
        if  ( urlPath == "http://192.168.1.140:81/Shopbyuber/home/ExecutePayment/false" ) {
            
            self.navigationController?.popViewController(animated: true)
        }
        return true;
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func btnBack_Tapped(_ sender: Any) {
    
        if (urlPath?.contains("pageState=done"))! {
            
            delegate?.PaymentSuccess()
            self.navigationController?.popViewController(animated: true)
        }else{
            
            self.showAlertWithButtonAction(title: "", message: Constant.Message.kPaymetnExitMessage, buttonTitle: "YES", buttonTitle2: "NO", onOKClick: {
                
                self.navigationController?.popViewController(animated: true)
                
            }, onCancelClick: {
                
            })
            
        }
        
    }
}

protocol PaymentSuccessDelegate{
    func PaymentSuccess()
}
