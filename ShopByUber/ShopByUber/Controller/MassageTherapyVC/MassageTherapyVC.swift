//
//  MassageTherapyVC.swift
//  ShopByUber
//
//  Created by Differenz215 on 6/21/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class MassageTherapyVC: BaseView {
    
    //MARK:- IBOutlet
    @IBOutlet var tblMassge:      UITableView!
    @IBOutlet var vw_Progress:    UIActivityIndicatorView!
    @IBOutlet var lblNoDataFound: UILabel!
    
    @IBOutlet weak var lblTitle: UILabel!
    //MARK: Variables
    var recordCount = 0
    var pageNo = 0
    var isFromInfinite : Bool = false
    var numOfRecords = 10
    var currentZipCode : String = ""
    var isRefreshing:Bool = false
    var categoryId: String?
    var subCategoryID : String?
    var strTitle : String = ""
    var arr_Massgetherapy = [CompanyServiceList]()
    
    //MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialConfig()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.mapView.delegate = self
        if arr_Massgetherapy.count == 0
        {
            self.getCategories(showLoader: true)
            
            self.setPagination()
        }
    }
    
    //MARK:- Private Methods
    
    func initialConfig(){
        
        lblTitle.text = strTitle
        self.tblMassge.register(UINib(nibName: "MassageTherapyCell", bundle: nil), forCellReuseIdentifier: "MassageTherapyCell")
        self.setCurretnLocationMarker()
    }
    
    func setPagination() {
        
        tblMassge.es.addPullToRefresh {
            self.pageNo = 0
            self.arr_Massgetherapy.removeAll()
            self.tblMassge.reloadData()
            self.refreshRecord()
        }
    }
    
    func refreshRecord()
    {
        if self.isRefreshing == false
        {
            if self.pageNo == 0
            {
                self.getCategories(showLoader: self.arr_Massgetherapy.count==0 ?true :false)
            }
            else
            {
                self.getCategories(showLoader: false)
            }
        }
    }
    
    func stopLoading()
    {
        self.hideProgressView()
        self.tblMassge.es.stopPullToRefresh()
        self.tblMassge.es.stopLoadingMore()
        self.tblMassge.reloadData()
        self.lblNoDataFound.isHidden = arr_Massgetherapy.count==0 ?false :true
    }
    
    func setLocationMarker(data:CompanyServiceList){
        //        print(lat ?? 0)
        //        print(lon ?? 0)
        let locationMarker = GMSMarker()
        locationMarker.position = CLLocationCoordinate2D(latitude: data.latitude ?? 0.0, longitude: data.longitude ?? 0.0)
        locationMarker.icon = UIImage(named: "ic_marker")
        // locationMarker.title = pincode
        
        locationMarker.appearAnimation = .pop
        locationMarker.map = self.mapView
        locationMarker.userData = data

    }
    //MARK: - MapView Delegate 
    override func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        print("try")
        
        self.userPopup = Bundle.main.loadNibNamed("UserInfoPupup", owner: self, options: nil)?[0] as! UserInfoPopup
        self.userPopup.frame = self.view.bounds
        self.userPopup.delegate = self
        let data  = marker.userData as! CompanyServiceList
        self.userPopup.lblUserName.text = data.comapnyScreenName
        self.userPopup.companyID = data.companyId
        self.userPopup.txtCompanyDetails.text = data.serviceDescription
        if data.companyProfileImage != nil{
            self.userPopup.imageProfile.sd_setImage(with: URL.init(string: (data.companyProfileImage)!), placeholderImage: nil, options: SDWebImageOptions.retryFailed, completed:{ (image, err, nil, url) in
                if image == nil
                {
                    self.userPopup.imgPlaceHolder.isHidden = false
                }
            })
        }
        else
        {
            self.userPopup.imgPlaceHolder.isHidden = false
        }

        self.userPopup.txtCompanyDetails.textContainerInset = UIEdgeInsets.zero
        
        
        //for chage textview height on graterthan 100 height in compny detail
        let lblTemp = UILabel.init(frame: CGRect(x: 0, y: 0, width: self.userPopup.viewPopup.frame.size.width - 20, height: 21))
        lblTemp.numberOfLines = 0
        lblTemp.font = self.userPopup.txtCompanyDetails.font
        lblTemp.text = data.serviceDescription
        lblTemp.sizeToFit()
        
        if(lblTemp.frame.size.height > (Utilities.isDeviceiPad() ? 150 : 100))
        {
            self.userPopup.constarinttextViewHeight.constant = (Utilities.isDeviceiPad() ? 150 : 100);
        }
        else
        {
            self.userPopup.constarinttextViewHeight.constant = lblTemp.frame.size.height;
        }
        self.view.addSubview(self.userPopup)
        
        return true
    }
    //MARK: -  API Call
    func getCategories(showLoader:Bool)
    {
        if showLoader
        {
            self.showProgressView()
        }
        pageNo += 1
        
        let dict = getRequestParamsForGetCategories(pageNo: pageNo, numOfRecords: numOfRecords)
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kGetCategoryVendors, inputParams: dict, success: { (dict) in
            self.tblMassge.tableFooterView?.isHidden = true
            self.tblMassge.tableFooterView?.frame.size = .zero
            
            if dict?["Result"] as? Bool == true {
                let recordCount = dict?["recordCount"] as? Int
                self.recordCount = recordCount ?? 0
                let data = (dict?["Data"] as? [String:Any])?["companyListing"] as? NSArray
                if self.pageNo == 1
                {
                    self.arr_Massgetherapy.removeAll()
                }
                if data != nil{
                    
                    if data!.count > 0{
                        for companyListing in data!
                        {
                            let objcompanyListing = CompanyServiceList.init(dict: companyListing as! [String : Any])
                            self.setLocationMarker(data: objcompanyListing)
                            self.arr_Massgetherapy.append(objcompanyListing)
                        }
                    }
                }
            }
            self.stopLoading()
        }) { error in
            
            self.stopLoading()
            
        }
    
    }

    func getRequestParamsForGetCategories(pageNo: Int?,numOfRecords:Int?) -> [String : Any]  {
        
        
        var dict = [String : Any]()
        
        dict["sessionId"]     = DataModel.sharedInstance.onFetchSessionId()
        dict["userId"]        = DataModel.sharedInstance.onFetchUserId()
        dict["pageNo"]        = pageNo
        dict["numOfRecords"]  = numOfRecords
        dict["zipcode"]       = currentZipCode
        dict["categoryId"]    = categoryId
        dict["subCategoryId"] = subCategoryID
        
        return dict
    }
    
    //MARK: Custom Methods
    
    func showProgressView(){
        self.vw_Progress.isHidden = false
        self.vw_Progress.startAnimating()
    }
    
    func hideProgressView(){
        self.vw_Progress.isHidden = true
        self.vw_Progress.stopAnimating()
    }
}

extension MassageTherapyVC : UITableViewDataSource,UITableViewDelegate
{
    //MARK: TableView Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arr_Massgetherapy.count
    }
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        if !isRefreshing
//        {
//            if indexPath.row > self.arr_Massgetherapy.count - 2 && self.recordCount > self.arr_Massgetherapy.count{
//                
//                let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
//                spinner.startAnimating()
//                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
//                
//                self.tblMassge.tableFooterView = spinner
//                self.tblMassge.tableFooterView?.isHidden = false
//                refreshRecord()
//            }
//        }
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MassageTherapyCell", for: indexPath) as! MassageTherapyCell
        Utilities.applyShadowEffect(view: [cell.vwBG])
        
        let serviceListData = self.arr_Massgetherapy[indexPath.row]
        
        if serviceListData.companyProfileImage != nil{
            cell.imgMassage.sd_setImage(with: URL.init(string: serviceListData.companyProfileImage!), placeholderImage: UIImage(named:"placeHolder"), options: SDWebImageOptions.retryFailed, completed: nil)
        }else{
            cell.imgMassage.image = UIImage.init(named: "placeHolder")
        }
        
        cell.lblHeading.text = serviceListData.comapnyScreenName
//        cell.lblMassage.text = serviceListData.companyServiceDesc
        cell.lblStatus.isHidden = true
        cell.lblMassage.isHidden = true
        let yConstraint = NSLayoutConstraint(item: cell.lblHeading, attribute: .centerY, relatedBy: .equal, toItem: cell, attribute: .centerY, multiplier: 1, constant: -5)
        
        cell.addConstraint(yConstraint)
        if serviceListData.isAvailable!
        {
            cell.lblStatus.text = "Available Now"
            cell.lblStatus.textColor = Constant.Color.serviceAvailableColor
        }
        else
        {
            cell.lblStatus.text = "Available by Appointment"
            cell.lblStatus.textColor = Constant.Color.serviceAvailableByAppColor
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = self.arr_Massgetherapy[indexPath.row]
        let objMassageTherapyDetailsVC = MassageTherapyDetailsVC(nibName: "MassageTherapyDetailsVC", bundle: nil) as MassageTherapyDetailsVC
        
        objMassageTherapyDetailsVC.companyID        = data.companyId
        objMassageTherapyDetailsVC.categoryID       = data.categoryId
        objMassageTherapyDetailsVC.subCatagoryID    = data.subCatagoryID
       
        self.navigationController?.pushViewController(objMassageTherapyDetailsVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       // return Utilities.isDeviceiPad() ? 92.0 : 72.0
        return Utilities.isDeviceiPad() ? 82.0 : 62.0
    }

}
