//
//  ReviewsVC.swift
//  ShopByUber
//
//  Created by Differenz215 on 7/11/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit
import Cosmos
class ReviewsVC: BaseView {
    
    //MARK: - IBOutlet
    
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var lblUserProfileName: UILabel!
    @IBOutlet weak var cosmos_Rating: CosmosView!
    @IBOutlet weak var txtFeedback: UITextView!
    @IBOutlet weak var vw_feedbackcontainner: UIView!
    
    @IBOutlet weak var btnSubmit: UIButton!
    //MARK: - Variables
    var reviewsData: Reviews!
    var requestDetails:OrderDetail!

    //MARK: - View Life Cycle Method
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initialConfig()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Private Method
    func initialConfig(){
        
        Utilities.applyShadowEffect(view: [self.vw_feedbackcontainner])
        self.cosmos_Rating.didFinishTouchingCosmos = didFinishTouchingCosmos
        self.callGetRatingsDataApi()
    }
    
    func setReviewData()  {
        if self.reviewsData?.profileImage != nil{
            self.imgUserProfile.sd_setImage(with: URL.init(string: (self.reviewsData?.profileImage)!), placeholderImage: #imageLiteral(resourceName: "defaultUser"), options: SDWebImageOptions.retryFailed, completed:{ (image, err, nil, url) in
                if image != nil
                {
                    self.imgUserProfile.image = image
                    self.imgUserProfile.contentMode = .scaleAspectFill
                    self.imgUserProfile.clipsToBounds = true

                }
            })
        }
        
        Utilities.decorateView(imgUserProfile.layer, cornerRadius: self.imgUserProfile.frame.height/2, borderWidth: 5, borderColor: Utilities.colorWithHexString("C5DCDF"))
        self.lblUserProfileName.text = self.reviewsData?.name
        self.cosmos_Rating.rating    = (self.reviewsData?.rating)!
        self.txtFeedback.text        = self.reviewsData?.feedback
        if (self.reviewsData?.rating)! > 0
        {
            self.btnSubmit.backgroundColor = UIColor.black  // Constant.Color.kButtonColor
            self.btnSubmit.isEnabled = true
        }
        else
        {
            self.btnSubmit.backgroundColor = UIColor.black.withAlphaComponent(0.5) //Constant.Color.kButtonColor.withAlphaComponent(0.5)
            self.btnSubmit.isEnabled = false
        }
    }
    
    //MARK: - IBAction Method
    @IBAction func btnSubmit_Clicked(_ sender: Any) {
        if self.cosmos_Rating.rating > 0
        {
            self.callGiveFeedBackApi()
        }
    }
    
    private func didFinishTouchingCosmos(_ rating: Double) {
        if rating > 0
        {
            self.btnSubmit.backgroundColor = UIColor.black //Constant.Color.kButtonColor
            self.btnSubmit.isEnabled = true
        }
        else
        {
            self.btnSubmit.backgroundColor = UIColor.black.withAlphaComponent(0.5) // Constant.Color.kButtonColor.withAlphaComponent(0.5)
            self.btnSubmit.isEnabled = false
        }
    }
    
    //MARK: - API Call
    func callGetRatingsDataApi()
    {
        KVNProgress.show()
        var dict = [String : Any]()
        
        dict["sessionId"]    = DataModel.sharedInstance.onFetchSessionId()
        dict["userId"]       = DataModel.sharedInstance.onFetchUserId()
        dict["serviceId"]    = self.requestDetails.serviceId
        dict["requestId"]    = self.requestDetails.requestId
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kGetRatingScreenData, inputParams: dict, success: { (dict) in
            
            KVNProgress.dismiss()
            if dict?["Result"] as? Bool == true {
                let data = dict?["Data"] as? [String:Any]
                if data?.count != 0
                {
                    self.reviewsData = Reviews.init(fromDictionary: data!)
                    self.setReviewData()
                }
            }
        }) { error in
            KVNProgress.dismiss()
            
            Utilities.showAlertView(title: "", message: error)
            
        }
    }
    
    func callGiveFeedBackApi(){
        
        KVNProgress.show()
        var dict = [String : Any]()
        
        dict["sessionId"]    = DataModel.sharedInstance.onFetchSessionId()
        dict["userId"]       = DataModel.sharedInstance.onFetchUserId()
        dict["serviceId"]    = self.reviewsData.serviceId
        dict["requestId"]    = self.reviewsData.requestId
        dict["companyId"]    = self.reviewsData.companyId
        dict["rating"]       = self.cosmos_Rating.rating
        dict["feedback"]     = self.txtFeedback.text

        APIManager.callAPI(url: Constant.serverAPI.URL.kGiveRatingFeedbackService, inputParams: dict, success: { (dict) in
            
            KVNProgress.dismiss()
            if dict?["Result"] as? Bool == true {
                Utilities.showAlertWithAction(title: "", message: (dict?["Message"] as? String)!, buttonTitle: "OK", buttonTitle2: "", onOKClick: {
                    Singleton.sharedManager.isFromactivity = .ServiceDetails
                    self.navigationController?.popToRootViewController(animated: true)
                    
                    
                }, onCancelClick: {
                    
                })
                
            }
            }) { error in
            KVNProgress.dismiss()
            
            Utilities.showAlertView(title: "", message: error)
            
        }
    }
}
