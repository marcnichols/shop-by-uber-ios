//
//  MassageTherapyDetailsVC.swift
//  ShopByUber
//
//  Created by Differenz215 on 6/21/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit
import Cosmos


class MassageTherapyDetailsVC: BaseView, UITableViewDelegate,UITableViewDataSource,ServiceDetailsCellDelegate,UINavigationControllerDelegate,GMSAutocompleteViewControllerDelegate{
    
    //MARK:- IBOutlet
    @IBOutlet weak var tblReviews: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    
    //Company Header Outlets
    @IBOutlet weak var vw_ServiceHeader: UIView!
    @IBOutlet weak var imgHeader: UIImageView!
    @IBOutlet weak var lblHeaderCompanyTitle: UILabel!
    @IBOutlet weak var lblHeaderDesc: UILabel!
    @IBOutlet weak var lblHeaderAvailabilityStatus: UILabel!
    @IBOutlet weak var btnHeaderFavourite: UIButton!
    @IBOutlet var lblVendorAddress: UILabel!    //Vendor address
    
    //Reviews Outlets
    @IBOutlet weak var vw_Reviews: UIView!
    @IBOutlet weak var cosmos_Ratings: CosmosView!
    @IBOutlet weak var nsLayoutServiceTable: NSLayoutConstraint!
    @IBOutlet weak var nslayouttblReviewHeight: NSLayoutConstraint!
    @IBOutlet weak var nslayoutReviewViewHeight: NSLayoutConstraint!
    @IBOutlet weak var tblService: UITableView!
    
    @IBOutlet weak var nslcViewServiceHeaderTop: NSLayoutConstraint!
    
    
    //MARK: - Variables
    var serviceData: CompanyServiceList?
    var companyService = [CompanyService]()
    var arr_Reviews = [ReviewsModel]()
    var categoryID: String?
    var subCatagoryID: String?
    var serviceId: String?
    var requestId: String?
    
    var companyID : String?
    var companyDesc : String?
    var indexpathIndex: Int?
    var addressData = [[String:Any]]()
    let notify = UILocalNotification()
    var servicesMultiple : Int?
    var isFromNotification = false
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialConfig()
        let notificationName = Notification.Name("MassageTherapyDetailsVC")
        NotificationCenter.default.addObserver(self, selector: #selector(initialConfig), name: notificationName, object: nil)
       
//        if #available(iOS 10.0, *) {
//            UNUserNotificationCenter.current().requestAuthorization(options: [.alert,.badge,.sound])
//            { (success, error) in
//                if success {
//                    print("Permission Granted")
//
//                } else {
//                    print("There was a problem!")
//
//                }
//            }
//        } else {
//            // Fallback on earlier versions
//            let settings = UIUserNotificationSettings(types: [.alert,.badge,.sound], categories: nil)
//            UIApplication.shared.registerUserNotificationSettings(settings)
//
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        Singleton.sharedManager.currentView = self
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        Singleton.sharedManager.currentView = nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillLayoutSubviews() {
        self.nsLayoutServiceTable.constant = self.tblService.contentSize.height
        
        self.nslayoutReviewViewHeight.constant = 99 + self.tblReviews.contentSize.height
        self.view.layoutIfNeeded()
    }
    override func viewDidLayoutSubviews() {
        self.nsLayoutServiceTable.constant = self.tblService.contentSize.height
        self.nslayoutReviewViewHeight.constant = 99 + self.tblReviews.contentSize.height
        
        self.view.layoutIfNeeded()

        
    }
    
    
    //MARK:- Private Methods
    func initialConfig(){
        
        self.tblReviews.register(UINib(nibName: "ReviewCell", bundle: nil), forCellReuseIdentifier: "ReviewCell")
        self.tblService.register(UINib(nibName: "ServiceDetailsCell", bundle: nil), forCellReuseIdentifier: "ServiceDetailsCell")
        tblReviews.rowHeight = UITableViewAutomaticDimension
        self.callGetMassgeTherapyDetailsAPI()
        
    }
    
    func setHeaderData()
    {
        if (serviceData?.isAvailable)!
        {
            //self.lblHeaderAvailabilityStatus.text = "Available Now"
            //self.lblHeaderAvailabilityStatus.textColor = Utilities.colorWithHexString("#FA8606")
        }
        else
        {
            //self.lblHeaderAvailabilityStatus.text = "Available by Appointment"
            //self.lblHeaderAvailabilityStatus.textColor = Utilities.colorWithHexString("#29A036")
            
        }
        vw_ServiceHeader.isHidden = false
        if self.serviceData?.companyProfileImage != nil{
            self.imgHeader.sd_setImage(with: URL.init(string: (self.serviceData?.companyProfileImage)!), placeholderImage: UIImage(named:"placeHolder"), options: SDWebImageOptions.retryFailed, completed: nil)
        }
        self.lblHeaderCompanyTitle.text = serviceData?.comapnyScreenName
        self.lblHeaderDesc.text = serviceData?.companyServiceDesc
        if serviceData?.companyLocation == "" {
            self.nslcViewServiceHeaderTop.constant = Utilities.isDeviceiPad() ? 30 : 20
        }else {
            self.lblVendorAddress.text = serviceData?.companyLocation.uppercased()
            self.nslcViewServiceHeaderTop.constant = Utilities.isDeviceiPad() ? 60 : 50
        }
        
        
        if serviceData?.isFavourite == false
        {
            self.btnHeaderFavourite.setImage(#imageLiteral(resourceName: "unfavorited"), for: .normal)
        }
        else
        {
            self.btnHeaderFavourite.setImage(#imageLiteral(resourceName: "favorited"), for: .normal)
        }
    }
    
    //MARK: - Service Detail Delegate
    func ServiceDetailCellDelegate(cell: ServiceDetailsCell, buttonType: UIButton) {
        
        if(buttonType == cell.btnServiceFavourite )
        {
            KVNProgress.show()
            let service = self.companyService[cell.btnServiceFavourite.tag]
            var dict = [String : Any]()
            dict["sessionId"]    = DataModel.sharedInstance.onFetchSessionId()
            dict["userId"]       = DataModel.sharedInstance.onFetchUserId()
            dict["serviceId"]    = service.serviceId
            
            APIManager.callAPI(url: Constant.serverAPI.URL.kAddToFavoriteService, inputParams: dict, success: { (dict) in
                
                
                if dict?["Result"] as? Bool == true {
                    
                    if service.isFavourite == false
                    {
                        service.isFavourite = true
                        cell.btnServiceFavourite.setImage(#imageLiteral(resourceName: "favorited"), for: .normal)
                        
                    }
                    else
                    {
                        service.isFavourite = false
                        cell.btnServiceFavourite.setImage(#imageLiteral(resourceName: "unfavorited"), for: .normal)
                    }
                    KVNProgress.showSuccess(withStatus: dict?["Message"] as! String)
                }
                
            }) { error in
                KVNProgress.dismiss()
                Utilities.showAlertView(title: "", message: error)
                
            }
            
        }
        if(buttonType == cell.btnInfo)
        {
            let service = self.serviceData
            self.view.showSimpleDetailsPopup(topHeading: service?.screenName, infoContent: service?.companyDescription)
        }
    }
    
    //MARK: - Send Request Book
    func ServiceDetailCellWithDataDelegate(cell:ServiceDetailsCell,buttonType:UIButton,arrTimeSlot: TimeSlot)
    {
        if(buttonType == cell.btnSendBookRequest )
        {
            let addressDict = self.addressData[cell.tag]
            if (addressDict["address"] as? String) == ""
            {
                Utilities.showAlertView(title: "", message: Constant.ValidationMessage.kAddress)
            }
            else
            {
                KVNProgress.show()
                let service = self.companyService[cell.tag]
                var dict = [String : Any]()
                dict["sessionId"]            = DataModel.sharedInstance.onFetchSessionId()
                dict["userId"]               = DataModel.sharedInstance.onFetchUserId()
                dict["serviceId"]            = service.serviceId
                dict["appointmentTimeId"]    = [arrTimeSlot.timeslotId]
                dict["status"]               = 0
                dict["requestFee"]           = service.flatFee
                dict["userInfo"]             = self.addressData[cell.tag]
                dict["requestId"]            = self.requestId ?? ""
                
                APIManager.callAPI(url: Constant.serverAPI.URL.kSendRequestToBook, inputParams: dict, success: { (dict) in
                    
                    if dict?["Result"] as? Bool == true {
                   
                        self.showLocalNotification()
                        Utilities.showAlertWithAction(title: "", message: (dict?["Message"] as? String)!, buttonTitle: "OK", buttonTitle2: "", onOKClick: {

                            Singleton.sharedManager.isFromactivity = .ServiceDetails
                            if self.segmentControl != nil
                            {
                                self.segmentControl.selectedIndex = 3
                            }
                            self.navigationController?.popToRootViewController(animated: true)
                            appDeletgate.dismissBubble()
                        }, onCancelClick: {

                        })
                    }
                    KVNProgress.dismiss()
                }) { error in
                    KVNProgress.dismiss()
                    
                    Utilities.showAlertView(title: "", message: error)
                    
                }
            }
        }
        
    }
    
    func ServiceDetailCellBookNowDelegate(cell: ServiceDetailsCell, buttonType: UIButton) {
        if(buttonType == cell.btnBookNow )
        {
            
            let addressDict = self.addressData[cell.tag]
            if (addressDict["address"] as? String) == ""
            {
                Utilities.showAlertView(title: "", message: Constant.ValidationMessage.kAddress)
            }
            else
            {
                
            
            let service = self.companyService[cell.tag]
                
                    
                    let rootView = UIApplication.shared.delegate?.window??.rootViewController as? UINavigationController
                    let topView = UIApplication.topViewController(base: rootView)
                    let alert = UIAlertController(title: "Are you Sure?", message: "Would you like the Service Provider to come to your location now?", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alert.addAction(UIAlertAction (title: "Yes", style: .default, handler: { (alert) in
                        
                    KVNProgress.show()
                    
                    var dict = [String : Any]()
                    dict["sessionId"]            = DataModel.sharedInstance.onFetchSessionId()
                    dict["userId"]               = DataModel.sharedInstance.onFetchUserId()
                    dict["serviceId"]            = service.serviceId
                    dict["appointmentTimeId"]    = [""]
                    dict["status"]               = 0
                    dict["requestFee"]           = service.flatFee
                    dict["userInfo"]             = self.addressData[cell.tag]
                    dict["requestId"]            = self.requestId ?? ""
                        
                    APIManager.callAPI(url: Constant.serverAPI.URL.kSendRequestToBook, inputParams: dict, success: { (dict) in
                        
                        if dict?["Result"] as? Bool == true {
                            
                            self.showLocalNotification() //Show local notification
                            
                            Utilities.showAlertWithAction(title: "", message: (dict?["Message"] as? String)!, buttonTitle: "OK", buttonTitle2: "", onOKClick: {
                                Singleton.sharedManager.isFromactivity = .ServiceDetails
                                if self.segmentControl != nil
                                {
                                    self.segmentControl.selectedIndex = 3
                                }
                                self.navigationController?.popToRootViewController(animated: true)
                                appDeletgate.dismissBubble()
                                
                            }, onCancelClick: {
                                
                            })
                        }
                        else {
                            
                            Utilities.showAlertWithAction(title: "", message: (dict?["Message"] as? String)!, buttonTitle: "OK", buttonTitle2: "", onOKClick: {
                                

                                
                            }, onCancelClick: {
                                
                            })
                            
                        }
  
                        KVNProgress.dismiss()
                    }) { error in
                        KVNProgress.dismiss()
                        
                        Utilities.showAlertView(title: "", message: error)
                        
                    }
                    
                    }))
                    alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
                    topView?.present(alert, animated: true, completion: nil)

            }
            
        }
    }
    
    func showLocalNotification() {
        //set title and body
        notify.alertTitle = Constant.appTitle.strTitle /*"The EZList"*/
        notify.fireDate = NSDate(timeIntervalSinceNow: 1) as Date
        notify.alertBody = "Your appointment request has been sent to the vendor."
        notify.soundName = UILocalNotificationDefaultSoundName
        notify.category = "request"
        UIApplication.shared.scheduleLocalNotification(notify)
    }
    
    //MARK: - Add Location
    func segmentForLocationValueChange(cell: ServiceDetailsCell, segment: UISegmentedControl) {
        
        if segment.selectedSegmentIndex == 0
        {
            let attRequest = NSMutableAttributedString(string: DataModel.sharedInstance.onFetchAddressLines())
            cell.lblLocation.attributedText = attRequest
            let dict = ["address":DataModel.sharedInstance.onFetchAddressLines(),"latitude":DataModel.sharedInstance.onFetchLatitude(),"longitude":DataModel.sharedInstance.onFetchLongtitude()] as [String : Any]
            self.addressData[segment.tag] = dict
            
        }
        else
        {
            self.indexpathIndex = segment.tag
            let autocompleteController = GMSAutocompleteViewController()
            autocompleteController.delegate = self
            present(autocompleteController, animated: true, completion: nil)
        }
    }
    
    
    
    //MARK: - Auto Complete Google Delegates
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        if let addressLines = place.addressComponents {
            // Populate all of the address fields we can find.
            
            for field in addressLines {
                
                switch field.type {
                case kGMSPlaceTypePostalCode:
                    //  self.zipcode = field.name
                    break
                default:
                    //print("Type: \(field.type), Name: \(field.name)")
                    print("")
                }
            }
        }
        
        let attRequest = NSMutableAttributedString(string: place.formattedAddress! )
        let cell = tblService.cellForRow(at: IndexPath.init(row: self.indexpathIndex!, section: 0)) as! ServiceDetailsCell
        let dict = ["address":place.formattedAddress!,"latitude":place.coordinate.latitude,"longitude":place.coordinate.longitude] as [String : Any]
        self.addressData[self.indexpathIndex!] = dict
        cell.lblLocation.attributedText = attRequest
        dismiss(animated: true, completion: nil)
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController)
    {
        viewController.dismiss(animated: true, completion: nil)
        let cell = tblService.cellForRow(at: IndexPath.init(row: self.indexpathIndex!, section: 0)) as! ServiceDetailsCell
        cell.segmentLocation.selectedSegmentIndex = 0
    }
    
     //MARK:  - TableView Methods
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == self.tblService
        {
            let service = self.companyService[indexPath.row]
            if service.serviceAvailable!
            {
                if service.availableTimeSlot.count != 0
                {
                    return  Utilities.isDeviceiPad() ? /*815 895 */1140 : 820
                }
                else
                {
                    return  Utilities.isDeviceiPad() ? /*815 730*/1080 : 710
                }
                
            }
            else
            {
                return Utilities.isDeviceiPad() ? /*755 855*/ 1100 : 770
            }
        }
        else
        {
            let font = UIFont.systemFontLight(with: 15)
            let review = self.arr_Reviews[indexPath.row]
            let serviceName = Utilities.heigth(from: review.Feedback!, with: font, width:self.view.frame.size.width - 40)
            return 60 + serviceName
        }

    }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == self.tblService
        {
            return UITableViewAutomaticDimension
        }
        else
        {
            let font = UIFont.systemFontLight(with: 15)
            let review = self.arr_Reviews[indexPath.row]
            let serviceName = Utilities.heigth(from: review.Feedback!, with: font, width:self.view.frame.size.width - 40)
            return 60 + serviceName
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tblService
        {
            return self.companyService.count
        }
        else
        {
            return self.arr_Reviews.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->
        UITableViewCell {
            
            if tableView == self.tblService
            {
     
                let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceDetailsCell", for: indexPath) as! ServiceDetailsCell
                let service = self.companyService[indexPath.row]
                if service.serviceImage != nil{
                    cell.imgServiceHead.sd_setImage(with: URL.init(string: (service.serviceImage)!), placeholderImage: UIImage(named:""), options: SDWebImageOptions.retryFailed, completed:{ (image, err, nil, url) in
                        
                    })
                }
                
                cell.lblOfferDetails.text = service.serviceDetail
                cell.lblOfferDetails.sizeToFit()
                Utilities.applyShadowEffect(view: [cell.vwOfferContent])
                
                //Service Headview Height as Content
                cell.lblServiceTitle.text = service.screenName
                cell.lblPriceService.text = "$ " + service.flatFee!
                if service.isFavourite == false
                {
                    cell.btnServiceFavourite.setImage(#imageLiteral(resourceName: "unfavorited"), for: .normal)
                }
                else
                {
                    cell.btnServiceFavourite.setImage(#imageLiteral(resourceName: "favorited"), for: .normal)
                }
                
                cell.serviceAvailable = service.serviceAvailable!   //ASSIGNING BOOLEAN VALUE TO CELL VARIABLE
                
                cell.btnServiceFavourite.tag = indexPath.row
                cell.btnInfo.tag = indexPath.row
                cell.segmentLocation.tag = indexPath.row
                cell.delegate = self
                
                //Vendors Locatio
//                let vendoraddress = service.companyAddress
//                var vendorAdd = NSMutableAttributedString(string: "Vendor's Location : " + vendoraddress! as String)
//                vendorAdd = Utilities.decorateText(attributedString: vendorAdd, decorateString: "Vendor's Location : ", decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: Utilities.isDeviceiPad() ? 14 : 16),NSForegroundColorAttributeName:Constant.Color.kSegmentSelectColor])
//                cell.lblVendorLocation.attributedText = vendorAdd
//                cell.lblVendorLocation.sizeToFit()
                
                //Buyer Address
                let address = self.addressData[indexPath.row]
                let attRequest = NSMutableAttributedString(string:(address["address"] as! String))
                cell.lblLocation.attributedText = attRequest
                
                if service.availableTimeSlot.count == 0 && service.serviceAvailable == false {
                    
                    cell.constraintTblSlotHeight.constant = 80
                    cell.lblAvailibility.text = "Available by Appointment"
                    cell.lblAvailibility.textColor = Utilities.colorWithHexString("#29A036")
                    cell.constraintBtnBookNow.constant = 0.0
                    cell.btnBookNow.isHidden = true
                    cell.nslcBtnRequestTop.constant = Utilities.isDeviceiPad() ? 20 :10
                    cell.nslcSegmentTop.constant = Utilities.isDeviceiPad() ? 120 : 100
                }
                else
                {
                if service.availableTimeSlot.count != 0
                {
                    
                    cell.selectedTimeSlot = service.availableTimeSlot[0]
                    cell.arrTimeSlots = service.availableTimeSlot
                    cell.tbl_TimeSlot.reloadData()
                    cell.constraintTblSlotHeight.constant = 80
                    cell.nslcSegmentTop.constant = Utilities.isDeviceiPad() ? 120 : 100
                }
                else
                {
                    cell.selectedTimeSlot = nil
                    cell.arrTimeSlots = [TimeSlot]()
                    cell.constraintTblSlotHeight.constant = 0
                    cell.tbl_TimeSlot.isHidden = true
                    cell.nslcSegmentTop.constant = 10
                }
                
                if (service.serviceAvailable)!
                {
                    cell.lblAvailibility.text = "Available Now"
                    cell.lblAvailibility.textColor = Utilities.colorWithHexString("#FA8606")
                    cell.constraintBtnBookNow.constant = 40
                    cell.btnBookNow.isHidden = false
                }
                else
                {
                    cell.lblAvailibility.text = "Available by Appointment"
                    cell.lblAvailibility.textColor = Utilities.colorWithHexString("#29A036")
                    cell.constraintBtnBookNow.constant = 0.0
                    cell.btnBookNow.isHidden = true
                    cell.nslcBtnRequestTop.constant = 10
                  
                }
                if indexPath.row == self.companyService.count - 1 {
                    
                    //Hide Seperator
                    cell.vwSeperatorLine.isHidden = true
                }
                    
                    
                cell.tag = indexPath.row
                cell.layoutIfNeeded()
                return cell
            }
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewCell", for: indexPath) as! ReviewCell
                let review = self.arr_Reviews[indexPath.row]
                cell.lblDate.text = review.feedbackDateTime
                cell.lblNameReviewBy.text = review.givenByUser
                cell.lblReview.text = review.Feedback
                return cell
            }
            return UITableViewCell()
    }
    
    //MARK:- IBAction
    @IBAction func btnFavouriteCompanyClick(_ sender: Any) {
        KVNProgress.show()
        var dict = [String : Any]()
        dict["sessionId"]    = DataModel.sharedInstance.onFetchSessionId()
        dict["userId"]       = DataModel.sharedInstance.onFetchUserId()
        dict["companyId"]    = self.serviceData?.companyId
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kAddToFavoriteCompany, inputParams: dict, success: { (dict) in
            
            
            if dict?["Result"] as? Bool == true {
                
                if self.serviceData?.isFavourite == false
                {
                    self.serviceData?.isFavourite = true
                    self.btnHeaderFavourite.setImage(#imageLiteral(resourceName: "favorited"), for: .normal)
                    
                }
                else
                {
                    self.serviceData?.isFavourite = false
                    self.btnHeaderFavourite.setImage(#imageLiteral(resourceName: "unfavorited"), for: .normal)
                }
                KVNProgress.showSuccess(withStatus: dict?["Message"] as! String)
            }
            
        }) { error in
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
        }
    }
    
    //MARK: - API Call
    func callGetMassgeTherapyDetailsAPI(){
        
        KVNProgress.show()
        var dict = [String : Any]()
        dict["sessionId"]     = DataModel.sharedInstance.onFetchSessionId()
        dict["userId"]        = DataModel.sharedInstance.onFetchUserId()
        dict["categoryId"]    = self.categoryID
        dict["companyId"]     = self.companyID
        dict["subCategoryId"] = self.subCatagoryID
        
        if isFromNotification {
            dict["serviceId"]     = self.serviceId
             //  dict["servicesMultiple"] = 0
        }
     
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kGetCompanyService, inputParams: dict, success: { (dict) in
            
            KVNProgress.dismiss()
            if dict?["Result"] as? Bool == true {
                if dict?["Data"] != nil
                {
                    let data = dict?["Data"] as? [String:Any]
                    if data?["companyInfo"] != nil
                    {
                        print("Data=======>",data?["companyInfo"])
                        self.serviceData = CompanyServiceList.init(dict: data?["companyInfo"] as! [String : Any])
                        self.setHeaderData()
                    }
                    let services = data?["services"] as? NSArray
                    if services?.count != 0
                    {
                        for i in services!
                        {
                            let service = CompanyService.init(dict: i as! [String : Any])
                            self.companyService.append(service)
                            service.arrServiveTimeSlot = NSMutableArray()
                            if service.availableTimeSlot.count != 0
                            {
                                service.arrServiveTimeSlot.add(service.availableTimeSlot[0] as Any)
                            }
                            let dict = ["address":DataModel.sharedInstance.onFetchAddressLines(),"latitude":DataModel.sharedInstance.onFetchLatitude(),"longitude":DataModel.sharedInstance.onFetchLongtitude()] as [String : Any]
                            self.addressData.append(dict)
                        }
                        
                        self.tblService.reloadData()
                    }
                    let reviews = data?["reviews"] as? NSArray
                    if reviews?.count != 0
                    {
                        for i in reviews!
                        {
                            let review = ReviewsModel.init(dict: i as! [String : Any])
                            self.arr_Reviews.append(review)
                        }
                        self.tblReviews.reloadData()
                        
                    }
                    self.cosmos_Ratings.rating = (data?["overAllRating"] as? Double)!
                    self.vw_Reviews.isHidden = false
                    self.lblTitle.text = self.serviceData?.comapnyScreenName
                    self.view.layoutIfNeeded()
                    self.nsLayoutServiceTable.constant = self.tblService.contentSize.height
                }
                else
                {
                    Utilities.showAlertView(title: "", message: dict?["Message"] as? String)
                }
            }
            else
            {
                Utilities.showAlertView(title: "", message: dict?["Message"] as? String)
            }
            
        }) { error in
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
        }
    }
}
