//
//  BuyerVC.swift
//  ShopByUber
//
//  Created by mac on 5/25/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit


class BuyerVC: BaseView {

    //MARK: IBOutlet
    @IBOutlet var vwContent: UIView!
    @IBOutlet var vwTop: UIView!

   
    
    //MARK:- Variables
    var viewPages = UIView()
    var viewLabel = UILabel()
    var pagesController: UIPageViewController!
    var pages: [UIViewController] = []
    var promoCode = String()
    
    
    //MARK:- View Lifecycles
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if appDeletgate.isfromMyWantedService {
            segmentControl.selectedIndex = 2
            appDeletgate.isfromMyWantedService = false
        }else {
            segmentControl.selectedIndex = 0  //Catagories Tab
        }
        self.initialConfig()
        
        segmentControl.segmentColors = [UIColor.black, UIColor.init(red: 29/255, green: 87/255, blue: 145/255, alpha: 1.0), UIColor.black]
        
        segmentControl.segmentFont = [ UIFont(name: "SOURCESANSPRO-REGULAR", size: 12)! ,UIFont(name: "SOURCESANSPRO-REGULAR", size: 12)!, UIFont(name: "SOURCESANSPRO-REGULAR"/*"CarnivaleeFreakshow"*/, size: 12)!, UIFont(name: "SOURCESANSPRO-REGULAR", size: 12)!]
        
        
        if DataModel.sharedInstance.onFetchVersionUpdate() {
            appDeletgate.VersionChecker()
            DataModel.sharedInstance.CheckVersionUpdate(checkVersionUpdate: false)
        }
    }


    func updateCount() {
        
        if self.btnNotify != nil
        {
            if Singleton.sharedManager.unreadNotificationCount != 0
            {
                self.btnNotify.badgeValue = Singleton.sharedManager.unreadNotificationCount.description
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateCount), name: NSNotification.Name(rawValue: "updateNotificationCount"), object: nil)
        
        if Singleton.sharedManager.isFromactivity == .ServiceDetails
        {
            self.initialConfig()
            segmentControl.selectedIndex = 3
            Singleton.sharedManager.isFromactivity = .Anyothers
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func loadView() {
        super.loadView()
        
        self.setupSegmentView()
    }
    
    //MARK:- Private Methods
    func initialConfig(){
        Singleton.sharedManager.unreadHandler={(success) in
            if self.btnNotify != nil
            {
                if Singleton.sharedManager.unreadNotificationCount > 0
                {
                    self.btnNotify.badgeValue = Singleton.sharedManager.unreadNotificationCount.description
                }
                else
                {
                    self.btnNotify.badgeValue = ""
                }
                
            }
        }
        //Show Bubble
        if DataModel.sharedInstance.onFetchPromoCode() != "" {
            
            appDeletgate.ShowBubble(promoCode: DataModel.sharedInstance.onFetchPromoCode())
        }
    }
    
    func setupSegmentView(){
        
        initSegmentControl()
        initPagesController()
        
        view.addSubview(segmentControl)
        view.addSubview(viewPages)
        viewPages.addSubview(pagesController.view)
        view.addSubview(viewLabel)
        
        segmentControl.translatesAutoresizingMaskIntoConstraints = false
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[segmentControl]|", options: .alignAllLeading, metrics: nil, views: ["segmentControl": segmentControl]))
        view.addConstraint(NSLayoutConstraint(item: segmentControl, attribute: .top, relatedBy: .equal, toItem: self.vwTop, attribute: .bottom, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: segmentControl, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 0, constant: Utilities.isDeviceiPad() ? 60.0 : 50.0))
        
        //viewPages.gestureRecognizers = pagesController.gestureRecognizers
        viewPages.translatesAutoresizingMaskIntoConstraints = false
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[viewPages]|", options: .alignAllLeading, metrics: nil, views: ["viewPages": viewPages]))
        view.addConstraint(NSLayoutConstraint(item: viewPages, attribute: .top, relatedBy: .equal, toItem: segmentControl, attribute: .bottom, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: viewPages, attribute: .bottom, relatedBy: .equal, toItem: viewLabel, attribute: .top, multiplier: 1, constant: 0))
        
        pagesController.view.translatesAutoresizingMaskIntoConstraints = false
        viewPages.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[pagesView]|", options: .alignAllLeading, metrics: nil, views: ["pagesView": pagesController.view]))
        viewPages.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[pagesView]|", options: .alignAllFirstBaseline, metrics: nil, views: ["pagesView": pagesController.view]))
        
        viewLabel.translatesAutoresizingMaskIntoConstraints = false
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[viewLabel]|", options: .alignAllLeading, metrics: nil, views: ["viewLabel": viewLabel]))
        view.addConstraint(NSLayoutConstraint(item: viewLabel, attribute: .top, relatedBy: .equal, toItem: self.bottomLayoutGuide, attribute: .top, multiplier: 1, constant: 37))
        view.addConstraint(NSLayoutConstraint(item: viewLabel, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 0, constant: 50))
        
        viewLabel.textAlignment = .center
        viewLabel.textColor = UIColor.black
       // viewLabel.backgroundColor = UIColor.blue
    }

    
        
    func initSegmentControl() {
        
        segmentControl = WBSegmentControl()
        
        segmentControl.segments = [
            TextSegment(text: "Categories"),
            TextSegment(text: Constant.appTitle.strTitle + " Picks"),
            //IconSegment(icon: UIImage(named: "logo-headder")!),
            TextSegment(text: "Wanted Services"),
            TextSegment(text: "Your Activity"),
        ]
       
        
        segmentControl.backgroundColor = Constant.Color.segmentBackgroundColor
        segmentControl.style = .cover
        segmentControl.segmentTextBold = false
        segmentControl.segmentTextFontSize = Utilities.isDeviceiPad() ? 18.0 : 15.0
        segmentControl.cover_color = UIColor(red: 202/255.0, green: 235/255.0, blue: 215/255.0, alpha: 1)
        segmentControl.delegate = self
      
        
    }
    
    func initPagesController() {
        pagesController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pagesController.dataSource = self
        pagesController.delegate = self
        
        let vc1 = CategoriesVC(nibName: "CategoriesVC", bundle: nil)
        let vc2 = ShopByUberPicksVC(nibName: "ShopByUberPicksVC", bundle: nil)
        let vc3 = AllWantedServiceVC(nibName: "AllWantedServiceVC", bundle: nil)
        let vc4 = ActivityVC(nibName: "ActivityVC", bundle: nil)
        self.pages = [vc1,vc2,vc3,vc4]

    }
}

extension BuyerVC: WBSegmentControlDelegate {
    func segmentControl(_ segmentControl: WBSegmentControl, selectIndex newIndex: Int, oldIndex: Int) {
        let targetPages = [pages[newIndex]]
        let direction = ((newIndex > oldIndex) ? UIPageViewControllerNavigationDirection.forward : UIPageViewControllerNavigationDirection.reverse)
        pagesController.setViewControllers(targetPages, direction: direction, animated: true, completion: nil)
        
        if let selectedSegment = segmentControl.selectedSegment as? TextSegment {
            viewLabel.text = selectedSegment.otherAttr
        }
    }
}

extension BuyerVC: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let index = pages.index(of: viewController)!
        return index > 0 ? pages[index - 1] : nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let index = pages.index(of: viewController)!
        return index < pages.count - 1 ? pages[index + 1] : nil
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return pages.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
}

extension BuyerVC: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed == false {
            guard let targetPage = previousViewControllers.first else {
                return
            }
            guard let targetIndex = pages.index(of: targetPage) else {
                return
            }
            segmentControl.selectedIndex = targetIndex
            pageViewController.setViewControllers(previousViewControllers, direction: .reverse, animated: true, completion: nil)
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        guard let targetPage = pendingViewControllers.first else {
            return
        }
        guard let targetIndex = pages.index(of: targetPage) else {
            return
        }
        segmentControl.selectedIndex = targetIndex
    }
}
