//
//  NotificationVC.swift
//  ShopByUber
//
//  Created by Differenz215 on 7/3/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class NotificationVC: BaseView {

    //MARK:- IBOutlet
    @IBOutlet weak var tblNotifications: UITableView!
    @IBOutlet var lblNoDataFound: UILabel!
    
    //MARK: - Variables
    var recordCount = 0
    var arrNotification = [NotificationModel]()
    var pageNo:Int = 0
    var numOfRecords = 10
    var isRefreshing:Bool = false
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        let notificationName = Notification.Name("NotificationVC")
        NotificationCenter.default.addObserver(self, selector: #selector(initialConfig), name: notificationName, object: nil)
        initialConfig()
        
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        Singleton.sharedManager.currentView = self
        if arrNotification.count == 0
        {
            self.setPagination()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        Singleton.sharedManager.currentView = nil
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Private Methods
    func initialConfig(){
       

        self.tblNotifications.register(UINib(nibName: "NotificationCell", bundle: nil), forCellReuseIdentifier: "NotificationCell")
        
    }
    
    func setPagination() {
        self.callNotificationDataApi(showLoader: true)
        
        tblNotifications.es.addPullToRefresh {
            if !(!self.isRefreshing) {
                self.pageNo = 0
                self.arrNotification.removeAll()
                self.tblNotifications.reloadData()
                self.refreshRecord()
            }
            else {
                self.tblNotifications.es.stopPullToRefresh()
            }
        }
    }
    
    func refreshRecord()
    {
        if self.isRefreshing == false
        {
            if self.pageNo == 0
            {
                self.callNotificationDataApi(showLoader: self.arrNotification.count==0 ?true :false)
            }
            else
            {
                self.callNotificationDataApi(showLoader: false)
            }
        }
    }
    
    func stopLoading()
    {
        self.isRefreshing = false
        self.tblNotifications.es.stopPullToRefresh()
        self.tblNotifications.reloadData()
        self.lblNoDataFound.isHidden = arrNotification.count==0 ?false :true
    }
    
    //MARK: - Call Api
    func callNotificationDataApi(showLoader:Bool)
    {
        pageNo += 1
        self.isRefreshing = true
        var dict = [String : Any]()
        
        dict["sessionId"]    = DataModel.sharedInstance.onFetchSessionId()
        dict["userId"]       = DataModel.sharedInstance.onFetchUserId()
        dict["pageNo"]       = pageNo
        dict["numOfRecords"] = numOfRecords
        if showLoader
        {
            KVNProgress.show()
        }
        APIManager.callAPI(url: Constant.serverAPI.URL.kGetNotificationList, inputParams: dict, success: { (dict) in
            
            if dict?["Result"] as? Bool == true {
                let dictData = dict?["Data"] as? [String:Any]
                let data = dictData?["notifications"] as! NSArray
                self.recordCount = dictData?["recordCount"] as! Int
                self.tblNotifications.tableFooterView?.isHidden = true
                self.tblNotifications.tableFooterView?.frame.size = .zero

                var arrNotify = [NotificationModel]()
                for i in data
                {
                    let notificationData = NotificationModel.init(dict: i as! [String : Any])
                    arrNotify.append(notificationData)
                }
                if self.pageNo == 1
                {
                    self.arrNotification.removeAll()
                }
                if arrNotify.count == 0
                {
                    self.pageNo -= 1
                }
                else
                {
                    if arrNotify.count < self.numOfRecords
                    {
                        self.pageNo -= 1
                    }
                    
                    if self.arrNotification.count == 0
                    {
                        self.arrNotification.append(contentsOf: arrNotify)
                    }
                    else
                    {
                        let responsRecord = arrNotify as NSArray
                        responsRecord.enumerateObjects({ (objF, idxF, nil) in
                            let objNotificationModel = objF as! NotificationModel
                            
                            if self.arrNotification.contains(where: {$0.notificationId == objNotificationModel.notificationId}) {
                                //
                                print("it exists, do something")
                            } else {
                                //
                                print("item could not be found")
                                self.arrNotification.append(objNotificationModel)
                            }
                        })
                    }
                }
                self.tblNotifications.reloadData()
                
                
            }
            else
            {
                let msg = dict?["Message"] as? String
                Utilities.showAlertView(title: "", message: msg)
                self.pageNo -= 1
            }
            self.stopLoading()
        }) { error in
            
            self.pageNo -= 1
            self.stopLoading()
        }
    }
     
}

extension NotificationVC : UITableViewDelegate, UITableViewDataSource
{
    //MARK: - Tableview delegate method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrNotification.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Utilities.isDeviceiPad() ? 100 : 80
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if !isRefreshing
        {
            if indexPath.row > self.arrNotification.count - 2 && self.recordCount > self.arrNotification.count{

                let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))

                self.tblNotifications.tableFooterView = spinner
                self.tblNotifications.tableFooterView?.isHidden = false
                refreshRecord()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! NotificationCell
        let notificationData = self.arrNotification[indexPath.row]
        if notificationData.isRead == false
        {
            cell.vw_Content.backgroundColor = UIColor.init(red: 202/255, green: 235/255, blue: 215/255, alpha: 1.0)//Utilities.colorWithHexString("#E4FAFC")
        }
        else
        {
            cell.vw_Content.backgroundColor = .white
        }
        if notificationData.serviceImage != nil{
            cell.imgService.sd_setImage(with: URL.init(string: notificationData.serviceImage!), placeholderImage: UIImage(named:"placeHolder"), options: SDWebImageOptions.retryFailed, completed: nil)
        }
        
       // cell.nslclblServiceTitleTop.constant = 15
        cell.lblServiceDetails.text = notificationData.screenName
        cell.lblVendortrack.text    = notificationData.message
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let notificationData = self.arrNotification[indexPath.row]
        if DataModel.sharedInstance.isUserLoggedIn() {
            if notificationData.isRead == false
            {   self.arrNotification[indexPath.row].isRead = true
                self.tblNotifications.reloadData()
                self.callReadNotification(notificationId: notificationData.notificationId!)
            }
            if DataModel.sharedInstance.onFetchUserLoginType() == Constant.UserType.Buyer{
                
                //Buyer
                switch notificationData.notifyId! {
    
                case 4,11:
                    let vc = PayForServiceVC(nibName: "PayForServiceVC", bundle: nil)
                    let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
                    vc.isFrom = trackProgress.isFromNotification

                    navigationController.pushViewController(vc, animated: true)
                case 5:
                    
                    let track = TrackProgressVC.init(nibName: "TrackProgressVC", bundle: nil)
                    track.requestID = notificationData.requestId!
                    track.isFrom = trackProgress.isFromNotification
                    self.navigationController?.pushViewController(track, animated: true)

                case 6,7,8,13:
                    let vc = BuyerPurchaseDetailsVC(nibName: "BuyerPurchaseDetailsVC", bundle: nil)
                    vc.requestID = notificationData.requestId
                    let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
                    navigationController.pushViewController(vc, animated: true)
                
                case 12:
                    let objMassageTherapyDetailsVC = MassageTherapyDetailsVC(nibName: "MassageTherapyDetailsVC", bundle: nil) as MassageTherapyDetailsVC
                    
                    objMassageTherapyDetailsVC.companyID    = notificationData.companyId
                    objMassageTherapyDetailsVC.categoryID   = notificationData.categoryId
                    
                    let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
                    navigationController.pushViewController(objMassageTherapyDetailsVC, animated: true)
                case 15:
                    let objMassageTherapyDetailsVC = MassageTherapyDetailsVC(nibName: "MassageTherapyDetailsVC", bundle: nil) as MassageTherapyDetailsVC

                    objMassageTherapyDetailsVC.companyID          = notificationData.companyId
                    objMassageTherapyDetailsVC.categoryID         = notificationData.categoryId
                    objMassageTherapyDetailsVC.subCatagoryID      = notificationData.subCategoryId
                    objMassageTherapyDetailsVC.serviceId          = notificationData.serviceId
                    objMassageTherapyDetailsVC.requestId          = notificationData.requestId
                    objMassageTherapyDetailsVC.isFromNotification = true

                    let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
                    navigationController.pushViewController(objMassageTherapyDetailsVC, animated: true)
                default:
                    break
                }
                
            }else{
                
                //Seller
                switch notificationData.notifyId! {
                case 4:
                    let vc = RequestDetailsVC(nibName: "RequestDetailsVC", bundle: nil)
                    vc.requestID = notificationData.requestId
                    let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
                    navigationController.pushViewController(vc, animated: true)
                case 5:
                    let vc = RequestDetailsVC(nibName: "RequestDetailsVC", bundle: nil)
                    vc.requestID = notificationData.requestId
                    let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
                    navigationController.pushViewController(vc, animated: true)
                case 6:
                    let vc = RequestDetailsVC(nibName: "RequestDetailsVC", bundle: nil)
                    vc.requestID = notificationData.requestId
                    let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
                    navigationController.pushViewController(vc, animated: true)
                case 10:
                    let vc = RequestDetailVC(nibName: "RequestDetailVC", bundle: nil)
                    vc.requestId = notificationData.requestId!
                    let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
                    navigationController.pushViewController(vc, animated: true)
                default:
                    break
                }
                
            }
        }
    }
}
