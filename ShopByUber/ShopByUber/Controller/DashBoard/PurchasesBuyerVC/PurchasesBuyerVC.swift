//
//  PurchasesBuyerVC.swift
//  ShopByUber
//
//  Created by Differenz215 on 7/7/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class PurchasesBuyerVC: BaseView {
    
    //MARK: - IBOutlet
    @IBOutlet weak var tblPurchases: UITableView!
    @IBOutlet weak var btnContinueShop: UIButton!
    @IBOutlet var vw_Progress: UIActivityIndicatorView!
    @IBOutlet var lblNoDataFound: UILabel!
    
    
    //MARK: - Variables
    var arr_PurchaseServices = [Purchase]()
    
    //MARK: API Variables
    var recordCount = 0
    var pageNo = 0
    var numOfRecords = 10
    var isRefreshing : Bool = false
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialConfig()
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        pageNo = 0
        self.setPagination()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        let rootView = UIApplication.shared.delegate?.window??.rootViewController as? UINavigationController
        let topView = UIApplication.topViewController(base: rootView)
        topView?.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Private Methods
    func initialConfig(){
        
        self.tblPurchases.register(UINib(nibName: "RecentSellerRequestCell", bundle: nil), forCellReuseIdentifier: "RecentSellerRequestCell")
    
    }
    
    func setPagination() {
        self.getPurchaseServicesData(showLoader: true)
        
        tblPurchases.es.addPullToRefresh {
            self.pageNo = 0
            self.arr_PurchaseServices.removeAll()
            self.tblPurchases.reloadData()
            self.refreshRecord()
        }
    }
    
    func refreshRecord()
    {
        if self.isRefreshing == false
        {
            if self.pageNo == 0
            {
                self.getPurchaseServicesData(showLoader: self.arr_PurchaseServices.count==0 ?true :false)
            }
            else
            {
                self.getPurchaseServicesData(showLoader: false)
            }
        }
    }
    
    
    //MARK:- IBAction Method
    @IBAction func btnContinueShoppingClick(_ sender: Any) {
        appDeletgate.dismissBubble()
        appShareManager.selectedMenuItemIndex = 0
        let revealController = self.revealViewController
        let vc = BuyerVC(nibName: "BuyerVC", bundle: nil) as BuyerVC
        revealController().pushFrontViewController(vc, animated: true)
    }
    
    
    //MARK: Call API
    func getPurchaseServicesData(showLoader:Bool)
    {
        if showLoader
        {
            self.showProgressView()
        }
        self.pageNo += 1
        Purchase.callPurchasesAPI(pageNo: self.pageNo, numOfRecords: self.numOfRecords, success: { (Favourites,recordcnt) in
            
            
            self.recordCount = recordcnt
            self.tblPurchases.tableFooterView?.isHidden = true
            self.tblPurchases.tableFooterView?.frame.size = .zero
            
            if self.pageNo == 1
            {
                self.arr_PurchaseServices.removeAll()
            }
            if Favourites.count == 0
            {
                self.pageNo -= 1
            }
            else
            {
                if Favourites.count < self.numOfRecords
                {
                    self.pageNo -= 1
                }
                
                if self.arr_PurchaseServices.count == 0
                {
                    self.arr_PurchaseServices.append(contentsOf: Favourites)
                }
                else
                {
                    let responsRecord = Favourites as NSArray
                    //let tempLocal = self.arrRequest as NSArray
                    responsRecord.enumerateObjects({ (objF, idxF, nil) in
                        let objCompanyService = objF as! Purchase
                        
                        if self.arr_PurchaseServices.contains(where: {$0.requestId == objCompanyService.requestId}) {
                            //
                            print("it exists, do something")
                        } else {
                            //
                            print("item could not be found")
                            self.arr_PurchaseServices.append(objCompanyService)
                        }
                        
                    })
                }
            }
            
            self.stopLoading()
            
        }) {
            
            self.stopLoading()
        }
    }
    
    func stopLoading()
    {
        self.isRefreshing = false
        self.hideProgressView()
        self.tblPurchases.es.stopPullToRefresh()
        self.tblPurchases.reloadData()
        self.lblNoDataFound.isHidden = self.arr_PurchaseServices.count==0 ?false :true
    }
    
    //MARK: Custom Methods
    
    func showProgressView(){
        self.vw_Progress.isHidden = false
        self.vw_Progress.startAnimating()
    }
    
    func hideProgressView(){
        self.vw_Progress.isHidden = true
        self.vw_Progress.stopAnimating()
    }
}

extension PurchasesBuyerVC : UITableViewDelegate, UITableViewDataSource
{
    //MARK: - Tableview delegate method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arr_PurchaseServices.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constant.DeviceType.IS_IPAD_PRO || Constant.DeviceType.IS_IPAD ? 92 : 72
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if !isRefreshing
        {
            if indexPath.row > self.arr_PurchaseServices.count - 2 && self.recordCount > self.arr_PurchaseServices.count{
                
                let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                
                self.tblPurchases.tableFooterView = spinner
                self.tblPurchases.tableFooterView?.isHidden = false
                refreshRecord()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecentSellerRequestCell", for: indexPath) as! RecentSellerRequestCell
        let data = self.arr_PurchaseServices[indexPath.row]
        
        if data.serviceImage != nil{
            cell.imgService.sd_setImage(with: URL.init(string: data.serviceImage!), placeholderImage: UIImage(named:"placeHolder"), options: SDWebImageOptions.retryFailed, completed: nil)
        }else{
            cell.imgService.image = UIImage.init(named: "placeHolder")
        }
        cell.lblPrice.text        = "$" + data.flatFee!
        cell.lblServiceTitle.text = data.screenName
        switch data.status {
            
        case 2:
            cell.lblStatus.text = Constant.StatusText.kNotStarted
            cell.lblStatus.textColor = UIColor.black
            
        case 3:
            cell.lblStatus.text = "On Route"
            cell.lblStatus.textColor = Utilities.colorWithHexString("f97a09")
            
        case 4:
            cell.lblStatus.text = "In Progress"
            cell.lblStatus.textColor = Utilities.colorWithHexString("2b9e36")
            
        case 5:
            cell.lblStatus.text = "Completed"
            cell.lblStatus.textColor = Utilities.colorWithHexString("c4c4c4")
            
        case 6:
            cell.lblStatus.text = "Cancelled"
            cell.lblStatus.textColor = Utilities.colorWithHexString("D52A2B")
            
        default:
            break
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = self.arr_PurchaseServices[indexPath.row]
        let request = BuyerPurchaseDetailsVC(nibName:"BuyerPurchaseDetailsVC",bundle:nil) as BuyerPurchaseDetailsVC
        request.requestID     = data.requestId
        self.navigationController?.pushViewController(request, animated: true)
        
    }
}
