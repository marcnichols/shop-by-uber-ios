//
//  MenuVC.swift
//  ShopByUber
//
//  Created by mac on 5/25/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class MenuVC: UIViewController {
    
    //MARK: IBOutlet
    @IBOutlet weak var tblMenu: UITableView!
    
    
    //MARK:- Variables
    var userRole = Int()
    
    
   
    var arrMenuForBuyer = [["title":"Home","imageName":"Menu_home"],["title":"Favorites","imageName":"Menu_Favorites"],["title":"Purchases","imageName":"Menu_Purchases"],["title":"My Profile","imageName":"Menu_MyProfile"],["title":"Settings","imageName":"Menu_Settings"],["title":"My Wanted Services","imageName":"wanted"],["title":"Sell on " + Constant.appTitle.strTitle,"imageName":"Menu_Sell"]/*,["title":"TV Commercial","imageName":"video"]*/,["title":"Terms and Conditions","imageName":"Menu_Terms"]/*,["title":"Chat","imageName":"chatIcon"]*/]
    
    var arrMenuForSeller = [["title":"My Services","imageName":"Menu_home"],["title":"Wanted Services","imageName":"wanted"],["title":"Requests","imageName":"Menu_Requests"],["title":"Appointments","imageName":"Menu_appointment"],["title":"My Sales","imageName":"Menu_Purchases"],["title":"Business Profile","imageName":"Menu_Service"],["title":"Settings","imageName":"Menu_Settings"],["title":"Buy on " + Constant.appTitle.strTitle,"imageName":"Menu_buy"]/*,["title":"TV Commercial","imageName":"video"]*/,["title":"Terms and Conditions","imageName":"Menu_Terms"]/*,["title":"Chat","imageName":"chatIcon"]*/]
    
    var previosIndex : Int = 0
    var window: UIWindow?
    
    //MARK:- View Lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialConfig()
        // Do any additional setup after loading the view.
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        tblMenu.reloadData()
        let row = tblMenu.indexPathForSelectedRow
        if row == nil {
            tblMenu.selectRow(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: .bottom)
        }
    }
    
    //MARK:- Private Methods
    func initialConfig(){
        tblMenu.register(UINib(nibName: "MenuCell", bundle: nil), forCellReuseIdentifier: "MenuCell")
        tblMenu.delegate = self
        tblMenu.dataSource = self
        tblMenu.tableFooterView = UIView()
        tblMenu.reloadData()
    }
    
    //MARK:- IBActions
    @IBAction func btnPrivacyPolicy_Clicked(_ sender: Any) {
        
        let termsAndCondition = TermsAndConditionVC(nibName: "TermsAndConditionVC", bundle: nil)
        termsAndCondition.strTitle = "Privacy Policy"
        termsAndCondition.url = URL (string: Constant.serverAPI.URL.kPrivacyAndPolicy)
        self.revealViewController().pushFrontViewController(termsAndCondition, animated: true)
    }
    
    @IBAction func btnContactUs_Clicked(_ sender: Any) {
        
        if self.revealViewController() != nil
        {
            self.revealToggle(true)
        }
        let objContactUsVC = ContactUsVC(nibName: "ContactUsVC", bundle: nil)
        Singleton.sharedManager.navigationController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController
        Singleton.sharedManager.navigationController?.pushViewController(objContactUsVC, animated: true)
    }
    
    //MARK: Private Methods
    
    func pushToSettingVC(){
        
        let revealController = self.revealViewController
        let vc  = SettingsVC(nibName: "SettingsVC", bundle: nil) as SettingsVC
        revealController().pushFrontViewController(vc, animated: true)
        
    }
    
    func pushToBusinessProfileVC(){
        
        let revealController = self.revealViewController
        let vc  = BusinessProfileVC(nibName: "BusinessProfileVC", bundle: nil) as BusinessProfileVC
        revealController().pushFrontViewController(vc, animated: true)
    }
    
    func pushToProfileVC(){
        
        
        let revealController = self.revealViewController
        var vc  = UIViewController()
        
        if DataModel.sharedInstance.onFetchUserLoginType() == Constant.UserType.Buyer{
            
            vc = BuyerProfileVC(nibName: "BuyerProfileVC", bundle: nil) as BuyerProfileVC
            
        }else{
            
            vc = ProfileVC(nibName: "ProfileVC", bundle: nil) as ProfileVC
            
        }
        revealController().pushFrontViewController(vc, animated: true)
        
    }
    
    func pushToDashBoard(){
        
        appDeletgate.dismissBubble()
        let revealController = self.revealViewController
        var vc = UIViewController()
        if DataModel.sharedInstance.onFetchUserLoginType() == Constant.UserType.Buyer{
            
            vc =  BuyerVC(nibName: "BuyerVC", bundle: nil) as BuyerVC
            
        }else{
            
            vc =  SellerVC(nibName: "SellerVC", bundle: nil) as SellerVC
            
        }
        
        revealController().pushFrontViewController(vc, animated: true)
        
    }
    
    func pushToFavourites()
    {
        let revealController = self.revealViewController
        let vc  = MyFavouritesBuyerVC(nibName: "MyFavouritesBuyerVC", bundle: nil) as MyFavouritesBuyerVC
        revealController().pushFrontViewController(vc, animated: true)
    }
    
    func pushToPurchases()
    {
        let revealController = self.revealViewController
        let vc  = PurchasesBuyerVC(nibName: "PurchasesBuyerVC", bundle: nil) as PurchasesBuyerVC
        revealController().pushFrontViewController(vc, animated: true)
    }
    
    func pushToMyRequests()
    {
        self.revealViewController().pushFrontViewController(MyRequestsVC(), animated: true)
    }
    
    func pushToMyAppointment()
    {
        self.revealViewController().pushFrontViewController(MyAppointmentsVC(nibName: "MyAppointmentsVC", bundle: nil), animated: true)
    }
    func pushToVideoPlayer() {
        let videoPlayer = VideoTutorialVC(nibName: "VideoTutorialVC", bundle: nil)
        videoPlayer.isfromMenu = true
        self.revealViewController().revealToggle(animated: true)
        appShareManager.selectedMenuItemIndex = self.previosIndex
        self.revealViewController().frontViewController.navigationController?.pushViewController(videoPlayer, animated: true)
    }
    func pushToTermsAndCondition()
    {
        let termsAndCondition = TermsAndConditionVC(nibName: "TermsAndConditionVC", bundle: nil)
        termsAndCondition.strTitle = "Terms and Conditions"
        termsAndCondition.url = URL (string: Constant.serverAPI.URL.kTermsAndCondition)
        self.revealViewController().pushFrontViewController(termsAndCondition, animated: true)
    }
    func pushToChat()  {
        let messageListVC = MessageListVC(nibName: "MessageListVC", bundle: nil)
        self.navigationController?.pushViewController(messageListVC, animated: true)
    }
    func pushToMySales()
    {
        let mySales = MySalesVC(nibName: "MySalesVC", bundle: nil)
        self.revealViewController().pushFrontViewController(mySales, animated: true)
    }
    func pushToWantedService()
    {
        let myWantedServices = MyWantedServiceVC(nibName: "MyWantedServiceVC", bundle: nil)
        myWantedServices.isFromSeller = false
        self.revealViewController().pushFrontViewController(myWantedServices, animated: true)
    }
    
    func pushToBuyerRegistration()
    {
        Utilities.showAlertWithAction(title: "Are you sure?", message: "Would you like to switch profiles? \n This will allow you to switch from the current Vendor profile and redirect into the Buyer profile.", buttonTitle: "Yes", buttonTitle2: "Cancel", onOKClick: {
            
            self.userRole = 1
            self.onLogout()
            
        }) {
            appShareManager.selectedMenuItemIndex = self.previosIndex
            if self.revealViewController() != nil
            {
                self.revealToggle(true)
            }
        }
        
       // NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constant.kPushToBuyerRegister), object: nil)
    }
    func pushToSellerRegistration()
    {
        Utilities.showAlertWithAction(title: "Are you sure?", message: "Would you like to switch profiles? \n This will allow you to switch from the current Buyer profile and redirect into the Vendor profile.", buttonTitle: "Yes", buttonTitle2: "Cancel", onOKClick: {
            
            self.userRole = 2
         //   self.callLogoutAPI()
            self.onLogout()
            
        }) {
            appShareManager.selectedMenuItemIndex = self.previosIndex
            if self.revealViewController() != nil
            {
                self.revealToggle(true)
            }
        }

       // NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constant.kPushToSellerRegister), object: nil)
    }
   
    
    
    //MARK: API CAll
    
//    func callLogoutAPI() {
//
//      //  User.callLogoutAPI {
//
//            self.onLogout()
//     //   }
//
//    }
    func onLogout() {
        
        SwitchUser.switchUser(userId: DataModel.sharedInstance.onFetchUserId(), sessionId: DataModel.sharedInstance.onFetchSessionId(), switchTo: userRole, success: { (dict) in
            
            let legalBusinessName = dict?["legalBusinessName"] as? String
            
            if DataModel.sharedInstance.onFetchUserLoginType() == Constant.UserType.Buyer {
                //Buyer DashBoard
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.onSetupDashboardPage()
                //Clear Notification
                UIApplication.shared.cancelAllLocalNotifications()

            }else{
              
                //Seller
                UserDefaults.standard.set(legalBusinessName, forKey: Constant.DataModelKey.kLegalBusinessName)
                UserDefaults.standard.synchronize()
                DataModel.sharedInstance.setValueInUserDefaults(value: dict?["isCompanyProfileCreated"] as! Bool, key: Constant.DataModelKey.kIsProfileCreated)
                if dict?["isCompanyProfileCreated"] as! Bool {
                    //Profile Created
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.onSetupDashboardPage(switchedToSeller: true)
                    //Clear Notification
                    UIApplication.shared.cancelAllLocalNotifications()
                }else{
                    //Profile Not Created
                    
                    let serviceProfile = ServiceProfileVC(nibName: "ServiceProfileVC", bundle: nil) as ServiceProfileVC
                    _ = self.navigationController?.pushViewController(serviceProfile, animated: true)
                    
                 //   self.pushtoCreateService()
                    
                    
                    //Clear Notification
                    UIApplication.shared.cancelAllLocalNotifications()
                }
            }
        }, failure: {
            
        })
    }
    
    func pushtoCreateService() {
        
        var frontViewController = UIViewController()
        frontViewController = ServiceProfileVC(nibName: "ServiceProfileVC", bundle: nil) //
        let rearViewController = MenuVC(nibName: "MenuVC", bundle: nil)
        let frontNavigationController = UINavigationController(rootViewController: frontViewController)
        frontNavigationController.isNavigationBarHidden = true
        let revealController = SWRevealViewController(rearViewController: rearViewController, frontViewController: frontNavigationController)
        revealController?.panGestureRecognizer().isEnabled = true
        let navigationController = UINavigationController.init(rootViewController: revealController!)
        navigationController.isNavigationBarHidden = true
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
        let viewStatus = UIView.init(frame: CGRect(x: 00, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: 20))
        viewStatus.backgroundColor = .black// #colorLiteral(red: 0.168627451, green: 0.5294117647, blue: 0.6, alpha: 1)
        let window = UIApplication.shared.keyWindow
        window?.addSubview(viewStatus)
    }
}

extension MenuVC : UITableViewDelegate,UITableViewDataSource
{
    //MARK: TableView Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if DataModel.sharedInstance.onFetchUserLoginType() == Constant.UserType.Buyer{
            return arrMenuForBuyer.count
            
        }else{
            return arrMenuForSeller.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:MenuCell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as! MenuCell
        var data = [String:String]()
        
        if DataModel.sharedInstance.onFetchUserLoginType() == Constant.UserType.Buyer{
            
            data =  arrMenuForBuyer[indexPath.row]
            //wanted Font
//            if indexPath.row == 5 {
//                cell.lblMenuTitle.font = UIFont(name: "CarnivaleeFreakshow", size: 15.0)
//            }
            
        }else{
            data =  arrMenuForSeller[indexPath.row]
            //Wanted Font
//            if indexPath.row == 1 {
//                cell.lblMenuTitle.font = UIFont(name: "CarnivaleeFreakshow", size: 15.0)
//            }
        }
        
        cell.imgMenuIcon.image = UIImage(named: data["imageName"] ?? "")
        cell.lblMenuTitle.text = data["title"]
                
        if appShareManager.selectedMenuItemIndex == indexPath.row
        {
            cell.backgroundColor = Constant.Color.menuSelectedColor
            cell.lblMenuTitle.textColor = UIColor.black
            cell.imgMenuIcon.tintColor = UIColor.black
        }
        else
        {
            cell.backgroundColor = UIColor.black
            cell.lblMenuTitle.textColor = UIColor.white
            cell.imgMenuIcon.tintColor = UIColor.white
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if DataModel.sharedInstance.onFetchUserLoginType() == Constant.UserType.Buyer{
            
            switch indexPath.row {
                
            case 0:
                //Home
                self.setSelectedIndexPath(indexPath)
                self.pushToDashBoard()
            case 1:
                //Favourites
                self.setSelectedIndexPath(indexPath)
                self.pushToFavourites()
                break
            case 2:
                //Purchases
                self.setSelectedIndexPath(indexPath)
                self.pushToPurchases()
                break
            case 3:
                //Profile
                self.setSelectedIndexPath(indexPath)
                self.pushToProfileVC()
            case 4:
                //Settings
                self.setSelectedIndexPath(indexPath)
                self.pushToSettingVC()
                
            case 5:
                //wanted Services
                self.setSelectedIndexPath(indexPath)
                //self.pushToWantedService()
                let myWantedServices = MyWantedServiceVC(nibName: "MyWantedServiceVC", bundle: nil)
                myWantedServices.isFromSeller = false
                self.revealViewController().pushFrontViewController(myWantedServices, animated: true)
                
            case 6:
                //Seller Registration
                previosIndex = appShareManager.selectedMenuItemIndex
                self.setSelectedIndexPath(indexPath)
                self.pushToSellerRegistration()
//            case 6:
//                //Commercial
//                self.setSelectedIndexPath(indexPath)
//                self.pushToVideoPlayer()
            case 7:
                //Terms & Condition
                self.setSelectedIndexPath(indexPath)
                self.pushToTermsAndCondition()
//            case 8:
//                //Chat
//                self.setSelectedIndexPath(indexPath)
//                self.pushToChat()
            default:
                break
            }
            
        }else{
            
            switch indexPath.row {
            case 0:
                //Home
                self.setSelectedIndexPath(indexPath)
                self.pushToDashBoard()
                
                
            case 1:
                //Wanted Service
                self.setSelectedIndexPath(indexPath)
               // self.pushToWantedService()
                let myWantedServices = MyWantedServiceVC(nibName: "MyWantedServiceVC", bundle: nil)
                myWantedServices.isFromSeller = true
                self.revealViewController().pushFrontViewController(myWantedServices, animated: true)
                
            case 2:
                //My Requests
                self.setSelectedIndexPath(indexPath)
                self.pushToMyRequests()
            case 3:
                //My Requests
                self.setSelectedIndexPath(indexPath)
                self.pushToMyAppointment()
            case 4:
                //My Sales
                self.setSelectedIndexPath(indexPath)
                self.pushToMySales()
            case 5:
                //Business Profile
                self.setSelectedIndexPath(indexPath)
                self.pushToBusinessProfileVC()
            case 6:
                //Settings
                self.setSelectedIndexPath(indexPath)
                self.pushToSettingVC()
            case 7:
                //Buyer Registration
                previosIndex = appShareManager.selectedMenuItemIndex
                self.setSelectedIndexPath(indexPath)
                self.pushToBuyerRegistration()
//            case 7:
//                //Commercial
//                self.setSelectedIndexPath(indexPath)
//                self.pushToVideoPlayer()
            case 8:
                //Terms & Condition
                self.setSelectedIndexPath(indexPath)
                self.pushToTermsAndCondition()
//            case 9:
//                //Chat
//                self.setSelectedIndexPath(indexPath)
//                self.pushToChat()
            default:
                break
            }
            
        }
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  Utilities.isDeviceiPad() ? 60.0 : 50.0
    }
    
    func setSelectedIndexPath(_ indexPath:IndexPath)
    {
        appShareManager.selectedMenuItemIndex = indexPath.row
    }
    
}
