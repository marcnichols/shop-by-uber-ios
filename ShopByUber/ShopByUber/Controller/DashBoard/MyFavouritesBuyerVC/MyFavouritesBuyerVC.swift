//
//  MyFavouritesBuyerVC.swift
//  ShopByUber
//
//  Created by Differenz215 on 7/7/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class MyFavouritesBuyerVC: BaseView {

    //MARK: - IBOutlet
    @IBOutlet weak var tblFavourites: UITableView!
    @IBOutlet weak var btnContinueShop: UIButton!
    @IBOutlet var vw_Progress: UIActivityIndicatorView!
    @IBOutlet var lblNoDataFound: UILabel!

    //MARK: - Variables
    var arr_FavouritesServices = [CompanyService]()
    
    //MARK: API Variables
    var recordCount = 0
    var pageNo = 0
    var numOfRecords = 10
    var isRefreshing : Bool = false
    var isFromInfinite : Bool = false

    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialConfig()
        self.automaticallyAdjustsScrollViewInsets = false
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if self.arr_FavouritesServices.count == 0{
           
                self.setPagination()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        let rootView = UIApplication.shared.delegate?.window??.rootViewController as? UINavigationController
        let topView = UIApplication.topViewController(base: rootView)
        topView?.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Private Methods
    func initialConfig(){
        
        self.tblFavourites.register(UINib(nibName: "MyFavoiuriteServiceCell", bundle: nil), forCellReuseIdentifier: "MyFavoiuriteServiceCell")
    }
    
    func setPagination() {
        self.getFavouriteData(showLoader: true)
        
        tblFavourites.es.addPullToRefresh {
        
        if !(!self.isRefreshing) {
            self.pageNo = 0
            self.arr_FavouritesServices.removeAll()
            self.tblFavourites.reloadData()
            self.refreshRecord()
        }
        else {
            self.tblFavourites.es.stopPullToRefresh()
        }
        }
    }
    
    func refreshRecord()
    {
        if self.isRefreshing == false
        {
            if self.pageNo == 0
            {
                self.getFavouriteData(showLoader: self.arr_FavouritesServices.count==0 ?true :false)
            }
            else
            {
                self.getFavouriteData(showLoader: false)
            }
        }
    }

    func btnFavouriteClick(sender: UIButton)
    {
        let data = self.arr_FavouritesServices[sender.tag]
        KVNProgress.show()
        var dict = [String : Any]()
        dict["sessionId"]    = DataModel.sharedInstance.onFetchSessionId()
        dict["userId"]       = DataModel.sharedInstance.onFetchUserId()
        dict["serviceId"]    = data.serviceId
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kAddToFavoriteService, inputParams: dict, success: { (dict) in
            
            
            if dict?["Result"] as? Bool == true {
                
                self.arr_FavouritesServices.remove(at: sender.tag)
                self.tblFavourites.deleteRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .fade)
                KVNProgress.showSuccess(withStatus: dict?["Message"] as! String)
                self.stopLoading()
            }
            
        }) { error in
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
            
        }
        

    }
    
    //MARK:- IBAction Method
    @IBAction func btnContinueShoppingClick(_ sender: Any) {
        
        appDeletgate.dismissBubble()
        appShareManager.selectedMenuItemIndex = 0
        let revealController = self.revealViewController
        let vc = BuyerVC(nibName: "BuyerVC", bundle: nil) as BuyerVC
        if revealController() != nil
        {
            revealController().pushFrontViewController(vc, animated: true)
        }
        else
        {
            appDeletgate.onSetupDashboardPage()
        }
    }
    
    
    //MARK: Call API
    func getFavouriteData(showLoader:Bool)
    {
        if showLoader
        {
            self.showProgressView()
        }
        self.pageNo += 1
        CompanyService.callMyFavouritesAPI(pageNo: self.pageNo, numOfRecords: self.numOfRecords, success: { (Favourites,recordcnt) in
            
            
            self.recordCount = recordcnt
            self.tblFavourites.tableFooterView?.isHidden = true
            self.tblFavourites.tableFooterView?.frame.size = .zero
            
            
            if self.pageNo == 1
            {
                self.arr_FavouritesServices.removeAll()
            }
            if Favourites.count == 0
            {
                self.pageNo -= 1
            }
            else
            {
                if Favourites.count < self.numOfRecords
                {
                    self.pageNo -= 1
                }
                
                if self.arr_FavouritesServices.count == 0
                {
                    self.arr_FavouritesServices.append(contentsOf: Favourites)
                }
                else
                {
                    let responsRecord = Favourites as NSArray
                    //let tempLocal = self.arrRequest as NSArray
                    responsRecord.enumerateObjects({ (objF, idxF, nil) in
                        let objCompanyService = objF as! CompanyService
                        
                        if self.arr_FavouritesServices.contains(where: {$0.serviceId == objCompanyService.serviceId}) {
                            //
                            print("it exists, do something")
                        } else {
                            //
                            print("item could not be found")
                            self.arr_FavouritesServices.append(objCompanyService)
                        }
                        
                    })
                }
            }
            
            self.stopLoading()
            
        }) {
            self.stopLoading()
        }
        
    }
    
    func stopLoading()
    {
        self.isRefreshing = false
        self.hideProgressView()
        self.tblFavourites.es.stopPullToRefresh()
        self.tblFavourites.reloadData()
        self.lblNoDataFound.isHidden = self.arr_FavouritesServices.count==0 ?false :true
    }
    
    //MARK: Custom Methods
    
    func showProgressView(){
        self.vw_Progress.isHidden = false
        self.vw_Progress.startAnimating()
    }
    
    func hideProgressView(){
        self.vw_Progress.isHidden = true
        self.vw_Progress.stopAnimating()
    }
}

extension MyFavouritesBuyerVC : UITableViewDelegate, UITableViewDataSource
{
    //MARK: - Tableview delegate method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arr_FavouritesServices.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Utilities.isDeviceiPad() ? 92 : 72
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if !isRefreshing
        {
            if indexPath.row > self.arr_FavouritesServices.count - 2 && self.recordCount > self.arr_FavouritesServices.count{
                
                let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                
                self.tblFavourites.tableFooterView = spinner
                self.tblFavourites.tableFooterView?.isHidden = false
                refreshRecord()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyFavoiuriteServiceCell", for: indexPath) as! MyFavoiuriteServiceCell
        let data = self.arr_FavouritesServices[indexPath.row]
        
        if data.serviceImage != nil{
            cell.imgService.sd_setImage(with: URL.init(string: data.serviceImage!), placeholderImage: UIImage(named:Constant.PlaceHolder.kPhoto), options: SDWebImageOptions.retryFailed, completed: nil)
        }else{
            cell.imgService.image = UIImage.init(named: Constant.PlaceHolder.kPhoto)
        }
        cell.lblPrice.text        = "$" + data.flatFee!
        cell.btnFavourite.tag     = indexPath.row
        cell.btnFavourite.addTarget(self, action: #selector(self.btnFavouriteClick(sender:)), for: .touchUpInside)
        cell.lblServiceTitle.text = data.screenName
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = self.arr_FavouritesServices[indexPath.row]
        let objMassageTherapyDetailsVC = MassageTherapyDetailsVC(nibName: "MassageTherapyDetailsVC", bundle: nil) as MassageTherapyDetailsVC
        
        objMassageTherapyDetailsVC.companyID     = data.companyId
        objMassageTherapyDetailsVC.categoryID    = data.categoryId
        objMassageTherapyDetailsVC.subCatagoryID = data.subCategoryId

        self.navigationController?.pushViewController(objMassageTherapyDetailsVC, animated: true)

    }
}
