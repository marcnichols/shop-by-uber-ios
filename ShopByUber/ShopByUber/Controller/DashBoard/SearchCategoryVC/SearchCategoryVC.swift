//
//  SearchCategoryVC.swift
//  ShopByUber
//
//  Created by Administrator on 6/29/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

let kHistoryCell = "HistoryCell"
let kNavBarHeight:CGFloat = 75.0

class SearchCategoryVC: UIViewController {

    //MARK:- IBOutlet
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tblList: UITableView!
    @IBOutlet weak var tblSearch: UITableView!
    @IBOutlet weak var lblNoDataFound: UILabel!
    @IBOutlet weak var lblAllCategories: UILabel!
    @IBOutlet weak var consTblHistoryHeight: NSLayoutConstraint!
   
    
    //MARK:- Variable
     var searchStringText : String = ""
    var arrHistory = [String]()
    var arrDisplayhistory = [String]()
    var avaiblableHeight : CGFloat = 0.0
    
    //MARK: API Variables
    var recordCount = 0
    var pageNo = 0
    var arrSearchCategories = [Category]()
    var arr_SubCategories = [SubCategory]()
    var arrSearchCompanies = [Companies]()
    var numOfRecords = 10
    var isFromInfinite : Bool = false
    var isRefreshing : Bool = false
    
    var totalCount:Int = 0
    var refreshControl = UIRefreshControl()
    
    //MARK:- View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.initialConfig()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Private Methods
    
    func keyboardWillShow(notification:NSNotification) {
        let userInfo:NSDictionary = notification.userInfo! as NSDictionary
        let keyboardFrame:NSValue = userInfo.value(forKey: UIKeyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.cgRectValue
        avaiblableHeight = Constant.ScreenSize.SCREEN_HEIGHT - keyboardRectangle.height - kNavBarHeight
    }
    
    func initialConfig(){
        
        searchBar.setPlaceholderTextColorTo(color: UIColor.init(red: 0/255, green: 254/255, blue: 103/255, alpha: 1.0))
        searchBar.setMagnifyingGlassColorTo(color: UIColor.init(red: 0/255, green: 254/255, blue: 103/255, alpha: 1.0))
     //   searchBar.setCloseIconColorTo(color: UIColor.init(red: 0/255, green: 254/255, blue: 103/255, alpha: 1.0))
        
        lblNoDataFound.text = "Please enter your search topic!"
        lblAllCategories.isHidden = true
        self.tblList.isHidden = true
        
        self.tblList.register(UINib(nibName: "ServiceCell", bundle: nil), forCellReuseIdentifier: "ServiceCell")
        self.tblList.tableFooterView = UIView(frame: CGRect.zero)
        
        self.tblSearch.register(UITableViewCell.self, forCellReuseIdentifier: kHistoryCell)
        self.tblSearch.tableFooterView = UIView()
        
        if self.arrSearchCategories.count == 0{
            
            self.setPagination()
        }
        
        if let data = Utilities.retriveObjectFromDefault(forKey: Constant.kHistory)
        {
            arrHistory = data as! [String]
        }
        
        onAddRefreshController()
    }

    func setPagination() {
        
        tblList.es.addPullToRefresh {
            self.pageNo = 0
            self.arrHistory.removeAll()
            self.tblList.reloadData()
            self.refreshRecord()
        }
    }

    func refreshRecord()
    {
        if self.isRefreshing == false
        {
            if self.pageNo == 0
            {
                self.callGetLocalDataAPI(showLoader: false)
            }
            else
            {
                self.callGetLocalDataAPI(showLoader: false)
            }
        }
    }
    
//    //MARK: Comment Made You Service configure with Infinate Scroll
    func getSearchDataServiceCall() {
       
        self.callGetLocalDataAPI(showLoader: true)

        self.tblList.es.addInfiniteScrolling {
            if self.isRefreshing == false
            {
                if ((self.pageNo == 0) || (self.arrSearchCompanies.count < self.totalCount) || (self.arrSearchCategories.count < self.totalCount)) {
                    self.callGetLocalDataAPI(showLoader: true)
                }
                else {
                    self.tblList.es.noticeNoMoreData()
                    self.tblList.es.removeRefreshFooter()
                }

                self.tblList.es.stopLoadingMore()
                if self.refreshControl.isRefreshing {
                    self.refreshControl.endRefreshing()
                }
            }
        }
        
    }
    // Referesh controller configure with tableview
    func onAddRefreshController() {
        
        self.refreshControl.addTarget(self, action: #selector(refreshList), for: .valueChanged)
        if #available(iOS 10.0, *) {
            self.tblList.refreshControl = self.refreshControl
        } else {
            self.tblList.addSubview(self.refreshControl)
        }
    }
    
    //MARK: Pull to referesh configure
    @objc func refreshList() {
    
        if Utilities.checkInternetConnection() {
            if !isRefreshing {
                self.refreshControl.beginRefreshing()
                pulltorefereshDataLoad()
                
            }
            else {
                self.refreshControl.endRefreshing()
                self.tblList.es.stopLoadingMore()
                self.tblList.es.noticeNoMoreData()
            }
        }
        else {
            if self.refreshControl.isRefreshing {
                self.refreshControl.endRefreshing()
                self.tblList.es.stopLoadingMore()
                self.tblList.es.noticeNoMoreData()
            }
        }
    }
    // Pull to referesh data configure
    func pulltorefereshDataLoad() {
        
        self.tblList.es.resetNoMoreData()
        self.arrSearchCompanies.removeAll()
        self.arrSearchCategories.removeAll()
        self.pageNo = 0
        self.callGetLocalDataAPI(showLoader: true)
        self.refreshControl.endRefreshing()
        self.tblList.reloadData()
        
    }
    
    //MARK: API Call
    
    func callGetLocalDataAPI(showLoader:Bool){
        
        self.pageNo += 1
        if searchStringText != "" {
        Category.callGetSearchDataAPI(searchText: searchStringText, pageNo: self.pageNo, numOfRecords: self.numOfRecords, success: { (dict) in
            
           
            let cat = dict!["categories"] as? [[String:Any]]
         //   let Comp = dict!["companies"] as? [[String:Any]]
            
            // If Categories is not nil, get categories data
            if cat?.count != 0 {
                let categories = dict?["categories"] as? [[String:Any]]
                let recordCount = dict?["recordCount"] as? Int
                self.recordCount = recordCount ?? 0
                
                self.tblList.tableFooterView?.isHidden = true
                self.tblList.tableFooterView?.frame.size = .zero
                
                if categories != nil{
                    
                    self.isRefreshing = false
                    if self.pageNo == 1
                    {
                        self.arrSearchCategories.removeAll()
                    }
                    if categories?.count == 0
                    {
                        self.pageNo -= 1
                        self.lblNoDataFound.text = Constant.Message.kSearchNoDataFound
                        self.lblNoDataFound.isHidden = false
                        self.tblList.isHidden = true
                        self.lblAllCategories.isHidden = true
                    }
                    else
                    {
                        
                        //self.arrSearchCategories.removeAll()
                        for category in categories!{
                            
                            let objCategory = Category.init(dict: category) as Category
                            
                            if self.arrSearchCategories.contains(where: {$0.id == objCategory.id}) {
                                //
                                print("it exists, do something")
                            } else {
                                //
                                print("item could not be found")
                                self.arrSearchCategories.append(objCategory)
                            }
                            
                        }
                        
                        self.lblNoDataFound.isHidden = true
                        self.tblList.isHidden = false
                        self.lblAllCategories.isHidden = false
                        
                        
                    }
                    
                }else{
                    
                    self.lblNoDataFound.text =  Constant.Message.kSearchNoDataFound
                    self.lblNoDataFound.isHidden = false
                    self.tblList.isHidden = true
                    self.lblAllCategories.isHidden = true
                    self.pageNo -= 1
                }
                
                
                //  self.tblList.es.footer?.noMoreData = false
                self.tblList.footer?.noMoreData = false
                self.tblList.es.stopPullToRefresh()
                self.tblList.es.stopLoadingMore()
                self.tblList.delegate = self
                self.tblList.dataSource = self
                self.tblList.reloadData()
                
            }else
            {
                
                // Get Companies Data
                let companies = dict?["companies"] as? [[String:Any]]
                let recordCount = dict?["recordCount"] as? Int
                self.recordCount = recordCount ?? 0
                
                self.tblList.tableFooterView?.isHidden = true
                self.tblList.tableFooterView?.frame.size = .zero
                
                if companies != nil{
                    
                    self.isRefreshing = false
                    if self.pageNo == 1
                    {
                        self.arrSearchCompanies.removeAll()
                    }
                    if companies?.count == 0
                    {
                        self.pageNo -= 1
                        self.lblNoDataFound.text = Constant.Message.kSearchNoDataFound
                        self.lblNoDataFound.isHidden = false
                        self.tblList.isHidden = true
                        self.lblAllCategories.isHidden = true
                    }
                    else
                    {
                        
                        //self.arrSearchCategories.removeAll()
                        for comp in companies!{
                            
                            let objCategory = Companies.init(dict: comp) as Companies
                            
                            if self.arrSearchCompanies.contains(where: {$0.id == objCategory.id}) {
                                //
                                print("it exists, do something")
                            } else {
                                //
                                print("item could not be found")
                                self.arrSearchCompanies.append(objCategory)
                            }
                            
                        }
                        
                        self.lblNoDataFound.isHidden = true
                        self.tblList.isHidden = false
                        self.lblAllCategories.isHidden = false
                        
                        
                    }
                    
                }else{
                    
                    self.lblNoDataFound.text =  Constant.Message.kSearchNoDataFound
                    self.lblNoDataFound.isHidden = false
                    self.tblList.isHidden = true
                    self.lblAllCategories.isHidden = true
                    self.pageNo -= 1
                }
                
                
                //  self.tblList.es.footer?.noMoreData = false
                self.tblList.footer?.noMoreData = false
                self.tblList.es.stopPullToRefresh()
                self.tblList.es.stopLoadingMore()
                self.tblList.delegate = self
                self.tblList.dataSource = self
                self.tblList.reloadData()
                
                
            }
        }) {
            //No Data Found
            self.lblNoDataFound.text = Constant.Message.kSearchNoDataFound
            self.lblNoDataFound.isHidden = false
            self.tblList.isHidden = true
            self.lblAllCategories.isHidden = true
            self.pageNo -= 1
        }
        }
    }
    
    //MARK: Button viewForHeaderInSection
    
    @IBAction func btnBack_Tapped(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }

}
//MARK: UISearch Bar Extension
extension UISearchBar
{
    func setPlaceholderTextColorTo(color: UIColor)
    {
        let textFieldInsideSearchBar = self.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = color
        let textFieldInsideSearchBarLabel = textFieldInsideSearchBar!.value(forKey: "placeholderLabel") as? UILabel
        textFieldInsideSearchBarLabel?.textColor = color
    }
    
    func setMagnifyingGlassColorTo(color: UIColor)
    {
        let textFieldInsideSearchBar = self.value(forKey: "searchField") as? UITextField
        let glassIconView = textFieldInsideSearchBar?.leftView as? UIImageView
        glassIconView?.image = glassIconView?.image?.withRenderingMode(.alwaysTemplate)
        glassIconView?.tintColor = color
    }
    
//    func setCloseIconColorTo(color: UIColor){
//
//         let textFieldInsideSearchBar = self.value(forKey: "clearButton") as! UITextField
//         let clearButton = textFieldInsideSearchBar.rightView as? UIButton
//        clearButton?.setImage(clearButton?.imageView?.image?.withRenderingMode(.alwaysTemplate),
//                                 for : .normal)
//        clearButton?.tintColor = color
//    }
}

//MARK: UITableview delegate
extension SearchCategoryVC : UITableViewDelegate,UITableViewDataSource
{

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tblList
        {
            if arrSearchCompanies.count != 0 {
                return arrSearchCompanies.count
            }else {
            return arrSearchCategories.count
            }
        }
        else{
            return arrDisplayhistory.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == tblList
        {
                return Utilities.isDeviceiPad() ? 92.0 : 72.0
        }
        else
        {
            return Utilities.isDeviceiPad() ?57 :44
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblList
        {
            if arrSearchCompanies.count != 0 {
                //Show Companies cell
                let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceCell", for: indexPath) as? ServiceCell
                Utilities.applyShadowEffect(view: [cell!.vwBG])
                
                let localCompaniesData = self.arrSearchCompanies[indexPath.row]
                
                if localCompaniesData.imageURL != nil{
                    cell?.serviceImage.sd_setImage(with: URL.init(string: localCompaniesData.imageURL!), placeholderImage: UIImage(named:Constant.PlaceHolder.kPhoto), options: SDWebImageOptions.retryFailed, completed: nil)
                }else{
                    cell?.serviceImage.image = UIImage.init(named: Constant.PlaceHolder.kPhoto)
                }
                
                cell?.lblTitle.text = localCompaniesData.comapnyScreenName
                cell?.nslcLblCatagoryTypeHight.constant = Utilities.isDeviceiPad() ? 22:18
                cell?.nslclblDescHight.constant = 0
                
                cell?.lblCatagoryType.text = localCompaniesData.categories!
                cell?.layoutIfNeeded()
                return cell!
            }
            else {
            //Show Categories Cell
            let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceCell", for: indexPath) as? ServiceCell
            Utilities.applyShadowEffect(view: [cell!.vwBG])
            
            let localCategoryData = self.arrSearchCategories[indexPath.row]
            
            if localCategoryData.categoryImage != nil{
                cell?.serviceImage.sd_setImage(with: URL.init(string: localCategoryData.categoryImage!), placeholderImage: UIImage(named:Constant.PlaceHolder.kPhoto), options: SDWebImageOptions.retryFailed, completed: nil)
            }else{
                cell?.serviceImage.image = UIImage.init(named: Constant.PlaceHolder.kPhoto)
            }
            
            cell?.lblTitle.text = localCategoryData.catName
           
            cell?.nslcLblCatagoryTypeHight.constant = 0
            cell?.nslclblDescHight.constant = 0
          //  cell?.lblDescription.isHidden = true
                
//            let yConstraint = NSLayoutConstraint(item: cell?.lblTitle as Any, attribute: .centerY, relatedBy: .equal, toItem: cell, attribute: .centerY, multiplier: 1, constant: -5)
//
//                cell?.addConstraint(yConstraint)
            cell?.layoutIfNeeded()
            return cell!
            }
            
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: kHistoryCell, for: indexPath)
            
            cell.textLabel?.text = arrDisplayhistory[indexPath.row]
            cell.textLabel?.font = UIFont.systemFontReguler(with: 16)
            
            return cell
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblSearch
        {
            searchBar.text = arrDisplayhistory[indexPath.row]
            self.searchBarSearchButtonClicked(searchBar)
        }
        else
        {
            //Check if catagories cell or Companies cell is displayed
            if arrSearchCategories.count != 0 {
                
                let catID = self.arrSearchCategories[indexPath.row].id
                SubCategory.getSubSubcategories(categoryId: catID, allSubcategoryRequired: true, success: { (sub_Cat) in
                    
                    if sub_Cat.count == 0 {
                        //No Data
                    }
                    else
                    {
                        self.arr_SubCategories.removeAll()
                        self.arr_SubCategories.append(contentsOf: sub_Cat)
                        let cat_Name = self.arr_SubCategories.map({ $0.subCatName! })
                        
                        //  Show Sub Category pop up
                        self.view.showRadioBoxPopup(topHeading: Constant.PopupTitle.kSelectSubCategory, defaultSelectedIndex: 0, arrData: cat_Name ) { (selectedText, selectedIndex) in
                            
                            let subCategoryData = self.arr_SubCategories[selectedIndex]
                            let objMassageTherapyVC = MassageTherapyVC(nibName: "MassageTherapyVC", bundle: nil) as MassageTherapyVC
                            objMassageTherapyVC.categoryId = catID
                            objMassageTherapyVC.subCategoryID = subCategoryData.id
                            objMassageTherapyVC.strTitle = subCategoryData.subCatName!
                            objMassageTherapyVC.currentZipCode = ""
                            let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
                            navigationController.pushViewController(objMassageTherapyVC, animated: true)
                        }
                    }
                    
                }) {_ in
                    //Alert
                    print("API Call Fail")
                    
                }
            }else {
                let Data = self.arrSearchCompanies[indexPath.row]
                let objServiceDetailForSBUVC = ServiceDetailForSBUVC(nibName: "ServiceDetailForSBUVC", bundle: nil) as ServiceDetailForSBUVC
                objServiceDetailForSBUVC.companyID    = Data.id
                objServiceDetailForSBUVC.screenName   = Data.companyName!
                let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
                navigationController.pushViewController(objServiceDetailForSBUVC, animated: true)
            }
            
            
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if !isRefreshing
        {
            if self.arrSearchCategories.count != 0 {
            if indexPath.row > self.arrSearchCategories.count - 2 && self.recordCount > self.arrSearchCategories.count{
                
                let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                
                self.tblList.tableFooterView = spinner
                self.tblList.tableFooterView?.isHidden = false
                refreshRecord()
            }
            }
            if self.arrSearchCompanies.count != 0 {
                if indexPath.row > self.arrSearchCompanies.count - 2 && self.recordCount > self.arrSearchCompanies.count{
                    
                    let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
                    spinner.startAnimating()
                    spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                    
                    self.tblList.tableFooterView = spinner
                    self.tblList.tableFooterView?.isHidden = false
                    refreshRecord()
            }
        
        }
    }
    }
}

extension SearchCategoryVC : UISearchBarDelegate,UISearchDisplayDelegate
{
    //MARK: UISearchBar delegate
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        searchStringText = searchBar.text!
        searchBar.resignFirstResponder()
        pageNo = 0
        
        getSearchDataServiceCall()
        
        if !appShareManager.isDisableHistory
        {
            let str = searchBar.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            if (str?.count)! > 0
            {
                if !arrHistory.contains(searchBar.text!)
                {
                    arrHistory.append(searchBar.text!)
                    
                    Utilities.setObjectToDefault(object: arrHistory, forKey: Constant.kHistory)
                }
                
            }
        }
        UIView.animate(withDuration: 0.2, animations: {
            self.tblSearch.alpha = 0.0
        })
        
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
    }
    
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        searchBar.resignFirstResponder()
        searchBar.text = ""
        self.arrSearchCompanies.removeAll()
        self.arrSearchCategories.removeAll()
        self.tblList.es.stopPullToRefresh()
        self.tblList.es.removeRefreshHeader()
        pageNo = 0
        self.tblList.reloadData()
    }
    
    func searchBarBookmarkButtonClicked(_ searchBar: UISearchBar) {
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        //searchText
        searchStringText = searchText
        
        if searchText.count > 0 {
            
            if arrSearchCategories.count == 0 {
                lblNoDataFound.text = Constant.Message.kSearhDefaultMessage
                lblNoDataFound.isHidden = true
            }
        }
        if searchText.count == 0 {
            
            if arrSearchCategories.count == 0 || arrSearchCompanies.count == 0 {
                lblNoDataFound.text = Constant.Message.kSearhDefaultMessage
                lblNoDataFound.isHidden = false
                self.arrSearchCompanies.removeAll()
                self.arrSearchCategories.removeAll()
                self.tblList.es.stopPullToRefresh()
                self.tblList.es.removeRefreshHeader()
                pageNo = 0
                self.tblList.reloadData()
            }
            
        }
        
        if !appShareManager.isDisableHistory
        {
            arrDisplayhistory = arrHistory.filter { (str) -> Bool in
                return str.contains(searchText)
            }
            
            var height:CGFloat = CGFloat(arrDisplayhistory.count * 44)
            
            if height > avaiblableHeight
            {
                height = avaiblableHeight
            }
            
            consTblHistoryHeight.constant = height
            tblSearch.reloadData()
            if height > 0
            {
                UIView.animate(withDuration: 0.2, animations: {
                    self.tblSearch.alpha = 1.0
                })
            }

        }

    }

}
