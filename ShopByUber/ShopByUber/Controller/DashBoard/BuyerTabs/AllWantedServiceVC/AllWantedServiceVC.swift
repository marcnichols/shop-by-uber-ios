//
//  AllWantedServiceVC.swift
//  The EZ List
//
//  Created by mac on 4/13/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

import UIKit

class AllWantedServiceVC: UIViewController {

    @IBOutlet var tblWantedServices: UITableView!
    @IBOutlet var vwProgress: UIActivityIndicatorView!
    @IBOutlet var lblNoDataFound: UILabel!
    
    var arrMyWantedServices = [MyWantedServices]()
    var recordCount = 0
    var pageNo = 0
    var numOfRecords = 10
    var isRefreshing : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialConfig()
        
        if appDeletgate.isfromMyWantedService == false{
            self.view.ShowWantedServicePopup(title: "What are \"Wanted Services\" ?", msg: "If a Buyer cannot find a service Provider to accommodate a specific request, a Buyer can create a Wanted Service Ad on this screen. Once created, the requeste will be listed here for Service Provider to review, and also a noficiation will be sent out to all existing Service Provider within a Buyer general location. If a Vendor answers a Buyers Wanted Service Ad by creating a Service to accommodate the request, the Buyer will be notifies immediately in order to book that service.")
        }
        // Do any additional setup after loading the view.
        //self.automaticallyAdjustsScrollViewInsets = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.arrMyWantedServices.removeAll()
//        recordCount = 0
//        self.isRefreshing = false
//        self.pageNo = 0
        
        if self.arrMyWantedServices.count == 0{
            self.setPagination()
        }

    }
    //MARK: IBAction
    
    @IBAction func btnRequestService(_ sender: Any) {
        let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
        let requestServiceVC = RequestServiceVC(nibName: "RequestServiceVC", bundle: nil) as RequestServiceVC
        navigationController.pushViewController(requestServiceVC, animated: true)

    }
    
    //MARK: Private Methods
    func initialConfig()
    {
        tblWantedServices.register(UINib.init(nibName: "WantedServiceCell", bundle: nil), forCellReuseIdentifier: "WantedServiceCell")
        self.tblWantedServices.separatorStyle = .none
    }
    
    func setPagination() {
        self.getWantedList(showLoader: true)
        
        tblWantedServices.es.addInfiniteScrolling {
            self.refreshRecord()
        }
        
        tblWantedServices.es.resetNoMoreData()
        
        if let animator = self.tblWantedServices.footer?.animator as? ESRefreshFooterAnimator {
            animator.noMoreDataDescription = ""
        }
        
        tblWantedServices.es.addPullToRefresh {
            
//            if !(!self.isRefreshing) {
//                self.arrMyWantedServices.removeAll()
//                self.tblWantedServices.reloadData()
//                self.pageNo = 0
//                self.refreshRecord()
//            }
//            else {
//                self.tblWantedServices.es.stopPullToRefresh()
//            }
            self.pageNo = 0
            self.arrMyWantedServices.removeAll()
            self.tblWantedServices.reloadData()
            self.refreshRecord()
        }
    }
    
    func refreshRecord()
    {
        if self.isRefreshing == false
        {
            if self.pageNo == 0
            {
                self.getWantedList(showLoader: self.arrMyWantedServices.count==0 ?true :false)
            }
            else
            {
                self.getWantedList(showLoader: false)
            }
        }
    }
    
    //MARK: Call API
    
    func getWantedList(showLoader:Bool)
    {
        if showLoader
        {
            self.showProgressView()
        }
        
        pageNo += 1
        self.isRefreshing = true
        MyWantedServices.getMyWantedService(pageNo: pageNo, getAll: true ,numOfRecords: numOfRecords, isShowloader: showLoader, success: { (services,recordcnt,message) in
            
            self.recordCount = recordcnt
            self.tblWantedServices.tableFooterView?.isHidden = true
            self.tblWantedServices.tableFooterView?.frame.size = .zero
             self.isRefreshing = false
            
            if self.pageNo == 1
            {
                self.arrMyWantedServices.removeAll()
            }
            if services.count == 0
            {
                self.pageNo -= 1
            }
            else
            {
                if services.count < self.numOfRecords
                {
                    self.pageNo -= 1
                }
                
                if self.arrMyWantedServices.count == 0
                {
                    self.arrMyWantedServices.append(contentsOf: services)
                }
                else
                {
                    let responsRecord = services as NSArray
                    //let tempLocal = self.arrRequest as NSArray
                    responsRecord.enumerateObjects({ (objF, idxF, nil) in
                        let objService = objF as! MyWantedServices
                        if self.arrMyWantedServices.contains(where: {$0.requestId == objService.requestId}) {
                            //
                            print("it exists, do something")
                        } else {
                            //
                        print("item could not be found")
                        self.arrMyWantedServices.append(objService)
                        }
                        
                    })
                }
            }
            if self.arrMyWantedServices.count == 0 {
                self.lblNoDataFound.text = message
            }
            self.stopLoading()
            
            
        }) {
            
            self.stopLoading()
            self.tblWantedServices.tableFooterView?.isHidden = true
            self.tblWantedServices.tableFooterView?.frame.size = .zero
            
        }
    }
    
    func stopLoading()
    {
        self.hideProgressView()
        self.tblWantedServices.es.stopPullToRefresh()
        self.tblWantedServices.es.stopLoadingMore()
        self.tblWantedServices.reloadData()
        if self.recordCount == self.arrMyWantedServices.count {
            self.tblWantedServices.es.noticeNoMoreData()
        }
        self.lblNoDataFound.isHidden = arrMyWantedServices.count==0 ?false :true
    }
    
    func showProgressView(){
        self.vwProgress.isHidden = false
        self.vwProgress.startAnimating()
    }
    
    func hideProgressView(){
        self.vwProgress.isHidden = true
        self.vwProgress.stopAnimating()
    }
}

extension AllWantedServiceVC : UITableViewDelegate,UITableViewDataSource
{
    //MARK: - Tableview Method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrMyWantedServices.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Utilities.isDeviceiPad() ? 102.0 : 92.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WantedServiceCell", for: indexPath) as? WantedServiceCell
        
        Utilities.applyShadowEffect(view: [cell!.vwBG])
        
        let WantedServiceData = self.arrMyWantedServices[indexPath.row]
        
        if WantedServiceData.serviceImage != nil{
            cell?.imgServiceProfile.sd_setImage(with: URL.init(string: WantedServiceData.serviceImage!), placeholderImage: UIImage(named:Constant.PlaceHolder.kPhoto), options: SDWebImageOptions.retryFailed, completed: nil)
        }else{
            cell?.imgServiceProfile.image = UIImage.init(named: Constant.PlaceHolder.kPhoto)
        }
        
        cell?.lblServiceName.text = WantedServiceData.serviceName
        cell?.lblCategory.text =  WantedServiceData.catName
        cell?.lblDescription.text = WantedServiceData.serviceDescription
        
        cell?.btnDescriptionPopup.addTarget(self, action: #selector(self.handleClick(_ :)), for: .touchUpInside)
        if WantedServiceData.isServiceCreated == true {
            cell?.lblServiceAvaliable.isHidden = false
        }else {
            cell?.lblServiceAvaliable.isHidden = true
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let data = self.arrMyWantedServices[indexPath.row]
        let VC = RequestDetailVC(nibName: "RequestDetailVC", bundle: nil)
        VC.requestId = data.requestId
        let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
        navigationController.pushViewController(VC, animated: true)
    }
    
    @IBAction func handleClick(_ sender:UIButton){
        
        let point = sender.superview?.convert(sender.center, to: self.tblWantedServices) ?? .zero
        let indexPath = self.tblWantedServices.indexPathForRow(at: point)
        let data = self.arrMyWantedServices[(indexPath?.row)!]
        let desc = data.serviceDescription
        let heading = data.serviceName
        self.view.showSimpleDetailsPopup(topHeading: heading, infoContent: desc)
    }
    
}
