//
//  ActivityVC.swift
//  ShopByUber
//
//  Created by Administrator on 6/6/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class ActivityVC: BaseView{
    
    //MARK: IBOutlets
    
    @IBOutlet weak var tblNotification: UITableView!
    @IBOutlet var vw_Progress:    UIActivityIndicatorView!
    @IBOutlet var lblNoDataFound: UILabel!
    
    //MARK: - Variables
    var arr_activities  = [Activity]()
    var recordCount = 0
    var pageNo = 0
    var numOfRecords = 10
    var isRefreshing:Bool = false
    
    //MARK: View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialConfig()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if self.arr_activities.count == 0{
            
            self.setPagination()
        }
        if Singleton.sharedManager.isFromactivity == .ServiceDetails
        {
            self.arr_activities.removeAll()
            self.recordCount = 0
            self.pageNo = 0
            self.isRefreshing = false
            
            self.refreshRecord()
        }
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Private Methods
    
    func initialConfig(){
        self.tblNotification.register(UINib(nibName: "NotificationCell", bundle: nil), forCellReuseIdentifier: "NotificationCell")
        
        self.arr_activities.removeAll()
        
    }
    
    func setPagination() {
        self.getMyActivities(showLoader: true)
        
        tblNotification.es.addPullToRefresh {
            self.pageNo = 0
            self.arr_activities.removeAll()
            self.tblNotification.reloadData()
            self.refreshRecord()
        }
    }
    
    func refreshRecord()
    {
        if self.isRefreshing == false
        {
            if self.pageNo == 0
            {
                self.getMyActivities(showLoader: self.arr_activities.count==0 ?true :false)
            }
            else
            {
                self.getMyActivities(showLoader: false)
            }
        }
    }
    
    //MARK: - Call Api
    func getMyActivities(showLoader:Bool)
    {
        
        if showLoader
        {
            self.showProgressView()
        }
        pageNo += 1
        self.isRefreshing = true

        let dict = getRequestParamsForGetActivities(pageNo: pageNo, numOfRecords: numOfRecords)
        
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kYourActivity, inputParams: dict, success: { (dict) in
            self.tblNotification.tableFooterView?.isHidden = true
            self.tblNotification.tableFooterView?.frame.size = .zero
            
            if dict?["Result"] as? Bool == true {
                
                let recordCount = (dict?["Data"] as? [String:Any])?["recordCount"] as? Int
                self.recordCount = recordCount ?? 0
                let data = (dict?["Data"] as? [String:Any])?["activities"] as? NSArray
                if self.pageNo == 1
                {
                    self.arr_activities.removeAll()
                }
                if data != nil{
                    
                    if data!.count > 0{
                        for i in data!
                        {
                            let objActivity = Activity.init(fromDictionary: i as! [String : Any])
                            
                            self.arr_activities.append(objActivity)
                        }
                    }
                }
            }
            self.stopLoading()
        }) { error in
            
            self.stopLoading()
            
        }
    }
    
    func getRequestParamsForGetActivities(pageNo: Int?,numOfRecords:Int?) -> [String : Any]  {
        
        
        var dict = [String : Any]()
        
        dict["sessionId"]    = DataModel.sharedInstance.onFetchSessionId()
        dict["userId"]       = DataModel.sharedInstance.onFetchUserId()
        dict["pageNo"]       = pageNo
        dict["numOfRecords"] = numOfRecords
        
        return dict
    }
    
    //MARK: Custom Methods
    
    func showProgressView(){
        self.vw_Progress.isHidden = false
        self.vw_Progress.startAnimating()
    }
    
    func hideProgressView(){
        self.vw_Progress.isHidden = true
        self.vw_Progress.stopAnimating()
    }
    
    func stopLoading()
    {
        self.hideProgressView()
        self.isRefreshing = false
        self.tblNotification.es.stopPullToRefresh()
        self.tblNotification.reloadData()
        self.lblNoDataFound.isHidden = self.arr_activities.count==0 ?false :true
    }
}

extension ActivityVC: UITableViewDataSource, UITableViewDelegate
{
    //MARK: - Tableview Method
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constant.DeviceType.IS_IPAD || Constant.DeviceType.IS_IPAD_PRO ? 92 : 72
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arr_activities.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if !isRefreshing
        {
            if indexPath.row > self.arr_activities.count - 2 && self.recordCount > self.arr_activities.count{
                
                let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                
                self.tblNotification.tableFooterView = spinner
                self.tblNotification.tableFooterView?.isHidden = false
                refreshRecord()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! NotificationCell
        let notificationData = self.arr_activities[indexPath.row]
        if notificationData.serviceImage != nil{
            cell.imgService.sd_setImage(with: URL.init(string: notificationData.serviceImage!), placeholderImage: UIImage(named:"placeHolder"), options: SDWebImageOptions.retryFailed, completed: nil)
        }
        cell.lblServiceDetails.text = notificationData.screenName
        
      //  cell.nslclblServiceTitleTop.constant = 15
        cell.nslclblServiceDiscHight.constant = 0
        cell.lblServiceDesc.text    = notificationData.serviceDetail
        
        cell.lblVendortrack.text    = notificationData.message
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = self.arr_activities[indexPath.row]
        switch data.status {
            
        case 0,4,5,6:
            let vc = BuyerPurchaseDetailsVC(nibName: "BuyerPurchaseDetailsVC", bundle: nil)
            vc.requestID = data.requestId
            let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
            navigationController.pushViewController(vc, animated: true)
            
        case 1:
            
            let vc = PayForServiceVC(nibName: "PayForServiceVC", bundle: nil)
            let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
            vc.isFrom = trackProgress.isFromHome
            navigationController.pushViewController(vc, animated: true)
       
        case 2,3:
            
            let track = TrackProgressVC.init(nibName: "TrackProgressVC", bundle: nil)
            track.requestID = data.requestId!
            track.isFrom = trackProgress.isFromHome
            let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
            navigationController.pushViewController(track, animated: true)
        default:
            break
        }
    }
}
