//
//  CategoriesVC.swift
//  ShopByUber
//
//  Created by Administrator on 6/6/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class CategoriesVC: BaseView {
    
    //MARK: IBOutlet
    
    @IBOutlet var tblServices: UITableView!
    
    @IBOutlet var vw_Progress: UIActivityIndicatorView!
    @IBOutlet var lblNoDataFound: UILabel!
    
    //MARK: Variables
    var recordCount = 0
    var pageNo = 0
    var numOfRecords = 10
    var isRefreshing:Bool = false
    var arr_Categories = [Category]()
    var arr_SubCategories = [SubCategory]()
    var isFromInfinite : Bool = false
    var selectedIndexCat = -1
    
    //MARK: View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialConfig()
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if arr_Categories.count == 0
        {
            self.getCategories(showLoader: true)
            
            self.setPagination()
        }
        self.mapView.delegate = self

    }
    
    //MARK: IBAction
    
    
    //MARK: Private Methods
    
    func initialConfig(){
              
        self.tblServices.register(UINib(nibName: "ServiceCell", bundle: nil), forCellReuseIdentifier: "ServiceCell")
        
        self.setCurretnLocationMarker()
        
    }
    
    func setPagination() {
        tblServices.es.addPullToRefresh {
            self.pageNo = 0
            self.arr_Categories.removeAll()
            self.tblServices.reloadData()
            self.refreshRecord()
        }
    }
    
    func refreshRecord()
    {
        if self.isRefreshing == false
        {
            if self.pageNo == 0
            {
                self.getCategories(showLoader: self.arr_Categories.count==0 ?true :false)
            }
            else
            {
                self.getCategories(showLoader: false)
            }
        }
    }
    

    
    //MARK: API Call
    func getCategories(showLoader:Bool)
    {
        if showLoader
        {
            self.showProgressView()
        }
        
        pageNo += 1
        
        Category.callGetCategoriesAPI(pageNo: pageNo, numOfRecords: numOfRecords, success: { (categories,recordcnt) in
            
            self.hideProgressView()
            
            self.recordCount = recordcnt
            self.tblServices.tableFooterView?.isHidden = true
            self.tblServices.tableFooterView?.frame.size = .zero
            self.isRefreshing = false
            if self.pageNo == 1
            {
                self.arr_Categories.removeAll()
            }
            if categories.count == 0
            {
                self.pageNo -= 1
            }
            else
            {
                if categories.count < self.numOfRecords
                {
                    self.pageNo -= 1
                }
                
                if self.arr_Categories.count == 0
                {
                    self.arr_Categories.append(contentsOf: categories)
                    self.setPin(arrCategory: self.arr_Categories)
                }
                else
                {
                    let responsRecord = categories as NSArray
                    //let tempLocal = self.arrRequest as NSArray
                    responsRecord.enumerateObjects({ (objF, idxF, nil) in
                        let objCategory = objF as! Category
                        
                        if self.arr_Categories.contains(where: {$0.id == objCategory.id}) {
                            //
                            print("it exists, do something")
                        } else {
                            //
                            print("item could not be found")
                            self.arr_Categories.append(objCategory)
                            self.setPin(arrCategory: [objCategory])
                        }
                        
                    })
                }
            }
            
            self.stopLoading()
            
        }) {
            
            self.stopLoading()
            
        }
        
    }
    
    func setPin(arrCategory:[Category])
    {
        (arrCategory as NSArray).enumerateObjects({ (obj, idx, nil) in
            if let objCate = obj as? Category
            {
                (objCate.arrCompanyData as NSArray).enumerateObjects({ (objData, idxD, nil) in
                    if let data = objData as? CompanyData
                    {
                        self.setSellerMarker(data: data)
                    }
                })
            }
        })
    }
    
    
    func stopLoading()
    {
        self.hideProgressView()
        self.tblServices.es.stopPullToRefresh()
        self.tblServices.es.stopLoadingMore()
        self.tblServices.reloadData()
        self.lblNoDataFound.isHidden = arr_Categories.count==0 ?false :true
    }
    
    
    //MARK: Custom Methods
    
    func showProgressView(){
        self.vw_Progress.isHidden = false
        self.vw_Progress.startAnimating()
    }
    
    func hideProgressView(){
        self.vw_Progress.isHidden = true
        self.vw_Progress.stopAnimating()
    }
    
    
    
}

extension CategoriesVC:UITableViewDataSource,UITableViewDelegate
{
    //MARK: TableView Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arr_Categories.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceCell", for: indexPath) as! ServiceCell
        //Utilities.applyShadowEffect(view: [cell.vwBG])
        cell.vwBG.layer.borderWidth = 0.5
        
        
        let categoryData = self.arr_Categories[indexPath.row]
        
        if categoryData.categoryImage != nil{
            cell.serviceImage.sd_setImage(with: URL.init(string: categoryData.categoryImage!), placeholderImage: UIImage(named:Constant.PlaceHolder.kPhoto), options: SDWebImageOptions.retryFailed, completed: nil)
        }else{
            cell.serviceImage.image = UIImage.init(named: Constant.PlaceHolder.kPhoto)
        }
        
        cell.lblTitle.text = categoryData.catName
        cell.lblDescription.isHidden = true
        let yConstraint = NSLayoutConstraint(item: cell.lblTitle, attribute: .centerY, relatedBy: .equal, toItem: cell, attribute: .centerY, multiplier: 1, constant: -5)
        
        cell.addConstraint(yConstraint)
  
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let catID = self.arr_Categories[indexPath.row].id
        SubCategory.getSubSubcategories(categoryId: catID, allSubcategoryRequired: true, success: { (sub_Cat) in
            
            if sub_Cat.count == 0 {
                //No Data
            }
            else
            {
                self.arr_SubCategories.removeAll()
                self.arr_SubCategories.append(contentsOf: sub_Cat)
                let cat_Name = self.arr_SubCategories.map({ $0.subCatName! })
                
                //  Show Sub Category pop up
                self.view.showRadioBoxPopup(topHeading: Constant.PopupTitle.kSelectSubCategory, defaultSelectedIndex: 0, arrData: cat_Name ) { (selectedText, selectedIndex) in
                    
                    let subCategoryData = self.arr_SubCategories[selectedIndex]
                    let objMassageTherapyVC = MassageTherapyVC(nibName: "MassageTherapyVC", bundle: nil) as MassageTherapyVC
                    objMassageTherapyVC.categoryId = catID
                    objMassageTherapyVC.subCategoryID = subCategoryData.id
                    objMassageTherapyVC.strTitle = subCategoryData.subCatName!
                    objMassageTherapyVC.currentZipCode = ""
                    let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
                    navigationController.pushViewController(objMassageTherapyVC, animated: true)
                }
            }
            
        }) {_ in 
            //Alert
            print("API Call Fail")
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      //  return Utilities.isDeviceiPad() ? 92.0 : 72.0
        
        return Utilities.isDeviceiPad() ? 82.0 : 62.0
    }
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        if !isRefreshing
//        {
//            if indexPath.row > self.arr_Categories.count - 2 && self.recordCount > self.arr_Categories.count{
//                
//                let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
//                spinner.startAnimating()
//                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
//                
//                self.tblServices.tableFooterView = spinner
//                self.tblServices.tableFooterView?.isHidden = false
//                refreshRecord()
//            }
//        }
//        
//    }

}
