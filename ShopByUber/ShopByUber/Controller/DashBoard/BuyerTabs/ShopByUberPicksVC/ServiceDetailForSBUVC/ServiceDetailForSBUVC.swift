//
//  ServiceDetailForSBUVC.swift
//  ShopByUber
//
//  Created by Differenz215 on 8/11/17.
//  Copyright © 2017 Administrator. All rights reserved.
//


import UIKit
import Cosmos

class ServiceDetailForSBUVC: BaseView, UITableViewDelegate,UITableViewDataSource,ShopByUberPicksServiceCellDelegate,UINavigationControllerDelegate,GMSAutocompleteViewControllerDelegate{
    
    //MARK:- IBOutlet
    @IBOutlet weak var tblReviews: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    
    //Reviews Outlets
    @IBOutlet weak var vw_Reviews: UIView!
    @IBOutlet weak var cosmos_Ratings: CosmosView!
    @IBOutlet weak var nsLayoutServiceTable: NSLayoutConstraint!
    @IBOutlet weak var nslayoutReviewViewHeight: NSLayoutConstraint!
    @IBOutlet weak var tblService: UITableView!
    
    @IBOutlet weak var lblNoDataFound: UILabel!
    //MARK: - Variables
    var serviceData: CompanyServiceList?
    var companyService = [UberServiceDetails]()
    var arr_Reviews = [ReviewsModel]()
    var categoryID: String?
    var companyID : String?
    var indexpathIndex: Int?
    var addressData = [[String:Any]]()
    
    //Pagenation Related
    var recordCount = 0
    var pageNo = 0
    var numOfRecords = 5
    var isRefreshing:Bool = false
    var screenName = String()
    let notify = UILocalNotification()
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialConfig()
        
    }
    
    //MARK:- Private Methods
    func initialConfig(){
        
        self.tblReviews.register(UINib(nibName: "ReviewCell", bundle: nil), forCellReuseIdentifier: "ReviewCell")
        self.tblService.register(UINib(nibName: "ShopByUberPicksServiceCell", bundle: nil), forCellReuseIdentifier: "ShopByUberPicksServiceCell")
        self.tblService.register(UINib(nibName: "NoDayTimeAvailable", bundle: nil), forCellReuseIdentifier: "NoDayTimeAvailable")
        
        tblService.rowHeight = UITableViewAutomaticDimension
        tblReviews.rowHeight = UITableViewAutomaticDimension
        self.lblTitle.text = self.screenName
        
        self.callGetMassgeTherapyDetailsAPI(showLoader: true)
        
    }
    
    
    func refreshRecord()
    {
        if self.isRefreshing == false
        {
            if self.pageNo == 0
            {
                self.callGetMassgeTherapyDetailsAPI(showLoader: self.companyService.count==0 ?true :false)
            }
            else
            {
                self.callGetMassgeTherapyDetailsAPI(showLoader: false)
            }
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        Singleton.sharedManager.currentView = nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillLayoutSubviews() {
        self.nsLayoutServiceTable.constant = self.tblService.contentSize.height
        self.nslayoutReviewViewHeight.constant = 99 + self.tblReviews.contentSize.height
        self.view.layoutIfNeeded()
    }
    
    //MARK: - Service Detail Delegate
    func ServiceDetailCellDelegate(cell: ShopByUberPicksServiceCell, buttonType: UIButton) {
        
        if(buttonType == cell.btnServiceFavourite )
        {
            KVNProgress.show()
            let service = self.companyService[cell.tag]
            var dict = [String : Any]()
            dict["sessionId"]    = DataModel.sharedInstance.onFetchSessionId()
            dict["userId"]       = DataModel.sharedInstance.onFetchUserId()
            dict["serviceId"]    = service.serviceId
            
            APIManager.callAPI(url: Constant.serverAPI.URL.kAddToFavoriteService, inputParams: dict, success: { (dict) in
                
                
                if dict?["Result"] as? Bool == true {
                    
                    if service.isFavourite == false
                    {
                        service.isFavourite = true
                        cell.btnServiceFavourite.setImage(#imageLiteral(resourceName: "favorited"), for: .normal)
                        
                    }
                    else
                    {
                        service.isFavourite = false
                        cell.btnServiceFavourite.setImage(#imageLiteral(resourceName: "unfavorited"), for: .normal)
                        
                    }
                    
                    KVNProgress.showSuccess(withStatus: dict?["Message"] as! String)
                }
                
            }) { error in
                KVNProgress.dismiss()
                Utilities.showAlertView(title: "", message: error)
                
            }
            
        }
        
        if(buttonType == cell.btnHeaderFavourite)
        {
            KVNProgress.show()
            var dict = [String : Any]()
            dict["sessionId"]    = DataModel.sharedInstance.onFetchSessionId()
            dict["userId"]       = DataModel.sharedInstance.onFetchUserId()
            dict["companyId"]    = self.serviceData?.companyId
            
            APIManager.callAPI(url: Constant.serverAPI.URL.kAddToFavoriteCompany, inputParams: dict, success: { (dict) in
                
                
                if dict?["Result"] as? Bool == true {
                    if self.serviceData?.isFavourite == false
                    {
                        self.serviceData?.isFavourite = true
                        cell.btnHeaderFavourite.setImage(#imageLiteral(resourceName: "favorited"), for: .normal)
                        
                    }
                    else
                    {
                        self.serviceData?.isFavourite = false
                        cell.btnHeaderFavourite.setImage(#imageLiteral(resourceName: "unfavorited"), for: .normal)
                        
                    }
                    
                    KVNProgress.showSuccess(withStatus: dict?["Message"] as! String)
                }
                
            }) { error in
                KVNProgress.dismiss()
                Utilities.showAlertView(title: "", message: error)
                
            }
        }
        
        if(buttonType == cell.btnInfo)
        {
           // let service = self.companyService[cell.tag]
            let service = self.serviceData
            self.view.showSimpleDetailsPopup(topHeading: service?.screenName, infoContent: service?.companyDescription)
        }
    }
    
    func ServiceDetailCellWithDataDelegate(cell:ShopByUberPicksServiceCell,buttonType:UIButton,arrTimeSlot: TimeSlot)
    {
        if(buttonType == cell.btnSendBookRequest )
        {
            let addressDict = self.addressData[cell.btnSendBookRequest.tag]
            if (addressDict["address"] as? String) == ""
            {
                Utilities.showAlertView(title: "", message: Constant.ValidationMessage.kAddress)
            }
            else
            {
                
                KVNProgress.show()
                let service = self.companyService[cell.tag]
                var dict = [String : Any]()
                dict["sessionId"]            = DataModel.sharedInstance.onFetchSessionId()
                dict["userId"]               = DataModel.sharedInstance.onFetchUserId()
                dict["serviceId"]            = service.serviceId
                dict["appointmentTimeId"]    = [arrTimeSlot.timeslotId]
                dict["status"]               = 0
                dict["requestFee"]           = service.flatFee
                dict["userInfo"]             = self.addressData[cell.tag]
                dict["requestId"]            = ""
               
                APIManager.callAPI(url: Constant.serverAPI.URL.kSendRequestToBook, inputParams: dict, success: { (dict) in
                    
                     self.showLocalNotification() //Local notification
                    if dict?["Result"] as? Bool == true {
                        Utilities.showAlertWithAction(title: "", message: (dict?["Message"] as? String)!, buttonTitle: "OK", buttonTitle2: "", onOKClick: {
                            Singleton.sharedManager.isFromactivity = .ServiceDetails
                            if self.segmentControl != nil
                            {
                                self.segmentControl.selectedIndex = 3
                            }
                            self.navigationController?.popToRootViewController(animated: true)
                            appDeletgate.dismissBubble()
                            
                        }, onCancelClick: {
                            
                        })
                        
                        
                    }
                    KVNProgress.dismiss()
                }) { error in
                    KVNProgress.dismiss()
                    
                    Utilities.showAlertView(title: "", message: error)
                    
                }
            }
        }
        
    }
    
    func ServiceDetailCellBookNowDelegate(cell: ShopByUberPicksServiceCell, buttonType: UIButton) {
        if(buttonType == cell.btnBookNow )
        {    let addressDict = self.addressData[cell.tag]
            if (addressDict["address"] as? String) == ""
            {
                Utilities.showAlertView(title: "", message: Constant.ValidationMessage.kAddress)
            }
            else
            {
                
                
                let service = self.companyService[cell.tag]
                
  
                    
                    let rootView = UIApplication.shared.delegate?.window??.rootViewController as? UINavigationController
                    let topView = UIApplication.topViewController(base: rootView)
                    let alert = UIAlertController(title: "Are you Sure?", message: "Would you like the Service Provider to come to your location now?", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alert.addAction(UIAlertAction (title: "Yes", style: .default, handler: { (alert) in
                        
                        KVNProgress.show()

                
                var dict = [String : Any]()
                dict["sessionId"]            = DataModel.sharedInstance.onFetchSessionId()
                dict["userId"]               = DataModel.sharedInstance.onFetchUserId()
                dict["serviceId"]            = service.serviceId
                dict["appointmentTimeId"]    = [""]
                dict["status"]               = 0
                dict["requestFee"]           = service.flatFee
                dict["userInfo"]             = self.addressData[cell.tag]
                dict["requestId"]            = ""
                        
                APIManager.callAPI(url: Constant.serverAPI.URL.kSendRequestToBook, inputParams: dict, success: { (dict) in
                    
                    if dict?["Result"] as? Bool == true {
                        
                        self.showLocalNotification() //Local notification
                        Utilities.showAlertWithAction(title: "", message: (dict?["Message"] as? String)!, buttonTitle: "OK", buttonTitle2: "", onOKClick: {
                            Singleton.sharedManager.isFromactivity = .ServiceDetails
                            if self.segmentControl != nil
                            {
                                self.segmentControl.selectedIndex = 3
                            }
                            self.navigationController?.popToRootViewController(animated: true)
                            appDeletgate.dismissBubble()
                            
                        }, onCancelClick: {
                            
                        })
                    }
                    else {
                        
                        Utilities.showAlertWithAction(title: "", message: (dict?["Message"] as? String)!, buttonTitle: "OK", buttonTitle2: "", onOKClick: {
                            
                        }, onCancelClick: {
                            
                        })
                        
                    }
                    KVNProgress.dismiss()
                }) { error in
                    KVNProgress.dismiss()
                    
                    Utilities.showAlertView(title: "", message: error)
                    
                }
                    }))
                    alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
                    topView?.present(alert, animated: true, completion: nil)
//            }
//            else
//                {
//                
//                    KVNProgress.dismiss()
//                    
//                    Utilities.showAlertView(title: "", message: "You have already sent a request to the Provider to book this service. If you would like to request a new time period, please cancel the current request by visiting \"Your Activity\" and come back to the service screen to book a new appointment time. ")
//            }
            }
        }
    }
    
    func showLocalNotification() {
        //set title and body
        notify.alertTitle = Constant.appTitle.strTitle
        notify.fireDate = NSDate(timeIntervalSinceNow: 1) as Date
        notify.alertBody = "Your appointment request has been sent to the vendor."
        notify.soundName = UILocalNotificationDefaultSoundName
        notify.category = "request"
        UIApplication.shared.scheduleLocalNotification(notify)
    }
    
    func segmentForLocationValueChange(cell: ShopByUberPicksServiceCell, segment: UISegmentedControl) {
        
        if segment.selectedSegmentIndex == 0
        {
            
            let attRequest = NSMutableAttributedString(string: DataModel.sharedInstance.onFetchAddressLines())
            cell.lblLocation.attributedText = attRequest
            cell.lblLocation.sizeToFit()
            
            let dict = ["address":DataModel.sharedInstance.onFetchAddressLines(),"latitude":DataModel.sharedInstance.onFetchLatitude(),"longitude":DataModel.sharedInstance.onFetchLongtitude()] as [String : Any]
            self.addressData[cell.tag] = dict
            
        }
        else
        {
            self.indexpathIndex = cell.tag
            let autocompleteController = GMSAutocompleteViewController()
            autocompleteController.delegate = self
            present(autocompleteController, animated: true, completion: nil)
        }
    }
    
    
    
    //MARK: - Auto Complete Google Delegates
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        if let addressLines = place.addressComponents {
            // Populate all of the address fields we can find.
            
            for field in addressLines {
                
                switch field.type {
                case kGMSPlaceTypePostalCode:
                    //  self.zipcode = field.name
                    break
                default:
                    //print("Type: \(field.type), Name: \(field.name)")
                    print("")
                }
            }
        }
        
        let attRequest = NSMutableAttributedString(string: place.formattedAddress! )
        let cell = tblService.cellForRow(at: IndexPath.init(row: self.indexpathIndex!, section: 0)) as! ShopByUberPicksServiceCell
        let dict = ["address":place.formattedAddress!,"latitude":place.coordinate.latitude,"longitude":place.coordinate.longitude] as [String : Any]
        self.addressData[self.indexpathIndex!] = dict
        cell.lblLocation.attributedText = attRequest
        cell.lblLocation.sizeToFit()
        dismiss(animated: true, completion: nil)
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
        
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController)
    {
        
        viewController.dismiss(animated: true, completion: nil)
        
    }
    
    
    //MARK:  - TableView Methods
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == self.tblService
        {
            let service = self.companyService[indexPath.row]
            if service.serviceAvailable!
            {
                if service.availableTimeSlot.count != 0
                {
                    return Utilities.isDeviceiPad() ? 1280 : 920
                }
                else
                {
                    return Utilities.isDeviceiPad() ? 1210 : 830
                }
            }
            else
            {
                return Utilities.isDeviceiPad() ? 1080 : 810
            }
        }
        else
        {
            let font = UIFont.systemFontLight(with: 15)
            let review = self.arr_Reviews[indexPath.row]
            let serviceName = Utilities.heigth(from: review.Feedback!, with: font, width:self.view.frame.size.width - 40)
            return 60 + serviceName
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == self.tblService
        {
                return UITableViewAutomaticDimension
        }
        else
        {
            let font = UIFont.systemFontLight(with: 15)
            let review = self.arr_Reviews[indexPath.row]
            let serviceName = Utilities.heigth(from: review.Feedback!, with: font, width:self.view.frame.size.width - 40)
            return 60 + serviceName
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView == tblService
        {
            if !isRefreshing
            {
                if indexPath.row > self.companyService.count - 2 && self.recordCount > self.companyService.count{
                    
                    let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
                    spinner.startAnimating()
                    spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                    
                    self.tblService.tableFooterView = spinner
                    self.tblService.tableFooterView?.isHidden = false
                    refreshRecord()
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tblService
        {
            return self.companyService.count
        }
        else
        {
            return self.arr_Reviews.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->
        UITableViewCell {
          
            
            if tableView == self.tblService
            {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "ShopByUberPicksServiceCell", for: indexPath) as! ShopByUberPicksServiceCell
                let service = self.companyService[indexPath.row]
                if self.serviceData?.companyProfileImage != nil{
                    cell.imgHeader.sd_setImage(with: URL.init(string: (self.serviceData?.companyProfileImage)!), placeholderImage: UIImage(named: Constant.PlaceHolder.kPhoto), options: SDWebImageOptions.retryFailed, completed:{ (image, err, nil, url) in
                        
                    })
                }
                cell.lblOfferDetails.text = service.serviceDetail
                cell.lblOfferDetails.sizeToFit()
                
                Utilities.applyShadowEffect(view: [cell.vwOfferDetails])

                
                cell.lblHeaderCompanyTitle.text = serviceData?.comapnyScreenName
                cell.lblHeaderDesc.text = service.catName

                if serviceData?.isFavourite == false
                {
                    cell.btnHeaderFavourite.setImage(#imageLiteral(resourceName: "unfavorited"), for: .normal)
                }
                else
                {
                    cell.btnHeaderFavourite.setImage(#imageLiteral(resourceName: "favorited"), for: .normal)
                }
                if service.isFavourite == false
                {
                    cell.btnServiceFavourite.setImage(#imageLiteral(resourceName: "unfavorited"), for: .normal)
                }
                else
                {
                    cell.btnServiceFavourite.setImage(#imageLiteral(resourceName: "favorited"), for: .normal)
                }
                
                if service.serviceImage != nil{
                    cell.imgServiceHead.sd_setImage(with: URL.init(string: (service.serviceImage)!), placeholderImage: UIImage(named:""), options: SDWebImageOptions.retryFailed, completed:{ (image, err, nil, url) in
                        
                    })
                }
                cell.imgServiceHead.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleBottomMargin, .flexibleRightMargin, .flexibleLeftMargin, .flexibleTopMargin]
                cell.imgServiceHead.contentMode = UIViewContentMode.scaleAspectFill
                cell.imgServiceHead.clipsToBounds = true
                
                cell.serviceAvailable = service.serviceAvailable  //ASSIGNING BOOLEAN VALUE TO CELL VARIABLE
                
                //Service Headview Height as Content
                cell.lblServiceTitle.text = service.screenName
                cell.lblPriceService.text = "$ " + service.flatFee!
                
                
                cell.tag = indexPath.row
                cell.delegate = self
                
                //Vendor address
                let vendoraddress = service.companyAddress
                var vendorAdd = NSMutableAttributedString(string: vendoraddress! as String)
                vendorAdd = Utilities.decorateText(attributedString: vendorAdd, decorateString: "Vendor's Location : ", decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: Utilities.isDeviceiPad() ? 14 : 16),NSForegroundColorAttributeName:Constant.Color.kSegmentSelectColor])
                
                print("Addreass",vendorAdd)
             //   cell.lblVendorLocation.attributedText = vendorAdd
                print("Addreass",vendorAdd)
                if vendoraddress == "" {
                    cell.nslcViewCompanyHeaderTop.constant = Utilities.isDeviceiPad() ? 30 : 20
                }else {
                    cell.lblVendorAddress.text = vendoraddress?.uppercased()
                    cell.nslcViewCompanyHeaderTop.constant = Utilities.isDeviceiPad() ? 60 : 50
                }
               
                
                print("Addreass",vendorAdd)
                
                print("Des add======>",String(describing: vendorAdd))
              //  cell.lblVendorLocation.sizeToFit()
                cell.lblVendorAddress.sizeToFit()
                
                
                //Buyer address
                let address = self.addressData[indexPath.row]
                let attRequest = NSMutableAttributedString(string:(address["address"] as! String))
                cell.lblLocation.attributedText = attRequest
                cell.lblLocation.sizeToFit()
                
                if service.availableTimeSlot.count == 0 && service.serviceAvailable == false {
                    
                    
                    cell.constraintTblSlotHeight.constant = 80
                    cell.lblAvailibility.text = "Available by Appointment"
                    cell.lblAvailibility.textColor = Utilities.colorWithHexString("#29A036")
                    cell.constraintBtnBookNow.constant = 0.0
                    cell.btnBookNow.isHidden = true
                    cell.nslcBtnRequestTop.constant = Utilities.isDeviceiPad() ? 20 :10
                    cell.nslcSegmentTop.constant = Utilities.isDeviceiPad() ? 120 : 100
                }
                else
                {
                    
                if service.availableTimeSlot.count != 0
                {
                    cell.selectedTimeSlot = service.availableTimeSlot[0]
                    cell.arrTimeSlots = service.availableTimeSlot
                    cell.tbl_TimeSlot.reloadData()
                    cell.constraintTblSlotHeight.constant = Utilities.isDeviceiPad() ? 100 : 80
                    cell.nslcSegmentTop.constant = Utilities.isDeviceiPad() ? 120 : 100
                }
                else
                {                    
                    cell.selectedTimeSlot = nil
                    cell.arrTimeSlots = [TimeSlot]()
                    cell.constraintTblSlotHeight.constant = 0
                    cell.tbl_TimeSlot.isHidden = true
                    cell.nslcSegmentTop.constant = 10
                    cell.layoutIfNeeded()
                }
                
                if (service.serviceAvailable)!
                {
                    cell.lblAvailibility.text = "Available Now"
                    cell.lblAvailibility.textColor = Utilities.colorWithHexString("#FA8606")
                    cell.constraintBtnBookNow.constant = 40
                    cell.btnBookNow.isHidden = false
                }
                else
                {
                    
                    cell.lblAvailibility.text = "Available by Appointment"
                    cell.lblAvailibility.textColor = Utilities.colorWithHexString("#29A036")
                    cell.constraintBtnBookNow.constant = 0.0
                    cell.btnBookNow.isHidden = true
                    cell.nslcBtnRequestTop.constant = 10
           
                }
                }
                
                if indexPath.row == self.companyService.count - 1 {
                    
                    //Hide Seperator
                    cell.vwSeperatorLine.isHidden = true
                }
                
                cell.layoutIfNeeded()
           
                return cell
            }
            else if tableView == self.tblReviews
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewCell", for: indexPath) as! ReviewCell
                let review = self.arr_Reviews[indexPath.row]
                cell.lblDate.text = review.feedbackDateTime
                cell.lblNameReviewBy.text = review.givenByUser
                cell.lblReview.text = review.Feedback
                
                return cell
            }
            
            
            return UITableViewCell()
    }
    
    //MARK:- IBAction
    
    
    @IBAction func BackClicked(_ sender: Any) {
        let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
        navigationController.popViewController(animated: true)
    }
    
    //MARK: - API Call
    func callGetMassgeTherapyDetailsAPI(showLoader:Bool){
        
        KVNProgress.show()
        
        pageNo += 1
        
        let dict = getRequestParamsForGetCategories(pageNo: pageNo, numOfRecords: numOfRecords)
        
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kGetPicksCompanyService, inputParams: dict, success: { (dict) in
            
            KVNProgress.dismiss()
            self.tblService.tableFooterView?.isHidden = true
            self.tblService.tableFooterView?.frame.size = .zero
            
            if dict?["Result"] as? Bool == true {
                if dict?["Data"] != nil
                {
                    let data = dict?["Data"] as? [String:Any]
                    if self.pageNo == 1
                    {
                        self.companyService.removeAll()
                    }
                    
                    if data?["companyInfo"] != nil
                    {
                        self.serviceData = CompanyServiceList.init(dict: data?["companyInfo"] as! [String : Any])
                        
                    }
                    let services = data?["services"] as? NSArray
                    if services?.count != 0
                    {
                        for i in services!
                        {
                            let service = UberServiceDetails.init(fromDictionary: i as! [String : Any])
                            self.companyService.append(service)
                            service.arrServiceTimeSlot = NSMutableArray()
                            if service.availableTimeSlot.count != 0
                            {
                                service.arrServiceTimeSlot.add(service.availableTimeSlot[0])
                            }
                            let dict = ["address":DataModel.sharedInstance.onFetchAddressLines(),"latitude":DataModel.sharedInstance.onFetchLatitude(),"longitude":DataModel.sharedInstance.onFetchLongtitude()] as [String : Any]
                            self.addressData.append(dict)
                        }
                        // Tableview height as per number of Confirm Request in cart
                        self.tblService.reloadData()
                     //   self.nsLayoutServiceTable.constant = self.tblService.contentSize.height
                    }
                    
                    self.tblService.reloadData()
                    let reviews = data?["reviews"] as? NSArray
                    if reviews?.count != 0
                    {
                        for i in reviews!
                        {
                            let review = ReviewsModel.init(dict: i as! [String : Any])
                            self.arr_Reviews.append(review)
                        }
                        self.tblReviews.reloadData()
                        
                    }
                    self.cosmos_Ratings.rating = (data?["overAllRating"] as? Double)!
                    self.vw_Reviews.isHidden = false
                    self.view.layoutIfNeeded()
                    self.nsLayoutServiceTable.constant = self.tblService.contentSize.height

                }
                else
                {
                    self.stopLoading()
                    Utilities.showAlertView(title: "", message: dict?["Message"] as? String)
                }
            }
            else
            {
                self.stopLoading()
                Utilities.showAlertView(title: "", message: dict?["Message"] as? String)
                
            }
            
        }) { error in
            self.stopLoading()
            Utilities.showAlertView(title: "", message: error)
            
        }
    }
    
    func getRequestParamsForGetCategories(pageNo: Int?,numOfRecords:Int?) -> [String : Any]  {
        
        
        var dict = [String : Any]()
        
        dict["pageNo"]       = pageNo
        dict["numOfRecords"] = numOfRecords
        dict["sessionId"]    = DataModel.sharedInstance.onFetchSessionId()
        dict["userId"]       = DataModel.sharedInstance.onFetchUserId()
        dict["companyId"]    = self.companyID
        return dict
    }
    
    func stopLoading()
    {
        self.tblService.es.stopPullToRefresh()
        self.tblService.es.stopLoadingMore()
        self.tblService.reloadData()
        self.lblNoDataFound.isHidden = companyService.count==0 ?false :true
        
    }
}
