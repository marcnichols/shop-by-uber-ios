//
//  ShopByUberPicksVC.swift
//  ShopByUber
//
//  Created by Administrator on 6/5/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class ShopByUberPicksVC: UIViewController {

    //MARK: IBOutlet
    
    @IBOutlet var tbl_Services: UITableView!
    @IBOutlet var vw_Progress: UIActivityIndicatorView!
    @IBOutlet var lblNoDataFound: UILabel!
    
    //MARK: Variables
    
    var arr_PicksListing = [UberPicks]()
    var recordCount = 0
    var pageNo = 0
    var numOfRecords = 10
    var isRefreshing:Bool = false
    
    //MARK: View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initialConfig()
        
    }
  
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if self.arr_PicksListing.count == 0{
            
            self.setPagination()
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setPagination() {
        self.getMyServices(showLoader: true)
        
        tbl_Services.es.addInfiniteScrolling {
            self.refreshRecord()
        }
        tbl_Services.es.resetNoMoreData()
        
        if let animator = self.tbl_Services.footer?.animator as? ESRefreshFooterAnimator {
            animator.noMoreDataDescription = ""
        }
        
        tbl_Services.es.addPullToRefresh {
            self.pageNo = 0
            self.arr_PicksListing.removeAll()
            self.tbl_Services.reloadData()
            self.refreshRecord()
        }
    }
    
    func refreshRecord()
    {
        if self.isRefreshing == false
        {
            if self.pageNo == 0
            {
                self.getMyServices(showLoader: self.arr_PicksListing.count==0 ?true :false)
            }
            else
            {
                self.getMyServices(showLoader: false)
            }
        }
    }
    
    //MARK: Call API
    func getMyServices(showLoader:Bool)
    {
        if showLoader
        {
            self.showProgressView()
        }
        
        self.pageNo += 1
        UberPicks.callGetUberPicksAPI(pageNo: pageNo, numOfRecords: numOfRecords, success: { (companyListing,recordcnt) in
            
            self.recordCount = recordcnt
            self.tbl_Services.tableFooterView?.isHidden = true
            self.tbl_Services.tableFooterView?.frame.size = .zero
            
            
            self.isRefreshing = false
            if self.pageNo == 1
            {
                self.arr_PicksListing.removeAll()
            }
            if companyListing.count == 0
            {
                self.pageNo -= 1
            }
            else
            {
                if companyListing.count < self.numOfRecords
                {
                    self.pageNo -= 1
                }
                
                if self.arr_PicksListing.count == 0
                {
                    self.arr_PicksListing.append(contentsOf: companyListing)
                }
                else
                {
                    let responsRecord = companyListing as NSArray
                    responsRecord.enumerateObjects({ (objF, idxF, nil) in
                        let objPiks = objF as! UberPicks
                        
                        if self.arr_PicksListing.contains(where: {$0.id == objPiks.id}) {
                            //
                            print("it exists, do something")
                        } else {
                            //
                            print("item could not be found")
                            self.arr_PicksListing.append(objPiks)
                        }
                        
                    })
                }
            }
            self.stopLoading()
        }) {
            self.stopLoading()
            self.tbl_Services.tableFooterView?.isHidden = true
            self.tbl_Services.tableFooterView?.frame.size = .zero
            
        }

    }
    
    func stopLoading()
    {
        self.hideProgressView()
        self.tbl_Services.es.stopPullToRefresh()
        self.tbl_Services.es.stopLoadingMore()
        if self.recordCount == self.arr_PicksListing.count {
            self.tbl_Services.es.noticeNoMoreData()
        }
        self.tbl_Services.reloadData()
        self.lblNoDataFound.isHidden = arr_PicksListing.count==0 ?false :true
    }
    

    //MARK: Private Methods
    func initialConfig(){
        self.tbl_Services.register(UINib(nibName: "ServiceCell", bundle: nil), forCellReuseIdentifier: "ServiceCell")
        
    }
    
    
    //MARK: Custom Methods
    
    func showProgressView(){
        self.vw_Progress.isHidden = false
        self.vw_Progress.startAnimating()
    }
    
    func hideProgressView(){
        self.vw_Progress.isHidden = true
        self.vw_Progress.stopAnimating()
    }
    
}

extension ShopByUberPicksVC : UITableViewDataSource,UITableViewDelegate
{
    //MARK: TableView Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr_PicksListing.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceCell", for: indexPath) as? ServiceCell
        Utilities.applyShadowEffect(view: [cell!.vwBG])
        
        let data = self.arr_PicksListing[indexPath.row]
        
        if data.imageURL != nil{
            cell?.serviceImage.sd_setImage(with: URL.init(string: data.imageURL!), placeholderImage: UIImage(named:Constant.PlaceHolder.kPhoto), options: SDWebImageOptions.retryFailed, completed: nil)
        }else{
            cell?.serviceImage.image = UIImage.init(named: Constant.PlaceHolder.kPhoto)
        }
        
        //cell?.lblTitle.text = data.companyName  // Removed Company Name
        cell?.lblTitle.text = data.comapnyScreenName
        cell?.nslcLblCatagoryTypeHight.constant = Utilities.isDeviceiPad() ? 22:18
        cell?.nslclblDescHight.constant = 0
        cell?.lblCatagoryType.text = data.categories
       // cell?.lblDescription.text = data.compDescription //Removed Description
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if !isRefreshing
        {
            if indexPath.row > self.arr_PicksListing.count - 2 && self.recordCount > self.arr_PicksListing.count{
                
                let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                
                self.tbl_Services.tableFooterView = spinner
                self.tbl_Services.tableFooterView?.isHidden = false
                refreshRecord()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let Data = self.arr_PicksListing[indexPath.row]
        let objServiceDetailForSBUVC = ServiceDetailForSBUVC(nibName: "ServiceDetailForSBUVC", bundle: nil) as ServiceDetailForSBUVC
        objServiceDetailForSBUVC.companyID    = Data.id
        objServiceDetailForSBUVC.screenName   = Data.companyName!
        let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
        navigationController.pushViewController(objServiceDetailForSBUVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Utilities.isDeviceiPad() ? 92.0 : 72.0
    }
    

}
