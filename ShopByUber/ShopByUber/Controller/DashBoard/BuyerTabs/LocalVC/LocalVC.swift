//
//  LocalVC.swift
//  ShopByUber
//
//  Created by Administrator on 6/6/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class LocalVC: BaseView {
    
    //MARK: IBOutlet
    
    @IBOutlet var tblServices: UITableView!
    @IBOutlet var vw_Progress: UIActivityIndicatorView!
    @IBOutlet var lblNoDataFound: UILabel!
    
    //MARK: Variables
    let locationManager = CLLocationManager()
    var didFindMyLocation = true
    var currentZipCode = ""

    //MARK: API Variables
    var recordCount = 0
    var pageNo = 0
    var arr_LocalCategories = [Category]()
    var arr_Local = [Local]()
    var arr_SubCategories = [SubCategory]()
    var numOfRecords = 10
    var isRefreshing : Bool = false
    var markerLocal = [GMSMarker]()
    var isFromInfinite : Bool = false
    
    //MARK: View Life Cycle
    
    override func viewDidLoad() {
         print(DataModel.sharedInstance.onFetchLatitude())
        
        super.viewDidLoad()
        self.initialConfig()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        
        if self.arr_LocalCategories.count == 0{
          
            self.getLocalData(showLoader: true)
            self.setPagination()
        }
        self.mapView.delegate = self
    }
  
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Private Methods
    
    func initialConfig(){
        
        self.tblServices.register(UINib(nibName: "ServiceCell", bundle: nil), forCellReuseIdentifier: "ServiceCell")
        self.tblServices.tableFooterView = UIView(frame: CGRect.zero)
        self.setCurretnLocationMarker()
      
       
    }
    
    func setPagination() {
       
//        tblServices.es.addInfiniteScrolling {
//            self.refreshRecord()
//
//        }
        
        tblServices.es.addPullToRefresh {
            self.pageNo = 0
            self.arr_LocalCategories.removeAll()
            self.tblServices.reloadData()
            self.refreshRecord()
        }
    }
    
    func refreshRecord()
    {
        if self.isRefreshing == false
        {
            if self.pageNo == 0
            {
                self.getLocalData(showLoader: self.arr_Local.count == 0 ? true: false)
            }
            else
            {
                self.getLocalData(showLoader: false)
            }
        }
    }
    
    //MARK: Call API
    func getLocalData(showLoader:Bool)
    {
        if showLoader
        {
            self.showProgressView()
        }
        self.pageNo += 1
        Local.callGetLocalAPI(pageNo: self.pageNo, numOfRecords: self.numOfRecords, success: { (locals,recordcnt) in
            
            self.hideProgressView()
            
            self.recordCount = recordcnt
            
            self.tblServices.tableFooterView?.isHidden = true
            self.tblServices.tableFooterView?.frame.size = .zero
            
            
            self.isRefreshing = false
            if self.pageNo == 1
            {
                self.arr_Local.removeAll()
            }
            if locals.count == 0
            {
                self.pageNo -= 1
            }
            else
            {
                if locals.count < self.numOfRecords
                {
                    self.pageNo -= 1
                }
                
                if self.arr_Local.count == 0
                {
                    self.arr_Local.append(contentsOf: locals)
                    self.mapView.clear()
                    self.setPin(arrlocal: self.arr_Local)
                }
                else
                {
                    let responsRecord = locals as NSArray
                    //let tempLocal = self.arrRequest as NSArray
                    responsRecord.enumerateObjects({ (objF, idxF, nil) in
                        let objlocal = objF as! Local
                        
                        if self.arr_Local.contains(where: {$0.id == objlocal.id}) {
                            //
                            print("it exists, do something")
                        } else {
                            //
                            print("item could not be found")
                            self.arr_Local.append(objlocal)
                            self.setPin(arrlocal: [objlocal])
                        }
                        
                    })
                }
            }
            
            self.stopLoading()
            self.tblServices.es.removeRefreshFooter()
            self.tblServices.tableFooterView?.isHidden = true
            self.tblServices.tableFooterView?.frame.size = .zero
            
        },failure: {(msg) in
            
            self.stopLoading()
            self.tblServices.es.removeRefreshFooter()
            self.tblServices.tableFooterView?.isHidden = true
            self.tblServices.tableFooterView?.frame.size = .zero
            self.lblNoDataFound.text = msg
        })
        
    }
    
    func stopLoading()
    {
        self.hideProgressView()
        self.tblServices.es.stopPullToRefresh()
        self.tblServices.es.stopLoadingMore()
        self.tblServices.reloadData()
        self.lblNoDataFound.isHidden = arr_Local.count==0 ?false :true
        
    }
    
    func setPin(arrlocal:[Local])
    {
        arrlocal.forEach {
            let obj = CompanyData(local: $0)
            self.setSellerMarker(data: obj)
        }
    }
    
    //MARK: Custom Methods
    func showProgressView(){
        self.vw_Progress.isHidden = false
        self.vw_Progress.startAnimating()
    }
    
    func hideProgressView(){
        self.vw_Progress.isHidden = true
        self.vw_Progress.stopAnimating()
    }
}

extension LocalVC :UITableViewDataSource,UITableViewDelegate
{
    //MARK: TableView Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arr_Local.count
    }
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        if !isRefreshing
//        {
//            if indexPath.row > self.arr_LocalCategories.count - 2 && self.recordCount > self.arr_LocalCategories.count{
//                
//                let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
//                spinner.startAnimating()
//                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
//                
//                self.tblServices.tableFooterView = spinner
//                self.tblServices.tableFooterView?.isHidden = false
//                refreshRecord()
//            }
//        }
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceCell", for: indexPath) as! ServiceCell
        Utilities.applyShadowEffect(view: [cell.vwBG])
        
        let localData = self.arr_Local[indexPath.row]
        
        if localData.imageURL != nil{
            cell.serviceImage.sd_setImage(with: URL.init(string: localData.imageURL!), placeholderImage: UIImage(named:Constant.PlaceHolder.kPhoto), options: SDWebImageOptions.retryFailed, completed: nil)
        }else{
            cell.serviceImage.image = UIImage.init(named: Constant.PlaceHolder.kPhoto)
        }
        
        cell.lblTitle.text = localData.comapnyScreenName
        cell.nslcLblCatagoryTypeHight.constant = Utilities.isDeviceiPad() ? 22:18
        cell.nslclblDescHight.constant = 0
        cell.lblCatagoryType.text = localData.categories
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let Data = self.arr_Local[indexPath.row]
        let objServiceDetailForSBUVC = ServiceDetailForSBUVC(nibName: "ServiceDetailForSBUVC", bundle: nil) as ServiceDetailForSBUVC
        objServiceDetailForSBUVC.companyID    = Data.id
        objServiceDetailForSBUVC.screenName   = Data.companyName!
        let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
        navigationController.pushViewController(objServiceDetailForSBUVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       // return Utilities.isDeviceiPad() ? 92.0 : 72.0
        return Utilities.isDeviceiPad() ? 82.0 : 62.0
    }

}
