//
//  CancelOrderVC.swift
//  ShopByUber
//
//  Created by Differenz215 on 7/12/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class CancelOrderVC: BaseView,UITableViewDelegate,UITableViewDataSource {
    
    //MARK: - IBOutlet
    @IBOutlet weak var tblOrder: UITableView!
    @IBOutlet weak var vw_scrollContentView: UIView!
    @IBOutlet weak var nsLayoutServiceTable: NSLayoutConstraint!
    @IBOutlet weak var lblReason: UILabel!
    @IBOutlet weak var vw_Reasoncontent: UIView!
    @IBOutlet weak var vw_feedbackContent: UIView!
    
    @IBOutlet weak var lblScreenTitle: UILabel!
    @IBOutlet weak var txtFeedback: UITextView!
    
    @IBOutlet weak var txtCancelReason: UITextView!
    //MARK: - Variables
    var arrOrderData = [CancelOrderModel]()
    var requestID: String!
    var selectedIndexReason: Int?
    
    var requestIndexPath:IndexPath!
    var handlerDeleteEvent:((IndexPath)->())?
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialConfig()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Private Methods
    
    func initialConfig(){
        
        Utilities.applyShadowEffect(view: [self.vw_feedbackContent])
        
        
        if DataModel.sharedInstance.onFetchUserLoginType() == Constant.UserType.Buyer{
            
            //buyer side user Details
            self.tblOrder.register(UINib(nibName: "AppointmentDetailsCell", bundle: nil), forCellReuseIdentifier: "AppointmentDetailsCell")
            
        }else{
            //seller side user Details
            self.tblOrder.register(UINib(nibName: "SellerCancelRequestCell", bundle: nil), forCellReuseIdentifier: "SellerCancelRequestCell")
            
        }
        callGetOrderDetailsAPI()
    }
    
    func setHeaderData()
    {
        
        if Constant.DeviceType.IS_IPAD_PRO || Constant.DeviceType.IS_IPAD
        {
            self.nsLayoutServiceTable.constant = CGFloat(285 * self.arrOrderData.count)
        }
        else
        {
            self.nsLayoutServiceTable.constant = CGFloat(226 * self.arrOrderData.count)
        }
        tblOrder.reloadData()
    }
    
    
    @IBAction func btnCancel_Clicked(_ sender: Any) {
        
        if self.txtCancelReason.text.characters.count != 0
        {
            KVNProgress.show()
            var dict = [String : Any]()
            
            dict["sessionId"]    = DataModel.sharedInstance.onFetchSessionId()
            dict["userId"]       = DataModel.sharedInstance.onFetchUserId()
            dict["requestId"]    = self.requestID
            dict["serviceId"]    = self.arrOrderData[0].serviceId
            dict["timeSlotId"]   = self.arrOrderData[0].timeSlotId
            dict["flatFee"]      = self.arrOrderData[0].flatFee
            dict["cancelReason"] = self.txtCancelReason.text?.trimmingCharacters(in: .whitespaces)
            
            APIManager.callAPI(url: Constant.serverAPI.URL.kDoCancelRequest, inputParams: dict, success: { (dict) in
                
                KVNProgress.dismiss()
                if dict?["Result"] as? Bool == true {
                    
                    if DataModel.sharedInstance.onFetchUserLoginType() == Constant.UserType.Buyer{
                        
                        self.navigationController!.popToRootViewController(animated: true)
                    }else
                    {
                        self.navigationController!.popToRootViewController(animated: true)
                        if self.handlerDeleteEvent != nil
                        {
                            if self.requestIndexPath != nil
                            {
                                self.handlerDeleteEvent!(self.requestIndexPath)
                            }
                        }
                    }
                    
                }
                else
                {
                    Utilities.showAlertView(title: "", message: dict?["Message"] as? String)
                }
                
                
            }) { error in
                KVNProgress.dismiss()
                
                Utilities.showAlertView(title: "", message: error)
                
            }
        }
        else
        {
            Utilities.showAlertView(title: "", message: Constant.ValidationMessage.kCancelReasone)
        }
    }
    
    
    //MARK:  - TableView Methods
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constant.DeviceType.IS_IPAD || Constant.DeviceType.IS_IPAD_PRO ? 285 : 226
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrOrderData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->
        UITableViewCell {
            
            let appointmentCellData = self.arrOrderData[indexPath.row]
            
            if DataModel.sharedInstance.onFetchUserLoginType() == Constant.UserType.Buyer{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "AppointmentDetailsCell", for: indexPath) as! AppointmentDetailsCell
                if appointmentCellData.serviceImage != nil{
                    
                    cell.imgHeader.sd_setImage(with: URL.init(string: (appointmentCellData.serviceImage)!), placeholderImage: UIImage(named:Constant.PlaceHolder.kPhoto), options: SDWebImageOptions.retryFailed, completed:{ (image, err, nil, url) in
                        cell.imgHeader.image = image
                        cell.imgHeader.contentMode = .scaleAspectFill
                    })
                    
                }
                cell.lblHeaderTitle.text = appointmentCellData.screenName
                cell.lblFee.text = "$ " + appointmentCellData.flatFee!
                
                cell.lblProfileName.text = appointmentCellData.vendor?.name
                cell.imgProfileImage.contentMode = .scaleAspectFit
                if appointmentCellData.vendor?.profileImage != nil{
                    
                    cell.imgProfileImage.sd_setImage(with: URL.init(string: (appointmentCellData.vendor?.profileImage)!), placeholderImage: nil, options: SDWebImageOptions.retryFailed, completed:{ (image, err, nil, url) in
                        cell.imgProfileImage.image = image
                        cell.imgProfileImage.contentMode = .scaleAspectFill
                    })
                    
                }
                
                cell.lblAppointmentTime.text = appointmentCellData.duration
                cell.lblProfileDetails.text = appointmentCellData.vendor?.speciality
                cell.lblExperience.text  = appointmentCellData.vendor?.vendordescription
                cell.btnRemoveService.isHidden = true
                return cell
                
            }else{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "SellerCancelRequestCell", for: indexPath) as! SellerCancelRequestCell
                if appointmentCellData.serviceImage != nil{
                    cell.imgHeader.sd_setImage(with: URL.init(string: (appointmentCellData.serviceImage)!), placeholderImage: UIImage(named:Constant.PlaceHolder.kPhoto), options: SDWebImageOptions.retryFailed, completed: nil)
                }
                cell.lblHeaderTitle.text = appointmentCellData.screenName
                cell.lblFee.text = "$ " + appointmentCellData.flatFee!
                
                cell.tbl_TimeSlot.reloadData()
                cell.lblProfileName.text = appointmentCellData.vendor?.name
                cell.imgProfileImage.contentMode = .scaleAspectFit
                if appointmentCellData.vendor?.profileImage != nil{
                    
                    cell.imgProfileImage.sd_setImage(with: URL.init(string: (appointmentCellData.vendor?.profileImage)!), placeholderImage: nil, options: SDWebImageOptions.retryFailed, completed:{ (image, err, nil, url) in
                        if image != nil
                        {
                            cell.imgPlaceHolder.isHidden = true
                            cell.imgProfileImage.image = image
                            cell.imgProfileImage.contentMode = .scaleAspectFill
                        }
                        else
                        {
                            cell.imgPlaceHolder.isHidden = false
                        }
                    })
                    
                }
                
                cell.lblAppointmentTime.text = appointmentCellData.duration
                return cell
                
            }
    }
    
    //MARK: - API Call
    
    func callGetOrderDetailsAPI(){
        
        KVNProgress.show()
        var dict = [String : Any]()
        
        dict["sessionId"]    = DataModel.sharedInstance.onFetchSessionId()
        dict["userId"]       = DataModel.sharedInstance.onFetchUserId()
        dict["requestId"]    = self.requestID
        dict["userRole"]     = DataModel.sharedInstance.onFetchUserLoginType()
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kGetCancelOrderScreenData, inputParams: dict, success: { (dict) in
            
            KVNProgress.dismiss()
            if dict?["Result"] as? Bool == true {
                let data = dict?["Data"] as? [String:Any]
                if data?.count != 0
                {
                    let confirmServices = data?["confirmServices"] as? NSArray
                    if confirmServices != nil
                    {
                        for i in confirmServices!
                        {
                            let appointment = CancelOrderModel.init(dict: i as! [String : Any])
                            self.arrOrderData.append(appointment)
                        }
                    }
                    
                    self.setHeaderData()
                    self.vw_scrollContentView.isHidden = false
                }
            }
        }) { error in
            KVNProgress.dismiss()
            
            Utilities.showAlertView(title: dict["Message"] as? String ?? "", message: error)
            
        }
    }
}
