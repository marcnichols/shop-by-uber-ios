//
//  BuyerPurchaseDetailsVC.swift
//  ShopByUber
//
//  Created by Differenz215 on 7/12/17.
//  Copyright © 2017 Administrator. All rights reserved.
//


import UIKit

class BuyerPurchaseDetailsVC: BaseView {
    
    //MARK:- IBOutlet
    @IBOutlet weak var imgService: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var lbl3: UILabel!
    @IBOutlet weak var lbl4: UILabel!
    @IBOutlet weak var lbl5: UILabel!
    @IBOutlet weak var lbl6: UILabel!
    @IBOutlet weak var lbl7: UILabel! // Cancel Reason
    @IBOutlet weak var lbl8: UILabel! //Service Detail
    @IBOutlet weak var lbl10: UILabel! //Sub Catagory
    
    @IBOutlet weak var lbl9: UILabel! //Service Desc
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var vwImage: UIView!
    @IBOutlet weak var vwStatus: UIView!
    
    @IBOutlet weak var nslcBtnCancelheight: NSLayoutConstraint!
    @IBOutlet weak var btnChangeStatus: UIButton!
    
    //MARK:- Variable
    var requestDetails:OrderDetail!
    var requestStatus :Constant.RequestStatus!
    var requestID:String!
    var requestIndexPath:IndexPath!
    var isfromTracking = false
    
    var handlerDeleteEvent:((IndexPath)->())?
    
    //MARK:- View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //NotificationCenter is For reload View on pushnotification
        let notificationName = Notification.Name("BuyerPurchaseDetailsVC")
        NotificationCenter.default.addObserver(self, selector: #selector(initialConfig), name: notificationName, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.initialConfig()
        Singleton.sharedManager.currentView = self
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        Singleton.sharedManager.currentView = nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated
	}
	
    @IBAction override func btnBack_Click(_ sender: Any) {
        if isfromTracking == true {
            _ = self.navigationController?.popToRootViewController(animated: true)
        }else {
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    func initialConfig()
    {
        Utilities.applyShadowEffect(view: [vwImage])
        Utilities.applyShadowEffect(view: [vwStatus])
        
        OrderDetail.getOrderDetail(requestID: requestID, isShowloader: true) { (response) in
            self.requestDetails = response
            self.setupData()
            self.scrollView.isHidden = false
            switch self.requestDetails.status! {
           
                
            case 6, 4:
                self.nslcBtnCancelheight.constant = 0
                self.btnChangeStatus.isHidden = true
                
            case 2,0:
                self.nslcBtnCancelheight.constant =  Utilities.isDeviceiPad() ? 50 : 30
                self.btnChangeStatus.isHidden = true
                
            case 3:
                self.nslcBtnCancelheight.constant =  Utilities.isDeviceiPad() ? 50 : 30
                self.btnChangeStatus.isHidden = false
                self.btnChangeStatus.setTitle(Constant.StatusText.kSTARTTRACKING, for: .normal)
                
            case 5:
                self.nslcBtnCancelheight.constant = 0
                if self.requestDetails.isFeedbackGiven
                {
                    self.btnChangeStatus.isHidden = true
                    self.isfromTracking = true
                }
                else
                {
                    self.btnChangeStatus.isHidden = false
                    self.btnChangeStatus.setTitle(Constant.StatusText.kRATEREVIEWUS, for: .normal)
                }
            default:
                break
            }
        }
    }
    
    func setupData()
    {
        
        imgService.sd_setImage(with: URL(string:requestDetails.serviceImage), placeholderImage: UIImage(named:"placeholder"))
        var fontSize:CFloat = 15
        if Utilities.isDeviceiPad()
        {
            fontSize = 15 * 1.3
        }
        //==================== ServiceName =====================
        var attName = NSMutableAttributedString(string: "Service Name : " + requestDetails.screenName)
        attName = Utilities.decorateText(attributedString: attName, decorateString: requestDetails.screenName, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
        lblTitle.attributedText = attName
        //===================================================
        
       
        var statusText = ""
        var  statusColor = UIColor.black
        
        switch self.requestDetails.status! {
        case 0:
            statusText = Constant.StatusText.kNotStarted
            
        case 1:
            
            self.navigationController?.viewControllers.removeLast()
            let objPayForServiceVC = PayForServiceVC(nibName: "PayForServiceVC", bundle: nil) as PayForServiceVC
            objPayForServiceVC.isFrom = trackProgress.isFromHome
            self.navigationController?.pushViewController(objPayForServiceVC, animated: true)
            
        case 2:
            statusText = Constant.StatusText.kNotStarted
            
        case 3:
            statusText  = Constant.StatusText.kOntheWay
            
        case 4:
            statusText = Constant.StatusText.kInProgress
            statusColor = Utilities.colorWithHexString("2b9e36")
        case 5:
            statusText = Constant.StatusText.kCompleted
            
        case 6:
            if requestDetails.isCancelled
            {
                statusText = requestDetails.cancelByText  //Constant.StatusText.kCancelledby + "Buyer"
            }
            else
            {
                statusText = requestDetails.cancelByText  //Constant.StatusText.kCancelledby + "User"
            }
            statusColor = Utilities.colorWithHexString("D52A2B")
        default:
            break
        }
        
        var attStatus = NSMutableAttributedString(string: "Status : " + statusText)
        attStatus = Utilities.decorateText(attributedString: attStatus, decorateString: statusText, decorate: [NSFontAttributeName:UIFont.systemFontLight(with: fontSize),NSForegroundColorAttributeName: statusColor])
        
        if self.requestDetails.status == 6
        {
            if self.requestDetails.isRefund
            {
                //==================== Amount Paid =====================
                var attPaid = NSMutableAttributedString(string: "Amount Refund : $ " + requestDetails.amountPaid)
                attPaid = Utilities.decorateText(attributedString: attPaid, decorateString: "$ " + requestDetails.amountPaid, decorate: [NSFontAttributeName:UIFont.systemFontSemiBold(with: fontSize),NSForegroundColorAttributeName:Constant.Color.kGreen])
                lbl1.attributedText = attPaid
                //=======================================================
                
                //==================== Refund Method =====================
                var attRefundMethod = NSMutableAttributedString(string: "Refund Method : " + requestDetails.paymentMethod)
                attRefundMethod = Utilities.decorateText(attributedString: attRefundMethod, decorateString: requestDetails.paymentMethod, decorate: [NSFontAttributeName:UIFont.systemFontSemiBold(with: fontSize),NSForegroundColorAttributeName:Constant.Color.kBlue])
                lbl2.attributedText = attRefundMethod
                //=======================================================
                
                //==================== Category =====================
                var attCate = NSMutableAttributedString(string: "Category : " + requestDetails.catName)
                attCate = Utilities.decorateText(attributedString: attCate, decorateString: requestDetails.catName, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
                lbl3.attributedText = attCate
                //=================== Sub Category =====================
                var attSubCate = NSMutableAttributedString(string: "Sub Category : " + requestDetails.subCatName)
                attSubCate = Utilities.decorateText(attributedString: attSubCate, decorateString: requestDetails.catName, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
                lbl10.attributedText = attSubCate
                
                //==================== Status =====================
                
                lbl4.attributedText = attStatus
                //===================================================
                
                //==================== Cancel Reason =====================
                var attCancelReason = NSMutableAttributedString(string: "Cancellation Reason : " + requestDetails.cancelReason)
                attCancelReason = Utilities.decorateText(attributedString: attCancelReason, decorateString: requestDetails.cancelReason, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
                lbl7.attributedText = attCancelReason
                //===================================================
                
                
                //==================== Schedule =====================
                var attSchedule = NSMutableAttributedString(string: "Schedule : " + requestDetails.schedule)
                attSchedule = Utilities.decorateText(attributedString: attSchedule, decorateString: requestDetails.schedule, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
                lbl5.attributedText = attSchedule
                //===================================================
                
                lbl6.text = ""
                
                //===================================================
                var attServiceDetails = NSMutableAttributedString(string: "Service Detail : ")
                var attServiceDesc =  NSMutableAttributedString(string: requestDetails.serviceDetail)

                attServiceDetails = Utilities.decorateText(attributedString: attServiceDetails, decorateString: requestDetails.serviceDetail, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
                attServiceDesc = Utilities.decorateText(attributedString: attServiceDesc, decorateString: requestDetails.serviceDetail, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
                
                lbl8.attributedText = attServiceDetails
                lbl9.attributedText = attServiceDesc
               
            }
            else
            {
                 //==================== Category =====================
                var attCate = NSMutableAttributedString(string: "Category : " + requestDetails.catName)
                attCate = Utilities.decorateText(attributedString: attCate, decorateString: requestDetails.catName, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
                lbl1.attributedText = attCate
                //=================== Sub Category =====================
                var attSubCate = NSMutableAttributedString(string: "Sub Category : " + requestDetails.subCatName)
                attSubCate = Utilities.decorateText(attributedString: attSubCate, decorateString: requestDetails.subCatName, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
                lbl2.attributedText = attSubCate
                
                //==================== Status =====================
                
                lbl4.attributedText = attStatus
                //===================================================
                
                //==================== Schedule =====================
                var attSchedule = NSMutableAttributedString(string: "Schedule : " + requestDetails.schedule)
                attSchedule = Utilities.decorateText(attributedString: attSchedule, decorateString: requestDetails.schedule, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
                lbl3.attributedText = attSchedule
                //===================================================
                lbl10.text = ""
                lbl5.text = ""
                lbl6.text = ""
                
                //==================== Cancel Reason =====================
                var attCancelReason = NSMutableAttributedString(string: "Cancellation Reason : " + requestDetails.cancelReason)
                attCancelReason = Utilities.decorateText(attributedString: attCancelReason, decorateString: requestDetails.cancelReason, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
                lbl7.attributedText = attCancelReason
                //===================================================
                
                //===================================================
                var attServiceDetails = NSMutableAttributedString(string: "Service Detail : ")
                var attServiceDesc =  NSMutableAttributedString(string: requestDetails.serviceDetail)
                
                attServiceDetails = Utilities.decorateText(attributedString: attServiceDetails, decorateString: requestDetails.serviceDetail, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
                attServiceDesc = Utilities.decorateText(attributedString: attServiceDesc, decorateString: requestDetails.serviceDetail, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
                
                lbl8.attributedText = attServiceDetails
                lbl9.attributedText = attServiceDesc

            }
        }
        else if self.requestDetails.status == 0
        {
            //==================== Amount Paid =====================
            var attPaid = NSMutableAttributedString(string: "Amount : $ " + requestDetails.amountPaid)
            attPaid = Utilities.decorateText(attributedString: attPaid, decorateString: "$ " + requestDetails.amountPaid, decorate: [NSFontAttributeName:UIFont.systemFontSemiBold(with: fontSize),NSForegroundColorAttributeName:Constant.Color.kGreen])
            lbl1.attributedText = attPaid
            //=======================================================
            
            
            //==================== Service Provider =====================
            var attProvider = NSMutableAttributedString(string: "Service Provider : " + requestDetails.serviceProvider)
            attProvider = Utilities.decorateText(attributedString: attProvider, decorateString: requestDetails.serviceProvider, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName: UIColor.black])
            lbl2.attributedText = attProvider
            //=======================================================
            
            
            //==================== Category =====================
            var attCate = NSMutableAttributedString(string: "Category : " + requestDetails.catName)
            attCate = Utilities.decorateText(attributedString: attCate, decorateString: requestDetails.catName, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
            lbl3.attributedText = attCate
            //=================== Sub Category =====================
            var attSubCate = NSMutableAttributedString(string: "Sub Category : " + requestDetails.subCatName)
            attSubCate = Utilities.decorateText(attributedString: attSubCate, decorateString: requestDetails.catName, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
            lbl10.attributedText = attSubCate
            
            //==================== Status =====================
            
            lbl4.attributedText = attStatus
            //===================================================
            
            //==================== Schedule =====================
            var attSchedule = NSMutableAttributedString(string: "Schedule : " + requestDetails.schedule)
            attSchedule = Utilities.decorateText(attributedString: attSchedule, decorateString: requestDetails.schedule, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
            lbl5.attributedText = attSchedule
            //===================================================
            
            lbl7.text = ""
            
            //===================================================
            var attServiceDetails = NSMutableAttributedString(string: "Service Detail : ")
            var attServiceDesc =  NSMutableAttributedString(string: requestDetails.serviceDetail)
            
            attServiceDetails = Utilities.decorateText(attributedString: attServiceDetails, decorateString: requestDetails.serviceDetail, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
            attServiceDesc = Utilities.decorateText(attributedString: attServiceDesc, decorateString: requestDetails.serviceDetail, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
            
            lbl8.attributedText = attServiceDetails
            lbl9.attributedText = attServiceDesc

        }
        else
        {
            //==================== Amount Paid =====================
            var attPaid = NSMutableAttributedString(string: "Amount Paid : $ " + requestDetails.amountPaid)
            attPaid = Utilities.decorateText(attributedString: attPaid, decorateString: "$ " + requestDetails.amountPaid, decorate: [NSFontAttributeName:UIFont.systemFontSemiBold(with: fontSize),NSForegroundColorAttributeName:Constant.Color.kGreen])
            lbl1.attributedText = attPaid
            //=======================================================
            
            //==================== Payment Method =====================
            var attPaymentMethod = NSMutableAttributedString(string: "Payment Method : " + requestDetails.paymentMethod)
            attPaymentMethod = Utilities.decorateText(attributedString: attPaymentMethod, decorateString: requestDetails.paymentMethod, decorate: [NSFontAttributeName:UIFont.systemFontSemiBold(with: fontSize),NSForegroundColorAttributeName:Constant.Color.kBlue])
            lbl2.attributedText = attPaymentMethod
            //=======================================================
            
            
            //==================== Service Provider =====================
            var attProvider = NSMutableAttributedString(string: "Service Provider : " + requestDetails.serviceProvider)
            attProvider = Utilities.decorateText(attributedString: attProvider, decorateString: requestDetails.serviceProvider, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName: UIColor.black])
            lbl3.attributedText = attProvider
            //=======================================================
            
            
            //==================== Category =====================
            var attCate = NSMutableAttributedString(string: "Category : " + requestDetails.catName)
            attCate = Utilities.decorateText(attributedString: attCate, decorateString: requestDetails.catName, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
            lbl10.attributedText = attCate
            //=================== Sub Category =====================
            var attSubCate = NSMutableAttributedString(string: "Sub Category : " + requestDetails.subCatName)
            attSubCate = Utilities.decorateText(attributedString: attSubCate, decorateString: requestDetails.catName, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
            lbl4.attributedText = attSubCate
            
            //==================== Status =====================
            
            lbl5.attributedText = attStatus
            //===================================================
            
            //==================== Schedule =====================
            var attSchedule = NSMutableAttributedString(string: "Schedule : " + requestDetails.schedule)
            attSchedule = Utilities.decorateText(attributedString: attSchedule, decorateString: requestDetails.schedule, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
            lbl6.attributedText = attSchedule
            //===================================================
            
            lbl7.text = ""
            
            //===================================================
            var attServiceDetails = NSMutableAttributedString(string: "Service Detail : ")
            var attServiceDesc =  NSMutableAttributedString(string: requestDetails.serviceDetail)
            
            attServiceDetails = Utilities.decorateText(attributedString: attServiceDetails, decorateString: requestDetails.serviceDetail, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
            attServiceDesc = Utilities.decorateText(attributedString: attServiceDesc, decorateString: requestDetails.serviceDetail, decorate: [NSFontAttributeName:UIFont.systemFontReguler(with: fontSize),NSForegroundColorAttributeName:UIColor.black])
            
            lbl8.attributedText = attServiceDetails
            lbl9.attributedText = attServiceDesc

        }
        
    }
    
    //MARK:- IBActions
    @IBAction func btnChangeStatus_Clicked(_ sender: Any) {
        
        switch self.requestDetails.status! {
            
        case 3:
            let track = TrackProgressVC.init(nibName: "TrackProgressVC", bundle: nil)
            track.requestID = self.requestDetails.requestId
            track.isFrom = trackProgress.isFromHome
            self.navigationController?.pushViewController(track, animated: true)
        case 5:
            let vc = ReviewsVC(nibName: "ReviewsVC", bundle: nil)
            vc.requestDetails = requestDetails
            self.navigationController?.pushViewController(vc, animated: true)
            
        default:
            break
        }
        
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        let vc = CancelOrderVC(nibName: "CancelOrderVC", bundle: nil) as CancelOrderVC
        vc.requestID = self.requestID
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
