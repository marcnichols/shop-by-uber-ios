//
//  Singleton.swift
//  ShopByUber
//
//  Created by Chirag Kalsariya on 7/15/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class Singleton: NSObject {
    
    static let sharedManager = Singleton()
    
    var devieToken:String! = ""
    var selectedMenuItemIndex:Int = -1
    var unreadNotificationCount: Int = 0
    var isDisableHistory:Bool = true
    var isNotification:Bool = false
    var isNotiSoundEnable:Bool = true
    var isVibrateEnable:Bool = true
    var isLightEnable:Bool = true
    var isFromactivity: isFromActivity = .Anyothers
    var unreadHandler:((Bool)->())?
    var userInfo: notificationData?
    var navigationController: UINavigationController?
    var currentView : UIViewController?
    
    override init() {
        super.init()
        
        isDisableHistory = Utilities.retriveBoolValue(forKey: Constant.kDisableHistory)
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIApplicationDidBecomeActive, object: nil, queue: OperationQueue.main, using: {(_ note: Notification) -> Void in
            
            self.getNotficationCount()
        })
    }
    
    func getNotficationCount()
    {
        if DataModel.sharedInstance.isUserLoggedIn() {
            
            var dict = [String : Any]()
            
            dict["sessionId"]    = DataModel.sharedInstance.onFetchSessionId()
            dict["userId"]       = DataModel.sharedInstance.onFetchUserId()
            
            APIManager.callAPI(url: Constant.serverAPI.URL.kGetUnreadNotificationCount, inputParams: dict, success: { (dict) in
                if dict?["Result"] as? Bool == true {
                    let data = dict?["Data"] as? [String:Any]
                    if data?.count != 0
                    {
                        self.unreadNotificationCount = (data?["userUnreadNotificationCount"] as? Int) ?? 0
                        //success
                        if self.unreadHandler != nil
                        {
                            self.unreadHandler!(true)
                        }
                    }
                }
            }) { error in
            }
        }

    }
    
    func getNotificationSetting()
    {
        NotificationSetting.getSetting(success: { (settings) in
            var data = settings[0]
            self.isNotiSoundEnable = data.isEnabled
            data = settings[1]
            self.isVibrateEnable = data.isEnabled
            data = settings[2]
            self.isLightEnable = data.isEnabled
            
        }) {
            
        }
    }
}
