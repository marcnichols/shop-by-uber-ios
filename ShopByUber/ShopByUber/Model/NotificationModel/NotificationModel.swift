//
//  NotificationModel.swift
//  ShopByUber
//
//  Created by Differenz215 on 7/3/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class NotificationModel: NSObject {
    
    var notifyId:           Int?
    var isRead:             Bool?
    var message:            String?
    var createdDatetime:    String?
    var requestId:          String?
    var serviceId:          String?
    var screenName:         String?
    var serviceImage:       String?
    var notificationId:     String?
    var pubChannelName : String?
    var userAddress : String?
    var userLatitude : Int?
    var userLongitude : Int?
    var categoryId: String?
    var companyId: String?
    var subCategoryId: String?
    
    init(dict:[String:Any]) {
        super.init()
        
        notifyId                = dict["notifyId"] as?  Int
        isRead                  = dict["isRead"] as? Bool
        message                 = dict["message"] as? String
        createdDatetime         = dict["createdDatetime"] as? String
        requestId               = dict["requestId"] as? String
        serviceId               = dict["serviceId"] as? String
        screenName              = dict["screenName"] as? String
        serviceImage            = dict["serviceImage"] as? String
        notificationId          = dict["notificationId"] as? String
        companyId               = dict["companyId"] as? String
        categoryId              = dict["categoryId"] as? String
        pubChannelName          = dict["pubChannelName"] as? String
        userAddress             = dict["userAddress"] as? String
        userLatitude            = dict["userLatitude"] as? Int
        userLongitude           = dict["userLongitude"] as? Int
        subCategoryId           = dict["subCategoryId"] as? String
        
    }
    
}


class notificationData: NSObject {
    var notificationId: String?
    var notifyType: Int?
    var requestId: String?
    var categoryId: String?
    var companyId: String?
    var subCategoryID: String?
    var serviceId: String?
    
    //Chat
    var dialogId: String?
    var dialogName: String?
    var profileImage: String?
    var receiverId: String?
    var occupantId: Int?
    var userName:String?
    
    init(dict:[String:Any], withAction: Bool) {
        super.init()
        notificationId = dict["notificationId"] as?  String
        notifyType = dict["notifyType"] as? Int
        requestId = dict["requestId"] as? String
        companyId = dict["companyId"] as? String
        categoryId = dict["categoryId"] as? String
        subCategoryID = dict["subCategoryId"] as? String
        serviceId = dict["serviceId"] as? String
        
        dialogId = dict["dialogId"] as? String
        dialogName = dict["dialogName"] as? String
        profileImage = dict["profileImage"] as? String
        receiverId = dict["userId"] as? String
        occupantId = dict["quickBloxId"] as? Int
        userName = dict["userName"] as? String
        
        
        if withAction {
            notifyType = 0
            dialogName = dict["userName"] as? String
        }
    }
}
