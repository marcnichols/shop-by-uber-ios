//
//  DataModel.swift
//  ShopByUber
//
//  Created by Administrator on 5/30/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class DataModel: NSObject {

    static let sharedInstance = DataModel()
   
    func setValueInUserDefaults(value: Any?, key: String) {
        UserDefaults.standard.set(value, forKey: key)
        UserDefaults.standard.synchronize()
    }
   
    func onFetchDeviceToken() -> String {
        let deviceTokenStr = UserDefaults.standard.value(forKey: Constant.DataModelKey.KDeviceToken) as? String
        
        return deviceTokenStr ?? ""
    }
    
    func getCurrentTimeZone() -> String {
        return TimeZone.current.identifier
    }
    
    //MARK: User Data
    
    func isUserLoggedIn() -> Bool{
        let isLoggedIn = UserDefaults.standard.value(forKey: Constant.DataModelKey.KIsUserLoggedIn) as? Bool
        return isLoggedIn ?? false
    }
    
    func onFetchUserLoginType() -> Int {
        let loginType = UserDefaults.standard.value(forKey: Constant.DataModelKey.KUserType) as? Int
        if loginType == nil
        {
            return 0
        }else
        {
            return loginType ?? 0
        }
    }
    
    func onFetchSessionId() -> String {
        let id  = UserDefaults.standard.object(forKey: Constant.DataModelKey.KSessionId) as? String
        return id ?? ""
    }
    
    func onFetchUserId() -> String {
        let id  = UserDefaults.standard.object(forKey: Constant.DataModelKey.KUserId) as? String
        return id ?? ""
    }
    
    func isProfileCreated() -> Bool{
        let isProfileCreated = UserDefaults.standard.value(forKey: Constant.DataModelKey.kIsProfileCreated) as? Bool
        return isProfileCreated ?? true
    }
    
    func isServiceAvailable() -> Bool{
        let isServiceAvailable = UserDefaults.standard.value(forKey: Constant.DataModelKey.kIsServiceAvailable) as? Bool
        return isServiceAvailable ?? true
    }
    
    
    //MARK: Fetch User Data
    
    func onFetchEmail() -> String {
        let email  = UserDefaults.standard.object(forKey: Constant.DataModelKey.KUserEmail) as? String
        return email ?? ""
    }
    
    func onFetchMobileNumber() -> String {
        let mobile  = UserDefaults.standard.object(forKey: Constant.DataModelKey.KUserPhone) as? String
        return mobile ?? ""
    }
    func onFetchCountryCode() -> String {
        let countrycode  = UserDefaults.standard.object(forKey: Constant.DataModelKey.KUserCountryCode) as? String
        return countrycode ?? ""
    }
    func onFetchUserName() -> String {
        let name  = UserDefaults.standard.object(forKey: Constant.DataModelKey.KUserName) as? String
        return name ?? ""
    }
    func onFetchFirstName() -> String {
        let name  = UserDefaults.standard.object(forKey: Constant.DataModelKey.kFirstName) as? String
        return name ?? ""
    }
    func onFetchLastName() -> String {
        let name  = UserDefaults.standard.object(forKey: Constant.DataModelKey.KLastName) as? String
        return name ?? ""
    }
    func onFetchEzListScreenName() -> String {
        let name  = UserDefaults.standard.object(forKey: Constant.DataModelKey.kezListScreenName) as? String
        return name ?? ""
    }
    
    func onFetchUserImage() -> String {
        let image  = UserDefaults.standard.object(forKey: Constant.DataModelKey.KUserImage) as? String
        return image ?? ""
    }
    
    func onFetchSellerDescription() -> String {
        let des  = UserDefaults.standard.object(forKey: Constant.DataModelKey.KSellerDescription) as? String
        return des ?? ""
    }
    func onFetchPromoCode() -> String {
        let code = UserDefaults.standard.object(forKey: Constant.DataModelKey.kpromoCode) as? String
        return code ?? ""
    }
    func onFetchIsSalesPerson() -> Bool {
        let isSalesPerson = UserDefaults.standard.object(forKey: Constant.DataModelKey.kisSalesperson) as? Bool
        return isSalesPerson ?? false
    }
    func onFetchQuickBloxId() -> Int {
        let quickBloxId = UserDefaults.standard.object(forKey: Constant.DataModelKey.kQuickbloxId) as? Int
        return quickBloxId ?? 0
    }
    
    //MARK: Fetch Location Data
    
    func onFetchZipCode() -> String {
        let zipCode  = UserDefaults.standard.object(forKey: Constant.DataModelKey.kzipCode) as? String
        return zipCode ?? ""
    }
    
    func onFetchAddressLines() -> String {
        let address  = UserDefaults.standard.object(forKey: Constant.DataModelKey.kLines) as? String
        return address ?? ""
    }
    
    func onFetchLatitude() -> Double {
        let latitude  = UserDefaults.standard.object(forKey: Constant.DataModelKey.kLatitude) as? Double
        print(UserDefaults.standard.object(forKey: Constant.DataModelKey.kLatitude) as? Double)
        return latitude ?? 0.0
    }
    
    func onFetchLongtitude() -> Double {
        let longtitude  = UserDefaults.standard.object(forKey: Constant.DataModelKey.kLongtitude) as? Double
        return longtitude ?? 0.0
    }
    
    func onFetchCity() -> String {
        let city  = UserDefaults.standard.object(forKey: Constant.DataModelKey.kCity) as? String
        return city ?? ""
    }
    
    func onFetchState() -> String {
        let state  = UserDefaults.standard.object(forKey: Constant.DataModelKey.kState) as? String
        return state ?? ""
    }
    func onFetchAddress() -> String {
        let address  = UserDefaults.standard.object(forKey: Constant.DataModelKey.kaddress) as? String
        return address ?? ""
    }
    
    func onFetchLegalBusinessName() -> String{
        let legalBusinessName = UserDefaults.standard.object(forKey: Constant.DataModelKey.kLegalBusinessName) as? String
        return legalBusinessName ?? ""
    }
    func onFetchVersionUpdate() -> Bool {
        let isUpdateAvailable = UserDefaults.standard.object(forKey: Constant.DataModelKey.kcheckVersionUpdate) as? Bool
        return isUpdateAvailable ?? false
    }
    func isCommercialPlayed() -> Bool {
        let isCommercialPlayed = UserDefaults.standard.object(forKey: Constant.DataModelKey.kisCommercialPlayed) as? Bool
        return isCommercialPlayed ?? false
    }
    
    //MARK: Set UserDefault Data
    func setUserDataWhenLogin(userID:String?,sessionId:String?,userType:Int?,email:String?,name:String?,firstName:String?,lastName:String?,mobile:String?,phone:String?,countryCode:String?,userProfile:String?,ezListScreenName: String?,description:String?, promoCode:String?){
        
        self.setValueInUserDefaults(value: true, key: Constant.DataModelKey.KIsUserLoggedIn)
        self.setValueInUserDefaults(value: userID, key: Constant.DataModelKey.KUserId)
        self.setValueInUserDefaults(value: sessionId, key: Constant.DataModelKey.KSessionId)
        self.setValueInUserDefaults(value: userType, key: Constant.DataModelKey.KUserType)
        self.setValueInUserDefaults(value: email, key: Constant.DataModelKey.KUserEmail)
        self.setValueInUserDefaults(value: name, key: Constant.DataModelKey.KUserName)
        self.setValueInUserDefaults(value: firstName, key: Constant.DataModelKey.kFirstName)
        self.setValueInUserDefaults(value: lastName, key: Constant.DataModelKey.KLastName)
        self.setValueInUserDefaults(value: mobile, key: Constant.DataModelKey.KUserMobile)
        self.setValueInUserDefaults(value: phone, key: Constant.DataModelKey.KUserPhone)
        self.setValueInUserDefaults(value: countryCode, key: Constant.DataModelKey.KUserCountryCode)
        self.setValueInUserDefaults(value: ezListScreenName, key: Constant.DataModelKey.kezListScreenName)
        self.setValueInUserDefaults(value: mobile, key: Constant.DataModelKey.KUserMobile)
        self.setValueInUserDefaults(value: userProfile, key: Constant.DataModelKey.KUserImage)
        self.setValueInUserDefaults(value: description, key: Constant.DataModelKey.KSellerDescription)
        self.setValueInUserDefaults(value: promoCode, key: Constant.DataModelKey.kpromoCode)
        

    }
    func setUserDefaultIsSalesperson(isSalesperson: Bool?){
        self.setValueInUserDefaults(value: isSalesperson, key: Constant.DataModelKey.kisSalesperson)
    }
    
    func setUserDefaultIsServicesCreated(isServicesCreated: Bool?) {
        self.setValueInUserDefaults(value: isServicesCreated, key: Constant.DataModelKey.kIsServiceAvailable)
    }
    
    func setUserDefaultQuickBloxId(quickBloxId: Int?){
        self.setValueInUserDefaults(value: quickBloxId, key: Constant.DataModelKey.kQuickbloxId)
    }
    func CheckVersionUpdate(checkVersionUpdate:Bool){
        self.setValueInUserDefaults(value: checkVersionUpdate, key: Constant.DataModelKey.kcheckVersionUpdate)
    }
    func setUserDefaultIsCommercialPlayed(isCommercialPlayed: Bool?) {
        self.setValueInUserDefaults(value: isCommercialPlayed, key: Constant.DataModelKey.kisCommercialPlayed)
    }
    func setLatitude(lat: Double?) {
        self.setValueInUserDefaults(value: lat, key: Constant.DataModelKey.kLatitude)
    }
    func setLongtitude(long: Double?) {
        self.setValueInUserDefaults(value: long, key: Constant.DataModelKey.kLongtitude)
    }
    
    //MARK: Reset Data
    func resetUserDataWhenLogout(){
        
        let store = Twitter.sharedInstance().sessionStore
        if let userID = store.session()?.userID {
            store.logOutUserID(userID)
        }
        
        self.setValueInUserDefaults(value: "", key: Constant.DataModelKey.KDeviceToken)
        self.setValueInUserDefaults(value: false, key: Constant.DataModelKey.KIsUserLoggedIn)
        self.setValueInUserDefaults(value: "", key: Constant.DataModelKey.KUserType)
        self.setValueInUserDefaults(value: "", key: Constant.DataModelKey.KUserId)
        self.setValueInUserDefaults(value: "", key: Constant.DataModelKey.KSessionId)
        self.setValueInUserDefaults(value: true, key: Constant.DataModelKey.kIsProfileCreated)
        self.setValueInUserDefaults(value: "", key: Constant.DataModelKey.KUserCountryCode)
        self.setValueInUserDefaults(value: "", key: Constant.DataModelKey.KUserPhone)
        self.setValueInUserDefaults(value: "", key: Constant.DataModelKey.KUserEmail)
        self.setValueInUserDefaults(value: "", key: Constant.DataModelKey.KUserName)
        self.setValueInUserDefaults(value: "", key: Constant.DataModelKey.KUserImage)
        self.setValueInUserDefaults(value: "", key: Constant.DataModelKey.KUserMobile)
        self.setValueInUserDefaults(value: "", key: Constant.DataModelKey.KSellerDescription)
        self.setValueInUserDefaults(value: "", key: Constant.DataModelKey.kQuickbloxId)
        self.setValueInUserDefaults(value: "", key: Constant.DataModelKey.kState)
        self.setValueInUserDefaults(value: "", key: Constant.DataModelKey.kCity)
        self.setValueInUserDefaults(value: "", key: Constant.DataModelKey.kaddress)
        
       // self.setValueInUserDefaults(value: true, key: Constant.DataModelKey.kisServicesCreated)
        
        //Quickblox: Logout from chat and quickblox
        if Utilities.checkInternetConnection() {
            QuickBloxManager.sharedInstance.onLogoutFromQuickblox()
        }
    }
    
}
