//
//  CitiesList.swift
//  The EZ List
//
//  Created by DIFFERENZ101 on 12/8/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class CitiesList: NSObject {

    var cityId : String!
    var cityName : String!
    
    init(fromDictionary dictionary: [String:Any]){
        cityId = dictionary["_id"] as? String
        cityName = dictionary["city"] as? String
    }
    
}
