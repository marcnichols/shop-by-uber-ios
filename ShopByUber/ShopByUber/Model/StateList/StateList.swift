//
//  StateList.swift
//  The EZ List
//
//  Created by DIFFERENZ101 on 12/8/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class StateList: NSObject {

    var stateId : String!
    var stateName : String!
    
    init(fromDictionary dictionary: [String:Any]){
        stateId = dictionary["_id"] as? String
        stateName = dictionary["name"] as? String
    }
}
