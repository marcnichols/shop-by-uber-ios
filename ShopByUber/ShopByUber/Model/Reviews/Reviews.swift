//
//  Reviews.swift
//  ShopByUber
//
//  Created by Differenz215 on 7/11/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import Foundation

import Foundation


class Reviews : NSObject, NSCoding{
    
    var companyId : String!
    var feedback : String!
    var name : String!
    var profileImage : String!
    var rating : Double!
    var requestId : String!
    var serviceId : String!
    var vendorId : String!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        companyId = dictionary["companyId"] as? String
        feedback = dictionary["feedback"] as? String
        name = dictionary["name"] as? String
        profileImage = dictionary["profileImage"] as? String
        rating = dictionary["rating"] as? Double
        requestId = dictionary["requestId"] as? String
        serviceId = dictionary["serviceId"] as? String
        vendorId = dictionary["vendorId"] as? String
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if companyId != nil{
            dictionary["companyId"] = companyId
        }
        if feedback != nil{
            dictionary["feedback"] = feedback
        }
        if name != nil{
            dictionary["name"] = name
        }
        if profileImage != nil{
            dictionary["profileImage"] = profileImage
        }
        if rating != nil{
            dictionary["rating"] = rating
        }
        if requestId != nil{
            dictionary["requestId"] = requestId
        }
        if serviceId != nil{
            dictionary["serviceId"] = serviceId
        }
        if vendorId != nil{
            dictionary["vendorId"] = vendorId
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        companyId = aDecoder.decodeObject(forKey: "companyId") as? String
        feedback = aDecoder.decodeObject(forKey: "feedback") as? String
        name = aDecoder.decodeObject(forKey: "name") as? String
        profileImage = aDecoder.decodeObject(forKey: "profileImage") as? String
        rating = aDecoder.decodeObject(forKey: "rating") as? Double
        requestId = aDecoder.decodeObject(forKey: "requestId") as? String
        serviceId = aDecoder.decodeObject(forKey: "serviceId") as? String
        vendorId = aDecoder.decodeObject(forKey: "vendorId") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if companyId != nil{
            aCoder.encode(companyId, forKey: "companyId")
        }
        if feedback != nil{
            aCoder.encode(feedback, forKey: "feedback")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if profileImage != nil{
            aCoder.encode(profileImage, forKey: "profileImage")
        }
        if rating != nil{
            aCoder.encode(rating, forKey: "rating")
        }
        if requestId != nil{
            aCoder.encode(requestId, forKey: "requestId")
        }
        if serviceId != nil{
            aCoder.encode(serviceId, forKey: "serviceId")
        }
        if vendorId != nil{
            aCoder.encode(vendorId, forKey: "vendorId")
        }
        
    }
    
}
