//
//  OccupiedData.swift
//  ShopByUber
//
//  Created by Administrator on 6/20/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class OccupiedData: NSObject {
    
    var timeslot: NSMutableArray = []
    var allocatedDates:NSMutableArray = []
    var availableDays: NSMutableArray = []
    var objServiceDetail : ServiceDetail?
    var objTimeSlotList = [TimeSlotDetail]()
    var objPaymentDetails : PaymentDetails?
    
    override init() {
        
    }
    
    init(dict:[String:Any]) {
        super.init()
        
        let arrTimeSlot = dict["timeslot"] as? NSArray
        if arrTimeSlot != nil {
            for element in arrTimeSlot! {
                
                let dict = element as? NSDictionary
                let objTimeSlotList : TimeSlotList = TimeSlotList.init(dict: dict!) as TimeSlotList
                timeslot.add(objTimeSlotList)
            }
        }
        
        let arrAllocatedDates = dict["allocatedDates"] as? NSArray
        if arrAllocatedDates != nil {
            for element in arrAllocatedDates! {
                
                let dict = element as? NSDictionary
                let objTimeSlotList : AllocatedDates = AllocatedDates.init(dict: dict!) as AllocatedDates
                allocatedDates.add(objTimeSlotList)
            }
        }
        
        let arrAvailableDays = dict["availableDays"] as? NSArray
        if arrAvailableDays != nil {
            for element in arrAvailableDays! {
                
                let dict = element as? NSDictionary
                let objTimeSlotList : AvailableDays = AvailableDays.init(dict: dict!) as AvailableDays
                availableDays.add(objTimeSlotList)
            }
        }
        
        if dict["serviceDetail"] != nil {
            
            objServiceDetail = ServiceDetail.init(dict: dict["serviceDetail"] as! NSDictionary)
        }
        
        else {
            
            objServiceDetail = ServiceDetail.init(dict: NSDictionary())
        }
        
        if  let arrAllocate = dict["allocate"] as? [Any]
        {
            if (arrAllocate.count) > 0
            {
                let dictAllocate = arrAllocate.first as! [String:Any]
                let arrTimeslot = dictAllocate["timeslot"] as? [[String:Any]]
                
                if arrTimeslot != nil {
                    for element in arrTimeslot! {
                        
                        let dict = element as NSDictionary
                        let objTimeSlot : TimeSlotDetail = TimeSlotDetail.init(dict: dict)
                        self.objTimeSlotList.append(objTimeSlot)
                    }
                    
                }
            }
        }
//        else if dict["TimeSlotList"] != nil {
//            
//            objTimeSlotList = TimeSlotList.init(dict: dict["TimeSlotList"] as! NSDictionary)
//        }
    }
    
    class func isValidCatagory(category: String?) -> Bool {
        
        let category = category?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        var message = ""
        if category == "" {
            
            message = "Please select a category for your service"
            OccupiedData.showValidation(message: message)
            return false
        }else{
            return true
        }
    }
    
    class func isValidServiceData(imageServiceProvide: UIImage?,category: String?,subcategory: String?, screenName: String?,duration: String?,days: String?,time:String?,flatFee:String?,offerDetail:String?,keyword:String?,objCount:Int)  -> Bool {
        
        let category = category?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let subcategory = subcategory?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let screenName = screenName?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let duration = duration?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let days = days?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let time = time?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let flatFee = flatFee?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let offerDetail = offerDetail?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let keyword = keyword?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        var message = ""
        
        if imageServiceProvide == nil {
            
            message = "Please add your service provider image"
            OccupiedData.showValidation(message: message)
            return false
        }
        else if category == "" {
            
            message = "Please select a category for your service"
            OccupiedData.showValidation(message: message)
            return false
            
        }else if subcategory == "" {
            
            message = "Please select a Subcategory that best matches the Service you are creating!"//"Please select sub category for your service"
            OccupiedData.showValidation(message: message)
            return false
            
        }
        else if screenName == "" {
            
            message = "Please enter your screen name"
            OccupiedData.showValidation(message: message)
            return false
            
        }
        else if keyword == "" {
            
            message = "Please add Keywords to your Service so our Buyers can search and find you easily!"
            OccupiedData.showValidation(message: message)
            return false
        }
        if objCount == 0
        {
             if duration == ""{
                
                message =  "Please select your service appointment duration"
                OccupiedData.showValidation(message: message)
                return false
                
            } else if days == "" {
                
                message =  Constant.ValidationMessage.kSelectDay
                OccupiedData.showValidation(message: message)
                return false
                
            }else if time == "" {
                
                message = "Please select available time"
                OccupiedData.showValidation(message: message)
                return false
                
            }
            else if flatFee == "" {
                
                message = "Please enter flat fee for your service"
                OccupiedData.showValidation(message: message)
                return false
                
            } else if offerDetail == ""{
                
                message = "Please enter the offer details for your service"
                OccupiedData.showValidation(message: message)
                return false
                
            }
            else {
                
                return true
            }

        }else
        {
            if duration == ""{
                
                message =  "Please select appointment duration"
                OccupiedData.showValidation(message: message)
                return false
                
            } else if days == "" {
                
                message =  Constant.ValidationMessage.kSelectDay
                OccupiedData.showValidation(message: message)
                return false
                
            }else if time == "" {
                
                message = "Please select available time"
                OccupiedData.showValidation(message: message)
                return false
                
            }
            else if flatFee == "" {
                
                message = "Please enter flat fee for your service"
                OccupiedData.showValidation(message: message)
                return false
                
            } else if offerDetail == ""{
                
                message = "Please enter the offer details for your service"
                OccupiedData.showValidation(message: message)
                return false
                
            }
            else {
                
                return true
            }

        }

    }
    
    class func showValidation(message:String)
    {
        Utilities.showAlertView(title: "Alert", message: message)
    }
    
    class func callGetCategoriesListAPI(success: @escaping ([String:Any]?) -> (), failure: @escaping () -> ()) {
        
        
        var dict = [String:Any]()
        dict["userId"] = DataModel.sharedInstance.onFetchUserId()
        dict["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kgetCategoryList, inputParams: dict, success: { (dict) in
            
            if dict?["Result"] as? Bool == true {
                
                let data = dict?["Data"] as? [String:Any]
                success(data)
            }
                
            else {
                
                let msg = dict?["Message"] as? String
                Utilities.showAlertView(title: "", message: msg)
                
            }
            
        }) { error in
            
            Utilities.showAlertView(title: "", message: error)
        }
        
        
    }
    
    
    class func getDataEditService(dictParam: [String:Any], success: @escaping ([String:Any]?) -> (), failure: @escaping () -> ()) {
        
        KVNProgress.show()
        
        /*
         var dict = [String:Any]()
         dict["userId"] = DataModel.sharedInstance.onFetchUserId()
         dict["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
         dict["serviceId"] = serviceID
         */
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kgetDataEditService, inputParams: dictParam, success: { (dict) in
            
            KVNProgress.dismiss()
            
            if dict?["Result"] as? Bool == true {
                
                let data = dict?["Data"] as? [String:Any]
                success(data)
            }
                
            else {
                
                let msg = dict?["Message"] as? String
                Utilities.showAlertView(title: "", message: msg)
                failure()
                
            }
            
        }) { error in
            
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
            failure()
        }
        
    }
    
}


class TimeSlotList: NSObject {
    
    var duration : Int?
    var timeslot : NSMutableArray = []
    
    init(dict:NSDictionary) {
        
        duration = dict.value(forKey: "duration") as? Int
        let arrTimeSlot = dict["timeslot"] as? NSArray
        if arrTimeSlot != nil {
            for element in arrTimeSlot! {
                
                let dict = element as? NSDictionary
                let objTimeSlotDetail : TimeSlotDetail = TimeSlotDetail.init(dict: dict!) as TimeSlotDetail
                timeslot.add(objTimeSlotDetail)
                
            }
        }
        
    }
}

class TimeSlotDetail: NSObject {
    
    var time : String?
    var allocated : Bool?
    
    init(dict:NSDictionary) {
        time = dict.value(forKey: "time") as? String
        allocated = dict.value(forKey: "allocated") as? Bool
    }
}

class AllocatedDates: NSObject {
    
    var date : String?
    var timeslot : NSMutableArray = []
    
    init(dict:NSDictionary) {
        
        date = dict.value(forKey: "date") as? String
        let arrTimeSlot = dict["timeslot"] as? NSArray
        if arrTimeSlot != nil {
            for element in arrTimeSlot! {
                
                let dict = element as? NSDictionary
                let objTimeSlotDetail : TimeSlotDetail = TimeSlotDetail.init(dict: dict!) as TimeSlotDetail
                timeslot.add(objTimeSlotDetail)
                
            }
        }
        
    }
}
class AvailableDays: NSObject {
    
    var day : Int?
    var isAllocate : Bool?
    
    init(dict:NSDictionary) {
        day = dict.value(forKey: "day") as? Int
        isAllocate = dict.value(forKey: "isAllocate") as? Bool
    }
}
class ServiceDetail: NSObject {
    
    var categoryId : String?
    var subCatagoryId : String?
    var screenName : String?
    var appointmentDuration : Int?
    var availableDay : [Int]?
    //var availableMonth : [Int]?
    //var availableYear : [Int]?
    var availableTimes : [String]?
    var flatFee : String?
    var serviceDetail : String?
    var companyId : String?
    var serviceImage : String?
    var dayScheduling = [AllocatedDates]()
    var paymentMethod : Int!
    var paypalEmail : String?
    var keywords : String?
    
    init(dict:NSDictionary) {
        
        categoryId = dict.value(forKey: "categoryId") as? String
        subCatagoryId = dict.value(forKey: "subCategoryId") as? String ?? ""
        screenName = dict.value(forKey: "screenName") as? String
        appointmentDuration = dict.value(forKey: "appointmentDuration") as? Int
        availableDay = dict.value(forKey: "availableDay") as? [Int]
        //availableMonth = dict.value(forKey: "availableMonth") as? [Int]
        //availableYear = dict.value(forKey: "availableYear") as? [Int]
        availableTimes = dict.value(forKey: "availableTimes") as? [String]
        flatFee = dict.value(forKey: "flatFee") as? String
        serviceDetail = dict.value(forKey: "serviceDetail") as? String
        companyId = dict.value(forKey: "companyId") as? String
        serviceImage = dict.value(forKey: "serviceImage") as? String
        paypalEmail = dict["paypalEmail"] as? String ?? ""
        paymentMethod = dict["paymentMethod"] as? Int ?? 0
        keywords = dict["keywords"] as? String ?? ""
        
        let arrDayScheduling = dict["dayScheduling"] as? NSArray
        if arrDayScheduling != nil {
            for element in arrDayScheduling! {
                
                let dict = element as? NSDictionary
                let objAllocatedDates : AllocatedDates = AllocatedDates.init(dict: dict!) as AllocatedDates
                dayScheduling.append(objAllocatedDates)
            }
        }
        
    }
}

class PaymentDetails : NSObject {
    
    var firstName: String!
    var lastName: String!
    var city:String!
    var cityName:String!
    var state:String!
    var stateName:String!
    var street: String!
    var zipCode:String!
    var routingNumber:String!
    var accountNumber:String!
    
    init(dict:[String:Any]) {
        super.init()
        firstName = dict["firstName"] as? String ?? ""
        lastName = dict["lastName"] as? String ?? ""
        street = dict["streetAddress"] as? String ?? ""
        city = dict["city"] as? String ?? ""
        state = dict["state"] as? String ?? ""
        stateName = dict["stateName"] as? String ?? ""
        cityName = dict["cityName"] as? String ?? ""
        zipCode = dict["zipCode"] as? String ?? ""
        routingNumber = dict["routingNumber"] as? String ?? ""
        accountNumber = dict["accountNumber"] as? String ?? ""
        
    }
}



