//
//  MonthOccupiedData.swift
//  ShopByUber
//
//  Created by Chirag Kalsariya on 7/1/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class MonthOccupiedData: NSObject {

    var year : Int!
    var day : [Int]!
    var duration : Int!
    var month : Int!
    var timings : [String]!
    var timeIndexs : [Int]!
    var arrDayScheduling = [AllocatedDates]()
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        year = dictionary["year"] as? Int ?? 0
        day = dictionary["day"] as? [Int] ?? [0]
        duration = dictionary["duration"] as? Int ?? 0
        month = dictionary["month"] as? Int ?? 0
        timings = dictionary["timings"] as? [String] ?? [""]
        timeIndexs = dictionary["timiIndexs"] as?[Int] ?? [0]
        arrDayScheduling = (dictionary["dayScheduling"] as? [AllocatedDates])!
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if year != nil{
            dictionary["year"] = year
        }
        if day != nil{
            dictionary["day"] = day
        }
        if duration != nil{
            dictionary["duration"] = duration
        }
        if month != nil{
            dictionary["month"] = month
        }
        if timings != nil{
            dictionary["timings"] = timings
        }
    
        return dictionary
    }

}
