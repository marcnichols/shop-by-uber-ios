//
//  RequestDetail.swift
//  ShopByUber
//
//  Created by Nirav Shah on 6/29/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class RequestDetail: NSObject {
    
    var amountPaid : String!
    var catName : String!
    var subcatName : String!
    var isCancelled : Bool!
    var pubChannelName : String!
    var refundMethod : String!
    var requestFrom : String!
    var requestId : String!
    var schedule : String!
    var screenName : String!
    var serviceImage : String!
    var status : Int!
    var userAddress : String!
    var userLatitude : Double!
    var userLongitude : Double!
    var isRefund: Bool!
    var serviceDetail: String!
    var userProfileImage: String!
    var companyAddress : String!
    var companyLatitude : Double!
    var companyLongitude : Double!
    var cancelReason : String!
    var quickBloxId : Int!
    var receiverId : String!
    var cancelByText: String!
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        amountPaid = dictionary["amountPaid"] as? String ?? ""
        catName = dictionary["catName"] as? String ?? ""
        subcatName = dictionary["subCatName"] as? String ?? ""
        isCancelled = dictionary["isCancelled"] as? Bool ?? false
        pubChannelName = dictionary["pubChannelName"] as? String ?? ""
        refundMethod = dictionary["refundMethod"] as? String ?? ""
        requestFrom = dictionary["requestFrom"] as? String ?? ""
        requestId = dictionary["requestId"] as? String ?? ""
        schedule = dictionary["schedule"] as? String ?? ""
        screenName = dictionary["screenName"] as? String ?? ""
        serviceImage = dictionary["serviceImage"] as? String ?? ""
        status = dictionary["status"] as? Int ?? 0
        userLatitude = dictionary["userLatitude"] as? Double ?? 0.0
        userLongitude = dictionary["userLongitude"] as? Double ?? 0.0
        isRefund = dictionary["isRefund"] as? Bool ?? false
        userAddress = dictionary["userAddress"] as? String ?? ""
        serviceDetail = dictionary["serviceDetail"] as? String ?? ""
        userProfileImage = dictionary["userProfileImage"] as? String ?? ""
        companyAddress = dictionary["companyAddress"] as? String ?? ""
        companyLatitude = dictionary["companyLatitude"] as? Double ?? 0.0
        companyLongitude = dictionary["companyLongitude"] as? Double ?? 0.0
        cancelReason = dictionary["cancelReason"] as? String ?? ""
        quickBloxId = dictionary["quickBloxId"] as? Int ?? 0
        receiverId = dictionary["receiverId"] as? String ?? ""
        cancelByText = dictionary["cancelByText"] as? String ?? ""
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if amountPaid != nil{
            dictionary["amountPaid"] = amountPaid
        }
        if catName != nil{
            dictionary["catName"] = catName
        }
        if subcatName != nil{
            dictionary["subcatName"] = subcatName
        }
        if refundMethod != nil{
            dictionary["refundMethod"] = refundMethod
        }
        if requestFrom != nil{
            dictionary["requestFrom"] = requestFrom
        }
        if requestId != nil{
            dictionary["requestId"] = requestId
        }
        if schedule != nil{
            dictionary["schedule"] = schedule
        }
        if screenName != nil{
            dictionary["screenName"] = screenName
        }
        if serviceImage != nil{
            dictionary["serviceImage"] = serviceImage
        }
        if status != nil{
            dictionary["status"] = status
        }
        if isRefund != nil{
            dictionary["isRefund"] = isRefund
        }
        if userAddress != nil{
            dictionary["userAddress"] = userAddress
        }
        if serviceDetail != nil{
            dictionary["serviceDetail"] = serviceDetail
        }
        if userProfileImage != nil{
            dictionary["userProfileImage"] = userProfileImage
        }
        if companyAddress != nil{
            dictionary["companyAddress"] = companyAddress
        }
        if companyLatitude != nil{
            dictionary["companyLatitude"] = companyLatitude
        }
        if companyLongitude != nil{
            dictionary["companyLongitude"] = companyLongitude
        }
        if cancelReason != nil{
            dictionary["cancelReason"] = cancelReason
        }
        if quickBloxId != nil{
            dictionary["quickBloxId"] = quickBloxId
        }
        if cancelByText != nil{
            dictionary["cancelByText"] = cancelByText
        }
        
        return dictionary
    }
    
    class func getRequestDetail(requestID:String,isShowloader:Bool,success:@escaping(RequestDetail)->())
    {
        if isShowloader
        {
            KVNProgress.show()
        }
        var dictParam=[String:Any]()
        dictParam["userId"] = DataModel.sharedInstance.onFetchUserId()
        dictParam["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
        dictParam["requestId"] = requestID
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kViewRequestDetail, inputParams: dictParam, success: { (response) in
            
            KVNProgress.dismiss()
            if response?["Result"] as? Bool == true {
                let dict = response?["Data"] as? [String:Any]
                
                success(RequestDetail.init(fromDictionary: dict!))
            }
            else {
                let msg = response?["Message"] as? String
                Utilities.showAlertView(title: "", message: msg)
            }
            
        }) { (error) in
            
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
        }
        
    }

}

class OrderDetail : NSObject, NSCoding{
    
    var amountPaid : String!
    var catName : String!
    var subCatName : String!
    var companyId : String!
    var paymentMethod : String!
    var pubChannelName : String!
    var requestId : String!
    var schedule : String!
    var screenName : String!
    var serviceId : String!
    var serviceImage : String!
    var serviceProvider : String!
    var serviceProviderImage : String!
    var status : Int!
    var userLatitude : Double!
    var userLongitude : Double!
    var isCancelled: Bool!
    var isRefund: Bool!
    var isFeedbackGiven: Bool!
    var companyAddress : String!
    var companyLatitude : Double!
    var companyLongitude : Double!
    var cancelReason : String!
    var serviceDetail : String!
    var quickBloxId : Int!
    var receiverId : String!
    var cancelByText : String!

    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        
        amountPaid = dictionary["amountPaid"] as? String ?? ""
        catName = dictionary["catName"] as? String ?? ""
        subCatName = dictionary["subCatName"] as? String ?? ""
        companyId = dictionary["companyId"] as? String ?? ""
        paymentMethod = dictionary["paymentMethod"] as? String ?? ""
        pubChannelName = dictionary["pubChannelName"] as? String ?? ""
        requestId = dictionary["requestId"] as? String ?? ""
        schedule = dictionary["schedule"] as? String ?? ""
        screenName = dictionary["screenName"] as? String ?? ""
        serviceId = dictionary["serviceId"] as? String ?? ""
        serviceImage = dictionary["serviceImage"] as? String ?? ""
        serviceProvider = dictionary["serviceProvider"] as? String ?? ""
        serviceProviderImage = dictionary["serviceProviderImage"] as? String ?? ""
        status = dictionary["status"] as? Int ?? 0
        userLatitude = dictionary["userLatitude"] as? Double ?? 0.0
        userLongitude = dictionary["userLongitude"] as? Double ?? 0.0
        isCancelled = dictionary["isCancelled"] as? Bool ?? true
        isRefund = dictionary["isRefund"] as? Bool ?? false
        companyAddress = dictionary["companyAddress"] as? String ?? ""
        companyLatitude = dictionary["companyLatitude"] as? Double ?? 0.0
        companyLongitude = dictionary["companyLongitude"] as? Double ?? 0.0
        isFeedbackGiven = dictionary["isFeedbackGiven"] as? Bool ?? false
        cancelReason = dictionary["cancelReason"] as? String ?? ""
        serviceDetail = dictionary["serviceDetail"] as? String ?? ""
        quickBloxId = dictionary["quickBloxId"] as? Int ?? 0
        receiverId = dictionary["receiverId"] as? String ?? ""
        cancelByText = dictionary["cancelByText"] as? String ?? ""
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if amountPaid != nil{
            dictionary["amountPaid"] = amountPaid
        }
        if catName != nil{
            dictionary["catName"] = catName
        }
        if subCatName != nil{
            dictionary["subcatName"] = subCatName
        }
        if companyId != nil{
            dictionary["companyId"] = companyId
        }
        if paymentMethod != nil{
            dictionary["paymentMethod"] = paymentMethod
        }
        if pubChannelName != nil{
            dictionary["pubChannelName"] = pubChannelName
        }
        if requestId != nil{
            dictionary["requestId"] = requestId
        }
        if schedule != nil{
            dictionary["schedule"] = schedule
        }
        if screenName != nil{
            dictionary["screenName"] = screenName
        }
        if serviceId != nil{
            dictionary["serviceId"] = serviceId
        }
        if serviceImage != nil{
            dictionary["serviceImage"] = serviceImage
        }
        if serviceProvider != nil{
            dictionary["serviceProvider"] = serviceProvider
        }
        if serviceProviderImage != nil{
            dictionary["serviceProviderImage"] = serviceProviderImage
        }
        if status != nil{
            dictionary["status"] = status
        }
        if userLatitude != nil{
            dictionary["userLatitude"] = userLatitude
        }
        if userLongitude != nil{
            dictionary["userLongitude"] = userLongitude
        }
        if isRefund != nil{
            dictionary["isRefund"] = isRefund
        }
        if companyAddress != nil{
            dictionary["companyAddress"] = companyAddress
        }
        if companyLatitude != nil{
            dictionary["companyLatitude"] = companyLatitude
        }
        if companyLongitude != nil{
            dictionary["companyLongitude"] = companyLongitude
        }
        if isFeedbackGiven != nil{
            dictionary["isFeedbackGiven"] = isFeedbackGiven
        }
        if cancelReason != nil{
            dictionary["cancelReason"] = cancelReason
        }
        if quickBloxId != nil{
            dictionary["quickBloxId"] = quickBloxId
        }
        if receiverId != nil {
            dictionary["receiverId"] = receiverId
        }
        if cancelByText != nil {
            dictionary["cancelByText"] = cancelByText
        }
        
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        amountPaid = aDecoder.decodeObject(forKey: "amountPaid") as? String
        catName = aDecoder.decodeObject(forKey: "catName") as? String
        companyId = aDecoder.decodeObject(forKey: "companyId") as? String
        paymentMethod = aDecoder.decodeObject(forKey: "paymentMethod") as? String
        pubChannelName = aDecoder.decodeObject(forKey: "pubChannelName") as? String
        requestId = aDecoder.decodeObject(forKey: "requestId") as? String
        schedule = aDecoder.decodeObject(forKey: "schedule") as? String
        screenName = aDecoder.decodeObject(forKey: "screenName") as? String
        serviceId = aDecoder.decodeObject(forKey: "serviceId") as? String
        serviceImage = aDecoder.decodeObject(forKey: "serviceImage") as? String
        serviceProvider = aDecoder.decodeObject(forKey: "serviceProvider") as? String
        serviceProviderImage = aDecoder.decodeObject(forKey: "serviceProviderImage") as? String
        status = aDecoder.decodeObject(forKey: "status") as? Int
        userLatitude = aDecoder.decodeObject(forKey: "userLatitude") as? Double
        userLongitude = aDecoder.decodeObject(forKey: "userLongitude") as? Double
        isCancelled = aDecoder.decodeObject(forKey: "isCancelled") as? Bool ?? true
        isRefund = aDecoder.decodeObject(forKey: "isRefund") as? Bool ?? true
        companyAddress = aDecoder.decodeObject(forKey: "companyAddress") as? String
        companyLatitude = aDecoder.decodeObject(forKey: "companyLatitude") as? Double
        companyLongitude = aDecoder.decodeObject(forKey: "companyLongitude") as? Double
        isFeedbackGiven = aDecoder.decodeObject(forKey: "isFeedbackGiven") as? Bool ?? false
        cancelReason  = aDecoder.decodeObject(forKey: "cancelReason") as? String
        quickBloxId = aDecoder.decodeObject(forKey: "quickBloxId") as? Int ?? 0
        receiverId = aDecoder.decodeObject(forKey: "receiverId") as? String
        cancelByText = aDecoder.decodeObject(forKey: "cancelByText") as? String
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if amountPaid != nil{
            aCoder.encode(amountPaid, forKey: "amountPaid")
        }
        if catName != nil{
            aCoder.encode(catName, forKey: "catName")
        }
        if companyId != nil{
            aCoder.encode(companyId, forKey: "companyId")
        }
        if paymentMethod != nil{
            aCoder.encode(paymentMethod, forKey: "paymentMethod")
        }
        if pubChannelName != nil{
            aCoder.encode(pubChannelName, forKey: "pubChannelName")
        }
        if requestId != nil{
            aCoder.encode(requestId, forKey: "requestId")
        }
        if schedule != nil{
            aCoder.encode(schedule, forKey: "schedule")
        }
        if screenName != nil{
            aCoder.encode(screenName, forKey: "screenName")
        }
        if serviceId != nil{
            aCoder.encode(serviceId, forKey: "serviceId")
        }
        if serviceImage != nil{
            aCoder.encode(serviceImage, forKey: "serviceImage")
        }
        if serviceProvider != nil{
            aCoder.encode(serviceProvider, forKey: "serviceProvider")
        }
        if serviceProviderImage != nil{
            aCoder.encode(serviceProviderImage, forKey: "serviceProviderImage")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        if userLatitude != nil{
            aCoder.encode(userLatitude, forKey: "userLatitude")
        }
        if userLongitude != nil{
            aCoder.encode(userLongitude, forKey: "userLongitude")
        }
        if isRefund != nil{
            aCoder.encode(isRefund, forKey: "isRefund")
        }
        if companyAddress != nil{
            aCoder.encode(companyAddress, forKey: "companyAddress")
        }
        if companyLongitude != nil{
            aCoder.encode(companyLongitude, forKey: "companyLongitude")
        }
        if companyLatitude != nil{
            aCoder.encode(companyLatitude, forKey: "companyLatitude")
        }
        if isFeedbackGiven != nil{
            aCoder.encode(isFeedbackGiven, forKey: "isFeedbackGiven")
        }
        if cancelReason != nil{
            aCoder.encode(cancelReason, forKey: "cancelReason")
        }
        if quickBloxId != nil{
            aCoder.encode(quickBloxId, forKey: "quickBloxId")
        }
        if receiverId != nil{
            aCoder.encode(receiverId, forKey: "receiverId")
        }
        if cancelByText != nil{
            aCoder.encode(cancelByText, forKey: "cancelByText")
        }
    }
    
    class func getOrderDetail(requestID:String,isShowloader:Bool,success:@escaping(OrderDetail)->())
    {
        if isShowloader
        {
            KVNProgress.show()
        }
        var dictParam=[String:Any]()
        dictParam["userId"] = DataModel.sharedInstance.onFetchUserId()
        dictParam["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
        dictParam["requestId"] = requestID
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kGetOrderDetail, inputParams: dictParam, success: { (response) in
            
            KVNProgress.dismiss()
            if response?["Result"] as? Bool == true {
                let dict = response?["Data"] as? [String:Any]
                
                success(OrderDetail.init(fromDictionary: dict!))
               
            }
            else {
                let msg = response?["Message"] as? String
                Utilities.showAlertView(title: "", message: msg)
            }
            
        }) { (error) in
            
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
        }
        
    }
}


class MapData : NSObject, NSCoding{
    
    var address : String!
    var catName : String!
    var companyAddress : String!
    var companyLatitude : Double!
    var companyLongitude : Double!
    var pubChannelName : String!
    var requestId : String!
    var serviceProvider : String!
    var serviceProviderImage : String!
    var userLatitude : Double!
    var userLongitude : Double!
    var status: Int!
    var displayMessage: String!
    var screenName: String!
    var requestFee: String!
    var paymentMethod: String!
    var duration: String!
    var serviceId : String!
    var companyId : String!
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        address = dictionary["address"] as? String
        catName = dictionary["catName"] as? String
        companyAddress = dictionary["companyAddress"] as? String
        companyLatitude = dictionary["companyLatitude"] as? Double ?? 0.0
        companyLongitude = dictionary["companyLongitude"] as? Double ?? 0.0
        pubChannelName = dictionary["pubChannelName"] as? String
        requestId = dictionary["requestId"] as? String
        serviceProvider = dictionary["serviceProvider"] as? String
        serviceProviderImage = dictionary["serviceProviderImage"] as? String ?? ""
        userLatitude = dictionary["userLatitude"] as? Double ?? 0.0
        userLongitude = dictionary["userLongitude"] as? Double ?? 0.0
        status = dictionary["status"] as? Int
        displayMessage = dictionary["displayMessage"] as? String
        screenName = dictionary["screenName"] as? String
        requestFee = dictionary["requestFee"] as? String
        paymentMethod = dictionary["paymentMethod"] as? String
        duration = dictionary["duration"] as? String
        serviceId = dictionary["serviceId"] as? String
        companyId = dictionary["companyId"] as? String
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if address != nil{
            dictionary["address"] = address
        }
        if catName != nil{
            dictionary["catName"] = catName
        }
        if companyAddress != nil{
            dictionary["companyAddress"] = companyAddress
        }
        if companyLatitude != nil{
            dictionary["companyLatitude"] = companyLatitude
        }
        if companyLongitude != nil{
            dictionary["companyLongitude"] = companyLongitude
        }
        if pubChannelName != nil{
            dictionary["pubChannelName"] = pubChannelName
        }
        if requestId != nil{
            dictionary["requestId"] = requestId
        }
        if serviceProvider != nil{
            dictionary["serviceProvider"] = serviceProvider
        }
        if serviceProviderImage != nil{
            dictionary["serviceProviderImage"] = serviceProviderImage
        }
        if userLatitude != nil{
            dictionary["userLatitude"] = userLatitude
        }
        if userLongitude != nil{
            dictionary["userLongitude"] = userLongitude
        }
        if status != nil{
            dictionary["status"] = status
        }
        if displayMessage != nil{
            dictionary["displayMessage"] = displayMessage
        }
        if screenName != nil{
            dictionary["screenName"] = screenName
        }
        if requestFee != nil{
            dictionary["requestFee"] = requestFee
        }
        if paymentMethod != nil{
            dictionary["paymentMethod"] = paymentMethod
        }
        if duration != nil{
            dictionary["duration"] = duration
        }
        if serviceId != nil{
            dictionary["serviceId"] = serviceId
        }
        if companyId != nil{
            dictionary["companyId"] = companyId
        }
        
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        address = aDecoder.decodeObject(forKey: "address") as? String
        catName = aDecoder.decodeObject(forKey: "catName") as? String
        companyAddress = aDecoder.decodeObject(forKey: "companyAddress") as? String
        companyLatitude = aDecoder.decodeObject(forKey: "companyLatitude") as? Double
        companyLongitude = aDecoder.decodeObject(forKey: "companyLongitude") as? Double
        pubChannelName = aDecoder.decodeObject(forKey: "pubChannelName") as? String
        requestId = aDecoder.decodeObject(forKey: "requestId") as? String
        serviceProvider = aDecoder.decodeObject(forKey: "serviceProvider") as? String
        serviceProviderImage = aDecoder.decodeObject(forKey: "serviceProviderImage") as? String
        userLatitude = aDecoder.decodeObject(forKey: "userLatitude") as? Double
        userLongitude = aDecoder.decodeObject(forKey: "userLongitude") as? Double
        status = aDecoder.decodeObject(forKey: "status") as? Int
        displayMessage = aDecoder.decodeObject(forKey: "displayMessage") as? String
        screenName     = aDecoder.decodeObject(forKey: "screenName") as? String
        requestFee = aDecoder.decodeObject(forKey: "requestFee") as? String
        paymentMethod = aDecoder.decodeObject(forKey: "paymentMethod") as? String
        duration = aDecoder.decodeObject(forKey: "duration") as? String
        serviceId = aDecoder.decodeObject(forKey: "serviceId") as? String
        companyId = aDecoder.decodeObject(forKey: "companyId") as? String
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if address != nil{
            aCoder.encode(address, forKey: "address")
        }
        if catName != nil{
            aCoder.encode(catName, forKey: "catName")
        }
        if companyAddress != nil{
            aCoder.encode(companyAddress, forKey: "companyAddress")
        }
        if companyLatitude != nil{
            aCoder.encode(companyLatitude, forKey: "companyLatitude")
        }
        if companyLongitude != nil{
            aCoder.encode(companyLongitude, forKey: "companyLongitude")
        }
        if pubChannelName != nil{
            aCoder.encode(pubChannelName, forKey: "pubChannelName")
        }
        if requestId != nil{
            aCoder.encode(requestId, forKey: "requestId")
        }
        if serviceProvider != nil{
            aCoder.encode(serviceProvider, forKey: "serviceProvider")
        }
        if serviceProviderImage != nil{
            aCoder.encode(serviceProviderImage, forKey: "serviceProviderImage")
        }
        if userLatitude != nil{
            aCoder.encode(userLatitude, forKey: "userLatitude")
        }
        if userLongitude != nil{
            aCoder.encode(userLongitude, forKey: "userLongitude")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        if displayMessage != nil{
            aCoder.encode(displayMessage, forKey: "displayMessage")
        }
        if screenName != nil{
            aCoder.encode(screenName, forKey: "screenName")
        }
        if paymentMethod != nil{
            aCoder.encode(paymentMethod, forKey: "paymentMethod")
        }
        if duration != nil{
            aCoder.encode(duration, forKey: "duration")
        }
        if requestFee != nil{
            aCoder.encode(requestFee, forKey: "requestFee")
        }
        if serviceId != nil{
            aCoder.encode(requestFee, forKey: "serviceId")
        }
        if companyId != nil{
            aCoder.encode(requestFee, forKey: "companyId")
        }
        
    }
    
}
