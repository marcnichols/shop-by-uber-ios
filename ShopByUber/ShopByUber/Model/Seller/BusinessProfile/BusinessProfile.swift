//
//  OccupiedData.swift
//  ShopByUber
//
//  Created by Administrator on 6/20/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class BusinessProfile: NSObject {

    var companyName: String?
    var firstName: String?
    var lastName: String?
    var comapnyScreenName:String?
    var companyAddress:String?
    var latitude: Double?
    var longitude: Double?
    var zipcode: String?
    var paypalEmail: String!
    var companyPhone: String?
    var websiteUrl: String?
    var serviceDescription: String?
    var companyProfileImage: String?
    var companyId: String?
    var serviceImage: String?
    var countryCode: String?
    var paymentMethod: Int!
    var promoCode: String!
    
    init(dict:[String:Any]) {
        super.init()
        
        companyName            = dict["companyName"] as? String
        firstName              = dict["firstName"] as? String
        lastName               = dict["lastName"] as? String
        comapnyScreenName      = dict["comapnyScreenName"] as? String
        companyAddress         = dict["companyAddress"] as? String
        latitude               = dict["latitude"] as? Double
        longitude              = dict["longitude"] as? Double
        zipcode                = dict["zipCode"] as? String
        companyPhone           = dict["companyPhoneNumber"] as? String
        websiteUrl             = dict["websiteUrl"] as? String
        serviceDescription     = dict["serviceDescription"] as? String
        companyProfileImage    = dict["companyProfileImage"] as? String
        companyId              = dict["companyId"] as? String
        serviceImage           = dict["serviceImage"] as? String
        countryCode            = dict["countryCode"] as? String
        paypalEmail            = dict["paypalEmail"] as? String ?? ""
        paymentMethod          = dict["paymentMethod"] as? Int ?? 0
        promoCode              = dict["promoCode"] as? String ?? ""
        
    }
}

class PaymentData : NSObject {
    
    var firstName: String!
    var lastName: String!
    var city:String!
    var cityName:String!
    var state:String!
    var stateName:String!
    var street: String!
    var zipCode:String!
    var routingNumber:String!
    var accountNumber:String!
    
    init(dict:[String:Any]) {
        super.init()
        firstName = dict["firstName"] as? String ?? ""
        lastName = dict["lastName"] as? String ?? ""
        street = dict["streetAddress"] as? String ?? ""
        city = dict["city"] as? String ?? ""
        state = dict["state"] as? String ?? ""
        stateName = dict["stateName"] as? String ?? ""
        cityName = dict["cityName"] as? String ?? ""
        zipCode = dict["zipCode"] as? String ?? ""
        routingNumber = dict["routingNumber"] as? String ?? ""
        accountNumber = dict["accountNumber"] as? String ?? ""
        
    }
}

class sellerUserData : NSObject, NSCoding{
    
    var countryCode : String!
    var descriptionField : String!
    var email : String!
    var legalBusinessName : String!
    var mobilePhone : String!
    var name : String!
    var phoneNumber : String!
    var profileImage : String!
    var speciality : String!
    var timeZone : String!
    var userRole : Int!
    var address : String!
    var isSalesperson : Bool!
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        countryCode = dictionary["countryCode"] as? String
        descriptionField = dictionary["description"] as? String
        email = dictionary["email"] as? String
        legalBusinessName = dictionary["legalBusinessName"] as? String
        mobilePhone = dictionary["mobilePhone"] as? String
        name = dictionary["name"] as? String
        phoneNumber = dictionary["phoneNumber"] as? String
        profileImage = dictionary["profileImage"] as? String
        speciality = dictionary["speciality"] as? String
        timeZone = dictionary["timeZone"] as? String
        userRole = dictionary["userRole"] as? Int
        address = dictionary["address"] as? String
        isSalesperson = dictionary["isSalesperson"] as? Bool ?? false
    }


    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if countryCode != nil{
            dictionary["countryCode"] = countryCode
        }
        if descriptionField != nil{
            dictionary["description"] = descriptionField
        }
        if email != nil{
            dictionary["email"] = email
        }
        if legalBusinessName != nil{
            dictionary["legalBusinessName"] = legalBusinessName
        }
        if mobilePhone != nil{
            dictionary["mobilePhone"] = mobilePhone
        }
        if name != nil{
            dictionary["name"] = name
        }
        if phoneNumber != nil{
            dictionary["phoneNumber"] = phoneNumber
        }
        if profileImage != nil{
            dictionary["profileImage"] = profileImage
        }
        if speciality != nil{
            dictionary["speciality"] = speciality
        }
        if timeZone != nil{
            dictionary["timeZone"] = timeZone
        }
        if userRole != nil{
            dictionary["userRole"] = userRole
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        countryCode = aDecoder.decodeObject(forKey: "countryCode") as? String
        descriptionField = aDecoder.decodeObject(forKey: "description") as? String
        email = aDecoder.decodeObject(forKey: "email") as? String
        legalBusinessName = aDecoder.decodeObject(forKey: "legalBusinessName") as? String
        mobilePhone = aDecoder.decodeObject(forKey: "mobilePhone") as? String
        name = aDecoder.decodeObject(forKey: "name") as? String
        phoneNumber = aDecoder.decodeObject(forKey: "phoneNumber") as? String
        profileImage = aDecoder.decodeObject(forKey: "profileImage") as? String
        speciality = aDecoder.decodeObject(forKey: "speciality") as? String
        timeZone = aDecoder.decodeObject(forKey: "timeZone") as? String
        userRole = aDecoder.decodeObject(forKey: "userRole") as? Int
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if countryCode != nil{
            aCoder.encode(countryCode, forKey: "countryCode")
        }
        if descriptionField != nil{
            aCoder.encode(descriptionField, forKey: "description")
        }
        if email != nil{
            aCoder.encode(email, forKey: "email")
        }
        if legalBusinessName != nil{
            aCoder.encode(legalBusinessName, forKey: "legalBusinessName")
        }
        if mobilePhone != nil{
            aCoder.encode(mobilePhone, forKey: "mobilePhone")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if phoneNumber != nil{
            aCoder.encode(phoneNumber, forKey: "phoneNumber")
        }
        if profileImage != nil{
            aCoder.encode(profileImage, forKey: "profileImage")
        }
        if speciality != nil{
            aCoder.encode(speciality, forKey: "speciality")
        }
        if timeZone != nil{
            aCoder.encode(timeZone, forKey: "timeZone")
        }
        if userRole != nil{
            aCoder.encode(userRole, forKey: "userRole")
        }
        
    }
    
}
