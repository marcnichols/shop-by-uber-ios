//
//  Appointment.swift
//  ShopByUber
//
//  Created by Nirav Shah on 6/29/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class Appointment : NSObject{
    
    var address : String!
    var duration : String!
    var latitude : Double!
    var longitude : Double!
    var pubChannelName : String!
    var requestId : String!
    var screenName : String!
    var serviceImage : String!
    var status : Int!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        address = dictionary["address"] as? String ?? ""
        duration = dictionary["duration"] as? String ?? ""
        latitude = dictionary["latitude"] as? Double ?? 0.0
        longitude = dictionary["longitude"] as? Double ?? 0.0
        pubChannelName = dictionary["pubChannelName"] as? String ?? ""
        requestId = dictionary["requestId"] as? String ?? ""
        screenName = dictionary["screenName"] as? String ?? ""
        serviceImage = dictionary["serviceImage"] as? String ?? ""
        status = dictionary["status"] as? Int ?? 0
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if address != nil{
            dictionary["address"] = address
        }
        if duration != nil{
            dictionary["duration"] = duration
        }
        if latitude != nil{
            dictionary["latitude"] = latitude
        }
        if longitude != nil{
            dictionary["longitude"] = longitude
        }
        if pubChannelName != nil{
            dictionary["pubChannelName"] = pubChannelName
        }
        if requestId != nil{
            dictionary["requestId"] = requestId
        }
        if screenName != nil{
            dictionary["screenName"] = screenName
        }
        if serviceImage != nil{
            dictionary["serviceImage"] = serviceImage
        }
        if status != nil{
            dictionary["status"] = status
        }
        return dictionary
    }
    
    class func getAppointment(appointTypeURL:String,pageNo:Int,numOfRecords:Int,isShowloader:Bool,success:@escaping([Appointment])->(),failure:@escaping()->())
    {
        if isShowloader
        {
            KVNProgress.show()
        }
        var dictParam=[String:Any]()
        dictParam["userId"] = DataModel.sharedInstance.onFetchUserId()
        dictParam["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
        dictParam["pageNo"] = pageNo
        dictParam["numOfRecords"] = numOfRecords
        
        APIManager.callAPI(url: appointTypeURL, inputParams: dictParam, success: { (response) in
            
            KVNProgress.dismiss()
            if response?["Result"] as? Bool == true {
                let dict = response?["Data"] as? [String:Any]
                let data = dict?["appointment"] as! NSArray
                
                var arrAppoint = [Appointment]()
                
                data.enumerateObjects({ (obj, idx, nil) in
                    arrAppoint.append(Appointment(fromDictionary: obj as! [String:Any]))
                })
                
                success(arrAppoint)
            }
            else {
                _ = response?["Message"] as? String
                //Utilities.showAlertView(title: "", message: msg)
                failure()
            }
            
        }) { (error) in
            
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
            failure()
        }
    }

}
