//
//  MyServices.swift
//  ShopByUber
//
//  Created by Administrator on 6/12/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class MyServices: NSObject {
    
    var id: String?
    var screenName:String?
    var isAvailable: Bool?
    var serviceImage:String?
    var flatFee :String?
    var editable:Bool?
    
    override init() {
        
    }
    
    init(dict:[String:Any]) {
        super.init()
        
        id = dict["serviceId"] as? String
        screenName = dict["screenName"] as? String
        serviceImage = dict["serviceImage"] as? String
        isAvailable = dict["isAvailable"] as? Bool
        flatFee = dict["flatFee"] as? String
        editable = dict["editAble"] as? Bool
        
    }
    
    //MARK: API Call
    
    class func callGetMyServicesAPI(pageNo: Int?,numOfRecords:Int?, success: @escaping ([MyServices],Int) -> (), failure: @escaping () -> ()) {
        
        
        let dict = getRequestParamsForGetMyServices(pageNo: pageNo, numOfRecords: numOfRecords)
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kGetMyServices, inputParams: dict, success: { (dict) in
            
            
            if dict?["Result"] as? Bool == true {
                
                let data = dict?["Data"] as? [String:Any]
                let services = data?["services"] as? [[String:Any]]
                let recordcnt = data?["recordCount"] as? Int
                var arrServices = [MyServices]()
                
                for service in services!{
                    
                    let objPicks = MyServices.init(dict: service)
                    arrServices.append(objPicks)
                    
                }
                success(arrServices,recordcnt!)
            }
                
            else {
                
                let msg = dict?["Message"] as? String
                Utilities.showAlertView(title: "", message: msg)
                failure()
                
            }
            
            
        }) { error in
            
            
            Utilities.showAlertView(title: "", message: error)
            failure()
        }
        
        
    }
    
    private class func getRequestParamsForGetMyServices(pageNo: Int?,numOfRecords:Int?) -> [String : Any]  {
        
        
        var dict = [String : Any]()
        
        dict["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
        dict["userId"] = DataModel.sharedInstance.onFetchUserId()
        dict["pageNo"] = pageNo
        dict["numOfRecords"] = numOfRecords
        
        return dict
    }
    
    class func callSetServiceAvailabilityAPI(serviceId:String?,isAvailable:Bool?, success: @escaping () -> (), failure: @escaping () -> ()) {
        
        KVNProgress.show()
        
        var dict = [String : Any]()
        
        dict["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
        dict["userId"] = DataModel.sharedInstance.onFetchUserId()
        dict["serviceId"] = serviceId
        dict["isAvailable"] = isAvailable
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kSetServiceAvailability, inputParams: dict, success: { (dict) in
            
            KVNProgress.dismiss()
            
            if dict?["Result"] as? Bool == true {
                
                success()
            }
                
            else {
                
                let msg = dict?["Message"] as? String
                Utilities.showAlertView(title: "", message: msg)
                failure()
                
            }
            
            
        }) { error in
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
            failure()
        }
        
    }
}
