//
//  SwitchUser.swift
//  The EZ List
//
//  Created by DIFFERENZ101 on 12/1/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class SwitchUser: NSObject {

    var userRole : Int
    var deviceToken : String
    var timeZone : String
    var countryCode : Int
    var phoneNumber : Int
    var ezListScreenName : String
    var mobilePhone : Int
    var email : String
    var address : String
    var profileImage : String
    var name : String
    var desc : String
    var speciality : String
    var sessionId : String
    var userId : String
    var shortprofileImage : String
    var appUdate : Bool
    var isCompanyProfileCreated: Bool
    var legalBusinessName: String
    
    init(fromDictionary dictionary: [String:Any]){
        userRole = (dictionary["userRole"] as! Int?)!
        deviceToken = dictionary["deviceToken"] as! String
        timeZone = dictionary["timeZone"] as! String
        countryCode = dictionary["countryCode"] as! Int
        phoneNumber = dictionary["phoneNumber"] as! Int
        ezListScreenName = dictionary["ezListScreenName"] as! String
        mobilePhone = dictionary["mobilePhone"] as! Int
        email = dictionary["email"] as! String
        address = dictionary["address"] as! String
        profileImage = dictionary["profileImage"] as! String
        name = dictionary["name"] as! String
        desc = dictionary["description"] as! String
        speciality = dictionary["speciality"] as! String
        sessionId = dictionary["sessionId"] as! String
        userId = dictionary["userId"] as! String
        shortprofileImage = dictionary["shortprofileImage"] as! String
        appUdate = (dictionary["appUdate"] != nil)
        isCompanyProfileCreated = (dictionary["isCompanyProfileCreated"] != nil)
        legalBusinessName = (dictionary["legalBusinessName"] as! String)
    }
    
    class func switchUser(userId: String,sessionId: String,switchTo: Int,success:@escaping([String:Any]?)->(),failure:@escaping()->())
    {
        KVNProgress.show()
    
        var dictParam=[String:Any]()
        dictParam["userId"] = DataModel.sharedInstance.onFetchUserId()
        dictParam["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
        dictParam["switchTo"] = switchTo
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kSwitchUser, inputParams: dictParam, success: { (response) in
            
            KVNProgress.dismiss()
            if response?["Result"] as? Bool == true {
                let dict = response?["Data"] as? [String:Any]
    
                let sessionId = dict?["sessionId"] as? String
                let userType = dict?["userRole"] as? Int
                let email = dict?["email"] as? String
                let mobilePhone = dict?["mobilePhone"] as? String
                let name = dict?["name"] as? String
                let firstName = dict?["firstName"] as? String
                let lastName = dict?["lastName"] as? String
                let userImage = dict?["profileImage"] as? String
                let description = dict?["description"] as? String
                let countryCode = dict?["countryCode"] as? String
                let phoneNumber = dict?["phoneNumber"] as? String
                let ezListScreenName = dict?["ezListScreenName"] as? String
                let promoCode = dict?["promoCode"] as? String
                let isSalesperson = dict?["isSalesperson"] as? Bool ?? false
                let isServicesCreated = dict?["isServicesCreated"] as? Bool ?? false
                let city = dict?["city"] as? String
                let state = dict?["state"] as? String
                let address = dict?["address"] as? String
                let lat = dict?["latitude"] as? Double
                let long = dict?["longitude"] as? Double
                
                DataModel.sharedInstance.setUserDataWhenLogin(userID: userId, sessionId: sessionId, userType: userType, email: email, name: name, firstName: firstName, lastName: lastName, mobile: mobilePhone,phone:phoneNumber,countryCode: countryCode,userProfile: userImage, ezListScreenName: ezListScreenName, description: description, promoCode: promoCode)
                
                DataModel.sharedInstance.setUserDefaultIsSalesperson(isSalesperson: isSalesperson)
                DataModel.sharedInstance.setValueInUserDefaults(value: dict?["isServicesCreated"] as? Bool, key: Constant.DataModelKey.kIsServiceAvailable)
                DataModel.sharedInstance.setValueInUserDefaults(value: city, key: Constant.DataModelKey.kCity)
                DataModel.sharedInstance.setValueInUserDefaults(value: state, key: Constant.DataModelKey.kState)
                DataModel.sharedInstance.setValueInUserDefaults(value: address, key: Constant.DataModelKey.kaddress)
                DataModel.sharedInstance.setLatitude(lat: lat)
                DataModel.sharedInstance.setLongtitude(long: long)
                
                NotificationCenter.default.post(name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil, userInfo: nil)
                success(dict)
            }
            else {
                let msg = response?["Message"] as? String
                Utilities.showAlertView(title: "", message: msg)
                failure()
            }
            
            
        }) { (error) in
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
            failure()
        }
        
        
    }
}
