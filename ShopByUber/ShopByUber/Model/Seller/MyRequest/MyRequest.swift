//
//  MyRequest.swift
//
//  Created by Nirav Shah on 6/28/17
//  Copyright (c) . All rights reserved.
//

import Foundation

class MyRequest : NSObject{
    
    var duration : String!
    var requestId : String!
    var screenName : String!
    var serviceImage : String!
    var status : Int!
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        duration = dictionary[Constant.DataModelKey.kDuration] as? String ?? ""
        requestId = dictionary[Constant.DataModelKey.kRequestId] as? String ?? ""
        screenName = dictionary[Constant.DataModelKey.kScreenName] as? String ?? ""
        serviceImage = dictionary[Constant.DataModelKey.kServiceImage] as? String ?? ""
        status = dictionary[Constant.DataModelKey.kStatus] as? Int ?? 0
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if duration != nil{
            dictionary[Constant.DataModelKey.kDuration] = duration
        }
        if requestId != nil{
            dictionary[Constant.DataModelKey.kRequestId] = requestId
        }
        if screenName != nil{
            dictionary[Constant.DataModelKey.kScreenName] = screenName
        }
        if serviceImage != nil{
            dictionary[Constant.DataModelKey.kServiceImage] = serviceImage
        }
        if status != nil{
            dictionary[Constant.DataModelKey.kStatus] = status
        }
        return dictionary
    }
    
    class func getMyRequest(pageNo:Int,numOfRecords:Int,isShowloader:Bool,success:@escaping([MyRequest])->(),failure:@escaping()->())
    {
        if isShowloader
        {
            KVNProgress.show()
        }
        var dictParam=[String:Any]()
        dictParam["userId"] = DataModel.sharedInstance.onFetchUserId()
        dictParam["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
        dictParam["pageNo"] = pageNo
        dictParam["numOfRecords"] = numOfRecords
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kMyRequest, inputParams: dictParam, success: { (response) in
            
            KVNProgress.dismiss()
            if response?["Result"] as? Bool == true {
                let dict = response?["Data"] as? [String:Any]
                let data = dict?["requests"] as! NSArray
                
                var arrRequest = [MyRequest]()
                
                data.enumerateObjects({ (obj, idx, nil) in
                    arrRequest.append(MyRequest(fromDictionary: obj as! [String:Any]))
                })
                
                success(arrRequest)
            }
            else {
                //let msg = response?["Message"] as? String
                //Utilities.showAlertView(title: "", message: msg)
                failure()
            }

        }) { (error) in
            
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
            failure()
        }
    }

}
