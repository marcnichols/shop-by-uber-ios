//
//	MySales.swift
//
//	Create by Chirag Kalsariya on 14/7/2017
//	Copyright © 2017. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class MySales : NSObject{

	var duration : String!
	var flatFee : String!
	var requestId : String!
	var screenName : String!
	var serviceImage : String!
    var bookedBy: String!
	var status : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		duration = dictionary["duration"] as? String ?? ""
		flatFee = dictionary["flatFee"] as? String ?? ""
		requestId = dictionary["requestId"] as? String ?? ""
		screenName = dictionary["screenName"] as? String ?? ""
		serviceImage = dictionary["serviceImage"] as? String ?? ""
        bookedBy = dictionary["bookedBy"] as? String ?? ""
		status = dictionary["status"] as? Int ?? 0
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if duration != nil{
			dictionary["duration"] = duration
		}
		if flatFee != nil{
			dictionary["flatFee"] = flatFee
		}
		if requestId != nil{
			dictionary["requestId"] = requestId
		}
		if screenName != nil{
			dictionary["screenName"] = screenName
		}
		if serviceImage != nil{
			dictionary["serviceImage"] = serviceImage
		}
		if status != nil{
			dictionary["status"] = status
		}
        if bookedBy != nil{
            dictionary["bookedBy"] = bookedBy
        }
		return dictionary
	}

    class func getMySales(pageNo:Int,numOfRecords:Int,isShowloader:Bool,success:@escaping([MySales],Int)->(),failure:@escaping()->())
    {
        if isShowloader
        {
            KVNProgress.show()
        }
        var dictParam=[String:Any]()
        dictParam["userId"] = DataModel.sharedInstance.onFetchUserId()
        dictParam["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
        dictParam["pageNo"] = pageNo
        dictParam["numOfRecords"] = numOfRecords
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kGetMySales, inputParams: dictParam, success: { (response) in
            
            KVNProgress.dismiss()
            if response?["Result"] as? Bool == true {
                let data = response?["Data"] as? [String:Any]
                let sales = data?["sales"] as! NSArray
                let recordcnt = data?["recordCount"] as? Int
                
                var arrSalse = [MySales]()
                
                sales.enumerateObjects({ (obj, idx, nil) in
                    arrSalse.append(MySales(fromDictionary: obj as! [String:Any]))
                })
                
                 success(arrSalse,recordcnt!)
            }
            else {
                _ = response?["Message"] as? String
                
                failure()
            }
            
        }) { (error) in
            
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
            failure()
        }

    }
}

