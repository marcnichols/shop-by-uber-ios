//
//  RecentServiceModel.swift
//  ShopByUber
//
//  Created by Differenz215 on 7/6/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import Foundation
import Foundation


class RecentServiceModel : NSObject, NSCoding{
    
    var duration : String!
    var flatFee : String!
    var requestId : String!
    var screenName : String!
    var serviceImage : String!
    var status : Int!
    var companyId: String!
    var categoryId: String!
    var serviceId: String!
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        duration = dictionary["duration"] as? String ?? ""
        flatFee = dictionary["flatFee"] as? String ?? ""
        requestId = dictionary["requestId"] as? String ?? ""
        screenName = dictionary["screenName"] as? String ?? ""
        serviceImage = dictionary["serviceImage"] as? String ?? ""
        status = dictionary["status"] as? Int ?? 0
        companyId = dictionary["companyId"] as? String ?? ""
        categoryId = dictionary["categoryId"] as? String ?? ""
        serviceId = dictionary["serviceId"] as? String ?? ""
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if duration != nil{
            dictionary["duration"] = duration
        }
        if flatFee != nil{
            dictionary["flatFee"] = flatFee
        }
        if requestId != nil{
            dictionary["requestId"] = requestId
        }
        if screenName != nil{
            dictionary["screenName"] = screenName
        }
        if serviceImage != nil{
            dictionary["serviceImage"] = serviceImage
        }
        if status != nil{
            dictionary["status"] = status
        }
        if companyId != nil{
            dictionary["companyId"] = status
        }
        if categoryId != nil{
            dictionary["categoryId"] = status
        }
        if serviceId != nil{
            dictionary["serviceId"] = serviceId
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        duration = aDecoder.decodeObject(forKey: "duration") as? String
        flatFee = aDecoder.decodeObject(forKey: "flatFee") as? String
        requestId = aDecoder.decodeObject(forKey: "requestId") as? String
        screenName = aDecoder.decodeObject(forKey: "screenName") as? String
        serviceImage = aDecoder.decodeObject(forKey: "serviceImage") as? String
        status = aDecoder.decodeObject(forKey: "status") as? Int
        categoryId = aDecoder.decodeObject(forKey: "categoryId") as? String
        companyId = aDecoder.decodeObject(forKey: "companyId") as? String
        serviceId = aDecoder.decodeObject(forKey: "serviceId") as? String
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if duration != nil{
            aCoder.encode(duration, forKey: "duration")
        }
        if flatFee != nil{
            aCoder.encode(flatFee, forKey: "flatFee")
        }
        if requestId != nil{
            aCoder.encode(requestId, forKey: "requestId")
        }
        if screenName != nil{
            aCoder.encode(screenName, forKey: "screenName")
        }
        if serviceImage != nil{
            aCoder.encode(serviceImage, forKey: "serviceImage")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        if companyId != nil{
            aCoder.encode(screenName, forKey: "companyId")
        }
        if categoryId != nil{
            aCoder.encode(serviceImage, forKey: "categoryId")
        }
        if serviceId != nil{
            aCoder.encode(serviceId, forKey: "serviceId")
        }
    }
    
}
