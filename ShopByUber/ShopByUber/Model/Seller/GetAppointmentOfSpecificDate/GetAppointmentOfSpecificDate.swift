//
//  GetAppointmentOfSpecificDate.swift
//  The EZ List
//
//  Created by DIFFERENZ101 on 12/18/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class GetAppointmentOfSpecificDate: NSObject {

    var requestId : String?
    var bookByUserId : String?
    var serviceId : String?
    var timeSlotId : String?
    var userName : String?
    var screenName : String?
    var serviceDetail : String?
    var status : Int?
    var flatFee : String?
    var userImage : String?
    var duration : String?
    var address : String?
    
    init(dict:[String:Any]) {
        super.init()
        
        requestId = dict["requestId"] as? String ?? ""
        bookByUserId = dict["bookByUserId"] as? String ?? ""
        serviceId = dict["serviceId"] as? String ?? ""
        timeSlotId = dict["timeSlotId"] as? String ?? ""
        serviceDetail = dict["serviceDetail"] as? String ?? ""
        screenName = dict["screenName"] as? String ?? ""
        status = dict["status"] as? Int
        flatFee = dict["flatFee"] as? String ?? ""
        userImage = dict["userImage"] as? String ?? ""
        duration = dict["duration"] as? String ?? ""
        userName = dict["userName"] as? String ?? ""
        address = dict["address"] as? String ?? ""
        
    }
    
    
    
}
