//
//  User.swift
//  ShopByUber
//
//  Created by Administrator on 5/30/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class User: NSObject {
    
    //MARK: Register API Call
    
    class func callRegisterUserAPI(firstName: String?, lastName: String?, ezListScreenName: String?,email: String?,mobileNumber: String?, countryCode: String? ,password:String?,userType:Int, registerFrom:String?, success: @escaping ([String:Any]?) -> (), failure: @escaping () -> ()) {
        
        KVNProgress.show()
        
        let dict = getRequestParamsForUserRegister(firstName: firstName, lastName: lastName, ScreenName: ezListScreenName, email: email, mobileNumber: mobileNumber, countryCode: countryCode, password: password, userType: userType, registerFrom: registerFrom, quickBloxId: 0)
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kRegisterUser, inputParams: dict, success: { (dict) in
            
            KVNProgress.dismiss()
            
            if dict?["Result"] as? Bool == true {
                
                let data = dict?["Data"] as? [String:Any]
                success(data)
            }
                
            else {
                
                let msg = dict?["Message"] as? String
                Utilities.showAlertView(title: "", message: msg)
                failure()
                
            }
            
        }) { error in
            
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
            failure()
            
        }

    }
    
    class func callRegisterBuyerAPI(firstName: String?, lastName: String?, ezListScreenName: String?,email: String?,mobileNumber: String?, countryCode: String? ,password:String?,userType:Int, address: String?, city: String?, state:String?, country: String?, latitude: Double?, longitude: Double?,registerFrom: String?,quickBloxId: Int, success: @escaping ([String:Any]?) -> (), failure: @escaping () -> ()) {
        
        KVNProgress.show()
        
        let dict = getRequestParamsForUserRegister(firstName: firstName, lastName: lastName, ScreenName: ezListScreenName, email: email, mobileNumber: mobileNumber, countryCode: countryCode, password: password, userType: userType, address: address, city: city, state: state, country: country, latitude: latitude, longitude: longitude, registerFrom: registerFrom, quickBloxId: quickBloxId)
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kRegisterUser, inputParams: dict, success: { (dict) in
            
            KVNProgress.dismiss()
            
            if dict?["Result"] as? Bool == true {
                
                let data = dict?["Data"] as? [String:Any]
                success(data)
            }
                
            else {
                
                let msg = dict?["Message"] as? String
                Utilities.showAlertView(title: "", message: msg)
                failure()
                
            }
            
        }) { error in
            
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
            failure()
            
        }
        
    }
    
    private class func getRequestParamsForUserRegister(firstName: String?, lastName: String?, ScreenName: String?,email: String?,mobileNumber: String?, countryCode: String?,password:String?,userType:Int, address: String? = nil, city:String? = nil, state:String? = nil, country:String? = nil, latitude: Double? = nil, longitude:Double? = nil, registerFrom:String?, quickBloxId:Int) -> [String : Any]  {
        
        var dict = [String : Any]()
        
        dict["firstName"] = firstName
        dict["lastName"] = lastName
        dict["ezListScreenName"] = ScreenName
        dict["mobilePhone"] = countryCode! + mobileNumber!
        dict["countryCode"] = countryCode!
        dict["phoneNumber"] = mobileNumber!
        dict["facebookId"] = ""
        dict["twitterId"] = ""
        dict["email"]     = email
        dict["password"] = password
        dict["userRole"] = userType
        dict["timeZone"] = DataModel.sharedInstance.getCurrentTimeZone()
        dict["deviceType"] = Constant.deviceType
        dict["deviceToken"] = DataModel.sharedInstance.onFetchDeviceToken()
        dict["registerFrom"] = "ios"
        dict["quickBloxId"] = 0
        
        if address != nil {
            dict["address"] = address
            dict["latitude"] = latitude
            dict["longitude"] = longitude
            dict["city"] = city
            dict["state"] = state
            dict["country"] = country
        }
        return dict
    }

    class func isValidUserData(userType:Int,firstName: String?, lastName: String?,email: String?,mobileNumber: String?,password:String?,confirmPassword:String?)  -> Bool {
        
        let firstName = firstName?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let lastName = lastName?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let email = email?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let mobileNumber = mobileNumber?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let password = password?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let confirmPassword = confirmPassword?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        var message = ""
        
        if firstName == "" {
            
            message = userType == 0 ? "Please enter your first name" : "Please enter the owners name"
            
        } else if lastName == "" {
            
           message = userType == 0 ? "Please enter your last name" : "Please enter your business name"
            
        } else if mobileNumber == ""{
            
            message =  Constant.ValidationMessage.kMobileNumber
            
        } else if mobileNumber?.characters.count != 10{
            
            message =  Constant.ValidationMessage.kValidMobileNumber
            
        }else if email == "" {
            
            message = "Please enter your email address"
            
        } else if !Utilities.isValidEmail(email!) {
            
            message = "Please enter a valid email address"
            
        } else if password == ""{
            
            message = "Please provide a valid password"
            
        }else if confirmPassword == ""{
            
            message = Constant.ValidationMessage.kEnterConfirmPassword
            
        }else if password != confirmPassword{
            
            message = Constant.ValidationMessage.kConfirmPassword
        
        }
        else {
            
            return true
        }
        
        Utilities.showAlertView(title: "Alert", message: message)
        return false
    }
    
    
    //MARK: Login API Call
    
    class func isValidLoginData(email: String?,password:String?)  -> Bool {
        
        let email = email?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let password = password?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        
        var message = ""
        if email == "" {
            
            message = "Please enter your email address"
            
        } else if !Utilities.isValidEmail(email!) {
            
            message = "Please enter a valid email address"
            
        } else if password == ""{
            
            message = "Please provide a valid password"
            
        }
            
        else {
            
            return true
        }
        
        Utilities.showAlertView(title: "Alert", message: message)
        return false
    }
    
    class func callLoginAPI(email: String?,password:String?, success: @escaping ([String:Any]?) -> (), failure: @escaping () -> ()) {
        
        KVNProgress.show()
        
        let dict = getRequestParamsForLogin(email: email, password: password)
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kLogin, inputParams: dict, success: { (dict) in
            
            KVNProgress.dismiss()
            
            if dict?["Result"] as? Bool == true {
                
                let data = dict?["Data"] as? [String:Any]
                success(data)
            }
                
            else {
                
                let msg = dict?["Message"] as? String
                Utilities.showAlertView(title: "", message: msg)
                failure()
                
            }
            
        }) { error in
            
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
            failure()
        }
        
    }
    
    private class func getRequestParamsForLogin(email: String?,password:String?) -> [String : Any]  {
        
        var dict = [String : Any]()
        
        dict["email"] = email
        dict["password"] = password
        dict["deviceType"] = Constant.deviceType
        dict["deviceToken"] = Singleton.sharedManager.devieToken
        dict["timeZone"] = DataModel.sharedInstance.getCurrentTimeZone()
        return dict
    }
    
    //MARK: Send Verification Code API
    
    class func callSendVerificationCodeAPI(userId: String?,mobileNumber:String?, success: @escaping ([String:Any]?) -> (), failure: @escaping () -> ()) {
        
        KVNProgress.show()
        
        let dict = getRequestParamsForSendCode(userId: userId, mobileNumber: mobileNumber)
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kSendCode, inputParams: dict, success: { (dict) in
            
            KVNProgress.dismiss()
            
            if dict?["Result"] as? Bool == true {
                
                
                success(dict)
            }
                
            else {
                
                let msg = dict?["Message"] as? String
                Utilities.showAlertView(title: "", message: msg)
                failure()
                
            }
            
        }) { error in
            
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
            failure()
        }
        
    }
    
    private class func getRequestParamsForSendCode(userId: String?,mobileNumber:String?) -> [String : Any]  {
        
        var dict = [String : Any]()
        
        dict["userId"] = userId
        dict["mobilePhone"] = mobileNumber!
       
        return dict
    }
    
    //MARK: Verify Code API
    
    class func callVerifyCodeAPI(userId: String?,mobileNumber:String?,code:String?, success: @escaping ([String:Any]?) -> (), failure: @escaping () -> ()) {
        
        KVNProgress.show()
        
        let dict = getRequestParamsForVerifyCode(userId: userId, mobileNumber: mobileNumber, code: code)
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kVerifyCode, inputParams: dict, success: { (dict) in
            
            KVNProgress.dismiss()
            
            if dict?["Result"] as? Bool == true {
                
                let data = dict?["Data"] as? [String:Any]
                success(data)
            }
                
            else {
                
                let msg = dict?["Message"] as? String
                Utilities.showAlertView(title: "", message: msg)
                failure()
                
            }
            
        }) { error in
            
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
            failure()
            
        }
        
    }
    
    private class func getRequestParamsForVerifyCode(userId: String?,mobileNumber:String?,code:String?) -> [String : Any]  {
        
        var dict = [String : Any]()
        
        dict["userId"] = userId
        dict["mobilePhone"] = mobileNumber
        dict["verifyCode"] = code
        dict["deviceType"] = Constant.deviceType
        dict["deviceToken"] = DataModel.sharedInstance.onFetchDeviceToken()
        return dict
    }
    
    //MARK: LogIn with Facebook/Twitter API
    class func callFacebookTwitterAPI(firstName: String?, lastName: String?, ezListScreenName: String?, mobilePhone:String?,facebookId:String?, twitterId:String?, email:String?, password:String?, address: String?, latitude:Double?, longitude:Double?, city: String?, state: String?, country: String?, userRole:Int, isFacebookSignUp: Bool, success: @escaping ([String:Any]?) -> (), failure: @escaping () -> ()) {
        
        KVNProgress.show()
        
        let dict = getRequestParamsForFacebookTwitter(firstName: firstName, lastName: lastName, ScreenName: ezListScreenName, mobilePhone: mobilePhone, facebookId: facebookId, twitterId: twitterId, email: email, password: password, userRole: userRole, address: address, city: city, state: state, country: country, latitude: latitude, longitude: longitude)
        
        var url = ""
        if isFacebookSignUp
        {
            url = Constant.serverAPI.URL.kConnectWithFacebook
        }else
        {
            url = Constant.serverAPI.URL.kConnectWithTwitter
        }
        
        APIManager.callAPI(url: url, inputParams: dict, success: { (dict) in
            
            KVNProgress.dismiss()
            
            if dict?["Result"] as? Bool == true {
                
                let data = dict?["Data"] as? [String:Any]
                success(data)
            }
                
            else {
                
                let msg = dict?["Message"] as? String
                Utilities.showAlertView(title: "", message: msg)
                failure()
                
            }
            
            
        }) { error in
            
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
            failure()
            
        }
        
    }
    
    private class func getRequestParamsForFacebookTwitter(firstName: String?, lastName: String?, ScreenName: String?, mobilePhone:String?,facebookId:String?, twitterId:String?, email:String?, password:String?, userRole:Int, address: String?, city: String?, state:String?, country:String?, latitude:Double?, longitude:Double?) -> [String : Any]  {
        
        var dict = [String : Any]()
        dict["firstName"] = firstName
        dict["lastName"] = lastName
        dict["ezListScreenName"] = ScreenName
        dict["mobilePhone"] = mobilePhone!
        dict["facebookId"] = facebookId
        dict["twitterId"] = twitterId
        dict["email"] = email
        dict["password"] = password
        dict["userRole"] = userRole
        dict["timeZone"] = DataModel.sharedInstance.getCurrentTimeZone()
        dict["deviceType"] = Constant.deviceType
        dict["deviceToken"] = DataModel.sharedInstance.onFetchDeviceToken()
        dict["address"] = address
        dict["latitude"] = latitude
        dict["longitude"] = longitude
        dict["city"] = city
        dict["state"] = state
        dict["country"] = country
        return dict
    }
    
    //MARK: Forgot Password
    
    class func callSendCodeForResetCodeAPI(email: String?, success: @escaping ([String:Any]?) -> (), failure: @escaping () -> ()) {
        
        KVNProgress.show()
        
        var dict = [String:Any]()
        dict["email"] = email
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kRecoverPassword, inputParams: dict, success: { (dict) in
            
            KVNProgress.dismiss()
            
            if dict?["Result"] as? Bool == true {
                
                let data = dict?["Data"] as? [String:Any]
                success(data)
            }
                
            else {
                
                let msg = dict?["Message"] as? String
                Utilities.showAlertView(title: "", message: msg)
                failure()
                
            }
            
            
        }) { error in
            
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
            failure()
            
        }
        
    }
    
    class func callVerifyCodeForgotAPI(code: String?, success: @escaping ([String:Any]?) -> (), failure: @escaping () -> ()) {
        
        KVNProgress.show()
        
        var dict = [String:Any]()
        dict["userId"] = DataModel.sharedInstance.onFetchUserId()
        dict["verifyCode"] = code
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kRecoverPasswordVerifyCode, inputParams: dict, success: { (dict) in
            
            KVNProgress.dismiss()
            
            if dict?["Result"] as? Bool == true {
                
                let data = dict?["Data"] as? [String:Any]
                success(data)
            }
                
            else {
                
                let msg = dict?["Message"] as? String
                Utilities.showAlertView(title: "", message: msg)
                failure()
                
            }
            
            
        }) { error in
            
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
            failure()
            
        }
        
    }
   
    class func callResetPasswordAPI(password: String?, success: @escaping ([String:Any]?) -> (), failure: @escaping () -> ()) {
        
        var dict = [String:Any]()
        dict["userId"] = DataModel.sharedInstance.onFetchUserId()
        dict["password"] = password
        dict["deviceType"] = Constant.deviceType
        dict["deviceToken"] = DataModel.sharedInstance.onFetchDeviceToken()
        dict["timeZone"] = DataModel.sharedInstance.getCurrentTimeZone()
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kSetNewPassword, inputParams: dict, success: { (dict) in
            
            KVNProgress.dismiss()
            
            if dict?["Result"] as? Bool == true {
                
                let data = dict?["Data"] as? [String:Any]
                success(data)
            }
                
            else {
                
                let msg = dict?["Message"] as? String
                Utilities.showAlertView(title: "", message: msg)
                failure()
                
            }
            
            
        }) { error in
            
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
            failure()
            
        }
        
    }
    
    class func callLogoutAPI( completion: @escaping () -> ()) {
        
        KVNProgress.show()
        
        var dict = [String:Any]()
        dict["userId"] = DataModel.sharedInstance.onFetchUserId()
        dict["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kLogout, inputParams: dict, success: { (dict) in
            
            KVNProgress.dismiss()
            
            if dict?["Result"] as? Bool == true {
                
                KVNProgress.dismiss()
                completion()
            }
                
            else {
                
                KVNProgress.dismiss()
                let msg = dict?["Message"] as? String
                Utilities.showAlertView(title: "", message: msg)
                completion()
                
            }
            
            
        }) { error in
            
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
            completion()
            
        }
        
    }
    
    
    //MARK: Edit Profile
    
    class func isValidUpdateUserData(firstName: String?,lastName: String?,screenName: String?,email: String?,mobileNumber: String?,password:String?,confirmPassword:String?)  -> Bool {
        
        let firstName = firstName?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let lastNname = lastName?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let screenName = screenName?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let email = email?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let mobileNumber = mobileNumber?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let password = password?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let confirmPassword = confirmPassword?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        var message = ""
        
        if firstName == "" {
            
            message =  Constant.ValidationMessage.kfirstName
            
        }else if lastNname == "" {
            
            message =  Constant.ValidationMessage.kLastName
            
        }else if screenName == "" {
            
            message =  Constant.ValidationMessage.kEZScreenName
            
        }
        else if (mobileNumber?.characters.count)! < 12 && (mobileNumber?.characters.count)! > 13{
            
            message =  Constant.ValidationMessage.kMobileNumberCountryCode
            
        }else if email == "" {
            
            message = Constant.ValidationMessage.kEmail
            
        } else if !Utilities.isValidEmail(email!) {
            
            message = Constant.ValidationMessage.kValidEmail
            
        } else if password != confirmPassword{
            
            message = Constant.ValidationMessage.kConfirmPassword
            
        }
            
        else {
            
            return true
        }
        
        Utilities.showAlertView(title: "Alert", message: message)
        return false
    }
    
    class func callUpdateUserDataAPI(imageName: String?,name: String?, firstName: String?,lastName: String?,ezListScreenName:String?,mobile:String? = "",countryCode:String? = "",email:String?,password:String?,sellerDescription:String?, success: @escaping ([String:Any]?) -> (), failure: @escaping () -> ()) {
        
        var dict = [String:Any]()
        
        
        dict["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
        dict["userId"] = DataModel.sharedInstance.onFetchUserId()
        dict["firstName"] = firstName?.trimmingCharacters(in: .whitespaces)
        dict["lastName"] = lastName?.trimmingCharacters(in: .whitespaces)
        dict["ezListScreenName"] = ezListScreenName?.trimmingCharacters(in: .whitespaces)
        dict["mobilePhone"] = mobile!
        dict["countryCode"] = countryCode!
        dict["phoneNumber"] = mobile!
        dict["password"]    = password?.trimmingCharacters(in: .whitespaces)
        dict["description"] = sellerDescription?.trimmingCharacters(in: .whitespaces)
        
        if imageName?.count != 0 {
            dict["profileImage"] = imageName
        }
    
        if imageName == ""{
        
            KVNProgress.show()
        }
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kUpdateMyProfile, inputParams: dict, success: { (dict) in
            
            KVNProgress.dismiss()
            
            if dict?["Result"] as? Bool == true {
                
                let data = dict?["Data"] as? [String:Any]
                success(data)
                
            }
                
            else {
                
                let msg = dict?["Message"] as? String
                Utilities.showAlertView(title: "", message: msg)
                failure()
                
            }
            
        }) { error in
            
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
            failure()
            
        }
    }
    class func callGetUSerProfileDataAPI(success: @escaping ([String:Any]?) -> (), failure: @escaping () -> ()) {
        
        var dict = [String:Any]()
        
        
        dict["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
        dict["userId"] = DataModel.sharedInstance.onFetchUserId()
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kgetUserProfileData, inputParams: dict, success: { (dict) in
            
            KVNProgress.dismiss()
            
            if dict?["Result"] as? Bool == true {
                
                let data = dict?["Data"] as? [String:Any]
                success(data)
                
            }
                
            else {
                
                let msg = dict?["Message"] as? String
                Utilities.showAlertView(title: "", message: msg)
                failure()
                
            }
            
        }) { error in
            
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
            failure()
            
        }
    }
    
    class func callUpdateQuickbloxID(quickBloxId: UInt) {
        
        var dict = [String:Any]()
        dict["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
        dict["userId"] = DataModel.sharedInstance.onFetchUserId()
        dict["quickBloxId"] = quickBloxId
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kupdateQuickBloxId, inputParams: dict, success: { (dict) in
            print(dict?["Result"])
        }) { error in
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
        }
    }
}
