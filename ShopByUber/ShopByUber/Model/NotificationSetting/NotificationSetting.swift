//
//	NotificationSetting.swift
//
//	Create by Chirag Kalsariya on 14/7/2017
//	Copyright © 2017. All rights reserved.

import Foundation

class NotificationSetting : NSObject{
    
    var isEnabled : Bool!
    var notifyId : Int!
    var notifyLabel : String!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        isEnabled = dictionary["isEnabled"] as? Bool ?? false
        notifyId = dictionary["notifyId"] as? Int ?? 0
        notifyLabel = dictionary["notifyLabel"] as? String ?? ""
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if isEnabled != nil{
            dictionary["isEnabled"] = isEnabled
        }
        if notifyId != nil{
            dictionary["notifyId"] = notifyId
        }
        if notifyLabel != nil{
            dictionary["notifyLabel"] = notifyLabel
        }
        return dictionary
    }
    
    class func getSetting(success:@escaping([NotificationSetting])->(),failure:(@escaping()->()))
    {

        KVNProgress.show()
        var dictParam=[String:Any]()
        dictParam["userId"] = DataModel.sharedInstance.onFetchUserId()
        dictParam["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
        APIManager.callAPI(url: Constant.serverAPI.URL.kGetNotificationData, inputParams: dictParam, success: { (response) in
            
            KVNProgress.dismiss()
            if response?["Result"] as? Bool == true {
                let dict = response?["Data"] as? [String:Any]
                var data = dict?["basicNotification"] as! NSArray
                
                var arrNotification = [NotificationSetting]()
            
                data.enumerateObjects({ (obj, idx, nil) in
                    arrNotification.append(NotificationSetting(fromDictionary: obj as! [String:Any]))
                })
                
                data = dict?["otherNotification"] as! NSArray
                data.enumerateObjects({ (obj, idx, nil) in
                    arrNotification.append(NotificationSetting(fromDictionary: obj as! [String:Any]))
                })
                
                success(arrNotification)
            }
            else {
                _ = response?["Message"] as? String
                
                failure()
            }
            
        }) { (error) in
            
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
            failure()
        }

    }
    
    class func setSetting(notificationID:Int,isEnable:Bool,completion: @escaping (Bool) -> ())
    {
        
        KVNProgress.show()
        var dictParam=[String:Any]()
        dictParam["userId"] = DataModel.sharedInstance.onFetchUserId()
        dictParam["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
        dictParam["notifyId"] = notificationID
        dictParam["isEnabled"] = isEnable
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kSetNotificationData, inputParams: dictParam, success: { (response) in
            
            print(response?["Message"] as? String ?? "")
            
            KVNProgress.dismiss()
            if response?["Result"] as? Bool == true {
                
                completion(true)
            }
            else {
                
                completion(false)
                let msg = response?["Message"] as? String
                Utilities.showAlertView(title: "", message: msg)
            }
            
        }) { (error) in
            
            completion(false)
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
            
        }
    }
}
