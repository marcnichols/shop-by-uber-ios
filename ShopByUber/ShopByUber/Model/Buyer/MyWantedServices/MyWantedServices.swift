//
//  MyWantedServices.swift
//  The EZ List
//
//  Created by mac on 4/13/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

import Foundation

class MyWantedServices : NSObject{
    
    var catName : String!
    var categoryId : String!
    var serviceImage : String!
    var requestId : String!
    var serviceDescription : String!
    var serviceName : String!
    var userId : String!
    var isServiceCreated : Bool!

    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        serviceName = dictionary["serviceName"] as? String ?? ""
        serviceDescription = dictionary["serviceDescription"] as? String ?? ""
        categoryId = dictionary["categoryId"] as? String ?? ""
        catName = dictionary["catName"] as? String ?? ""
        serviceImage = dictionary["image"] as? String ?? ""
        requestId = dictionary["requestId"] as? String ?? ""
        userId = dictionary["userId"] as? String ?? ""
        isServiceCreated = dictionary["isServiceCreated"] as? Bool ?? false
    }
    
    
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if serviceName != nil{
            dictionary["serviceName"] = serviceName
        }
        if serviceDescription != nil{
            dictionary["serviceDescription"] = serviceDescription
        }
        if categoryId != nil{
            dictionary["categoryId"] = categoryId
        }
        if catName != nil{
            dictionary["catName"] = catName
        }
        if serviceImage != nil{
            dictionary["serviceImage"] = serviceImage
        }
        if requestId != nil {
            dictionary["requestId"] = requestId
        }
        if userId != nil {
            dictionary["userId"] = userId
        }
        if isServiceCreated != nil {
            dictionary["isServiceCreated"] = isServiceCreated
        }
       
        return dictionary
    }
    
    
    //API call for wanted Services.....
    
    class func getMyWantedService(pageNo:Int,getAll:Bool,numOfRecords:Int,isShowloader:Bool,success:@escaping([MyWantedServices],Int,String)->(),failure:@escaping()->())
    {
        if isShowloader
        {
            KVNProgress.show()
        }
        var dictParam=[String:Any]()
        dictParam["userId"] = DataModel.sharedInstance.onFetchUserId()
        dictParam["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
        dictParam["getAll"] = getAll
        dictParam["pageNo"] = pageNo
        dictParam["numOfRecords"] = numOfRecords
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kGetAllWantedList, inputParams: dictParam, success: { (response) in
            
            KVNProgress.dismiss()
            if response?["Result"] as? Bool == true {
                let data = response?["Data"] as? [String:Any]
                let wantedList = data?["wantedList"] as! NSArray
                let recordcnt = data?["recordCount"] as? Int
                let message = response?["Message"] as? String
                
                var arrService = [MyWantedServices]()
                
                wantedList.enumerateObjects({ (obj, idx, nil) in
                    arrService.append(MyWantedServices(fromDictionary: obj as! [String:Any]))
                })
                
                success(arrService,recordcnt!,message!)
            }
            else {
                _ = response?["Message"] as? String
                
                failure()
            }
            
        }) { (error) in
            
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
            failure()
        }
        
    }
    
    class func getWantedDetail(requestID:String,isShowloader:Bool,success:@escaping([String:Any])->())
    {
        if isShowloader
        {
            KVNProgress.show()
        }
        var dictParam=[String:Any]()
        dictParam["userId"] = DataModel.sharedInstance.onFetchUserId()
        dictParam["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
        dictParam["requestId"] = requestID
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kShowWantedDetails, inputParams: dictParam, success: { (response) in
            
            KVNProgress.dismiss()
            if response?["Result"] as? Bool == true {
                let dict = response?["Data"] as? [String:Any]
                
                success(dict!)
                
            }
            else {
                let msg = response?["Message"] as? String
                Utilities.showAlertView(title: "", message: msg)
            }
            
        }) { (error) in
            
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
        }
        
    }

    class func getWantedDetailForAddNewService(requestID:String,isShowloader:Bool,success:@escaping([String:Any])->())
    {
        if isShowloader
        {
            KVNProgress.show()
        }
        var dictParam=[String:Any]()
        dictParam["userId"] = DataModel.sharedInstance.onFetchUserId()
        dictParam["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
        dictParam["requestId"] = requestID
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kgetDetail, inputParams: dictParam, success: { (response) in
            
            KVNProgress.dismiss()
            if response?["Result"] as? Bool == true {
                let dict = response?["Data"] as? [String:Any]
                
                success(dict!)
                
            }
            else {
                let msg = response?["Message"] as? String
                Utilities.showAlertView(title: "", message: msg)
            }
            
        }) { (error) in
            
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
        }
        
    }

    class func cancelWantedService(requestID:String,isShowloader:Bool,success:@escaping([String:Any])->())
    {
        if isShowloader
        {
            KVNProgress.show()
        }
        var dictParam=[String:Any]()
        dictParam["userId"] = DataModel.sharedInstance.onFetchUserId()
        dictParam["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
        dictParam["requestId"] = requestID
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kCancelWantedService, inputParams: dictParam, success: { (response) in
            
            KVNProgress.dismiss()
//            if response?["Result"] as? Bool == true {
//                let msg = response?["Message"] as? String
//                Utilities.showAlertView(title: "", message: msg)
//            }
//            else {
//                let msg = response?["Message"] as? String
//                Utilities.showAlertView(title: "", message: msg)
//            }
            success(response!)
        }) { (error) in
            
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
        }
        
    }
    
    class func updateWantedService(requestID:String, serviceName: String, categoryId: String, subCategoryId:String, serviceDescription:String, isShowloader:Bool,success:@escaping([String:Any])->())
    {
        if isShowloader
        {
            KVNProgress.show()
        }
        var dictParam=[String:Any]()
        dictParam["userId"] = DataModel.sharedInstance.onFetchUserId()
        dictParam["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
        dictParam["requestId"] = requestID
        dictParam["serviceName"] = serviceName
        dictParam["categoryId"] = categoryId
        dictParam["subCategoryId"] = subCategoryId
        dictParam["serviceDescription"] = serviceDescription
        
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kUpdatewantedService, inputParams: dictParam, success: { (response) in
            
            KVNProgress.dismiss()

            success(response!)
        }) { (error) in
            
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
        }
        
    }
    
}
