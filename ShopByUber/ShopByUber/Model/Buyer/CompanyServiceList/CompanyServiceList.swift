//
//  ServiceListModel.swift
//  ShopByUber
//
//  Created by Differenz215 on 6/23/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class CompanyServiceList: NSObject {
    
    var companyId: String!
    var isFavourite:Bool!
    var comapnyScreenName:String!
    var companyProfileImage: String!
    var companyServiceDesc:String!
    var companyDescription:String?
    var isAvailable:Bool!
    var latitude: Double!
    var longitude: Double!
    var zipcode: String!
    var categoryId: String!
    var subCatagoryID: String!
    var serviceDescription: String!
    var profileImage: String!
    var screenName: String!
    var companyLocation: String!
    
    
    init(dict:[String:Any]) {
        super.init()
        
        companyId            = dict["companyId"] as? String ?? ""
        isFavourite          = dict["isFavourite"] as? Bool ?? false
        comapnyScreenName    = dict["comapnyScreenName"] as? String ?? ""
        companyProfileImage  = dict["companyProfileImage"] as? String ?? ""
        companyServiceDesc   = dict["catName"] as? String ?? ""
        companyDescription   = dict["companyDescription"] as? String ?? ""
        isAvailable          = dict["isAvailable"] as? Bool ?? false
        latitude             = dict["latitude"] as? Double ?? 0.0
        longitude            = dict["longitude"] as? Double ?? 0.0
        zipcode              = dict["zipcode"] as? String ?? ""
        categoryId           = dict["categoryId"] as? String ?? ""
        subCatagoryID        = dict["subCategoryId"] as? String ?? ""
        serviceDescription   = dict["serviceDescription"] as? String ?? ""
        profileImage         = dict["profileImage"] as? String ?? ""
        screenName           = dict["screenName"] as? String ?? ""
        companyLocation      = dict["companyAddress"] as? String ?? ""
    }
}

class CompanyService: NSObject {
    
    var categoryId      : String!
    var subCategoryId   : String!
    var companyId       : String!
    var serviceId       : String!
    var isFavourite     : Bool!
    var serviceDetail   : String!
    var serviceImage    : String!
    var screenName      : String!
    var flatFee         : String!
    var serviceAvailable: Bool!
    var companyAddress  : String!
    
    var bookNowDone : Bool!
    var availableTimeSlot = [TimeSlot]()
    var arrServiveTimeSlot = NSMutableArray()
    
    
    init(dict:[String:Any]) {
        super.init()
        
        categoryId           = dict["categoryId"] as? String ?? ""
        subCategoryId        = dict["subCategoryId"] as? String ?? ""
        companyId            = dict["companyId"] as? String ?? ""
        serviceId            = dict["serviceId"]  as? String ?? ""
        isFavourite          = dict["isFavourite"]as? Bool ?? false
        serviceDetail        = dict["serviceDetail"] as? String ?? ""
        serviceImage         = dict["serviceImage"] as? String ?? ""
        screenName           = dict["screenName"] as? String ?? ""
        flatFee              = dict["flatFee"] as? String ?? ""
        serviceAvailable     = dict["serviceAvailable"] as? Bool ?? false
        bookNowDone          = dict["bookNowDone"] as? Bool ?? false
        companyAddress       = dict["companyAddress"] as? String ?? ""
        
        let TimeSlots        = dict["availableTimeSlot"] as? NSArray
        if TimeSlots != nil {
            for element in TimeSlots! {
                
                let dict = element as? NSDictionary
                let objTimeSlotDetail : TimeSlot = TimeSlot.init(dict: dict! as! [String : Any])
                availableTimeSlot.append(objTimeSlotDetail)
                
            }
        }
    }
    
    class func callMyFavouritesAPI(pageNo: Int?,numOfRecords:Int?, success: @escaping ([CompanyService],Int) -> (), failure: @escaping () -> ()) {
        
        
        let dict = getRequestParamsForGetCategories(pageNo: pageNo, numOfRecords: numOfRecords)
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kGetFavouriteServices, inputParams: dict, success: { (dict) in
            
            if dict?["Result"] as? Bool == true {
                
                let data = dict?["Data"] as? [String:Any]
                let categories = data?["services"] as? [[String:Any]]
                let recordcnt = data?["recordCount"] as? Int
                var arrCategory = [CompanyService]()
                for category in categories!{
                    
                    let objCategory = CompanyService.init(dict: category)
                    arrCategory.append(objCategory)
                    
                }
                
                success(arrCategory,recordcnt!)
            }
                
            else {

                let msg = dict?["Message"] as? String
                Utilities.showAlertView(title: "", message: msg)
                failure()
            }
            
        }) { error in
            
            Utilities.showAlertView(title: "", message: error)
            failure()
        }
        
        
    }
    
    private class func getRequestParamsForGetCategories(pageNo: Int?,numOfRecords:Int?) -> [String : Any]  {
        
        
        var dict = [String : Any]()
        
        dict["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
        dict["userId"] = DataModel.sharedInstance.onFetchUserId()
        dict["pageNo"] = pageNo
        dict["numOfRecords"] = numOfRecords
        
        return dict
    }

    
}

class TimeSlot: NSObject{
    var timeslotId: String?
    var duration:String?
   
    
    
    init(dict:[String:Any]) {
        super.init()
        
        timeslotId            = dict["timeslotId"] as? String
        duration              = dict["duration"]as? String
        
    }

}
