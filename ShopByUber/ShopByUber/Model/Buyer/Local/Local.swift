//
//  Local.swift
//  The EZ List
//
//  Created by DIFFERENZ101 on 2/8/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

import UIKit

class Local: NSObject {
    
    var id: String?
    var companyName:String?
    var compDescription: String?
    var comapnyScreenName: String?
    var imageURL:String?
    var categories : String?
    var latitude : Double?
    var longitude : Double?
    
    override init() {
        
    }

    init(dict:[String:Any]) {
        super.init()
        
        id = dict["_id"] as? String
        companyName = dict["companyName"] as? String
        comapnyScreenName = dict["comapnyScreenName"] as? String
        compDescription = dict["serviceDescription"] as? String
        imageURL = dict["companyProfileImage"] as? String
        categories = dict["categories"] as? String
        latitude = dict["latitude"] as? Double
        longitude = dict["longitude"] as? Double
    }
    
    class func callGetLocalAPI(pageNo: Int?,numOfRecords:Int?, success: @escaping ([Local],Int) -> (), failure: @escaping (String) -> ()) {
        
        let dict = getRequestParamsForLocal(pageNo: pageNo, numOfRecords: numOfRecords)
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kgetLocalVendors, inputParams: dict, success: { (dict) in
            
            if dict?["Result"] as? Bool == true {
                
                let data = dict?["Data"] as? [String:Any]
                let companyListing = data?["companyListing"] as? [[String:Any]]
                let recordcnt = data?["recordCount"] as? Int
                var arrPiks = [Local]()
                for company in companyListing!{
                    
                    let objPicks = Local.init(dict: company)
                    arrPiks.append(objPicks)
                    
                }
                success(arrPiks,recordcnt!)
            }
                
            else {
                
                let msg = dict?["Message"] as? String
                Utilities.showAlertView(title: "", message: msg)
                failure(msg!)
                
            }
            
        }) { error in
            let msg = "At the moment there are no vendors available in your local area! Please check back daily!"
            Utilities.showAlertView(title: "", message: msg)
            failure(msg)
        }
    }
    
    private class func getRequestParamsForLocal(pageNo: Int?,numOfRecords:Int?) -> [String : Any]  {
        
        var dict = [String : Any]()
        
        dict["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
        dict["userId"] = DataModel.sharedInstance.onFetchUserId()
        dict["pageNo"] = pageNo
        dict["numOfRecords"] = numOfRecords
        dict["latitude"] = DataModel.sharedInstance.onFetchLatitude()
        //  dict["latitude"] = 10.523654
        print(DataModel.sharedInstance.onFetchLatitude())
        dict["longitude"] = DataModel.sharedInstance.onFetchLongtitude()
        // dict["longitude"] = 12.326585
        print(DataModel.sharedInstance.onFetchLongtitude())
        
        return dict
    }
}
