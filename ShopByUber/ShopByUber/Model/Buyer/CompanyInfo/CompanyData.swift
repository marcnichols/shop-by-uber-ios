//
//  CompanyData.swift
//  ShopByUber
//
//  Created by Administrator on 6/12/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class CompanyData: NSObject {
    
    var companyName:String?
    var comapnyScreenName: String?
    var serviceDetail:String?
    var companyProfile:String?
    var companyID:String?
    var latitude: Double?
    var longitude:Double?
    
    init(local: Local) {
        companyName    = local.companyName
        comapnyScreenName = local.comapnyScreenName
        serviceDetail  = local.compDescription
        companyProfile = local.imageURL
        latitude       = local.latitude
        longitude      = local.longitude
        companyID      = local.id
    }
    
    init(dict:[String:Any]) {
        super.init()
        
        companyName    = dict["companyName"] as? String
        comapnyScreenName = dict["comapnyScreenName"] as? String
        serviceDetail  = dict["serviceDescription"] as? String
        companyProfile = dict["companyProfileImage"] as? String
        latitude       = dict["latitude"] as? Double
        longitude      = dict["longitude"] as? Double
        companyID      = dict["companyId"]as? String
        
    }
    
    
}
