//
//  Companies.swift
//  The EZ List
//
//  Created by DIFFERENZ101 on 1/31/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

import UIKit

class Companies: NSObject {

    var id: String?
    var companyName:String?
    var compDescription: String?
    var comapnyScreenName: String?
    var imageURL:String?
    var categories : String?
    
    override init() {
        
    }
    
    init(dict:[String:Any]) {
        super.init()
        
        id = dict["_id"] as? String
        companyName = dict["companyName"] as? String
        comapnyScreenName = dict["comapnyScreenName"] as? String
        compDescription = dict["serviceDescription"] as? String
        imageURL = dict["companyProfileImage"] as? String
        categories = dict["categories"] as? String
    }
}
