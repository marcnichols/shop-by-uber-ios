//
//  UberPicks.swift
//  ShopByUber
//
//  Created by Administrator on 6/6/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class UberPicks: NSObject {
    
    var id: String?
    var companyName:String?
    var compDescription: String?
    var comapnyScreenName: String?
    var imageURL:String?
    var categories : String?
    
    override init() {
        
    }
    
    init(dict:[String:Any]) {
        super.init()
        
        id = dict["_id"] as? String
        companyName = dict["companyName"] as? String
        comapnyScreenName = dict["comapnyScreenName"] as? String
        compDescription = dict["serviceDescription"] as? String
        imageURL = dict["companyProfileImage"] as? String
        categories = dict["categories"] as? String
    }
    
    class func callGetUberPicksAPI(pageNo: Int?,numOfRecords:Int?, success: @escaping ([UberPicks],Int) -> (), failure: @escaping () -> ()) {
        
        let dict = getRequestParamsForGetPicks(pageNo: pageNo, numOfRecords: numOfRecords)
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kGetShopbyUberPicks, inputParams: dict, success: { (dict) in
            
            if dict?["Result"] as? Bool == true {
                
                let data = dict?["Data"] as? [String:Any]
                let companyListing = data?["companyListing"] as? [[String:Any]]
                let recordcnt = data?["recordCount"] as? Int
                var arrPiks = [UberPicks]()
                for company in companyListing!{
                    
                    let objPicks = UberPicks.init(dict: company)
                    arrPiks.append(objPicks)
                    
                }
                success(arrPiks,recordcnt!)
            }
                
            else {
                
//                let data = dict?["Data"] as? [String:Any]
//                let count = data?["recordCount"] as? Int
//                if count == 0 {
//                    let msg = dict?["Message"] as? String
//                    Utilities.showAlertView(title: "", message: msg)
//                }
                failure()
                
            }
            
        }) { error in
            
            Utilities.showAlertView(title: "", message: error)
            failure()
        }
        
    }
    
    private class func getRequestParamsForGetPicks(pageNo: Int?,numOfRecords:Int?) -> [String : Any]  {
        
        var dict = [String : Any]()
        
        dict["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
        dict["userId"] = DataModel.sharedInstance.onFetchUserId()
        dict["pageNo"] = pageNo
        dict["numOfRecords"] = numOfRecords
        dict["latitude"] = DataModel.sharedInstance.onFetchLatitude()
        dict["longitude"] = DataModel.sharedInstance.onFetchLongtitude()
        
        
        return dict
    }
    
    
    
    
    
    class func callGetWantedServiceListAPI(requestID: String?, success: @escaping ([UberPicks],Int) -> (), failure: @escaping () -> ()) {
        
        let dict = getRequestParamsForGetWantedList(requestID: requestID)
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kGetwantedServiceDetailList, inputParams: dict, success: { (dict) in
            
            if dict?["Result"] as? Bool == true {
                
                let data = dict?["Data"] as? [String:Any]
                let companyListing = data?["companyListing"] as? [[String:Any]]
                let recordcnt = data?["recordCount"] as? Int
                var arrPiks = [UberPicks]()
                for company in companyListing!{
                    
                    let objPicks = UberPicks.init(dict: company)
                    arrPiks.append(objPicks)
                    
                }
                success(arrPiks,recordcnt!)
            }
                
            else {
                
                //                let data = dict?["Data"] as? [String:Any]
                //                let count = data?["recordCount"] as? Int
                //                if count == 0 {
                //                    let msg = dict?["Message"] as? String
                //                    Utilities.showAlertView(title: "", message: msg)
                //                }
                failure()
                
            }
            
        }) { error in
            
            Utilities.showAlertView(title: "", message: error)
            failure()
        }
        
    }
    
    private class func getRequestParamsForGetWantedList(requestID: String?) -> [String : Any]  {
        
        var dict = [String : Any]()
        dict["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
        dict["userId"] = DataModel.sharedInstance.onFetchUserId()
        dict["requestId"] = requestID
        
        return dict
    }

    
}
