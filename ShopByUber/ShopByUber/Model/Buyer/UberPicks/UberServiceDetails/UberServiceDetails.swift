//
//  UberServiceDetails.swift
//  ShopByUber
//
//  Created by Differenz215 on 8/12/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import Foundation

class UberServiceDetails : NSObject, NSCoding{
    
    var availableTimeSlot = [TimeSlot]()
    var catName : String!
    var categoryId : String!
    var flatFee : String!
    var isFavourite : Bool!
    var screenName : String!
    var serviceAvailable : Bool!
    var serviceDetail : String!
    var serviceId : String!
    var serviceImage : String!
    var arrServiceTimeSlot = NSMutableArray()
    var bookNowDone : Bool!
    var companyAddress : String!
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]) {
        
        catName = dictionary["catName"] as? String ?? ""
        categoryId = dictionary["categoryId"] as? String ?? ""
        flatFee = dictionary["flatFee"] as? String ?? ""
        isFavourite = dictionary["isFavourite"] as? Bool ?? false
        screenName = dictionary["screenName"] as? String ?? ""
        serviceAvailable = dictionary["serviceAvailable"] as? Bool ?? false
        serviceDetail = dictionary["serviceDetail"] as? String ?? ""
        serviceId = dictionary["serviceId"] as? String ?? ""
        serviceImage = dictionary["serviceImage"] as? String ?? ""
        bookNowDone = dictionary["bookNowDone"] as? Bool ?? false
        companyAddress = dictionary["companyAddress"] as? String ?? ""
        
        let TimeSlots        = dictionary["availableTimeSlot"] as? NSArray
        if TimeSlots != nil {
            for element in TimeSlots! {
                
                let dict = element as? NSDictionary
                let objTimeSlotDetail : TimeSlot = TimeSlot.init(dict: dict! as! [String : Any])
                availableTimeSlot.append(objTimeSlotDetail)
                
            }
        }
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if catName != nil{
            dictionary["catName"] = catName
        }
        if categoryId != nil{
            dictionary["categoryId"] = categoryId
        }
        if flatFee != nil{
            dictionary["flatFee"] = flatFee
        }
        if isFavourite != nil{
            dictionary["isFavourite"] = isFavourite
        }
        if screenName != nil{
            dictionary["screenName"] = screenName
        }
        if serviceAvailable != nil{
            dictionary["serviceAvailable"] = serviceAvailable
        }
        if serviceDetail != nil{
            dictionary["serviceDetail"] = serviceDetail
        }
        if serviceId != nil{
            dictionary["serviceId"] = serviceId
        }
        if serviceImage != nil{
            dictionary["serviceImage"] = serviceImage
        }
        if companyAddress != nil{
            dictionary["companyAddress"] = companyAddress
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        catName = aDecoder.decodeObject(forKey: "catName") as? String
        categoryId = aDecoder.decodeObject(forKey: "categoryId") as? String
        flatFee = aDecoder.decodeObject(forKey: "flatFee") as? String
        isFavourite = aDecoder.decodeObject(forKey: "isFavourite") as? Bool
        screenName = aDecoder.decodeObject(forKey: "screenName") as? String
        serviceAvailable = aDecoder.decodeObject(forKey: "serviceAvailable") as? Bool
        serviceDetail = aDecoder.decodeObject(forKey: "serviceDetail") as? String
        serviceId = aDecoder.decodeObject(forKey: "serviceId") as? String
        serviceImage = aDecoder.decodeObject(forKey: "serviceImage") as? String
        companyAddress = aDecoder.decodeObject(forKey: "companyAddress") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        
        if catName != nil{
            aCoder.encode(catName, forKey: "catName")
        }
        if categoryId != nil{
            aCoder.encode(categoryId, forKey: "categoryId")
        }
        if flatFee != nil{
            aCoder.encode(flatFee, forKey: "flatFee")
        }
        if isFavourite != nil{
            aCoder.encode(isFavourite, forKey: "isFavourite")
        }
        if screenName != nil{
            aCoder.encode(screenName, forKey: "screenName")
        }
        if serviceAvailable != nil{
            aCoder.encode(serviceAvailable, forKey: "serviceAvailable")
        }
        if serviceDetail != nil{
            aCoder.encode(serviceDetail, forKey: "serviceDetail")
        }
        if serviceId != nil{
            aCoder.encode(serviceId, forKey: "serviceId")
        }
        if serviceImage != nil{
            aCoder.encode(serviceImage, forKey: "serviceImage")
        }
        if companyAddress != nil{
            aCoder.encode(companyAddress, forKey: "companyAddress")
        }
        
    }
    
}
