//
//  Activity.swift
//  ShopByUber
//
//  Created by Differenz215 on 8/21/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import Foundation


class Activity : NSObject, NSCoding{
    
    var message : String!
    var pubChannelName : String!
    var requestId : String!
    var screenName : String!
    var serviceId : String!
    var serviceImage : String!
    var status : Int!
    var userAddress : String!
    var userLatitude : Int!
    var userLongitude : Int!
    var vendorId : String!
    var serviceDetail : String!
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        message = dictionary["message"] as? String
        pubChannelName = dictionary["pubChannelName"] as? String
        requestId = dictionary["requestId"] as? String
        screenName = dictionary["screenName"] as? String
        serviceId = dictionary["serviceId"] as? String
        serviceImage = dictionary["serviceImage"] as? String
        status = dictionary["status"] as? Int
        userAddress = dictionary["userAddress"] as? String
        userLatitude = dictionary["userLatitude"] as? Int
        userLongitude = dictionary["userLongitude"] as? Int
        vendorId = dictionary["vendorId"] as? String
        serviceDetail = dictionary["serviceDetail"] as? String
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if message != nil{
            dictionary["message"] = message
        }
        if pubChannelName != nil{
            dictionary["pubChannelName"] = pubChannelName
        }
        if requestId != nil{
            dictionary["requestId"] = requestId
        }
        if screenName != nil{
            dictionary["screenName"] = screenName
        }
        if serviceId != nil{
            dictionary["serviceId"] = serviceId
        }
        if serviceImage != nil{
            dictionary["serviceImage"] = serviceImage
        }
        if status != nil{
            dictionary["status"] = status
        }
        if userAddress != nil{
            dictionary["userAddress"] = userAddress
        }
        if userLatitude != nil{
            dictionary["userLatitude"] = userLatitude
        }
        if userLongitude != nil{
            dictionary["userLongitude"] = userLongitude
        }
        if vendorId != nil{
            dictionary["vendorId"] = vendorId
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        message = aDecoder.decodeObject(forKey: "message") as? String
        pubChannelName = aDecoder.decodeObject(forKey: "pubChannelName") as? String
        requestId = aDecoder.decodeObject(forKey: "requestId") as? String
        screenName = aDecoder.decodeObject(forKey: "screenName") as? String
        serviceId = aDecoder.decodeObject(forKey: "serviceId") as? String
        serviceImage = aDecoder.decodeObject(forKey: "serviceImage") as? String
        status = aDecoder.decodeObject(forKey: "status") as? Int
        userAddress = aDecoder.decodeObject(forKey: "userAddress") as? String
        userLatitude = aDecoder.decodeObject(forKey: "userLatitude") as? Int
        userLongitude = aDecoder.decodeObject(forKey: "userLongitude") as? Int
        vendorId = aDecoder.decodeObject(forKey: "vendorId") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if message != nil{
            aCoder.encode(message, forKey: "message")
        }
        if pubChannelName != nil{
            aCoder.encode(pubChannelName, forKey: "pubChannelName")
        }
        if requestId != nil{
            aCoder.encode(requestId, forKey: "requestId")
        }
        if screenName != nil{
            aCoder.encode(screenName, forKey: "screenName")
        }
        if serviceId != nil{
            aCoder.encode(serviceId, forKey: "serviceId")
        }
        if serviceImage != nil{
            aCoder.encode(serviceImage, forKey: "serviceImage")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        if userAddress != nil{
            aCoder.encode(userAddress, forKey: "userAddress")
        }
        if userLatitude != nil{
            aCoder.encode(userLatitude, forKey: "userLatitude")
        }
        if userLongitude != nil{
            aCoder.encode(userLongitude, forKey: "userLongitude")
        }
        if vendorId != nil{
            aCoder.encode(vendorId, forKey: "vendorId")
        }
        
    }
    
}
