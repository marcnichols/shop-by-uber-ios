//
//  Category.swift
//  ShopByUber
//
//  Created by Administrator on 6/6/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class Category: NSObject {
    
    var id: String?
    var catName:String?
    var categoryImage: String?
    var services:String?
    var arrCompanyData = [CompanyData]()
    var allCompanyNames = ""
    
    override init() {
        
    }
    
    init(dict:[String:Any]) {
        super.init()
        
        id = dict["_id"] as? String
        catName = dict["catName"] as? String
        categoryImage = dict["categoryImage"] as? String
        
        if let companyData = dict["companyData"] as? [[String:Any]]{
            
            for data in companyData{
                let name = data["companyName"] as? String
                if (allCompanyNames == "") { allCompanyNames += name ?? ""} else { allCompanyNames += ",\(name ?? "")"}
                let objData = CompanyData.init(dict: data)
                self.arrCompanyData.append(objData)
            
            }
            
        }
        
    }
    
    class func callGetCategoriesAPI(pageNo: Int?,numOfRecords:Int?, success: @escaping ([Category],Int) -> (), failure: @escaping () -> ()) {
        
        
        let dict = getRequestParamsForGetCategories(pageNo: pageNo, numOfRecords: numOfRecords)
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kGetCategories, inputParams: dict, success: { (dict) in
            
            if dict?["Result"] as? Bool == true {
                
                let data = dict?["Data"] as? [String:Any]
                let categories = data?["categories"] as? [[String:Any]]
                let recordcnt = data?["recordCount"] as? Int
                var arrCategory = [Category]()
                for category in categories!{
                    
                    let objCategory = Category.init(dict: category)
                    arrCategory.append(objCategory)
                    
                }
                
                success(arrCategory,recordcnt!)
            }
                
            else {
                
                let msg = dict?["Message"] as? String
                Utilities.showAlertView(title: "", message: msg)
                failure()
                
            }
            
        }) { error in
            
            Utilities.showAlertView(title: "", message: error)
            failure()
        }
        
        
    }
    
    
    
    private class func getRequestParamsForGetCategories(pageNo: Int?,numOfRecords:Int?) -> [String : Any]  {
        
        
        var dict = [String : Any]()
        
        dict["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
        dict["userId"] = DataModel.sharedInstance.onFetchUserId()
        dict["pageNo"] = pageNo
        dict["numOfRecords"] = numOfRecords
        
        return dict
    }

    class func callGetLocalCategoriesAPI(zipCode:String?,pageNo: Int?,numOfRecords:Int?, success: @escaping ([Category],Int) -> (), failure: @escaping () -> ()) {
        
        
        let dict = getRequestParamsForGetLocalData(zipCode: zipCode, pageNo: pageNo, numOfRecords: numOfRecords)
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kGetLocalCategories, inputParams: dict, success: { (dict) in
            
            if dict?["Result"] as? Bool == true {
                
                let data = dict?["Data"] as? [String:Any]
                
                let recordcnt = data?["recordCount"] as? Int
                let categories = data?["localCategories"] as? [[String:Any]]
                var arrLocal = [Category]()
                for category in categories!{
                    
                    let objCategory = Category.init(dict: category) as Category
                    arrLocal.append(objCategory)
                }
                success(arrLocal,recordcnt!)
            }
                
            else {
                
                _ = dict?["Message"] as? String
                failure()
                
            }
            
        }) { error in
            
            
            Utilities.showAlertView(title: "", message: error)
            failure()
        }
        
    }
    
    private class func getRequestParamsForGetLocalData(zipCode:String?,pageNo: Int?,numOfRecords:Int?) -> [String : Any]  {
        
        
        var dict = [String : Any]()
        
        dict["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
        dict["userId"] = DataModel.sharedInstance.onFetchUserId()
        dict["latitude"] = DataModel.sharedInstance.onFetchLatitude()
        dict["longitude"] = DataModel.sharedInstance.onFetchLongtitude()
        dict["pageNo"] = pageNo
        dict["numOfRecords"] = numOfRecords
        dict["zipcode"] = zipCode
        
        return dict
    }
    
    
    class func callGetSearchDataAPI(searchText:String?,pageNo: Int?,numOfRecords:Int?, success: @escaping ([String:Any]?) -> (), failure: @escaping () -> ()) {
        
        
        var dict = [String : Any]()
        
        dict["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
        dict["userId"] = DataModel.sharedInstance.onFetchUserId()
        dict["pageNo"] = pageNo
        dict["numOfRecords"] = numOfRecords
        dict["searchText"] = searchText
        dict["latitude"] = DataModel.sharedInstance.onFetchLatitude()
        dict["longitude"] = DataModel.sharedInstance.onFetchLongtitude()
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kGetSearchData, inputParams: dict, success: { (dict) in
            
            if dict?["Result"] as? Bool == true {
                
                let data = dict?["Data"] as? [String:Any]
                success(data)
            }
                
            else {
                
                //let msg = dict?["Message"] as? String
                //Utilities.showAlertView(title: "", message: msg)
                failure()
                
            }
            
        }) { error in
            
            
            Utilities.showAlertView(title: "", message: error)
            failure()
        }
        
    }

}



