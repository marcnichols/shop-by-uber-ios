//
//  SubCategory.swift
//  The EZ List
//
//  Created by DIFFERENZ101 on 1/3/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

import UIKit

class SubCategory: NSObject {
    
    var id: String?
    var subCatName:String?
    var subCategoryImage: String?
    
    
    override init() {
        
    }
    
    init(dict:[String:Any]) {
        super.init()
        
        id = dict["_id"] as? String
        subCatName = dict["subcatName"] as? String
        subCategoryImage = dict["subCatImage"] as? String
        
    }
    
    class func getSubSubcategories(categoryId: String?,isfromService: Bool = false,allSubcategoryRequired: Bool?, success: @escaping ([SubCategory]) -> (), failure: @escaping (_ errorMsg: String?) -> ()) {
        
        let dict = getRequestParamsForGetSubCategories(categoryId: categoryId, allSubcategoryRequired: allSubcategoryRequired)
        
        KVNProgress.show()
        APIManager.callAPI(url: Constant.serverAPI.URL.kgetSubSubcategories, inputParams: dict, success: { (dict) in
            
            if dict?["Result"] as? Bool == true {
                
                let data = dict?["Data"] as? [String:Any]
                let subcat = data?["subCategories"] as? [[String:Any]]
                var arrsubCategory = [SubCategory]()
                
                for sub in subcat! {
                    
                    let objSubCat = SubCategory.init(dict: sub)
                    arrsubCategory.append(objSubCat)
                }
                
                success(arrsubCategory)
                KVNProgress.dismiss()
            }
                
            else {
                
                if isfromService == true {
                    let msg = dict?["Message"] as? String
                    failure(msg)
                }else {
                    let msg = dict?["Message"] as? String
                    Utilities.showAlertView(title: "", message: msg)
                    failure(msg)
                }
               KVNProgress.dismiss()
            }
        }) { error in
            
            Utilities.showAlertView(title: "", message: error)
            let mess = String()
            failure(mess)
            KVNProgress.dismiss()
        }
    }
    
    private class func getRequestParamsForGetSubCategories(categoryId: String?, allSubcategoryRequired:Bool?) -> [String : Any]  {
        
        
        var dict = [String : Any]()
        
        dict["categoryId"] = categoryId
        dict["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
        dict["userId"] = DataModel.sharedInstance.onFetchUserId()
        dict["allSubcategoryRequired"] = allSubcategoryRequired
        
        return dict
    }

}
