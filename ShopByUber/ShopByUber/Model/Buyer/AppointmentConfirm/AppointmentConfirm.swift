//
//  AppointmentConfirm.swift
//  ShopByUber
//
//  Created by Differenz215 on 6/28/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class AppointmentConfirm: NSObject {
    
    var serviceId: String?
    var serviceImage: String?
    var screenName:String?
    var flatFee:String?
    var duration: String?
    var requestId: String?
    var timeSlotId: String?
    var vendor: vendorData?
    var requests = [Requests]()
    var serviceDetail: String?
    var arrAppointment = NSMutableArray()
    init(dict:[String:Any]) {
        super.init()
        
        serviceId            = dict["serviceId"]  as? String
        serviceImage         = dict["serviceImage"] as? String
        screenName           = dict["screenName"] as? String
        flatFee              = dict["flatFee"] as? String
        duration             = dict["duration"] as? String
        requestId            = dict["requestId"] as? String
        timeSlotId           = dict["timeSlotId"] as? String
        serviceDetail        = dict["serviceDetail"] as? String
        let request          = dict["requests"] as? NSArray
        if request != nil {
            for element in request! {
                
                let dict = element as? NSDictionary
                let objRequestsDetail : Requests = Requests.init(dict: dict! as! [String : Any])
                requests.append(objRequestsDetail)
                
            }
        }
        
        
        let vendorD          = dict["vendorData"] as? [String : Any]
        vendor               = vendorData.init(dict: vendorD!)
        
        
    }
}

class vendorData: NSObject{
    
    var vendorId: String?
    var name: String?
    var vendordescription : String?
    var profileImage: String?
    var speciality: String?
    var address: String?
    
    init(dict:[String:Any]) {
        super.init()
        
        vendorId                = dict["vendorId"] as? String
        name                    = dict["name"]as? String
        vendordescription       = dict["description"]as? String
        profileImage            = dict["profileImage"]as? String
        speciality              = dict["speciality"] as? String
        address                 = dict["address"] as? String
        
    }
}

class userData: NSObject{
    
    var name: String?
    var address     : String?
    var landmark    : String?
    var latitude    : Double?
    var longitude   : Double?
    
    init(dict:[String:Any]) {
        super.init()
        
        name                    = dict["name"]as? String
        address                 = dict["address"]as? String
        landmark                = dict["landmark"]as? String
        latitude                = dict["latitude"]as? Double
        longitude               = dict["longitude"]as? Double
        
    }
}

class Requests: NSObject{
    var timeslotId: String?
    var duration: String?
    var requestId: String?
    var userInfo: userData?
    
    init(dict:[String:Any]) {
        super.init()
        
        timeslotId            = dict["timeSlotId"] as? String
        duration              = dict["duration"] as? String
        requestId             = dict["requestId"] as? String
        userInfo              = userData.init(dict: (dict["userInfo"] as? [String : Any])!)
    }
}

