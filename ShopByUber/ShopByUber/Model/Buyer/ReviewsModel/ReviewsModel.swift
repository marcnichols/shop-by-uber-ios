//
//  File.swift
//  ShopByUber
//
//  Created by Differenz215 on 6/24/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class ReviewsModel: NSObject {
    
    var rating: Double?
    var Feedback:String?
    var givenBy:String?
    var feedbackDateTime: String?
    var givenByUser: String?
    init(dict:[String:Any]) {
        super.init()
        
        rating            = dict["rating"] as? Double
        Feedback          = dict["feedback"] as? String
        givenBy           = dict["givenBy"] as? String
        feedbackDateTime  = dict["feedbackDateTime"] as? String
        givenByUser       = dict["givenByUser"] as? String
   }
}
