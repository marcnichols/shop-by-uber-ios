//
//	Purchase.swift
//
//	Create by mac on 7/7/2017
//	Copyright © 2017. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class Purchase : NSObject, NSCoding{
    
    var flatFee : String!
    var requestId : String!
    var screenName : String!
    var serviceId : String!
    var serviceImage : String!
    var status : Int!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        flatFee = dictionary["flatFee"] as? String ?? ""
        requestId = dictionary["requestId"] as? String ?? ""
        screenName = dictionary["screenName"] as? String ?? ""
        serviceId = dictionary["serviceId"] as? String ?? ""
        serviceImage = dictionary["serviceImage"] as? String ?? ""
        status = dictionary["status"] as? Int ?? 0
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if flatFee != nil{
            dictionary["flatFee"] = flatFee
        }
        if requestId != nil{
            dictionary["requestId"] = requestId
        }
        if screenName != nil{
            dictionary["screenName"] = screenName
        }
        if serviceId != nil{
            dictionary["serviceId"] = serviceId
        }
        if serviceImage != nil{
            dictionary["serviceImage"] = serviceImage
        }
        if status != nil{
            dictionary["status"] = status
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        flatFee = aDecoder.decodeObject(forKey: "flatFee") as? String
        requestId = aDecoder.decodeObject(forKey: "requestId") as? String
        screenName = aDecoder.decodeObject(forKey: "screenName") as? String
        serviceId = aDecoder.decodeObject(forKey: "serviceId") as? String
        serviceImage = aDecoder.decodeObject(forKey: "serviceImage") as? String
        status = aDecoder.decodeObject(forKey: "status") as? Int
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if flatFee != nil{
            aCoder.encode(flatFee, forKey: "flatFee")
        }
        if requestId != nil{
            aCoder.encode(requestId, forKey: "requestId")
        }
        if screenName != nil{
            aCoder.encode(screenName, forKey: "screenName")
        }
        if serviceId != nil{
            aCoder.encode(serviceId, forKey: "serviceId")
        }
        if serviceImage != nil{
            aCoder.encode(serviceImage, forKey: "serviceImage")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        
    }
    
    class func callPurchasesAPI(pageNo: Int?,numOfRecords:Int?, success: @escaping ([Purchase],Int) -> (), failure: @escaping () -> ()) {
        
        
        let dict = getRequestParamsForGetCategories(pageNo: pageNo, numOfRecords: numOfRecords)
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kGetMyPurchases, inputParams: dict, success: { (dict) in
            
            if dict?["Result"] as? Bool == true {
                
                let data = dict?["Data"] as? [String:Any]
                let categories = data?["purchases"] as? [[String:Any]]
                let recordcnt = data?["recordCount"] as? Int
                var arrCategory = [Purchase]()
                for category in categories!{
                    
                    let objCategory = Purchase.init(fromDictionary: category)
                    arrCategory.append(objCategory)
                    
                }
                
                success(arrCategory,recordcnt!)
            }
                
            else {

                let msg = dict?["Message"] as? String
                Utilities.showAlertView(title: "", message: msg)
                failure()
                
            }
            
        }) { error in
            
            Utilities.showAlertView(title: "", message: error)
            failure()
        }
        
        
    }
    
    private class func getRequestParamsForGetCategories(pageNo: Int?,numOfRecords:Int?) -> [String : Any]  {
        
        
        var dict = [String : Any]()
        
        dict["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
        dict["userId"] = DataModel.sharedInstance.onFetchUserId()
        dict["pageNo"] = pageNo
        dict["numOfRecords"] = numOfRecords
        
        return dict
    }

    
}
