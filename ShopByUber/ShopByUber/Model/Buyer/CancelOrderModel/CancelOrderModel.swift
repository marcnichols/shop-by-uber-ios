//
//  CancelOrderModel.swift
//  ShopByUber
//
//  Created by Differenz215 on 7/14/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class CancelOrderModel: NSObject {
    
    var serviceId: String?
    var serviceImage: String?
    var screenName:String?
    var flatFee:String?
    var duration: String?
    var requestId: String?
    var timeSlotId: String?
    var vendor: vendorData?

    var arrAppointment = NSMutableArray()
    init(dict:[String:Any]) {
        super.init()
        
        serviceId            = dict["serviceId"]  as? String
        serviceImage         = dict["serviceImage"] as? String
        screenName           = dict["screenName"] as? String
        flatFee              = dict["flatFee"] as? String
        duration             = dict["duration"] as? String
        requestId            = dict["requestId"] as? String
        timeSlotId           = dict["timeSlotId"] as? String
        
        let vendorD          = dict["data"] as? [String : Any]
        vendor               = vendorData.init(dict: vendorD!)
        
        
    }
    
    
}


class Reason : NSObject, NSCoding{
    
    var id : String!
    var refundText : String!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        id = dictionary["id"] as? String
        refundText = dictionary["refund_text"] as? String
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if id != nil{
            dictionary["id"] = id
        }
        if refundText != nil{
            dictionary["refund_text"] = refundText
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        id = aDecoder.decodeObject(forKey: "id") as? String
        refundText = aDecoder.decodeObject(forKey: "refund_text") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if refundText != nil{
            aCoder.encode(refundText, forKey: "refund_text")
        }
        
    }
    
}




