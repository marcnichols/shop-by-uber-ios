//
//  ServiceProfile.swift
//  ShopByUber
//
//  Created by Administrator on 6/1/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class ServiceProfile: NSObject {

    //MARK: Validation
    
    class func isValidServiceData(isProfileSelected:Bool,firstName: String?, lastName: String?,screenName: String?,number:String?,state:String?,city:String?)  -> Bool {
        
        let firstName = firstName?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let lastName = lastName?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let screenName = screenName?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let number = number?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let state = state?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let city = city?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        
     //   let paypalEmail = paypalEmail?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        var message = ""
        
        if !isProfileSelected  {
            
            message = "Please add your profile image"
            
        } else if firstName == "" {
            
            message = "Please enter first name"
            
        } else if lastName == ""{
            
            message =  "Please enter last name"
            
        }else if screenName == ""{
            
            message =  "Please enter your screen name"
            
        }
//        else if address == ""{
//
//            message =  "Please enter company address/location"
//
//        }
        else if number == ""{
            
            message =  "Please enter company phone number"
            
        }
//        else if !Utilities.isValidEmail(paypalEmail!){
//
//            message =  Constant.ValidationMessage.kValidPaypalEmail
//
//        }
        else if (number?.count)! != 12{
            
            message =  Constant.ValidationMessage.kCompanyValidPhone
            
        }
        else if DataModel.sharedInstance.onFetchState() == "" || DataModel.sharedInstance.onFetchCity() == ""{
            if state == "" {
                message =  Constant.ValidationMessage.kSelectedState
            }else if city == "" {
                message =  Constant.ValidationMessage.kSelectedCity
            }
            else {
                return true
            }
        }

//        else if paypalEmail == ""{
//
//            message =  Constant.ValidationMessage.kPaypalEmail
//
//        }
        /*else if website == "" {
            
            message = Constant.ValidationMessage.kCompnayWebsite
            
        }else if !self.validateUrl(urlString: website!) {
            
            message = "Please enter valid website url"
            
        }else if description == "" {
            
            message = "Please enter brief description of your services"
            
        }*/
            
        else {
            
            return true
        }
        
        Utilities.showAlertView(title: "Alert", message: message)
        return false
    }
    
    static func validateUrl (urlString: String) -> Bool {
//        let urlRegEx = "((?:http|https)://)?(?:www\\.)?[\\w\\d\\-_]+\\.\\w{2,3}(\\.\\w{2})?(/(?<=/)(?:[\\w\\d\\-./_]+)?)?"
//        return NSPredicate(format: "SELF MATCHES %@", urlRegEx).evaluate(with: urlString)
        
        let types: NSTextCheckingResult.CheckingType = [.link]
        let detector = try? NSDataDetector(types: types.rawValue)
        guard (detector != nil && urlString.count > 0) else { return false }
        if detector!.numberOfMatches(in: urlString, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, urlString.count)) > 0 {
            return true
        }
        return false
    }
    
    //MARK: Service API Call
    
    class func callUploadImageAPI(imageType:Int,image: UIImage?, success: @escaping (String?) -> (), failure: @escaping () -> ()){
    
        KVNProgress.show()
        
        let imgData = UIImageJPEGRepresentation(image!, 0.6)
        
        APIManager.uploadImage(Constant.serverAPI.URL.kUploadImage, imageType: imageType, key: "profileImage", withRequest: imgData!, withSuccess: { (dict) in
           
            if dict?["Result"] as? Bool == true {
                
                let data = dict?["Data"] as? [String:Any]
                let imageName = data?["fileName"] as? String
                success(imageName)
                
            }
                
            else {
                
                KVNProgress.dismiss()
                let msg = dict?["Message"] as? String
                Utilities.showAlertView(title: "", message: msg)
                failure()
                
            }
            
        }) { err in
            
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: err)
            failure()
            
        }
        
    }
    
    
    class func callCreateServiceAPI(imageName: String?,companyName:String?,ownerName:String?,firstName: String?,lastName:String?,screenName:String?,addess:String?,latitude:Double,longitude:Double,zipcode:String,phoneNumber:String?,website:String?,
                                    paypalEmail: String?, description:String?, countryCode: String?,paymentMethod: Int?,paymentDetail:[String: Any]?,promoCode:String?, city:String?, state: String?, success: @escaping ([String:Any]?) -> (), failure: @escaping () -> ()) {
        
        let dict = getRequestParamsForCreateService(imageName: imageName, companyName: "",ownerName: ownerName,firstName:firstName,lastName:lastName, screenName: screenName, address: addess, latitude: latitude, longitude: longitude, zipcode: zipcode, phoneNumber: phoneNumber, website: "", paypalEmail: paypalEmail,description: description, countryCode: countryCode, paymentMethod: paymentMethod, paymentDetail: paymentDetail, promoCode: promoCode, city: city, state: state)
        
        APIManager.callAPI(url: Constant.serverAPI.URL.kAddCompanyProfile, inputParams: dict, success: { (dict) in
            
            KVNProgress.dismiss()
            
            if dict?["Result"] as? Bool == true {
                
                let data = dict?["Data"] as? [String:Any]
                success(data)
                
            }
                
            else {
                
                let msg = dict?["Message"] as? String
                Utilities.showAlertView(title: "", message: msg)
                failure()
                
            }
            
        }) { error in
            
            KVNProgress.dismiss()
            Utilities.showAlertView(title: "", message: error)
            failure()
    
        }
    }
    
    private class func getRequestParamsForCreateService(imageName: String?,companyName:String?,ownerName:String?,firstName:String?,lastName: String?,screenName:String?,address:String?,latitude:Double,longitude:Double,zipcode:String,phoneNumber:String?,website:String?,paypalEmail: String?, description:String?,countryCode: String?, paymentMethod: Int?, paymentDetail: [String:Any]?,promoCode:String?,city:String?,state:String?) -> [String : Any]  {
        
        
        var dict = [String : Any]()
        
        dict["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
        dict["vendorId"] = DataModel.sharedInstance.onFetchUserId()
        dict["companyName"] = companyName?.trimmingCharacters(in: .whitespaces)
        dict["comapnyScreenName"] = screenName?.trimmingCharacters(in: .whitespaces)
        dict["ownerName"] = ownerName?.trimmingCharacters(in: .whitespaces)
        dict["firstName"] = firstName?.trimmingCharacters(in: .whitespaces)
        dict["lastName"] = lastName?.trimmingCharacters(in: .whitespaces)
        dict["companyAddress"] = address?.trimmingCharacters(in: .whitespaces)
        dict["companyPhone"] = phoneNumber!
        dict["paypalEmail"] = paypalEmail
        dict["websiteUrl"] = website?.trimmingCharacters(in: .whitespaces)
        dict["serviceDescription"] = description?.trimmingCharacters(in: .whitespaces)
        dict["profileImage"] = imageName
        dict["latitude"] = latitude
        dict["longitude"] = longitude
        dict["zipcode"] = zipcode
        dict["companyPhoneNumber"] = phoneNumber
        dict["countryCode"] = countryCode
        dict["paymentMethod"] = paymentMethod
        dict["paymentDetail"] = paymentDetail
        dict["promoCode"] = promoCode ?? ""
        dict["city"] = city
        dict["state"] = state
        
        return dict
    }
    
    
}
