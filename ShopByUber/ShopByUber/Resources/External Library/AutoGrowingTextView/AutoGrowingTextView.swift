//
//  AutoGrowingTextView.swift
//  QSMobile
//
//  Created by Administrator on 6/29/17.
//  Copyright © 2017 Differenz System Pvt. Ltd. All rights reserved.
//

import UIKit

class AutoGrowingTextView: UITextView {
    
    var heightConstraint: NSLayoutConstraint?
    var minHeightConstraint: NSLayoutConstraint?
    var maxHeightConstraint: NSLayoutConstraint?
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        
        super.init(frame: frame, textContainer: textContainer)
        associateConstraints()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        associateConstraints()
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    private func associateConstraints() {
        // iterate through all text view's constraints and identify
        // height, max height and min height constraints.
        for constraint: NSLayoutConstraint in constraints {
            if constraint.firstAttribute == .height {
                if constraint.relation == .equal {
                    heightConstraint = constraint
                }
                else if constraint.relation == .lessThanOrEqual {
                    maxHeightConstraint = constraint
                }
                else if constraint.relation == .greaterThanOrEqual {
                    minHeightConstraint = constraint
                }
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        // calculate size needed for the text to be visible without scrolling
        var sizeThatFits: CGSize? = layoutManager.usedRect(for: textContainer).size
        sizeThatFits?.height += textContainerInset.top + textContainerInset.bottom
        var newHeight: CGFloat? = sizeThatFits?.height
        // if there is any minimal height constraint set, make sure we consider that
        
        if newHeight != nil {
         
            if (maxHeightConstraint != nil) {
                newHeight = min(newHeight!, maxHeightConstraint!.constant)
            }
            // if there is any maximal height constraint set, make sure we consider that
            if (minHeightConstraint != nil) {
                newHeight = max(newHeight!, minHeightConstraint!.constant)
            }
            // update the height constraint
            heightConstraint?.constant = newHeight!
        }
    }

}
