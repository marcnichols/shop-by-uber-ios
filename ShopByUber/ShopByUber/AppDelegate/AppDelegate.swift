//
//  AppDelegate.swift
//  ShopByUber
//
//  Created by Administrator on 5/23/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit
import UserNotifications
import AudioToolbox
import Quickblox
import Flurry_iOS_SDK

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate,FCChatHeadsControllerDatasource {
    
    var window: UIWindow?
    var delegate: UpdateCountDelegate?
    var frontNavigationController : UINavigationController?
    var chatheadController: FCChatHeadsController?
    var isOnPrivateChatVC = false
    //Quickblox
    var strDialogID: String?
    var check:Bool = false
    var isOnTrackProgressVC: Bool = false
    var trackProgressVC: TrackProgressVC?
    var isRotate:Bool = false
    var isfromMyWantedService: Bool = false
   // var isFromRequestservice:Bool = false
//    var isFromRequestWantedService : Bool = false
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        //Check for version update
        self.VersionChecker()
        
        UIApplication.shared.statusBarStyle = .lightContent
        
        //Set UIAlert Controller Button Text Color
        UIView.appearance(whenContainedInInstancesOf: [UIAlertController.self]).tintColor = UIColor.init(red: 48/255, green: 173/255, blue: 99/255, alpha: 1.0)
        
        //Twitter
        //Fabric.with([Twitter.self])
        Fabric.with([Twitter.self, Crashlytics.self])
        
        //Keyboard
        IQKeyboardManager.shared().isEnabled = true
       
        //Enable Google API
        GMSPlacesClient.provideAPIKey(Constant.googleAPIKey)
        GMSServices.provideAPIKey(Constant.googleAPIKey)
        
        //MARK:- Register For Notification
        self.onRegisterForRemoteNotification()
        if launchOptions != nil {
            //opened from a push notification when the app is closed
            var userInfo = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as? [AnyHashable: Any] ?? [AnyHashable: Any]()
            if userInfo.count != 0 {
                let userData = userInfo["aps"] as? NSDictionary
                Singleton.sharedManager.isNotification = true
                Singleton.sharedManager.unreadNotificationCount += 1
                self.delegate?.UpdateNotificationCount()
                Singleton.sharedManager.userInfo = notificationData.init(dict: userData?["params"] as! [String : Any], withAction: false)
            }
        }
        
        //Disable keyboard for chat view controller
        IQKeyboardManager.shared().disabledToolbarClasses.add(PrivateChatVC.self)
        
        //Configure quickblox
        self.configureQuickBloxAccount()
        
        //MARK:- Initial Setup
        
        self.onSetupDashboardPage()
        
        _ = LocationManager.sharedManger
        
        //MARK:- Paypal initialize
        PayPalMobile .initializeWithClientIds(forEnvironments: [PayPalEnvironmentProduction: Constant.paypal_Client_Id,
                                                                PayPalEnvironmentSandbox: Constant.sandbox_paypal_Client_Id])
        
        //MARK:- Strip integration
        //STPPaymentConfiguration.shared().publishableKey = Constant.stripe_key
        
        //MARK:- Flurry Integration
//        Flurry.startSession("DR23F66Q68FNRGJSYNVB", with: FlurrySessionBuilder
//            .init()
//            .withCrashReporting(true)
//            .withLogLevel(FlurryLogLevelAll))
        //Client Account
        Flurry.startSession("2CT737NGBCSJDD5TQQCS", with: FlurrySessionBuilder
            .init()
            .withCrashReporting(true)
            .withLogLevel(FlurryLogLevelAll))
        
        return true
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url as URL!, sourceApplication: sourceApplication, annotation: annotation)
    }

    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    //MARK:- Force Update
    func test(new: String) -> Bool{
        var Update:Bool?
        if let running = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        {
            print("==========================================>Version:",running)
           //if Current version(running) less then server version(new)- Update
            if running < new {
                print("=====================>store version is newer")
                Update = true
            }
            else{
                print("=====================>App running with newer version")
                Update = false
            }
        }
        return Update!
    }
    
    func VersionChecker() {
        
        checkVersion(success: {(checkUpdate,ver) in
            
            print("Ver=========================>",ver)
            
            if checkUpdate == true{

                self.check = self.test(new: ver)
                
                if self.check == true{
                    let alert = UIAlertController(title:
                        "Update Available", message: "Upgraded version of the application is now available in the App Store. Please update to continue.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Update", style: .default, handler: {(action:UIAlertAction!) in
                        print("you have pressed the ok button")
                        if let url = URL(string: "https://itunes.apple.com/us/app/the-ez-list/id1305101269?ls=1&mt=8"),
                            UIApplication.shared.canOpenURL(url){
                            UIApplication.shared.openURL(url)
                        }
                    }))
                    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                    
                    let window = UIApplication.shared.keyWindow
                    window?.rootViewController?.present(alert, animated: true, completion: nil)
                }}
            
        }) {
            print("Failed...")
        }
    }
    func checkVersion(success:@escaping(Bool,String)->(),failure:@escaping()->()){
        
        if DataModel.sharedInstance.isUserLoggedIn() {
            var dictParam=[String:Any]()
            dictParam["userId"] = DataModel.sharedInstance.onFetchUserId()
            dictParam["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
            
            APIManager.callAPI(url: Constant.serverAPI.URL.kcheckLogin, inputParams: dictParam, success: { (response) in
                if response?["Result"] as? Bool == true {
                    let data = response?["Data"] as? [String:Any]
                    let checkUpdate = data!["checkUpdate"] as? Bool
                    let version = String(data?["version"] as? Float ?? 0)
                    
                    print("version in API============>",version)
                    
                    success(checkUpdate!,version)
                }
                else {
                    _ = response?["Message"] as? String
                    failure()
                }
                
            }) { (error) in
                
                KVNProgress.dismiss()
                Utilities.showAlertView(title: "", message: error)
                failure()
            }
        }
    }
    
    // MARK: - Register PushNotification Handler Methods
    func onRegisterForRemoteNotification() {
        
        if #available(iOS 10.0, *) {
            let center  = UNUserNotificationCenter.current()
            center.delegate = self
            
            let replyAction = UNNotificationAction(identifier: "reply.action",title: "Reply",options: [.foreground])
            let replyCategory = UNNotificationCategory(identifier: "reply",actions: [replyAction], intentIdentifiers: [],options: [])
            center.setNotificationCategories([replyCategory])
            
            center.requestAuthorization(options: [.alert,.sound]) { (granted, error) in
                if error == nil{
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                }
            }
        } else {
            let settings = UIUserNotificationSettings(types: [.alert,.sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        var token = ""
        for i in 0..<deviceToken.count {
            token = token + String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }
        print("Device token : \(token) ")
        Singleton.sharedManager.devieToken = token
        DataModel.sharedInstance.setValueInUserDefaults(value: token, key: Constant.DataModelKey.KDeviceToken)
        
        if DataModel.sharedInstance.isUserLoggedIn() {
            var dictParam=[String:Any]()
            dictParam["userId"] = DataModel.sharedInstance.onFetchUserId()
            dictParam["sessionId"] = DataModel.sharedInstance.onFetchSessionId()
            dictParam["deviceType"] = Constant.deviceType
            dictParam["deviceToken"] = token
            
            APIManager.callAPI(url: Constant.serverAPI.URL.kUpdateDeviceToken, inputParams: dictParam, success: { (response) in
                
            }) { (error) in
            }
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error.localizedDescription)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        NSLog("application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any])")
        self.handlePushnotfication(userInfo: userInfo as! [String : Any])
    }
    
    // Called when a notification is delivered to a foreground app.
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        print("User Info = ",notification.request.content.userInfo)
        let userInfo = notification.request.content.userInfo
        let catgory = notification.request.content.categoryIdentifier
        
        
        if catgory == "request" || catgory == "chatMessage" {
            //Display Local Notification
            completionHandler([.alert, .sound])
        } else {
            //Display push Notitication
            
            self.handlePushnotfication(userInfo: userInfo as! [String : Any])
        }
    }
    
    // Called to let your app know which action was selected by the user for a given notification.
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        print("User Info = ",response.notification.request.content.userInfo)
        completionHandler()
        
        let userInfo = response.notification.request.content.userInfo
        let action = response.actionIdentifier
        let catgory = response.notification.request.content.categoryIdentifier
        
        if catgory == "chatMessage" {
            
            let vc = PrivateChatVC(nibName: "PrivateChatVC", bundle: nil)
            vc.strDialogId = userInfo["dialogId"] as? String
            vc.strReceiverId = userInfo["userId"] as? String
            vc.strDialogName = userInfo["userName"] as? String
            vc.occupantId = userInfo["occupantId"] as? Int
            vc.strRequestID = userInfo["requestId"] as? String
            vc.isfromlocalNotification = true
            
            let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
            navigationController.pushViewController(vc, animated: true)
        }
        else if catgory == "request" {
            //Local Notification
            
                let vc = NotificationVC(nibName: "NotificationVC", bundle: nil)
                let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
                navigationController.pushViewController(vc, animated: true)
        
        }else {
            if action == "reply.action"  {
            //Actionable Push Notification
                self.handlePushnotfication(userInfo: userInfo as! [String : Any], withAction: true)
            } else {
            //Push Notification
                self.handlePushnotfication(userInfo: userInfo as! [String : Any])
            }
        }
    }
    
    // MARK: - PushNotification Handler Methods
    func handlePushnotfication(userInfo:[String:Any], withAction: Bool = false)
    {
        print("\(userInfo)")
        
        let userData = userInfo["aps"] as? [String:Any]
        let parama = userData!["params"] as? [String:Any]
        let state = UIApplication.shared.applicationState
        
    
        if state == .active {
            //UnreadCountUpdate
            Singleton.sharedManager.unreadNotificationCount += 1
            //Check for chat message
            let notifyType = parama?["notifyType"] as? Int ?? 0
            self.delegate?.UpdateNotificationCount()

            if notifyType != 13 {
                if notifyType == 0 {

                    let param = userData?["params"] as? [String:Any]
                    let currentUser = param?["dialogId"] as? String
                    print("Current User---->",currentUser!)
                    print(param!)
                    
                    if let currentDialog = self.strDialogID, currentUser == currentDialog && isOnPrivateChatVC {
                        // & on the privatechatVC, dont show local notification
                        print("dont Show Notification")
                    }else {
                        //Show local notification
                        
                        let notify = UILocalNotification()
                        notify.alertTitle = param?["userName"] as? String
                        notify.fireDate = NSDate(timeIntervalSinceNow: 1) as Date
                        notify.alertBody = param?["message"] as? String
                        notify.soundName = UILocalNotificationDefaultSoundName
                        notify.category = "chatMessage"
                        notify.userInfo = userData?["params"] as! [String : Any]
                        UIApplication.shared.scheduleLocalNotification(notify)
                    }
                }else if notifyType == 6 && self.isOnTrackProgressVC {
                    trackProgressVC?.showNotificationPopup()
                }else if notifyType == 7 && self.isOnTrackProgressVC {
                    //Dont Show Notification Alert as buyer has completed the service & Review pop up has appeard on the Screen
                }
                else if ((notifyType == 14 || notifyType == 9 ) && self.isOnPrivateChatVC){
                    let param = userData!["params"] as? [String:Any]
                    
                    NotificationCenter.default.post(name: Notification.Name("SilentNotification"), object: nil ,
                                                 userInfo: ["requestId" : param?["requestId"] as? String])
                }
                else
                {
                 if notifyType != 14 && notifyType != 9
                 {
                    let alert = UIAlertController(title: nil, message: userData?["alert"] as? String, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        let objBaseView = BaseView()
                        objBaseView.HandlePushNotification(userInfo: notificationData.init(dict: userData?["params"] as! [String : Any], withAction: withAction))
                    }))
                    let topView = UIApplication.topViewController()
                    topView?.present(alert, animated: true, completion: nil)
                 }
                }
            }
            else {
                //Dont show alert in Local Notification
            }
        } else {
            // Background
            NSLog("background action \(withAction)")
            Singleton.sharedManager.isNotification = true
            Singleton.sharedManager.userInfo = notificationData.init(dict: userData?["params"] as! [String : Any], withAction: withAction)
            let objBaseView = BaseView()
            objBaseView.HandlePushNotification(userInfo: notificationData.init(dict: userData?["params"] as! [String : Any], withAction: withAction))
        }
    }
    
    //MARK: - Quickblox configuration
    func configureQuickBloxAccount() {
        QBSettings.applicationID = Constant.QuickBloxCredentials.kQBApplicationID
        QBSettings.authKey = Constant.QuickBloxCredentials.kQBAuthKey
        QBSettings.authSecret = Constant.QuickBloxCredentials.kQBAuthSecret
        QBSettings.accountKey = Constant.QuickBloxCredentials.kQBAccountKey
        
        QBSettings.keepAliveInterval = 30
        QBSettings.autoReconnectEnabled = true
        
        // enabling carbons for chat
        QBSettings.carbonsEnabled = true
        
        // Enables Quickblox REST API calls debug console output.
        QBSettings.logLevel = QBLogLevel.nothing
        
        // Enables detailed XMPP logging in console output.
        QBSettings.disableFileLogging()
    }
//    if dict?["isServicesCreated"] as! Bool {
//    let seller = SellerVC(nibName: "SellerVC", bundle: nil) as SellerVC
//    _ = self.navigationController?.pushViewController(seller, animated: true)
//    }
    //MARK: - Setup root view
    func onSetupDashboardPage(switchedToSeller: Bool = false) {
        
        let viewStatus = UIView.init(frame: CGRect(x: 00, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: 20))
        var frontViewController = UIViewController()
        
        
        //Show Commercial view first time.
//        if DataModel.sharedInstance.isCommercialPlayed() == false {
//            frontViewController = VideoTutorialVC(nibName: "VideoTutorialVC", bundle: nil)
//        }
        //Auto Login Logic
        /*else*/ if DataModel.sharedInstance.isUserLoggedIn() {
            viewStatus.backgroundColor = .black
            if DataModel.sharedInstance.onFetchUserLoginType() == Constant.UserType.Buyer{
                
                //Buyer
                appShareManager.selectedMenuItemIndex = 0
                frontViewController = BuyerVC(nibName: "BuyerVC", bundle: nil)
                
            }else{
                
                //Seller
                if DataModel.sharedInstance.isProfileCreated() == false {
                    let serviceProfileVC = ServiceProfileVC(nibName: "ServiceProfileVC", bundle: nil)
                    frontViewController = serviceProfileVC
                }else{

                    //Profile Created then redirect him to dashboard
                    if DataModel.sharedInstance.isServiceAvailable()
                    {
                        appShareManager.selectedMenuItemIndex = 0
                        let sellerVC = SellerVC(nibName: "SellerVC", bundle: nil)
                         sellerVC.showAddService = false
//                        if switchedToSeller {
//                            sellerVC.showAddService = false
//                        }
                        frontViewController = sellerVC
                    }
                    else {
                        appShareManager.selectedMenuItemIndex = 0
                        let sellerVC = SellerVC(nibName: "SellerVC", bundle: nil)
                         sellerVC.showAddService = true
                         sellerVC.isFromMenu = true
//                        if switchedToSeller {
//                            sellerVC.showAddService = true
//                        }
                        frontViewController = sellerVC
                    }

                }
            }
            // Quickblox
            QuickBloxManager.sharedInstance.checkAndLoginInQuickBlox()
        }else{
            viewStatus.backgroundColor = .black
            frontViewController = LoginVC(nibName: "LoginVC", bundle: nil)
        }
        
        let rearViewController = MenuVC(nibName: "MenuVC", bundle: nil)
        
        let frontNavigationController = UINavigationController(rootViewController: frontViewController)
        frontNavigationController.isNavigationBarHidden = true
        
        let revealController = SWRevealViewController(rearViewController: rearViewController, frontViewController: frontNavigationController)
        revealController?.panGestureRecognizer().isEnabled = true
        
        //revealController?.rightViewController = rearViewController
        
        let navigationController = UINavigationController.init(rootViewController: revealController!)
        navigationController.isNavigationBarHidden = true
        
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
        
        let window = UIApplication.shared.keyWindow
        window?.addSubview(viewStatus)
    }
    
    func toggleFlash(isOn:AVCaptureTorchMode = .off) {
        if let device = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo), device.hasTorch {
            do {
                try device.lockForConfiguration()
                _ = !device.isTorchActive
                
                try device.setTorchModeOnWithLevel(1.0)
                
                device.torchMode = isOn
                device.unlockForConfiguration()
            } catch {
                print("error")
            }
        }
    }
    
    func ShowBubble(promoCode: String) {
        
        self.chatheadController = FCChatHeadsController()
        
        let bubbleView = UIView()
        let bubbleLabel = UILabel()
        let codeLabel = UILabel()
        
        if Constant.DeviceType.IS_IPAD {
            bubbleView.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
            bubbleLabel.frame = CGRect(x: 10, y: 25, width: 80, height: 28)
            codeLabel.frame = CGRect(x: 20, y: 55, width: 60, height: 25)
        }else {
            bubbleView.frame = CGRect(x: 0, y: 0, width: 70, height: 70)
            bubbleLabel.frame = CGRect(x: 5, y: 18, width: 60, height: 20)
            codeLabel.frame = CGRect(x: 15, y: 40, width: 40, height: 18)
        }
        bubbleView.backgroundColor = UIColor.white
        bubbleView.layer.cornerRadius = bubbleView.frame.size.height / 2
        bubbleView.layer.masksToBounds = true
        bubbleView.layer.borderColor = UIColor.init(red: 48/255, green: 173/255, blue: 99/255, alpha: 1.0).cgColor
        bubbleView.layer.borderWidth = 1

        //Title Label Attributes
        let myString = "PROMO CODE"
        let myAttribute = [NSFontAttributeName:UIFont(name: "SourceSansPro-Bold", size: Constant.DeviceType.IS_IPAD ? 13 : 9)!,NSForegroundColorAttributeName : UIColor.black]
        let myAttrString = NSAttributedString(string: myString, attributes: myAttribute)
        bubbleLabel.attributedText = myAttrString
        bubbleLabel.textAlignment = .center
        bubbleLabel.numberOfLines = 1
        
        //Code Label Attributes
        let codeString = promoCode
        let codeAttribute = [NSFontAttributeName:UIFont(name: "SourceSansPro-Regular", size: Constant.DeviceType.IS_IPAD ? 16 : 12)!,NSForegroundColorAttributeName : UIColor.init(red: 48/255, green: 173/255, blue: 99/255, alpha: 1.0)]
        let codeAttrString = NSAttributedString(string: codeString, attributes: codeAttribute)
        codeLabel.attributedText = codeAttrString
        codeLabel.layer.borderColor = UIColor.gray.cgColor
        codeLabel.layer.borderWidth = 1
        codeLabel.textAlignment = .center
        codeLabel.layer.cornerRadius = 2
        
        
        bubbleView.addSubview(bubbleLabel)
        bubbleView.addSubview(codeLabel)
        chatheadController?.presentChatHead(with: bubbleView, chatID: "123")
    }
    
    func onLogout() {
        
        DataModel.sharedInstance.resetUserDataWhenLogout()
        appShareManager.selectedMenuItemIndex = 0
        Singleton.sharedManager.unreadNotificationCount = 0
        let frontViewController = LoginVC(nibName: "LoginVC", bundle: nil)
        
        let rearViewController = MenuVC(nibName: "MenuVC", bundle: nil)
        
        let frontNavigationController = UINavigationController(rootViewController: frontViewController)
        frontNavigationController.isNavigationBarHidden = true
        
        let revealController = SWRevealViewController(rearViewController: rearViewController, frontViewController: frontNavigationController)
        
        let navigationController = UINavigationController.init(rootViewController: revealController!)
        navigationController.isNavigationBarHidden = true
        
        UIApplication.shared.delegate?.window??.rootViewController = navigationController
        UIApplication.shared.delegate?.window??.makeKeyAndVisible()
        
        let viewStatus = UIView.init(frame: CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: 20))
        viewStatus.backgroundColor = .black// #colorLiteral(red: 0.168627451, green: 0.5294117647, blue: 0.6, alpha: 1)
        let window = UIApplication.shared.keyWindow
        window?.addSubview(viewStatus)
        
        //Dismiss Bubble
        dismissBubble()
        
    }
    func dismissBubble() {
        chatheadController?.dismissAllChatHeads(true)
    }
    
    // MARK: - Device Rotation
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        if self.isRotate == true
        {
            // Unlock landscape view orientations for video view controller
            return .allButUpsideDown;
        }
        
        // Only allow portrait (standard behaviour)
        return .portrait;
    }
}

protocol UpdateCountDelegate{
    func UpdateNotificationCount()
}
